Disaster Recovery
===

**TODO:** We should have a VM ready with the credentials to perform the following operations

**TODO:** Set up notifications in case AWS Data Pipeline fails to create the backup

**TODO:** Set up scheduled heroku backups

---------------------------------------


*Get URL for last DB snapshot*

    heroku pg:backups public-url <snapshot id - leave empty for last backup> --app <heroku production app>


*Restore DB from snapshot*

    heroku pg:backups restore  "url to last backup" DATABASE_URL --app <heroku production app>



*Rebuild Elasticsearch index*

    heroku run rake chewy:reset --app <heroku production app>



*Restore S3 assets from snapshot*

    aws s3 cp s3://<s3 backup bucket>/<date last backup>/ s3://<production s3 bucket>/uploads --recursive --acl public-read



