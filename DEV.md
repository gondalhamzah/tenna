# BuildSourced

## Reset Heroku DEV Environment


### Background

The application has many entities and permissions that make testing a challenge. Therefore, we use a set of test fixtures to bootstrap the DEV environment. We don't use seed data, since we don't want this data to be loaded into PROD accidentally.

When loading fixtures into the DB, Rails disables constraints, which is not allowed in Heroku Postgres, so we must build a DB dump locally and then import into Heroku Postgres.

### Create/Configure a Fresh DB

* Re-create your local development DB.
* `rake db:reset db:migrate`
* `rake db:fixtures:load`
* Comment out `after_validation` & `update_index` in `zip_code.rb`.
* `rake db:seed_fu`

### Dump DB

You need to store this DB dump somewhere publicly accessible (i.e. Dropbox). Replace `atwoodjw` before running.

* `pg_dump -Fc --no-acl --no-owner -h localhost -U atwoodjw buildsourced_development > /Users/atwoodjw/Dropbox/Public/buildsourced_development.dump`

### Restore DB

Replace `https://dl.dropboxusercontent.com/u/27526562/buildsourced_development.dump` before running.

* `heroku pg:backups restore 'https://dl.dropboxusercontent.com/u/27526562/buildsourced_development.dump' DATABASE_URL -a buildsourced-development --confirm buildsourced-development`

### Reset Elasticsearch Index

* `heroku run rake chewy:reset -a buildsourced-development`

### Clear S3 Bucket

We store uploaded attachments in a S3 bucket on AWS. When we reset the database, we abandon all the uploaded assets.

* Log into AWS and delete the `buildsourced-development-rz/uploads` bucket.

### Extra Credit

A few test fixtures have attachments, which can no longer be referenced after this process. (The attachments are stored on your local machine, not S3.)

* Sign in as an admin user and remove and re-upload photos/attachments.
