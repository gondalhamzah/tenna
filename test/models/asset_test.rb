# http://jeffkreeftmeijer.com/2014/using-test-fixtures-with-carrierwave/

require 'test_helper'

class AssetTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess

  test 'has a photo' do
    asset = assets(:equipment_1)
    assert File.exist? asset.photos[0].file.path
    assert File.exist? asset.photos[0].file.thumb.path
  end

  test 'creates asset with photos' do
    asset = Asset.create!(
      photos_attributes: [
      {file: fixture_file_upload('/files/excavator.jpg', 'image/jpg')},
      {file: fixture_file_upload('/files/wrecking_ball.jpg', 'image/jpg')},
    ],
      project: projects(:company_a_project_1),
      type: 'Material',
      title: 'A',
      company: companies(:company_a),
      creator: users(:company_a_company_admin),
      category: categories(:equipment_category_1),
      location_zip: '44672',
      #country: 'United States',
      available_on: Date.today,
      expires_on: Date.today,
      quantity: 1,
      market_value: 1,
      status: 'available',
      public: false,
      owned: true,
      rental: false
    )

    assert File.exist? asset.photos[0].file.path
    assert File.exist? asset.photos[0].file.thumb.path

    assert File.exist? asset.photos[1].file.path
    assert File.exist? asset.photos[1].file.thumb.path
  end

  test 'updates asset with photos' do
    asset = assets(:equipment_2)

    asset.photos.build(file: fixture_file_upload('/files/excavator.jpg', 'image/jpg'))
    asset.photos.build(file: fixture_file_upload('/files/wrecking_ball.jpg', 'image/jpg'))

    asset.save!

    assert File.exist? asset.photos[0].file.path
    assert File.exist? asset.photos[0].file.thumb.path

    assert File.exist? asset.photos[1].file.path
    assert File.exist? asset.photos[1].file.thumb.path
  end

  test 'creates asset with photo and validation errors' do
    asset_params = ActionController::Parameters.new(
        photos_attributes: [
            {file: fixture_file_upload('/files/wrecking_ball.jpg', 'image/jpg')},
            {file: fixture_file_upload('/files/wrecking_ball.jpg', 'image/jpg')},
            {file: fixture_file_upload('/files/wrecking_ball.jpg', 'image/jpg')},
            {file: fixture_file_upload('/files/wrecking_ball.jpg', 'image/jpg')}
        ],
        project: projects(:company_a_project_1),
        type: 'Material',
        title: 'A',
        company: companies(:company_a),
        creator: users(:company_a_company_admin),
        category: categories(:equipment_category_1),
        location_zip: '44672',
        #country: 'United States',
        available_on: Date.today,
        expires_on: Date.today,
        quantity: 1,
        market_value: 1,
        status: 'available',
        public: false,
        owned: true,
        rental: false
    )

    asset = Asset.new(asset_params.to_hash)
    assert asset.save

    assert File.exist? asset.photos[0].file.path
    assert File.exist? asset.photos[0].file.thumb.path
  end

end
