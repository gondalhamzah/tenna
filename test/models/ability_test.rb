require 'test_helper'

class AbilityTest < ActiveSupport::TestCase
  ## START: Admin
  test 'admins can do anything' do
    ability = Ability.new(users(:admin_1))
    assert ability.can?(:manage, :all)

    assert ability.can?(:create, Company.new())
    assert ability.can?(:create, Project.new())
    assert ability.can?(:create, Asset.new())
  end
  ## END: Admin

  ## START: Companies
  test 'company admins cannot create companies' do
    assert Ability.new(users(:company_a_company_admin)).cannot?(:create, Company.new())
  end

  test 'company admins can update their company' do
    ability = Ability.new(users(:company_a_company_admin))
    assert ability.can?(:update, companies(:company_a))
    assert ability.cannot?(:update, companies(:company_b))
  end

  test 'company admins cannot delete their company' do
    assert Ability.new(users(:company_a_company_admin)).cannot?(:destroy, companies(:company_a))
  end

  test 'company users can read their company' do
    ability = Ability.new(users(:company_a_user_1))
    assert ability.can?(:read, companies(:company_a))
    assert ability.cannot?(:read, companies(:company_b))
  end
  ## END: Companies

  ## START: Users
    test 'company admins can invite users to their company' do
      ability = Ability.new(users(:company_a_company_admin))
      assert ability.can?(:invite, users(:company_a_user_1))
      assert ability.cannot?(:invite, users(:company_b_user_1))
    end

    test 'project admins can invite users to their company' do
      ability = Ability.new(users(:company_a_project_admin_1))
      assert ability.can?(:invite, users(:company_a_user_1))
      assert ability.cannot?(:invite, users(:company_b_user_1))
    end
  ## END: Users

  ## START: Projects
  test 'company admins can manage company projects' do
    ability = Ability.new(users(:company_a_company_admin))
    assert ability.can?(:create, Project.new(:company => companies(:company_a)))
    assert ability.can?(:update, projects(:company_a_project_1))
    assert ability.can?(:destroy, projects(:company_a_project_1))

    assert ability.cannot?(:create, Project.new(:company => companies(:company_b)))
    assert ability.cannot?(:update, projects(:company_b_project_1))
    assert ability.cannot?(:destroy, projects(:company_b_project_1))
  end

  test 'project admins cannot create projects' do
    assert Ability.new(users(:company_a_project_admin_1)).cannot?(:create, Project.new(:company => companies(:company_a)))
  end

  test 'project admins can modify their projects' do
    ability = Ability.new(users(:company_a_project_admin_1))
    assert ability.can?(:update, projects(:company_a_project_1))
    assert ability.can?(:destroy, projects(:company_a_project_1))

    assert ability.cannot?(:update, projects(:company_a_project_2))
    assert ability.cannot?(:destroy, projects(:company_a_project_2))

    assert ability.cannot?(:update, projects(:company_b_project_1))
    assert ability.cannot?(:destroy, projects(:company_b_project_1))
  end

  test 'company users can read company projects' do
    ability = Ability.new(users(:company_a_user_1))
    assert ability.can?(:read, projects(:company_a_project_1))
    assert ability.cannot?(:read, projects(:company_b_project_1))
  end
  ## END: Projects

  ## START: Assets
  test 'company admins can manage company assets' do
    ability = Ability.new(users(:company_a_company_admin))
    assert ability.can?(:create, Asset.new(:company => companies(:company_a)))
    assert ability.can?(:update, assets(:equipment_1))
    assert ability.can?(:destroy, assets(:equipment_1))

    assert ability.cannot?(:create, Asset.new(:company => companies(:company_b)))
    assert ability.cannot?(:update, assets(:equipment_4))
    assert ability.cannot?(:destroy, assets(:equipment_4))
  end

  test 'company admins can make company assets public' do
    ability = Ability.new(users(:company_a_company_admin))
    assert ability.can?(:toggle_marketplace, assets(:equipment_1))
    assert ability.cannot?(:toggle_marketplace, assets(:equipment_4))
  end

  test 'project admins can make project assets public' do
    ability = Ability.new(users(:company_a_project_admin_1))
    assert ability.can?(:toggle_marketplace, assets(:equipment_1))
    assert ability.cannot?(:toggle_marketplace, assets(:material_3))
    assert ability.cannot?(:toggle_marketplace, assets(:equipment_4))
  end

  test 'project users cannot make project assets public' do
    ability = Ability.new(users(:company_a_user_1))
    assert ability.cannot?(:toggle_marketplace, assets(:equipment_1))
    assert ability.cannot?(:toggle_marketplace, assets(:material_3))
    assert ability.cannot?(:toggle_marketplace, assets(:equipment_4))
  end

  test 'project admins can manage project assets' do
    ability = Ability.new(users(:company_a_project_admin_1))
    assert ability.can?(:create, Asset.new(:company => companies(:company_a)))
    assert ability.can?(:update, assets(:equipment_1))
    assert ability.can?(:destroy, assets(:equipment_1))

    # same company, different project
    assert ability.cannot?(:update, assets(:material_3))
    assert ability.cannot?(:destroy, assets(:material_3))

    # different company, different project
    assert ability.cannot?(:create, Asset.new(:company => companies(:company_b)))
    assert ability.cannot?(:update, assets(:equipment_4))
    assert ability.cannot?(:destroy, assets(:equipment_4))
  end

  test 'project users can manage their project assets' do
    ability = Ability.new(users(:company_a_user_1))
    assert ability.can?(:create, Asset.new(:company => companies(:company_a)))
    assert ability.can?(:update, assets(:equipment_1))
    assert ability.can?(:destroy, assets(:equipment_1))

    assert ability.cannot?(:create, Asset.new())
    assert ability.cannot?(:update, assets(:equipment_2))
    assert ability.cannot?(:destroy, assets(:equipment_2))
  end

  test 'company users can read company assets' do
    ability = Ability.new(users(:company_a_user_1))
    assert ability.can?(:read, assets(:equipment_2))
    assert ability.cannot?(:read, assets(:equipment_5))
  end

  test 'public users can manage their assets' do
    ability = Ability.new(users(:public_user_1))
    assert ability.can?(:create, Asset.new())
    assert ability.can?(:update, assets(:equipment_7))
    assert ability.can?(:destroy, assets(:equipment_7))

    assert ability.cannot?(:create, Asset.new(:company => companies(:company_a)))
    assert ability.cannot?(:update, assets(:equipment_1))
    assert ability.cannot?(:destroy, assets(:equipment_1))

    assert ability.cannot?(:update, assets(:material_8))
    assert ability.cannot?(:destroy, assets(:material_8))
  end

  test 'public users cannot read companies or projects' do
    ability = Ability.new(users(:public_user_1))
    assert ability.cannot?(:read, companies(:company_a))
    assert ability.cannot?(:read, projects(:company_a_project_1))
  end

  test 'all users can read public assets' do
    ability = Ability.new(users(:company_a_user_1))
    assert ability.can?(:read, assets(:material_8))

    ability = Ability.new(users(:public_user_1))
    assert ability.can?(:read, assets(:material_8))
  end
  ## END: Assets
end
