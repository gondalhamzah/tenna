require 'yaml'

equipment_categories = [
  "Arrow / VMB Board",
  "Attachments",
  "Attenuator",
  "ATV",
  "Auto/ Pick-up Truck/ Van",
  "Boat",
  "Cement Mixer",
  "Compactor",
  "Compressor",
  "Conveyor",
  "Crane",
  "Dozer",
  "Drill Rig",
  "Excavator",
  "Farm Tractor",
  "Forklift",
  "Fuel/Lube Truck",
  "Generator",
  "Hydra-Platform",
  "Hydroseeder",
  "Light Tower",
  "Line Striper",
  "Loader",
  "Loader Backhoe",
  "Manlift",
  "Milling Machine",
  "Motor Grader",
  "Office Trailer",
  "Paver",
  "Pile/ Vibro Hammer/Leads",
  "Power Buggy",
  "Power Washer",
  "Pump - water or trash",
  "Pump - Concrete",
  "Rack Truck",
  "Saw - Concrete",
  "Saw - Rock Wheel",
  "Scissor Lift",
  "Skid Steer",
  "Shuttle Buggy - Asphalt",
  "Screen - Material",
  "Bidwell/Air Screed",
  "Drill - Tie-back Drill",
  "Storage Container",
  "Sweeper",
  "Trailer - Dump",
  "Trailer - Flat Bed",
  "Trailer - Tack",
  "Trailer - Utility",
  "Trench Box",
  "Trencher",
  "Truck - Boom",
  "Truck - Dump",
  "Truck - Off Road",
  "Truck - Vac",
  "Truck - Water",
  "Welder",
  "Winch"
]
material_categories = [
  "Aggregates",
  "Asphalts",
  "Blades, Bits, and Busters",
  "Blasting",
  "Catch Systems",
  "Concrete & Related Supplies",
  "Conduit & Appurtenances",
  "Disposal",
  "Electrical & Related Supplies",
  "Environmental Supplies",
  "Erosion Control",
  "Fencing",
  "Formwork & Related Supplies",
  "Geotextile/Liners & Related Supplies",
  "Guard Rail & Related Supplies",
  "HVAC / Plumbing & Related Supplies",
  "Landscaping & Related Supplies",
  "Lifting/Rigging",
  "Lumber  & Related Supplies",
  "Masonry & Related Supplies",
  "Metal Structures (other than Steel)",
  "Netting & Related Supplies",
  "Office Furniture",
  "Paint/Coating & Related Supplies",
  "Piling/Shoring & Related Supplies",
  "Pipe / Connectors",
  "Plastic Sheeting",
  "Precast Items & Related Supplies",
  "Reinforcing Bars & Related Supplies",
  "Retaining Walls & Related Supplies",
  "Safety Supplies",
  "Sinage",
  "Small Tools",
  "Stair Towers",
  "Steel / Beams",
  "Structural Steel and Related Supplies",
  "Survey Supplies",
  "Traffic Control",
  "Tarp / Blankets",
  "Vibration/Settlement Monitoring Instrumentations",
  "Welding Supplies"
]

def load_categories_into(file, categories, type)
  increment = 1

  categories.each do |c|
    attrs = { 'name' => c, 'category_type' => type }
    output = { "#{type}_category_#{increment}" => attrs }

    file << output.to_yaml.gsub(/^---\n/,'') + "\n"

    increment += 1
  end
end

fixture = File.expand_path('../categories.yml', __FILE__)
File.delete(fixture) if File.exist?(fixture)
File.open(fixture, 'w') do |f|
  load_categories_into(f, equipment_categories, 'equipment')
  load_categories_into(f, material_categories, 'material')
end
