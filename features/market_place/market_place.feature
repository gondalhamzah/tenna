@javascript
Feature: Market place
  In order to verify Market place for different users
  As a user
  I want to verify that only for sale and for rent assets are there in market place

  Scenario: Verify market place
    Given Login as "Public User" without clear
    And goto market place for "Public users"
    And create 2 assets for "Public users"
    Then there should be 0 asset
    Given goto assets page
    Then there should be 2 asset
    Then logout from "Public User"
    Given goto market place without login
    Then there should be 2 asset
    Given Login as "Company Admin" without clear
    And create 4 assets for "Company Admin"
    And goto market place for "Company Admin"
    Then there should be 4 asset
    Given goto to assets page mark asset mark owned asset as public for "Company Admin"
    And mark rental asset as public then there should be error for "Company Admin"
    And goto market place for "Company Admin"
    Then there should be 5 asset
    Then logout from "Company Admin"
    Given goto market place without login
    Then there should be 5 asset
    Given Login as "Public User" without clear
    And create 2 assets for "Public users"
    And goto market place for "Public users"
    Then there should be 7 asset
    Then logout from "Public User"
    Given Login as "Company User" without clear
    And create 4 assets for "Company User"
    And goto market place for "Company User"
    Then there should be 9 asset
    Given goto to assets page there is no checkbox for user
    And goto market place for "Company User"
    Then there should be 9 asset
    Then logout from "Company User"
    Given goto market place without login
    Then there should be 9 asset
    Given Login as "Public User" without clear
    And goto market place for "Public users"
    Then there should be 9 asset
    Then logout from "Public User"
    Given Login as "Company Admin" without clear
    And goto market place for "Company Admin"
    Then there should be 9 asset
    Then logout from "Company Admin"
    Given Login as "Project Leader" without clear
    And create 4 assets for "Project Leader"
    And goto market place for "Project Leader"
    Then there should be 11 asset
    Then logout from "Project Leader"
    Given goto market place without login
    Then there should be 11 asset
    Given Login as "Public User" without clear
    And goto market place for "Public users"
    Then there should be 11 asset
    Then logout from "Public User"
    And sleep for "3" second
    Given Login as "Company Admin" without clear
    And goto market place for "Company Admin"
    Then there should be 11 asset
    Then logout from "Company Admin"
    Given Login as "Project Member" without clear
    And create 4 assets for "Project Member"
    And goto market place for "Project Member"
    Then there should be 13 asset
    And goto market place for "Project Member"
    Then there should be 13 asset
    Then logout from "Project Member"
    Given goto market place without login
    Then there should be 13 asset
    Given Login as "Public User" without clear
    And goto market place for "Public users"
    Then there should be 13 asset
    Then logout from "Public User"
    Given Login as "Company Admin" without clear
    And goto market place for "Company Admin"
    Then there should be 13 asset
    Then logout from "Company Admin"
    Given Login as "Company User" without clear
    And goto market place for "Company User"
    Then there should be 13 asset
    Then logout from "Company User"
    Given Login as "Field" without clear
    And create 4 assets for "Field"
    And goto market place for "Field"
    # remaining
    # Then there should be 14 asset
    # Then logout from "Field"
    # Given goto market place without login
    # Then there should be 14 asset
    # Given Login as "Public User"
    # And goto market place for "Public users"
    # Then there should be 14 asset
    # Then logout from "Public User"
    # Given Login as "Company Admin"
    # And goto market place for "Company Admin"
    # Then there should be 14 asset
    # Then logout from "Company Admin"
    # Given Login as "Company User"
    # And goto market place for "Company User"
    # Then there should be 14 asset
    # Then logout from "Company User"
