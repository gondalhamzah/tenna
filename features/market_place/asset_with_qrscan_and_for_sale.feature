@javascript
Feature: Market place 
  In order to verify Assets that are for-sale and have QR codes should be included on the Marketplace for Company Admin
  As a Company Admin
  I want to verify that Assets are there in market place for Company Admin

  Scenario: Verify Assets that are for sale and have QR codes should be included on the Marketplace
    Given reset asset index
    Given Login as "Company Admin"
    When create 4 assets for "Company Admin"
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    And goto market place for "Company Admin"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "for-rent" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 1
    And goto market place for "Company Admin"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "for-sale" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 2
    And goto market place for "Company Admin"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "owned" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 3
    And goto market place for "Company Admin"
    Then there should be 2 asset
    And goto assets page
    Given Mark owned asset as public for "Company Admin"
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 3
    And goto market place for "Company Admin"
    Then there should be 3 asset
    And goto assets page
    Given Add QR-Code for "rental" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 4
    And goto market place for "Company Admin"
    Then there should be 3 asset

  Scenario: Verify Assets that are for sale and have QR codes should be included on the Marketplace
    Given Login as "Project Leader"
    And goto market place for "Project Leader"
    Then there should be 0 asset
    When create 4 assets for "Project Leader"
    And goto assets page
    Then there should be 4 asset
    And goto market place for "Project Leader"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "for-rent" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 1
    And goto market place for "Project Leader"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "for-sale" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 2
    And goto market place for "Project Leader"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "owned" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 3
    And goto market place for "Project Leader"
    Then there should be 2 asset
    And goto assets page
    Given Mark owned asset as public for "Project Leader"
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 3
    And goto market place for "Project Leader"
    Then there should be 3 asset
    And goto assets page
    Given Add QR-Code for "rental" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 4
    And goto market place for "Project Leader"
    Then there should be 3 asset

  Scenario: Verify Assets that are for sale and have QR codes should be included on the Marketplace
    Given Login as "Project Member"
    And goto market place for "Project Member"
    Then there should be 0 asset
    When create 4 assets for "Project Member"
    And goto assets page
    Then there should be 4 asset
    And goto market place for "Project Member"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "for-rent" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 1
    And goto market place for "Project Member"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "for-sale" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 2
    And goto market place for "Project Member"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "owned" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 3
    And goto market place for "Project Member"
    Then there should be 2 asset
    And goto assets page
    Given Mark owned asset as public for "Project Member"
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 3
    And goto market place for "Project Member"
    Then there should be 3 asset
    And goto assets page
    Given Add QR-Code for "rental" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 4
    And goto market place for "Project Member"
    Then there should be 3 asset

  Scenario: Verify Assets that are for sale and have QR codes should be included on the Marketplace
    Given Login as "Field"
    And goto market place for "Field"
    Then there should be 0 asset
    When create 4 assets for "Field"
    And goto assets page
    Then there should be 4 asset
    And goto market place for "Field"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "for-rent" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 1
    And goto market place for "Field"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "for-sale" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 2
    And goto market place for "Field"
    Then there should be 2 asset
    And goto assets page
    Given Add QR-Code for "owned" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 3
    And goto market place for "Field"
    Then there should be 2 asset
    And goto assets page
    Given Mark owned asset as public for "Field"
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 3
    And goto market place for "Field"
    Then there should be 3 asset
    And goto assets page
    Given Add QR-Code for "rental" asset
    And sleep for "2" second
    And goto assets page
    Then there should be 4 asset
    When Apply filter with QR-Code
    Then assets count should be 4
    And goto market place for "Field"
    Then there should be 3 asset
