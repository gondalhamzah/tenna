@javascript
Feature: Marketplace
  In order to ensure
  that Asset details can
  be viewed in the Marketplace

  Scenario: Verify that Company Admin can view details
    Given javascript driver is changed
    Given Login as "Company Admin"
    Given user is viewing Public Marketplace
    Given all types of assets are created as "Admin"
    Then asset details can be viewed for each asset
    Then switch to default driver

  Scenario: Verify that Project Leader can view details
    Given javascript driver is changed
    Given Login as "Project Leader"
    Given user is viewing Public Marketplace
    Given all types of assets are created as "Leader"
    Then asset details can be viewed for each asset
    Then switch to default driver

  Scenario: Verify that Project Member can view details
    Given javascript driver is changed
    Given Login as "Project Member"
    Given user is viewing Public Marketplace
    Given all types of assets are created as "Member"
    Then asset details can be viewed for each asset
    Then switch to default driver

  Scenario: Verify that Field Member can view details
    Given javascript driver is changed
    Given Login as "Field"
    Given user is viewing Public Marketplace
    Given all types of assets are created as "Field"
    Then asset details can be viewed for each asset
    Then switch to default driver

  Scenario: Verify that Company User can view details
    Given javascript driver is changed
    Given Login as "Company User"
    Given user is viewing Public Marketplace
    Given all types of assets are created as "User"
    Then asset details can be viewed for each asset
    Then switch to default driver
