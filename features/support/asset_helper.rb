def login_as_company_user
  Chewy.strategy(:bypass)
  @category_group = FactoryGirl.create(:category_group)
  @category1 = FactoryGirl.create(:category, :category_type => 'equipment', category_group_id: @category_group.id)
  @category2 = FactoryGirl.create(:category, :category_type => 'material', category_group_id: @category_group.id)

  @company_user = FactoryGirl.create(:company_user, company: FactoryGirl.create(:company))
  @project = FactoryGirl.create(:project, company: @company_user.company, contact: @company_user)
  @project2 = FactoryGirl.create(:project, company: @company_user.company, contact: @company_user)
  @company_user.company.project_id = @project.id
  @company_user.company.save
  @group = FactoryGirl.create(:group, company: @company_user.company, project: @project)
  sleep 1
  visit new_user_session_path
  login @company_user
end

def login_as_company_user_as_sites(sites)
  Chewy.strategy(:bypass)
  @category_group = FactoryGirl.create(:category_group)
  @category1 = FactoryGirl.create(:category, :category_type => 'equipment', category_group_id: @category_group.id)
  @category2 = FactoryGirl.create(:category, :category_type => 'material', category_group_id: @category_group.id)
  @company_admin = FactoryGirl.create(:company_admin, company: FactoryGirl.create(:company))
  if sites == 'with site'
    @project = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin, duration: 'some duration here')
    @company_admin.company.project_id = @project.id
    @company_admin.company.save
    @group = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  end
  visit new_user_session_path
  login @company_admin
end

def login_as_field_member
  Chewy.strategy(:bypass)
  @category_group = FactoryGirl.create(:category_group)
  @category1 = FactoryGirl.create(:category, :category_type => 'equipment', category_group_id: @category_group.id)
  @category2 = FactoryGirl.create(:category, :category_type => 'material', category_group_id: @category_group.id)

  @field_member = FactoryGirl.create(:field_member, company: FactoryGirl.create(:company))
  @project = FactoryGirl.create(:project, company: @field_member.company, contact: @field_member)
  @project2 = FactoryGirl.create(:project, company: @field_member.company, contact: @field_member)
  @field_member.company.project_id = @project.id
  @field_member.company.save
  visit new_user_session_path
  login @field_member
end

def login_as_leader
  Chewy.strategy(:bypass)
  @category_group = FactoryGirl.create(:category_group)
  @category1 = FactoryGirl.create(:category, :category_type => 'equipment', category_group_id: @category_group.id)
  @category2 = FactoryGirl.create(:category, :category_type => 'material', category_group_id: @category_group.id)

  @company_admin = FactoryGirl.create(:company_admin)
  @company_user = FactoryGirl.create(:company_user, company: @company_admin.company)

  @project = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  @project2 = FactoryGirl.create(:project, company: @company_user.company, contact: @company_user)

  @project3 = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)

  @company_admin.company.project_id = @project.id
  @company_admin.company.save
  @leader = FactoryGirl.create(:project_assignment_admin, user: @company_user, project: @project)
  @leader2 = FactoryGirl.create(:project_assignment_admin, user: @company_user, project: @project3)
  visit new_user_session_path
  login @company_user
end

def assign_project_role_leader user
  @project2 = FactoryGirl.create(:project, company: user.company, contact: user)
  @leader = FactoryGirl.create(:project_assignment_admin, user: user, project: @project2)
end

def login_as_member
  Chewy.strategy(:bypass)
  @category_group = FactoryGirl.create(:category_group)
  @category1 = FactoryGirl.create(:category, :category_type => 'equipment', category_group_id: @category_group.id)
  @category2 = FactoryGirl.create(:category, :category_type => 'material', category_group_id: @category_group.id)

  @company_admin = FactoryGirl.create(:company_admin)
  @company_user = FactoryGirl.create(:company_user, company: @company_admin.company)
  @project = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  @project2 = FactoryGirl.create(:project, company: @company_user.company, contact: @company_user)

  @project3 = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  @company_admin.company.project_id = @project.id
  @company_admin.company.save
  @leader = FactoryGirl.create(:project_assignment, user: @company_user, project: @project)
  @leader2 = FactoryGirl.create(:project_assignment, user: @company_user, project: @project3)
  visit new_user_session_path
  login @company_user
end

def login_as_company_admin
  Chewy.strategy(:bypass)
  @category_group = FactoryGirl.create(:category_group)
  @category1 = FactoryGirl.create(:category, :category_type => 'equipment', category_group_id: @category_group.id)
  @category2 = FactoryGirl.create(:category, :category_type => 'material', category_group_id: @category_group.id)
  @company_admin = FactoryGirl.create(:company_admin)
  @company_user = FactoryGirl.create(:company_user, company: @company_admin.company)
  @project = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  @project2 = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  @company_admin.company.project_id = @project.id
  @company_admin.company.save
  visit new_user_session_path
  login @company_admin
  sleep 1
  visit new_group_path
  fill_in 'Name', with: 'Group 1'
  find(:xpath, '//*[@id="new_group"]/div[1]/div[1]/div[4]/div[1]/span/span[1]/span').click
  all(:css, "#select2-group_project_id-results li")[1].click
  click_on 'Create Group'
  sleep 2
  @group = Group.last
end

def create_for_sale_asset(arg1)
  find(:xpath, '//*[@id="new_asset"]/div[1]/div[1]/div[2]/div/span[2]/label').click
  fill_in 'Title', with: arg1
  fill_in 'Location Zip / Postal Code', with: '66002'

  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: @project.name).click

  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: @category1.name).click

  select 'United States', from: 'asset_country'

  fill_in 'Price', with: '2100'
  fill_in 'asset_hours', with: '9'
  fill_in 'asset_miles', with: '9'
  fill_in 'asset_quantity', with: '100'
  sleep 2
  begin
    find(:xpath, '//*[@id="new_asset"]/div[2]/div/div/button').click
  rescue
    page.execute_script("$('#new_asset > div.form-actions > div > div > button').trigger('click')")
  end
end

def create_for_sale_asset_with_canada(arg1)
  find(:xpath, '//*[@id="new_asset"]/div[1]/div[1]/div[2]/div/span[2]/label').click
  fill_in 'Title', with: arg1

  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: @project.name).click

  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: @category1.name).click

  select 'Canada', :from => 'asset_country'
  fill_in 'Location Zip / Postal Code', with: 'M4B 1B3'
  fill_in 'Price', with: '2100'
  fill_in 'asset_quantity', with: '100'

  sleep 4
  begin
    find(:xpath, '//*[@id="new_asset"]/div[2]/div/div/button').click
  rescue
    page.execute_script("$('#new_asset > div.form-actions > div > div > button').trigger('click')")
  end
end

def fill_assets_csv_with(arg1, arg2, arg3, arg4)
  # if ENV["BROWSER_MODE"] == "true"
    # sdm
    # csv structure is changed which is giving error
    # CSV.open("tmp/MT/MT.csv", "wb") do |csv|
      # csv << ["Owned", "Rental", "For Sale", "For Rent", "Type of Asset", "*Category", "*Title", "Description", "Make", "Model", "Year", "Hours/Miles", "Condition", "*Market value", "*Price/Cost", "Quantity", "*Time/ Units", "Asset number", "*Zip code", "Notes", "Durability", "*In use until"]
      # csv << ["FALSE", "TRUE", "FALSE", "FALSE", "Equipment", "Loader", arg2, "set of forks included, engine recently overhauled",  "CAT",  "450", "2010", "8888",  "Good", "10000", "10000", "1", "day", "", "12345", "", "",  "1/1/2018"]
      # csv << ["TRUE", "FALSE", "FALSE", "FALSE", "Equipment", "Loader", arg1, "set of forks included, engine recently overhauled",  "CAT",  "450", "2010", "8888",  "Good", "10000", "10000", "1", "day", "", "12345", "", "",  "1/1/2018"]
      # csv << ["TRUE", "FALSE", "FALSE", "TRUE", "Equipment", "Loader", arg3, "set of forks included, engine recently overhauled", "CAT",  "450", "2010", "8888",  "Good", "10000", "10000", "1", "day", "", "12345", "", "",  "1/1/2018"]
      # csv << ["TRUE", "FALSE", "TRUE", "FALSE", "Equipment", "Loader", arg4, "set of forks included, engine recently overhauled", "CAT",  "450", "2010", "8888",  "Good", "10000", "10000", "1", "day", "", "12345", "", "",  "1/1/2018"]
    # end
    # sleep 2
    # find(:xpath, '//*[@id="spreadsheet-links"]/a[3]').click
    # find(".modal-trigger.project-detail-icons").click
    # sleep 1
    # attach_file 'csv_import_file', File.join(Rails.root, 'tmp', 'MT', 'MT.csv'), :visible => false
    # click_button "Import"
    # sleep 8
  # end
end

def create_for_rent_asset(arg1)
  find(:xpath, '//*[@id="new_asset"]/div[1]/div[1]/div[2]/div/span[2]/label').click
  fill_in 'Title', with: arg1
  find(:xpath, '//*[@id="new_asset"]/div[1]/div[1]/div[3]/div/span[2]/label').click

  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: @project.name).click

  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: @category1.name).click

  fill_in 'Location Zip / Postal Code', with: '66002'
  select 'United States', :from => 'asset_country'
  find(:xpath, '//*[@id="asset_price"]').set '21000'
  sleep 6
  begin
    find(:xpath, '//*[@id="new_asset"]/div[2]/div/div/button').click
  rescue
    page.execute_script("$('#new_asset > div.form-actions > div > div > button').trigger('click')")
  end
end

def create_owned_asset(arg1)
  fill_in 'Title', with: arg1
  fill_in("asset_location_zip", with: "66002")

  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: @project.name).click

  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: @category1.name).click

  select 'United States', from: 'asset_country'
  fill_in 'Price', with: '2100'
  fill_in 'asset_quantity', with: '100'
  sleep 5
  begin
    find(:xpath, '//*[@id="new_asset"]/div[2]/div/div/button').click
  rescue
    page.execute_script("$('#new_asset > div.form-actions > div > div > button').trigger('click')")
  end
end

def create_rental_asset(arg1)
  find(:xpath, '//*[@id="new_asset"]/div[1]/div[1]/div[1]/div/span[2]/label').click
  fill_in 'Title', with: arg1
  fill_in 'Location Zip / Postal Code', with: '66002'

  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: @project.name).click

  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: @category1.name).click

  select 'United States', :from => 'asset_country'
  fill_in '*Total Rental Cost', with: '2000'
  fill_in 'Rental Cost', with: '2001'
  sleep 6
  begin
      find(:xpath, '//*[@id="new_asset"]/div[2]/div/div/button').click
  rescue
    page.execute_script("$('#new_asset > div.form-actions > div > div > button').trigger('click')")
  end
end

def for_sale?
  find(:xpath, '//*[@id="asset_1"]/td[3]/a').click
  expect(page).to have_content("For Sale")
end

def owned?
  if ENV['BROWSER_MODE'] == 'true'
    find(:xpath, '//*[@id="asset_1"]/td[3]/a').click
    expect(page.find(:xpath, '//*[@id="assets-show"]/main/div[2]/div[2]/div[2]/div[1]/p')).to have_content('Owned')
  end
end

def rental?
  if ENV['BROWSER_MODE'] == 'true'
    find(:xpath, '//*[@id="asset_1"]/td[3]/a').click
    expect(page.find(:xpath, '//*[@id="assets-show"]/main/div[2]/div[2]/div[2]/div[1]/p')).to have_content('Rental')
  end
end

def for_rent?
  if ENV['BROWSER_MODE'] == 'true'
    find(:xpath, '//*[@id="asset_1"]/td[3]/a').click
    expect(page).to have_content('For Rent')
  end
end

def create_rental_asset_for(company_admin)
  # Chewy.strategy(:bypass)
  @rental_asset1 = FactoryGirl.create(:asset, company: company_admin.company, creator: company_admin, project: @project)
  @rental_asset1.creator = company_admin
  @rental_asset1.project = @project
  @rental_asset1.rental = true
  @rental_asset1.public = false
  @rental_asset1.owned = false
  @rental_asset1.type = 'Material'
  @rental_asset1.save
  AssetsIndex.import(asset: @rental_asset1)
  AssetsVersionsIndex.import(asset: @rental_asset1)
end

def create_owned_asset_for(company_admin)
  # Chewy.strategy(:bypass)
  @owned_asset1 = FactoryGirl.create(:asset, company: company_admin.company, creator: company_admin, project: @project)
  @owned_asset1.creator = company_admin
  @owned_asset1.project = @project
  @owned_asset1.rental = false
  @owned_asset1.public = false
  @owned_asset1.owned = true
  @owned_asset1.type = 'Material'
  @owned_asset1.save
  AssetsIndex.import(asset: @owned_asset1)
  AssetsVersionsIndex.import(asset: @owned_asset1)
end

def create_for_sale_asset_for(company_admin)
  # Chewy.strategy(:bypass)
  @for_sale1 = FactoryGirl.create(:asset, company: company_admin.company, creator: company_admin, project: @project)
  @for_sale1.creator = company_admin
  @for_sale1.project = @project
  @for_sale1.rental = false
  @for_sale1.public = true
  @for_sale1.owned = true
  @for_sale1.type = 'Material'
  @for_sale1.save
  AssetsIndex.import(asset: @for_sale1)
  AssetsVersionsIndex.import(asset: @for_sale1)
end

def create_for_rent_asset_for(company_admin)
  # Chewy.strategy(:bypass)
  @for_rent1 = FactoryGirl.create(:asset, company: company_admin.company, creator: company_admin, project: @project)
  @for_rent1.creator = company_admin
  @for_rent1.project = @project
  @for_rent1.rental = true
  @for_rent1.public = true
  @for_rent1.owned = true
  @for_rent1.type = 'Material'
  @for_rent1.save
  AssetsIndex.import(asset: @for_rent1)
  AssetsVersionsIndex.import(asset: @for_rent1)
end

def create_rental_asset_for_public_user (company_admin)
  # Chewy.strategy(:bypass)
  @rental_asset1 = FactoryGirl.create(:asset, creator: company_admin)
  @rental_asset1.rental = true
  @rental_asset1.public = false
  @rental_asset1.owned = false
  @rental_asset1.type = 'Material'
  @rental_asset1.save
  AssetsIndex.import(asset: @rental_asset1)
  AssetsVersionsIndex.import(asset: @rental_asset1)
end

def create_owned_asset_for_public_user (company_admin)
  # Chewy.strategy(:bypass)
  @owned_asset1 = FactoryGirl.create(:asset, creator: company_admin)
  @owned_asset1.rental = false
  @owned_asset1.public = false
  @owned_asset1.owned = true
  @owned_asset1.type = 'Material'
  @owned_asset1.save
  AssetsIndex.import(asset: @owned_asset1)
  AssetsVersionsIndex.import(asset: @owned_asset1)
end

def create_for_sale_asset_for_public_user(company_admin)
  # Chewy.strategy(:bypass)
  @for_sale1 = FactoryGirl.create(:asset, creator: company_admin)
  @for_sale1.rental = false
  @for_sale1.public = true
  @for_sale1.owned = true
  @for_sale1.type = 'Material'
  @for_sale1.save
  AssetsIndex.import(asset: @for_sale1)
  AssetsVersionsIndex.import(asset: @for_sale1)
end

def create_for_rent_asset_for_public_user(company_admin)
  # Chewy.strategy(:bypass)
  @category_group = FactoryGirl.create(:category_group)
  @for_rent1 = FactoryGirl.create(:asset, creator: company_admin)
  @for_rent1.rental = true
  @for_rent1.public = true
  @for_rent1.owned = true
  @for_rent1.type = 'Material'
  @for_rent1.save
  AssetsIndex.import(asset: @for_rent1)
  AssetsVersionsIndex.import(asset: @for_rent1)
end

def create_four_assets(company_admin)
  create_rental_asset_for company_admin
  create_owned_asset_for company_admin
  create_for_sale_asset_for company_admin
  create_for_rent_asset_for company_admin
end

def reset_assets_filter
  find(:xpath, '//*[@id="new_filter_search"]/div[8]/button').click
  sleep 1
end

def link_all_assets_to_group
  group = Group.last
  @rental_asset1.group = group
  @rental_asset1.save
  @owned_asset1.group = group
  @owned_asset1.save
  @for_rent1.group = group
  @for_rent1.save
  @for_sale1.group = group
  @for_sale1.save
  AssetsIndex.import(asset: @owned_asset1)
  AssetsVersionsIndex.import(asset: @owned_asset1)
  AssetsIndex.import(asset: @for_sale1)
  AssetsVersionsIndex.import(asset: @for_sale1)
  AssetsIndex.import(asset: @for_rent1)
  AssetsVersionsIndex.import(asset: @for_rent1)
  AssetsIndex.import(asset: @rental_asset1)
  AssetsVersionsIndex.import(asset: @rental_asset1)
  GroupsIndex.import(group: group)
end

def link_all_assets_group group
  @rental_asset1.group = group
  @rental_asset1.save
  @owned_asset1.group = group
  @owned_asset1.save
  @for_rent1.group = group
  @for_rent1.save
  @for_sale1.group = group
  @for_sale1.save
    AssetsIndex.import(asset: @owned_asset1)
    AssetsVersionsIndex.import(asset: @owned_asset1)
    AssetsIndex.import(asset: @for_sale1)
    AssetsVersionsIndex.import(asset: @for_sale1)
    AssetsIndex.import(asset: @for_rent1)
    AssetsVersionsIndex.import(asset: @for_rent1)
    AssetsIndex.import(asset: @rental_asset1)
    AssetsVersionsIndex.import(asset: @rental_asset1)
    GroupsIndex.import(group: group)
end

def create_a_group
  Chewy.strategy(:bypass)
  visit new_group_path
  fill_in 'Name', with: 'Group 1'
  find(:xpath, '//*[@id="new_group"]/div[1]/div[1]/div[4]/div[1]/span/span[1]/span').click
  all(:css, "#select2-group_project_id-results li")[1].click
  click_on 'Create Group'
  sleep 2
end

def create_a_group_for member
  Chewy.strategy(:bypass)
  visit new_group_path
  fill_in 'Name', with: 'Group 1'
  find(:xpath, '//*[@id="new_group"]/div[1]/div[1]/div[4]/div[1]/span/span[1]/span').click
  all(:css, "#select2-group_project_id-results li")[1].click
  click_on 'Create Group'
  sleep 2
end

def reset_asset_index
  AssetsIndex.reset!
end

def reset_project_index
  ProjectsIndex.reset!
end
