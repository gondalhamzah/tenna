def login(user)
  find(:xpath, "//*[@id='user_email']").set(user.email)
  fill_in 'user_password', :with => user.password
  click_button 'Sign In'
end

def group_detail_page
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @project.contact = @company_admin
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  GroupsIndex.import(group: @group_b)
  visit group_path id: @group_b.id
end

def groups_page
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @project.contact = @company_admin
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  GroupsIndex.import(group: @group_b)
  ProjectsIndex.import(project: @project)
  GroupsVersionsIndex.import(group: @group_b)
  visit groups_path
end

def link_asset_to_group(group)
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @project.contact = @company_admin
  asset = FactoryGirl.create(:asset, company: @company_admin.company)
  AssetsIndex.import(asset: asset)
  visit assets_path
end

def group_edit_page
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @project.contact = @company_admin
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  GroupsIndex.import(group: @group_b)
  visit edit_group_path id: @group_b.id
end

def asset_detail_page
  @category_group = FactoryGirl.create(:category_group)
  @public_asset = FactoryGirl.create(:asset, public: true, company: @company_admin.company)
  @public_asset.creator = @company_admin
  @public_asset.project = @project
  @public_asset.save
  AssetsIndex.import(asset: @public_asset)
  AssetsVersionsIndex.import(asset: @public_asset)
  visit asset_path id: @public_asset.id
end

def asset_list_page
  @category_group = FactoryGirl.create(:category_group)
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @project.contact = @company_admin
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  GroupsIndex.import(group: @group_b)
  @public_asset = FactoryGirl.create(:asset, public: true, company: @company_admin.company)
  @public_asset_b = FactoryGirl.create(:asset, public: true, company: @company_admin.company)
  AssetsIndex.import(asset: @public_asset)
  AssetsIndex.import(asset: @public_asset_b)
  AssetsVersionsIndex.import(asset: @public_asset)
  AssetsVersionsIndex.import(asset: @public_asset_b)
  visit assets_path
end

def project_detail_page
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @project.contact = @company_admin
  @group_a = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  GroupsIndex.import(group: @group_a)
  GroupsIndex.import(group: @group_b)
  GroupsVersionsIndex.import(group: @group_a)
  GroupsVersionsIndex.import(group: @group_b)
  ProjectsIndex.import(project: @project)
  visit project_path id: @project.id
end

def project_page
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @group_a = FactoryGirl.create(:group, company: @company_admin.company, name: "Groub A", project: @project)
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, name: "Groub B", project: @project)

  @public_asset = FactoryGirl.create(:asset, public: true, company: @company_admin.company)
  @public_asset_b = FactoryGirl.create(:asset, public: true, company: @company_admin.company)

  @public_asset.group = @group_a
  @public_asset_b.group = @group_b

  @public_asset.creator = @company_admin
  @public_asset_b.creator = @company_admin

  @public_asset.project = @project
  @public_asset_b.project = @project

  @public_asset.save
  @public_asset_b.save

  AssetsIndex.import(asset: @public_asset)
  AssetsIndex.import(asset: @public_asset_b)
  AssetsVersionsIndex.import(asset: @public_asset)
  AssetsVersionsIndex.import(asset: @public_asset_b)

  GroupsIndex.import(group: @group_a)
  GroupsIndex.import(group: @group_b)
  GroupsVersionsIndex.import(group: @group_a)
  GroupsVersionsIndex.import(group: @group_b)

  ProjectsIndex.import(project: @project)
  visit projects_path
end

def project_page_close_group
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @project.contact = @company_admin
  @group_a = FactoryGirl.create(:group, company: @company_admin.company, name: "Groub A", project: @project)
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, name: "Groub B", project: @project)

  @public_asset = FactoryGirl.create(:asset, public: true, company: @company_admin.company)
  @public_asset_b = FactoryGirl.create(:asset, public: true, company: @company_admin.company)

  @public_asset.group = @group_a
  @public_asset_b.group = @group_b

  @public_asset.creator = @company_admin
  @public_asset_b.creator = @company_admin

  @public_asset.project = @project
  @public_asset_b.project = @project
  @group_a.state = "closed"

  @group_a.save
  @public_asset.save
  @public_asset_b.save

  AssetsIndex.import(asset: @public_asset)
  AssetsIndex.import(asset: @public_asset_b)
  AssetsVersionsIndex.import(asset: @public_asset)
  AssetsVersionsIndex.import(asset: @public_asset_b)

  GroupsIndex.import(group: @group_a)
  GroupsIndex.import(group: @group_b)
  GroupsVersionsIndex.import(group: @group_a)
  GroupsVersionsIndex.import(group: @group_b)

  ProjectsIndex.import(project: @project)
  visit projects_path
end

def project_page_deleted_group
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @project.contact = @company_admin
  @group_a = FactoryGirl.create(:group, company: @company_admin.company, name: "Groub A", project: @project)
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, name: "Groub B", project: @project)

  @public_asset = FactoryGirl.create(:asset, public: true,  company: @company_admin.company)
  @public_asset_b = FactoryGirl.create(:asset, public: true, company: @company_admin.company)

  @public_asset.group = @group_a
  @public_asset_b.group = @group_b

  @public_asset.creator = @company_admin
  @public_asset_b.creator = @company_admin

  @public_asset.project = @project
  @public_asset_b.project = @project
  @group_a.deleted_at = Time.now

  @group_a.save
  @public_asset.save
  @public_asset_b.save

  AssetsIndex.import(asset: @public_asset)
  AssetsIndex.import(asset: @public_asset_b)
  AssetsVersionsIndex.import(asset: @public_asset)
  AssetsVersionsIndex.import(asset: @public_asset_b)

  GroupsIndex.import(group: @group_a)
  GroupsIndex.import(group: @group_b)
  GroupsVersionsIndex.import(group: @group_a)
  GroupsVersionsIndex.import(group: @group_b)

  ProjectsIndex.import(project: @project)
  visit projects_path
end

def admin_login(user)
  fill_in 'Email', with: user.email
  fill_in 'Password', with: user.password
  click_button 'Login'
end

def multiple_groups_with_different_project
  @project1 = FactoryGirl.create(:project, contact: @company_admin)
  @project2 = FactoryGirl.create(:project, contact: @company_admin)
  @project3 = FactoryGirl.create(:project, contact: @company_admin)
  @project4 = FactoryGirl.create(:project, contact: @company_admin)
  @groups = Array.new
  @groups[0] = FactoryGirl.create(:group, company: @company_admin.company, project: @project1)
  @groups[1] = FactoryGirl.create(:group, company: @company_admin.company, project: @project2)
  @groups[2] = FactoryGirl.create(:group, company: @company_admin.company, project: @project3)
  @groups[3] = FactoryGirl.create(:group, company: @company_admin.company, project: @project4)
  ProjectsIndex.import(project: @project1)
  ProjectsIndex.import(project: @project2)
  ProjectsIndex.import(project: @project3)
  ProjectsIndex.import(project: @project4)
  GroupsIndex.import(group: @groups)
  GroupsVersionsIndex.import(group: @groups)
end

def create_public_user
  user = FactoryGirl.create(:user)
end

def execute_rake(file, task)
  require 'rake'
  rake = Rake::Application.new
  Rake.application = rake
  Rake::Task.define_task(:environment)
  load "#{Rails.root}/lib/tasks/#{file}"
  rake[task].invoke
end

def create_subscription
  Chewy.strategy(:bypass)
  @company_admin = FactoryGirl.create(:company_admin, email: "pakistan@gmail.com")
  @subscription = Subscription.new(company: @company_admin.company, api_key: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTMwNmZjMmYxNzM5OWFmMGEzZjgyZDIiLCJpYXQiOjE0OTYzNDY1NjJ9.IDaX4fwp4Q8eGVy55KbNdhUGTT18xgYlj05OtdwY5lc", api_key_created_at: "Wed, 17 May 2017 11:26:38 UTC +00:00",
  api_key_updated_at: "Wed, 17 May 2017 11:26:38 UTC +00:00",
  category: "routing",
  number_of_units: 7,
  state: "active",
  note: "Sultan Routing Note here.",
  deleted_at: nil,
  vendor_id: "59247775c4a6fdad0a6d75bf",
  created_at: "Fri, 19 May 2017 14:19:57 UTC 00:00",
  updated_at: "Fri, 19 May 2017 14:19:57 UTC 00:00")
  @subscription.save!
  company = Company.last.address
  company.line_1 = "745 Atlantic Ave"
  company.city = "Boston"
  company.state = "MA"
  company.zip = "02111"
  company.save
  @category_group = FactoryGirl.create(:category_group)
  @category1 = FactoryGirl.create(:category, category_type: "equipment")
  @category2 = FactoryGirl.create(:category, category_type: "material")
  @category3 = FactoryGirl.create(:category)
  @project = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin, address: @address)
  @project2 = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  @asset = FactoryGirl.create(:asset, project: @project, creator: @company_admin, company: @company_admin.company)
  @asset2 = FactoryGirl.create(:asset, project: @project, creator: @company_admin, company: @company_admin.company)
  @asset3 = FactoryGirl.create(:asset, project: @project2, creator: @company_admin, company: @company_admin.company)
  @asset4 = FactoryGirl.create(:asset, project: @project2, creator: @company_admin, company: @company_admin.company)
  AssetsIndex.import(asset: @asset)
  AssetsIndex.import(asset: @asset2)
  AssetsIndex.import(asset: @asset3)
  AssetsIndex.import(asset: @asset4)
  AssetsVersionsIndex.import(asset: @asset)
  AssetsVersionsIndex.import(asset: @asset2)
  AssetsVersionsIndex.import(asset: @asset3)
  AssetsVersionsIndex.import(asset: @asset4)
  ProjectsIndex.import(project: @project)
  ProjectsIndex.import(project: @project2)
  @company_admin.company.project_id = @project.id
  Project.all.each do |p|
    p.address.lat = 40.9208056
    p.address.lng = -81.0202533
    p.save!
  end
  @company_admin.company.save
end

def pretty_address object
  a = object.address
  c_s = "#{a.line_1}, #{a.city}, #{a.state} #{a.zip}, USA"
end
