@javascript
Feature: Rental Asset 
  In order to show Action items on rental asset list
  As a user
  I want to verify Action items

  Scenario: Verify action items on rental asset list
    Given User is login
    And create rental and owned assets
    And goto assets page and goto action
    Then there should be "mark as public"
    And goto assets page
    And apply asset management filter "Rentals"
    And click on text "Apply Filters"
    And goto action
    Then there should be "no mark as public"
    And goto assets page
    And apply asset management filter "Rentals"
    And click on text "Apply Filters"
    And goto action
    Then there should be "mark as public"

  Scenario: Mass action on rental+owned assets
    Given User is login
    And create rental and owned assets
    And Select both assets and apply action mark as sold
    Then error occur "action cannot be performed"
    And Select both assets and apply action mark as public
    And select only owned asset and apply mark as sold
    Then it should successfully apply
    And goto project create page create a project with owned and rental asset
    And Goto project page and Select owned assets as well as rental assets
    And select mass action mark as sold
    And select mass action mark as public
    And unselect rental asset and apply mark as public
    Then successfull mark as public
    And unselect rental asset and apply mark as sold
    Then successfull mark as sold
    And goto group create page create a group with owned and rental asset
    And Goto group page and Select owned assets as well as rental assets
    And select mass action mark as sold
    And select mass action mark as public
    And unselect rental asset and apply mark as public
    Then successfull mark as public
    And unselect rental asset and apply mark as sold
    Then successfull mark as sold
