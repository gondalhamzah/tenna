@javascript
Feature: Delete asset on Assets Page
  In order to delete an asset
  As a company admin
  I want to verify that asset deleted successfully on assets page

  Scenario: delete asset on Assets Page
    Given javascript driver is changed
    Given "company_admin" is login
    And User on Assets Page 4 assets of all kinds( Rental, Owned, For-Sale, For-Rent)
    When select asset owned and apply action "Deleted" on assets page
    Then there should be 3 assets on assets page
    When select rental asset and apply action "Deleted" on assets page
    Then switch to default driver

  Scenario: delete asset on Assets Page
    Given javascript driver is changed
    Given field_member is login
    And User on Assets Page 4 assets of all kinds for field_member
    When select asset and apply action "Deleted" on assets page
    Then there should be no option for delete
    Then switch to default driver

  Scenario: delete asset on Assets Page as Leader
    Given javascript driver is changed
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given project leader is login
    And User on asset Detail Page 6 assets of all kinds for leader
    When select asset owned and apply action "Deleted" on assets page
    Then there should be 5 assets on assets page
    When select rental asset and apply action "Deleted" on assets page
    Then there should be 4 assets on assets page
    When select for-sale asset and apply action "Deleted" on assets page
    Then there should be 3 assets on assets page
    When select for-rent asset and apply action "Deleted" on assets page
    Then there should be 2 assets on assets page
    Then switch to default driver

  Scenario: delete asset on Assets Page as member
    Given javascript driver is changed
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given member is login
    And User on asset Detail Page 4 assets of all kinds for member
    When select "owned" and apply action Deleted on assets page
    Then there should be 3 assets on assets page
    When select "rental" and apply action Deleted on assets page
    Then there should be 2 assets on assets page
    When select "for-sale" and apply action Deleted on assets page
    Then there should be 1 assets on assets page
    When select "for-rent" and apply action Deleted on assets page
    Then there should be 0 assets on assets page
    Then switch to default driver
