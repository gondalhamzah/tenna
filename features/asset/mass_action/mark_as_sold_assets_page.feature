@javascript
Feature: Mark as Sold on Assets Page
  In order to mark asset as sold
  As a user
  I want to verify that asset mark as sold successfully

  Scenario: Mark asset as Sold on Assets Page
    Given javascript driver is changed
    Given "company_admin" is login
    And User on Assets Page 4 assets of all kinds( Rental, Owned, For-Sale, For-Rent)
    When select asset owned and apply action "Mark as sold"
    Then it should through successfully update owned asset to marked as sold
    When select rental asset and apply action "Mark as sold"
    # Then it should through error message
    And Apply filter with sold asset there should be only 1 asset
    When select for-sale asset and apply action "Mark as sold"
    Then it should through successfully marked as sold
    And Apply filter with sold asset there should be only 2 asset
    And goto assets page
    When select for-rent asset and apply action "Mark as sold"
    Then it should through successfully marked
    And Apply filter with sold asset there should be only 3 asset
    Then switch to default driver

  Scenario: Mark asset as Sold on Assets Page as a field member
    Given javascript driver is changed
    Given field_member is login
    And User on Assets Page 4 assets of all kinds for field_member
    When select asset and apply action "Marked as sold" on assets page
    Then there should be no option for Mark as Sold
    Then switch to default driver

  Scenario: Mark asset as Sold on Assets Page for leader
    Given javascript driver is changed
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given project leader is login
    And User on Assets Page 4 assets of all kinds( Rental, Owned, For-Sale, For-Rent)
    When select asset owned and apply action "Mark as sold"
    Then it should through successfully update owned asset to marked as sold
    When select rental asset and apply action "Mark as sold"
    # Then it should through error message
    And Apply filter with sold asset there should be only 1 asset
    When select for-sale asset and apply action "Mark as sold"
    Then it should through successfully marked as sold
    And Apply filter with sold asset there should be only 2 asset
    And goto assets page
    When select for-rent asset and apply action "Mark as sold"
    Then it should through successfully marked
    And Apply filter with sold asset there should be only 3 asset
    Then switch to default driver
