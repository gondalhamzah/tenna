@javascript
Feature: Reallocate project of asset on Groups Page
  In order to reallocate asset
  As a user
  I want to verify that asset group change successfully

  Scenario: Reallocation on Group Page
    Given "company_admin" is login
    And User on Group detail Page 4 assets of all kinds( Rental, Owned, For-Sale, For-Rent)
    When select asset "owned" and apply action "reallocation" on group detail page
    And change project of asset
    Then it should successfully update project of "owned" asset on group page
    When select asset "rental" and apply action "reallocation" on group detail page
    And change project of asset
    Then it should successfully update project of "rental" asset on group page
    When select asset "for-sale" and apply action "reallocation" on group detail page
    And change project of asset
    Then it should successfully update project of "for-sale" asset on group page
    Given scroll to page end
    When select asset "for-rent" and apply action "reallocation" on group detail page
    And change project of asset
    Then it should successfully update project of "for-rent" asset on group page

  Scenario: Reallocation on Group Page for fied_member
    Given field_member is login
    And User on Group Detail Page 4 assets of all kinds for field_member
    When select asset "owned" and apply action "reallocation" on group detail page for field_member
    And change project of asset
    Then it should successfully update project of "owned" asset on group page
    When select asset "rental" and apply action "reallocation" on group detail page for field_member
    And change project of asset
    Then it should successfully update project of "rental" asset on group page
    When select asset "for-sale" and apply action "reallocation" on group detail page for field_member
    And change project of asset
    Then it should successfully update project of "for-sale" asset on group page
    Given scroll to page end
    And sleep for "1" second
    When select asset "for-rent" and apply action "reallocation" on group detail page for field_member
    And change project of asset
    Then it should successfully update project of "for-rent" asset on group page

  Scenario: Reallocation on Group Page for leader
    # FIX IS IN 2.9
    # Given "company_admin" is login
    # And Create a group
    # Then logout from "Company Admin"
    # Given project leader is login
    # And User on Group Detail Page assets of all kinds for leader
    # When select asset "owned" and apply action "reallocation" on group detail page for leader
    # And change project of asset
    # Then it should successfully update project of "owned" asset on group page
    # When select asset "rental" and apply action "reallocation" on group detail page for leader
    # And change project of asset
    # Then it should successfully update project of "rental" asset on group page
    # When select asset "for-sale" and apply action "reallocation" on group detail page for leader
    # And change project of asset
    # Then it should successfully update project of "for-sale" asset on group page
    # When select asset "for-rent" and apply action "reallocation" on group detail page for leader
    # And change project of asset
    # Then it should successfully update project of "for-rent" asset on group page
