@javascript
Feature: Mark as Sold on Groups Page
  In order to mark asset as sold
  As a user
  I want to verify that asset mark as sold successfully

  Scenario: Mark asset as Sold on Groups Page
    Given javascript driver is changed
    Given "company_admin" is login
    And User on Group detail Page 4 assets of all kinds( Rental, Owned, For-Sale, For-Rent)
    When select asset "owned" and apply action "Mark as sold" on group page
    Then it should successfully update asset to marked as sold on groups page
    And there should be 3 assets now on group page
    When select asset "rental" and apply action "Mark as sold" on group page
    Then it should throw error rental asset cannot be sold on groups page
    And there should be 3 assets now on group page
    When select asset "for-rent" and apply action "Mark as sold" on group page
    Then it should successfully update asset to marked as sold on groups page
    And there should be 2 assets now on group page
    When select asset "for-sale" and apply action "Mark as sold" on group page
    Then it should successfully update asset to marked as sold on groups page
    And there should be 1 assets now on group page
    Then switch to default driver

  Scenario: Mark asset as Sold on Groups Page as field member
    Given javascript driver is changed
    Given field_member is login
    And User on Group Detail Page 4 assets of all kinds for field_member
    When select asset and apply action "Mark as Sold" on group page
    Then there should be no option for mark as sold
    Then switch to default driver

  Scenario: Mark asset as Sold on Groups Page as a leader
    Given javascript driver is changed
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given project leader is login
    And User on asset Detail Page 4 assets of all kinds for leader
    When select asset "owned" and apply action "Mark as sold" on group page
    Then it should successfully update asset to marked as sold on groups page
    And there should be 3 assets now on group page
    When select asset "rental" and apply action "Mark as sold" on group page
    Then it should throw error rental asset cannot be sold on groups page
    And there should be 3 assets now on group page
    When select asset "for-rent" and apply action "Mark as sold" on group page
    Then it should successfully update asset to marked as sold on groups page
    And there should be 2 assets now on group page
    When select asset "for-sale" and apply action "Mark as sold" on group page
    Then it should successfully update asset to marked as sold on groups page
    And there should be 1 assets now on group page
    Then switch to default driver
