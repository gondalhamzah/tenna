@javascript
Feature: Link asset to group on Group Page
  In order to link asset to a group
  As a user
  I want to verify that asset link successfully

  Scenario: Link ass to group on Groups Page
    Given "company_admin" is login
    And User on Group Detail Page 4 assets of all kinds( Rental, Owned, For-Sale, For-Rent)
    When select asset "owned" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "owned"asset on group page
    When select asset "rental" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "rental"asset on group page
    When select asset "for-sale" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "for-sale"asset on group page
    When select asset "for-rent" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "for-rent"asset on group page

  Scenario: Link ass to group on Groups Page as field member
    Given field_member is login
    And User on Group Detail Page 4 assets of all kinds for field_member
    When select asset "owned" and apply action link to group on group page for field_member
    And link asset to group
    Then it should successfully update group of "owned"asset on group page
    When select asset "rental" and apply action link to group on group page for field_member
    And link asset to group
    Then it should successfully update group of "rental"asset on group page
    When select asset "for-sale" and apply action link to group on group page for field_member
    And link asset to group
    Then it should successfully update group of "for-sale"asset on group page
    When select asset "for-rent" and apply action link to group on group page for field_member
    And link asset to group
    Then it should successfully update group of "for-rent"asset on group page

  Scenario: Link assset to group on Groups Page as leader
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given project leader is login
    And User on Group Detail Page 4 assets of all kinds for leader
    When select asset "owned" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "owned"asset on group page
    When select asset "rental" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "rental"asset on group page
    When select asset "for-sale" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "for-sale"asset on group page
    When select asset "for-rent" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "for-rent"asset on group page

  Scenario: Link assset to group on Groups Page as member
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given member is login
    And User on asset Detail Page 4 assets of all kinds for member
    When select asset "owned" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "owned"asset on group page
    When select asset "rental" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "rental"asset on group page
    When select asset "for-sale" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "for-sale"asset on group page
    When select asset "for-rent" and apply action link to group on group page
    And link asset to group
    Then it should successfully update group of "for-rent"asset on group page
