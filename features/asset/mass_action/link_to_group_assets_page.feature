@javascript
Feature: Link asset to group on Assets Page
  In order to link asset to a group
  As a user
  I want to verify that asset link successfully

  Scenario: link asset to group on Assets Page
    Given "company_admin" is login
    And User on Assets Page 4 assets of all kinds( Rental, Owned, For-Sale, For-Rent)
    When select asset "owned" and apply action link to group
    And link asset to group
    Then it should successfully update group of "owned"asset
    When select asset "rental" and apply action link to group
    And link asset to group
    Then it should successfully update group of "rental"asset
    When select asset "for-sale" and apply action link to group
    And link asset to group
    Then it should successfully update group of "for-sale"asset
    When select asset "for-rent" and apply action link to group
    And link asset to group
    Then it should successfully update group of "for-rent"asset

  Scenario: link asset to group on Assets Page
    Given field_member is login
    And User on Assets Page 4 assets of all kinds for field_member
    When select asset "owned" and apply action link to group for field_member
    And link asset to group
    Then it should successfully update group of "owned"asset
    When select asset "rental" and apply action link to group for field_member
    And link asset to group
    Then it should successfully update group of "rental"asset
    When select asset "for-sale" and apply action link to group for field_member
    And link asset to group
    Then it should successfully update group of "for-sale"asset
    When select asset "for-rent" and apply action link to group for field_member
    And link asset to group
    Then it should successfully update group of "for-rent"asset

  Scenario: link asset to group on Assets Page for projecct leader
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given project leader is login
    And User on asset Detail Page 4 assets of all kinds for leader
    When select asset "owned" and apply action link to group
    And link asset to group
    Then it should successfully update group of "owned"asset
    When select asset "rental" and apply action link to group
    And link asset to group
    Then it should successfully update group of "rental"asset
    When select asset "for-sale" and apply action link to group
    And link asset to group
    Then it should successfully update group of "for-sale"asset
    When select asset "for-rent" and apply action link to group
    And link asset to group
    Then it should successfully update group of "for-rent"asset

  Scenario: link asset to group on Assets Page for member
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given member is login
    And User on asset Detail Page 4 assets of all kinds for member
    When select asset "owned" and apply action link to group for member
    And link asset to group
    Then it should successfully update group of "owned"asset
    When select asset "rental" and apply action link to group for member
    And link asset to group
    Then it should successfully update group of "rental"asset
    When select asset "for-sale" and apply action link to group for member
    And link asset to group
    Then it should successfully update group of "for-sale"asset
    When select asset "for-rent" and apply action link to group for member
    And link asset to group
    Then it should successfully update group of "for-rent"asset


