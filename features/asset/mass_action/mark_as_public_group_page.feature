@javascript
Feature: Mark as Public on Groups Page
	In order to mark asset as public 
	As a user
	I want to verify that asset mark as public successfully

	Scenario: Mark asset as Public on Groups Page as a field member
	  Given field_member is login
	  And User on Group Detail Page 4 assets of all kinds for field_member
	  When select asset and apply action "Marked as Public" on group page
	  Then there should be no option for Mark as Public
