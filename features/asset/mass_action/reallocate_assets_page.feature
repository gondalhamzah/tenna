@javascript
Feature: Reallocate project of asset on Groups Page
  In order to reallocate asset
  As a user
  I want to verify that asset group change successfully

  Scenario: Reallocation on Assets Page for company admin
    Given "company_admin" is login
    And User on Assets Page 4 assets of all kinds( Rental, Owned, For-Sale, For-Rent)
    When select asset "owned" and apply action "reallocation"
    And change project of asset
    Then it should successfully update project of "owned"asset
    When select asset "rental" and apply action "reallocation"
    And change project of asset
    Then it should successfully update project of "rental"asset
    When select asset "for-sale" and apply action "reallocation"
    And change project of asset
    Then it should successfully update project of "for-sale"asset
    When select asset "for-rent" and apply action "reallocation"
    And change project of asset
    Then it should successfully update project of "for-rent"asset

  Scenario: Reallocation on Assets Page for field member
    Given field_member is login
    And User on Assets Page 4 assets of all kinds for field_member
    When select asset "owned" and apply action "reallocation" for field_member
    And change project of asset
    Then it should successfully update project of "owned"asset
    When select asset "rental" and apply action "reallocation" for field_member
    And change project of asset
    Then it should successfully update project of "rental"asset
    When select asset "for-sale" and apply action "reallocation" for field_member
    And change project of asset
    Then it should successfully update project of "for-sale"asset
    When select asset "for-rent" and apply action "reallocation" for field_member
    And change project of asset
    Then it should successfully update project of "for-rent"asset

  Scenario: Reallocation on Assets Page for leader
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given project leader is login
    And User on Assets Page 4 assets of all kinds for leader
    When select asset "owned" and apply action "reallocation" for leader
    And change project of asset
    Then it should successfully update project of "owned"asset
    When select asset "rental" and apply action "reallocation" for leader
    And change project of asset
    Then it should successfully update project of "rental"asset
    When select asset "for-sale" and apply action "reallocation" for leader
    And change project of asset
    Then it should successfully update project of "for-sale"asset
    When select asset "for-rent" and apply action "reallocation" for leader
    And change project of asset
    Then it should successfully update project of "for-rent"asset
