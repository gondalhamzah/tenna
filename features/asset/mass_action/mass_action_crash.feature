@javascript
Feature: Mark as Public on Assets Page
  In order to mark asset as public
  As a user
  I want to verify that asset mark as public successfully

  Scenario: Mark asset as Public on Assets Page as a field member
    Given field_member is login
    And User on Assets Page 4 assets of all kinds for field_member
    When select asset and apply action "Marked as Public" on assets page
    Then there should be no option for Mark as Public
