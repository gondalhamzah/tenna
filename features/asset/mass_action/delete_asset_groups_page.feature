@javascript
Feature: Delete asset on Groups Page
  In order to delete an asset on groups page
  As a company admin
  I want to verify that asset deleted successfully on assets page

  Scenario: delete asset on groups Page
    Given javascript driver is changed
    Given "company_admin" is login
    And User on Group Detail Page 4 assets of all kinds( Rental, Owned, For-Sale, For-Rent)
    When select asset owned and apply action "Deleted" on group page
    Then there should be 3 assets on group page
    When select rental asset and apply action "Deleted" on group page
    Then there should be 2 assets on group page
    When select for-sale asset and apply action "Deleted" on group page
    Then there should be 1 assets on group page
    When select for-rent asset and apply action "Deleted" on group page
    Then there should be 0 assets on group page
    Then switch to default driver

  Scenario: delete asset on groups Page
    Given javascript driver is changed
    Given field_member is login
    And User on Group Detail Page 4 assets of all kinds for field_member
    When select asset and apply action "Deleted" on group page
    Then there should be no option for delete
    Then switch to default driver

  Scenario: delete asset on groups Page as Leader
    # FIX IS IN 2.9
    # Given javascript driver is changed
    # Given project leader is login
    # And User on Group Detail Page 6 assets of all kinds for leader
    # When select asset owned and apply action "Deleted" on group page
    # Then there should be 3 assets on group page
    # When select rental asset and apply action "Deleted" on group page
    # Then there should be 2 assets on group page
    # When select for-sale asset and apply action "Deleted" on group page
    # Then there should be 1 assets on group page
    # When select for-rent asset and apply action "Deleted" on group page
    # Then there should be 0 assets on group page
    # Then switch to default driver

  Scenario: delete asset on groups Page as member
    # FIX IS IN 2.9
    # Given javascript driver is changed
    # Given member is login
    # And User on Group Detail Page 4 assets of all kinds for member
    # When select "owned" and apply action Deleted on groups page
    # Then there should be 3 assets on groups page
    # When select "rental" and apply action Deleted on groups page
    # Then there should be 2 assets on groups page
    # When select "for-sale" and apply action Deleted on groups page
    # Then there should be 1 assets on groups page
    # When select "for-rent" and apply action Deleted on groups page
    # Then there should be 0 assets on groups page
    # Then switch to default driver
