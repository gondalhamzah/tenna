@javascript
Feature: Create For sale Asset
  In order to create asset with import/export
  As a company admin
  I want to verify asset successfully created

  Scenario: Verify asset created successfully
    Given "company_admin" is login
    And goto project detail page
    And Fill template with Name "Test-Owned-Asset" as owned "Test-Renal-Asset" as rental "Test-For-Rent-Asset" as for-rent and "Test-For-Sale-Asset" as for-sale
    And goto project detail page
    Then There should be 4 assets  with Name "Test-Owned-Asset" as owned "Test-Renal-Asset" as rental "Test-For-Rent-Asset" as for-rent and "Test-For-Sale-Asset" as for-sale on project page
    And goto assets page
    Then There should be 4 assets  with Name "Test-Owned-Asset" as owned "Test-Renal-Asset" as rental "Test-For-Rent-Asset" as for-rent and "Test-For-Sale-Asset" as for-sale on assets page

  Scenario: Verify asset created successfully
    Given "company_admin" is login
    And goto group detail page
    And import asset template on groups page
    And Fill template with Name "Test-Owned-Asset" as owned "Test-Renal-Asset" as rental "Test-For-Rent-Asset" as for-rent and "Test-For-Sale-Asset" as for-sale
    And Import template on groupspage
    And goto groups detail page
    Then There should be 4 assets  with Name "Test-Owned-Asset" as owned "Test-Renal-Asset" as rental "Test-For-Rent-Asset" as for-rent and "Test-For-Sale-Asset" as for-sale on project page
