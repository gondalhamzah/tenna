@javascript
Feature: Wanted Post

  Scenario: Verify that CA can create and edit Wanted Posts
    Given javascript driver is changed
    Given Login as "Company Admin"
    Then goto add wanted post page
    Then user can create Wanted Post as "Admin"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 1
    Then user can edit Wanted Post for "Admin"
    Then check a wanted asset
    Then click on Action
    And mark as "Delete"
    Then Wanted Asset count should be 0
    Then switch to default driver

  Scenario: Verify that Leader can create and edit Wanted Posts
    Given javascript driver is changed
    Given Login as "Project Leader"
    Then goto add wanted post page
    Then user can create Wanted Post as "Leader"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 1
    Then user can edit Wanted Post for "Leader"
    Then check a wanted asset
    Then click on Action
    And mark as "Delete"
    Then Wanted Asset count should be 0
    Then switch to default driver

  Scenario: Verify that Member can create and edit Wanted Posts
    Given javascript driver is changed
    Given Login as "Project Member"
    Then goto add wanted post page
    Then user can create Wanted Post as "Member"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 1
    Then user can edit Wanted Post for "Member"
    Then check a wanted asset
    Then click on Action
    And mark as "Delete"
    Then Wanted Asset count should be 0
    Then switch to default driver

  Scenario: Verify that Field can create and edit Wanted Posts
    Given javascript driver is changed
    Given Login as "Field"
    Then goto add wanted post page
    Then user can create Wanted Post as "Field"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 1
    Then user can edit Wanted Post for "Field"
    Then check a wanted asset
    Then click on Action
    And mark as "Delete"
    Then Wanted Asset count should be 0
    Then switch to default driver

  Scenario: Verify that Company User can create and edit Wanted Posts
    Given javascript driver is changed
    Given Login as "Company User"
    Then goto add wanted post page
    Then user can create Wanted Post as "User"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 1
    Then user can edit Wanted Post for "User"
    Then check a wanted asset
    Then click on Action
    And mark as "Delete"
    Then Wanted Asset count should be 0
    Then switch to default driver

  Scenario: Wanted Post Priorities MARS-845
    Given javascript driver is changed
    Given "company_admin" is login
    Then visit wanted post index page
    And verify text "Priority"
    And verify wanted post priorities present on index page
    Then goto add wanted post page
    And verify wanted post priorities present on create page
    And verify wanted post priority "Medium" checked on create page
    Then user can create Wanted Post as "Admin"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 1
    And verify text "Priority"
    And verify wanted post priorities present on index page
    Then goto add wanted post page
    And verify wanted post priorities present on create page
    And verify wanted post priority "Medium" checked on create page
    Then user can create Wanted Post as "Admin"
    And choose wanted post priority "High"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 2
    Then goto add wanted post page
    And verify wanted post priorities present on create page
    And verify wanted post priority "Medium" checked on create page
    Then user can create Wanted Post as "Admin"
    And choose wanted post priority "Low"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 3
    Then choose wanted post filter "High"
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then choose wanted post filter "Medium"
    And click on Apply Filters
    Then Wanted Asset count should be 2
    Then choose wanted post filter "Low"
    And click on Apply Filters
    Then Wanted Asset count should be 3
    Then visit wanted post index page
    Then Wanted Asset count should be 3
    And select wanted asset with priority "High"
    Then click on Action
    And mark as "fullfilled"
    Then Wanted Asset count should be 2
    And select wanted asset with priority "Medium"
    Then click on Action
    And mark as "fullfilled"
    Then Wanted Asset count should be 1
    And select wanted asset with priority "Low"
    Then click on Action
    And mark as "fullfilled"
    Then Wanted Asset count should be 0
    Then visit wanted post index page
    Then Wanted Asset count should be 0
    Then check "fullfilled" filter option
    Then choose wanted post filter "High"
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then choose wanted post filter "Medium"
    And click on Apply Filters
    Then Wanted Asset count should be 2
    Then choose wanted post filter "Low"
    And click on Apply Filters
    Then Wanted Asset count should be 3

  Scenario: Wanted Post Priorities MARS-1111
    Given javascript driver is changed
    Given "company_admin" is login
    And click on menu icon
    And click on menu item "Want It"
    And verify text "Wanted Posts"

    Then goto add wanted post page
    Then user can create Wanted Post as "Admin"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 1

    Then goto add wanted post page
    Then user can create Wanted Post as "Admin"
    And choose wanted post priority "High"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 2

    Then goto add wanted post page
    Then user can create Wanted Post as "Admin"
    And choose wanted post priority "Low"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 3

    Then select all wanted assets
    Then click on Action
    And mark as "fullfilled"
    Then choose wanted post filter "Fulfilled"
    Then choose wanted post filter "High"
    And click on Apply Filters
    And sleep for "2" second
    Then Wanted Asset count should be 1
    Then visit wanted post index page
    Then choose wanted post filter "Fulfilled"
    Then choose wanted post filter "Medium"
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then visit wanted post index page
    Then choose wanted post filter "Fulfilled"
    Then choose wanted post filter "Low"
    And click on Apply Filters
    Then Wanted Asset count should be 1

    Then goto add wanted post page
    Then user can create Wanted Post as "Admin"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 1

    Then goto add wanted post page
    Then user can create Wanted Post as "Admin"
    And choose wanted post priority "High"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 2

    Then goto add wanted post page
    Then user can create Wanted Post as "Admin"
    And choose wanted post priority "Low"
    And click on text "Create Wanted asset"
    Then Wanted Asset count should be 3

    Then visit wanted post index page
    Then choose wanted post filter "My Wanted Posts"
    Then choose wanted post filter "Low"
    And click on Apply Filters
    Then Wanted Asset count should be 1

    Then visit wanted post index page
    Then choose wanted post filter "My Wanted Posts"
    Then choose wanted post filter "Medium"
    And click on Apply Filters
    Then Wanted Asset count should be 1

    Then visit wanted post index page
    Then choose wanted post filter "My Wanted Posts"
    Then choose wanted post filter "High"
    And click on Apply Filters
    Then Wanted Asset count should be 1
