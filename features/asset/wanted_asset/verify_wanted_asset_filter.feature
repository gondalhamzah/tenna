 @javascript

 Feature: Wanted asset Filter Options
  In Order to check wanted asset filter function
  As a Tester
  I want to verify filter options are fetching valid assets

  Scenario: Verify fullfilled as an Admin
    Given javascript driver is changed
    Given "company_admin" is login for wanted assets
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    Then Wanted Asset count should be 1
    And check a wanted asset
    Then click on Action
    And mark as "fullfilled"
    Then Wanted Asset count should be 0
    Then check "fullfilled" filter option
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And I should not see mark as fullfilled in actions dropdown
    Then select a category
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then select a project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And unselect category
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And unselect project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And select another category
    And click on Apply Filters
    Then Wanted Asset count should be 0
    And select another project
    And click on Apply Filters
    Then Wanted Asset count should be 0
    Then select a category
    Then select a project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then switch to default driver

  Scenario: Verify fullfilled as field member
    Given javascript driver is changed
    Given field_member is login for wanted assets
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    Then Wanted Asset count should be 1
    And check a wanted asset
    Then click on Action
    And mark as "fullfilled"
    Then Wanted Asset count should be 0
    Then check "fullfilled" filter option
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And I should not see mark as fullfilled in actions dropdown
    Then select a category
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then select a project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And unselect category
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And unselect project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And select another category
    And click on Apply Filters
    Then Wanted Asset count should be 0
    And select another project
    And click on Apply Filters
    Then Wanted Asset count should be 0
    Then select a category
    Then select a project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then switch to default driver

  Scenario: Verify fullfilled as Project Leader
    Given javascript driver is changed
    Given project leader is login for wanted assets
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    Then Wanted Asset count should be 1
    And check a wanted asset
    Then click on Action
    And mark as "fullfilled"
    Then Wanted Asset count should be 0
    Then check "fullfilled" filter option
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And I should not see mark as fullfilled in actions dropdown
    Then select a category
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then select a project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And unselect category
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And unselect project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And select another category
    And click on Apply Filters
    Then Wanted Asset count should be 0
    And select another project for leader
    And click on Apply Filters
    Then Wanted Asset count should be 0
    Then select a category
    Then select a project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then switch to default driver

  Scenario: Verify fullfilled as Project Member
    Given javascript driver is changed
    Given Login as "Project Member"
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    Then Wanted Asset count should be 1
    And check a wanted asset
    Then click on Action
    And mark as "fullfilled"
    Then Wanted Asset count should be 0
    Then check "fullfilled" filter option
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And I should not see mark as fullfilled in actions dropdown
    Then select a category
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then select a project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And unselect category
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And unselect project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    And select another category
    And click on Apply Filters
    Then Wanted Asset count should be 0
    And select another project for member
    And click on Apply Filters
    Then Wanted Asset count should be 0
    Then select a category
    Then select a project
    And click on Apply Filters
    Then Wanted Asset count should be 1
    Then switch to default driver
