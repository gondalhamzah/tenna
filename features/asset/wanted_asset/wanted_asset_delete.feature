@javascript
Feature: Wanted asset marked as fullfilled
  In Order to check wanted asset delete function
  As a Tester
  I want to verify delete function is working

  Scenario: Verify delete as an Admin
    Given javascript driver is changed
    Given "company_admin" is login for wanted assets
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    Then Wanted Asset count should be 1
    And check a wanted asset
    Then click on Action
    And mark as "Delete"
    Then Wanted Asset count should be 0
    Then switch to default driver

  Scenario: Verify delete as Project Member
    Given javascript driver is changed
    Given Login as "Project Member"
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    Then Wanted Asset count should be 1
    And check a wanted asset
    Then click on Action
    And mark as "Delete"
    Then Wanted Asset count should be 0
    Then switch to default driver

  Scenario: Verify delete as Field Member
    Given javascript driver is changed
    Given field_member is login for wanted assets
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    Then Wanted Asset count should be 1
    And check a wanted asset
    Then click on Action
    And mark as "Delete"
    Then Wanted Asset count should be 0
    Then switch to default driver
