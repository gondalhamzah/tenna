@javascript
Feature: Create Wanted Asset
  In order to add new wanted asset
  As a company admin
  I want to verify wanted assets are created

  Scenario: Verify wanted asset creation As Company Admin
    Given javascript driver is changed
    Given "company_admin" is login for wanted assets
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    And sleep for "3" second
    Then Wanted Asset count should be 1
    Then switch to default driver

  Scenario: Verify wanted asset creation As Project Leader
    Given javascript driver is changed
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given project leader is login
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    And sleep for "3" second
    Then Wanted Asset count should be 1
    Then switch to default driver

  Scenario: Verify wanted asset creation As Project Member
    Given javascript driver is changed
    Given Login as "Project Member"
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    And sleep for "3" second
    Then Wanted Asset count should be 1
    Then switch to default driver

  Scenario: Verify wanted asset creation As Field Member
    Given javascript driver is changed
    Given field_member is login for wanted assets
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    And sleep for "3" second
    Then Wanted Asset count should be 1
    Then switch to default driver

  Scenario: Verify wanted asset creation As Company Admin via Canadian
    Given javascript driver is changed
    Given "company_admin" is login for wanted assets
    Then goto add wanted post page
    And fill the Add Wanted Post form with candian
    Then click on "Create Wanted asset"
    And sleep for "3" second
    Then Wanted Asset count should be 1
    Then switch to default driver

  Scenario: Verify categories filters on wanted assets index page BUG-735
    Given javascript driver is changed
    Given "company_admin" is login for wanted assets
    Then goto add wanted post page
    And fill the Add Wanted Post form
    Then click on "Create Wanted asset"
    And sleep for "3" second
    Then Wanted Asset count should be 1
    Then verify categories filter new look
    Then switch to default driver

  Scenario: Wanted Post contribution/fulfilled MARS-1084
    Given "company_admin" is login for wanted assets
    And enable assets-reports subscription
    Then user can create Wanted Post as "user"
    And visit reports page
    And click on attribute tab
    And select wanted
    And sleep for "2" second
    And select contributed option
    And sleep for "2" second
    And select fulfilled option
