@javascript
Feature: Asset
  In order to verify
  that Users can create
  and delete assets

  Scenario: Verify Company Admin can delete assets
    Given javascript driver is changed
    Given Login as "Company Admin"
    Given a new category is created
    Then user creates an asset with name "Owned Test Asset"
    And sleep for "2" second
    And goto assets page
    Then "Owned Test Asset" is visible on Assets page
    Then user can also delete "Owned Test Asset"
    Then switch to default driver

  Scenario: Verify Project Leader can delete assets
    Given javascript driver is changed
    Given Login as "Project Leader"
    Then user creates an asset with name "Owned Test Asset"
    And sleep for "2" second
    And goto assets page
    Then "Owned Test Asset" is visible on Assets page
    Then user can also delete "Owned Test Asset"
    Then switch to default driver

  Scenario: Verify Project Member can delete assets
    Given javascript driver is changed
    Given Login as "Project Member"
    Then user creates an asset with name "Owned Test Asset"
    And sleep for "2" second
    And goto assets page
    Then "Owned Test Asset" is visible on Assets page
    Then user can also delete "Owned Test Asset"
    Then switch to default driver

  Scenario: Verify Field cannot delete assets
    Given javascript driver is changed
    Given Login as "Field"
    Then user creates an asset with name "Owned Test Asset"
    And sleep for "2" second
    And goto assets page
    Then "Owned Test Asset" is visible on Assets page
    Then user cannot delete "Owned Test Asset"
    Then switch to default driver

  Scenario: Verify Company User cannot delete assets
    Given javascript driver is changed
    Given Login as "Company User"
    Then adding asset throws error
    Then switch to default driver
