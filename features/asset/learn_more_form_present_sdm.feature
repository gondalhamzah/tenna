@javascript
Feature: Learn more form
  In order to fill learn more form
  As a User
  I want to verify learn more form exists

  Scenario: Verify Learn more form is present
    Given go to home page
    Then find learn more button should not be on page
    # And find learn more form and its fields should present
    Then go to industries page
    Then find learn more button should not be on page
    And find learn more form and its fields should present
    Then go to value page
    Then find learn more button should not be on "value" page
    And find learn more form and its fields should present
    Then go to about page
    Then find learn more button should not be on "about" page
    And find learn more form and its fields should present
