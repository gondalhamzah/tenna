@javascript
Feature: Asset
  In order to check asset bluetooth update history
  As a Company Admin
  I want to verify asset history

  Scenario: Capture bluetooth tracker history
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    Then select bt tracking and add "unique" tracker value
    And click on text "Submit"
    And sleep for "2" second
    Then there should be "unique"
    Then change Asset lat and lng
    Given refresh the current page
    And click on text "Add Tracking"
    Then select bt tracking and add "another_unique" tracker value
    And click on text "Submit"
    Given refresh the current page
    Then there should be "another_unique"

  Scenario: QA Track count is not is not working. No increment on adding tracker to asset BUG-806
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And select "BT" tracking and add "unique" value
    And click on text "Submit"
    And sleep for "2" second
    Then there should be "unique"
    And visit dashboard
    And sleep for "5" second
    And verify qr code amount 0
    And goto asset detail page of "Fake param"
    And sleep for "2" second
    And click on text "Add Tracking"
    And select "Sigfox" tracking and add "unique1" value
    And click on text "Submit"
    And sleep for "2" second
    Then there should be "unique1"
    And visit dashboard
    And sleep for "5" second
    And verify qr code amount 0
    And goto asset detail page of "Fake param"
    And sleep for "2" second
    And click on text "Add Tracking"
    And select "Cellular" tracking and add "unique2" value
    And click on text "Submit"
    And sleep for "2" second
    Then there should be "unique2"
    And visit dashboard
    And sleep for "5" second
    And verify qr code amount 0
    And goto asset detail page of "Fake param"
    And sleep for "2" second
    And click on text "Add Tracking"
    And select "LoRa" tracking and add "unique3" value
    And click on text "Submit"
    And sleep for "2" second
    Then there should be "unique3"
    And visit dashboard
    And sleep for "5" second
    And verify qr code amount 0
    And goto asset detail page of "Fake param"
    And sleep for "2" second
    And click on text "Add Tracking"
    And select "RFID" tracking and add "unique4" value
    And click on text "Submit"
    And sleep for "2" second
    Then there should be "unique4"
    And visit dashboard
    And sleep for "5" second
    And verify qr code amount 1
    And goto asset detail page of "Fake param"
    And sleep for "2" second
    And click on text "Add Tracking"
    And select "QR" tracking and add "unique5" value
    And click on text "Submit"
    And sleep for "2" second
    Then there should be "unique5"
    And visit dashboard
    And sleep for "5" second
    And verify qr code amount 1
    And goto asset detail page of "Fake param"
    And sleep for "2" second
    And click on text "Add Tracking"
    And select "UPC" tracking and add "unique6" value
    And click on text "Submit"
    And sleep for "2" second
    Then there should be "unique6"
    And visit dashboard
    And sleep for "5" second
    And verify qr code amount 1
