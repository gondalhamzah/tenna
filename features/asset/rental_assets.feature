@javascript
Feature: Asset 
  In order to show asset detail page
  As a user
  I want to verify rental assets

  Scenario: Verify Filters Rental Correction
    Given User is login
    And On Asset Create Page create Rental asset and "type material"

    And goto assets page
    And apply asset management filter "Rentals"
    And click on text "Apply Filters"
    Then rental assets count should be 1

    And apply asset management filter "Owned"
    And click on text "Apply Filters"
    Then rental assets count should be 0
    And On Asset Create Page create Rental asset and "type equipment"
    And goto assets page

    And apply asset management filter "Rentals"
    And click on text "Apply Filters"
    Then rental assets count should be 2
    And goto assets page

    And apply asset management filter "Rentals"
    And apply asset type filter "Material"
    And click on text "Apply Filters"
    Then rental assets count should be 1
    And goto assets page

    And apply asset management filter "Rentals"
    And apply asset type filter "Equipment"
    And click on text "Apply Filters"
    Then rental assets count should be 1

    Given Create a group and allocate both assets to it
    And goto groups detail page

    And apply asset management filter "Rentals"
    And click on text "Apply Filters"
    Then rental assets count should be 2

    And apply asset management filter "Rentals"
    And apply asset type filter "Material"
    And click on text "Apply Filters"
    Then rental assets count should be 1

    And apply asset management filter "Rentals"
    And apply asset type filter "Equipment"
    Then rental assets count should be 1

    Given Create a project and allocate both assets to it
    And goto project detail page
    And apply asset management filter "Rentals"
    Then rental assets count should be 2

    And apply asset management filter "Rentals"
    And apply asset type filter "Material"
    Then rental assets count should be 1

    And apply asset management filter "Rentals"
    And apply asset type filter "Equipment"
    Then rental assets count should be 1

    Given GO to rental asset edit with type material and change it to owned
    And goto assets page
    And apply asset management filter "Rentals"
    And apply asset type filter "Equipment"
    And click on text "Apply Filters"
    Then rental assets count should be 1

    And goto assets page
    And apply asset management filter "Rentals"
    And apply asset type filter "Material"
    And click on text "Apply Filters"
    Then assets count should be 0

    And goto project detail page
    And apply asset management filter "Rentals"
    Then rental assets count should be 1

    And apply asset management filter "Rentals"
    And apply asset type filter "Equipment"
    And click on text "Apply Filters"
    Then rental assets count should be 1

    And apply asset management filter "Rentals"
    And apply asset type filter "Material"
    And click on text "Apply Filters"
    Then assets count should be 0

    And goto group detail page
    And apply asset management filter "Rentals"
    And click on text "Apply Filters"
    Then rental assets count should be 1

    And apply asset management filter "Rentals"
    And apply asset type filter "Equipment"
    And click on text "Apply Filters"
    Then assets count should be 0

    And goto assets page
    And apply asset management filter "Rentals"
    And click on text "Apply Filters"
    Then assets count should be 1

    And apply asset management filter "Owned"
    And click on text "Apply Filters"
    Then assets count should be 1

    And goto project detail page
    And apply asset management filter "Rentals"
    Then rental assets count should be 1

    And apply asset management filter "Owned"
    And click on text "Apply Filters"
    Then assets count should be 1

    And goto group detail page
    And apply asset management filter "Rentals"
    And click on text "Apply Filters"
    Then rental assets count should be 1

    And apply asset management filter "Owned"
    And click on text "Apply Filters"
    Then rental assets count should be 1

    Given GO to rental asset edit with type equipment and change it to owned

    And goto assets page
    And apply asset management filter "Rentals"
    And click on text "Apply Filters"
    Then assets count should be 0

    And apply asset management filter "Rentals"
    And apply asset type filter "Material"
    And click on text "Apply Filters"
    Then assets count should be 0
