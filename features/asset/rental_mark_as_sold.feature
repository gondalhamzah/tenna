@javascript
Feature: Asset 
  In order to verify Mark as sold
  As a user
  I want to verify mark as sold option for rental assets

  Scenario: Verify Filters Rental Correction
    Given User is login
    And create rental and owned assets
    And goto asset edit page of "rental asset"
    Then there should be "no mark as sold"
    And goto asset edit page of "owned asset"
    Then there should be "mark as sold"
    And goto assets page and goto action
    Then there should be "mark as sold"
    And go to assets page and apply "rental filter"
    And goto action
    Then there should be "no mark as sold"
    And go to assets page and apply "owned filter"
    And goto action
    Then there should be "mark as sold"
    And goto groups detail page and goto action
    Then there should be "mark as sold"
    And go to group detail and apply "rental filter"
    And goto action
    Then there should be "no mark as sold"
    And go to group detail and apply "owned filter"
    And goto action
    Then there should be "mark as sold"
    And goto project detail page and goto action
    Then there should be "mark as sold"
    And go to project detail and apply "rental filter"
    Then there should be "no mark as sold"
    And go to project detail and apply "owned filter"
