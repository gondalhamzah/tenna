@javascript
Feature: Create Assets
  In order to create asset
  As a company admin
  I want to verify asset successfully created

  Scenario: Verify Category creation from active admin
    Given Admin is login
    And visit category group page
    Then fill category group data and verify creation

  Scenario: Verify For-sale asset created successfully
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    When goto asset edit page
    Then input "asset_quantity" should be 100

  Scenario: Verify For-rent asset created successfully
    Given "company_admin" is login
    And on asset create page
    And create a for-rent asset with name "For-rent-Test-Asset"
    Then Verify "For-rent-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-rent-Test-Asset" and verify it label as for-rent

  Scenario: Verify Owned asset created successfully BUG-731
    Given "company_admin" is login
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    Then input "asset_quantity" should be 100

  Scenario: Verify Rental asset created successfully
    Given javascript driver is changed
    Given "company_admin" is login
    And on asset create page
    And create a rental asset with name "Rental-Test-Asset"
    Then Verify "Rental-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Rental-Test-Asset" and verify it label as rental
    Then switch to default driver

  Scenario: Verify For-sale asset created successfully with canadian
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset" with canada
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one

  Scenario: Verify if text is Max Value
    Given javascript driver is changed
    Given User is login
    When visit assets new page
    And verify text "Max value"

  Scenario: Checking Inventory Asset quantity on edit
    Given Login as "Company Admin"
    And goto assets create page
    Then there should be Maintenance checkbox with text "Make sure to uncheck if the maintenance is complete"
    When check the Maintenance checkbox
    And input asset quantity 1
    Then check the inventory option
    And click on text "Create Asset"
    Then there should be text "Owned"
    When goto asset edit page
    And input asset quantity 0
    And click on text "Update Asset"

  Scenario: Asset quantity changes to 1 when go to edit asset screen BUG-738
    Given "company_admin" is login
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    Then input "asset_quantity" should be 100

  Scenario: Asset: When trying to create new marketplace asset, it is letting me choose  'This is an inventory asset' option BUG-752
    Given "company_admin" is login
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And change asset type to "Public Marketplace"
    And check inventory asset check box
    And sleep for "2" second
    And verify text "Public asset cannot move to Inventory"

  # Scenario: Unlimited PDF file upload MARS-1110
  #   Given "company_admin" is login
  #   And on asset create page
  #   And fill input field "asset_title" with "Owned-Test-Asset"
  #   And fill input field "asset_location_zip" with "66002"
  #   And select site with id "1"
  #   And select category with id "1"
  #   And select country with name "United States"
  #   And fill input field "asset_price" with "2100"
  #   And fill input field "asset_quantity" with "100"
  #   And attach "1" pdf file(s)
  #   Given scroll to page end
  #   And click create asset button
  #   And sleep for "5" second
  #   Then there should be text "Owned-Test-Asset"
  #   Then there should be text "dummy-pdf_1.pdf"
  #   And click on class ".icon-fab-edit"
  #   And sleep for "5" second
  #   Given scroll to page end
  #   And attach "2" pdf file(s)
  #   And click on text "Update Asset"
  #   And sleep for "5" second
  #   Then there should be text "dummy-pdf_1.pdf"
  #   Then there should be text "dummy-pdf_2.pdf"
  #   And click on class ".icon-fab-edit"
  #   And sleep for "5" second
  #   Given scroll to page end
  #   And attach "3" pdf file(s)
  #   And click on text "Update Asset"
  #   And sleep for "5" second
  #   Then there should be text "dummy-pdf_1.pdf"
  #   Then there should be text "dummy-pdf_2.pdf"
  #   Then there should be text "dummy-pdf_3.pdf"
  #   And click on class ".icon-fab-edit"
  #   And sleep for "5" second
  #   Given scroll to page end
  #   And remove all pdf files from asset
  #   And click on text "Update Asset"
  #   And sleep for "5" second
  #   Then there should not be "dummy-pdf_1.pdf"
  #   Then there should not be "dummy-pdf_2.pdf"
  #   Then there should not be "dummy-pdf_3.pdf"

   Scenario: Low level error while creating an asset with maintenance checkbox BUG-875
    Given "company_admin" is login
    And click on menu and click on asset
    And click on create asset
    And check maintainance checkbox

   Scenario: Create Asset - Cross Link/button on schedule maintenance popup is not working BUG-870
    Given "company_admin" is login
    And click on menu icon
    And click on menu item "Assets"
    And click on create new asset
    And click on schedule link
    And click on cross popup
    And verify text absence "Tenna’s Maintenance feature is"
