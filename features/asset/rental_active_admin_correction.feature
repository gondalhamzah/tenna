@javascript
Feature: Reantal Asset
  In order to verify Active Admin Update asset data record as rental
  As a Admin
  I want to verify rental asset update correction as active admin

  Scenario: Active Admin Update asset data record as rental
    Given Admin is login
    And create rental and owned assets for active admin
    And goto assets page for active admin
    And select owned for active admin
    And select action of an asset as for rent
    And select rental option update asset for active admin
    Then Asset updated successfully
    And goto asset index page of active admin
    Then asset should be present in the list with rental field true
    And goto to asset detail page of active admin
    Then asset should be rental for active admin
