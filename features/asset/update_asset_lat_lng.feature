@javascript
Feature: Asset Scanning Lat Lng Position
  In order to verify Asset Position
  As a User
  I want to verify Asset Lat and Lng are added

  Scenario: Check Asset Lat Lng Position
    Given "company_admin" is login
    And sleep for "2" second
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    And confirm location access
    And click on text "Add Tracking"
    And sleep for "2" second
    And select QR from select and fill "ping"
    And sleep for "5" second
    And visit dashboard
    And sleep for "2" second
    Then go to find it page
    And scan the asset with ping RFID
    And sleep for "2" second

  Scenario: Application error while updating group for an asset BUG-849
    Given "company_admin" is login
    And on asset create page
    And create a for-rent asset with name "For-rent-Test-Asset"
    And click on menu and click on asset
    And click on specific
    And click to edit that
    And update its group
    And click update asset