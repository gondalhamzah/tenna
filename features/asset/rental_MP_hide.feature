@javascript
Feature: Asset 
  In order to verify Add /Edit page must hide MP radio button on rental asset
  As a user
  I want to verify MP is hide for rental assets

  Scenario: Verify Filters Rental Correction
    Given User is login
    And create rental and owned assets
    And goto asset edit page of "rental asset"
    Then there MP should "Hide"
    And goto asset edit page of "owned asset"
    Then there MP should "Show"
    And goto asset edit page of "owned asset"
    And select rental asset
    Then there MP should "Hide"
