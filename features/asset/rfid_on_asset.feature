@javascript
Feature: RFID verification
  In order to use RFID
  As a system user
  I want to verify RFID function is working

  Scenario: Verify RFID Inventory function
    Given javascript driver is changed
    Given "company_admin" is login
    Given enable RFID from admin panel
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And add a RFID code to asset
    Then go to find it page
    And scan the asset with RFID
    Then there should be "1" asset in investory tab
    And check if save button disabled
    And click on text "Start Inventory"
    And click on text "Save List"
    And click on link View Saved Lists
    Then there should be "1" list
    And click on the list and it should have one asset
    And scan the asset with same RFID
    Then there should "Already Scanned."
    Then enter a wrong RFID
    Then there should "No asset found with RFID"
    And on asset create page
    And create a for-sale asset with name "Third-sale-Test-Asset"
    Then Verify "Third-sale-Test-Asset" created successfully
    Then go to assets path
    And goto this asset detail page of "Third-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And add a "qqqq" RFID
    Then go to find it page
    And scan the asset with "qqqq" RFID
    Then there should be "1" asset in investory tab
    And check if save button disabled
    And click on text "Start Inventory"
    And click on text "Save List"
    And click on link View Saved Lists
    Then there should be "2" list
    And click on the list and it should have another asset
    Then enter a wrong RFID
    Then there should be text "No asset found with RFID"
    And scan the asset with RFID
    Then there should be "2" asset in investory tab
    And check if save button disabled
    And click on text "Start Inventory"
    And click on text "Save List"
    And click on link View Saved Lists
    And click on first view list and it should have another asset
    And check the asset in the save list
    Then choose Action realocate
    And goto this asset detail page of "Third-sale-Test-Asset" and verify it label as for-sale
    Then there should be text "Reallocated"
    And check the first asset in the save list
    Then switch to default driver

  Scenario: Verify RFID checklist function
    Given javascript driver is changed
    Given "company_admin" is login
    Given enable RFID from admin panel
    And on asset create page
    And create a for-sale asset with name "Third-sale-Test-Asset"
    Then Verify "Third-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    And sleep for "1" second
    Then goto asset detail page of "Third-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And add a third RFID code to asset
    And sleep for "2" second
    Then go to findit checklist page
    Then check for sale option
    And sleep for "1" second
    And click on button Save List
    And sleep for "2" second
    And click on link View Saved Lists
    And sleep for "2" second
    Then click on first saved list
    And sleep for "1" second
    Then there should be "1" asset in looking for table
    And click on button RESET
    And sleep for "2" second
    Then click on Asset Type
    And select first category
    And click on Apply Filter
    And sleep for "2" second
    # Reset button is redirecting to wrong url
    # Then there should be "1" asset in looking for table
    # And click on button Save List
    # And sleep for "2" second
    # And click on link View Saved Lists
    # And sleep for "2" second
    # Then click on first saved list
    # Then there should be "1" asset in looking for table
    Then switch to default driver
