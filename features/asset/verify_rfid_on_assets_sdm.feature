@javascript
Feature: RFID verification
  In order to use RFID
  As a system user
  I want to verify RFID function is working

  Scenario: Company admin cannot see asset of another company
    Given javascript driver is changed
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And add a "qqqq" RFID
    Then logout from "Company Admin"
    Given "company_admin" is login
    Then go to find it page
    And scan the asset with "qqqq" RFID
    Then there should be text "No asset found with RFID qqqq."
    And sleep for "1" second
    Then switch to default driver

  Scenario: Company admin can add tracking to the assets
    Given javascript driver is changed
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    Then add new tracking code to the asset
    And apply tracker filter should return correct asset

  Scenario: Check List filter with Equipment option
    Given javascript driver is changed
    Given "company_admin" is login
    Given enable find-it subscription
    And on asset create page
    And create a for-sale asset with name "Fourth-sale-Test-Asset"
    Then Verify "Fourth-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    And sleep for "1" second
    Then goto asset detail page of "Fourth-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And add a third RFID code to asset
    And sleep for "2" second
    Then go to find it page
    Then click on Asset Type
    And check equipment option
    And click on Apply Filter
    # Then there should be "1" asset in looking for table
    # And click on button Save List
    # And click on text "Save List"
    # And sleep for "2" second
    # And click on link View Saved Lists
    # And sleep for "2" second
    # Then click on first saved list
    # Then there should be "1" asset in looking for table
    # And check the first asset in the save list
    # And click on Action drop down
    # And click on text "Actions"
    # And click on Delete drop down
    # And click on text "Delete"
    # And confirm a popup
    # Then there should be text "Successfully updated 1 asset."
    # Then switch to default driver

  Scenario: Check Save list when filter result is empty
    Given javascript driver is changed
    Given "company_admin" is login
    Given enable find-it subscription
    And on asset create page
    And create a for-sale asset with name "Third-sale-Test-Asset"
    Then Verify "Third-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    And sleep for "1" second
    Then goto asset detail page of "Third-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And add a third RFID code to asset
    And sleep for "2" second
    Then go to find it page
    And change filter to sold
    And scan third asset with "poll" RFID
    And sleep for "1" second
    And click on button Save List
    And sleep for "2" second
    And click on link View Saved Lists
    And sleep for "2" second
    Then click on first saved list
    And sleep for "1" second
    # Then there should be "1" asset in not in filtered list table
    Then switch to default driver
