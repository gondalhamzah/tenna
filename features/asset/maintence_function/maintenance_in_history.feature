@javascript
Feature: Maintenance should be in history
  In order to verify Maintenance in history
  As a user
  I want to verify maintenance is in history

  Scenario: Verify maintenance option in history
    Given javascript driver is changed
    Given Login as "Company Admin"
    And goto assets create page
    And create "owned" asset from view
    And goto asset page and complete maintenance for "owned"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "rental" asset from view
    And goto asset page and complete maintenance for "rental"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "for-sale" asset from view
    And goto asset page and complete maintenance for "for-sale"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "for-rent" asset from view
    And goto asset page and complete maintenance for "for-rent"
    Then there should be Maintenance requested and completed in history
    Then switch to default driver

  Scenario: Verify maintenance option in history for leader
    Given javascript driver is changed
    Given Login as "Project Leader"
    And goto assets create page
    And create "owned" asset from view
    And goto asset page and complete maintenance for "owned"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "rental" asset from view
    And goto asset page and complete maintenance for "rental"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "for-sale" asset from view
    And goto asset page and complete maintenance for "for-sale"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "for-rent" asset from view
    And goto asset page and complete maintenance for "for-rent"
    Then there should be Maintenance requested and completed in history
    Then switch to default driver

  Scenario: Verify maintenance option in history for project member
    Given javascript driver is changed
    Given Login as "Project Member"
    And goto assets create page
    And create "owned" asset from view
    And goto asset page and complete maintenance for "owned"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "rental" asset from view
    And goto asset page and complete maintenance for "rental"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "for-sale" asset from view
    And goto asset page and complete maintenance for "for-sale"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "for-rent" asset from view
    And goto asset page and complete maintenance for "for-rent"
    Then there should be Maintenance requested and completed in history
    Then switch to default driver

  Scenario: Verify maintenance option in history for field
    Given javascript driver is changed
    Given Login as "Field"
    And goto assets create page
    And create "owned" asset from view
    And goto asset page and complete maintenance for "owned"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "rental" asset from view
    And goto asset page and complete maintenance for "rental"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "for-sale" asset from view
    And goto asset page and complete maintenance for "for-sale"
    Then there should be Maintenance requested and completed in history
    And goto assets create page
    And create "for-rent" asset from view
    And goto asset page and complete maintenance for "for-rent"
    Then there should be Maintenance requested and completed in history
    Then switch to default driver
