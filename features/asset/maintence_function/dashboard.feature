@javascript
Feature: Maintenance assets count on dashboard
  The number of asset marked for maintainence should be displayed on dashboard
  Scenario: Verify Maintenance assets count on dashboard
    Given Login as "Company Admin"
    And goto assets create page
    Then there should be Maintenance checkbox with text "Make sure to uncheck if the maintenance is complete"
    When check the Maintenance checkbox
    And save asset as "For-sale asset"
    Given reset asset index
    And visit dashboard
    Then maintenance count should be 1
