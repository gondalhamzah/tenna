@javascript
Feature: Maintenance Function
  In order to verify Maintenance function for different users on asset page
  As a user
  I want to verify that there should be Maintenance function on asset page

  Scenario: Verify Maintenance function
    Given Login as "Public User"
    And goto assets create page
    Then there should not be "Make sure to uncheck if the maintenance is complete."
    Then there should not be "Schedule Maintenance"
    Then there should not be "Maintenance"
    Then logout from "Public User"

  Scenario: Split 1
    Given Login as "Company Admin"
    And goto assets create page
    Then there should be Maintenance checkbox with text "Make sure to uncheck if the maintenance is complete"
    When check the Maintenance checkbox
    And save asset as "For-sale asset"
    Then asset page should contain require Maintenance with Maintenance icon
    When goto assets page
    Then there should be Maintenance icon for that asset
    When goto asset edit page
    Then there should be maintaince icon marked as checked
    Given uncheck the Maintenance checkbox
    And save asset
    Then asset page should not contain require Maintenance with Maintenance icon
    When goto assets page
    Then there should no Maintenance icon for that asset
    When goto asset edit page
    Then there should be maintaince icon marked as uncheck
    Then logout from "Company Admin"

  Scenario: Split 2
    Given Login as "Project Leader"
    And goto assets create page
    Then there should be Maintenance checkbox with text "Make sure to uncheck if the maintenance is complete"
    When check the Maintenance checkbox
    And save asset as "For-sale asset and with project"
    Then asset page should contain require Maintenance with Maintenance icon
    When goto assets page
    Then there should be Maintenance icon for that asset
    When goto asset edit page
    Then there should be maintaince icon marked as checked
    Given uncheck the Maintenance checkbox
    And save asset
    Then asset page should not contain require Maintenance with Maintenance icon
    When goto assets page
    Then there should no Maintenance icon for that asset
    When goto asset edit page
    Then there should be maintaince icon marked as uncheck
    Then logout from "Project Leader"

  Scenario: Split 3
    Given Login as "Project Member"
    And goto assets create page
    Then there should be Maintenance checkbox with text "Make sure to uncheck if the maintenance is complete"
    When check the Maintenance checkbox
    And save asset as "For-sale asset and with project"
    Then asset page should contain require Maintenance with Maintenance icon
    When goto assets page
    Then there should be Maintenance icon for that asset
    When goto asset edit page
    Then there should be maintaince icon marked as checked
    Given uncheck the Maintenance checkbox
    And save asset
    Then asset page should not contain require Maintenance with Maintenance icon
    When goto assets page
    Then there should no Maintenance icon for that asset
    When goto asset edit page
    Then there should be maintaince icon marked as uncheck
    Then logout from "Project Member"

  Scenario: Split 4
    Given Login as "Field"
    And goto assets create page
    Then there should be Maintenance checkbox with text "Make sure to uncheck if the maintenance is complete"
    When check the Maintenance checkbox
    And save asset as "For-sale asset and with project"
    Then asset page should contain require Maintenance with Maintenance icon
    When goto assets page
    Then there should be Maintenance icon for that asset
    When goto asset edit page
    Then there should be maintaince icon marked as checked
    Given uncheck the Maintenance checkbox
    And save asset
    Then asset page should not contain require Maintenance with Maintenance icon
    When goto assets page
    Then there should no Maintenance icon for that asset
    When goto asset edit page
    Then there should be maintaince icon marked as uncheck
    Then logout from "Field"
