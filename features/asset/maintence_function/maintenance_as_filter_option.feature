@javascript
Feature: Maintenance FIlter option on assets
  In order to verify Maintenance filter option there on assets and working
  As a user
  I want to verify that maintenance filter option working fine

  Scenario: Verify maintenance filter option
    Given Login as "Company Admin"
    And create 4 assets for "Company Admin"
    And mark 2 asset as require maintenance
    And goto assets page
    Then there should be 4 asset
    And apply filter with maintence
    Then assets count should be 2
    When link these assets to a group for "Company Admin"
    And goto detail page of that group
    Then there should be 4 asset
    And apply filter with maintence
    Then assets count should be 2
    And goto detail page of that project
    Then there should be 4 asset
    And apply filter with maintence
    Then assets count should be 2
    Then logout from "Company Admin"

  Scenario: Verify maintenance filter option for leader
    Given Login as "Project Leader"
    And create 4 assets for "Project Leader"
    And mark 2 asset as require maintenance
    And goto assets page
    Then there should be 4 asset
    And apply filter with maintence
    Then assets count should be 2
    When link these assets to a group for "Project Leader"
    And goto detail page of that group
    Then there should be 4 asset
    And apply filter with maintence
    Then assets count should be 2
    And goto detail page of that project
    Then there should be 4 asset
    And apply filter with maintence
    Then assets count should be 2
    Then logout from "Project Leader"

  Scenario: Verify maintenance filter option for member
    Given "company_admin" is login
    And Create a group
    Then logout from "Company Admin"
    Given Login as "Project Member"
    And create 4 assets for "Project Member"
    And mark 2 asset as require maintenance
    And goto assets page
    Then there should be 4 asset
    And apply filter with maintence
    Then assets count should be 2
    When link these assets to a group for "Project Member"
    # BUG : Project member don't have ability to view group or its assets
    # And goto detail page of that group
    # Then there should be 4 asset
    # And apply filter with maintence
    # Then assets count should be 2
    # And goto detail page of that project
    # Then there should be 4 asset
    # And apply filter with maintence
    # Then assets count should be 2

  Scenario: Asset filter on Hours and Miles
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    Then go to assets path
    Then assets count should be 1
    And click on tab Asset Type
    And fill in min hours tabs
    And click on Apply Filters
    Then assets count should be 1
    And click on tab Asset Type
    And fill in max hours tabs
    Then assets count should be 1
    And visit reports page
    Then there should be text "Asset Reports it is a Premium service"
    Given enable assets-reports subscription
    And visit reports page
    And click on text "Live Data"
    And click on live data first select
    And click on live data second select hours
    Then there should be text "9"
    Then there should be text "for-sale-test-asset"
    Then there should be text "Test category 1"

