@javascript
Feature: Asset
  In order to create assets
  As a Tester
  I Want to verify zip code is working for worldwide countries

  Scenario: Validate Zip Code as company admin
    Given javascript driver is changed
    Given "company_admin" is login
    And on asset create page
    And fill the assets form with wrong info
    Then there should be "Select a valid country or valid zip / postal code"
    And fill the assets form with right info
    And click on text "Create Asset"
    And sleep for "4" second
    Then should be "1" assets
    And on asset create page
    And fill the assets form with Canadian info
    And click on text "Create Asset"
    And sleep for "4" second
    Then should be "2" assets
    And on asset create page
    And fill the assets form with American info
    And click on text "Create Asset"
    And sleep for "4" second
    Then should be "3" assets
    Then switch to default driver

  Scenario: Validate Zip Code as leader
    Given javascript driver is changed
    Given Login as "Project Leader"
    And on asset create page
    And fill the assets form with wrong info
    Then there should be "Select a valid country or valid zip / postal code"
    And fill the assets form with right info
    And click on text "Create Asset"
    And sleep for "3" second
    Then should be "1" assets
    And on asset create page
    And fill the assets form with Canadian info
    And click on text "Create Asset"
    Then should be "2" assets
    And on asset create page
    And fill the assets form with American info
    And click on text "Create Asset"
    Then should be "3" assets
    Then switch to default driver

  Scenario: Validate Zip Code as project member
    Given javascript driver is changed
    Given Login as "Project Member"
    And on asset create page
    And fill the assets form with wrong info
    Then there should be "Select a valid country or valid zip / postal code"
    And fill the assets form with right info
    And click on text "Create Asset"
    Then should be "1" assets
    And on asset create page
    And fill the assets form with Canadian info
    And click on text "Create Asset"
    Then should be "2" assets
    And on asset create page
    And fill the assets form with American info
    And click on text "Create Asset"
    Then should be "3" assets
    Then switch to default driver

  Scenario: Validate Zip Code as field
    Given javascript driver is changed
    Given Login as "Field"
    And on asset create page
    And fill the assets form with wrong info
    Then there should be "Select a valid country or valid zip / postal code"
    And fill the assets form with right info
    And click on text "Create Asset"
    Then should be "1" assets
    And on asset create page
    And fill the assets form with Canadian info
    And click on text "Create Asset"
    Then should be "2" assets
    And on asset create page
    And fill the assets form with American info
    And click on text "Create Asset"
    Then should be "3" assets
    Then switch to default driver
