@javascript
Feature: Asset 
  In order to show asset detail page
  As a user
  I want to show asset detail page

  Scenario: Verify that on asset detail page there should exists a tab for groups under History
    Given User is login
    And User on Asset detail page

  Scenario: Verify action asset linked to group in Group history on asset detail page
    Given User is login
    And goto asset page select an asset
    And select "Link To Group" from action
    Then action asset "linked to group" in Group history on asset detail page

  Scenario: Verify action asset unlinked to group in Group history on asset detail page
    Given User is login
    And goto asset page select an asset
    And select "Link To Group" from action
    Then action asset "linked to group" in Group history on asset detail page
    And goto asset detail page of "Fake param"
    And select "Unlink Group" from action
    Then action asset "unlinked to group" in Group history on asset detail page

  Scenario: Verify Asset Detail page have group information
    Given User is login
    And On group detail page link asset to group
    Then there is information of linked group

  Scenario: Verify Asset mass action - "link to group" is there
    Given User is login
    And On asset page
    Then There should be an option like "Link To Group" in mass actions on assets page

  Scenario: Reallocation pop message should not appear while changing category of asset BUG-732
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And select second category from dropdown
    And sleep for "2" second
    And verify text absence "Is this a reallocation?"

  Scenario: Asset Quantity is not appearing on asset detail page BUG-785
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And verify text absence "Quantity: 100"
    When goto asset edit page
    And change asset type to "Material"
    And click on text "Update Asset"
    And sleep for "2" second
    And verify text "Quantity: 100"

   Scenario: Login as Public user and click on Asset then Application Error BUG-924
    Given Login as "Public User"
    And click on menu icon
    And click on menu item "Assets"
    And verify text "Assets"

  Scenario: QR Scan screen UX redesign - Action Links Main Page (QR Code is Associated to Asset) MARS-1250
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And select "RFID" tracking and add "asdf" value
    And click on text "Submit"
    And sleep for "2" second
    Then there should be "unique4"
    And visit QR scanning page
    And verify text "For-sale-Test-Asset"
    And click on text "Maintenance"
    And verify text "Request Maintenance"
    And fill input field "equipment_notes" with "These are some maintenance notes"
    And click on text "Save"
    And click on text "Done"
