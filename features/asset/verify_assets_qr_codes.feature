@javascript
Feature: Assets that are for sale and have QR codes should be included on the Marketplace
  In order to verify assets with QR codes and for sale in marketplace
  As a user
  I want to verify assets with QR codes and for sale in marketplace

  Scenario: Verify assets with QR codes and for sale in marketplace as company admin
    Given "company_admin" is login
    Given javascript driver is changed
    Given "company_admin" is login
    And check initial qr code amount
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And add "random" qr code to this asset
    And goto marketplace
    Then there should be "For-sale-Test-Asset" asset with QR code
    And check qr code amount after asset creation
    Then switch to default driver

  Scenario: Verify assets with QR codes and for sale in marketplace as leader
    Given javascript driver is changed
    Given reset asset index
    Given Login as "Project Leader"
    And check initial qr code amount
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And add "random" qr code to this asset
    And goto marketplace
    Then there should be "For-sale-Test-Asset" asset with QR code
    And check qr code amount after asset creation
    Then switch to default driver

  Scenario: Verify assets with QR codes and for sale in marketplace as project member
    Given javascript driver is changed
    Given reset asset index
    Given Login as "Project Member"
    And check initial qr code amount
    And on asset create page
    Given create "for-sale" asset for "Project Member"
    Then Verify "Test Asset1" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset"
    And click on text "Add Tracking"
    And add "random" qr code to this asset
    And goto marketplace
    Then there should be "For-sale-Test-Asset" asset with QR code
    And check qr code amount after asset creation
    Then switch to default driver

  Scenario: Verify assets with QR codes and for sale in marketplace as field
    Given javascript driver is changed
    Given reset asset index
    Given Login as "Field"
    And check initial qr code amount
    And on asset create page
    Given create "for-sale" asset for "Field"
    Then Verify "Test Asset1" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset"
    And click on text "Add Tracking"
    And add "random" qr code to this asset
    And goto marketplace
    Then there should be "For-sale-Test-Asset" asset with QR code
    And check qr code amount after asset creation
    Then switch to default driver
