@javascript
Feature: Asset 
  In order to verify rental Corrections on Asset pages
  As a user
  I want to rental Corrections on Asset pages

  Scenario: Rental Corrections on Asset pages
    Given User is login
    And create rental and owned assets
    And goto assets page
    And select rental
    Then public marketplace option greyed out
    Then Time length and time unit  appear and Price replaced by rental cost and value replaced with total rental cost
