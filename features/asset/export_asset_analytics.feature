@javascript
Feature: Export Asset Analytics
  In order to export assets analytics
  As a company admin
  I want to download analytics

  Scenario: Download Asset Analytics
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    And click on Export analytics
    Then check analytics export file is downloaded
    And read the downloaded CSV file

  Scenario: Import Assets: Text update MARS-1231
    Given javascript driver is changed
    Given "company_admin" is login
    And go to group page
    And click on group template
    And sleep for "2" second
    And prepare group csv file for import
    And click on import group
    And verify import groups popup opened
    And attach group template csv
    And click on text "Import"
    And verify text present "2 Groups imported successfully."
    And visit group detail of 1
    And verify group detail of 1 is opened
    And click on assets template
    And sleep for "2" second
    And click on import asset
    And verify import assets popup opened
    And attach asset template csv
    And click on text "Import"
    Then add a byebug

   Scenario: Import Assets: Importing assets thru Site BUG-898
    Given "company_admin" is login
    And click on menu icon
    And click on menu item "Sites"
    And click on site template
    And click on import site button
    And verify import sites popup opened
    And attach site template csv
    And click on text "Import"

   Scenario: Export asset report - heading and data in columns not matches BUG-927
    Given "company_admin" is login
    Given enable assets-reports subscription
    And visit assets reports page
    And click on export button

