@javascript
Feature: Asset 
  In order to show asset detail page
  As a user
  I want to show asset detail page

  Scenario: Verify that manage inventory option is hidden if asset has the inventory option unchecked
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And click on text "Add Tracking"
    And add a RFID code to asset
    Given enable RFID from admin panel
    Then go to find it page
    And scan the asset with RFID
    And visit QR scanning page
    Then there should not be manage inventory button
    Then enable inventory for last asset
    And visit QR scanning page
    Then there should be manage inventory button

  Scenario: Verify that the default slider screen should have 0 to begin with BUG-739
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And click on text "Add Tracking"
    And add a RFID code to asset
    Given enable RFID from admin panel
    When goto asset edit page
    Given scroll to page end
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    Then go to find it page
    And scan the asset with RFID
    Then there should be "1" asset in investory tab
    And visit bsqr asset page
    And check input value to be "0"

  Scenario: Verify that the default slider screen should have 0 to begin with BUG-730
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    Given enable assets-reports subscription
    And visit reports page
    And sleep for "2" second
    And click on myfilters
    And verify filter presence
    And click on delete asset link
    And verify delete popup is opened
    And click on text "Yes"
    And visit assets reports page
    And sleep for "2" second
    And click on myfilters
    And verify filter absence

  Scenario: Verify that the Tag/Tracker dropdown contains multiple values BUG-754
    Given "company_admin" is login
    Given enable assets-reports subscription
    And visit reports page
    And click on trackers tab
    And verify first dropdown values
    And verify second dropdown values

  Scenario: Verify save button color changes on manage inventory page BUG-748
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And click on text "Add Tracking"
    And add a RFID code to asset
    Given enable RFID from admin panel
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    And visit bsqr asset page
    Then subtract assets from inventory
    And check if save button "enabled"
    And set input value to be "101"
    And check if save button "disabled"

  Scenario: While using Move To and defining new asset - quantity and category should be disabled BUG-750
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And click on text "Add Tracking"
    And add a RFID code to asset
    Given enable RFID from admin panel
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    And visit bsqr asset page
    Then subtract assets from inventory
    And set input value to be "10"
    Then visit move quantity page
    And click on class ".btnFileInput"
    Then check field "#asset_quantity" value "10"
    And check if category dropdown disabled

  Scenario: Verify 2 Errors message appears when clicking on Save List with no assets on find it inventory page BUG-758
    Given "company_admin" is login
    Given enable RFID from admin panel
    And go to find it page
    And check if save button disabled

  Scenario: Verify that cancel button is working on create asset page BUG-766
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And click on text "Duplicate"
    And click on cancel button
    And verify current page is asset detail page of "1"

  Scenario: Verify that inventory asset is transfered BUG-737
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "2" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    And goto assets page
    When goto asset edit page
    And uncheck inventory asset check box
    And sleep for "2" second
    And click on continue from popup
    And sleep for "2" second
    Then move inventory to another asset
    And sleep for "2" second
    Then verify successfully popup opened
    # Last Asset's edit page
    When goto asset edit page
    Then verify asset quantity to be 1
    # First Asset's edit page
    When goto first asset edit page
    Then verify asset quantity to be 200

  Scenario: Verify that new field show no value if subtracting greater quantity than current for inventory asset BUG-747
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And click on text "Add Tracking"
    And add a RFID code to asset
    Given enable RFID from admin panel
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    Then go to find it page
    And scan the asset with RFID
    Then there should be "1" asset in investory tab
    And visit bsqr asset page
    Then verify new value "100"
    Then subtract assets from inventory
    And set input value to be "200"
    Then verify new value ""

  Scenario: Verify that new field show no value if subtracting greater quantity than current for inventory asset BUG-743
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And click on text "Add Tracking"
    And add a RFID code to asset
    Given enable RFID from admin panel
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    Then go to find it page
    And scan the asset with RFID
    Then there should be "1" asset in investory tab
    And visit bsqr asset page
    And set input value to be ","
    Then verify illegal char absence ","
    And set input value to be "$"
    Then verify illegal char absence "$"
    And set input value to be "@"
    Then verify illegal char absence "@"
    And set input value to be "Qqqq"
    Then verify illegal char absence "Qqqq"
    # Number more than 6 digits is not allowed
    And set input value to be "1234567890"
    Then verify illegal char absence "1234567890"
    And check input value to be "123456"

  Scenario: Assets -> Create/Edit Asset: Infinite Loop while transferring asset quantity BUG-797
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "2" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    And goto assets page
    When goto asset edit page
    And uncheck inventory asset check box
    And sleep for "2" second
    And click on continue from popup
    And sleep for "2" second
    Then move inventory to another asset
    And sleep for "2" second
    Then verify successfully popup opened
    Then verify inventory checkbox disabled

  Scenario: Inventory quantity remains the same after transferring the quantity to another asset BUG-777
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And change asset type to "Material"
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "2" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And change asset type to "Material"
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    And goto assets page
    When goto asset edit page
    And uncheck inventory asset check box
    And sleep for "2" second
    And click on continue from popup
    And sleep for "2" second
    Then move inventory to another asset
    And sleep for "2" second
    Then verify successfully popup opened
    # Last Asset's edit page
    When goto asset edit page
    Then verify asset quantity to be 1
    # First Asset's edit page
    When goto first asset edit page
    Then verify asset quantity to be 200

  Scenario: Notification for the number of added items for same asset not coming above the slider BUG-749
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And click on text "Add Tracking"
    And add a RFID code to asset
    Given enable RFID from admin panel
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    Then go to find it page
    And scan the asset with RFID
    Then there should be "1" asset in investory tab
    And visit bsqr asset page
    Then subtract assets from inventory
    And check if save button "enabled"
    And set input value to be "50"
    And verify text "You are taking 50 Items"
    Then add assets to inventory
    And verify text "You are adding 50 Items"
