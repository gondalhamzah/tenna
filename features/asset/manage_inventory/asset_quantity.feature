@javascript

Feature: Create Assets
  In order to verify asset quantity
  As a company admin
  I want to verify if asset quantity substraction is done properly

  Scenario: Verify asset substraction values
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset-QR"
    Then Verify "For-sale-Test-Asset-QR" created successfully
    And click on text "Add Tracking"
    And add "qqqq" qr code to this asset
    Then enable inventory for last asset
    Then add sleep for "2" seconds
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    And click on text "Add Tracking"
    And add "random" qr code to this asset
    Then enable inventory for last asset
    When browser is minimize
    And visit bsqr asset page
    Then goto new assets page from bsqr

  Scenario: Verify inventory quantity count on asset creation
    Given "company_admin" is login
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    And goto edit last asset
    And fill and verify quantity and inventory calculation

  Scenario: Deleted asset still appears on modal popup while transferring quantity. BUG-902
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "2" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And check inventory asset check box
    And click on text "Update Asset"
    And sleep for "2" second
    And goto assets page
    And select asset 1 on assets index
    And click on text "Actions"
    And apply action "Delete" on asset
    And goto asset edit page
    And uncheck inventory asset check box
    Then click on text "Continue"
    And verify inventory popup is empty
