@javascript
Feature: Asset Filter
  In order to check assets filter
  As a user
  I want to verify asset type has sub categories

  Scenario: Verify that Asset Type filter has sub-categories in dropdown
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And goto assets page
    And click on tab Asset Type
    And click on category dropdown
    And verify atleast one subcategory

  Scenario: Verify that category filter shows updated assets results gracefully BUG-722
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    Given enable assets-reports subscription
    And visit reports page
    Then click on filter type category
    Then choose category dropdown item number "0"
    Then choose sub-category dropdown item number "0"
    And sleep for "2" second
    Then verify "Test category 1" in all rows
    Then choose sub-category dropdown item number "1"
    And sleep for "2" second
    Then verify text present "No history to show"
    When goto asset edit page
    And select second category from dropdown
    And click on text "Update Asset"
    And visit reports page
    Then click on filter type category
    Then choose category dropdown item number "0"
    Then choose sub-category dropdown item number "1"
    And sleep for "2" second
    Then verify "Test category 2" in all rows

  Scenario: Verify that category filter shows updated assets results gracefully BUG-734
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "2" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    When goto asset edit page
    And select second category from dropdown
    And click on text "Update Asset"
    Given enable assets-reports subscription
    And visit reports page
    Then click on filter type category
    Then choose category dropdown item number "0"
    Then choose sub-category dropdown item number "0"
    And sleep for "2" second
    Then verify "Test category 1" in all rows
    Then choose sub-category dropdown item number "1"
    And sleep for "2" second
    Then verify "Test category 2" in all rows

  Scenario: Verify that category filter should not be under asset analytics in asset detail page BUG-776
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    Given scroll to page end
    Then verify category button not present

  Scenario: Category Filter drop down populating irrelevant fields when use filter BUG-780
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    Given enable assets-reports subscription
    And visit reports page
    Then fill new asset filter name
    Then click on filter type category
    Then choose category dropdown item number "0"
    Then choose sub-category dropdown item number "0"
    And sleep for "2" second
    Then verify "Test category 1" in all rows
    And click on text "Save As New"
    And sleep for "5" second
    And click on myfilters
    And click on useFilter
    And verify category dropdown values

  Scenario: Asset Report -> Filter drop down: My filters: Drop down is empty when using My filter BUG-721
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    Given enable assets-reports subscription
    And visit reports page
    Then fill new asset filter name
    Then click on filter type category
    Then choose category dropdown item number "0"
    Then choose sub-category dropdown item number "0"
    And sleep for "2" second
    Then verify "Test category 1" in all rows
    And click on text "Save As New"
    And sleep for "5" second
    And click on myfilters
    And click on useFilter
    And sleep for "4" second
    And verify category dropdown values
    # Then verify category dropdown not empty

  Scenario: Report It -> Asset Reports: When Use Filter under My Filter then *Category* Filter option is missing BUG-791
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then should be "1" assets
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    Given enable assets-reports subscription
    And visit reports page
    Then fill new asset filter name
    Then click on filter type category
    Then choose category dropdown item number "0"
    Then choose sub-category dropdown item number "0"
    And sleep for "2" second
    Then verify "Test category 1" in all rows
    And click on text "Save As New"
    And sleep for "5" second
    And click on myfilters
    And click on useFilter
    And sleep for "4" second
    Then verify filter type category present

  Scenario: Split Hours and Miles: Hours and Miles needs to split in filters too BUG-756
    Given "company_admin" is login
    And goto assets page
    And on asset create page
    And create a Owned asset with name "Owned-Test-Asset"
    Then Verify "Owned-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "Owned-Test-Asset" and verify it label as owned
    And goto assets page
    And click on tab Asset Type
    And verify asset type field present "#filter_search_hours_min"
    And verify asset type field present "#filter_search_hours_max"
    And verify asset type field present "#filter_search_miles_min"
    And verify asset type field present "#filter_search_miles_max"
    Given enable RFID from admin panel
    And go to find it page
    And click on tab Asset Type
    And verify asset type field present "#filter_search_hours_min"
    And verify asset type field present "#filter_search_hours_max"
    And verify asset type field present "#filter_search_miles_min"
    And verify asset type field present "#filter_search_miles_max"

  Scenario: On updating a filter the flash message appears "filter created successfully" BUG-953
    Given "company_admin" is login
    Given enable assets-reports subscription
    And visit reports page
    Then fill new asset filter name
    Then click on filter type category
    Then choose category dropdown item number "0"
    Then choose sub-category dropdown item number "0"
    Then verify "Test category 1" in all rows
    And click on text "Save As New"
    Then fill update asset filter name
    And click on text "Update Filter"
    And verify message successfuly

   Scenario: Search on asset index page is not working when we remove search text using cross sign BUG-956
    Given "company_admin" is login
    And click on menu icon
    And click on menu item "Assets"
    And fill search bar
    And click asset remove icon
    Then enter key press
