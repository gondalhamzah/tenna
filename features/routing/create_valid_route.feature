@javascript
Feature: Check if driver route is being created
  In order to Verify correct router creation
  As a company admin
  I want to Verify if route is created properly

  Scenario: Invite a user
    Given javascript driver is changed
    Given create subscription and visit routing
    When browser is maximize
    And visit company detail
    And click on text "Invite New User"
    When create a new driver
    And click on text "Send an Invitation"
    And sleep for "3" second
    And find text "Sam Osa (Invited)"

  Scenario: Route CRUD BUG-861 is also covered here
    Given javascript driver is changed
    Given visit dashboard page
    Given create subscription and visit routing
    And visit path finder page
    And fill vehicle capacity and project dropoff load
    Then click on create route
    And sleep for "5" second
    When limit exceed skip
    And verify route created and email check
    # Then check if mail has been sent succesfully
    Then check company and project correct address
    Then sleep for "3" second
    Then route should be created with "Joe Schmoe"
    And create another company from db
    And click on text "Edit Route"
    And sleep for "2" second
    Then change route driver
    Then change start and end time
    When change route name
    Then change project
    Then change vehicle capacity
    And change pickup
    And filters should be disabled
    And click on first text "Update Route"
    Then route should be created with "Editing Route Name"
    And visit route it page
    And click on text "MANAGE ROUTES" with settings
    And click on text Send Route
    And click on checkbox with settings
    And sleep for "2" second
    And send route
    And visit path finder page
    And click on text "MANAGE ROUTES" with settings
    And sleep for "2" second
    And click on text View with settings
    And sleep for "4" second
    And open new window to send route
    And sleep for "4" second
    And visit path finder page
    And click on text "MANAGE ROUTES" with settings
    And sleep for "2" second
    And click on text View with settings second
    # And open new window on another tab
    # And sleep for "5" second
    Then switch to default driver

  Scenario: Routing Error Messages
    Given visit dashboard page
    Given javascript driver is changed
    Given create subscription and visit routing
    And visit path finder page
    Then click on create route
    Then there should be text "Please select a Driver"
    And select a driver
    Then click on create route
    Then there should be text "Please select some assets"
    And visit path finder page
    When vehicle capacity and dropoff are not valid
    Then click on create route

  Scenario: UI and Spelling Checks
    Given javascript driver is changed
    Given create subscription and visit routing
    And visit path finder page
    And click on menu icon
    Then there should be text "Marketplace"
    Then there should be text "Sites"
    Then there should be text "Groups"
    Then there should be text "Assets"
    Then there should be text "Find It"
    Then there should be text "Route It"
    Then there should be text "Schedule It"

  Scenario: Verify email notifications on event creation
    Given javascript driver is changed
    Given "company_admin" is login
    Given enable schedule-it subscription
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    And visit asset details page
    And verify email check for assset maintainance
    Given disable schedule-it subscription

  Scenario: Not able to click on user name to see profile or sign out if routing feature is premium or suspended BUG-779
    Given javascript driver is changed
    Given "company_admin" is login
    And visit path finder page
    And verify routing feature is unsubscribed
    And logout using ui components
    And sleep for "3" second
    Given create subscription and visit routing
    And visit path finder page
    And verify routing feature is subscribed
    And logout using ui components

  Scenario: Not able to click on user name to see profile or sign out if routing feature is premium or suspended BUG-769
    Given javascript driver is changed
    Given visit dashboard page
    Given create subscription and visit routing
    And visit path finder page
    And fill vehicle capacity and project dropoff load
    Then click on create route
    And sleep for "5" second
    When limit exceed skip
    And verify route created and email check
    # Then check if mail has been sent succesfully
    # Then check company and project correct address
    And sleep for "5" second
    # And verify text "Notification sent successfully"
