@javascript
Feature: Assign company role
  In order to change company user role
  As a user
  I want to change the role of the user

  Scenario: Verify that Leader cannot assign company role
    Given javascript driver is changed
    Given Login as "Project Leader"
    When visit company profile page
    Then assign company role button is not present
    Then switch to default driver

  Scenario: Verify that Member cannot assign company role
    Given javascript driver is changed
    Given Login as "Project Member"
    When visit company profile page
    Then assign company role button is not present
    Then switch to default driver

  Scenario: Individual company index MARS-1232
    Given "company_admin" is login
    And create another company_admin_B
    And create asset with name "test_company_B_asset" company_admin_B
    And go to asset page
    And search company asset
    And go to site page
    And create project with name "test_company_B_project" company_admin_B
    And perform site search
    And go to group page
    And create group with name "test_company_B_group" company_admin_B
    And go to group page

  Scenario: Role dropdown is missing while adding site assignment BUG-929
    Given "company_admin" is login
    And click on company tab
    And click on user_name
    And click on add assignment button
    And role drop down check