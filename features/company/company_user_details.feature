@javascript
  Feature:  In Order to check company users
  As a Company Admin
  I want to visit user details page

  Scenario: Company Wide link to user details
    Given "company_admin" is login
    And go to "Company" page from nav
    Then there should be text "Re-invite"
    And user should not be clickable
    Then Accept user invite
    And go to "Company" page from nav
    And user should be clickable
    Then there should be text "Profile:"

  Scenario: Company Admin can add/edit User/s Phone Number BUG-716
    Given "company_admin" is login
    And go to "Company" page from nav
    And click on text "Invite New User"
    Then fill the invite new user form
    And open role dropdown
    And select "Company User" from role dropdown
    And click on text "Send an Invitation"
    Then check invitation email is sent
    Then there should be text "Re-invite"
    And go to "Company" page from nav
    And click on text "Aldaim User"
    And click on text "Edit"
    Then change user phone number

  Scenario: Company -> Invite New User: Application failure occurs when click on Invite New User BUG-689
    Given "company_admin" is login
    And go to "Company" page from nav
    And click on text "Invite New User"
    And sleep for "3" second
    And verify text "Send Invitation"

  Scenario: Send New Invitation UX updates MARS-1241 and also covers BUG-857
    Given javascript driver is changed
    Given "company_admin" is login
    And go to "Company" page from nav
    And click on text "Invite New User"
    And verify text "Send Invitation"
    And verify "email" field is focused
    And open role dropdown
    And verify role dropdown values on invitation pages
