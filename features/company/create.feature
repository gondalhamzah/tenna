@javascript
Feature: Create Company
  In order to Create Company
  As a Admin
  I want to Create Company Successfully

  Scenario: Verify Create Company Successfully
    Given Admin is login
    And on company create page
    And fill all require data
    Given scroll to page end
    And click save button
    Then Company Create Successfully

  Scenario: Verify name of company
    Given Admin is login
    And on company create page
    And click save button
    Then page Should throw an error to enter "name of company"
    Then page Should throw an error to enter "first name of company contact"
    Then page Should throw an error to enter "email of company contact"
    Then page Should throw an error to enter "last name of company contact"
    Then page Should throw an error to enter "address line of company"
    Then page Should throw an error to enter "city of company"
    Then page Should throw an error to enter "default project name"
    Then page Should throw an error to enter "valid closeout date for project of company"
    Then page Should throw an error to enter "address of project for company"
    Then page Should throw an error to enter "city of project"
    Then page Should throw an error to enter "state of project"
    Then page Should throw an error to enter "zipcode of project"

  Scenario: Edit API RPM while editing company
    Given Admin is login
    And on company create page
    And fill all require data
    Given scroll to page end
    And click save button
    Then Company Create Successfully
    And visit company detail page
    And Api RPM should be 0
    And click on nav "Companies"
    Then click on Generate new Api key
    Then there should be text "New API key has been generated successfully."
    And visit company detail page
    And Api RPM should be 100
    And click on text "Edit Company"
    Then update Api rmp
    Given scroll to page end
    And click on text "Update Company"
    Then there should be text "Company was successfully updated."
    # BUG API RPM is not updating
    # And Api RPM should be 200
