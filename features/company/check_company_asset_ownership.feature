@javascript
  Feature: Check Company asset ownership
  In Order to check company asset ownership
  As a Tester
  I want to verify company assets are not mixing

  Scenario: Verify company assets are not mixing
    Given "company_admin" is present
    Then create a company asset
    And add qr code for this asset
    Then create another "company_admin"
    And search the created asset
    Then company asset count should be 0

  Scenario: Individual Company Assets verification
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Then goto assets list page and verify assets count increse by one
    Then goto asset detail page of "For-sale-Test-Asset" and verify it label as for-sale
    And click on text "Add Tracking"
    And select QR from select and fill "ping"
    And sleep for "1" second
    When visit logged in company admin profile
    Then logout from "Company Admin"
    Given "company_admin" is login
    Given enable RFID from admin panel
    Then go to find it page
    Then check for sale option
    And sleep for "1" second
    And scan the asset with ping RFID in checklist
    And sleep for "2" second
    # Then there should be text "You do not have permissions to view this asset."
