@javascript
Feature: Check if Scheduling is working
  In order to Verify schedule creation
  As a company admin
  I want to Verify if scheduling is created properly

  Scenario: Verify Schedule it CRUD
    Given visit dashboard page
    Given a clear email queue
    Given javascript driver is changed
    Given a clear email queue
    Given visit dashboard page
    Given create subscription and visit routing
    And visit path finder page
    And sleep for "3" second
    And fill vehicle capacity and project dropoff load
    Then click on create route
    And sleep for "5" second
    When limit exceed skip
    And verify route created and email check
    Given enable Schedule it Subscription
    And visit schedule it page
    When check popup cancel button
    And edit and save the created event
    Then verify saved changes
    Then sleep for "5" second
    And check popup delete is working
    Then switch to default driver

  Scenario: Active Schedule it Page from Active Admin
    Given Admin is login
    And click on text "Companies"
    And click on text "Add Subscription"
    And select category "scheduleit" in active admin
    Given scroll to page end
    And click on Create Subscription
    Then there should be text "Subscription was successfully created."
    When subscription count should be 5
    And click on text "Logout"
    Given "company_admin" is login
    Given enable schedule-it subscription
    And visit schedule it page
    Then there should not be "Schedule It is an optional Premium selection"

  Scenario: Maintenance Subscription popup message
    # BUG 709: Reported 16 Nov 2017
    Given "company_admin" is login without clear
    Given Admin is login
    And click on text "Companies"
    Then click on second text "Add Subscription"
    When select option "maintenance" from id "subscription_category"
    When select option "suspended" from id "subscription_state"
    Given scroll to page end
    And click on Create Subscription
    And On asset page
    And sleep for "3" second
    And click on text "Test Asset1"
    When goto asset edit page
    When check the Maintenance checkbox on edit
    Then there should be text "Schedule It is an optional Premium selection"

  Scenario: Verify that User cannot create Events with past dates on Schedule It BUG-778
    Given visit dashboard page
    Given a clear email queue
    Given javascript driver is changed
    Given a clear email queue
    Given visit dashboard page
    Given create subscription and visit routing
    And visit path finder page
    And sleep for "3" second
    And fill vehicle capacity and project dropoff load
    Then click on create route
    And sleep for "5" second
    When limit exceed skip
    And verify route created and email check
    Given enable Schedule it Subscription
    And visit schedule it page
    When check popup cancel button
    And edit and verify event with past date not created
