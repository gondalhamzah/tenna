@javascript
Feature: Verify correct logic for Rental on dashboard
  In order to Rental chart on dashboard
  As a user
  I want to verify correct logic for Rental on dashboard

  Scenario: Verify correct logic for Rental on dashboard
    Given User is login
    And User on assets detail page having 3 assets
    And Verify all 3 assets are rental
    Then goto dashboard chart having 30$ for rental tab

  Scenario: Application failure when login as "Public user" BUG-698
    Given Login as "Public User"
    And verify text "Signed in successfully."

  Scenario: Live Data should not be accessible for Public User BUG-920
    Given Login as "Public User"
    And verify text absence "Live Data"
