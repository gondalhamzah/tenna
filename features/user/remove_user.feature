@javascript

Feature: Remove User from Company

	Scenario: Company Admin can remove users
		Given javascript driver is changed
		Given User is login
		When visit company profile page
		Then assign company role button is present
		Then remove user option is present
		Then switch to default driver

	Scenario: Project Leader cannot remove users
		Given javascript driver is changed
		Given Login as "Project Leader"
		When visit company profile page
		Then assign company role button is not present
		Then switch to default driver

	Scenario: Project Member cannot remove users
		Given javascript driver is changed
		Given Login as "Project Member"
		When visit company profile page
		Then assign company role button is not present
		Then switch to default driver