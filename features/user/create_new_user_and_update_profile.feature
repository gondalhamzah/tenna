@javascript
Feature: User
	In order to create user and its profile
	As a user
	I want to update my information on the profile

	Scenario: Verify if information is updated properly on profile
		Given a clear email queue
	  Given User is login
		And visit edit profile page
	  And Update user fields with new record
	  Then sleep for "2" second
	  When I open the email
	  Then I should see "Confirm my account" in the email body
	  And click on the link
		Then logout from "Company Admin"
	  Then visit sign-in path and sign-in with new credentials
	  And there should be text "Signed in successfully."

	Scenario: Verify if information is updated properly on profile as Project Leader
		Given a clear email queue
	  Given Login as "Project Leader"
		And visit edit profile page
	  And Update user fields with new record
	  Then sleep for "2" second
	  When I open the email
	  Then I should see "Confirm my account" in the email body
	  And click on the link
		Then logout from "Project Leader"
	  Then visit sign-in path and sign-in with new credentials
	  And there should be text "Signed in successfully."

	Scenario: Verify if information is updated properly on profile as Project Member
		Given a clear email queue
	  Given Login as "Project Member"
		And visit edit profile page
	  And Update user fields with new record
	  Then sleep for "2" second
	  When I open the email
	  Then I should see "Confirm my account" in the email body
	  And click on the link
		Then logout from "Project Member"
	  Then visit sign-in path and sign-in with new credentials
	  And there should be text "Signed in successfully."

	Scenario: Verify if information is updated properly on profile as Field Member
		Given a clear email queue
	  Given Login as "Field"
	  Then visit edit profile page
	  And Update user fields with new record
	  Then sleep for "2" second
	  When I open the email
	  Then I should see "Confirm my account" in the email body
	  And click on the link
		Then logout from "Field"
	  Then visit sign-in path and sign-in with new credentials
	  And there should be text "Signed in successfully."