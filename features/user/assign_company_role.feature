@javascript
Feature: User
	In order to change company user role
	As a user
	I want to change the role of the user

	Scenario: Verify if change company role is working fine as company admin
		Given javascript driver is changed
	  Given User is login
	  When visit company profile page
	  And click on text "Assign Company Role"
	  And change the role of user
	  Then user role should be changed
	  Then switch to default driver