@javascript

Feature: My Profile - Reset Password
	In order to verify that users
	can reset their own passwords
	through My Profile tab

	Scenario: Verify that CA can reset their own password
		Given visit dashboard page
		Given javascript driver is changed
		Given Login as "Company Admin"
		And visit edit profile page
		Then user can change their own password
		Then user can logout and log in again as "Company Admin" with updated password
		Given Login as "Project Leader"
		And visit edit profile page
		Then user can change their own password
		Then user can logout and log in again as "Leader" with updated password
		Given Login as "Project Member"
		And visit edit profile page
		Then user can change their own password
		Then user can logout and log in again as "Member" with updated password
		Given Login as "Company User"
		And visit edit profile page
		Then user can change their own password
		Then user can logout and log in again as "Company User" with updated password
		Given Login as "Field"
		And visit edit profile page
		Then user can change their own password
		Then user can logout and log in again as "Field" with updated password

#	Scenario: Verify that Leader can reset their own password
#		Given javascript driver is changed
#		Given Login as "Project Leader"
#		And visit edit profile page
#		Then user can change their own password
#		Then user can logout and log in again as "Leader" with updated password

#	Scenario: Verify that Member can reset their own password
#		Given javascript driver is changed
#		Given Login as "Project Member"
#		And visit edit profile page
#		Then user can change their own password
#		Then user can logout and log in again as "Member" with updated password

#	Scenario: Verify that Field can reset their own password
#		Given javascript driver is changed
#		Given Login as "Field"
#		And visit edit profile page
#		Then user can change their own password
#		Then user can logout and log in again as "Field" with updated password

#	Scenario: Verify that Company User can reset their own password
#		Given javascript driver is changed
#		Given Login as "Company User"
#		And visit edit profile page
#		Then user can change their own password
#		Then user can logout and log in again as "Company User" with updated password
