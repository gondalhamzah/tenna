@javascript
Feature: Check proper subscription validations
  In order to Verify subscriptions
  As a User
  Each user should be given correct subscriptions

  Scenario: Verify default subscriptions
    Given "company_admin" is login
    And check all unsubscribed feature notifications
    And check all subscribed feature notifications

  Scenario: Verify company subscription can only be done once BUG-687
    Given Admin is login
    And on company create page
    And fill all require data
    Given scroll to page end
    And click save button
    Then Company Create Successfully
    Then first click text "Add Subscription"
    When select option "asset reports" from id "subscription_category"
    Given scroll to page end
    And click on Create Subscription
    And visit companies page
    Then first click text "Add Subscription"
    When select option "asset reports" from id "subscription_category"
    Given scroll to page end
    And click on Create Subscription
    And visit companies page
    Then first click text "Add Subscription"
    When select option "asset reports" from id "subscription_category"
    Given scroll to page end
    And click on Create Subscription
    And visit companies page
    Then first click text "Add Subscription"
    When select option "asset reports" from id "subscription_category"
    Given scroll to page end
    And click on Create Subscription
    And visit companies page
