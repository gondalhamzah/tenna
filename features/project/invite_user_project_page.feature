@javascript
Feature: Project

  Scenario: Verify that Company Admin can invite Users from Project Users page
    Given javascript driver is changed
    Given User is login
    Given Company Admin created a project
    And Company Admin is on the project users page
    Then Company Admin invites a user from Project Users page
    Then switch to default driver

  Scenario: Verify that Site Leader can invite Users from Project Users page
    Given javascript driver is changed
    Given Login as "Project Leader"
    Given Project Leader is on their project users page
    Then Project Leader can invite a user from Project Users page
    Then switch to default driver

  Scenario: Verify that Site Member cannot invite Users from Project Users page
    Given javascript driver is changed
    Given Login as "Project Member"
    Then "member" goes to project description page
    Then Site Users link is not visible
    Then switch to default driver

  Scenario: Verify that Field Member cannot invite Users from Project Users page
    Given javascript driver is changed
    Given Login as "Field"
    Then "field" goes to project description page
    Then Site Users link is not visible
    Then switch to default driver

  Scenario: Verify that Company User cannot invite Users from Project Users page
    Given javascript driver is changed
    Given Login as "Company User"
    Then "company user" goes to project description page
    Then Site Users link is not visible
    Then switch to default driver
