@javascript
Feature: Verify fields
  In order to verify duration fields on site creation
  As a User
  I want to Verify there is no duration field on site creation

  Scenario: Verify site creation
    Given login company user "without site"
    And visit site creation page
    Then should not be duration fields
    Then site should be created succesfully

  Scenario: Verify site creation with existing duration
    Given login company user "with site"
    And visit site detail page
