@javascript
Feature: Invite User
  Scenario: Verify Site Leader can invite users
    Given javascript driver is changed
    Given Login as "Project Leader"
    When visit company profile page
    Then invite users button is found
    Then switch to default driver

  Scenario: Verify that Site Member cannot invite users
    Given javascript driver is changed
    Given Login as "Project Member"
    When visit company profile page
    Then invite users button cannot be found
    Then switch to default driver