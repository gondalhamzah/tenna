@javascript
Feature: Site
  To cover all features in site details page

  Scenario: Sites -> Sites Detail page -> Sites User -> Assign Existing User: User and Role drop downs are missing. BUG-859
    Given visit dashboard page
    Given javascript driver is changed
    Given Login as "Company Admin"
    And click on menu icon
    And click on menu item "Sites"
    Then visit site detail of 1
    And click on text "Site Users"
    And click on text "Assign Existing User"
    And verify default value "Joe Schmoe3" in user dropdown in site assignment page
    Then open user dropdown in site assignment page
    Then select "Joe Schmoe3" from user dropdown in site assignment page
    And verify default value "Joe Schmoe3" in user dropdown in site assignment page
    And verify default value "Leader" in role dropdown in site assignment page
    Then open role dropdown in site assignment page
    And verify role dropdown values in site assignment page

  Scenario: Sites -> Site detail page -> Export assets: Sort order drop down is missing. BUG-858
    Given javascript driver is changed
    Given visit dashboard page
    Given Login as "Company Admin"
    And click on menu icon
    And click on menu item "Sites"
    Then visit site detail of 1
    Then click on export assets
    And verify text "Export Asset Report"
    And open sort order dropdown export assets page
    And verify values in sort order dropdown on export assets page
