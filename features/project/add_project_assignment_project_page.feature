@javascript
Feature: Project
  To verify that only CA and PL
  can invite users form Site Users page
  All other users do not have the option

  Scenario: Verify that CA can add site assignment from Site Users page
    Given visit dashboard page
    Given javascript driver is changed
    Given Login as "Company Admin"
    Then "company admin" goes to project description page
    Then "company admin" assigns site role from site users page
    Then switch to default driver

  Scenario: Verify that PL can add site assignment from Site Users page
    Given visit dashboard page
    Given javascript driver is changed
    Given Login as "Project Leader"
    Then "leader" goes to project description page
    Then "project leader" assigns site role from site users page
    Then switch to default driver

  Scenario: Verify that Project Member cannot add site assignment from Site Users page
    Given visit dashboard page
    Given javascript driver is changed
    Given Login as "Project Member"
    Then "member" goes to project description page
    Then "member" cannot assign site assignments
    Then switch to default driver

  Scenario: Verify that Field cannot add site assignment from Site Users page
    Given visit dashboard page
    Given javascript driver is changed
    Given Login as "Field"
    Then "field" goes to project description page
    Then "field" cannot assign site assignments
    Then switch to default driver

  Scenario: Import site link is not working on Sites page BUG-851
    Given User is login
    And click on menu icon
    And click on menu item "Sites"
    And click on import site button
    And verify import sites popup opened

  Scenario: My company contact drop down is not appearing on create/update site BUG-854
    Given User is login
    And click on menu icon
    And click on menu item "Sites"
    Then go to new sites page
    Then verify company contact dropdown is opened
    Then verify company contact dropdown is switching values
