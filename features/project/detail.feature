@javascript
Feature: Project detail page
  In order to verify group tab on project detail page
  As a user
  I want to verify groups tab is working on Project Detail Page

  # has missing font bug
  Scenario: Verify groups tab is working on Project Detail Page
    Given User is login
    And on project detail page Click groups button
    Then user see the list of groups

  Scenario: Verify Project Detail Page has assets tab and groups tab
    Given User is login
    And on project detail page
    Then there should be text "ASSETS"
    Then there should be text "GROUPS"

  Scenario: Import assets link on site detail is not working BUG-853
    Given "company_admin" is login
    And project create
    And click on menu and click on site
    And click on a site
    And click on import asset-link

  Scenario: Sites: Within Miles drop down menu is missing BUG-928
    Given "company_admin" is login
    And click on menu icon
    And click on menu item "Sites"
    And within drop down check
