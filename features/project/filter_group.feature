@javascript
Feature: Project detail page
  In order to Verify the filter option on project page to include groups
  As a user
  I want to verify filter options on project page

  Scenario: Verify the filter option on project page to include groups
    Given User is login
    And on project detail
    And select a group in the Group section of the filter
    Then  assets belonging to the selected group must list

  Scenario: Verify the filter option on project to exclude closed group
    Given User is login
    And on group detail page with a close project
    Then group filter exclude that group

  Scenario: Verify the filter option on project page to exclude deleted groups
    Given User is login
    And on group detail page with a deleted project
    Then group filter exclude that deleted group

  Scenario: Search bar is not working on Sites index page BUG-958
    Given "company_admin" is login
    And click on menu icon
    And click on menu item "Sites"
    And fill search bar
    And click remove icon
    Then enter key press