@javascript
Feature: Site

  Scenario: Mandatory fields while creating New Site
    Given javascript driver is changed
    Given User is login
    Given user is on projects page
    And user is on add project menu
    Then system enforces Site "Name"
    Then fill in site name
    And click on text "Create Site"
    Then switch to default driver

  Scenario: Cancel button on Site Creation
    Given javascript driver is changed
    Given User is login
    Given user is on projects page
    And user is on add project menu
    And click on text "Cancel"
