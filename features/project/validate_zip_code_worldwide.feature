@javascript
Feature: Project
  In order to create projects
  As a Tester
  I Want to verify zip code is working for worldwide countries

  Scenario: Validate Zip Code as company admin
    Given javascript driver is changed
    Given reset project index
    Given "company_admin" is login
    And on project create page
    And create project with wrong info and validate
    And sleep for "2" second
    And click on text "Create Site"
    And on project create page
    And create project with right info and validate
    And click on text "Create Site" and goto projects page
    Then goto projects page
    And sleep for "2" second
    Then on projects page there should be "1" projects
    And on project create page
    And fill the projects form with Canadian info and validate
    And click on text "Create Site"
    Then goto projects page
    And click on text "Create Site" and goto projects page
    Then on projects page there should be "2" projects
    And on project create page
    And fill the projects form with American info and validate
    And click on text "Create Site"
    Then goto projects page
    And click on text "Create Site" and goto projects page
    Then on projects page there should be "3" projects

  Scenario: Validate Zip Code as field
    Given javascript driver is changed
    Given reset project index
    Given Login as "Field"
    And on project create page
    And create project with wrong info and validate
    And click on text "Create Site"
    And on project create page
    And create project with right info and validate
    And click on text "Create Site" and goto projects page
    Then goto projects page
    Then on projects page there should be "1" projects
    And on project create page
    And fill the projects form with Canadian info and validate
    And click on text "Create Site"
    Then goto projects page
    And click on text "Create Site" and goto projects page
    Then on projects page there should be "2" projects
    And on project create page
    And fill the projects form with American info and validate
    And click on text "Create Site" and goto projects page
    And click on text "Create Site"
    Then goto projects page
    Then on projects page there should be "3" projects
