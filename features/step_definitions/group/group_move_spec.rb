
Given(/^on group detail click move group$/) do
  @project = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  @project.contact = @company_admin
  project2 = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  project2.contact = @company_admin
  project3 = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  project3.contact = @company_admin
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  GroupsIndex.import(group: @group_b)
  ProjectsIndex.import(project: @project)
  ProjectsIndex.import(project: project2)
  ProjectsIndex.import(project: project3)
  visit groups_path
  if ENV["BROWSER_MODE"] == "true"
    #sleep 1
    page.find(:xpath, "//*[@id='group_#{@group_b.id}']/td[1]/label" ).click
    #sleep 1
    click_on "Actions"
    #sleep 1
    find(:xpath, "//*[@id='group-action-dropdown']/li[1]/span").click
    # click_on "Move"
    #sleep 2 
    page.first('#select2-project_id-container').click
    page.find('.select2-results__options', text: "#{project3.name}", exact: true).click
    #sleep 1
    click_on "Move Group" 
    #sleep 1
  end
end

Then(/^Group should be move from one project to another$/) do
  if ENV["BROWSER_MODE"] == "true"
    visit group_path id: @group_b.id
    #sleep 1
  end
end