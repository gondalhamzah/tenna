# Given(/^on group detail Click export button$/) do
# end

# Then(/^asset export successfully$/) do
# end

Then(/^click on import group$/) do
  find("#bulk-group-upload-modal-open").click
end

Then(/^verify import groups popup opened$/) do
  expect(page).to have_content("Import Groups")
  find(:css, "#bulk-group-upload-modal").present?
end

Then(/^click on group template$/) do
  find(".project-header-icons a", text: "Group Template").click
end

Then(/^prepare group csv file for import$/) do
  if ENV['BROWSER_MODE'] == "true"
    CSV.open("tmp/downloads/groups_inventory_capture_sheet.csv", "wb") do |csv|
      csv << ["*Name", "ID", "Client", "Description", "Notes"]
      csv << ["Group 1", "", "", " ", "",]
      csv << ["Group 2", "", "", " ", "",]
    end
  else
    puts "Needs to tested open browser"
  end
end

Then(/^attach group template csv$/) do
  if ENV['BROWSER_MODE'] == "true"
    within '.file-field.input-field' do
      page.attach_file 'file', File.join(Rails.root, 'tmp', 'downloads', 'groups_inventory_capture_sheet.csv'), :visible => false
    end
  else
    puts "Needs to tested open browser"
  end
end

Then(/^visit group detail of (\d+)$/) do |group_id|
  find(:css, "a[href='/groups/#{group_id.to_i}']").click
end

Then(/^verify group detail of (\d+) is opened$/) do |group_id|
  find(:xpath, '//*[@id="groups-show"]/main/div[1]/div[2]/div[1]/div[1]/h1').text == Group.find(group_id.to_i).name
end
