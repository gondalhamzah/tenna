Given(/^on group detail page$/) do
  if ENV["BROWSER_MODE"] == "true"
    @project = FactoryGirl.create(:project, contact: @company_admin)
    @project.contact = @company_admin
    @group = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
    visit group_path id: @group.id 
  end
end

Then(/^Group should be history tab$/) do
  if ENV["BROWSER_MODE"] == "true"
    visit group_path(id: @group.id) 
  end
end

Given(/^User on group history$/) do
  if ENV["BROWSER_MODE"] == "true"
    @project = FactoryGirl.create(:project, contact: @company_admin)
    @group = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
    GroupsIndex.import(group: @group)
  end
end

Then(/^history tab contain history of that group$/) do
  if ENV["BROWSER_MODE"] == "true"
    visit edit_group_path @group
    expect(page).to have_selector('#group_name', :count => 1)
    fill_in 'Name', with: "group123"
    click_button 'Update Group'
    visit group_path(id: @group.id)
    click_link 'History'
    expect(page).to have_selector('#tab-history', :count => 1)
  end
end

Given(/^User on group detail page click pencil button$/) do
  if ENV["BROWSER_MODE"] == "true"
    @project = FactoryGirl.create(:project, contact: @company_admin)
    @group = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
    GroupsIndex.import(group: @group)
    #sleep 2
    visit group_path(id: @group.id)
  end
end

Then(/^Group edit page should be open$/) do
  if ENV["BROWSER_MODE"] == "true"
    visit group_path(id: @group.id)
    expect(page).to have_css('i.icon-fab-edit', :count => 1)
    find(:xpath, "//*[@id='groups-show']/main/div[4]/a/i").click
    expect(page).to have_css(".form-inputs.row.separator-row")
  end
end


Given(/^User move group from one project to another$/) do
end

Then(/^Project should be in group history$/) do
end

Then(/^create a group$/) do
  fill_in 'Name', with: 'Group 1'
  find(:xpath, '//*[@id="new_group"]/div[1]/div[1]/div[4]/div[1]/span/span[1]/span').click
  find(:css, '#select2-group_project_id-results li', text: Project.last.name).click
end

Then(/^visit group detail page$/) do
  visit edit_group_path Group.last
end

Then(/^verify selected project$/) do
  find(:css, "#select2-group_project_id-container").text == Group.last.project.name
end

Given(/^User on group detail page for multiple groups$/) do
  multiple_groups_with_different_project
  @groups.each do |group|
    visit group_path id: group.id
    find("#btn-move-group").click
  end
end

Then(/^selected project should be group current project on project move tab$/) do
end