

Given(/^on group detail delete a group$/) do
  #sleep 1
  groups_page
  GroupsIndex.find(id: @group_b.id) do |a|
  		expect(page).to have_css('#group_#{a.id}', :count => 1)
  		check('#group-selected-#{a.id}')
  		expect(page).to have_css('#action-dropdown').eq[1].click 
  		page.evaluate_script('window.confirm = function() { return true; }')
  end
end

Then(/^Group should be deleted$/) do
  visit groups_path
  GroupsIndex.find(id: @group_b.id) do |a|
  	expect(page).not_to have_css('#group_#{a.id}', :count => 1)
  end
end