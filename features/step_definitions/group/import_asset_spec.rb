require "csv"

Given(/^on group detail Click assets template, templte should download$/) do
  # Chewy.strategy(:bypass)  
  # if ENV['BROWSER_MODE'] == "true"
  #   group_detail_page 
  #   #sleep 2
  #   click_link "ASSETS TEMPLATE"
  #   #sleep 2
  # else
  #   puts "Needs to be test open browser"
  # end
end

Given(/^Fill templete with requied data$/) do
end

Given(/^Click import asset button pop\-up will appear select CSV file$/) do
  if ENV['BROWSER_MODE'] == "true"
    CSV.open("tmp/downloads/inventory_capture_sheet.csv", "wb") do |csv|
      csv << ["Rental", "Public", "Type of Asset", "*Category", "*Title", "Description", "Make", "Model", "Year", "Hours/Miles", "Condition", "Price", "Quantity", "Asset number", "*Zip code", "Notes", "Durability", "*In use until", "*Market value"]
      csv << ["FALSE", "FALSE", "Material", "Aggregates ", "Asset 1", "set of forks included, engine recently overhauled", "CAT", "450", "2010", "8888", "Good", "29999", "1", "ID1234561", "12345", " ", "Permanent", "1/1/2018", "100"]
      csv << ["FALSE", "FALSE", "Material", "Aggregates ", "Asset 2", "set of forks included, engine recently overhauled", "CAT", "450", "2010", "8888", "Good", "29999", "1", "ID1234561", "12345", " ", "Permanent", "1/1/2018", "100"]
    end
    find(".modal-trigger.project-detail-icons").click
    page.attach_file 'csv_import_file', File.join(Rails.root, 'tmp', 'downloads', 'inventory_capture_sheet.csv'), :visible => false
    click_button "Import"
  else
    puts "Needs to tested open browser"
  end
end

Then(/^asset impport successfully$/) do
  # if ENV['BROWSER_MODE'] == "true"
  #   visit group_path id: @group_b.id
  # end
end

Given(/^on groups page Click group template, templte should download$/) do
  # visit groups_path 
  # if ENV['BROWSER_MODE'] == "true"
  #   #sleep 2
  #   click_link "GROUP TEMPLATE"
  # else
  #   puts "Needs to tested open browser"
  # end
end

Given(/^Click import group button pop\-up will appear select CSV file$/) do
  sleep 2
  if ENV['BROWSER_MODE'] == "true"
    sleep 2
    CSV.open("tmp/downloads/groups_inventory_capture_sheet.csv", "wb") do |csv|
      csv << ["*Name", "ID", "Client", "Description", "Notes"]
      csv << ["Group 1", "", "", " ", "",]
      csv << ["Group 2", "", "", " ", "",]
    end
    sleep 2
    click_link "IMPORT GROUP"
    sleep 2
    within '.file-field.input-field' do
      page.attach_file 'file', File.join(Rails.root, 'tmp', 'downloads', 'groups_inventory_capture_sheet.csv'), :visible => false 
      sleep 2
    end
    click_button('Import')
    sleep 2
  else
    puts "Needs to tested open browser"
  end
end

Then(/^group impport successfully$/) do
  # if ENV['BROWSER_MODE'] == "true"
  #   Chewy.strategy(:bypass)  
  #   visit groups_path
  #   #sleep 3
  #   clear_downloads  
  # else
  #   puts "Needs to tested open browser"
  # end
end

Given(/^Fill data only required filed and Click import group button$/) do
  if ENV['BROWSER_MODE'] == "true"
    CSV.open("tmp/downloads/groups_inventory_capture_sheet.csv", "wb") do |csv|
      csv << ["*Name", "ID", "Client", "Description", "Notes"]
      csv << ["Group 1", "", "", " ", "",]
      csv << ["Group 2", "", "", " ", "",]
    end
    sleep 2
    click_link "IMPORT GROUP"
    sleep 2
    within '.file-field.input-field' do
      page.attach_file 'file', File.join(Rails.root, 'tmp', 'downloads', 'groups_inventory_capture_sheet.csv'), visible: false
    end
    click_button('Import')
  else
    puts "Needs to tested open browser"
  end
end

Then(/^group impport successfully but not in history$/) do
  # if ENV['BROWSER_MODE'] == "true"
  #   find('#group_1').click
  #   within ".tabs.groups" do
  #     click_on "History"
  #   end
  #   #sleep 2
  #   # expect(page).not_to have_content("Description")
  #   #sleep 2
  # else
  #   puts "Needs to tested open browser"
  # end
end

Given(/^click on menu and click on group$/) do
  find(:xpath, '//*[@id="desktop_main_vertical_nav_btn"]').click
  sleep 2
  find(:xpath, '//*[@id="desktop_main_vertical_nav_dropdown"]/li[5]').click
end

Given(/^click on import group link$/) do
  find(:xpath, '//*[@id="bulk-group-upload-modal-open"]').click
  sleep 2
  expect(page).to have_content("Import Groups")
end

Given(/^click on a group$/) do
  find(:xpath, "//*[@id='group_#{Group.last.id}']/td[2]").click
end

Given(/^click on import asset link$/) do
  find(:xpath, '//*[@id="modal-upload-spreadsheet-open"]').click
  sleep 2
  expect(page).to have_content("Import")
end
