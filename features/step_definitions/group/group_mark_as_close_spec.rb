
Given(/^on group detail mark group as close$/) do
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @project.contact = @company_admin
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  GroupsIndex.import(group: @group_b)
  visit groups_path
  if ENV["BROWSER_MODE"] == "true"
    page.find(:xpath, "//*[@id='group_#{@group_b.id}']/td[1]/label" ).click
    click_on "Actions"
    find(:xpath, "//*[@id='group-action-dropdown']/li[2]/span").click
    page.driver.browser.switch_to.alert.accept
  end
end

Then(/^Group should closed$/) do
  #sleep 2
end
