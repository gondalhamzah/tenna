Given(/^on group detail$/) do
  # group_detail_page
end

Then(/^history should there on group detail page$/) do
  # expect(page).to have_content("History")
end


Then(/^history should contain the history of relevant group$/) do
  # expect(page).to have_content("History")
end

Given(/^on group detail click blue pencil$/) do
  # group_detail_page
  # first('.fixed-action-btn > a').click
end

Then(/^should redirect to edit group page$/) do
  # expect(page).to have_content("Edit Group")
end

Given(/^on asset detail page link asset to group$/) do
  if ENV['BROWSER_MODE'] == "true"
    @category_group = FactoryGirl.create(:category_group)
    project = FactoryGirl.create(:project, contact: @company_admin)
    project.contact = @company_admin
    group_b = FactoryGirl.create(:group, company: @company_admin.company, project: project)
    GroupsIndex.import(group: group_b)
    link_asset_to_group(group_b)
  end
end

Then(/^goto group history page it should contain history of asset$/) do
end

Given(/^change name of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    group_edit_page
    @oldname = @group_b.name
    fill_in "group[name]", with: "Name change"
    click_button "Update Group"
    GroupsIndex.import(group: @group_b)
    GroupsVersionsIndex.import(group: @group_b)
  end
end

Then(/^goto group history page it should contain new name and old name of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    visit group_path id: @group_b.id
    click_link("History")
  end
end

Given(/^change ID\/Number of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    group_edit_page
    @oldnumber = @group_b.group_num
    fill_in "group[group_num]", with: "Number change"
    click_button "Update Group"
    GroupsIndex.import(group: @group_b)
    GroupsVersionsIndex.import(group: @group_b)
  end
end

Then(/^goto group history page it should contain new ID\/Number and old ID\/Number of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    visit group_path id: @group_b.id
    click_link("History")
  end
end

Given(/^change Client of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    group_edit_page
    @oldclient = @group_b.client
    fill_in "group[client]", with: "change client"
    click_button "Update Group"
    GroupsIndex.import(group: @group_b)
    GroupsVersionsIndex.import(group: @group_b)
  end
end

Then(/^goto group history page it contain new and old client of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    visit group_path id: @group_b.id
    click_link("History")
  end
end

Given(/^change project of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    @project = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
    @project_b = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
    @project.contact = @company_admin
    @project_b.contact = @company_admin 
    @group_b = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
    GroupsIndex.import(group: @group_b)
    visit edit_group_path id: @group_b.id
    @oldprojectname = @group_b.project.name

    find(:css, '#select2-group_project_id-container').click
    find(:css, '#select2-group_project_id-results li', text: @project_b.name).click

    click_on "OK"
    click_button "Update Group"
    GroupsIndex.import(group: @group_b)
    GroupsVersionsIndex.import(group: @group_b)
  end
end

Then(/^goto group history page it contain new project and old project of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    visit group_path id: @group_b.id
    click_link("History")
  end
end

Given(/^change description of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    group_edit_page
    @old_description = @group_b.description
    fill_in "group[description]", with: "change description of group"
    click_button "Update Group"
    GroupsIndex.import(group: @group_b)
    GroupsVersionsIndex.import(group: @group_b)
  end 
end

Then(/^goto group history page it contain new and old description of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    visit group_path id: @group_b.id
    click_link("History")
  end
end

Given(/^update notes of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    group_edit_page
    @old_notes = @group_b.notes
    fill_in "group[notes]", with: "Group notes change"
    click_button "Update Group"
    GroupsIndex.import(group: @group_b)
    GroupsVersionsIndex.import(group: @group_b)
  end
end

Then(/^goto group history page it contain new and old notes of group$/) do
  if ENV['BROWSER_MODE'] == "true"
    visit group_path id: @group_b.id
    click_link("History")
  end
end
