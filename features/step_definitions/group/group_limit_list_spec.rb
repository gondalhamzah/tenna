
Given(/^on group detail having (\d+) groups$/) do |arg1|
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @project.contact = @company_admin
  groups = Array.new
  arg1.to_i.times do |count|
 	 groups[count] = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  end
  GroupsIndex.import(group: groups)
  GroupsVersionsIndex.import(group: groups)
  #sleep 2
  visit groups_path
end

Then(/^Group should be pagination at the end of page after (\d+) items$/) do |arg1|
  if arg1 == "25"
    expect(page).to have_content("MyString", count: arg1)
  elsif arg1 == "50"
    find('#candice-cobb-s-route > div.row.margin-bottom-0.mt-mn > div.col.s6.m6.l8.right-align > div > div.show-box.filter-item > form > div > input').click
		page.find(".select-dropdown.active li", text: '50', exact: true).click
		sleep 2
    expect(page).to have_content("MyString", count: arg1)
	elsif arg1 == "100"
    find('#candice-cobb-s-route > div.row.margin-bottom-0.mt-mn > div.col.s6.m6.l8.right-align > div > div.show-box.filter-item > form > div > input').click
    page.find(".select-dropdown.active li", text: '100', exact: true).click
    sleep 2
    expect(page).to have_content("MyString", count: arg1)
	end
end


Then(/^there should be pagination on page$/) do
  page.find(".pagination", text: '1', exact: true)
end
