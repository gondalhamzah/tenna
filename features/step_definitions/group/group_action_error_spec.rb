Given(/^User is login$/) do
  Chewy.strategy(:bypass) 
  @company_admin = FactoryGirl.create(:company_admin)
  project = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  @company_admin.company.project_id = project.id
  @company_admin.company.save
  visit new_user_session_path
  login @company_admin
end

Given(/^on group detail page perform a action$/) do
  groups_page
  sleep 2
  click_link 'Actions'
  sleep 1
  find(:xpath, "//*[@id='group-action-dropdown']/li[1]/span").click
  sleep 1
end

Then(/^it should throw an error$/) do
  if ENV["BROWSER_MODE"] == true
    expect(page).to have_content("Please Select The Groups To Perform This Action On.")
  end
end
