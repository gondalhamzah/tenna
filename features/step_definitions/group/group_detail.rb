Then(/^click on assets template$/) do
  find(".project-detail-links a", text: "Assets Template").click
end

Then(/^click on import asset$/) do
  find("#modal-upload-spreadsheet-open").click
end

Then(/^verify import assets popup opened$/) do
  expect(page).to have_content("Import Assets")
  find(:css, "#modal-upload-spreadsheet").present?
end

Then(/^attach asset template csv$/) do
  if ENV['BROWSER_MODE'] == "true"
    within '.file-field.input-field' do
      page.attach_file 'csv_import_file', File.join(Rails.root, 'tmp', 'downloads', 'inventory_capture_sheet.csv'), :visible => false
    end
  else
    puts "Needs to tested open browser"
  end
end

Given(/^attach site template csv$/) do
  if ENV['BROWSER_MODE'] == "true"
    within '.file-field.input-field' do
      page.attach_file 'file', File.join(Rails.root, 'tmp', 'downloads', 'projects_inventory_capture_sheet.csv'), :visible => false
    end
  else
    puts "Needs to tested open browser"
  end
end

Given(/^fill group search bar$/) do
  find("#group-nav-search").set("something \n")
end

Given(/^click group remove icon$/) do
  find(:xpath,'//*[@id="group-nav-search-clear"]').click
end

Then(/^group enter key press$/) do
  find("#group-nav-search").set("\n")
end
