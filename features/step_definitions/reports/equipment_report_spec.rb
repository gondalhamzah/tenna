

Given(/^select equipment select all the option of asset catagory$/) do
	#sleep 2
end

Given(/^select equipment Select Multiple Asset categories$/) do
  asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
  asset2 = FactoryGirl.create(:asset, company: @company_admin.company)
  cat1 = FactoryGirl.create(:category, category_type: "equipment")
  asset2.type = "Material"
  asset1.category = cat1
  asset1.save
  asset2.save
  AssetsIndex.import(asset: asset1)
  AssetsIndex.import(asset: asset2)
  AssetsVersionsIndex.import(asset: asset1)
  AssetsVersionsIndex.import(asset: asset2)
  if ENV["BROWSER_MODE"] == "true"
    report_page
    click_on "Asset Location Audit"
    #sleep 2
    find(:xpath, "//*[@id='new_report_assets_search']/div[1]/div[2]/div[1]/div[3]/div/label").click
    #sleep 1
    expect('#report_assets_search_asset_categories').not_to have_content(asset2.category.name)
    names = Array.new(1)
    names[0] = asset1.category.name
    select_catagory_for_report "All category", names
  else
    puts "Needs to be tested open browser"
  end
end