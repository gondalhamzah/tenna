
Given(/^select material Select Multiple Asset categories$/) do
  asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
  asset2 = FactoryGirl.create(:asset, company: @company_admin.company)
  asset2.type = "Material"
  asset1.type = "Equipment"
  asset1.category.category_type = "Equipment"
  asset2.category.category_type = "Material"
  asset1.save
  asset2.save
  AssetsIndex.import(asset: asset1)
  AssetsIndex.import(asset: asset2)
  AssetsVersionsIndex.import(asset: asset1)
  AssetsVersionsIndex.import(asset: asset2)
  if ENV["BROWSER_MODE"] == "true"
    sleep 1
    report_page
    sleep 1
    click_on "Asset Location Audit"
    #sleep 2
    find(:xpath, "//*[@id='new_report_assets_search']/div[1]/div[2]/div[1]/div[2]/div/label").click
    #sleep 1
    expect('#report_assets_search_asset_categories').not_to have_content(asset1.category.name)
    names = Array.new(1)
    names[0] = asset2.category.name
    select_catagory_for_report "All Sites", names
  else
    puts "Needs to be tested open browser"
  end
end