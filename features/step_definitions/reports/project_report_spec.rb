

Given(/^Select Project Multiple project categories$/) do
  project1 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  project2 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  project3 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  project4 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  ProjectsIndex.import(project: project1)
  ProjectsIndex.import(project: project2)
  ProjectsIndex.import(project: project3)
  ProjectsIndex.import(project: project4)
  if ENV["BROWSER_MODE"] == "true"
    report_page
    click_on "Asset Location Audit"
    #sleep 2
    find(:xpath, "//*[@id='new_report_assets_search']/div[1]/div[2]/div[2]/div[1]/div/label").click
    #sleep 1
    names = Array.new(3)
    names[0] = project1.name
    names[1] = project4.name
    #sleep 2
  else
    puts "Needs to be tested open browser"
  end
end

Given(/^Select all project option form dropdown$/) do
  project1 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  project2 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  project3 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  project4 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  ProjectsIndex.import(project: project1)
  ProjectsIndex.import(project: project2)
  ProjectsIndex.import(project: project3)
  ProjectsIndex.import(project: project4)
  if ENV["BROWSER_MODE"] == "true"
    report_page
    click_on "Asset Location Audit"
    #sleep 2
    find(:xpath, "//*[@id='new_report_assets_search']/div[1]/div[2]/div[2]/div[1]/div/label").click
    #sleep 1
    names = Array.new(3)
    select_project_for_report "All project option", names
    #sleep 2
  else
    puts "Needs to be tested open browser"
  end
end

Given(/^Select some projects and all Project from option$/) do
  project1 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  project2 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  project3 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  project4 = FactoryGirl.create(:project, contact: @company_admin, company: @company_admin.company)
  ProjectsIndex.import(project: project1)
  ProjectsIndex.import(project: project2)
  ProjectsIndex.import(project: project3)
  ProjectsIndex.import(project: project4)
  if ENV["BROWSER_MODE"] == "true"
    report_page
    click_on "Asset Location Audit"
    #sleep 2
    find(:xpath, "//*[@id='new_report_assets_search']/div[1]/div[2]/div[2]/div[1]/div/label").click
    #sleep 1
    names = Array.new(1)
    names[0] = project3.name
    select_project_for_report "All project option", names
    #sleep 2
  else
    puts "Needs to be tested open browser"
  end
end