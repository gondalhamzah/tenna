
Given(/^on Report page under project utilization tab$/) do
  if ENV["BROWSER_MODE"] == "true"
    report_page
  end
end

Given(/^select from date greater than to date$/) do
  if ENV["BROWSER_MODE"] == "true"
	  first(".date-range.datepicker.report-date.picker__input").click
	  #sleep 2
  else
    puts "Needs to be tested open browser"
  end
end

Given(/^select from date greater than from date$/) do
  if ENV["BROWSER_MODE"] == "true"
	  all(".date-range.datepicker.report-date.picker__input")[1].click
	  #sleep 2
  else
    puts "Needs to be tested open browser"
  end
end

Then(/^It shouldn't allow to select from date greater than to date$/) do
  
end