Given(/^on Report page under asset location tab$/) do
  report_page
  click_on "Asset Location Audit"
end

Given(/^select all the option of asset catagory$/) do
	sleep 2
end

Then(/^It select all options correctly$/) do
end

Given(/^Select Multiple Asset categories$/) do
  if ENV["BROWSER_MODE"] == "true"
    asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
    asset2 = FactoryGirl.create(:asset, company: @company_admin.company)
    AssetsIndex.import(asset: asset1)
    AssetsIndex.import(asset: asset2)
    AssetsVersionsIndex.import(asset: asset1)
    AssetsVersionsIndex.import(asset: asset2)
    sleep 1
    report_page
    byebug
    click_on "Asset Location Audit"
    names = Array.new(2)
    names[0] = asset1.category.name
    names[1] = asset2.category.name
    select_catagory_for_report "All category", names
  else
    puts "Needs to be tested open browser"
  end
end

Given(/^on Reports page under asset location tab$/) do
end

Given(/^Generate Asset Location Audit report$/) do
  if ENV["BROWSER_MODE"] == "true"
    click_on "Create Report"
  else
    puts "Needs to be tested open browser"
  end
end

Then(/^Report will be generated only for the selected asset categories$/) do
end

Given(/^Select all Asset categories$/) do
  asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
  asset2 = FactoryGirl.create(:asset, company: @company_admin.company)
  AssetsIndex.import(asset: asset1)
  AssetsIndex.import(asset: asset2)
  AssetsVersionsIndex.import(asset: asset1)
  AssetsVersionsIndex.import(asset: asset2)
  if ENV["BROWSER_MODE"] == "true"
    report_page
    click_on "Asset Location Audit"
    #sleep 2
    names = Array.new(2)
    select_catagory_for_report "All category option", names
  else
    puts "Needs to be tested open browser"
  end
end


Given(/^Select some Asset categories$/) do
  asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
  asset2 = FactoryGirl.create(:asset, company: @company_admin.company)
  AssetsIndex.import(asset: asset1)
  AssetsIndex.import(asset: asset2)
  AssetsVersionsIndex.import(asset: asset1)
  AssetsVersionsIndex.import(asset: asset2)
  if ENV["BROWSER_MODE"] == "true"
    report_page
    click_on "Asset Location Audit"
    selectbox = page.first('#report_assets_search_asset_categories')
    first(".select2-selection__rendered").click
    selectbox.select asset2.category.name
    first(".select2-selection__rendered").click
  else
    puts "Needs to be tested open browser"
  end
end

Given(/^Select some Asset categories and all categories from option$/) do
  asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
  asset2 = FactoryGirl.create(:asset, company: @company_admin.company)
  AssetsIndex.import(asset: asset1)
  AssetsIndex.import(asset: asset2)
  AssetsVersionsIndex.import(asset: asset1)
  AssetsVersionsIndex.import(asset: asset2)
  if ENV["BROWSER_MODE"] == "true"
    report_page
    click_on "Asset Location Audit"
    names = Array.new(2)
    select_catagory_for_report "All category option", names
    selectbox = page.first('#report_assets_search_asset_categories')
    first(".select2-selection__rendered").click
    first(".select2-selection__rendered").click
    first(".select2-selection__rendered").click
  else
    puts "Needs to be tested open browser"
  end
end

Then(/^there should error to "([^"]*)"$/) do |arg1|
  expect(page).to have_content(arg1)
end

Given(/^visit reports page$/) do
  AnalyticFilter.create(company_id: Company.last.id,  user_id: User.last.id, name: "asset reports filter")
  visit asset_reports_path
end

Given(/^enable assets\-reports subscription$/) do
  Subscription.create([{company_id: Company.last.id, category: 'asset reports', state: 'active', payment: true}])
end

Given(/^verify filter creation and deletion$/) do
  if ENV["BROWSER_MODE"] == "true"
    find(:xpath, "//*[@id='asset-reports-index']/main/div/ul/li[3]/a").click
    expect(page).to have_content('asset reports filter')
    AnalyticFilter.last.delete
    visit asset_reports_path
    find(:xpath, "//*[@id='asset-reports-index']/main/div/ul/li[3]/a").click
    expect(page).to have_no_content('asset reports filter')
  else
    puts "Test in Open Browser"
  end
end
