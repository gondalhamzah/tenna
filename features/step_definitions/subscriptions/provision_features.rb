Given(/^check all unsubscribed feature notifications$/) do
  visit path_finder_index_path
  expect(page).to have_content('Routing is an Optional, Premium Feature.')

  visit manage_routes_path_finder_index_path
  expect(page).to have_content('Routing is an Optional, Premium Feature.')

  visit findit_checklist_path
  expect(page).to have_content('Find It from Tenna is an optional Premium feature...')

  visit assets_findit_path
  expect(page).to have_content('Find It from Tenna is an optional Premium feature...')

  visit event_index_path
  expect(page).to have_content('Schedule It is an optional Premium selection...')

  visit asset_reports_path
  expect(page).to have_content('Asset Reports it is a Premium service...')
  puts 'live data is being implemented yet to check unsubscribed features'
end

Given(/^check all subscribed feature notifications$/) do
  Subscription.create([{company_id: Company.last.id, category: 'asset reports',state: 'active',payment: true}])
  visit asset_reports_path
  fill_in 'asset_filter_name', with: 'Some Title Here'

  Subscription.create([{company_id: Company.last.id, category: 'findit', state: 'active', payment: true}])
  visit assets_findit_path
  expect(page).to have_content('Inventory')

  Subscription.create([{company_id: Company.last.id, category: 'add tracking to asset', state: 'active', payment: true}])
  visit findit_checklist_path
  expect(page).to have_content('Checklist')

  @subscription = Subscription.new(company: @company_admin.company, api_key: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTMwNmZjMmYxNzM5OWFmMGEzZjgyZDIiLCJpYXQiOjE0OTYzNDY1NjJ9.IDaX4fwp4Q8eGVy55KbNdhUGTT18xgYlj05OtdwY5lc", api_key_created_at: "Wed, 17 May 2017 11:26:38 UTC +00:00",
  api_key_updated_at: "Wed, 17 May 2017 11:26:38 UTC +00:00",
  category: "routing",
  number_of_units: 7,
  state: "active",
  note: "Sultan Routing Note here.",
  deleted_at: nil,
  vendor_id: "59247775c4a6fdad0a6d75bf",
  created_at: "Fri, 19 May 2017 14:19:57 UTC 00:00",
  updated_at: "Fri, 19 May 2017 14:19:57 UTC 00:00")
  @subscription.save!
  Subscription.create([{company_id: Company.last.id, category: 'routing', state: 'active', payment: true}])

  visit path_finder_index_path
  expect(page).to have_content('Select Drivers')
  Subscription.create([{company_id: Company.last.id, category: 'scheduleit', state: 'active', payment: true}])

  visit manage_routes_path_finder_index_path
  sleep 2
  expect(page).to have_content('Manage Routes')

  visit event_index_path
  expect(page).to have_content('Today')

  puts 'live data is being implemented yet to check subscribed features'
end