Then(/^user goes to edit profile menu$/) do
  sleep 2
  click_link('menu')
  sleep 2
  click_link('My Profile')
  sleep 2
  click_link('Edit')
end

Then(/^user can change their own password$/) do
  expect(page).to have_content('Password')
  expect(page).to have_content("Leave it blank if you don't want to change it")
  expect(page).to have_content('Password Confirmation')
  find(:xpath,'//*[@id="user_password"]').set('password2')
  find(:xpath,'//*[@id="user_password_confirmation"]').set('password2')
  find(:xpath,'//*[@id="user_current_password"]').set('password1')
  find(:xpath,'//*[@id="edit_user"]/div[2]/button').click
  sleep 1
  expect(page).to have_content("Your account has been updated successfully")
end

Then(/^user can logout and log in again as "([^"]*)" with updated password$/) do |end_user|
  #to logout
  find(:xpath,'//*[@id="user-info"]/div/div/div[3]/div/a').click
  sleep 2
  #to login again
  if end_user == "Company Admin"
    visit new_user_session_path
    fill_in 'Email', :with => "#{@company_admin.email}"
    fill_in 'Password', :with => "password2"
    find(:xpath,'//*[@id="new_user"]/div[2]/button').click #sign in again
    visit company_user_path(company_id: Company.last.id, id: User.last.id)
    find(:xpath,'//*[@id="user-info"]/div/div/div[3]/div/a').click
  elsif end_user == "Leader"
    visit new_user_session_path
    fill_in 'Email', :with => "#{@company_user.email}"
    fill_in 'Password', :with => "password2"
    find(:xpath,'//*[@id="new_user"]/div[2]/button').click #sign in again
    visit company_user_path(company_id: Company.last.id, id: User.last.id)
    find(:xpath,'//*[@id="user-info"]/div/div/div[3]/div/a').click
  elsif end_user == "Member"
    visit new_user_session_path
    fill_in 'Email', :with => "#{@company_user.email}"
    fill_in 'Password', :with => "password2"
    find(:xpath,'//*[@id="new_user"]/div[2]/button').click #sign in again
    visit company_user_path(company_id: Company.last.id, id: User.last.id)
    find(:xpath,'//*[@id="user-info"]/div/div/div[3]/div/a').click
  elsif end_user == "Field"
    # visit new_user_session_path
    # fill_in 'Email', :with => "#{@field_member.email}"
    # fill_in 'Password', :with => "password2"
    # find(:xpath,'//*[@id="new_user"]/div[2]/button').click #sign in again
    # visit company_user_path(company_id: Company.last.id, id: User.last.id)
    # sleep 5
    # find(:xpath, "//*[@id='user-info']/div/div/div[2]/div[2]/a").click
  elsif end_user == "Company User"
    # visit new_user_session_path
    # fill_in 'Email', :with => "#{@company_user.email}"
    # fill_in 'Password', :with => "password2"
    # find(:xpath,'//*[@id="new_user"]/div[2]/button').click #sign in again
    # visit company_user_path(company_id: Company.last.id, id: User.last.id)
    # find(:xpath, "//*[@id='user-info']/div/div/div[2]/div[2]/a").click
  end
end

Given(/^visit dashboard page$/) do
  visit root_path
end
