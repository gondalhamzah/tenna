When(/^visit company profile page$/) do
	@company_user = FactoryGirl.create(:company_user, company: @company_admin.company)
	visit company_path(id: Company.last.id)
end

Then(/^change the role of user$/) do
	begin
		find(:xpath, "//*[@id='edit_user_3']/div/input").click
		find(".dropdown-content > li:nth-child(2) > span").click
	rescue
		page.execute_script("$('.select-dropdown').trigger('click');")
		page.execute_script("$('ul.dropdown-content > li:nth-child(2) > span').trigger('click');")
	end
end

Then(/^user role should be changed$/) do
  expect(page).to have_content("Field")
end
