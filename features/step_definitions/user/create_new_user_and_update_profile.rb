Then(/^open user profile page$/) do
	visit company_user_path(company_id: Company.last.id, id: User.last.id)
end

Then(/^visit edit profile page$/) do
	visit edit_user_registration_path(id: User.last.id)
end

Then(/^Update user fields with new record$/) do
	fill_in "user_first_name", with: "John"
	fill_in "user_last_name", with: "Doe"
	fill_in "user_current_password", with: "password1"
	fill_in "user_email", with: "johndoe@test.com"
	find(:xpath, "//*[@id='edit_user']/div[2]/button").click
end

Then(/^click on the link$/) do
 	visit_in_email 'Confirm my account'
 	user = User.last
	user.reload
	expect(user).to be_confirmed
end

Then(/^visit sign\-in path and sign-in with new credentials$/) do
	visit new_user_session_path
	fill_in "user_email", with: "johndoe@test.com"
	fill_in "user_password", with: "password1"
	find(:xpath, "//*[@id='new_user']/div[2]/button").click
end