Given(/^fill vehicle capacity and project dropoff load$/) do
  find(:xpath, "//*[@id='company_drivers']/div[1]/div/span[1]/label/label").click
  sleep 2
  fill_in "user_options_2_capacity", with: "7"
  first(:xpath, "//*[@id='panel-list']/ul/li/div[1]/div/div/div[1]/div/label").click
  sleep 3
  execute_script("$('#waypoint_options_project_2_load').val('4');")
end

Then(/^verify route created and email check$/) do
  sleep 5
  begin
    click_on('Create Route')
  rescue Exception => e
    begin
      find(:xpath,'//*[@id="joe-schmoe1-s-route"]/div[2]/div[3]/div/a[1]/span').click
    rescue Exception => e
      click_on('Send Route')
    end
  end
  find(:xpath, "//*[@id='send_route_form']/div[2]/label").click
  find(:css, 'label[for="other_notification_check"]').click
  fill_in 'notify_email', with: 'a@a.com'
  expect(page).to have_no_content('Please enter valid email or phone number.')
  find("#cancel_send_route_2").click
end

When(/^create a new driver$/) do
  fill_in 'First name', with: 'Sam'
  find(:xpath, "//*[@id='user_email']").set('sam@gmail.com')
  fill_in 'Last name', with: 'Osa'
  select "Company Admin", from: "user_role"
end

When(/^browser is maximize$/) do
  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.manage.window.resize_to(1270, 1000)
  else
    page.driver.resize(1600, 1200)
  end
end

When(/^browser is minimize$/) do
  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.manage.window.resize_to(412, 732)
  else
    page.driver.resize(1600, 1200)
  end
end

Given(/^create another company from db$/) do
  FactoryGirl.create(:company_user, company: @company_admin.company)
end

When(/^visit company detail$/) do
  visit company_path(@company_admin.company_id)
end

Then(/^change route driver$/) do
  find(:xpath, '//*[@id="company_drivers"]/div[1]/div/span[1]/label/label').click
  find(:xpath, '//*[@id="company_drivers"]/div[2]/div/span[1]/label/label').click
  begin
    find(:xpath, "//*[@id='user_options_3_capacity']").set("5")
  rescue
    page.execute_script("$('#user_options_3_capacity').val('5').text('5');")
  end
end

Then(/^click on first text "([^"]*)"$/) do |arg1|
  execute_script("$('#users_panel > div:nth-child(5) > button').trigger('click')")
  # click_on(arg1, :match => :first)
end

When(/^change route name$/) do
  fill_in 'Route Name', with: 'Editing Route Name'
end

Then(/^change vehicle capacity$/) do
  find(:xpath, "//*[@id='user_options_3_capacity']").set("15")
end

Then(/^change start and end time$/) do
  fill_in 'user_options_3_start_time', with: '9:30'
  fill_in 'user_options_3_end_time', with: '11:00'
  click_button "Done"
end

Then(/^start and end time should be updated$/) do
  expect(page).to have_selector("input#user_options_2_start_time[value='9:30']")
end

Then(/^change project$/) do
  begin
    find(:xpath, "//*[@id='panel-list']/ul/li[1]/div[1]/div/div/div[1]/div/label").click
  rescue
    execute_script("$('#panel-list > ul > li:nth-child(1) > div.project-header.collapsible-header.waypoint-wrapper.options-open > div > div > div.left.routing-project > div > label').trigger('click')")
  end
  begin
    find(:xpath, "//*[@id='panel-list']/ul/li[2]/div[1]/div/div/div[1]/div/label").click
  rescue
    execute_script("$('#panel-list > ul > li:nth-child(2) > div.project-header.collapsible-header.waypoint-wrapper > div > div > div.left.routing-project > div > label').trigger('click')")
  end
  execute_script("$('#waypoint_options_project_1_load').val('4');")
end

Then(/^change pickup$/) do
  execute_script("$('#waypoint_options_project_2_load').val('4');")
end

Then(/^click on manage routes$/) do
  begin
    find(:xpath, '//*[@id="routing-tabs"]/li[2]/a').click
  rescue
    begin
      find('#routing-tabs > li:nth-child(2) > a').click
    rescue
      execute_script("$('#routing-tabs > li:nth-child(2) > a').trigger('click');")
    end
  end
end

When(/^limit exceed skip$/) do
  if page.has_content?('A route could not be generated with the given constraints')
    skip_this_scenario
  end
end

Then(/^select a driver$/) do
  find(:xpath, "//*[@id='company_drivers']/div[1]/div/span[1]/label/label").click
end

When(/^vehicle capacity and dropoff are not valid$/) do
  find(:xpath, "//*[@id='company_drivers']/div[1]/div/span[1]/label/label").click
  sleep 2
  page.execute_script("$('#user_options_2_capacity').val('5').text('5')")
  first(:xpath, "//*[@id='panel-list']/ul/li/div[1]/div/div/div[1]/div/label").click
  sleep 3
  execute_script("$('#waypoint_options_project_2_load').val('8');")
end

Then(/^route should be created with "([^"]*)"$/) do |arg1|
  sleep 5
  if Route.last.name.include? arg1
    puts "Route has been created successfully"
  else
    puts "Sorry, created route has some bugs"
  end
end

Then(/^change start and end time with settings$/) do
  fill_in 'user_options_2_start_time', with: '9:30'
  fill_in 'user_options_2_end_time', with: '11:00'
  click_button "Done"
end

Given(/^click on menu icon$/) do
  find('#desktop_main_vertical_nav_btn').click
end

Then(/^filters should be disabled$/) do
  expect(page).to have_content("Filtering is disabled when editing routes.")
end

Then(/^close popup$/) do
  find(:xpath, "//*[@id='prelaunch-popup']/div/div/div[3]/a").click
end

Then(/^find text "([^"]*)"$/) do |arg1|
  expect(page).to have_content(arg1)
end

Given(/^enable schedule\-it subscription$/) do
  Subscription.create([{company_id: Company.last.id, category: 'scheduleit', state: 'active', payment: true}])
end

Given(/^disable schedule\-it subscription$/) do
  s = Subscription.last
  s.state = 'suspended'
  s.save
  visit event_index_path
  # Subscription.create([{company_id: Company.last.id, category: 'scheduleit', state: 'active', payment: true}])
end

Given(/^visit asset details page$/) do
  visit edit_asset_path id: Asset.last.id
  click_on 'Schedule Maintenance'
  sleep 2
  page.execute_script("$('.select2-selection__rendered').trigger('click')")
  first('.select2-results__option.select2-results__option--highlighted').click
  find(:xpath, "//*[@id='events-index']/div[1]/div[3]/div[2]").click
  expect(page.driver.browser.switch_to.alert.text).to eq("Please select a time in the future.")
  page.driver.browser.switch_to.alert.accept
  page.execute_script("$('.dhx_section_time select:last-child').val('2018')")
  page.execute_script("$('.dhx_section_time select:first-child').val('1410')")
  find(:xpath, "//*[@id='events-index']/div[1]/div[3]/div[2]").click
  sleep 2
end

Then(/^verify email check for assset maintainance$/) do
  begin
    SendEventNotificationJob.perform_now(Event.last.id, true)
  rescue Exception => e
    if ActionMailer::Base.deliveries.last.to[0] == User.last.email
      puts 'email delivered'
    else
      puts 'email not delivered'
    end
  end
end

Then(/^verify routing feature is unsubscribed$/) do
  expect(page).to have_content('Routing is an Optional, Premium Feature.')
end

Then(/^verify routing feature is subscribed$/) do
  expect(page).to have_no_content('Routing is an Optional, Premium Feature')
end
