Given(/^create subscription and visit routing$/) do
  create_subscription
  visit new_user_session_path
  login @company_admin
end

Given(/^add sleep for "([^"]*)" seconds$/) do |arg1|
  sleep arg1.to_i
end

Given(/^visit path finder page$/) do
  visit path_finder_index_path
end

Given(/^select vehicle capacity and project load$/) do
  find(:xpath, "//*[@id='company_drivers']/div[1]/div/span[1]/label/label").click
  find(:xpath, "//*[@id='user_options_2_capacity']").set("5")
  first(:xpath, "//*[@id='panel-list']/ul/li/div[1]/div/div/div[1]/div/label").click
  execute_script("$('.routing-load-slider-value').val('12');")
end

Then(/^click on create route$/) do
  sleep 5
  find(:xpath, "//*[@id='path_finder_form']/div[2]/button").click
end
