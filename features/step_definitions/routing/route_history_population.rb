Then(/^send route$/) do
  page.execute_script("$('#send_notification_submit').trigger('click')")
end

Then(/^open new window to send route$/) do
  if ENV["BROWSER_MODE"] == "true"
    find(:xpath, "//*[@id='desktop_main_vertical_nav_btn']").click
    find(:xpath, "//*[@id='desktop_main_vertical_nav_dropdown']/li[13]/a").click
    find(:xpath, "//*[@id='desktop_route_it_subnav_dropdown']/li[3]/a").click
    find(:xpath, "//*[@id='route_history_table']/tbody/tr[1]/td[4]/a[2]/i").click
    sleep 4
    new_window = window_opened_by { click_link 'Open' }
    within_window new_window do
      click_on 'Send Route'
      sleep 5
      find(:xpath, "//*[@id='send_route_form']/div[2]/label").click
      fill_in 'notify_email', with: 'a@a.com'
      expect(page).to have_no_content('Please enter valid email.')
      # fill_in "notify_phone", with: "+14539886242"
      page.execute_script("$('#send_notification_submit').trigger('click')")
    end
  end
end

Then(/^click on text "([^"]*)" with settings$/) do |arg1|
  page.execute_script("$(\'a[href=\"#route-history\"]').trigger('click')")
end


Then(/^click on checkbox with settings$/) do
  begin
    find(:xpath, "//*[@id='send_route_form']/div[2]/label").click
    find(:xpath, "//*[@id='notify_email']").set("a@a.com")
  rescue
    begin
      find("#send_route_form > div:nth-child(3) > label").click
      find(:xpath, "//*[@id='notify_email']").set("a@a.com")
    rescue
      page.execute_script("$('#other_notification_check').prop('checked', true);$('#notify_email').val('a@a.com')")
    end
  end
end

Then(/^goto path finder page$/) do
  visit path_finder_index_path
end

Then(/^check if mail has been sent succesfully$/) do
  sleep 2
  expect(page).to have_content("Notification sent successfully.")
end

Then(/^click on text Send Route$/) do
  begin
    within ".send-notification" do
      click_on "Send Route"
    end
    sleep 5
  rescue
    page.execute_script("$('a[href=\"#notification_modal\"]').trigger('click')")
    sleep 5
  end
end


Then(/^check if notification has been sent$/) do
  if ENV["BROWSER_MODE"] == "true"
    expect(page).to have_content("Notification sent successfully.")
  end
end

Then(/^click on text View with settings$/) do
  begin
    sleep 5
    find(:xpath, "//*[@id='route_history_table']/tbody/tr[2]/td[4]/a[2]").click
  rescue
    page.execute_script("$('#route_history_table > tbody > tr:nth-child(1) > td:nth-child(4) > a.preview-data').click()")
  end
end

Then(/^click on text View with settings second$/) do
  begin
    sleep 5
    find(:xpath, "//*[@id='route_history_table']/tbody/tr[1]/td[4]/a[2]").click
  rescue
    page.execute_script("$('#route_history_table > tbody > tr:nth-child(1) > td:nth-child(4) > a.preview-data').click()")
  end
end
