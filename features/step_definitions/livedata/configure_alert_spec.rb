Given(/^enable subscription$/) do
  Subscription.create([{company_id: Company.last.id, category: 'livedata', state: 'active', payment: true}])
  create_guage
end

Given(/^click on live data button$/) do
  find(:css,'#map-wrapper > a').click
end

Given(/^click on configure alert$/) do
  find(:css,'#live-data-index > main > div > div > div > div > div > div > div.liveDataNav > div.liveDataNav__mainNav > div > a:nth-child(2)').click
end

Given(/^click on bell icon$/) do
  find(:css,'#live-data-index > main > div > div > div > div > div > div > div.row > div.col.l3.m12.s12 > div > div.caFilterGroup > div > i.fa.caFilterGroup__editIcon.fa-bell').click
end

Given(/^click on add live data button$/) do
  find(:css,'#live-data-index > main > div > div > div > div > div > div > div.row > div.col.l9.m12.s12 > div > div > div.caGaugeList > div.caGaugeList__container > div > div > div > a').click
end

Given(/^choose add alert$/) do
  find(:css,'#modal_5 > div.modal-content > div > div > div > div:nth-child(1) > div > img').click
end

Then(/^click on add live data button in popup$/) do
  find(:css,'#modal_5 > div.modal-footer > button.btn.waves-effect.waves-light.btn-flat.modal-action.modal-close.but1').click
end

Then(/^click on guage button$/) do
  find(:css,'#live-data-index > main > div > div > div > div > div > div > div.row > div.col.l9.m12.s12 > div > div > div.caGaugeList > div.caGaugeList__container > div.caGaugeList__list > div > div > div > div > div > img').click
end

Then(/^click on note$/) do
  find(:css,'#live-data-index > main > div > div > div > div > div > div > div.row > div.col.l9.m12.s12 > div > div > div:nth-child(2) > div.row.liveDataAlerts > div > div.liveDataAlerts__tabContent.liveDataAlerts__tabContent--active > div > div.row > div.row.idleHourAlert__subNav.padding-left-5 > div > ul > li:nth-child(1) > a > i').click
  fill_in "input_7", with: 'something'
end

Then(/^select company contact$/) do
  find(:css,'#live-data-index > main > div > div > div > div > div > div > div.row > div.col.l9.m12.s12 > div > div > div:nth-child(2) > div.row.liveDataAlerts > div > div.liveDataAlerts__tabContent.liveDataAlerts__tabContent--active > div > div.row > div.row.idleHourAlert__subNav.padding-left-5 > div > ul > li:nth-child(2) > a > i').click
  find(:xpath,'//*[@id="live-data-index"]/main/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[1]/div/div[1]/div/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div[1]/div/div').click
  within ".Select-menu-outer" do
    find(:css, '.Select-menu .Select-option', text: "Joe Schmoe1").click
  end
end

Then(/^enter email$/) do
  find(:xpath,'//*[@id="live-data-index"]/main/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[1]/div/div[1]/div/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/label').click
end

Then(/^enter phonenumber$/) do
  find(:xpath,'//*[@id="live-data-index"]/main/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[1]/div/div[1]/div/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div/label').click
end

Then(/^click on save button$/) do
  find(:css,'#live-data-index > main > div > div > div > div > div > div > div.row > div.col.l9.m12.s12 > div > div > div:nth-child(2) > div:nth-child(2) > a.waves-effect.waves-light.btn.modal-close.but1.liveDataAlerts__saveButton').click
end
