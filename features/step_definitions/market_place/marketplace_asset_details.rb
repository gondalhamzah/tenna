Given(/^user is viewing Public Marketplace$/) do
  visit marketplace_path
end

Given(/^all types of assets are created as "([^"]*)"$/) do |person|
  if person == "Field"
    @company_admin = FactoryGirl.create(:company_admin, company: @field_member.company)
  elsif person == "User"
    @company_admin = FactoryGirl.create(:company_admin, company: @company_user.company)
  end
  create_four_assets(@company_admin)
end

Then(/^asset details can be viewed for each asset$/) do
  visit assets_path
  find(:xpath, "//*[@id='asset_1']/td[3]/a/div/div[1]/div[4]").click
  sleep 2
  expect(page).to have_content('Rental')
  visit assets_path
  find(:xpath,"//*[@id='asset_2']/td[3]/a/div/div[1]/div[4]").click
  sleep 2
  expect(page).to have_content('Owned')
  visit assets_path
  find(:xpath,"//*[@id='asset_3']/td[3]/a/div/div[1]/div[4]").click
  sleep 2
  expect(page).to have_content('For Sale')
  visit assets_path
  find(:xpath,"//*[@id='asset_4']/td[3]/a/div/div[1]/div[4]").click
  sleep 2
  expect(page).to have_content('For Rent')
end
