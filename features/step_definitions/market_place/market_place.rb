
Given(/^goto market place without login$/) do
  visit marketplace_path
  #sleep 2
end

Then(/^there should be (\d+) asset$/) do |arg1|
  expect(page).to have_css("table#table-assets tr", :count=>arg1.to_i)
end
Given(/^Login as "([^"]*)" without clear$/) do |arg1|
  Chewy.strategy(:bypass)
  if arg1 == "Public User"
    @public_user = create_public_user
    visit new_user_session_path
    sleep 2
    login @public_user
  elsif arg1 == "Company Admin"
    @company_admin = FactoryGirl.create(:company_admin)
    @project = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
    @company_admin.company.project_id = @project.id
    @company_admin.company.save
    # category group
    @category_group = FactoryGirl.create(:category_group)
    @category1 = FactoryGirl.create(:category, :category_type => 'equipment', category_group_id: @category_group.id)
    @category2 = FactoryGirl.create(:category, :category_type => 'material', category_group_id: @category_group.id)
    # category group end
    sleep 2
    visit new_user_session_path
    sleep 2

    login @company_admin
  elsif arg1 == "Company User"
    login_as_company_user
  elsif arg1 == "Project Leader"
    login_as_leader
  elsif arg1 == "Project Member"
    login_as_member
  elsif arg1 == "Field"
    login_as_field_member
  else

  end
end

Given(/^Login as "([^"]*)"$/) do |arg1|
  DatabaseCleaner.clean
  Chewy.strategy(:bypass)
  if arg1 == "Public User"
    @public_user = create_public_user
    visit new_user_session_path
    sleep 2
    login @public_user
  elsif arg1 == "Company Admin"
    @company_admin = FactoryGirl.create(:company_admin)
    @company_user = FactoryGirl.create(:company_user, company: @company_admin.company)
    @project = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
    @company_admin.company.project_id = @project.id
    @company_admin.company.save
    ProjectsIndex.import(project: @project)
    @category_group = FactoryGirl.create(:category_group)
    @category1 = FactoryGirl.create(:category, :category_type => 'equipment', category_group_id: @category_group.id)
    @category2 = FactoryGirl.create(:category, :category_type => 'material', category_group_id: @category_group.id)
    sleep 2
    visit new_user_session_path
    sleep 2
    login @company_admin
  elsif arg1 == "Company User"
    login_as_company_user
  elsif arg1 == "Project Leader"
    login_as_leader
  elsif arg1 == "Project Member"
    login_as_member
  elsif arg1 == "Field"
    login_as_field_member
  end
end

Given(/^create (\d+) assets for "([^"]*)"$/) do |arg1, arg2|
  if arg2 == "Public users"
    create_for_rent_asset_for_public_user @public_user
    create_for_sale_asset_for_public_user @public_user
  elsif arg2 == "Company Admin"
    create_four_assets @company_admin
  elsif arg2 == "Company User"
    create_four_assets @company_user
  elsif arg2 == "Project Leader"
    create_four_assets @company_user
  elsif arg2 == "Project Member"
    create_four_assets @company_user
  elsif arg2 == "Field"
    create_four_assets @field_member
  end
end

Given(/^goto market place for "([^"]*)"$/) do |arg1|
  visit marketplace_path
end

Given(/^goto to assets page mark asset mark owned asset as public for "([^"]*)"$/) do |arg1|
  visit assets_path
  if ENV["BROWSER_MODE"] == "true"
    if arg1 == "Company Admin"
      find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
    elsif arg1 == "Project Leader"
      find(:xpath, '//*[@id="asset_12"]/td[1]/label').click
    end
    sleep 2
    click_on "Actions"
    within '#action-dropdown' do
      click_on "Mark as Public"
    end
    sleep 2
    expect(page).to have_content("Successfully updated 1 asset.");
  else
    @owned_asset1.public = true
    @owned_asset1.save
    AssetsIndex.import(asset: @owned_asset1)
    AssetsVersionsIndex.import(asset: @owned_asset1)
  end
end

Given(/^mark rental asset as public then there should be error for "([^"]*)"$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    if arg1 == "Company Admin"
      find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
    elsif arg1 == "Project Leader"
      find(:xpath, '//*[@id="asset_11"]/td[1]/label').click
    elsif arg1 == "Field"
      find(:xpath, '//*[@id="asset_15"]/td[1]/label').click
    end
    click_on "Actions"
    within '#action-dropdown' do
      click_on "Mark as Public"
    end
    expect(page).not_to have_content("Successfully updated 1 asset.");
  end
end

Then(/^logout from "([^"]*)"$/) do |arg1|
  if arg1 == "Public User"
    visit user_profile_path
    within "#user-info" do
      click_on "Log Out"
    end
  elsif arg1 == "Company Admin"
    visit company_user_path :company_id => @company_admin.company.id, :id => @company_admin.id
    within "#user-info" do
      click_on "Log Out"
    end
  elsif arg1 == "Field"
    visit company_user_path :company_id => @field_member.company.id, :id => @field_member.id
    within "#user-info" do
      click_on "Log Out"
    end
  elsif arg1 == "Company User" || arg1 == "Project Leader" || arg1 == "Project Member"
    visit company_user_path :company_id => @company_user.company.id, :id => @company_user.id
    within "#user-info" do
      click_on "Log Out"
    end
  end
end

Given(/^goto to assets page there is no checkbox for user$/) do
end
