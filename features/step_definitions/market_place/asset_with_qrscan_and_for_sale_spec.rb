
Given(/^Add QR\-Code for "([^"]*)" asset$/) do |arg1|
	@category2 = FactoryGirl.create(:category, :category_type => "material")
	if arg1 == "for-rent"
		visit asset_path id: @for_rent1.id
	elsif arg1 == "for-sale"
		visit asset_path id: @for_sale1.id
	elsif arg1 == "owned"
		visit asset_path id: @owned_asset1.id
	elsif arg1 == "rental"
		visit asset_path id: @rental_asset1.id
	end
	within ".row-buttons" do
		click_on "Add Tracking"
	end
	find('#asset_token').set("ABC #{FFaker::PhoneNumber.short_phone_number}")
	find('#asset_token_type').click
	find('#asset_token_type').click
	find('#add-token-btn').click
end

When(/^Apply filter with QR\-Code$/) do
	find('#asset-management-header').click
	find(:xpath, '//*[@id="asset-mangement-filters"]/div[2]/div/label').click
  find('#asset-management-header').click
  first(:xpath, '//*[@id="new_filter_search"]/div[8]/button').click
end

Given(/^Mark owned asset as public for "([^"]*)"$/) do |arg1|
	if ENV["BOWSER_MODE"] == "true"
		find(:xpath, "//*[@id='asset_#{@owned_asset1.id}']/td[1]/label").click
		click_on "Actions"
		within '#action-dropdown' do
			click_on "Mark as Public"
		end
	else
		@owned_asset1.public = true
		@owned_asset1.save
		AssetsIndex.import(asset: @owned_asset1)
	    AssetsVersionsIndex.import(asset: @owned_asset1)
	end
end
