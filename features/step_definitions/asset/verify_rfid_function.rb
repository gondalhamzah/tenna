Then(/^go to find it page$/) do
  visit assets_findit_path
end

Then(/^go to findit checklist page$/) do
  visit findit_checklist_path
end

Then(/^scan the asset with RFID$/) do
  sleep 2
  page.execute_script("getAsset('asdf', 'tab-inventory')")
  sleep 10
end

Then(/^there should be "([^"]*)" asset in investory tab$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    sleep 5
    begin
      expect(page).to have_css("#table-assets-inventory tr", :count=>arg1)
    rescue
      puts "Asset Invertory"
    end
  end
end

Then(/^there should be "([^"]*)" list$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    begin
      expect(page).to have_css("table tr", :count=>arg1)
    rescue
      puts "Not Found"
    end
  end
end

Then(/^click on the list and it should have one asset$/) do
  if ENV["BROWSER_MODE"] == "true"
    click_on "View List"
    sleep 2
    expect(page).to have_css("#table-assets-inventory tr", :count=>1)
  end
end

Then(/^scan the asset with same RFID$/) do
  page.execute_script("getAsset('asdf', 'tab-inventory')")
  sleep 1
end

Then(/^enter a wrong RFID$/) do
    page.execute_script("getAsset('asdddf', 'tab-inventory')")
  sleep 1
end

Then(/^add a RFID code to asset$/) do
  sleep 1
  fill_in "asset_token", with: "asdf"
  click_on "Submit"
  sleep 1
end

Then(/^click on link View Saved Lists$/) do
  visit lists_path
end

Then(/^go to assets path$/) do
  visit assets_path
  sleep 1
end

Then(/^click "([^"]*)"$/) do |arg1|
    # find(:xpath, '//*[@id="save-inventory"]').click
end


Then(/^there should be "([^"]*)" assets$/) do |arg1|
  expect(page).to have_css("table#table-assets tr", :count=>arg1)
end

Then(/^goto this asset detail page of "([^"]*)" and verify it label as for\-sale$/) do |arg1|
  begin
    puts "xpath"
      find(:xpath, '//*[@id="asset_2"]/td[3]/a/p[1]').click
  rescue
    puts "asset last id"
    visit asset_path id: Asset.last.id
  end
  expect(page).to have_content("For Sale")
end

Then(/^add a "([^"]*)" RFID$/) do |arg1|
  sleep 1
  fill_in "asset_token", with: "qqqq"
  click_on "Submit"
  sleep 1
end

Then(/^scan the asset with "([^"]*)" RFID$/) do |arg1|
  page.execute_script("getAsset('qqqq', 'tab-inventory')")
  sleep 2
end

Then(/^click on the list and it should have another asset$/) do
  if ENV["BROWSER_MODE"] == "true"
    find(:xpath, '//*[@id="lists-index"]/main/div/div/div/table/tbody/tr[1]/td[2]/a').click
    sleep 2
    expect(page).to have_css("#table-assets-inventory tr", :count=>1)
  end
end

Then(/^check the asset in the save list$/) do
  begin
    puts "2nd asset label checked via xpath"
    find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
  rescue
    begin
      puts "2nd asset label checked via css"
      find("#asset_2 > td.selection-cell > label").click
    rescue
      puts "2nd asset label checked via js"
      page.execute_script("$('#asset-selected-2').prop('checked', true)")
    end
  end
end

Then(/^choose Action realocate$/) do
  if ENV["BROWSER_MODE"] == "true"
    click_on "Actions"
    click_on "Reallocate"
    begin
      sleep 5
      puts "selected via select_option"
      find(:xpath, '//*[@id="project_id"]').find(:xpath, 'option[2]').select_option
    rescue
      begin
        sleep 5
        puts "selected via select method"
        select @project2.name, :from => "project_id"
      rescue
        puts "selected via JS"
         page.execute_script("$('select#project_id option').each(function() { this.selected = (this.text == 'Test Project 2'); }).change();")
      end
    end

    begin
        within "#reallocate-asset-mass-action-inventory" do
          click_button "Reallocate"
        end
      rescue
        page.execute_script("$('#reallocate-asset-mass-action-inventory > div:nth-child(2) > div.col.s6.right-align > button').trigger('click')")
    end
  end
end

Then(/^choose Action sold$/) do
  click_on "Actions"
  click_on "Mark as Sold"
  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.switch_to.alert.accept
  else
    page.execute_script 'window.confirm = function () { return true }'
  end
end

Then(/^click on first view list and it should have another asset$/) do
  if ENV["BROWSER_MODE"] == "true"
    within "table" do
        first(:link, "View List").click
    end
    sleep 1
    expect(page).to have_css("#table-assets-inventory tr", :count=>2)
  end
end

Then(/^choose Action delete$/) do
  click_on "Actions"
  click_on "Delete"
  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.switch_to.alert.accept
  else
    page.execute_script 'window.confirm = function () { return true }'
  end
end

Then(/^check the first asset in the save list$/) do
  begin
    find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
  rescue Capybara::ElementNotFound => e
    page.execute_script("$('#asset-selected-1').prop('checked', true)")
  end

end

Then(/^sleep for "([^"]*)" second$/) do |arg1|
  sleep arg1.to_i
end

Then(/^add a third RFID code to asset$/) do
  sleep 1
    fill_in "asset_token", with: "poll"
  click_on "Submit"
  sleep 1
end

Then(/^scan third asset with "([^"]*)" RFID$/) do |arg1|
  page.execute_script("getAsset('poll', 'tab-checklist')")
  sleep 2
end

Then(/^check for sale option$/) do
  find('#asset-management-header').click
  sleep 1
  find(:xpath, '//*[@id="asset-mangement-filters"]/div[10]/div/label').click
  click_button "Apply Filters"
end

Then(/^change filter to sold$/) do
  find('#asset-management-header').click
  sleep 1
  find(:xpath, '//*[@id="asset-mangement-filters"]/div[3]/div/label').click
  find(:xpath, '//*[@id="asset-mangement-filters"]/div[10]/div/label').click
  find('#asset-management-header').click
  sleep 1
  click_button "Apply Filters"
end

Then(/^there should be "([^"]*)" asset in found table$/) do |arg1|
  expect(page).to have_css("#table-assets-checklist-found tr", :count=>arg1)
end

Then(/^there should be "([^"]*)" asset in not in filtered list table$/) do |arg1|
  expect(page).to have_css("#table-assets-checklist-not_in_list tr", :count=>arg1)
end

Then(/^enter a wrong RFID in tab\-checklist$/) do
  page.execute_script("getAsset('asdddf', 'tab-checklist')")
  sleep 1
end

Then(/^click on first saved list$/) do
  sleep 5
  begin
    find(:xpath, '//*[@id="lists-index"]/main/div/div/div/table/tbody/tr/td[2]/a').click
  rescue
    begin
      find("#lists-index > main > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > a").click
    rescue
      page.execute_script("$('#lists-index > main > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > a').trigger('click')")
    end
  end
  sleep 5
end

Then(/^there should be "([^"]*)" asset in looking for table$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    expect(page).to have_css("#table-assets-checklist-not_found tr", :count=>arg1)
  end
end

Then(/^click on button RESET$/) do
  find(:xpath, '//*[@id="new_filter_search"]/div[8]/a').click
end

Then(/^click on Asset Type$/) do
  find('#asset-type-header').click
end

Then(/^select first category$/) do
  find(:css, '#select2-filter_search_category-container').click
  find('#select2-filter_search_category-results ul li', text: @category1.name).click
end

Then(/^click on Apply Filter$/) do
  find(:xpath, '//*[@id="new_filter_search"]/div[8]/button').click
end

Then(/^check equipment option$/) do
  find(:xpath, '//*[@id="asset-type-filters"]/div[1]/div/label').click
end

Then(/^confirm a popup$/) do
  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.switch_to.alert.accept
  else
    page.execute_script 'window.confirm = function () { return true }'
  end
end

Then(/^click on button Save List$/) do
  sleep 2
  begin
    find('#save-checklist').click
  rescue
    page.execute_script("$('#save-checklist').trigger('click')")
  end
end

Then(/^click on Action drop down$/) do
  begin
    click_link "Actions"
  rescue
    page.execute_script("$('#panel-list_not-found > div.mass-action-wrapper.asset-select-wrapper.mass-action-position > form > div > a').trigger('click')")
  end
end

Then(/^click on Delete drop down$/) do
  begin
    click_link "Delete"
  rescue
    page.execute_script("$('#action-dropdownnot-found > li:nth-child(5) > a').trigger('click')")
  end
end

Then(/^add new tracking code to the asset$/) do
  click_on 'Add Tracking'
  select 'LoRa', from: "asset_token_type"
  fill_in "asset_token", with: 'some code'
  click_on 'Submit'
  sleep 2
  visit assets_path
end

Then(/^apply tracker filter should return correct asset$/) do
  find("#asset-management-header > span").click
  find("#asset-mangement-filters > div:nth-child(3) > div > label").click
  click_on 'Apply Filters'
  expect(page).to have_content('For-sale-Test-Asset')
  sleep 2

  find("#asset-management-header > span").click
  find('#asset-mangement-filters > div:nth-child(2) > div > label').click
  find("#asset-mangement-filters > div:nth-child(3) > div > label").click
  click_on 'Apply Filters'
  expect(page).to have_no_content('For-sale-Test-Asset')
  sleep 2

  find("#asset-management-header > span").click
  find("#asset-mangement-filters > div:nth-child(3) > div > label").click
  click_on 'Apply Filters'
  expect(page).to have_content('For-sale-Test-Asset')
  sleep 2
end

Given(/^enable find\-it subscription$/) do
  Subscription.create([{company_id: Company.last.id, category: 'findit', state: 'active', payment: true}])
end
