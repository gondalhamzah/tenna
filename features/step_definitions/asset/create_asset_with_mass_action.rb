Given(/^import asset template$/) do
  find(:xpath, '//*[@id="spreadsheet-links"]/a[2]/span[2]').click
end

Given(/^Fill template with Name "([^"]*)" as owned "([^"]*)" as rental "([^"]*)" as for\-rent and "([^"]*)" as for\-sale$/) do |arg1, arg2, arg3, arg4|
  fill_assets_csv_with arg1, arg2, arg3, arg4
end

Given(/^Import template$/) do
end

Then(/^There should be (\d+) assets  with Name "([^"]*)" as owned "([^"]*)" as rental "([^"]*)" as for\-rent and "([^"]*)" as for\-sale on project page$/) do |arg1, arg2, arg3, arg4, arg5|
end

Then(/^There should be (\d+) assets  with Name "([^"]*)" as owned "([^"]*)" as rental "([^"]*)" as for\-rent and "([^"]*)" as for\-sale on assets page$/) do |arg1, arg2, arg3, arg4, arg5|
end

Given(/^import asset template on groups page$/) do
  find(:xpath, "//*[@id='spreadsheet-links']/div/div[1]/div/a[3]/i").click
end

Given(/^Import template on groupspage$/) do
end

Given(/^goto groups detail page$/) do
  visit group_path id: Group.last.id
end
