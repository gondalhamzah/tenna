Given(/^"([^"]*)" is login without clear$/) do |arg1|
  if arg1 == "company_admin"
    login_as_company_admin
  elsif arg1 == "company_user"
    login_as_company_user
  end
  visit new_group_path
  sleep 3
  fill_in "Name", with: "Group 1"
  find(:xpath, '//*[@id="new_group"]/div[1]/div[1]/div[4]/div[1]/span/span[1]/span').click
  all(:css, "#select2-group_project_id-results li")[1].click
  # select @project.name, :from => 'group_project_id'
  click_on "Create Group"
  sleep 3
end

Given(/^"([^"]*)" is login$/) do |arg1|
  DatabaseCleaner.clean
  if arg1 == "company_admin"
    login_as_company_admin
  elsif arg1 == "company_user"
    login_as_company_user
  end
end

Given(/^"([^"]*)" is login for wanted assets$/) do |arg1|
  if arg1 == "company_admin"
    login_as_company_admin
  elsif arg1 == "company_user"
    login_as_company_user
  end
end

Given(/^on asset create page$/) do
  visit new_asset_path
end

Given(/^create a for\-sale asset with name "([^"]*)"$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    create_for_sale_asset arg1
  else
    for_sale = FactoryGirl.create(:asset, company: @company_admin.company, creator: @company_admin, project: @company_admin.company.project)
    for_sale.creator = @company_admin
    for_sale.project = @project
    for_sale.rental = false
    for_sale.public = true
    for_sale.owned = true
    for_sale.type = "Material"
    for_sale.save
    AssetsIndex.import(asset: for_sale)
    AssetsVersionsIndex.import(asset: for_sale)
  end
end

Given(/^create a for\-sale asset with name "([^"]*)" with canada$/) do |arg1|
  create_for_sale_asset_with_canada arg1
end

Then(/^Verify "([^"]*)" created successfully$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    sleep 6
    expect(page).to have_content(arg1)
  end
end

Then(/^goto assets list page and verify assets count increse by one$/) do
  sleep 1
  visit assets_path
  sleep 1
  if ENV["BROWSER_MODE"] == "true"
    expect(page).to have_css("table#table-assets tr", :count=>1)
  end
end

Then(/^goto asset detail page of "([^"]*)" and verify it label as for\-sale$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    for_sale?
  else
    visit asset_path :id => Asset.last.id
  end
end

Given(/^create a for\-rent asset with name "([^"]*)"$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    create_for_rent_asset arg1
  else
    FactoryGirl.create(:asset, creator: @company_admin, company: @company_admin.company, project: @company_admin.company.project)
  end
end

Then(/^goto asset detail page of "([^"]*)" and verify it label as for\-rent$/) do |arg1|
  for_rent?
end

Given(/^create a Owned asset with name "([^"]*)"$/) do |arg1|
  create_owned_asset arg1
end

Then(/^goto asset detail page of "([^"]*)" and verify it label as owned$/) do |arg1|
  find(:xpath, '//*[@id="asset_1"]/td[3]/a').click
  expect(page).to have_content('Owned')
end

Given(/^create a rental asset with name "([^"]*)"$/) do |arg1|
  create_rental_asset arg1
end

Then(/^goto asset detail page of "([^"]*)" and verify it label as rental$/) do |arg1|
  find(:xpath, '//*[@id="asset_1"]/td[3]/a').click
  expect(page).to have_content('Rental')
end

Given(/^visit category group page$/) do
  visit admin_category_groups_path
end

Then(/^fill category group data and verify creation$/) do
  click_on 'Create one'
  fill_in 'category_group_name', with: 'John Doe Category'
  click_on 'Create Category group'
  expect(page).to have_content('John Doe Category')
end

When(/^visit assets new page$/) do
  visit new_asset_path
end

When(/^verify text "([^"]*)"$/) do |arg1|
  expect(page).to have_content(arg1)
end

Then(/^check the inventory option$/) do
  find("label[for='asset_maintenance']").click
end

When(/^input asset quantity (\d+)$/) do |arg1|
  fill_in 'asset_quantity', with: arg1
end

Given(/^check maintainance checkbox$/) do
  find(:xpath, '//*[@id="new_asset"]/div[1]/div[1]/div[2]/div/span[2]/label').click
  fill_in 'Title', with: 'test'
  fill_in 'Location Zip / Postal Code', with: '66002'
  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: @project.name).click
  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: @category1.name).click
  select 'United States', from: 'asset_country'
  fill_in 'Price', with: '2100'
  fill_in 'asset_hours', with: '9'
  fill_in 'asset_miles', with: '9'
  fill_in 'asset_quantity', with: '100'
  find(:xpath, '//*[@id="new_asset"]/div[1]/div[2]/div[7]/div/label').click
  find(:xpath,'//*[@id="new_asset"]/div[2]/div/div/button').click
end

Given(/^click on create new asset$/) do
  find(:css, '#assets-index > main > div > div:nth-child(3) > div > a').click
end

Given(/^click on schedule link$/) do
  find(:css, '#new_asset > div.form-inputs.row.separator-row > div:nth-child(2) > a').click
end

Given(/^click on cross popup$/) do
  find(:css, '#un-subscribed-schedule > div > a > i').click
end

