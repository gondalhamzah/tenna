Then(/^user creates an asset with name "([^"]*)"$/) do |arg1|
  visit new_asset_path
  create_owned_asset(arg1)
end

Given(/^a new category is created$/) do
  @category1 = FactoryGirl.create(:category, category_type: 'equipment')
end

Then(/^"([^"]*)" is visible on Assets page$/) do |arg1|
  visit assets_path
  sleep 2
  expect(page).to have_content(arg1)
end

Then(/^user can also delete "([^"]*)"$/) do |arg1|
  find(:xpath,'//*[@id="asset_1"]/td[1]/label').click #selects all assets
  sleep 2
  find(:xpath,'//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
  sleep 2
  find('#action-dropdown li:last-child a').click
  sleep 2
  # accept alert box
  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.switch_to.alert.accept
  else
    page.execute_script 'window.confirm = function () { return true }'
  end
  visit assets_path
  expect(page).to have_no_content(arg1)
end

Then(/^user cannot delete "([^"]*)"$/) do |arg1|
  find(:xpath,'//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
  sleep 2
  expect(page).to have_no_content('Delete')
end

Then(/^adding asset throws error$/) do
  visit new_asset_path
  expect(page).to have_content('You are not authorized to access this page.')
end
