Then(/^click on Apply Filters$/) do
  click_button "Apply Filters"
end

Then(/^I should not see mark as fullfilled in actions dropdown$/) do
  expect(page).to have_no_xpath('//*[@id="action-dropdown"]/li[1]/a')
end

Then(/^select a category$/) do
  find(:css, '#select2-filter_search_category-container').click
  find('#select2-filter_search_category-results ul li', text: @category1.name).click
end

Then(/^select a project$/) do
  find(:css, '#select2-filter_search_project-container').click
  find('#select2-filter_search_project-results li', text: @project.name).click
end

Then(/^unselect category$/) do
  find(:css, '#select2-filter_search_category-container').click
  find('#select2-filter_search_category-results li', text: "All categories").click
end

Then(/^unselect project$/) do
  find(:css, '#select2-filter_search_project-container').click
  find('#select2-filter_search_project-results li', text: "All sites").click
end

Then(/^select another category$/) do
  find(:css, '#select2-filter_search_category-container').click
  find('#select2-filter_search_category-results ul li', text: @category2.name).click
end

Then(/^select another project$/) do
  find(:css, '#select2-filter_search_project-container').click
  find('#select2-filter_search_project-results li', text: @project2.name).click
end

Then(/^select another project for leader$/) do
  find(:css, '#select2-filter_search_project-container').click
  find('#select2-filter_search_project-results li', text: @project3.name).click
end

Then(/^select another project for member$/) do
  find(:css, '#select2-filter_search_project-container').click
  find('#select2-filter_search_project-results li', text: @project3.name).click
end

Given(/^javascript driver is changed$/) do
  if ENV["BROWSER_MODE"] != "true"
    Capybara.current_driver = :poltergeist
    sleep 3
  end
end

Then(/^switch to default driver$/) do
end
