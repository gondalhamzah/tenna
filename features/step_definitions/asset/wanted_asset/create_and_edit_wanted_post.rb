Then(/^user can create Wanted Post as "([^"]*)"$/) do |person|
  @category1 = FactoryGirl.create(:category, category_type: 'equipment')
  @category2 = FactoryGirl.create(:category, category_type: 'material')
  if person == "Admin"
    @project_c = FactoryGirl.create(:project, company: @company_admin.company, contact: @company_admin)
  end

  visit new_wanted_asset_path

  if person != "User"
    find(:xpath, '//*[@id="new_wanted_asset"]/div[1]/div[1]/div[1]/div/span').click
    find(:css, "#select2-wanted_asset_project_id-results li", text: @project.name).click
  end

  select "United States", from: "wanted_asset_country"
  fill_in "wanted_asset_title", with: "Test Wanted Asset"
  sleep 2
  fill_in "wanted_asset_description", with: "Some description of wanted test here"
  fill_in "Location Zip / Postal Code", with: '08817'
  sleep 2
  find(:css, '#select2-wanted_asset_category_id-container').click
  find('#select2-wanted_asset_category_id-results ul li', text: @category1.name).click
end

Then(/^user can edit Wanted Post for "([^"]*)"$/) do |person|
  click_link('Test Wanted Asset')
  expect(page).to have_content('Edit Test Wanted Asset')

  if person == "Admin"
    find(:xpath, '//*[@id="select2-wanted_asset_project_id-container"]').click
    find(:css, "#select2-wanted_asset_project_id-results li", text: @project_c.name).click
  elsif person == "Leader"
    find(:xpath, '//*[@id="select2-wanted_asset_project_id-container"]').click
    find(:css, "#select2-wanted_asset_project_id-results li", text: @project3.name).click
  elsif person == "Member"
    find(:xpath, '//*[@id="select2-wanted_asset_project_id-container"]').click
    find(:css, "#select2-wanted_asset_project_id-results li", text: @project3.name).click
  elsif person == "Field"
    find(:xpath, '//*[@id="select2-wanted_asset_project_id-container"]').click
    find(:css, "#select2-wanted_asset_project_id-results li", text: @project2.name).click
  elsif person == "User"
    puts "Company User can create wanted post without specifying a Site"
  end

  click_button('Update Wanted asset')
  sleep 2
  visit wanted_assets_path
end

Then(/^verify wanted post priorities present on index page$/) do
  find(:css, 'label[for="filter_search_high"]').present?
  find(:css, 'label[for="filter_search_medium"]').present?
  find(:css, 'label[for="filter_search_low"]').present?
end

Then(/^verify wanted post priorities present on create page$/) do
  find(:css, 'label[for="wanted_asset_priority_high"]').present?
  find(:css, 'label[for="wanted_asset_priority_medium"]').present?
  find(:css, 'label[for="wanted_asset_priority_low"]').present?
end

Then(/^visit wanted post index page$/) do
  visit wanted_assets_path
end

Then(/^choose wanted post priority "([^"]*)"$/) do |priority|
  if priority.downcase == "high"
    find(:css, 'label[for="wanted_asset_priority_high"]').click
  elsif priority.downcase == "medium"
    find(:css, 'label[for="wanted_asset_priority_medium"]').click
  elsif priority.downcase == "low"
    find(:css, 'label[for="wanted_asset_priority_low"]').click
  else
    false
  end
end

Then(/^verify wanted post priority "([^"]*)" checked on create page$/) do |priority|
  if priority.downcase == "high"
    find(:css, '#wanted_asset_priority_high', visible: false).checked?
  elsif priority.downcase == "medium"
    find(:css, '#wanted_asset_priority_medium', visible: false).checked?
  elsif priority.downcase == "low"
    find(:css, '#wanted_asset_priority_low', visible: false).checked?
  else
    false
  end
end

Then(/^choose wanted post filter "([^"]*)"$/) do |priority|
  if priority.downcase == "high"
    find(:css, 'label[for="filter_search_high"]').click
  elsif priority.downcase == "medium"
    find(:css, 'label[for="filter_search_medium"]').click
  elsif priority.downcase == "low"
    find(:css, 'label[for="filter_search_low"]').click
  elsif priority.downcase == "fulfilled"
    find(:css, 'label[for="filter_search_fulfilled"]').click
  elsif priority.downcase == "my wanted posts"
    find(:css, 'label[for="filter_search_my_wanted"]').click
  else
    false
  end
end

Then(/^select wanted asset with priority "([^"]*)"$/) do |priority|
  if priority.downcase == "high"
    page.execute_script('$(".high-clr").closest("tr").find("label").click()')
  elsif priority.downcase == "medium"
    page.execute_script('$(".medium-clr").closest("tr").find("label").click()')
  elsif priority.downcase == "low"
    page.execute_script('$(".low-clr").closest("tr").find("label").click()')
  else
    false
  end
end

Then(/^select all wanted assets$/) do
  find(:css, 'label[for="asset-select-toggle"]').click
end
