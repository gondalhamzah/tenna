Then(/^check a wanted asset$/) do
  find(:xpath, '//*[@id="wanted_asset_1"]/td[1]/label').click
end

Then(/^click on Action$/) do
  click_on "Actions"
end

Then(/^mark as "([^"]*)"$/) do |arg1|
  sleep 2
  if arg1 == 'fullfilled'
    find(:xpath, '//*[@id="action-dropdown"]/li[1]/a').click
  else
    find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
    if ENV["BROWSER_MODE"] == "true"
      page.driver.browser.switch_to.alert.accept
    else
      page.execute_script 'window.confirm = function () { return true }'
    end
  end
end

Then(/^check "([^"]*)" filter option$/) do |arg1|
  find(:xpath, '//*[@id="new_filter_search"]/div[4]/div/label').click
end
