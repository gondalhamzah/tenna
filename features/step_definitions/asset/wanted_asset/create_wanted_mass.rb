Then(/^goto add wanted post page$/) do
  visit new_wanted_asset_path
end

Then(/^fill the Add Wanted Post form$/) do
  find(:xpath, '//*[@id="new_wanted_asset"]/div[1]/div[1]/div[1]/div/span').click
  find(:css, "#select2-wanted_asset_project_id-results li", text: @project.name).click

  select "United States", :from => "wanted_asset_country"
  fill_in "wanted_asset_title", with: "Test Wanted Asset"
  sleep 2
  fill_in "wanted_asset_description", with: "Some description of wanted test here"
  sleep 2
  find(:css, '#select2-wanted_asset_category_id-container').click
  find('#select2-wanted_asset_category_id-results ul li', text: @category1.name).click
end

Then(/^fill the Add Wanted Post form with candian$/) do
  find(:xpath, '//*[@id="new_wanted_asset"]/div[1]/div[1]/div[1]/div/span').click
  all(:css, "#select2-wanted_asset_project_id-results li")[1].click

  select "Canada", :from => "wanted_asset_country"
  fill_in "wanted_asset_title", with: "Test Wanted Asset"

  fill_in "Location Zip / Postal Code", with: "M4B 1B3"
  sleep 2
  fill_in "wanted_asset_description", with: "Some description of wanted test here"
  sleep 2
  find(:css, '#select2-wanted_asset_category_id-container').click
  find('#select2-wanted_asset_category_id-results ul li', text: Category.first.name).click
end

Then(/^click on "([^"]*)"$/) do |arg1|
  click_on arg1
  sleep 2
end

Then(/^Wanted Asset count should be (\d+)$/) do |arg1|
  expect(page).to have_css("table#table-assets tr", count: arg1)
end

Then(/^verify categories filter new look$/) do
  find(:xpath, '//*[@id="select2-filter_search_category-container"]').click
  find(:xpath,'//*[@id="select2-filter_search_category-results"]/li[2]/strong')
end
