Given(/^select the project on asset create page$/) do
  select @project.name, :from => 'asset_project_id'
end

Then(/^fill the assets form with wrong info$/) do
  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: @project.name).click
  # select @project.name, :from => 'asset_project_id'
  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: @category1.name).click
  # select @category1.name, :from => 'asset_category_id'
  fill_in "asset_title", with: "New Asset"
  fill_in "asset_market_value", with: "5555"
  fill_in "Location Zip / Postal Code", with: "M5J 2X2"
  select 'United States', :from => 'asset_country'
end

Then(/^fill the assets form with right info$/) do
  # select @project.name, :from => 'asset_project_id'
  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: @project.name).click

  # select @category1.name, :from => 'asset_category_id'
  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: @category1.name).click

  fill_in "asset_title", with: "New Asset"
  fill_in "asset_market_value", with: "5555"

  fill_in "Location Zip / Postal Code", with: "M5J 2X2"
  select 'Canada', :from => 'asset_country'
end

Then(/^fill the assets form with right zip code$/) do
  fill_in "Location Zip / Postal Code", with: "12345"
end

Then(/^should be "([^"]*)" asset$/) do |arg1|
  expect(page).to have_css("table#table-assets tr", count: 1)
end

Then(/^there should be error message "([^"]*)"$/) do |arg1|
  expect(page).to have_content(arg1)
end

Then(/^there should "([^"]*)"$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    expect(page).to have_content(arg1)
  end
end

Then(/^fill the remaining asset form$/) do
  fill_in "asset_title", with: "New Asset"
  fill_in "asset_market_value", with: "5555"
  select @category1.name, :from => 'equipment-select'
end

Then(/^fill the assets form with Canadian info$/) do
  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: @project.name).click
  # select @project.name, :from => 'asset_project_id'
  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: @category1.name).click
  # select @category1.name, :from => 'asset_category_id'
  fill_in "asset_title", with: "New Asset"
  fill_in "asset_market_value", with: "5555"
  fill_in "Location Zip / Postal Code", with: "M5J 2X2"
  select 'Canada', :from => 'asset_country'
end

Then(/^fill the assets form with American info$/) do
  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: @project.name).click
  # select @project.name, :from => 'asset_project_id'
  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: @category1.name).click
  # select @category1.name, :from => 'asset_category_id'

  # select @project.name, :from => 'asset_project_id'
  fill_in "asset_title", with: "New Asset"
  fill_in "asset_market_value", with: "5555"
  # select @category1.name, :from => 'asset_category_id'
  fill_in "Location Zip / Postal Code", with: "11223"
  select 'United States', :from => 'asset_country'
end


Then(/^should be "([^"]*)" assets$/) do |arg1|
  visit assets_path
  begin
    if Asset.count == arg1.to_i
      puts "#{arg1} Asset(s) was created"
    else
      puts "#{arg1} Asset(s) was not created"
    end
  rescue
    expect(page).to have_css("table#table-assets tr", :count=>arg1.to_i)
  end
end


Given(/^goto asset create page$/) do
  visit new_asset_path
end
