Given(/^User on Asset detail page$/) do
  asset_detail_page
end

Then(/^History tab contain a tab of group$/) do
  expect(find('ul.tabs')).to have_selector('li', exact: "Groups")
end

Given(/^goto asset page select an asset$/) do
  @category_group = FactoryGirl.create(:category_group)
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @group_a = FactoryGirl.create(:group, company: @company_admin.company, name: "Group A", project: @project)
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, name: "Group B", project: @project)
  GroupsIndex.import(group: @group_a)
  GroupsIndex.import(group: @group_b)
  GroupsVersionsIndex.import(group: @group_a)
  GroupsVersionsIndex.import(group: @group_b)
  ProjectsIndex.import(project: @project)
  asset_detail_page
end

Given(/^select "([^"]*)" from action$/) do |arg1|
  if arg1 == "Unlink Group"
    @public_asset.group = @group_a
    @public_asset.save
    AssetsIndex.import(asset: @public_asset)
    AssetsVersionsIndex.import(asset: @public_asset)
    visit asset_path id: @public_asset.id
  end

  if ENV["BROWSER_MODE"] == "true"
    click_on arg1
    if arg1 == "Unlink Group"
      begin
        find(:xpath, '//*[@id="edit_equipment_1"]/button').click
      rescue Exception => e
        within ".simple_form.reallocate-select" do
          click_on 'Unlink Group'
        end
      end
    end
    if arg1 == "Link To Group"
      within ".simple_form.reallocate-select" do
        click_on 'Link To Group'
      end
    end
  end
end

Then(/^action asset "([^"]*)" in Group history on asset detail page$/) do |arg1|
  visit current_url
  sleep 2
  expect(find('#DataTables_Table_0')).to have_content("Group A")
end

Given(/^On group detail page link asset to group$/) do
  @category_group = FactoryGirl.create(:category_group)
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @group_a = FactoryGirl.create(:group, company: @company_admin.company, name: "Group A", project: @project)
  @group_b = FactoryGirl.create(:group, company: @company_admin.company, name: "Group B", project: @project)
  GroupsIndex.import(group: @group_a)
  GroupsIndex.import(group: @group_b)
  GroupsVersionsIndex.import(group: @group_a)
  GroupsVersionsIndex.import(group: @group_b)
  ProjectsIndex.import(project: @project)
  @public_asset = FactoryGirl.create(:asset, public: true, company: @company_admin.company)
  @public_asset.creator = @company_admin
  @public_asset.project = @project
  @public_asset.group = @group_a
  @public_asset.save
  AssetsIndex.import(asset: @public_asset)
  AssetsVersionsIndex.import(asset: @public_asset)
  visit asset_path id: @public_asset.id
end

Then(/^there is information of linked group$/) do
  expect(page).to have_content("#{@group_a.name}")
end


Given(/^On asset page$/) do
  asset_list_page
end

Then(/^There should be an option like "([^"]*)" in mass actions on assets page$/) do |arg1|
  find(:xpath, "//*[@id='panel-list']/div/div[2]/div[1]/div/form/div[1]/a").click
  find(:xpath, "//*[@id='action-dropdown']/li[4]/a").click
end


Given(/^On asset page link an asset to a group$/) do
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @group_a = FactoryGirl.create(:group, company: @company_admin.company, name: "Group A", project: @project)
  @public_asset = FactoryGirl.create(:asset, public: true, company: @company_admin.company)
  @public_asset.creator = @company_admin
  @public_asset.project = @project
  @public_asset.save
  AssetsIndex.import(asset: @public_asset)
  AssetsVersionsIndex.import(asset: @public_asset)
  GroupsIndex.import(group: @group_a)
  GroupsVersionsIndex.import(group: @group_a)
  visit asset_path id: @public_asset.id
  # #sleep 60
  find("a[href='#link-group-asset']").click
  within "#link-group-asset" do
    click_on "Link To Group"
  end
end

Given(/^On asset page unlink an asset to a group$/) do
  click_on "Unlink Group"
end

Then(/^There should be Link To Group history before and unlink$/) do
end
