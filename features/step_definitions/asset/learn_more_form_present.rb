Given(/^go to home page$/) do
  visit root_path
end

Then(/^find learn more button should not be on page$/) do
	within "#wrapper" do 
		expect(page).not_to have_css("a", text: "LEARN MORE")
	end
end

Then(/^find learn more form and its fields should present$/) do
	within ".learn-more-form" do 
		expect(page).to have_content("Learn More") 
	end
end

Then(/^go to industries page$/) do
  visit industries_served_path
end

Then(/^go to value page$/) do
	visit value_path
end

Then(/^go to about page$/) do
	visit about_us_path
end

Then(/^find learn more button should not be on "([^"]*)" page$/) do |arg1|
	within ".wrapper" do 
		expect(page).not_to have_css("a", text: "LEARN MORE")
	end
end
