Then(/^click on Export analytics$/) do
  begin
    within '.rules-wrapper' do
      click_link 'Export'
    end
  rescue
    begin
      find(:xpath, '//*[@id="assets-show"]/main/div[2]/div[3]/div[1]/div/div/button').click
    rescue
      page.execute_script("$('#assets-show > main > div.row > div.col.s12.m12.history-row > div:nth-child(6) > div > div > a').trigger('click')")
    end
  end
end

Then(/^check analytics export file is downloaded$/) do
  sleep 5
  expect(DownloadHelpers::downloads.size).to eq(1)
end

Then(/^read the downloaded CSV file$/) do
  expect(DownloadHelpers::download_content.include?("Test Project 1")).to eq true
end

Given(/^go to group page$/) do
  visit groups_path
end

Given(/^click on site template$/) do
  find(".project-header-icons a", text: "Site Template").click
end

Given(/^click on export button$/) do
  find(:xpath, '//*[@id="tab-reports"]/div/div/div[1]/div/div/button').click
end


