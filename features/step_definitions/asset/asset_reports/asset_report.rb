Given(/^click on live data first select$/) do
  find('#tab-reports > div > div > div:nth-child(2) > div > div > div.filterGroupsContainer > div.group-liveData > div:nth-child(2) > div > div > div.flowform.rule-cell > div > div:nth-child(1) > div').click
end

Given(/^click on live data second select hours$/) do
  find('#tab-reports > div > div > div:nth-child(2) > div > div > div.filterGroupsContainer > div.group-liveData > div:nth-child(2) > div > div > div.flowform.rule-cell > div > div:nth-child(1) > div > div.Select-menu-outer > div > div:nth-child(2)').click
end

Given(/^click on attribute tab$/) do
  find(:xpath, '//*[@id="tab-reports"]/div/div/div[1]/div/div/div[1]/button[3]').click
end

Given(/^select wanted$/) do
  find(:xpath, '//*[@id="react-select-2--value"]/div[1]').click
  within ".Select-menu-outer" do
    find(:css, '.Select-menu .Select-option', text: "Wanted").click
  end
end

Given(/^select contributed option$/) do
  find(:xpath, '//*[@id="react-select-3--value"]/div[1]').click
  within ".Select-menu-outer" do
    find(:css, '.Select-menu .Select-option', text: "Contributed").click
  end
  expect(find(:css, '#table-view')).not_to have_content('fulfilled')
end

Given(/^select fulfilled option$/) do
  find(:xpath, '//*[@id="react-select-3--value"]/div[1]').click
  within ".Select-menu-outer" do
    find(:css, '.Select-menu .Select-option', text: "Fulfilled").click
  end
  expect(find(:css, '#table-view')).not_to have_content('contributed')
end

Given(/^as jhdfjshd$/) do
  byebug
end

