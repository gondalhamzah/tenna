Then(/^select QR from select and fill "([^"]*)"$/) do |arg1|
  select 'QR', :from => "asset_token_type"
  fill_in "asset_token", with: "ping"
  find('#add-token-btn').click
end

Then(/^confirm location access$/) do
  if ENV["BROWSER_MODE"] == "true"
  Capybara.javascript_driver = :poltergeist
  options = { js_errors: false, timeout: 180, phantomjs_logger: StringIO.new, logger: nil, phantomjs_options: ['--load-images=no', '--ignore-ssl-errors=yes'] }
  Capybara.register_driver(:poltergeist) do |app|
    Capybara::Poltergeist::Driver.new app, options
  end
  # Capybara.current_driver = :poltergeist
  # options = {js_errors: false}
  # Capybara.register_driver :poltergeist do |app|
  #   Capybara::Poltergeist::Driver.new(app, options)
  # end
  sleep 2
  page.execute_script("$('#bs-asset-map').data('lat', 333333);$('#bs-asset-map').data('lng', 999999);")
  Capybara.use_default_driver
  end
end

Then(/^QR code should be (\d+)$/) do |arg1|
    expect(find(:xpath, '//*[@id="dashboard-index"]/div[5]/div[2]/div[2]/a/div/span')).to have_content(arg1)
end

Then(/^scan the asset with ping RFID$/) do
  if ENV["BROWSER_MODE"] == "true"
  Capybara.javascript_driver = :poltergeist
  options = { js_errors: false, timeout: 180, phantomjs_logger: StringIO.new, logger: nil, phantomjs_options: ['--load-images=no', '--ignore-ssl-errors=yes'] }
  Capybara.register_driver(:poltergeist) do |app|
    Capybara::Poltergeist::Driver.new app, options
  end
  # Capybara.current_driver = :poltergeist
  # options = {js_errors: false}
  # Capybara.register_driver :poltergeist do |app|
  #   Capybara::Poltergeist::Driver.new(app, options)
  # end
  sleep 2
  page.execute_script("getAsset('ping', 'tab-inventory')")
  sleep 2
  Capybara.use_default_driver
  end
end

# Given(/^click on menu and click on asset$/) do
#   sleep 5
#   find(:xpath, '//*[@id="desktop_main_vertical_nav_btn"]').click
#   find(:xpath, '//*[@id="desktop_main_vertical_nav_dropdown"]/li[3]').click
# end

Given(/^click on specific$/) do
  find(:xpath, "//*[@id='asset_#{Asset.last.id}']/td[3]").click
end

Given(/^click to edit that$/) do
  find(:xpath, '//*[@id="assets-show"]/main/div[3]/a').click
end

Given(/^update its group$/) do
  find(:xpath, '//*[@id="edit_asset_1"]/div[1]/div[1]/div[8]/div/span/span[1]/span').click
end

Given(/^click update asset$/) do
  find(:xpath, '//*[@id="edit_asset_1"]/div[2]/div/div/button').click
end
