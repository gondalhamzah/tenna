Given(/^create rental and owned assets for active admin$/) do
  Chewy.strategy(:bypass)
  @category_group = FactoryGirl.create(:category_group)
  @company_admin = FactoryGirl.create(:company_admin)
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @company_admin.company.project_id = @project.id
  @company_admin.company.save
  @rental_asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
  @rental_asset1.creator = @company_admin
  @rental_asset1.project = @project
  @rental_asset1.rental = false
  @rental_asset1.public = false
  @rental_asset1.owned = true
  @rental_asset1.type = "Material"
  @rental_asset1.save
  AssetsIndex.import(asset: @rental_asset1)
  AssetsVersionsIndex.import(asset: @rental_asset1)
  ProjectsIndex.import(project: @project)
end

Given(/^goto assets page for active admin$/) do
  visit admin_materials_path
end

Given(/^select owned for active admin$/) do
  visit edit_admin_material_path :id => @rental_asset1.id
end

Given(/^select action of an asset as for rent$/) do
  page.find("#material_asset_kind").click
  select "For Rent", :from => "material_asset_kind"
end

Given(/^select rental option update asset for active admin$/) do
  select "United States", :from => "material_country_name"
  fill_in "Tag list", :with => ''
  page.execute_script("$('#edit_material').submit()")
end

Then(/^Asset updated successfully$/) do
  expect(page).to have_content("Material was successfully updated.")
end

Then(/^goto asset index page of active admin$/) do
  visit admin_materials_path
end

Then(/^asset should be present in the list with rental field true$/) do
end

Then(/^goto to asset detail page of active admin$/) do
  visit admin_material_path :id => @rental_asset1.id
end

Then(/^asset should be rental for active admin$/) do
  if ENV["BROWSER_MODE"] == true
    expect(page).to have_content("for_rent")
  end
end
