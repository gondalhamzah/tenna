Then(/^add "([^"]*)" qr code to this asset$/) do |arg1|
  sleep 5
	select "QR", from: "asset_token_type"
	fill_in "asset_token", with: arg1
	within "#add-id" do
		click_on "Submit"
	end
  sleep 5
end

Given(/^goto marketplace$/) do
  visit marketplace_path
end

Then(/^there should be "([^"]*)" asset with QR code$/) do |arg1|
  expect(page).to have_css('.fa-qrcode')
  # expect(page).to have_content(arg1)
end

Given(/^check initial qr code amount$/) do
  visit dashboard_path
  sleep 2
  expect(find(:xpath, '//*[@id="dashboard-index"]/div[4]/div[2]/div[2]/a/div/span')).to have_content("0")
end

Then(/^check qr code amount after asset creation$/) do
  visit dashboard_path
  expect(find(:xpath, '//*[@id="dashboard-index"]/div[4]/div[2]/div[2]/a/div/span')).to have_content("1")
end

Given(/^reset asset index$/) do
  reset_asset_index
end

Then(/^goto asset detail page of "([^"]*)"$/) do |arg1|
  visit asset_path :id => Asset.last.id
end

Then(/^select bt tracking and add "([^"]*)" tracker value$/) do |arg1|
  select "BT", from: "asset_token_type"
  fill_in "asset_token", with: arg1
end

Then(/^select "([^"]*)" tracking and add "([^"]*)" value$/) do |type,value|
  select type, from: "asset_token_type"
  fill_in "asset_token", with: value
end

Then(/^change Asset lat and lng$/) do
  asset = Asset.last
  asset.zip_code.lat = 35.3514653
  asset.zip_code.lng = -65.0601767
  asset.miles = 18
  asset.hours = 18
  asset.save
end

Then(/^verify qr code amount (\d+)$/) do |arg1|
  expect(find(:xpath, '//*[@id="dashboard-index"]/div[4]/div[2]/div[2]/a/div/span')).to have_content(arg1)
end