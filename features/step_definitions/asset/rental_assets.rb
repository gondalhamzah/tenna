# def single_filter(arg1)
#   if arg1 == "rental filter"
#     find('#asset-management-header').click
#     find(:xpath, '//*[@id="asset-mangement-filters"]/div[3]/div/label').click
#     find('#asset-management-header').click
#     first(:xpath, "//*[@id='new_filter_search']/div[8]/button").click
#   elsif arg1 == "owned asset"
#     find('#asset-management-header').click
#     find(:xpath, '//*[@id="asset-mangement-filters"]/div[2]/div/label').click
#     find('#asset-management-header').click
#     first(:xpath, '//*[@id="new_filter_search"]/div[8]/button').click
#   else
#     find('#asset-management-header').click
#     find(:xpath, '//*[@id="asset-mangement-filters"]/div[2]/div/label').click
#     find('#asset-management-header').click
#     first(:xpath, '//*[@id="new_filter_search"]/div[8]/button').click
#   end
# end

Given(/^On Asset Create Page create Rental asset and "([^"]*)"$/) do |arg1|
  @category_group = FactoryGirl.create(:category_group)
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @rental_asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
  @rental_asset1.creator = @company_admin
  @rental_asset1.project = @project
  @rental_asset1.rental = true
  @rental_asset1.public = false
  @rental_asset1.owned = false
  @rental_asset1.type = "Material"
  @rental_asset1.save
  AssetsIndex.import(asset: @rental_asset1)
  AssetsVersionsIndex.import(asset: @rental_asset1)
  if arg1 != "type material"
    @equip_asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
    @equip_asset1.creator = @company_admin
    @equip_asset1.project = @project
    @equip_asset1.rental = true
    @equip_asset1.public = false
    @equip_asset1.owned = false
    @equip_asset1.type = "Equipment"
    @equip_asset1.save
    AssetsIndex.import(asset: @equip_asset1)
    AssetsVersionsIndex.import(asset: @equip_asset1)
  end
  ProjectsIndex.import(project: @project)
end

# Given(/^go to assets page and apply "([^"]*)"$/) do |arg1|
#   find('#asset-management-header').click
#   find('#asset-mangement-filters label', text: arg1).click
#   find('#asset-management-header').click
#   find("#new_filter_search button").click
# end

Then(/^rental assets count should be (\d+)$/) do |arg1|
  if ENV["BROWSER_MODE"] == true
    within('table#table-assets') do
      expect(page).to have_xpath(".//tr", :count =>arg1)
    end
  end
end

Given(/^goto project detail page$/) do
  visit project_path :id => @project.id
end

Given(/^goto group detail page$/) do
  visit group_path :id => Group.last.id
end

# Then(/^Apply filter with "([^"]*)" and "([^"]*)"$/) do |arg1, arg2|
#   if arg1.downcase == "rentals" and arg2.downcase == "material"
#     find('#asset-management-header').click
#     find(:xpath, '//*[@id="asset-mangement-filters"]/div[3]/div/label').click
#     find('#asset-management-header').click
#     find('#asset-type-header').click
#     find(:xpath, '//*[@id="asset-type-filters"]/div[2]/div/label').click
#     find('#asset-type-header').click
#   else
#     find('#asset-management-header').click
#     sleep 1
#     find(:xpath, '//*[@id="asset-mangement-filters"]/div[3]/div/label').click
#     find('#asset-management-header').click
#     find('#asset-type-header').click
#     find(:xpath, '//*[@id="asset-type-filters"]/div[2]/div/label').click
#     find(:xpath, '//*[@id="asset-type-filters"]/div[1]/div/label').click
#     find('#asset-type-header').click
#   end

#   first(:xpath, '//*[@id="new_filter_search"]/div[8]/button').click
# end

Given(/^GO to rental asset edit with type equipment and change it to owned$/) do
  visit edit_asset_path :id => @equip_asset1.id
  @equip_asset1.rental = false
  @equip_asset1.owned = true
  @equip_asset1.save
  AssetsIndex.import(asset: @equip_asset1)
  AssetsVersionsIndex.import(asset: @equip_asset1)
end

# Given(/^goto assets page and apply filter with rental and material$/) do
#   visit assets_path
#   find('#asset-management-header').click
#   find(:xpath, '//*[@id="asset-mangement-filters"]/div[3]/div/label').click
#   find('#asset-management-header').click
#   find('#asset-type-header').click
#   find(:xpath, '//*[@id="asset-type-filters"]/div[2]/div/label').click
#   find('#asset-type-header').click
#   first(:xpath, '//*[@id="new_filter_search"]/div[8]/button').click
# end


Then(/^assets count should be (\d+)$/) do |arg1|
  if ENV["BROWSER_MODE"] == true
    within('table#table-assets') do
      expect(page).to have_xpath(".//tr", count: arg1)
    end
  end
end

Given(/^GO to rental asset edit with type material and change it to owned$/) do
  visit edit_asset_path :id => @rental_asset1.id
  @rental_asset1.rental = false
  @rental_asset1.owned = true
  @rental_asset1.save
  AssetsIndex.import(asset: @rental_asset1)
  AssetsVersionsIndex.import(asset: @rental_asset1)
end

Then(/^there should be no assets$/) do
  within('table#table-assets') do
    expect(page).to have_xpath(".//tr", :count => 0)
  end
end

# Given(/^goto assets page and apply filter with rental and equipment$/) do
#   visit assets_path
#   find('#asset-management-header').click
#   find(:xpath, '//*[@id="asset-mangement-filters"]/div[3]/div/label').click
#   find('#asset-management-header').click
#   find('#asset-type-header').click
#   find(:xpath, '//*[@id="asset-type-filters"]/div[1]/div/label').click
#   find('#asset-type-header').click
#   first(:xpath, '//*[@id="new_filter_search"]/div[8]/button').click
# end

Given(/^Create a group and allocate both assets to it$/) do
  @group = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  @rental_asset1.group = @group
  @equip_asset1.group = @group
  @equip_asset1.save
  @rental_asset1.save
  @group.save
  GroupsIndex.import(group: @group)
  AssetsVersionsIndex.import(asset: @rental_asset1)
  AssetsVersionsIndex.import(asset: @equip_asset1)
  AssetsIndex.import(asset: @rental_asset1)
  AssetsIndex.import(asset: @equip_asset1)
end

# Given(/^go to group detail and apply "([^"]*)"$/) do |arg1|
#   visit group_path :id => @group.id
#   single_filter arg1
# end

Given(/^Create a project and allocate both assets to it$/) do
end

# Given(/^go to project detail and apply "([^"]*)"$/) do |arg1|
#   visit project_path :id => @project.id
#   single_filter arg1
# end
