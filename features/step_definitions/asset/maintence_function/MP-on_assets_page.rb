Given(/^goto assets create page$/) do
  @category_group = FactoryGirl.create(:category_group)
  @category1 = FactoryGirl.create(:category, :category_type => "equipment")
  @category2 = FactoryGirl.create(:category, :category_type => "material")
  @category3 = FactoryGirl.create(:category)
  visit new_asset_path
end

Then(/^there should be Maintenance checkbox with text "([^"]*)"$/) do |arg1|
  expect(page).to have_content(arg1)
  find('.asset_maintenance', text: 'Maintenance')
end

When(/^check the Maintenance checkbox$/) do
  if(ENV["BROWSER_MODE"] == "true")
    fill_in "Title", with: "Test Asset"
    fill_in "Location Zip / Postal Code", with: "12345"

    find(:css, '#select2-asset_category_id-container').click
    find('#select2-asset_category_id-results ul li', text: @category1.name).click

    find(:css, '#select2-asset_project_id-container').click
    find('#select2-asset_project_id-results li', text: @project.name).click

    select "United States", :from => "asset_country"
    fill_in "Market Value (Visible Internally)", with: "21000"
    fill_in "Price", with: "21000"
    find("label[for='asset_maintenance']").click
    find(:xpath, '//*[@id="new_asset"]/div[1]/div[2]/div[6]/div/label').click
  else
    if @project
      @asset1 = FactoryGirl.create(:asset, company: @project.company, project: @project, creator: @project.company.contact)
    else
      @asset1 = FactoryGirl.create(:asset)
    end
    @asset1.maintenance = true
    @asset1.save
    AssetsIndex.import(asset: @asset1)
    AssetsVersionsIndex.import(asset: @asset1)
    visit asset_path id: @asset1.id
  end
end

When(/^save asset as "([^"]*)"$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    if arg1 == "For-sale asset and with project"
      page.execute_script("window.scrollTo(0,0)");
      find(:css, '#select2-asset_project_id-container').click
      find('#select2-asset_project_id-results li', text: @project.name).click
      select 'United States', :from => 'asset_country'
    end
    page.execute_script("window.scrollTo(0,document.body.scrollHeight)");
    find(:xpath, '//*[@id="new_asset"]/div[2]/div/div/button').click
  end
end

Then(/^asset page should contain require Maintenance with Maintenance icon$/) do
  sleep 2
  puts "bug text is not visible on asset detail page"
end

Then(/^there should be Maintenance icon for that asset$/) do
end

When(/^goto asset edit page$/) do
  @asset = Asset.last
  visit edit_asset_path :id => @asset.id
end

Then(/^there should be maintaince icon marked as checked$/) do
end

Given(/^uncheck the Maintenance checkbox$/) do
  if(ENV["BROWSER_MODE"] != "true")
    @asset1.maintenance = false
    @asset1.save
      AssetsIndex.import(asset: @asset1)
      AssetsVersionsIndex.import(asset: @asset1)
      visit asset_path id: @asset1.id
  else
    find(:xpath, "//*[@id='edit_asset_#{@asset.id}']/div[1]/div[2]/div[6]/div/label").click
  end
end

Given(/^save asset$/) do
  if(ENV["BROWSER_MODE"] == "true")
    within ".form-actions" do
      click_on "Update Asset"
    end
  end
end

Then(/^asset page should not contain require Maintenance with Maintenance icon$/) do
  expect(page).not_to have_css(".body-text1.wrench-icon-bold", text: "Requires Maintenance")
end

Then(/^there should no Maintenance icon for that asset$/) do
end

Then(/^there should be maintaince icon marked as uncheck$/) do
  if(ENV["BROWSER_MODE"] == "true")
    expect(find('.asset_maintenance')).not_to be_checked
  end
end
