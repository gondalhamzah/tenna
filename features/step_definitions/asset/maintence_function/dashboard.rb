And (/^visit dashboard$/) do
  visit dashboard_path
end

Then (/^maintenance count should be (\d+)$/) do|count|
  expect(find(:xpath, '//*[@id="dashboard-index"]/div[4]/div[2]/div[3]/a/div/span').text).to eq '1'
end

