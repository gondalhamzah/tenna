Given(/^create "([^"]*)" asset for "([^"]*)"$/) do |arg1, arg2|
  @category1 = FactoryGirl.create(:category, :category_type => "equipment")
  visit new_asset_path
  if arg2 == "Company Admin"
    if arg1 == "owned"
      create_owned_asset_for @company_admin
      sleep 2
    elsif arg1 == "rental"
      create_rental_asset_for @company_admin
    elsif arg1 == "for-sale"
      create_for_sale_asset_for @company_admin
    elsif arg1 == "for-sale"
      create_for_rent_asset_for @company_admin
    end
  elsif arg2 == "Project Leader" || arg2 == "Project Member"
    if arg1 == "owned"
      create_owned_asset_for @company_user
    elsif arg1 == "rental"
      create_rental_asset_for @company_user
    elsif arg1 == "for-sale"
      create_for_sale_asset_for @company_user
    elsif arg1 == "for-sale"
      create_for_rent_asset_for @company_user
    end
  else
    if arg1 == "owned"
      create_owned_asset_for @field_member
    elsif arg1 == "rental"
      create_rental_asset_for @field_member
    elsif arg1 == "for-sale"
      create_for_sale_asset_for @field_member
    elsif arg1 == "for-sale"
      create_for_rent_asset_for @field_member
    end
  end
  @asset = Asset.last
    visit asset_path id: @asset.id
end

Given(/^mark require maintenance$/) do
  @asset = Asset.last
  visit edit_asset_path id: @asset.id
  sleep 1
  find(:xpath, "//*[@id='edit_asset_#{@asset.id}']/div[1]/div[2]/div[6]/div/label").click
  within ".form-actions" do
    click_on "Update Asset"
  end
  sleep 2
end

Then(/^there should be Maintenance requested in history$/) do
  if ENV["BROWSER_MODE"] == "true"
    @asset = Asset.last
    visit asset_path id: @asset.id
    expect(find('#table-view')).to have_content("Maintenance requested")
  end
end

When(/^mark require completed$/) do
  @asset = Asset.last
  visit edit_asset_path id: @asset.id
  find(:xpath, "//*[@id='edit_asset_#{@asset.id}']/div[1]/div[2]/div[6]/div/label").click
  within ".form-actions" do
    click_on "Update Asset"
  end
end

Then(/^there should be Maintenance completed in history$/) do
  if ENV["BROWSER_MODE"] == "true"
    visit asset_path id: @asset.id
    expect(find('#table-view')).to have_content("Maintenance completed")
  end
end

Given(/^create owned asset for Company Admin from view$/) do
  sleep 4
  select "United States", from: "asset_country"
  fill_in "asset_price", with: '20'
  fill_in "asset_title", with: "New Asset for maintenance"
  find(:xpath, "//*[@id='new_asset']/div[1]/div[2]/div[6]/div/label").click
  click_on "Create Asset"
  sleep 4
end

Then(/^there should be Maintenance requested and completed in history$/) do
  sleep 2
  expect(find('#table-view')).to have_content("Maintenance requested")
  expect(find('#table-view')).to have_content("Maintenance completed")
end

Given(/^create "([^"]*)" asset from view$/) do |arg1|
  if arg1 == "owned"
    find(:css, '#select2-asset_project_id-container').click
    find('#select2-asset_project_id-results li', text: @project.name).click

    find(:css, '#select2-asset_category_id-container').click
    find('#select2-asset_category_id-results ul li', text: @category1.name).click

    fill_in "asset_location_zip", with: '07001'
    select "United States", from: "asset_country"
    fill_in "asset_price", with: '20'
    fill_in "asset_title", with: "New Asset for maintenance"
    find("label[for='asset_maintenance']").click
    click_on "Create Asset"
  elsif arg1 == "rental"
    find("label[for='asset_maintenance']").click
    click_on "Create Asset"
  elsif arg1 == "for-sale"
    find(:xpath, "//*[@id='new_asset']/div[1]/div[1]/div[2]/div/span[2]/label").click
    find("label[for='asset_maintenance']").click
    click_on "Create Asset"
  elsif arg1 == "for-rent"
    find(:xpath, "//*[@id='new_asset']/div[1]/div[1]/div[2]/div/span[2]/label").click
    find("label[for='asset_maintenance']").click
    click_on "Create Asset"
  end
  sleep 2
end

Given(/^goto asset page and complete maintenance$/) do
  @asset = Asset.last
  visit edit_asset_path id: @asset.id
  sleep 4
  find(:xpath, "//*[@id='edit_asset_1']/div[1]/div[2]/div[6]/div/label").click
  within ".form-actions" do
    click_on "Update Asset"
  end
end

Then(/^goto asset page and complete maintenance for "([^"]*)"$/) do |arg1|
  sleep 2
  @asset = Asset.last
  visit edit_asset_path id: @asset.id
  find("label[for='asset_maintenance']").click
  click_on "Update Asset"
end
