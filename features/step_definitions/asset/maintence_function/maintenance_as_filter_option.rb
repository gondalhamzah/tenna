

Given(/^mark (\d+) asset as require maintenance$/) do |arg1|
	if arg1 == "1"
		@for_rent1.maintenance = true
		@for_rent1.save
	elsif arg1 == "2"
		@for_rent1.maintenance = true
		@for_rent1.save
		@for_sale1.maintenance = true
		@for_sale1.save
	end
	AssetsIndex.import(asset: @for_sale1)
  AssetsVersionsIndex.import(asset: @for_sale1)
  AssetsIndex.import(asset: @for_rent1)
  AssetsVersionsIndex.import(asset: @for_rent1)
end
Then(/^apply filter with maintence$/) do
  find('#asset-management-header').click
	find(:xpath, '//*[@id="asset-mangement-filters"]/div[1]/div/label').click
  find('#asset-management-header').click
  first(:xpath, '//*[@id="new_filter_search"]/div[8]/button').click
end

When(/^link these assets to a group for "([^"]*)"$/) do |arg1|
	if arg1 == "Company Admin"
		create_a_group_for @company_admin
	elsif arg1 != "Project Member"
		create_a_group_for @company_user
	end
	@group2 = Group.last
	link_all_assets_group @group2
	GroupsIndex.import(group: @group2)
	GroupsVersionsIndex.import(group: @group2)
end

When(/^goto detail page of that group$/) do
	visit group_path id: Group.last.id
end

And(/^goto detail page of that project$/) do
	visit project_path id: @project.id
end

Then(/^click on tab Asset Type$/) do
  find('#asset-type-header').click
end


Then(/^fill in min hours tabs$/) do
  fill_in 'filter_search_hours_min', with: '9'
end

Then(/^fill in max hours tabs$/) do
  fill_in 'filter_search_hours_min', with: '9'
end

