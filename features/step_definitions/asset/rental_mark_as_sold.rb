Given(/^create rental and owned assets$/) do
  @category_group = FactoryGirl.create(:category_group)
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @group = FactoryGirl.create(:group, company: @company_admin.company, project: @project)
  @rental_asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
  @rental_asset1.creator = @company_admin
  @rental_asset1.project = @project
  @rental_asset1.rental = true
  @rental_asset1.public = false
  @rental_asset1.owned = false
  @rental_asset1.type = "Material"
  @rental_asset1.save
  @equip_asset1 = FactoryGirl.create(:asset, company: @company_admin.company)
  @equip_asset1.creator = @company_admin
  @equip_asset1.project = @project
  @equip_asset1.public = false
  @equip_asset1.rental = false
  @equip_asset1.owned = true
  @equip_asset1.save
  @group.save
  @rental_asset1.group = @group
  @equip_asset1.group = @group
  @rental_asset1.save
  @equip_asset1.save
  GroupsIndex.import(group: @group)
  AssetsIndex.import(asset: @rental_asset1)
  AssetsVersionsIndex.import(asset: @rental_asset1)
  AssetsIndex.import(asset: @equip_asset1)
  AssetsVersionsIndex.import(asset: @equip_asset1)
  ProjectsIndex.import(project: @project)
end


Given(/^goto asset edit page of "([^"]*)"$/) do |arg1|
  if arg1 == "rental asset"
    visit edit_asset_path :id => @rental_asset1.id
  else
    visit edit_asset_path :id => @equip_asset1.id
  end
end

Then(/^there should be "([^"]*)"$/) do |arg1|
  if arg1 == "no mark as sold"
    expect(page).not_to have_content("mark as sold")
  else
    expect(page).not_to have_content("mark as sold")
  end
end

Then(/^goto assets page and goto action$/) do
  visit assets_path
  within "#panel-list" do
    click_on "Actions"
  end
end

Then(/^goto action$/) do
  begin
    within "#panel-list" do
      click_on "Actions"
    end
  rescue
    page.execute_script("$('#panel-list > form > div.select-wrapper > a').trigger('click')")
  end
end


Then(/^goto groups detail page and goto action$/) do
  visit group_path :id => @group.id
  within "#panel-list" do
    click_on "Actions"
  end
end

Then(/^goto project detail page and goto action$/) do
  visit project_path :id => @project.id
  if ENV["BROWSER_MODE"] == true
    within "#panel-list" do
      click_on "Actions"
    end
  end
end
