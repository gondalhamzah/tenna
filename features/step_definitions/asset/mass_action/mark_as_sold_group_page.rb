

Given(/^User on Group detail Page (\d+) assets of all kinds\( Rental, Owned, For\-Sale, For\-Rent\)$/) do |arg1|
  create_four_assets @company_admin
  link_all_assets_to_group
  sleep 1
  visit group_path id: Group.last.id
end

When(/^select asset "([^"]*)" and apply action "([^"]*)" on group page$/) do |arg1, arg2|
	if arg1 == "owned"
		# //*[@id="asset_2"]/td[1]/label
		find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
		# check("asset-selected-#{@owned_asset1.id}")
	elsif arg1 == "rental"
		if ENV["BROWSER_MODE"] == "true"
			page.driver.browser.switch_to.alert.accept
		else
			page.execute_script 'window.confirm = function () { return true }'
		end
		find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
		# check("asset-selected-#{@rental_asset1.id}")
	elsif arg1 == "for-sale"
		if ENV["BROWSER_MODE"] == "true"
			page.driver.browser.switch_to.alert.accept
		else
			page.execute_script 'window.confirm = function () { return true }'
		end
		find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
		# check("asset-selected-#{@for_sale1.id}")
	elsif arg1 == "for-rent"
		if ENV["BROWSER_MODE"] == "true"
			page.driver.browser.switch_to.alert.accept
		else
			page.execute_script 'window.confirm = function () { return true }'
		end
		find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
		# check("asset-selected-#{@for_rent1.id}")
	end
	#sleep 2
	find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
	#sleep 1
	find(:xpath, '//*[@id="action-dropdown"]/li[1]/a').click
	#sleep 1
end

Then(/^it should successfully update asset to marked as sold on groups page$/) do

end

Then(/^there should be (\d+) assets now on group page$/) do |arg1|
	if ENV["BROWSER_MODE"] == true
		expect(page).to have_css("table#table-assets tr", :count=>arg1)
	end
end

Then(/^it should throw error rental asset cannot be sold on groups page$/) do
	if ENV["BROWSER_MODE"] == true
		expect(find('#flash')).to have_content("Successfully updated 0 assets.")
	end
end

Then(/^there should be no option for mark as sold$/) do
end
