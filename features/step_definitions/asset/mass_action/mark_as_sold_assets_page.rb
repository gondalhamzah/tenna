
Given(/^User on Assets Page (\d+) assets of all kinds\( Rental, Owned, For\-Sale, For\-Rent\)$/) do |arg1|
  create_four_assets @company_admin
  visit assets_path
  #sleep 2
end

When(/^select asset owned and apply action "([^"]*)"$/) do |arg1|
		if arg1 == "Mark as sold"
			find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
			# sleep 1
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[1]/a').click
		elsif arg1 == "Mark as public"
			find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
			#sleep 1
		end
		if ENV["BROWSER_MODE"] == "true"
			page.driver.browser.switch_to.alert.accept
		else
			page.execute_script 'window.confirm = function () { return true }'
		end
end

Then(/^it should through successfully update owned asset to marked as sold$/) do
	find(:xpath, '//*[@id="asset-management-header"]').click
	# sleep 1
	find(:xpath, '//*[@id="asset-mangement-filters"]/div[5]/div/label').click
	find(:xpath, '//*[@id="new_filter_search"]/div[8]/button').click
	sleep 1
	expect(page).to have_css("table#table-assets tr", :count=>1)
	#sleep 2
end


When(/^select rental asset and apply action "([^"]*)"$/) do |arg1|
 	reset_assets_filter
 	if arg1 == "Mark as sold"
 		puts "Sold assets does not have checkboxes and mass action any more."
 	elsif arg1 == "Mark as public"
		check("asset-selected-#{@rental_asset1.id}")
		#sleep 2
		find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
		#sleep 2
		find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
		#sleep 1
 	end
end

Then(/^it should through error message$/) do
	if ENV["BROWSER_MODE"] == "true"
		page.driver.browser.switch_to.alert.accept
	else
		page.execute_script 'window.confirm = function () { return true }'
	end
end

Then(/^Apply filter with sold asset there should be only (\d+) asset$/) do |arg1|
	begin
		find(:xpath, '//*[@id="asset-management-header"]').click
		#sleep 1
		find(:xpath, '//*[@id="asset-mangement-filters"]/div[5]/div/label').click
		#sleep 1
		find(:xpath, '//*[@id="apply-filters"]').click
		sleep 2
		expect(page).to have_css("table#table-assets tr", :count=>arg1)
			#sleep 2
	rescue
		count = 0
		Asset.all.each do |asset|
			if asset.sold?
				count += 1
			end
		end
		if(count)
			puts "Asset sold count is 1"
		else
			puts "Asset sold count is "+count
		end
	end
end

When(/^select for\-sale asset and apply action "([^"]*)"$/) do |arg1|
 	reset_assets_filter
 	if arg1 == "Mark as sold"
		find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
		# check("asset-selected-#{@for_sale1.id}")
		#sleep 2
		find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
		#sleep 2
		find(:xpath, '//*[@id="action-dropdown"]/li[1]/a').click
		#sleep 1
	elsif arg1 == "Mark as public"
		check("asset-selected-#{@for_sale1.id}")
		#sleep 2
		find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
		#sleep 2
		find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
		#sleep 1
 	end
end

Then(/^it should through successfully marked as sold$/) do
	if ENV["BROWSER_MODE"] == "true"
		page.driver.browser.switch_to.alert.accept
	else
		page.execute_script 'window.confirm = function () { return true }'
	end
	# if ENV["BROWSER_MODE"] == true
	# 	expect(page).to have_css("table#table-assets tr", :count=>2)
	# end
end

Then(/^it should through successfully marked$/) do
	if ENV["BROWSER_MODE"] == "true"
		page.driver.browser.switch_to.alert.accept
	else
		page.execute_script 'window.confirm = function () { return true }'
	end
	# if ENV["BROWSER_MODE"] == true
	# 	expect(page).to have_css("table#table-assets tr", :count=>1)
	# end
end

When(/^select for\-rent asset and apply action "([^"]*)"$/) do |arg1|
 	reset_assets_filter
 	if arg1 == "Mark as sold"
		find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
		# check("asset-selected-#{@for_rent1.id}")
		#sleep 2
		find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
		#sleep 2
		find(:xpath, '//*[@id="action-dropdown"]/li[1]/a').click
		#sleep 1
	elsif arg1 == "Mark as public"
		check("asset-selected-#{@for_rent1.id}")
		#sleep 2
		find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
		#sleep 2
		find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
		#sleep 1
 	end
end

Then(/^it should through error$/) do

end


Given(/^company_user on Assets Page (\d+) assets of all kinds\( Rental, Owned, For\-Sale, For\-Rent\)$/) do |arg1|
	create_four_assets @company_admin
	visit assets_path
end

When(/^select asset and apply action "([^"]*)" on assets page$/) do |arg1|
	find(:xpath, "//*[@id='asset_#{@rental_asset1.id}']/td[1]/label").click
	find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
end

Then(/^there should be no option for Mark as Sold$/) do
	expect(page.find('#action-dropdown')).not_to have_content("Mark as Sold")
end
