When(/^select asset "([^"]*)" and apply action "([^"]*)" on group detail page$/) do |arg1, arg2|
  if ENV["BROWSER_MODE"] == true
    if arg1 == "owned"
      find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
    elsif arg1 == "rental"
      find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
    elsif arg1 == "for-sale"
      find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
    elsif arg1 == "for-rent"
      find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
    end
  else
    if arg1 == "owned"
      find(:xpath, "//*[@id='asset_#{@owned_asset1.id}']/td[1]/label").click
    elsif arg1 == "rental"
      find(:xpath, "//*[@id='asset_#{@rental_asset1.id}']/td[1]/label").click
    elsif arg1 == "for-sale"
      find(:xpath, "//*[@id='asset_#{@for_sale1.id}']/td[1]/label").click
    elsif arg1 == "for-rent"
      find(:xpath, "//*[@id='asset_#{@for_rent1.id}']/td[1]/label").click
    end
  end

  if arg1 == "for-rent"
    if ENV["BROWSER_MODE"] == "true"
      page.execute_script("window.scrollTo(0,0)");
      sleep 2
    else
      puts "Scroll down in open browser"
    end
  end

  find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
  find(:xpath, '//*[@id="action-dropdown"]/li[3]/a').click
end

When(/^select asset "([^"]*)" and apply action "([^"]*)" on group detail page for leader$/) do |arg1, arg2|
  # if ENV["BROWSER_MODE"] == "true"
    if arg1 == "owned"
      find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
    elsif arg1 == "rental"
      page.driver.browser.switch_to.alert.accept
      find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
    elsif arg1 == "for-sale"
      page.driver.browser.switch_to.alert.accept
      find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
    elsif arg1 == "for-rent"
      page.driver.browser.switch_to.alert.accept
      find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
    end
  # else
  #   if arg1 == "owned"
  #     find(:xpath, "//*[@id='asset_#{@owned_asset1.id}']/td[1]/label").click
  #     # check("asset-selected-#{@owned_asset1.id}")
  #   elsif arg1 == "rental"
  #     find(:xpath, "//*[@id='asset_#{@rental_asset1.id}']/td[1]/label").click
  #     # check("asset-selected-#{@rental_asset1.id}")
  #   elsif arg1 == "for-sale"
  #     find(:xpath, "//*[@id='asset_#{@for_sale1.id}']/td[1]/label").click
  #     # check("asset-selected-#{@for_sale1.id}")
  #   elsif arg1 == "for-rent"
  #     find(:xpath, "//*[@id='asset_#{@for_rent1.id}']/td[1]/label").click
  #     # check("asset-selected-#{@for_rent1.id}")
  #   end
  # end

  find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
  find(:xpath, '//*[@id="action-dropdown"]/li[1]/a').click
end

Given(/^User on Group Detail Page assets of all kinds for leader$/) do
  create_four_assets @company_user
  sleep 1
  link_all_assets_to_group
  sleep 1
  visit group_path :id => Group.last.id
end

Then(/^it should successfully update project of "([^"]*)" asset on group page$/) do |arg1|
  if ENV["BROWSER_MODE"] == true
    if arg1 == "owned"
      expect(find('#flash')).to have_content("Successfully updated 1 asset.")
      expect(page).to have_css("table#table-assets tr", count: 3)
    elsif arg1 == "rental"
      expect(find('#flash')).to have_content("Successfully updated 1 asset.")
      expect(page).to have_css("table#table-assets tr", count: 2)
    elsif arg1 == "for-sale"
      expect(find('#flash')).to have_content("Successfully updated 1 asset.")
      expect(page).to have_css("table#table-assets tr", count: 1)
    elsif arg1 == "for-rent"
      expect(find('#flash')).to have_content("Successfully updated 1 asset.")
      expect(page).to have_css("table#table-assets tr", count: 0)
    end
  end
end

When(/^select asset "([^"]*)" and apply action "([^"]*)" on group detail page for field_member$/) do |arg1, arg2|
  if ENV["BROWSER_MODE"] == true
    if arg1 == "owned"
      find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
    elsif arg1 == "rental"
      find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
    elsif arg1 == "for-sale"
      find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
    elsif arg1 == "for-rent"
      find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
    end
  else
    if arg1 == "owned"
      find(:xpath, "//*[@id='asset_#{@owned_asset1.id}']/td[1]/label").click
    elsif arg1 == "rental"
      find(:xpath, "//*[@id='asset_#{@rental_asset1.id}']/td[1]/label").click
    elsif arg1 == "for-sale"
      find(:xpath, "//*[@id='asset_#{@for_sale1.id}']/td[1]/label").click
    elsif arg1 == "for-rent"
      find(:xpath, "//*[@id='asset_#{@for_rent1.id}']/td[1]/label").click
    end
  end

  if arg1 == "for-rent"
    if ENV["BROWSER_MODE"] == "true"
      page.execute_script("window.scrollTo(0,0)");
      sleep 2
    else
      puts "Scroll down in open browser"
    end
  end

  find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
  find(:xpath, '//*[@id="action-dropdown"]/li[1]/a').click
end
