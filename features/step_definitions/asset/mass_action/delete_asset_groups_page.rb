

When(/^select asset owned and apply action "([^"]*)" on group page$/) do |arg1|
	if ENV["BROWSER_MODE"] == "true"
		find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
		#sleep 2
		find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
		#sleep 2
		find(:xpath, '//*[@id="action-dropdown"]/li[5]/a').click
		#sleep 1
		page.driver.browser.switch_to.alert.accept
		#sleep 1
	end
end

Then(/^there should be (\d+) assets on group page$/) do |arg1|
	if ENV["BROWSER_MODE"] == "true"
	  expect(page).to have_css("table#table-assets tr", :count=>arg1)
	end
end

When(/^select rental asset and apply action "([^"]*)" on group page$/) do |arg1|
	if ENV["BROWSER_MODE"] == "true"
		find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
		#sleep 2
		find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
		#sleep 2
		find(:xpath, '//*[@id="action-dropdown"]/li[5]/a').click
		#sleep 1
		page.driver.browser.switch_to.alert.accept
		#sleep 1
	end
end

When(/^select for\-sale asset and apply action "([^"]*)" on group page$/) do |arg1|
	if ENV["BROWSER_MODE"] == "true"
		find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
		#sleep 2
		find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
		#sleep 2
		find(:xpath, '//*[@id="action-dropdown"]/li[5]/a').click
		#sleep 1
		page.driver.browser.switch_to.alert.accept
		#sleep 1
	end
end

When(/^select for\-rent asset and apply action "([^"]*)" on group page$/) do |arg1|
	if ENV["BROWSER_MODE"] == "true"
		find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
		#sleep 2
		find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
		#sleep 2
		find(:xpath, '//*[@id="action-dropdown"]/li[5]/a').click
		#sleep 1
		page.driver.browser.switch_to.alert.accept
		#sleep 1
	end
end

Given(/^User on Group Detail Page (\d+) assets of all kinds for field_member$/) do |arg1|
	create_four_assets @field_member
	create_a_group_for @field_member
	link_all_assets_to_group
	visit group_path id: Group.last.id
end

When(/^select asset and apply action "([^"]*)" on group page$/) do |arg1|
	find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
	#sleep 2
	find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
	#sleep 2
end


When(/^select "([^"]*)" and apply action Deleted on groups page$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
		if arg1 == "owned"
		 	find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[3]/a').click
		elsif arg1 == "rental"
		 	find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[3]/a').click
			#sleep 1
		elsif arg1 == "for-sale"
		 	find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[3]/a').click
		elsif arg1 == "for-rent"
		 	find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[3]/a').click
			#sleep 1
		end
		page.driver.browser.switch_to.alert.accept
		#sleep 1
	end
end

Then(/^there should be (\d+) assets on groups page$/) do |arg1|
	if ENV["BROWSER_MODE"] == "true"
	  expect(page).to have_css("table#table-assets tr", :count=>arg1)
	end
end

Given(/^User on Group Detail Page (\d+) assets of all kinds for member$/) do |arg1|
	create_four_assets @company_user
	#sleep 1
	link_all_assets_to_group
	#sleep 1
	visit group_path id: @group.id
end
