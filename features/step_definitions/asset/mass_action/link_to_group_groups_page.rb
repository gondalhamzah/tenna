Given(/^User on Group Detail Page (\d+) assets of all kinds\( Rental, Owned, For\-Sale, For\-Rent\)$/) do |arg1|
  create_four_assets @company_admin
  create_a_group
  link_all_assets_to_group
  visit group_path id: Group.last.id
end

When(/^select asset "([^"]*)" and apply action link to group on group page$/) do |arg1|
  if ENV["BROWSER_MODE"] == true
    if arg1 == "owned"
      find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
      find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
      find(:xpath, '//*[@id="action-dropdown"]/li[4]/a').click
      select "#{@group2.name}", :from => "group_id"
      within '#link-to-group' do
        click_on "Link to Group"
      end
    elsif arg1 == "rental"
      find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
      find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
      find(:xpath, '//*[@id="action-dropdown"]/li[4]/a').click
      select "#{@group2.name}", :from => "group_id"
      within '#link-to-group' do
        click_on "Link to Group"
      end
    elsif arg1 == "for-sale"
      find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
      find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
      find(:xpath, '//*[@id="action-dropdown"]/li[4]/a').click
      select "#{@group2.name}", :from => "group_id"
      within '#link-to-group' do
        click_on "Link to Group"
      end
    elsif arg1 == "for-rent"
      find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
      find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
      find(:xpath, '//*[@id="action-dropdown"]/li[4]/a').click
      select "#{@group2.name}", :from => "group_id"
      within '#link-to-group' do
        click_on "Link to Group"
      end
    end
  end
end

Then(/^it should successfully update group of "([^"]*)"asset on group page$/) do |arg1|
  if ENV["BROWSER_MODE"] == true
    if arg1 == "owned"
      expect(page).to have_css("table#table-assets tr", :count=>3)
    elsif arg1 == "rental"
      expect(page).to have_css("table#table-assets tr", :count=>2)
    elsif arg1 == "for-sale"
      expect(page).to have_css("table#table-assets tr", :count=>1)
    elsif arg1 == "for-rent"
      expect(page).to have_css("table#table-assets tr", :count=>0)
    end
  end
end

When(/^select asset "([^"]*)" and apply action link to group on group page for field_member$/) do |arg1|
  if ENV["BROWSER_MODE"] == true
    if arg1 == "owned"
      find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
      find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
      find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
      select "#{@group2.name}", :from => "group_id"
      within '#link-to-group' do
        click_on "Link to Group"
      end
    elsif arg1 == "rental"
      find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
      find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
      find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
      select "#{@group2.name}", :from => "group_id"
      within '#link-to-group' do
        click_on "Link to Group"
      end
    elsif arg1 == "for-sale"
      find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
      find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
      find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
      select "#{@group2.name}", :from => "group_id"
      within '#link-to-group' do
        click_on "Link to Group"
      end
    elsif arg1 == "for-rent"
      find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
      find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
      find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
      select "#{@group2.name}", :from => "group_id"
      within '#link-to-group' do
        click_on "Link to Group"
      end
    end
  end
end
