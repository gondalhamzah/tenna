
When(/^select asset "([^"]*)" and apply action link to group for field_member$/) do |arg1|
	if ENV["BROWSER_MODE"] == true
	 	if arg1 == "owned"
		 	find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end
		elsif arg1 == "rental"
		 	find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end

		elsif arg1 == "for-sale"
		 	find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end

		elsif arg1 == "for-rent"
		 	find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end
		end
	end
end


When(/^select asset "([^"]*)" and apply action link to group$/) do |arg1|
	if ENV["BROWSER_MODE"] == true
	 	if arg1 == "owned"
		 	find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[4]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end
		elsif arg1 == "rental"
		 	find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[4]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end

		elsif arg1 == "for-sale"
		 	find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[4]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end

		elsif arg1 == "for-rent"
		 	find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[4]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end
		end
	end
end

When(/^link asset to group$/) do
end

Then(/^it should successfully update group of "([^"]*)"asset$/) do |arg1|
	if ENV["BROWSER_MODE"] == true
		if arg1 == "owned"
			within '#asset_2' do
				expect(page).to have_content(@group.name)
			end
		elsif arg1 == "rental"
			within '#asset_1' do
				expect(page).to have_content(@group.name)
			end
		elsif arg1 == "for-sale"
			within '#asset_3' do
				expect(page).to have_content(@group.name)
			end
		elsif arg1 == "for-rent"
			within '#asset_4' do
				expect(page).to have_content(@group.name)
			end
		end
	end
end

When(/^select asset "([^"]*)" and apply action link to group for member$/) do |arg1|
	if ENV["BROWSER_MODE"] == true
	 	if arg1 == "owned"
		 	find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end
		elsif arg1 == "rental"
		 	find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end
		elsif arg1 == "for-sale"
		 	find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
			#sleep 2
			find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
			#sleep 2
			find(:xpath, '//*[@id="action-dropdown"]/li[2]/a').click
			#sleep 1
		 	within '#link-to-group' do
		 		click_on "Link to Group"
		 	end
		end
	end
end
