When(/^select asset owned and apply action "([^"]*)" on assets page$/) do |arg1|
  begin
    find(:xpath, "//*[@id='asset_#{@owned_asset1.id}']/td[1]/label").click
  rescue
    find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
  end

  find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
  within "#action-dropdown" do
    click_on "Delete"
  end

  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.switch_to.alert.accept
  else
    page.execute_script 'window.confirm = function () { return true }'
  end
end

Then(/^there should be (\d+) assets on assets page$/) do |arg1|
  expect(page).to have_css("table#table-assets tr", :count=>arg1)
end

When(/^select rental asset and apply action "([^"]*)" on assets page$/) do |arg1|
  find(:xpath, "//*[@id='asset_#{@rental_asset1.id}']/td[1]/label").click
  find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click

  within "#action-dropdown" do
    click_on "Delete"
  end

  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.switch_to.alert.accept
  else
    page.execute_script 'window.confirm = function () { return true }'
  end
end

When(/^select for\-sale asset and apply action "([^"]*)" on assets page$/) do |arg1|
  find(:xpath, "//*[@id='asset_#{@for_sale1.id}']/td[1]/label").click
  find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
  within "#action-dropdown" do
    click_on "Delete"
  end

  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.switch_to.alert.accept
  else
    page.execute_script 'window.confirm = function () { return true }'
  end
end

When(/^select for\-rent asset and apply action "([^"]*)" on assets page$/) do |arg1|
  find(:xpath, "//*[@id='asset_#{@for_rent1.id}']/td[1]/label").click
  find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click

  within "#action-dropdown" do
    click_on "Delete"
  end

  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.switch_to.alert.accept
  else
    page.execute_script 'window.confirm = function () { return true }'
  end
end

Given(/^field_member is login$/) do
  login_as_field_member
  visit new_group_path
  fill_in "Name", with: "Group 1"
  find(:xpath, '//*[@id="new_group"]/div[1]/div[1]/div[4]/div[1]/span/span[1]/span').click
  all(:css, "#select2-group_project_id-results li")[1].click
  click_on "Create Group"
  sleep 2
  @group = Group.last
  visit new_group_path
  fill_in "Name", with: "Group 2"
  find(:xpath, '//*[@id="new_group"]/div[1]/div[1]/div[4]/div[1]/span/span[1]/span').click
  all(:css, "#select2-group_project_id-results li")[1].click
  click_on "Create Group"
  sleep 2
  @group2 = Group.last
end

Given(/^field_member is login for wanted assets$/) do
  login_as_field_member
end

Given(/^User on Assets Page (\d+) assets of all kinds for field_member$/) do |arg1|
  create_four_assets @field_member
  visit assets_path
end

Then(/^there should be no option for delete$/) do
  expect(page.find('#action-dropdown')).not_to have_content("Delete")
end

Given(/^project leader is login$/) do
  login_as_leader
end

Given(/^Create a group$/) do
  visit new_group_path
  fill_in "Name", with: "Group 2"
  find(:xpath, '//*[@id="new_group"]/div[1]/div[1]/div[4]/div[1]/span/span[1]/span').click
  all(:css, "#select2-group_project_id-results li")[1].click
  click_on "Create Group"
  sleep 2
  @group2 = Group.last
end

Given(/^project leader is login for wanted assets$/) do
  login_as_leader
end

Given(/^User on asset Detail Page (\d+) assets of all kinds for leader$/) do |arg1|
  create_owned_asset_for @company_admin
  create_for_sale_asset_for @company_admin
  create_four_assets @company_user
  visit assets_path
end

Given(/^User on Group Detail Page (\d+) assets of all kinds for leader$/) do |arg1|
  create_four_assets @company_user
  link_all_assets_to_group
  visit group_path id: Group.last.id
end

Given(/^member is login$/) do
  login_as_member
end

Given(/^User on asset Detail Page (\d+) assets of all kinds for member$/) do |arg1|
  create_four_assets @company_user
  visit assets_path
end

When(/^select "([^"]*)" and apply action Deleted on assets page$/) do |arg1|
  if arg1 == "owned"
    find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
    find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
    within "#action-dropdown" do
      click_on "Delete"
    end
  elsif arg1 == "rental"
    find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
    find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
    within "#action-dropdown" do
      click_on "Delete"
    end
  elsif arg1 == "for-sale"
    find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
    find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
    within "#action-dropdown" do
      click_on "Delete"
    end
  elsif arg1 == "for-rent"
    find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
    find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
    within "#action-dropdown" do
      click_on "Delete"
    end
  end

  if ENV["BROWSER_MODE"] == "true"
    page.driver.browser.switch_to.alert.accept
  else
    page.execute_script 'window.confirm = function () { return true }'
  end
end
