When(/^select asset "([^"]*)" and apply action "([^"]*)"$/) do |arg1, arg2|
  if ENV["BROWSER_MODE"] == true
    if arg1 == "owned"
      find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
    elsif arg1 == "rental"
      find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
    elsif arg1 == "for-sale"
      find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
    elsif arg1 == "for-rent"
      find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
    end

    find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
    find(:xpath, '//*[@id="action-dropdown"]/li[3]/a').click
  end
end


When(/^change project of asset$/) do
  if ENV["BROWSER_MODE"] == true
    select "#{@project2.name}", :from => "project_id"
    within '#reallocate-asset-mass-action' do
      click_on "Reallocate"
    end
  end
end

Then(/^it should successfully update project of that asset$/) do
end

Then(/^it should successfully update project of "([^"]*)"asset$/) do |arg1|
  if ENV["BROWSER_MODE"] == true
  if arg1 == "owned"
    within '#asset_2' do
      expect(page).to have_content(@project2.name)
    end
  elsif arg1 == "rental"
    within '#asset_1' do
      expect(page).to have_content(@project2.name)
    end
  elsif arg1 == "for-sale"
    within '#asset_3' do
      expect(page).to have_content(@project2.name)
    end
  elsif arg1 == "for-rent"
    within '#asset_4' do
      expect(page).to have_content(@project2.name)
    end
  end
  end
end

When(/^select asset "([^"]*)" and apply action "([^"]*)" for field_member$/) do |arg1, arg2|
  if arg1 == "owned"
    find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
  elsif arg1 == "rental"
    find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
  elsif arg1 == "for-sale"
    find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
  elsif arg1 == "for-rent"
    find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
  end

  if arg1 == "for-rent"
    if ENV["BROWSER_MODE"] == "true"
      page.execute_script("window.scrollTo(0,0)");
      sleep 2
    else
      puts "Scroll down in open browser"
    end
  end

  find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
  find(:xpath, '//*[@id="action-dropdown"]/li[1]/a').click
end


Given(/^User on Assets Page (\d+) assets of all kinds for leader$/) do |arg1|
  create_four_assets @company_user
  assign_project_role_leader @company_user
  visit assets_path
end

When(/^select asset "([^"]*)" and apply action "([^"]*)" for leader$/) do |arg1, arg2|
  if ENV["BROWSER_MODE"] == true
    if arg1 == "owned"
      find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
    elsif arg1 == "rental"
      find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
    elsif arg1 == "for-sale"
      find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
    elsif arg1 == "for-rent"
      find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
    end
  else
    if arg1 == "owned"
      find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
    elsif arg1 == "rental"
      find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
    elsif arg1 == "for-sale"
      find(:xpath, '//*[@id="asset_3"]/td[1]/label').click
    elsif arg1 == "for-rent"
      find(:xpath, '//*[@id="asset_4"]/td[1]/label').click
    end
  end

  if arg1 == "for-rent"
    if ENV["BROWSER_MODE"] == "true"
      page.execute_script("window.scrollTo(0,0)");
      sleep 2
    else
      puts "Scroll down in open browser"
    end
  end

  find(:xpath, '//*[@id="panel-list"]/div/div[2]/div[1]/div/form/div[1]/a').click
  find(:xpath, '//*[@id="action-dropdown"]/li[3]/a').click
end
