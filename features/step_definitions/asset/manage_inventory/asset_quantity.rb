Then(/^visit bsqr asset page$/) do
  visit qr_scanning_asset_path(Asset.last, with_tracking_code: true)
  sleep 2
  find(:css, "#qr-manage-inventory").click
end

Then(/^fill and verify quantity and inventory calculation$/) do
  fill_in "asset_price", with: '50'
  find(:xpath, "//*[@id='edit_asset_1']/div[1]/div[1]/div[14]/div[1]/label").click
  find(:xpath, "//*[@id='asset_market_value']").value == "50"
  find(:xpath, "//*[@id='asset_quantity']").set("100")
  find(:xpath, "//*[@id='asset_market_value']").value == "50"
  find(:xpath, "//*[@id='asset_market_value']").value == "5000"
end

Then(/^goto new assets page from bsqr$/) do
  find(:css, '#manage-inventory-div > div > form > div.row.page-size > div:nth-child(2) > div > input.qtyplus').click
  find(:css, '#manage-inventory-div > div > form > div.row.page-size > div:nth-child(2) > div > input.qtyplus').click
  find(:xpath, "//*[@id='uperstatus']/span[2]/a").click
  find(:xpath, "//*[@id='asset_1']").click
  find(:xpath, "//*[@id='assets-manage-inventory']/main/div/form/div[3]/div[2]/input[3]").click
end

Then(/^goto assets bsqr link and move asset quantity$/) do
  find(:xpath, "//*[@id='i12']").set('2')
  find(:xpath, "//*[@id='uperstatus']/span[2]/a").click
  find(:xpath, "//*[@id='asset_1']/td[2]/div/a/div[1]").click
  find(:xpath, "//*[@id='assets-manage-inventory']/main/div/form/div[3]/div[2]/input[3]").click
end

Then(/^update inventory quantity$/) do
  find(:xpath, "//*[@id='asset_quantity']").set("10")
  find(:xpath, "//*[@id='edit_asset_1']/div[1]/div[1]/div[14]/div[1]/label").click
end

Then(/^enable inventory for last asset$/) do
  asset = Asset.last
  asset.is_inventory = true
  asset.quantity = 10
  asset.save
end

Then(/^goto edit last asset$/) do
  visit edit_asset_path(Asset.last)
end

And(/^fill quantity and inventory$/) do
  find(:xpath, "//*[@id='asset_quantity']").set(10)
  find(:xpath, "//*[@id='edit_asset_2']/div[1]/div[1]/div[14]/div[1]/label").click
  find(:xpath, "//*[@id='asset_market_value']").set(1000000)
end