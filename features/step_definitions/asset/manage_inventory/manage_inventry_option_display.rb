Then(/^visit QR scanning page$/) do
  visit qr_scanning_asset_path(Asset.last, with_tracking_code: true)
end

Then(/^there should not be manage inventory button$/) do
  expect(page).to have_no_content("Manage Inventory")
end

Then(/^there should be manage inventory button$/) do
  find(:css, '#qr-manage-inventory').present?
end

Then(/^check inventory asset check box$/) do
  page.execute_script("$('#asset_is_inventory').click()")
end

Then(/^uncheck inventory asset check box$/) do
  find(:xpath, "//*[@id='edit_asset_#{Asset.last.id}']/div[1]/div[1]/div[14]/div[1]/label").click
end

Then(/^verify filter presence$/) do
  expect(page).to have_content('asset reports filter')
end

Then(/^verify filter absence$/) do
  expect(page).to have_no_content('asset reports filter')
end

Then(/^click on myfilters$/) do
  find('#asset_reports_tabs a', text: "My Filters").click
end

Then(/^click on useFilter$/) do
  find(:xpath, '//*[@id="my_filter_2"]/td[3]/a[1]').click
end

Then(/^click on delete asset link$/) do
  begin
    find('#my-filter').click
  rescue
    begin
      find(:xpath, '//*[@id="my-filter"]').click
    rescue
      page.execute_script("$('#my-filter').trigger('click')")
    end
  end
end

Then(/^verify delete popup is opened$/) do
  expect(page).to have_content("Are you sure you want to delete asset reports filter?")
end

Then(/^delete asset filter$/) do
  sleep 10
  find(:xpath, '//*[@id="asset-reports-index"]/div[7]/div/div[10]/button[1]').click
end

Then(/^visit assets reports page$/) do
  visit asset_reports_path
end

Then(/^click on trackers tab$/) do
  find(:xpath, '//*[@id="tab-reports"]/div/div/div[1]/div/div/div[1]/button[2]').click
end

Then(/^verify first dropdown values$/) do
  find(:xpath, '//*[@id="react-select-2--value"]/div[1]').click
  within ".Select-menu-outer" do
    all(".Select-menu .Select-option")[0].click
  end

  find(:xpath, '//*[@id="tab-reports"]/div/div/div[1]/div/div/div[2]/div[4]/div[2]/div/div/div[1]/div/div[2]/div/div/span[3]').click
  within ".Select-menu-outer" do
    all(".Select-menu .Select-option")[0].text == "Exists"
    all(".Select-menu .Select-option")[1].text == "RFID"
    all(".Select-menu .Select-option")[2].text == "QR"
    all(".Select-menu .Select-option")[3].text == "UPC"
  end
end

Then(/^verify second dropdown values$/) do
  find(:xpath, '//*[@id="react-select-2--value"]/div[1]').click

  within ".Select-menu-outer" do
    all(".Select-menu .Select-option")[1].click
  end

  find(:xpath, '//*[@id="tab-reports"]/div/div/div[1]/div/div/div[2]/div[4]/div[2]/div/div/div[1]/div/div[2]/div/div/span[3]').click

  within ".Select-menu-outer" do
    all(".Select-menu .Select-option")[0].text == "Exists"
    all(".Select-menu .Select-option")[1].text == "Cellular"
    all(".Select-menu .Select-option")[2].text == "LoRa"
    all(".Select-menu .Select-option")[3].text == "Bluetooth"
    all(".Select-menu .Select-option")[3].text == "Sigfox"
  end
end

Then(/^subtract assets from inventory$/) do
  find(:css, ".qtyminus").click
end

Then(/^add assets to inventory$/) do
  find(:css, ".qtyplus").click
end

Then(/^check if save button "([^"]*)"$/) do |arg1|
  if arg1 == "enabled"
    within "#qr-inventory-form" do
      find(:css, '.reallocation-save').native.style('background-color') == "rgba(253, 79, 0, 1)"
    end
  elsif arg1 == "disabled"
    within "#qr-inventory-form" do
      find(:css, '.reallocation-save').native.style('background-color') == "rgba(204, 204, 204, 1)"
    end
  end
end

Then(/^visit move quantity page$/) do
  within "#lowrstatus" do
    find(:css, '.movebtn').click
  end
end

Then(/^check field "([^"]*)" value "([^"]*)"$/) do |arg1, arg2|
  find(:css, arg1).value == "10"
end

Then(/^check if category dropdown disabled$/) do
  find(:xpath, '//*[@id="new_asset"]/div[1]/div[2]/div[3]/div/span')[:class].include? "disabled"
end

Then(/^click on cancel button$/) do
  find(:xpath, '//*[@id="new_asset"]/div[2]/div/div/a').click
end

Then(/^verify current page is asset detail page of "([^"]*)"$/) do |asset_id|
  current_url.include? "/assets/" + Asset.find(asset_id).title.downcase
end

Then(/^click on continue from popup$/) do
  within '#disconnect_inventory_modal' do
    find(:css, '.btn_continue_inventory').click
  end
end

Then(/^move inventory to another asset$/) do
  find(:css, '#asset-2-1').click
  within ".inventoryModal-footer" do
    find(:css, '.btn_continue_move_asset').click
  end
end

Then(/^verify successfully popup opened$/) do
  expect(page).to have_content("Successfully Moved!")
end

Then(/^verify asset quantity to be (\d+)$/) do |asset_quantity|
  find(:css, '#asset_quantity').value == asset_quantity
end

When(/^goto first asset edit page$/) do
  visit edit_asset_path id: Asset.first.id
end

Then(/^verify new value "([^"]*)"$/) do |newval|
  find(:css, '.newval').text == newval
end

Then(/^verify illegal char absence "([^"]*)"$/) do |arg1|
  expect(find(:xpath,'//*[@id="i12"]').value).not_to eq arg1
end

Then(/^verify inventory checkbox disabled$/) do
  find(:css, '.asset_is_inventory')['innerHTML'].include? 'disabled="disabled"'
end

Given(/^check if save button disabled$/) do
  find(:css, '#save-inventory')['class'].include? "disabled"
end
