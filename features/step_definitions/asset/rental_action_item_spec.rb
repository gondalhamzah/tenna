
Given(/^Select both assets and apply action mark as sold$/) do
	visit assets_path
	find(:xpath, '//*[@id="asset_2"]/td[1]/label').click
	find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
	find(:xpath, "//*[@id='panel-list']/div/div[2]/div[1]/div/form/div[1]/a").click
	find(:xpath, "//*[@id='action-dropdown']/li[1]/a").click
	if ENV["BROWSER_MODE"] == "true"
		page.driver.browser.switch_to.alert.accept
	end
end

Then(/^error occur "([^"]*)"$/) do |arg1|
	# expect(page).to have_content("Successfully sold 1 asset.")
end
 
Then(/^flash should occur$/) do
	expect(page).to have_content("Successfully updated 0 assets.")
end


Then(/^Select both assets and apply action mark as public$/) do
	visit assets_path
	find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
	find(:xpath, "//*[@id='panel-list']/div/div[2]/div[1]/div/form/div[1]/a").click
	find(:xpath, "//*[@id='action-dropdown']/li[2]/a").click
end

Then(/^select only owned asset and apply mark as sold$/) do
	visit assets_path
	find(:xpath, '//*[@id="asset_1"]/td[1]/label').click
	find(:xpath, "//*[@id='panel-list']/div/div[2]/div[1]/div/form/div[1]/a").click
	find(:xpath, "//*[@id='action-dropdown']/li[1]/a").click
end

Then(/^it should successfully apply$/) do
  # pending # Write code here that turns the phrase above into concrete actions
end

Then(/^goto project create page create a project with owned and rental asset$/) do

end

Then(/^Goto project page and Select owned assets as well as rental assets$/) do

end

Then(/^select mass action mark as sold$/) do
end

Then(/^select mass action mark as public$/) do
#  pending # Write code here that turns the phrase above into concrete actions
end

Then(/^unselect rental asset and apply mark as public$/) do
 # pending # Write code here that turns the phrase above into concrete actions
end

Then(/^successfull mark as public$/) do
  #pending # Write code here that turns the phrase above into concrete actions
end

Then(/^unselect rental asset and apply mark as sold$/) do
  # pending # Write code here that turns the phrase above into concrete actions
end

Then(/^successfull mark as sold$/) do
  # pending # Write code here that turns the phrase above into concrete actions
end

Then(/^goto group create page create a group with owned and rental asset$/) do
  # pending # Write code here that turns the phrase above into concrete actions
end

Then(/^Goto group page and Select owned assets as well as rental assets$/) do
  # pending # Write code here that turns the phrase above into concrete actions
end

