Then(/^click on category dropdown$/) do
  find(:xpath, '//*[@id="select2-filter_search_category-container"]').click
end

Then(/^verify atleast one subcategory$/) do
  expect(page).to have_content("Test Category 1")
end

Then(/^click on filter type category$/) do
  find(:xpath, '//*[@id="tab-reports"]/div/div/div[1]/div/div/div[1]/button[1]').click
end

Then(/^verify filter type category present$/) do
  find(:xpath, '//*[@id="tab-reports"]/div/div/div[1]/div/div/div[1]/button[1]').text == "Category"
end

Then(/^choose category dropdown item number "([^"]*)"$/) do |arg1|
  find(:xpath, '//*[@id="react-select-2--value"]/div[1]').click
  within ".Select-menu-outer" do
    all(".Select-menu .Select-option")[arg1.to_i].click
  end
end

Then(/^choose sub\-category dropdown item number "([^"]*)"$/) do |arg1|
  find(:xpath, '//*[@id="react-select-3--value"]/div[1]').click
  within ".Select-menu-outer" do
    all(".Select-menu .Select-option").each_with_index do |item,index|
      if index == arg1.to_i
        item.click
      end
    end
  end
end

Then(/^select second category from dropdown$/) do
  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: Category.second.name).click
end

Then(/^verify "([^"]*)" in all rows$/) do |arg1|
  all(:css, '#table-view tbody tr').each do |single_row|
    single_row.find(:css, 'td.sorting_1').text == arg1
  end
end

Then(/^verify category button not present$/) do
  all(:css, 'button').each do |btn_txt|
    if btn_txt.text.downcase == "category"
      false
    end
  end
end

Then(/^fill new asset filter name$/) do
  fill_in 'asset_filter_name', with: 'Some Title Here'
end

Then(/^fill update asset filter name$/) do
  fill_in 'asset_filter_name', with: 'Some Title Here Updated'
end


Then(/^verify category dropdown values$/) do
  find(:xpath, '//*[@id="react-select-2--value"]/div[1]').click
  within ".Select-menu-outer" do
    all(".Select-menu .Select-option").count == 1
    find(:css, '.Select-menu .Select-option').text == "Test Category Group 1"
  end
end

Then(/^verify text present "([^"]*)"$/) do |arg1|
  expect(page).to have_content(arg1)
end

Then(/^verify category dropdown not empty$/) do
  find(:xpath, '//*[@id="react-select-2--value"]/div[1]').click
  within ".Select-menu-outer" do
    all(".Select-menu .Select-option").count == 1
    find(:css, '.Select-menu .Select-option').text == "MyString"
  end
end

Then(/^verify message successfuly$/) do
  expect(page).to have_content("Filter is updated successfully")
end

Given(/^click asset remove icon$/) do
  find(:xpath,'//*[@id="nav-search-clear"]').click
end

