Then(/^apply asset management filter "([^"]*)"$/) do |filter|
  sleep 1
  find('#asset-management-header').click
  find('#asset-mangement-filters label', text: filter).click
  find('#asset-management-header').click
end

Then(/^apply asset type filter "([^"]*)"$/) do |filter|
  sleep 1
  find('#asset-type-header').click
  find("#asset-type-filters label", text: filter).click
  find('#asset-type-header').click
end

Then(/^select asset (\d+) on assets index$/) do |asset_id|
  find("#table-assets label[for='asset-selected-#{asset_id}']").click
end

Then(/^apply action "([^"]*)" on asset$/) do |performAction|
  find("#action-dropdown li", text: performAction).click

  if performAction.downcase == "delete"
    page.accept_alert
  end
end
