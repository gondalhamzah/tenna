Then(/^visit schedule it page$/) do
  visit event_index_path
end

When(/^edit and save the created event$/) do
  execute_script("$('.dhx_body').dblclick()")
  text_area = first(:css, 'textarea').native
  text_area.send_keys('Some text added to description')
  find(:xpath, "//*[@id='events-index']/div[1]/div[3]/div[2]").click
  expect(page.driver.browser.switch_to.alert.text).to eq("Please select a time in the future.")
  page.driver.browser.switch_to.alert.accept
  page.execute_script("$('.dhx_section_time select:last-child').val('2018')")
  page.execute_script("$('.dhx_section_time select:first-child').val('1410')")
  find(:xpath, "//*[@id='events-index']/div[1]/div[3]/div[2]").click
  sleep 2
end

When(/^check popup cancel button$/) do
  execute_script("$('.dhx_body').dblclick()")
  sleep 5
  text_area = first(:css, 'textarea').native
  text_area.send_keys('Some text added to description')
  find(:xpath, "//*[@id='events-index']/div[1]/div[4]/div[2]").click
  sleep 2
end

Given(/^check popup delete is working$/) do
  begin
    execute_script("$('.dhx_body').dblclick()")
    find(:xpath, "//*[@id='events-index']/div[1]/div[5]/div[2]").click
    sleep 2
    find(:xpath, "//*[@id='events-index']/div[6]/div[2]/div[1]/div").click
  rescue
    puts "awaiting image bug to be resolved"
  end
end

Then(/^verify saved changes$/) do
  expect(page).to have_content("Some text added to description")
end

Given(/^click on first asset$/) do
  find(:xpath, "//*[@id='asset_1']/td[3]/a/p[4]").click
end

Given(/^click on schedule maintenance icon$/) do
  find(:xpath, "//*[@id='assets-show']/main/div[3]/a").click
end

Then(/^visit assets page$/) do
  visit assets_path
end

Then(/^verify if maintenance is checked on modal$/) do
end

Then(/^visit schedule it page to verify radio buttons working$/) do
  visit event_index_path
  find(:xpath, "//*[@id='bs_scheduler']/div[4]/div[1]").click
end

Then(/^click on edit asset link$/) do
  find(:xpath, "//*[@id='asset_1']/td[3]/a")
  sleep 3
end

Then(/^verify if routing dropdown is working on modal$/) do
  page.execute_script("$('#bs_scheduler > div.dhx_cal_data > div.dhx_scale_holder_now').dblclick();")
  script = ".dhx_cal_radio :input[value=" + "'routing'" + "]"
  script = '"' + script + '"'
  page.execute_script("$(" + script.to_s + ").trigger('click')")
  page.execute_script("$(" + script.to_s + ").is(':checked')")
  find(:xpath, "//*[@id='events-index']/div[1]/div[2]/div[2]/div[2]/span/span[1]/span").click
end

Then(/^create an event from schedule it page$/) do
  page.execute_script("$('#bs_scheduler > div.dhx_cal_data > div.dhx_scale_holder_now').dblclick();")
  script = ".dhx_cal_radio :input[value=" + "'routing'" + "]"
  script = '"' + script + '"'
  page.execute_script("$(" + script.to_s + ").trigger('click')")
  page.execute_script("$(" + script.to_s + ").is(':checked')")
  # Routing dropdown
  find(:xpath, "//*[@id='events-index']/div[1]/div[2]/div[2]/div[2]/span/span[1]/span").click
  first(".select2-results__options").click
  find(:xpath, "//*[@id='events-index']/div[1]/div[2]/div[4]/div[2]/span/span[1]/span").click
  first(".select2-results__options").click
  page.execute_script("$('.dhx_section_time option[value=270]').attr('selected','selected');")
  page.execute_script("$('.dhx_section_time option[value=7]').attr('selected','selected');")
  page.execute_script("$('.dhx_section_time option[value=2017]').attr('selected','selected');")
  find(:xpath, "//*[@id='events-index']/div[1]/div[3]/div[2]").click
  if Event.last.start_date.to_date == "Tue, 01 Aug 2017".to_date
    puts "saved succesfully"
  else
    puts "Somethings wrong"
  end
end

Then(/^create event and cancel it$/) do
  page.execute_script("$('#bs_scheduler > div.dhx_cal_data > div.dhx_scale_holder_now').dblclick();")
  find(:xpath, "//*[@id='events-index']/div[1]/div[4]/div[2]").click
end

Then(/^delete created schedule$/) do
  page.execute_script("$('#bs_scheduler > div.dhx_cal_data > div.dhx_scale_holder_now > div > div.dhx_event_move.dhx_title').dblclick();")
  find(:xpath, "//*[@id='events-index']/div[1]/div[5]/div[2]").click
  find(:xpath, "//*[@id='events-index']/div[7]/div[2]/div[1]/div").click
end

Then(/^open event and verify cancel button working$/) do
  page.execute_script("$('#bs_scheduler > div.dhx_cal_data > div.dhx_scale_holder_now > div > div.dhx_event_move.dhx_title').dblclick();")
  find(:xpath, "//*[@id='events-index']/div[1]/div[4]/div[2]").click
end

Given(/^enable Schedule it Subscription$/) do
  Subscription.create([{company_id: Company.last.id, category: 'scheduleit',state: 'active',payment: true}])
end

Given(/^select category "([^"]*)" in active admin$/) do |arg1|
  select 'scheduleit', from: 'subscription_category'
end

Given(/^click on Create Subscription$/) do
  begin
    find(:xpath, '//*[@id="subscription_submit_action"]/input').click
  rescue Exception => e
    begin
      find("#subscription_submit_action > input[type='submit']").click
    rescue Exception => e
      page.execute_script("$('#subscription_submit_action > input[type='submit']').trigger('click')")
    end
  end
end

When(/^subscription count should be (\d+)$/) do |arg1|
  fiver = Subscription.count
  expect(fiver).to be(5)
end

When(/^check the Maintenance checkbox on edit$/) do
  find('#edit_asset_2 > div.form-inputs.row.separator-row > div:nth-child(2) > div:nth-child(6) > div > label').click
  find('#edit_asset_2 > div.form-inputs.row.separator-row > div:nth-child(2) > div:nth-child(7) > a').click
end

When(/^edit and verify event with past date not created$/) do
  execute_script("$('.dhx_body').dblclick()")
  text_area = first(:css, 'textarea').native
  text_area.send_keys('Some text added to description')
  find(:xpath, "//*[@id='events-index']/div[1]/div[3]/div[2]").click
  expect(page.driver.browser.switch_to.alert.text).to eq("Please select a time in the future.")
  sleep 2
end