Given(/^"([^"]*)" is present$/) do |arg1|
  @company_admin1 = FactoryGirl.create(arg1)
end

Then(/^create a company asset$/) do
  Chewy.strategy(:bypass)
  @category_group = FactoryGirl.create(:category_group)
  @project = FactoryGirl.create(:project, company: @company_admin1.company, contact: @company_admin1)
  @asset = FactoryGirl.create(:asset, company: @company_admin1.company, creator: @company_admin1, project: @project)
end

Then(/^add qr code for this asset$/) do
  @qr_code = FactoryGirl.create(:tracking_code, status: 1, resource: @asset, resource_type: 'Asset')
end

Then(/^create another "([^"]*)"$/) do |arg1|
  @company_admin2 = FactoryGirl.create(arg1)
end

Then(/^search the created asset$/) do
  @admin1_assets = @company_admin1.assets.ids
  @admin1_company_assets = @company_admin1.company.assets.ids
end

Then(/^company asset count should be (\d+)$/) do |arg1|
  expect(@company_admin2.assets.ids == @admin1_assets).to be false
  expect(@company_admin2.company.assets.ids == @admin1_company_assets).to be false
end

When(/^visit logged in company admin profile$/) do
  visit company_user_path(company_id: @company_admin.company.id, id: @company_admin.id )
end

Then(/^scan the asset with ping RFID in checklist$/) do
  ################################################################
  # page.execute_script("getAsset('ping', 'tab-checklist')")
  ################################################################
end

Then(/^user should not be clickable$/) do
  expect{ find('#companies-show > main > div.row > div:nth-child(1) > div > table > tbody > tr > td.default-contact.body-text15 a').click }.not_to raise_error(Capybara::Poltergeist::MouseEventFailed)
end

Then(/^Accept user invite$/) do
  User.last.update(invitation_accepted_at: Time.now)
end

Then(/^user should be clickable$/) do
  all(:css, "table")[1].find("a").click
  # find('#companies-show > main > div.row > div:nth-child(1) > div > table > tbody > tr > td.default-contact.body-text15 a').click
end

Then(/^verify "([^"]*)" field is focused$/) do |arg1|
  if arg1.downcase == "email"
    find(:css, '#user_email')["autofocus"] == "true"
  end
end

Then(/^open role dropdown$/) do
  find(:css, '#select2-user_role-container').click
end

Then(/^verify role dropdown values on invitation pages$/) do
  all("#select2-user_role-results li").count == 3
  all("#select2-user_role-results li")[0].text == "Company User"
  all("#select2-user_role-results li")[1].text == "Administrator"
  all("#select2-user_role-results li")[2].text == "Field"
end

Then(/^select "([^"]*)" from role dropdown$/) do |role|
  if role == "Company User"
    all("#select2-user_role-results li")[0].click
  elsif role == "Administrator"
    all("#select2-user_role-results li")[1].click
  elsif role == "Field"
    all("#select2-user_role-results li")[2].click
  else
    false
  end
end
