Given(/^Admin is login$/) do
  company = FactoryGirl.create(:admin_company)
  @admin = FactoryGirl.create(:admin_user)
  visit new_admin_user_session_path
  admin_login @admin
  expect(page).to have_content("Signed in successfully.")
end

Given(/^on company create page$/) do
  visit new_admin_company_path
end

Given(/^fill all require data$/) do
  fill_in "company_name", with: "Test Company"
  fill_in "company_contact_attributes_email", with: @admin.email
  fill_in "company_contact_attributes_first_name", with: "Test First Name"
  fill_in "company_contact_attributes_last_name", with: "Test Last Name"
  fill_in "company_address_attributes_line_1", with: "2045 Lincoln Hwy"
  fill_in "company_address_attributes_city", with: "Edison"
  fill_in "company_address_attributes_state", with: "NJ"
  fill_in "company_address_attributes_zip", with: "08817"
  select "United States", :from => "company_address_attributes_country"
  fill_in "company_project_attributes_name", with: "Project Test"
  fill_in "company_project_address_attributes_line_1", with: "Project Address"
  fill_in "company_project_address_attributes_city", with: "Edison"
  fill_in "company_project_address_attributes_state", with: "NJ"
  select "United States", :from => "company_project_address_attributes_country"
end

Given(/^click save button$/) do
  fill_in "company_project_address_attributes_zip", with: "08817"
  begin
    click_button "Create Company"
  rescue
    begin
      excute_script("$('#company_submit_action > input[type='submit']').trigger('click')")
    rescue
      begin
        find(:css, '#company_submit_action > input[type="submit"]').click
      rescue
        find(:xpath, "//*[@id='company_submit_action']/input").click
      end
    end
  end
end

Then(/^Company Create Successfully$/) do
  expect(page).to have_content("Displaying all 2 Companies")
end

Then(/^page Should throw an error to enter "([^"]*)"$/) do |arg1|
  if arg1 == "name of company"
    expect(page).to have_content("Name can't be blank")
    expect(page.find('#company_name_input')).to have_content("can't be blank")
  elsif arg1 == "first name of company contact"
    expect(page).to have_content("First name can't be blank")
    expect(page.find('#company_contact_attributes_first_name_input')).to have_content("can't be blank")
  elsif arg1 == "email of company contact"
    expect(page).to have_content("Email can't be blank")
    expect(page.find('#company_contact_attributes_email_input')).to have_content("can't be blank")
  elsif arg1 == "last name of company contact"
    expect(page).to have_content("Last name can't be blank")
    expect(page.find('#company_contact_attributes_last_name_input')).to have_content("can't be blank")
  # elsif arg1 == "address line of company"
    # expect(page).to have_content("Line 1 can't be blank")
    # expect(page.find('#company_address_attributes_line_1_input')).to have_content("can't be blank")
  # elsif arg1 == "city of company"
  #   expect(page).to have_content("City can't be blank")
  #   expect(page.find('#company_address_attributes_city_input')).to have_content("can't be blank")
  elsif arg1 == "default project name"
    expect(page).to have_content("Name can't be blank")
    expect(page.find('#company_project_attributes_name_input')).to have_content("can't be blank")
  elsif arg1 == "address of project for company"
    # expect(page).to have_content("Name can't be blank")
    # expect(page.find('#company_name_input')).to have_content("can't be blank")
  elsif arg1 == "city of project"
    # expect(page).to have_content("Name can't be blank")
    # expect(page.find('#company_name_input')).to have_content("can't be blank")
  elsif arg1 == "state of project"
    # expect(page).to have_content("Name can't be blank")
    # expect(page.find('#company_name_input')).to have_content("can't be blank")
  elsif arg1 == "zipcode of project"
    # expect(page).to have_content("Name can't be blank")
    # expect(page.find('#company_name_input')).to have_content("can't be blank")
  end
end

Then(/^visit company detail page$/) do
  page.first(:link, "View").click
end

Then(/^update Api rmp$/) do
  fill_in "company_api_rpm", with: "200"
end

Then(/^click on nav "([^"]*)"$/) do |arg1|
  within '#tabs' do
    click_on 'Companies'
  end
end

Then(/^click on Generate new Api key$/) do
  page.first(:link, "Generate New API Key").click
end

Then(/^Api RPM should be (\d+)$/) do |arg1|
  expect(page).to have_css(".row-api_rpm td", text: arg1)
end

Given(/^scroll to page end$/) do
  if ENV["BROWSER_MODE"] == "true"
    page.execute_script("window.scrollTo(0,document.body.scrollHeight);");
  else
    puts "Scroll down in open browser"
  end
end

Then(/^change user phone number$/) do
  fill_in 'user_phone_number', with: '+1 319.000.4444'
end

Then(/^fill the invite new user form$/) do
  fill_in 'user_email', with: 'internet@gmail.com'
  fill_in 'user_first_name', with: 'Aldaim'
  fill_in 'user_last_name', with: 'User'
  fill_in 'user_phone_number', with: '+1 319.222.3333'
end

Then(/^check invitation email is sent$/) do
  mail = ActionMailer::Base.deliveries.last
  mail.to[0].should == 'internet@gmail.com'
  User.last.update(invitation_accepted_at: Time.now)
end
