Then(/^assign company role button is not present$/) do
  expect(page).to have_no_content("Assign Company Role")
end

Given(/^go to asset page$/) do
  visit assets_path
end

Given(/^search company asset$/) do
  place = page.find_by_id('nav-search').native
  place.send_keys("test_company_B_asset")
  place.send_key "\xEE\x80\x83"
end

Given(/^go to site page$/) do
  visit projects_path
end

Given(/^perform site search$/) do
  place = page.find_by_id('nav-search').native
  place.send_keys("test_company_B_project")
  place.send_key "\xEE\x80\x83"
end

Then(/^group must populate$/) do
  place = page.find_by_id('group-nav-search').native
  place.send_keys("test_company_B_group")
  place.send_key "\xEE\x80\x83"
end

Given(/^create another company_admin_B$/) do
  @company_admin2 = FactoryGirl.create(:company_admin, company: FactoryGirl.create(:company))
end

Given(/^create asset with name "([^"]*)" company_admin_B$/) do |arg1|
  @company_b_asset = FactoryGirl.create(:asset, title: arg1, company: @company_admin2.company, creator: @company_admin2, project: @company_admin2.company.project)
end

Given(/^create project with name "([^"]*)" company_admin_B$/) do |arg1|
  @company_b_project = FactoryGirl.create(:project, name: arg1, company: @company_admin2.company, contact: @company_admin2)
end

Given(/^create group with name "([^"]*)" company_admin_B$/) do |arg1|
  project = FactoryGirl.create(:project, company: @company_admin2.company, contact: @company_admin2)
  @company_b_group = FactoryGirl.create(:group, name: arg1, company: @company_admin2.company, project: project)
end

Given(/^click on company tab$/) do
  find(:css, '#first-nav > div > ul.right > li:nth-child(2) > a').click
end

Given(/^click on user_name$/) do
  find(:css,'#companies-show > main > div.row > div:nth-child(2) > div > table > tbody > tr > td:nth-child(1) > a:nth-child(1)').click
end

Given(/^click on add assignment button$/) do
  find(:css,'.projects-actions a').click
end

Given(/^role drop down check$/) do
  expect(page).to have_css("#new_project_assignment > div.form-inputs.row.separator-row > div:nth-child(2) > div > div")
end
