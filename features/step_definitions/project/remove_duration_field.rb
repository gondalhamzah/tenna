Given(/^visit site creation page$/) do
	visit new_project_path
end

Then(/^should not be duration fields$/) do
	fill_in 'project_name', with: 'Some Project Name Here'
	fill_in 'project_description', with: 'Some description here'
	find('#next-step').click
	find(:xpath, "//*[@id='step-address']/div[4]/div[2]/button").click
end


Given(/^login company user with duration$/) do
	login_as_company_user_with_sites_and_duration
end

Given(/^visit site detail page$/) do
	visit project_path(@project.id)
end

Then(/^should be duration fields$/) do
	if @project.duration.present?
		puts 'Duration is present'
	else
		puts 'Duration doesnt exist for the project'
	end
end

Given(/^login company user "([^"]*)"$/) do |arg1|
	login_as_company_user_as_sites(arg1)
end

Then(/^site should be created succesfully$/) do
end
