Given(/^User on dashboard can see project on map$/) do
    @project = FactoryGirl.create(:project, contact: @company_admin)
    ProjectsIndex.import(group: @project)
    find(:css, "input[id='location-input']").set(@project.address.zip)
    find(:css, "input[id='location-input']").native.send_key(:enter)
end

Given(/^delete that project$/) do
  if ENV['BROWSER_MODE'] == "true"
    sleep 2
    visit project_path id: @project.id
    sleep 4
    execute_script("$('.btn-floating.btn-large').trigger('mouseenter').click();")
    sleep 2
    find(".icon-fab-trash").click
    page.driver.browser.switch_to.alert.accept
  end
end

Then(/^goto map project is not on map$/) do
  if ENV['BROWSER_MODE'] == "true"
    find('#logo-container').click
    find(:css, "input[id='location-input']").set(@project.address.zip)
    find(:css, "input[id='location-input']").native.send_key(:enter)
  end
end
