Then(/^"([^"]*)" assigns site role from site users page$/) do |arg1|
  if arg1 == "company admin"
    click_link('Site Users')
    expect(page).to have_content('Assign Existing User')
  elsif arg1 == "project leader"
    click_link('Site Users')
    expect(page).to have_content('Assign Existing User')
  end
end

Then(/^"([^"]*)" cannot assign site assignments$/) do |arg1|
  if arg1 == "member"
    expect(page).to have_no_content('Site Users')
  elsif arg1 == "field"
    expect(page).to have_no_content('Site Users')
  end
end

Then(/^create site no (\d+) with address "([^"]*)"$/) do |site_number, add_address|
  fill_in 'project_name', with: 'Test site ' + site_number
  fill_in 'project_description', with: 'Quidem deserunt qui atque labore sunt quis laborum. Et iste laudantium nobis adipisci delectus. Quod vero repudiandae magni repellat totam. Id ullam a aperiam et laboriosam. Voluptas aut perspiciatis o...'
  find(:css, "#next-step").click

  if add_address == "true"
    # Add address here
  end
end

Then(/^create site no (\d+)$/) do |site_number|
  fill_in 'project_name', with: 'Test site ' + site_number
  fill_in 'project_description', with: 'Quidem deserunt qui atque labore sunt quis laborum. Et iste laudantium nobis adipisci delectus. Quod vero repudiandae magni repellat totam. Id ullam a aperiam et laboriosam. Voluptas aut perspiciatis o...'
  find(:css, "#next-step").click
end

Then(/^click on import site button$/) do
  find(:css, '#bulk-project-upload-modal-open').click
end

Then(/^verify import sites popup opened$/) do
  find(:css, '#bulk-project-upload-modal').present?
  expect(page).to have_content("Import Sites")
end

Then(/^go to new sites page$/) do
  find(:css, '#projects-index > main > div.fixed-action-btn > a').click
end

Then(/^verify company contact dropdown is opened$/) do
  find(:css, '#select2-project_contact_id-container').click
end

Then(/^verify company contact dropdown is switching values$/) do
  selected_val = find(:css, '#select2-project_contact_id-container').text
  selected_val == Company.first.contact.name
  find(:css ,'#select2-project_contact_id-results li', text: "Select Contact").click
  selected_val = find(:css, '#select2-project_contact_id-container').text
  selected_val == "Select Contact"
  find(:css, '#select2-project_contact_id-container').click
  find(:css ,'#select2-project_contact_id-results li', text: Company.first.contact.name).click
  selected_val = find(:css, '#select2-project_contact_id-container').text
  selected_val == Company.first.contact.name
end
