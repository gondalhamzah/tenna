Then(/^invite users button is found$/) do
	find(:xpath, "//*[@id='companies-show']/main/div[1]/div[3]/div[1]/a")
end

Then(/^invite users button cannot be found$/) do
  expect(page).to have_no_content("Invite New User")
end
