Given(/^on project detail page Click groups button$/) do
  if ENV['BROWSER_MODE'] == "true"
    project_detail_page
  end
end

Then(/^user see the list of groups$/) do
  if ENV['BROWSER_MODE'] == "true"
    within(".tabs.groups") do
      click_on("Groups")
    end
  end
end

Given(/^on project detail page$/) do
  if ENV['BROWSER_MODE'] == "true"
    project_detail_page
  end
end

Given(/^click on menu and click on site$/) do
  find(:xpath, '//*[@id="desktop_main_vertical_nav_btn"]').click
  sleep 2
  find(:xpath, '//*[@id="desktop_main_vertical_nav_dropdown"]/li[7]').click
end

Given(/^click on a site$/) do
  find(:css, '#table-projects > tbody > tr > td > a').click
end

Given(/^click on import asset\-link$/) do
  find(:xpath, '//*[@id="modal-upload-spreadsheet-open"]').click
  sleep 2
  expect(page).to have_content("Import")
end

Given(/^project create$/) do
  @project = FactoryGirl.create(:project, contact: @company_admin)
  ProjectsIndex.import(project: @project)
end

Given(/^within drop down check$/) do
  expect(page).to have_content("Within Miles")
end
