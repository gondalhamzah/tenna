

Given(/^on project detail$/) do
  project_page
  #sleep 2
  visit project_path id: @project.id
  #sleep 2
end

Given(/^select a group in the Group section of the filter$/) do
  page.find('#additional-options-header').click
  if ENV['BROWSER_MODE'] == "true"
    page.find("#select2-filter_search_group-container").click
    #sleep 2
    page.find('#select2-filter_search_group-results', text: "#{@group_a.name}", exact: true).click
    #sleep 2
    find(:xpath, "//*[@id='new_filter_search']/div[8]/button").click
    # first(:link, 'Apply Filters').click
    #sleep 2
  else
    puts "Needs to be tested in open browser"
  end
end

Then(/^assets belonging to the selected group must list$/) do

end


Given(/^on group detail page with a close project$/) do
  project_page_close_group
  #sleep 2
  visit project_path id: @project.id
  #sleep 2
end

Then(/^group filter exclude that group$/) do
  page.find('#additional-options-header').click
  if ENV['BROWSER_MODE'] == "true"
    page.find("#select2-filter_search_group-container").click
    #sleep 2
    expect('#select2-filter_search_group-results').not_to have_content(@group_a.name)
  end
end


Given(/^on group detail page with a deleted project$/) do
  project_page_deleted_group
  #sleep 2
  visit project_path id: @project.id
  #sleep 2
end

Then(/^group filter exclude that deleted group$/) do
  page.find('#additional-options-header').click
  if ENV['BROWSER_MODE'] == "true"
    page.find("#select2-filter_search_group-container").click
    #sleep 2
    expect('#select2-filter_search_group-results').not_to have_content(@group_a.name)
  end
end

Given(/^fill search bar$/) do
  find("#nav-search").set("something \n")
end

Then(/^click remove icon$/) do
  find(:xpath,'//*[@id="nav-search-clear"]').click
end

Then(/^enter key press$/) do
  find("#nav-search").set("\n")
end

