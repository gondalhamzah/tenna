Given(/^user is on projects page$/) do
  visit projects_path
  sleep 2
end

Given(/^user is on add project menu$/) do
  find(:xpath,'//*[@id="projects-index"]/main/div[2]/a/i').click
  sleep 2
end

Then(/^system enforces Site "([^"]*)"$/) do |field|
  if field == "Name"
    find(:xpath,'//*[@id="project_name"]').click
    find(:xpath,'//*[@id="project_project_num"]').click #clicks on ID/Number field so that error message appears
    expect(page).to have_content("This value is required")
    visit new_project_path
  end
end

Then(/^fill in site name$/) do
  fill_in 'project_name', with: 'Some Site Name'
  fill_in 'project_project_num', with: '1'
  p "and click on next step"
  page.execute_script("$(\'#next-step\').trigger(\'click\')")
end

