Given(/^on project create page$/) do
  visit new_project_path
end

Given(/^create project with right info and validate$/) do
  fill_in "project_name", with: "New Project"
  page.execute_script("$(\'#next-step\').trigger(\'click\')")
  fill_in "project_address_attributes_line_1", with: "347 Varick Street."
  fill_in "project_address_attributes_city", with: "Jersey City"
  fill_in "project_address_attributes_state", with: "NJ"
  fill_in "project_address_attributes_zip", with: "07302"
  select 'United States', :from => 'project_address_attributes_country'
end

Given(/^create project with wrong info and validate$/) do
  fill_in "project_name", with: "New Project"
  page.execute_script("$(\'#next-step\').trigger(\'click\')")
  fill_in "project_address_attributes_line_1", with: "347 Varick Street."
  fill_in "project_address_attributes_city", with: "Jersey City"
  fill_in "project_address_attributes_state", with: "NJ"
  fill_in "project_address_attributes_zip", with: "07302"
  select 'Canada', :from => 'project_address_attributes_country'
end

Then(/^fill the projects form with Canadian info and validate$/) do
  fill_in "project_name", with: "New Canadian Project"
  page.execute_script("$(\'#next-step\').trigger(\'click\')")
  fill_in "project_address_attributes_line_1", with: "Address1:1969 Merivale Rd"
  fill_in "project_address_attributes_city", with: "Nepean"
  fill_in "project_address_attributes_state", with: "ON"
  fill_in "project_address_attributes_zip", with: "K2G 1G1"
  select 'Canada', :from => 'project_address_attributes_country'
end

Then(/^fill the projects form with American info and validate$/) do
  fill_in "project_name", with: "New American Project"
  page.execute_script("$(\'#next-step\').trigger(\'click\')")
  fill_in "project_address_attributes_line_1", with: "347 Varick Street."
  fill_in "project_address_attributes_city", with: "Jersey City"
  fill_in "project_address_attributes_state", with: "NJ"
  fill_in "project_address_attributes_zip", with: "07302"
  select 'United States', :from => 'project_address_attributes_country'
end

Then(/^on projects page there should be "([^"]*)" projects$/) do |arg1|
  expect(page).to have_css("table#table-projects tr", :count=>arg1)
end

Then(/^goto projects page$/) do
  visit projects_path
end

Given(/^reset project index$/) do
  reset_project_index
end

Then(/^click on text "([^"]*)" and goto projects page$/) do |arg1|
  # click_on arg1
  # sleep 1
  # visit projects_path
end