Given(/^Company Admin created a project$/) do
  @project_a = FactoryGirl.create(:project, contact: @company_admin)
  ProjectsIndex.import(project: @project_a)
end

Given(/^Company Admin is on the project users page$/) do
  visit project_path(@project_a.id)
  find(:xpath,"//*[@id='projects-show']/main/div[1]/div[2]/div[1]/div[1]/a[1]").click
  expect(page).to have_content('Users')
end

Then(/^Company Admin invites a user from Project Users page$/) do
  find(:xpath,'//*[@id="project-assignments-index"]/main/div/div/div/div[2]/a[1]').click
  expect(page).to have_content('Send an invitation')

  email = 'random_email88@b.com'

  fill_in 'Email', :with => email
  fill_in 'First Name', :with => 'Random'
  fill_in 'Last Name', :with => 'Person'
  find(:xpath,'//*[@id="new_user"]/div[2]/button').click
  sleep 2
  expect(page).to have_content('Invite New User') #if invitation is successfully sent then it will go back to project users page
end

Given(/^Project Leader is on their project users page$/) do
  visit project_path(@project3.id)
end

Then(/^Project Leader can invite a user from Project Users page$/) do
  find(:xpath,"//*[@id='projects-show']/main/div[1]/div[2]/div[1]/div[1]/a[1]").click
  find(:xpath,'//*[@id="project-assignments-index"]/main/div/div/div/div[2]/a[1]').click

  email = 'random_email89@b.com'

  fill_in 'Email', :with => email
  fill_in 'First Name', :with => 'Random'
  fill_in 'Last Name', :with => 'Person'
  find(:xpath,'//*[@id="new_user"]/div[2]/button').click
  sleep 2
  expect(page).to have_content('Invite New User') #if invitation is successfully sent then it will go back to project users page
end

Then(/^"([^"]*)" goes to project description page$/) do |arg1|
  if arg1 == "member"
    visit project_path(id: @project2.id)
  elsif arg1 == "field"
    visit project_path(id: @project.id)
  elsif arg1 == "company user"
    visit project_path(id: @project2.id)
  elsif arg1 == "company admin"
    visit project_path(id: @project.id)
  elsif arg1 == "leader"
    visit project_path(id: @project3.id)
  end
end

Then(/^Site Users link is not visible$/) do
  expect(page).to have_no_content('Site Users')
end
