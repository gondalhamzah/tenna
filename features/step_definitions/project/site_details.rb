Then(/^visit site detail of (\d+)$/) do |site_id|
  find(:css, "#table-projects a[href='/sites/#{site_id.to_i}']").click
end

Then(/^verify default value "([^"]*)" in user dropdown in site assignment page$/) do |value|
  find(:css, '#select2-project_assignment_user_id-container').text == value
end

Then(/^open user dropdown in site assignment page$/) do
  find(:css, '#select2-project_assignment_user_id-container').click
end

Then(/^select "([^"]*)" from user dropdown in site assignment page$/) do |value|
  find(:css, '#select2-project_assignment_user_id-results li', text: value).click
end

Then(/^verify default value "([^"]*)" in role dropdown in site assignment page$/) do |value|
  find(:css, '#select2-project_assignment_role-container').text == value
end

Then(/^open role dropdown in site assignment page$/) do
  find(:css, '#select2-project_assignment_role-container').click
end

Then(/^verify role dropdown values in site assignment page$/) do
	all(:css, '#select2-project_assignment_role-results li').count == 2
  all(:css, '#select2-project_assignment_role-results li')[0] == "Leader"
  all(:css, '#select2-project_assignment_role-results li')[1] == "Member"
end

Then(/^select "([^"]*)" from role dropdown in site assignment page$/) do |value|
  find(:css, '#select2-project_assignment_role-results li', text: value).click
end

Then(/^click on export assets$/) do
  find("#spreadsheet-links a", text: "Export Assets").click
end

Then(/^open sort order dropdown export assets page$/) do
  find("#report_assets_search_sort_order").click
end

Then(/^verify values in sort order dropdown on export assets page$/) do
  find("#report_assets_search_sort_order").all("option").count == 2
  find("#report_assets_search_sort_order").all("option")[0].text == "Asset Id"
  find("#report_assets_search_sort_order").all("option")[1].text == "Asset Title"
end
