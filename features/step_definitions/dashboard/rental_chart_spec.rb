Given(/^User on assets detail page having (\d+) assets$/) do |arg1|
  @category_group = FactoryGirl.create(:category_group)
  @project = FactoryGirl.create(:project, contact: @company_admin)
  @public_asset = FactoryGirl.create(:asset, certified: false, company: @company_admin.company, price: 10, market_value: 10)
  @public_asset2 = FactoryGirl.create(:asset, certified: false, company: @company_admin.company, price: 10, market_value: 10)
  @public_asset3 = FactoryGirl.create(:asset, certified: false, company: @company_admin.company, price: 10, market_value: 10)
  @public_asset.creator = @company_admin
  @public_asset.project = @project
  @public_asset.public = false
  @public_asset.rental = true
  @public_asset.owned = false
  @public_asset.save
  @public_asset2.creator = @company_admin
  @public_asset2.project = @project
  @public_asset2.public = false
  @public_asset2.rental = true
  @public_asset2.owned = false
  @public_asset2.save
  @public_asset3.creator = @company_admin
  @public_asset3.project = @project
  @public_asset3.public = false
  @public_asset3.owned = false
  @public_asset3.rental = true
  @public_asset3.save
  AssetsIndex.import(asset: @public_asset)
  AssetsVersionsIndex.import(asset: @public_asset)
  AssetsIndex.import(asset: @public_asset2)
  AssetsVersionsIndex.import(asset: @public_asset2)
  AssetsIndex.import(asset: @public_asset3)
  AssetsVersionsIndex.import(asset: @public_asset3)
  visit assets_path
end

Given(/^Verify all (\d+) assets are rental$/) do |arg1|
end

Then(/^goto dashboard chart having (\d+)\$ for rental tab$/) do |arg1|
  visit dashboard_path
  thirty = Asset.sum(:price)
  if thirty != 30
    fail(ArgumentError.new('Asset price sum is not thirty'))
  end
  # begin
  #   expect(find(:css, '#dashboard-index > main > div.row.dashboard-chart > div.col.s12.m12.l4.chart.hide-on-large-only > div > div > div > div.col.s12.m5.l12.border-right > div > div.col.s12.h4.c-primary')).to have_content("30")
  # rescue
  #   begin
  #     expect(find(:xpath, '//*[@id="dashboard-index"]/main/div[1]/div[1]/div/div/div/div[2]/div/div[3]')).to have_content("30")
  #   rescue
  #     expect(page).to have_content('30')
  #   end
  # end
end
