Given(/^go to "([^"]*)" page from nav$/) do |arg1|
  within '#first-nav' do
    click_on arg1
  end
end

Given(/^refresh the current page$/) do
  visit current_url
end

Then(/^add a byebug$/) do
  byebug
end

Then(/^add a pry$/) do
  binding.pry
end

Then(/^click on text "([^"]*)"$/) do |arg1|
  click_on arg1
end

Then(/^there should be text "([^"]*)"$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    expect(page).to have_content(arg1)
  end
end

Then(/^there should not be "([^"]*)"$/) do |arg1|
  if ENV["BROWSER_MODE"] == "true"
    expect(page).not_to have_content(arg1)
  end
end

When(/^select option "([^"]*)" from id "([^"]*)"$/) do |arg1, arg2|
  select arg1, :from => arg2
end

Then(/^first click text "([^"]*)"$/) do |arg1|
  page.all('a', :text => arg1)[0].click
end

Then(/^click on second text "([^"]*)"$/) do |arg1|
  page.all('a', :text => arg1)[1].click
end

Then(/^input "([^"]*)" should be (\d+)$/) do |arg1, arg2|
  find_field(arg1, with: arg2)
end

Then(/^check input value to be "([^"]*)"$/) do |arg1|
  expect(find(:xpath,'//*[@id="i12"]').value).to eq arg1
end

Then(/^set input value to be "([^"]*)"$/) do |arg1|
  find(:xpath, "//*[@id='i12']").set(arg1)
end

Then(/^visit companies page$/) do
  visit admin_companies_path
end

# Refactoring code from here

Then(/^click on class "([^"]*)"$/) do |class_name|
  find(:css, class_name).click
end

Given(/^login user as "([^"]*)"$/) do |user_type|
  Chewy.strategy(:bypass)
  @user = nil

  if user_type == "company_admin"
    @user = FactoryGirl.create(:company_admin)
  end

  visit new_user_session_path
  login @user
end

Then(/^visit new sites page$/) do
  visit new_project_path
end

Then(/^visit new groups page$/) do
  visit new_group_path
end

Then(/^logout using ui components$/) do
  find(:css, '.user-dropdown-button').click
  click_on "Sign Out"
end

Then(/^verify text absence "([^"]*)"$/) do |text|
  expect(page).to have_no_content(text)
end

When(/^change asset type to "([^"]*)"$/) do |arg1|
  if arg1 == "Material"
    find(:css, 'label[for="asset_type_material"]').click
  elsif arg1 == "Public Marketplace"
    find(:css, 'label[for="asset_public_private_public"]').click
  end
end

Then(/^verify asset type field present "([^"]*)"$/) do |field_id|
  find(:css, "#{field_id}").present?
end


Given(/^fill input field "([^"]*)" with "([^"]*)"$/) do |target, value|
  fill_in target, with: value
end

Given(/^select site with id "([^"]*)"$/) do |site_id|
  find(:css, '#select2-asset_project_id-container').click
  find('#select2-asset_project_id-results li', text: Project.find(site_id.to_i).name).click
  # select Project.find(site_id.to_i).name, from: 'asset_project_id'
end

Given(/^select category with id "([^"]*)"$/) do |category_id|
  find(:css, '#select2-asset_category_id-container').click
  find('#select2-asset_category_id-results ul li', text: Category.find(category_id.to_i).name).click
  # select Category.find(category_id.to_i).name, from: 'asset_category_id'
end

Given(/^select country with name "([^"]*)"$/) do |arg1|
  select arg1, from: 'asset_country'
end

Given(/^click create asset button$/) do
  begin
    find(:xpath, '//*[@id="new_asset"]/div[2]/div/div/button').click
  rescue
    page.execute_script("$('#new_asset > div.form-actions > div > div > button').trigger('click')")
  end
end

Given(/^attach "([^"]*)" pdf file\(s\)$/) do |number|
  if number.to_i == 1
    page.attach_file "new_pdfs[file][]", [Rails.root + "features/dummy_pdfs/dummy-pdf_1.pdf"], visible: false
  elsif number.to_i == 2
    page.attach_file "new_pdfs[file][]", [Rails.root + "features/dummy_pdfs/dummy-pdf_2.pdf"], visible: false
  elsif number.to_i == 3
    page.attach_file "new_pdfs[file][]", [Rails.root + "features/dummy_pdfs/dummy-pdf_3.pdf"], visible: false
  end
end

Then(/^remove all pdf files from asset$/) do
  all(:css, '.destroy-file').each do |node|
    node.click
  end
end

Then(/^click on menu item "([^"]*)"$/) do |item|
  find("#desktop_main_vertical_nav_dropdown li", text: item).click
end

Then(/^verify inventory popup is empty$/) do
  find(:css, '.asset_list_inventory').all('*').length == 0
end
