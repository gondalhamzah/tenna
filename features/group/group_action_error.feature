@javascript
Feature: Group 
  In order to show error message on Group action
  As a user
  I want to Verify the error on mass action if there is no group selected

  Scenario: Verify the error on mass action if there is no group selected
    Given User is login
    And on group detail page perform a action
    Then it should throw an error
