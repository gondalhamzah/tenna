@javascript
Feature: Group Mark as close
  In order to verify group as close
  As a user
  I want to mark group as close

  Scenario: Group mass action - "Mark as Closed"
    Given User is login
    And on group detail mark group as close
    Then Group should closed
