@javascript
Feature: Group 
  In order to Group history
  As a user
  I want to show history

  Scenario: Verify the existence of history tab on group detail page
    Given User is login
    And on group detail page
    Then Group should be history tab

  Scenario: Verify that history tab contain the history of relevant group
    Given User is login
    Given User on group history
    Then history tab contain history of that group

  Scenario: Verify that group edit page open on click on blue pencil
    Given User is login
    Given User on group detail page click pencil button
    Then Group edit page should be open

  Scenario: Verify that move a group from one project to another should be in history
    Given User move group from one project to another
    Then Project should be in group history

  Scenario: When Edit a Group then group site changed automatically in drop down BUG-772
    Given login user as "company_admin"
    Then visit new sites page
    And create site no 1 with address "true"
    And click on text "Create Site"
    Then visit new groups page
    And create a group
    And click on text "Create Group"
    Then visit group detail page
    And verify selected project

  Scenario: Asset Import is giving "File Upload Failed" Error on group detail page BUG-919
    Given "company_admin" is login
    And click on menu icon
    And click on menu item "Groups"
    And click on a group
    And click on assets template
    And click on import asset
    And verify import assets popup opened
    And attach asset template csv
    And click on text "Import"

  Scenario: Group index page searchbar is not working fine BUG-957
    Given "company_admin" is login
    And click on menu icon
    And click on menu item "Groups"
    And fill group search bar
    And click group remove icon
    Then group enter key press