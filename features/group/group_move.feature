@javascript
Feature: Group 
  In order to Move a Group
  As a user
  I want to Move a group from one project to another

  Scenario: Group mass action - "move"
    Given User is login
    And on group detail click move group
    Then Group should be move from one project to another
