@javascript
Feature: Group 
  In order to Delete Group
  As a user
  I want to delete a group

  Scenario: Group mass action - "delete"
    Given User is login
    And on group detail delete a group
    Then Group should be deleted
