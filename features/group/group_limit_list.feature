@javascript
Feature: Group limit list
  In order to limit Group list
  As a user
  I want to show pagination

  Scenario: Verify by selecting 25 from show dropdown shows list of 25 groups
    Given User is login
    And on group detail having 30 groups
    Then Group should be pagination at the end of page after 25 items
    Then there should be pagination on page

  Scenario: Verify by selecting 50 from show dropdown shows list of 50 groups
    Given User is login
    And on group detail having 60 groups
    Then Group should be pagination at the end of page after 50 items
    Then there should be pagination on page

  Scenario: Verify by selecting 100 from show dropdown shows list of 100 groups
    Given User is login
    And on group detail having 110 groups
    Then Group should be pagination at the end of page after 100 items
    Then there should be pagination on page

  Scenario: Verify pagination on group list
    Given User is login
    And on group detail having 27 groups
    Then there should be pagination on page
