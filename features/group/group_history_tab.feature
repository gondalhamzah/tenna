@javascript
Feature: Group history tab
  In order to Verify the existence of history tab on group detail page
  As a user
  I want to history tab on group detail page

  Scenario: Verify the existence of history tab on group detail page
    Given User is login
    And on group detail
    Then history should there on group detail page

  Scenario: Verify that history tab contain the history of relevant group
    Given User is login
    And on group detail
    Then history should contain the history of relevant group

  Scenario: Verify that group edit page open on click on blue pencil
    Given User is login
    And on group detail click blue pencil
    Then should redirect to edit group page

  Scenario: Verify that link an asset to a group should be in history
    Given User is login
    And on asset detail page link asset to group
    Then goto group history page it should contain history of asset

  Scenario: Verify that change in name of group should be in history
    Given User is login
    And change name of group
    Then goto group history page it should contain new name and old name of group

  Scenario: Verify that change in ID/Number of group should be in history
    Given User is login
    And change ID/Number of group
    Then goto group history page it should contain new ID/Number and old ID/Number of group

  Scenario: Verify that change in Client of group should be in history
    Given User is login
    And change Client of group
    Then goto group history page it contain new and old client of group

  Scenario: Verify that change in project of a group should be in history
    Given User is login
    And change project of group
    Then goto group history page it contain new project and old project of group

  Scenario: Verify that change in description of group should be in history
    Given User is login
    And change description of group
    Then goto group history page it contain new and old description of group

  Scenario: Verify that change in notes of a group should be in history
    Given User is login
    And update notes of group
    Then goto group history page it contain new and old notes of group
