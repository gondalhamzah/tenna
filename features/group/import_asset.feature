@javascript
Feature: Group import assets
  In order to import asset
  As a user
  I want to import asset on group page

  Scenario: Verify import of assets on a group
    Given User is login
    And on group detail Click assets template, templte should download
    And Fill templete with requied data
    And Click import asset button pop-up will appear select CSV file
    Then asset impport successfully

  Scenario: Verify import of group on a group list page
    Given User is login
    And Click import group button pop-up will appear select CSV file
    Then group impport successfully

  Scenario: Verify import of group on in history
    Given User is login
    And on groups page Click group template, templte should download
    And Fill data only required filed and Click import group button
    Then group impport successfully but not in history

  Scenario: Group: Import Group link is not working BUG-850
    Given "company_admin" is login
    And click on menu and click on group
    And click on import group link

  Scenario: Import Asset link is not working on group & site page BUG-852
    Given "company_admin" is login
    And click on menu and click on group
    And click on a group
    And click on import asset link
