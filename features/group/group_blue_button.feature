@javascript
Feature: Group 
  In order to Redirect to create group page
  As a user
  I want to Verify the plus button on group list page redirect to group create page

  Scenario: Verify the plus button on group list page redirect to group create page
    Given User is login
    And on group detail click blue button
    Then page redirect to group create page
