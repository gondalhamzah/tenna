@javascript
Feature: Material ASSET LOCATION AUDIT
  In order to Verify report for equipment
  As a User
  I want to Verify Report generation under Asset Location Audit tab

  Scenario: Verify Report generation for Asset category under Asset Location Audit tab
    Given User is login
    And on Reports page under asset location tab
    And select material Select Multiple Asset categories
    And Generate Asset Location Audit report
    Then Report will be generated only for the selected asset categories
