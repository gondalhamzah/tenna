@javascript
Feature: Project Audit Report
  In order to Verify all options in Report generation for project
  As a User
  I want to Verify Report generation under Project Audit tab

  Scenario: Verify Report generation for project
    Given User is login
    And on Reports page under asset location tab
    And Select Project Multiple project categories
    And Generate Asset Location Audit report
    Then Report will be generated only for the selected asset categories

  Scenario: Verify Report generation for Asset category under Asset Location Audit tab
    Given User is login
    And on Reports page under asset location tab
    And Select all project option form dropdown
    And Generate Asset Location Audit report
    Then Report will be generated only for the selected asset categories

  Scenario: Verify Report generation for Asset category under Asset Location Audit tab
    Given User is login
    And on Reports page under asset location tab
    And Select some projects and all Project from option
    And Generate Asset Location Audit report
    Then Report will be generated only for the selected asset categories
