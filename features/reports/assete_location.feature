@javascript
Feature: ASSET LOCATION AUDIT
  In order to Verify all options in Report generation under Asset Location Audit tab
  As a User
  I want to Verify Report generation under Asset Location Audit tab

  Scenario: Verify user can select all options correctly in Report generation under Asset Location Audit tab
    Given User is login
    And on Reports page under asset location tab
    And select all the option of asset catagory
    Then It select all options correctly

  Scenario: Verify Report generation for Asset category under Asset Location Audit tab
    Given User is login
    And on Reports page under asset location tab
    And Select Multiple Asset categories
    And Generate Asset Location Audit report
    Then Report will be generated only for the selected asset categories

  Scenario: Verify Report generation for Asset category under Asset Location Audit tab
    Given User is login
    And on Reports page under asset location tab
    And Select all Asset categories
    And Generate Asset Location Audit report
    Then Report will be generated only for the selected asset categories

  Scenario: Verify Report generation for Asset category under Asset Location Audit tab
    Given User is login
    And on Reports page under asset location tab
    And Select some Asset categories
    And Generate Asset Location Audit report
    Then Report will be generated only for the selected asset categories


  Scenario: Verify Report generation for Asset category under Asset Location Audit tab
    Given User is login
    And on Reports page under asset location tab
    And Select some Asset categories and all categories from option
    And Generate Asset Location Audit report
    Then Report will be generated only for the selected asset categories

  Scenario: Verify Report generation for Asset category under Asset Location Audit tab
    Given javascript driver is changed
    Given "company_admin" is login
    And on asset create page
    And create a for-sale asset with name "For-sale-Test-Asset"
    Then Verify "For-sale-Test-Asset" created successfully
    Given enable assets-reports subscription
    And visit reports page
    And verify filter creation and deletion
