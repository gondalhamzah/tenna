@javascript
Feature: Create Report
  In order to Check Validation of date on report page
  As a User
  I want to Verify date range input under project utilization tab

  Scenario: Verify date range input under project utilization tab
    Given User is login
    And on Report page under project utilization tab
    And select from date greater than to date
    Then It shouldn't allow to select from date greater than to date

  Scenario: Verify date range input under project utilization tab
    Given User is login
    And on Report page under project utilization tab
    And select from date greater than from date
    Then It shouldn't allow to select from date greater than to date
