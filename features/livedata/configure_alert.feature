@javascript
Feature: live data configure alerts

  Scenario: MARS-1185 Live Data :: Configure Alerts :: Notifications MARS-1185
  	Given Login as "Company Admin"
  	And enable subscription
  	And click on live data button
  	And click on configure alert
  	And click on bell icon
  	And click on add live data button
  	And choose add alert
  	Then click on add live data button in popup
  	And click on guage button
  	And click on note
  	And select company contact
  	And enter email
  	And enter phonenumber
  	And click on save button
