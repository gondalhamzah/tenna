var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FilterGroups = (function (_React$Component) {
  _inherits(FilterGroups, _React$Component);

  function FilterGroups(props) {
    _classCallCheck(this, FilterGroups);

    _get(Object.getPrototypeOf(FilterGroups.prototype), 'constructor', this).call(this, props);
    this.$filterEl = $('#asset_filter_name');
    this.state = {
      filter: props.filterObject || {},
      name: this.$filterEl.val()
    };

    this.filterNameChanged = this.filterNameChanged.bind(this);
  }

  _createClass(FilterGroups, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.$filterEl.on('change', this.filterNameChanged);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.$filterEl.off('change', this.filterNameChanged);
    }
  }, {
    key: 'filterNameChanged',
    value: function filterNameChanged(ev) {
      this.setState({ name: this.$filterEl.val() });
    }
  }, {
    key: 'exportCSV',
    value: function exportCSV(ev) {
      var csvData = this.props.csvData;

      var results = _.map(csvData, function (row) {
        return row.join(",");
      });
      var target = $(ev.currentTarget);
      var data = "data:text/csv;base64;charset=utf-8," + window.btoa(unescape(encodeURIComponent(results.join("\r\n"))));
      target.attr("href", data);
    }
  }, {
    key: 'updateFilter',
    value: function updateFilter(typeStr) {
      var that = this;
      var url = '/reports/assets/filters.json';
      if (typeStr == 'PATCH') {
        url = '/reports/assets/filters/' + this.state.filter.id + '.json';
      }

      $.ajax({
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        url: url,
        type: typeStr,
        data: JSON.stringify({
          filter_json: JSON.stringify(this.props.groups),
          name: $('#asset_filter_name').val(),
          start_date: this.props.startDate,
          end_date: this.props.endDate
        }),
        success: function (res, textStatus, jqXhr) {
          if (!res.success) {
            $('#flash').append("<div class='chip alert flast-notice'>Filter name should be unique.<i class='material-icons'>close</i></div>");
            setTimeout(function () {
              $('.flast-notice').remove();
            }, 5000);
          } else {
            that.setState({ filter: res.filter });
            $('#tab-my-filters .dataTables_wrapper').html(res.html);
            var msg = "New filter created successfully.";
            if (typeStr == "PATCH") {
              var _msg = "The filter updated successfully.";
            }
            $('#flash').append("<div class='chip alert notice'>" + msg + "<i class='material-icons'>close</i></div>");
            setTimeout(function () {
              $('.chip.notice.alert').remove();
            }, 5000);
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(textStatus);
          $('#flash').append("<div class='chip alert flast-notice'>Something went wrong... Please retry!<i class='material-icons'>close</i></div>");
          setTimeout(function () {
            $('.flast-notice').remove();
          }, 5000);
        }
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this = this;

      var children = [];
      this.props.groups.forEach(function (group, i) {
        if (i > 0) {
          children.push(React.createElement(FilterRuleDivider, { word: 'or', key: 'divider-group-' + group.id }));
        }
        children.push(React.createElement(FilterGroup, {
          key: group.id,
          group: group,
          groupIdx: i + 1,
          handleAddRule: _this.props.handleAddRule,
          handleGroupRuleDelete: _this.props.handleGroupRuleDelete,
          handleGroupRuleChange: _this.props.handleGroupRuleChange }));
      });
      return React.createElement(
        'div',
        { className: 'rules-wrapper' },
        children,
        React.createElement(
          'div',
          { className: 'add-filter-button rule-cell' },
          React.createElement(
            'a',
            { className: 'waves-effect waves-light btn but1',
              onClick: function (ev) {
                return _this.props.handleAddGroup();
              } },
            'Add Filter'
          ),
          this.props.assetAnalytics ? React.createElement(
            'button',
            { className: 'waves-effect waves-light btn white text-blue margin-left',
              disabled: !this.props.groups.length > 0 || !this.state.name || !this.state.filter.id,
              onClick: function (ev) {
                return _this.updateFilter('PATCH');
              }
            },
            'Update Filter'
          ) : null,
          this.props.assetAnalytics ? React.createElement(
            'button',
            { className: 'waves-effect waves-light btn white text-blue margin-left',
              disabled: !this.props.groups.length > 0 || !this.state.name || this.state.name == this.props.filterObject.name,
              onClick: function (ev) {
                return _this.updateFilter('POST');
              }
            },
            'Save As New'
          ) : null
        ),
        React.createElement(
          'a',
          {
            disabled: this.props.csvData.length == 0,
            className: 'waves-effect waves-light btn pull-right show-exp-btn but1',
            onClick: function (ev) {
              return _this.exportCSV(ev);
            },
            target: '_blank' },
          'Export'
        )
      );
    }
  }]);

  return FilterGroups;
})(React.Component);

FilterGroups.propTypes = {
  handleGroupRuleDelete: React.PropTypes.func,
  handleGroupRuleChange: React.PropTypes.func,
  handleAddRule: React.PropTypes.func,
  handleAddGroup: React.PropTypes.func,
  handleShowData: React.PropTypes.func,
  groups: React.PropTypes.array,
  filterObject: React.PropTypes.object,
  csvData: React.PropTypes.array,
  assetAnalytics: React.PropTypes.bool
};