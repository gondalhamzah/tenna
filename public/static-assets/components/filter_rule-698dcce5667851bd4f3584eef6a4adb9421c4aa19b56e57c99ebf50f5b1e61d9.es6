var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FilterRule = (function (_React$Component) {
  _inherits(FilterRule, _React$Component);

  function FilterRule(props) {
    _classCallCheck(this, FilterRule);

    _get(Object.getPrototypeOf(FilterRule.prototype), "constructor", this).call(this, props);
  }

  _createClass(FilterRule, [{
    key: "handleRuleChange",
    value: function handleRuleChange(field, option) {
      if (option) {
        this.props.handleRuleChange(this.props.groupId, this.props.rule.id, field, option.value);
      } else {
        this.props.handleRuleChange(this.props.groupId, this.props.rule.id, field, null);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      var groupId = this.props.groupId;

      var ruleId = this.props.rule.id;
      var _type = (_.find(Analytics.keys, { value: this.props.rule.key }) || { type: '' }).type;
      var operators = _.filter(Analytics.qualifiers, function (q) {
        return q.type.indexOf(_type) > -1;
      });

      return React.createElement(
        "div",
        { className: "flow-rule rule-row" },
        React.createElement(
          "div",
          { className: "rule-legend rule-cell" },
          React.createElement(
            "div",
            { className: "rule-number" },
            this.props.groupIdx
          ),
          React.createElement("div", { className: "number-line-element" })
        ),
        React.createElement(
          "div",
          { className: "flowform rule-cell" },
          React.createElement(
            "div",
            { className: "row" },
            React.createElement(
              "div",
              { className: "col l4 m4 s12" },
              React.createElement(Select, {
                name: "rule-key",
                onChange: function (option) {
                  return _this.handleRuleChange('key', option);
                },
                value: this.props.rule.key,
                options: Analytics.keys })
            ),
            React.createElement(
              "div",
              { className: "col l4 m4 s12 analytics-padding" },
              React.createElement(Select, {
                disabled: _type.length == 0,
                name: "rule-match",
                onChange: function (option) {
                  return _this.handleRuleChange('oper', option);
                },
                value: this.props.rule.oper,
                options: operators })
            ),
            this.props.rule.oper == 'not_exists' || this.props.rule.oper == "exists" || ['bool', 'mt'].indexOf(this.props.rule.key) > -1 || _type.length == 0 || _type.indexOf('bool') > -1 ? null : React.createElement(
              "div",
              { className: "col l4 m4 s12 analytics-padding" },
              React.createElement("input", {
                type: "text",
                onChange: function (ev) {
                  return _this.props.handleRuleChange(groupId, ruleId, 'value', ev.target.value);
                },
                value: this.props.rule.value,
                className: "string required parsley-success" })
            )
          )
        ),
        React.createElement(
          "div",
          { className: "rule-cell" },
          React.createElement(
            "a",
            { className: "btn-floating btn waves-effect waves-light red",
              onClick: function (ev) {
                return _this.props.handleRuleDelete(groupId, ruleId);
              }
            },
            React.createElement(
              "i",
              { className: "material-icons" },
              "delete"
            )
          )
        )
      );
    }
  }]);

  return FilterRule;
})(React.Component);

FilterRule.propTypes = {
  handleRuleChange: React.PropTypes.func,
  handleRuleDelete: React.PropTypes.func,
  index: React.PropTypes.number,
  rule: React.PropTypes.object,
  groupId: React.PropTypes.string,
  groupIdx: React.PropTypes.number
};