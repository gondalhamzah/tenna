var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

var Analytics = (function (_React$Component) {
  _inherits(Analytics, _React$Component);

  function Analytics(props) {
    _classCallCheck(this, Analytics);

    _get(Object.getPrototypeOf(Analytics.prototype), "constructor", this).call(this, props);

    var groups = props.props || [];
    if (props.filterObject) {
      groups = JSON.parse(props.filterObject.filter_json);
    }

    this.state = {
      dropMenuType: "",
      catKeys: [],
      catQualifiers: [],
      groups: groups,
      csvData: [],
      start_date: $('input[name="asset[start_date]"]').val(),
      end_date: $('input[name="asset[end_date]"]').val(),
      filterObject: props.filterObject || {}
    };

    if (props.chat && props.series) {
      this.renderData(props.html, JSON.parse(props.chart), JSON.parse(props.series));
    }

    if (props.geoData) {
      window.historyGEOData = JSON.parse(props.geoData);
    } else {
      window.historyGEOData = [];
    }
    var that = this;

    $('#asset_start_date, #asset_end_date').change(function () {
      if ($(this).val()) {
        that.handleShowData();
      }

      that.setState({
        start_date: $('input[name="asset[start_date]"]').val(),
        end_date: $('input[name="asset[end_date]"]').val()
      });
    });

    $(document).on('click', '#go-table-view', function () {
      $('a[href="#table-view"]').trigger('click');
    });
    this.handleShowData();
    this.pendingRequestId = null;
  }

  _createClass(Analytics, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      var categoryGroups = [];
      var categories = [];
      this.props.categoryGroup.map(function (value) {
        categoryGroups.push({
          label: value.name,
          value: value.name,
          type: value.id.toString()
        });
      });
      this.props.category.map(function (value) {
        categories.push({
          label: value.name,
          value: value.name,
          type: value.category_group_id.toString()
        });
      });
      Analytics.qualifiers = $.merge(Analytics.qualifiers, categories);

      this.setState({
        catKeys: categoryGroups
      });
    }
  }, {
    key: "handleGroupRuleDelete",
    value: function handleGroupRuleDelete(groupId, ruleId) {
      var _this = this;

      var groupIdx = _.findIndex(this.state.groups, { id: groupId });
      var ruleIdx = _.findIndex(this.state.groups[groupIdx].rules, { id: ruleId });
      if (this.state.groups[groupIdx].rules.length == 1) {
        this.setState({
          groups: [].concat(_toConsumableArray(this.state.groups.slice(0, groupIdx)), _toConsumableArray(this.state.groups.slice(groupIdx + 1)))
        }, function () {
          return _this.handleShowData();
        });
      } else {
        this.setState({
          groups: [].concat(_toConsumableArray(this.state.groups.slice(0, groupIdx)), [_extends({}, this.state.groups[groupIdx], {
            rules: [].concat(_toConsumableArray(this.state.groups[groupIdx].rules.slice(0, ruleIdx)), _toConsumableArray(this.state.groups[groupIdx].rules.slice(ruleIdx + 1)))
          })], _toConsumableArray(this.state.groups.slice(groupIdx + 1)))
        }, function () {
          return _this.handleShowData();
        });
      }
    }
  }, {
    key: "handleGroupRuleChange",
    value: function handleGroupRuleChange(groupId, ruleId, field, val) {
      var _this2 = this;

      var groupIdx = _.findIndex(this.state.groups, { id: groupId });
      var ruleIdx = _.findIndex(this.state.groups[groupIdx].rules, { id: ruleId });
      var currentRule = _extends({}, this.state.groups[groupIdx].rules[ruleIdx], _defineProperty({}, field, val));

      if (field == 'key' && !currentRule.oper) {
        currentRule['oper'] = 'exists';
      }

      var currentGroup = this.state.groups[groupIdx];
      if (currentGroup.groupType == "categorybtn" && field == 'key') {
        currentRule['catType'] = 'gcat';
      } else if (currentGroup.groupType == "categorybtn" && field == 'oper') {
        currentRule['catType'] = 'cat';
      }

      this.setState({
        groups: [].concat(_toConsumableArray(this.state.groups.slice(0, groupIdx)), [_extends({}, this.state.groups[groupIdx], {
          rules: [].concat(_toConsumableArray(this.state.groups[groupIdx].rules.slice(0, ruleIdx)), [currentRule], _toConsumableArray(this.state.groups[groupIdx].rules.slice(ruleIdx + 1)))
        })], _toConsumableArray(this.state.groups.slice(groupIdx + 1)))
      }, function () {
        if (currentRule.oper && currentRule.key && currentRule.key.length > 0 && currentRule.oper.length > 0 && (['exists', 'not_exists', 'req', 'comp', 'rfid', 'qr', 'upc', 'cellular', 'lora', 'sigfox', 'bluetooth'].indexOf(currentRule.oper) > -1 || currentRule.value.length > 0 || currentRule['catType'] == 'cat')) {
          _this2.handleShowData();
        }
      });
    }
  }, {
    key: "handleAddRule",
    value: function handleAddRule(groupId) {
      var groupIdx = _.findIndex(this.state.groups, { id: groupId });
      this.setState({
        groups: [].concat(_toConsumableArray(this.state.groups.slice(0, groupIdx)), [_extends({}, this.state.groups[groupIdx], {
          rules: [].concat(_toConsumableArray(this.state.groups[groupIdx].rules), [{ id: makeid(), key: '', oper: '', value: '' }])
        })], _toConsumableArray(this.state.groups.slice(groupIdx + 1)))
      });
    }
  }, {
    key: "handleAddGroup",
    value: function handleAddGroup(filterBtn) {
      if (filterBtn == "categorybtn") {
        this.setState({
          dropMenuType: filterBtn,
          groups: [].concat(_toConsumableArray(this.state.groups), [{ id: makeid(), rules: [{ id: makeid(), key: '', oper: '', value: '', catType: '' }], groupType: filterBtn }])
        });
      } else {
        this.setState({
          dropMenuType: filterBtn,
          groups: [].concat(_toConsumableArray(this.state.groups), [{ id: makeid(), rules: [{ id: makeid(), key: '', oper: '', value: '' }], groupType: filterBtn }])
        });
      }
    }
  }, {
    key: "renderData",
    value: function renderData(html, chart, series) {
      series = series.map(function (s) {
        return {
          name: s.name,
          data: s.data.map(function (d) {
            return [Date.parse(d[0]), d[1]];
          })
        };
      });

      $('#table-view').html(html);

      if (series.length > 0) {
        chart['series'] = [].concat(_toConsumableArray(series));
        var columnViewShow = $('#column-view').css('display');
        var lineViewShow = $('#line-view').css('display');
        var scatterViewShow = $('#scatter-view').css('display');
        $('#line-view').show();
        $('#line-view').highcharts(chart);

        chart.title.text = 'Column Chart';
        chart['chart']['type'] = 'column';
        $('#column-view').show();
        $('#column-view').highcharts(chart);

        chart.title.text = 'Scatter Plot Chart';
        chart['chart'] = {
          type: 'scatter',
          zoomType: 'xy'
        };
        chart['plotOptions']['scatter'] = {
          tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.y}'
          }
        };
        $('#scatter-view').show();
        $('#scatter-view').highcharts(chart);
        $('.tabs a.active').trigger('click');
        $('#line-view').css('display', lineViewShow);
        $('#column-view').css('display', columnViewShow);
        $('#scatter-view').css('display', scatterViewShow);
      } else {
        $('.numeric-view').html('<br>No numeric data is available. Click ' + '<a href="javascript:void(0);" id="go-table-view">here</a>' + ' to see text data.');
      }
    }
  }, {
    key: "handleShowData",
    value: function handleShowData() {
      var _this3 = this;

      if (this.pendingRequestId) {
        clearTimeout(this.pendingRequestId);
      }
      this.pendingRequestId = setTimeout(function () {
        _this3.pendingRequestId = null;
        var url = '';
        if (_this3.props.assetId) {
          url = '/analytics.json?asset_id=' + _this3.props.assetId;
        }
        if (_this3.props.companyId) {
          url = '/analytics.json?company_id=' + _this3.props.companyId;
        }

        $.ajax({
          type: 'POST',
          url: url,
          data: JSON.stringify({
            groups: _this3.state.groups,
            start_date: $('input[name="asset[start_date]"]').val(),
            end_date: $('input[name="asset[end_date]"]').val()
          }),
          contentType: 'application/json',
          dataType: 'json',
          success: function (data) {
            _this3.renderData(data.html, data.chart, data.series);
            _this3.setState({ csvData: _this3.makedata(data.csvData) });
            if (window.historyMapInited) {
              window.clearHistoryMarkers();
              _.each(data.geoData, function (info) {
                return addHistoryMarker(info);
              });
            } else {
              window.historyGEOData = [].concat(_toConsumableArray(data.geoData));
            }
          }
        });
      });
    }
  }, {
    key: "makedata",
    value: function makedata(prop) {
      for (var i = prop.length - 1; i >= 0; i--) {
        if (prop[i][3]) prop[i][3] = prop[i][3].replace(/\,/g, "");
        if (prop[i][4]) prop[i][4] = prop[i][4].replace(/\,/g, "");
      }
      return prop;
    }
  }, {
    key: "showCurrentDate",
    value: function showCurrentDate() {
      this.daypicker.showMonth(this.state.month);
    }
  }, {
    key: "render",
    value: function render() {
      var selectedDay = this.state.selectedDay;

      return React.createElement(
        "div",
        null,
        React.createElement(FilterGroups, {
          catKeys: this.state.catKeys,
          catQualifiers: this.state.catQualifiers,
          dropMenuType: this.state.dropMenuType,
          groups: this.state.groups,
          handleShowData: this.handleShowData.bind(this),
          handleAddGroup: this.handleAddGroup.bind(this),
          handleAddRule: this.handleAddRule.bind(this),
          handleGroupRuleDelete: this.handleGroupRuleDelete.bind(this),
          handleGroupRuleChange: this.handleGroupRuleChange.bind(this),
          filterObject: this.state.filterObject,
          csvData: this.state.csvData,
          assetAnalytics: this.props.assetAnalytics,
          startDate: this.state.start_date,
          endDate: this.state.end_date
        })
      );
    }
  }]);

  return Analytics;
})(React.Component);

Analytics.attr_keys = [{ label: 'RFID Gate', value: 'rfid gate', type: 'gate' }, { label: 'Maintenance', value: 'mt', type: 'mt' }, { label: 'Notes', value: 'notes', type: 'nt' }, { label: 'Site', value: 'project_id', type: 'pro' }, { label: 'Group', value: 'group_id', type: 'gr' }, { label: 'User', value: 'whodunnit_name', type: 'user' }, { label: 'Price', value: 'price', type: 'pri' }, { label: 'Market Value', value: 'market_value', type: 'mv' }];

Analytics.qualifiers = [{ label: 'Exists', value: 'exists', type: 'mt,hm,nt,ilu,tags,trackers,gr,pri,mv,gate' }, { label: 'Requested', value: 'req', type: 'mt' }, { label: 'Completed', value: 'comp', type: 'mt' }, { label: 'RFID', value: 'rfid', type: 'tags' }, { label: 'QR', value: 'qr', type: 'tags' }, { label: 'UPC', value: 'upc', type: 'tags' }, { label: 'Cellular', value: 'cellular', type: 'trackers' }, { label: 'LoRa', value: 'lora', type: 'trackers' }, { label: 'Bluetooth', value: 'bluetooth', type: 'trackers' }, { label: 'Sigfox', value: 'sigfox', type: 'trackers' }, { label: 'Equal to(Number)', value: 'eq', type: 'mv,qt' }, { label: 'Greater Than(Number)', value: 'gt', type: 'hm,pri,mv,qt' }, { label: 'Less Than(Number)', value: 'lt', type: 'hm,pri,mv,qt' }, { label: 'Does Not Exist', value: 'not_exists', type: 'ilu' }, { label: 'Text Contains(Text)', value: 'contains', type: 'nt,pro,gr,user,gate' }, { label: 'Does Not Contain(Text)', value: 'not_contains', type: 'user' }, { label: 'Exactly Matches', value: 'matches', type: 'nt,user,gate' }];

// {label: 'Starts With', value: 'starts_with', type: ''},
// {label: 'Ends With', value: 'ends_with', type: ''},
Analytics.live_keys = [{ label: 'Inventory/Location Update', value: 'location', type: 'ilu' }, { label: 'Hours', value: 'hours', type: 'hm' }, { label: 'Miles', value: 'miles', type: 'hm' }, { label: 'Quantity', value: 'quantity', type: 'qt' }];

// {label: 'Hours/Miles', value: 'hours_miles', type: 'hm'},
Analytics.tag_keys = [{ label: 'Tags', value: 'tags', type: 'tags' }, { label: 'Trackers', value: 'trackers', type: 'trackers' }];

Analytics.propTypes = {
  assetId: React.PropTypes.number,
  companyId: React.PropTypes.number,
  dateStart: React.PropTypes.string,
  dateEnd: React.PropTypes.string,
  groups: React.PropTypes.array,
  chart: React.PropTypes.string,
  html: React.PropTypes.string,
  series: React.PropTypes.string,
  geoData: React.PropTypes.string,
  filterObject: React.PropTypes.any,
  assetAnalytics: React.PropTypes.bool,
  categoryGroup: React.PropTypes.array,
  category: React.PropTypes.array
};