/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/


CKEDITOR.plugins.add('email_template',
  {
    init: function(editor)
    {
      var pluginName = 'email_template';
      editor.ui.addButton('assets_loop', {
        label: 'Loop through assets',
        command: "assets_loop"
      });
      editor.ui.addButton('link_to_asset', {
        label: 'Add link to asset',
        command: "link_to_asset"
      });
      editor.ui.addButton('asset_title', {
        label: 'Add asset title',
        command: "asset_title"
      });
      editor.ui.addButton('asset_make', {
        label: 'Add asset make',
        command: "asset_make"
      });
      editor.ui.addButton('asset_model', {
        label: 'Add asset model',
        command: "asset_model"
      });
      editor.ui.addButton('asset_year', {
        label: 'Add asset year',
        command: "asset_year"
      });
      editor.ui.addButton('asset_category', {
        label: 'Add asset category',
        command: "asset_category"
      });
      editor.ui.addButton('asset_location', {
        label: 'Add asset location',
        command: "asset_location"
      });
      editor.ui.addButton('asset_price', {
        label: 'Add asset price',
        command: "asset_price"
      });

      editor.addCommand('assets_loop', {exec: function(e){
        e.insertText("{{#each assets}}\n\n{{/each}}")
      }});
      editor.addCommand('link_to_asset', {exec: function(e){
        e.insertText("{{{link_to asset}}}")
      }});
      editor.addCommand('asset_title', {exec: function(e){
        e.insertText("{{title}}")
      }});
      editor.addCommand('asset_make', {exec: function(e){
        e.insertText("{{make}}")
      }});
      editor.addCommand('asset_model', {exec: function(e){
        e.insertText("{{model}}")
      }});
      editor.addCommand('asset_year', {exec: function(e){
        e.insertText("{{year}}")
      }});
      editor.addCommand('asset_category', {exec: function(e){
        e.insertText("{{category}}")
      }});
      editor.addCommand('asset_location', {exec: function(e){
        e.insertText("{{location}}")
      }});
      editor.addCommand('asset_price', {exec: function(e){
        e.insertText("{{price}}")
      }});
    }
  }
);


CKEDITOR.editorConfig = function( config )
{
  config.toolbar_mini = [
    ["Bold",  "Italic",  "Underline",  "Strike",  "-",  "Subscript",  "Superscript"],
  ];
  config.toolbar = "mini";
  // Define changes to default configuration here. For example:
  // config.language = 'fr';
  // config.uiColor = '#AADC6E';
  config.extraPlugins = 'email_template';


  /* Filebrowser routes */
  // The location of an external file browser, that should be launched when "Browse Server" button is pressed.
  config.filebrowserBrowseUrl = "/ckeditor/attachment_files";

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Flash dialog.
  config.filebrowserFlashBrowseUrl = "/ckeditor/attachment_files";

  // The location of a script that handles file uploads in the Flash dialog.
  config.filebrowserFlashUploadUrl = "/ckeditor/attachment_files";

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Link tab of Image dialog.
  config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures";

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Image dialog.
  config.filebrowserImageBrowseUrl = "/ckeditor/pictures";

  // The location of a script that handles file uploads in the Image dialog.
  config.filebrowserImageUploadUrl = "/ckeditor/pictures";

  // The location of a script that handles file uploads.
  config.filebrowserUploadUrl = "/ckeditor/attachment_files";

  config.allowedContent = true;

  // Toolbar groups configuration.
  config.toolbar = [
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
    // { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
    // { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
    //{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor'] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
    '/',
    { name: 'templatesnippets', items: ['assets_loop', 'link_to_asset', 'asset_title', 'asset_make', 'asset_model', 'asset_year', 'asset_category', 'asset_location', 'asset_price', 'assets_count' ]}
  ];

  config.toolbar_mini = [
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
    { name: 'styles', items: [ 'Font', 'FontSize' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
  ];
};
