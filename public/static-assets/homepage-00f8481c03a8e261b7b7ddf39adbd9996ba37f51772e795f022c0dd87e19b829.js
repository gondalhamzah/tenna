$(document).ready(function () {
  $("#user_remember_me").addClass("filled-in checked-assets-count");
  var originW = 0;
  function initSlider(rsetFlag) {
    var w = $(window).width(),
        h = 470 / 1520 * w;
      originW = w;
      $('.slider').slider({height: h, interval: 10000});
      if(rsetFlag) {
        var activeItem = $('.slides li.active');
        var index = $('.slides li').index(activeItem) + 1;
      }
  }

  /////////video pop-up///////
  var url = window.location.href;
  var hashes = url.split("?")[1];
  if (hashes && hashes.length > 0) {
    var hash = hashes.split('=');
    if(hash[0] === 'video-tag' && hash[1] === 'true') {
      $("#video-link2").trigger('click');
    }
  }

  ////////////Pyramid graphic////////////
  var $contents = $('.pyramid-content');
  $contents.slice(1).hide();
  $('.show-content').click(function() {
    var $target = $('#' + this.id + 'show').show();
    $contents.not($target).hide();
    if(this.id =='tab2'){
      $('#py-btn1').removeClass('active');
      $('#py-btn2').addClass('active');
      $('#py-btn3').removeClass('active');
    }
    else if(this.id =='tab3'){
      $('#py-btn1').removeClass('active');
      $('#py-btn2').removeClass('active');
      $('#py-btn3').addClass('active');
    }
    else {
      $('#py-btn1').addClass('active');
      $('#py-btn2').removeClass('active');
      $('#py-btn3').removeClass('active');
    }
  });
  ////////////Eof Pyramid graphic////////////

  //////////Industries-Served Tabs//////////
  var $contents2 = $('.industrial-content');
  $contents2.slice(1).hide();
  $('.show-ind-content').click(function() {
    var $target2 = $('#' + this.id + 'show').show();
    $contents2.not($target2).hide();
    $('div').removeClass('active');
    $('div > i').removeClass('fa-caret-up');
    $(this).addClass('active');
    $('#' + this.id + '> i').addClass('fa-caret-up');
  });
  //////////Eof Industries-Served Tabs//////////

  /////////////anchor scroll/////////////
    $('#customer-stories').on('click',function (e) {
      e.preventDefault();

      var target = this.hash;
      var $target = $(target);

      $('html, body').stop().animate({
        'scrollTop': $target.offset().top
      }, 900, 'swing', function () {
        window.location.hash = target;
      });
    });
  /////////////Eof anchor scroll/////////////

/*  $('.slide-btn').click(function() {
    var index = $(this).data('index');
    $('ul.indicators li:nth-child('+(index+1)+')').trigger('click');
  });*/
  $(window).scroll(function(){
    var $cache = $('header#fix');
    var $cache2 = $('header');

    if ($(window).scrollTop() > 200) {
      $cache.addClass("headfix");
      $cache2.addClass('headhide');
    }
    if ($(window).scrollTop() > 350) {
      $cache.addClass("headfix-fixed");
    };
    if ($(window).scrollTop() < 350) {
      $cache.removeClass("headfix-fixed");
    };
    if ($(window).scrollTop() < 200) {
      $cache.removeClass("headfix");
      $cache.removeClass("headfix-fixed");
    };
    if ($(window).scrollTop() < 100) {
      $cache2.removeClass('headhide');
    };
  });

  if(localStorage.emailCookie === undefined) {
    var pdf_link = '';
    $('.leads-form').on('mouseover click', function() {
      pdf_link = $(this).data('lead-link');
      $('#pdf_name').val(pdf_link);
    });
    $(document).on('submit','#leads_form', function() {
      localStorage.emailCookie = $('#lead_popup_email #_email').val();
      window.open('//s3.amazonaws.com/buildsourced-production-1/' + pdf_link);
    });
    $('.leads-form').prop("href", "#lead-email");
  } else if (localStorage.emailCookie != undefined) {
    $('.leads-form').on('mouseover click', function() {
      var pdf_link = $(this).data('lead-link');
      $(this).prop("href", "//s3.amazonaws.com/buildsourced-production-1/" + pdf_link);
    });
  }

  $(document).on('submit','#learn_more2', function() {
    lead_pixel();
  });
});
