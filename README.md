# BuildSourced

| Develop | Master |
| ------- | ------ |
| [![Develop](https://circleci.com/gh/Raizlabs/buildsourced-web/tree/develop.svg?style=svg&circle-token=d939b80f258e6cf1d066d8675eff06b341319fa9)](https://circleci.com/gh/Raizlabs/buildsourced-web/tree/develop) | [![Master](https://circleci.com/gh/Raizlabs/buildsourced-web/tree/master.svg?style=svg&circle-token=d939b80f258e6cf1d066d8675eff06b341319fa9)](https://circleci.com/gh/Raizlabs/buildsourced-web/tree/master) |


## Table of Contents

* [Development](#development)
* [Deployment](#deployment)
* [Git Flow](#git-flow)
  * [Creating Release](#creating-release)
  * [Creating Hotfix](#creating-hotfix)
* [3rd Party Licenses](#3rd-party-licenses)
* [Version History](#version-history)

## Development

- Checkout develop branch
- Copy `.env.example` to `.env` and modify as necessary
- Run `bundle`
- Run `rake db:create`
- Run `rake db:migrate`
- Run `rake db:seed`
- Run `rails s` or `rails s -b 0.0.0.0` if you want it to be accessible in LAN
- App available on `http://localhost:3000` by default

## Deployment

Using Heroku

See [.env.example](.env.example) and add necessary configs with `heroku config:add key1=value1 key2=value2 ...`.

Minimum dynos: 1 web (puma)

Required add-ons:

* Postgresql
* Sendgrid

Optional add-ons:

* Airbrake
* Papertrail
* Newrelic

## Git Flow

* Use `develop` branch for small additions (features/bug fixes/anything else with quick development time)
* Use `feature` branches for bigger features, for examples: `user-role-system` or maybe `ajaxify-site-navigation`
* Use `hotfix` branches for fixing bugs existed in master branch, remember to not using it for fixing bug in develop branch
* Use `release` branches for releasing current develop branch to master

**Never edit `master` branch directly, create either [release](#creating-release) or [hotfix](#creating-hotfix)**

relations with servers and environments:

* production server should always use code from `master` branch
* staging server normally use `develop` branch, but it's ok to use other branch depend on requirement

FAQ:

* Does `hotfix` and `feature` branches need to be pushed to remote repo?
  * Depend, if the hotfix or feature need collaboration then yes, but remember to delete it in remote repo after it's finished, or use -F argument
* ... anything else?

Cheatsheet: http://danielkummer.github.io/git-flow-cheatsheet/

### Creating Release

*Before you decide to create release you might want to consider if it's better to create [hotfix](#creating-hotfix) instead*

create release branch (it will create branch based on develop), replace `[name]` with release name (version number?)
    git flow release start [name]

bump version in `VERSION` file then commit it (to newly created release branch which you should be on already)
then finish release branch
```
git flow release finish [name]
```

you will be asked to enter tag comment with vim editor (or possibly other editor depend on your OS) follow the following steps to enter it:

1. type `a` to enter INSERT mode, `INSERT` will be displayed in the bottom of your terminal
2. enter release comment, maybe list of changes in this release?
3. after you done, press `esc` to exit INSERT mode
4. type `:wq` and press `enter` to exit vim and continue

after it's done, the change you made in release branch should be merged to both master and develop, and release branch will be deleted automatically

now push both your local's master and develop branch to github, if you don't have any other branch you can simply push all local's branch in one go:
```
git push --all origin
```

and push earlier created tag too:
```
git push --tags origin
```

last thing is to push it to heroku,
staging: (assuming remote name is staging)
```
git push staging develop:master
```
and production: (assuming remote name is production)
```
git push production master:master
```

DONE!

### Creating Hotfix

create hotfix branch, it will be created based on master, replace `[name]` with hotfix name for example: `fix-typo-in-home`
```
git flow hotfix start [name]
```

start working on your fix and commit it (to the hotfix branch), you're not required to push it though (see FAQ above)
also bump the version in `VERSION` file if necessary

after everything done finish it:
```
git flow hotfix finish [name]
```

same as finishing release you'll be asked to enter comment in vim, see instruction in creating release guide above

after it's done, hotfix branch will be merged back to master and also develop, hotfix branch will be deleted and tag will be created

now push both your local's master and develop branch to github, if you don't have any other branch you can simply push all local's branch in one go:
```
git push --all origin
```

and push earlier created tag too:
```
git push --tags origin
```

last thing is to push it to heroku,
staging: (assuming remote name is staging)
```
git push staging develop:master
```
and production: (assuming remote name is production)
```
git push production master:master
```

DONE!

## 3rd Party Licenses

All 3rd Party Components are under free for commercial use licenses (mostly MIT) except for the following components:

* none yet

## Version History

See [Releases](#)

## BuildSourced Staging's Mailtrap

