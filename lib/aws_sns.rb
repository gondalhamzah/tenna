module AwsSns
  def send_sms(phone_number, message)
    phone_number.gsub!(/\D/, '')
    phone_number = phone_number.start_with?('1') ? phone_number : '1' + phone_number
    if Rails.env.development?
      p '---------SMS---------'
      p "In production, an SMS will be sent to #{phone_number} with the following message: "
      p "---- #{message} ----"
      p '---------SMS---------'
    else
      credentials = Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'])
      region = ENV['AWS_REGION']
      sns = Aws::SNS::Client.new(region: region, credentials: credentials)
      sns.publish({phone_number: phone_number, message: message})
    end
  end
end
