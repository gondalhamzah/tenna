namespace :change_state  do
  desc 'Change by default state of wanted assets to contributed'
  task of_wanted_asset_to_contributed: :environment do
    Chewy.strategy(:bypass) do
      WantedAsset.all.each do |wanted|
        if wanted.state == "wanted"
          wanted.state = "contributed"
          wanted.save(validate: false)
            puts "State set as 'Contributed' for \n ID: #{wanted.id}\n"
        end
      end
    end
  end
end