namespace :assets  do
  desc 'Set tags and trackers value to 0 if null.'
  task set_tags_trackers_value: :environment do
    Chewy.strategy(:bypass) do
      tracking_code_query = 'SELECT assets.id as asset_id, tc.token_type, case when token_type is null then 0 else count(*) end as tc_count FROM assets INNER JOIN tracking_codes as tc ON tc.resource_id = assets.id GROUP BY asset_id, token_type'
      TrackingCode.token_types.each do |token_type_str, token_type_num|
        update_query = " BEGIN;
                         UPDATE assets
                         SET #{token_type_str.downcase} = tracking_code_query.tc_count
                         FROM (#{tracking_code_query}) AS tracking_code_query
                         WHERE assets.id = tracking_code_query.asset_id AND (tracking_code_query.token_type IS NULL OR tracking_code_query.token_type = #{token_type_num});
                         UPDATE assets
                         SET #{token_type_str.downcase} = 0
                         where #{token_type_str.downcase} IS NULL;
                         COMMIT; "
        result = ActiveRecord::Base.connection.execute(update_query)

        puts "Updated: #{token_type_str.downcase}"
      end
    end
  end
end
