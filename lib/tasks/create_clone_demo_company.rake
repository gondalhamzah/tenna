namespace :create  do
  desc 'This task creates a clone of the demo company ## Temporary solution'
  task clone_demo_company: :environment do
    Chewy.strategy(:atomic) do
    demo_company = Company.find_by!(id: 4)
    clone = demo_company.dup
    clone.name = "QACorp"
    clone.address = demo_company.address.dup
    clone.save(validate: false)
    4.times do |i|
      User.create!({company: clone, email: "ca#{i+1}@qa.com", first_name: "ca#{i+1}", last_name: "QACorp", 
                    password: "1password1", role: "company_admin", confirmed_at: Time.now})
    
    end
    
    6.times do |i|
      User.create!({company: clone, email: "cu#{i+1}@qa.com", first_name: "cu#{i+1}", last_name: "QACorp", 
                    password: "1password", role: "company_user", confirmed_at: Time.now})
    
    end

    clone.update(contact: clone.users.first)
    clone.reload
    
    demo_company.projects.each do |p|
      cp = p.dup
      cp.company = clone
      cp.contact = clone.users.first
      cp.address = p.address.dup
      cp.save!
      p.assets.each do |a|
        ca = a.dup      
        ca.project = cp
        ca.company = clone
        ca.creator = clone.users.first
        #duplicate attachments
        a.photos.each do |pt|
          ca.photos.build(file: pt.file, position: pt.position)
        end
        a.pdfs.each do |pt|
          ca.pdfs.build(file: pt.file)
        end
        ca.save!
      end
    end
    end
  end
end


