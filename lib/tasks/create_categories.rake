namespace :create  do
  desc 'This task creates new categories'
  task categories: :environment do
    Chewy.strategy(:atomic) do
      Category.transaction do
        equipment_category_names = [
          "Arrow / VMB Board",
          "Attachments",
          "Attenuator",
          "ATV",
          "Auto/ Pick-up Truck/ Van",
          "Boat",
          "Cement Mixer",
          "Compactor",
          "Compressor",
          "Conveyor",
          "Crane",
          "Dozer",
          "Drill Rig",
          "Excavator",
          "Farm Tractor",
          "Forklift",
          "Fuel/Lube Truck",
          "Generator",
          "Hydra-Platform",
          "Hydroseeder",
          "Light Tower",
          "Line Striper",
          "Loader",
          "Loader Backhoe",
          "Manlift",
          "Milling Machine",
          "Motor Grader",
          "Office Trailer",
          "Other",
          "Paver",
          "Pile/ Vibro Hammer/Leads",
          "Power Buggy",
          "Power Washer",
          "Pump - water or trash",
          "Pump - Concrete",
          "Rack Truck",
          "Saw - Concrete",
          "Saw - Rock Wheel",
          "Scissor Lift",
          "Skid Steer",
          "Shuttle Buggy - Asphalt",
          "Screen - Material",
          "Bidwell/Air Screed",
          "Drill - Tie-back Drill",
          "Storage Container",
          "Sweeper",
          "Trailer - Dump",
          "Trailer - Flat Bed",
          "Trailer - Tack",
          "Trailer - Utility",
          "Trench Box",
          "Trencher",
          "Truck - Boom",
          "Truck - Dump",
          "Truck - Off Road",
          "Truck - Vac",
          "Truck - Water",
          "Welder",
          "Winch"
        ]

        materials_category_names = [
         "Aggregates",
         "Asphalts",
         "Blades, Bits, and Busters",
         "Blasting",
         "Catch Systems",
         "Concrete & Related Supplies",
         "Conduit & Appurtenances",
         "Disposal",
         "Electrical & Related Supplies",
         "Environmental Supplies",
         "Erosion Control",
         "Fencing",
         "Formwork & Related Supplies",
         "Geotextile/Liners & Related Supplies",
         "Guard Rail & Related Supplies",
         "HVAC / Plumbing & Related Supplies",
         "Landscaping & Related Supplies",
         "Lifting/Rigging",
         "Lumber  & Related Supplies",
         "Masonry & Related Supplies",
         "Metal Structures (other then Steel)",
         "Netting & Related Supplies",
         "Office Furniture",
         "Other",
         "Paint/Coating & Related Supplies",
         "Piling/Shoring & Related Supplies",
         "Pipe / Connectors",
         "Plastic Sheeting",
         "Precast Items & Related Supplies",
         "Reinforcing Bars & Related Supplies",
         "Retaining Walls & Related Supplies",
         "Safety Supplies",
         "Signage",
         "Small Tools",
         "Stair Towers",
         "Steel / Beams",
         "Structural Steel and Related Supplies",
         "Survey Supplies",
         "Traffic Control",
         "Tarp / Blankets",
         "Vibration/Settlement Monitoring Instrumentations",
         "Welding Supplies"
        ]

        equipment_category_type = Category::CATEGORY_TYPES[0]
        material_category_type = Category::CATEGORY_TYPES[1]

        old_category_ids = Category.all.select(:id).map(&:id)

        equipment_assets = Asset.where(type: Asset::TYPES[0])
        material_assets = Asset.where(type: Asset::TYPES[1])
        wanted_assets = WantedAsset.all

        equipment_categories = equipment_category_names.map do |category_name|
          Category.create!(name: category_name, category_type: equipment_category_type)
        end
        equipment_category = equipment_categories.first


        material_categories = materials_category_names.map do |category_name|
          Category.create!(name: category_name, category_type: material_category_type)
        end
        material_category = material_categories.first

        equipment_assets.each do |asset|
          asset.update!(category: equipment_category)
        end

        material_assets.each do |asset|
          asset.update!(category: material_category)
        end

        wanted_assets.each do |asset|
          if asset.category.category_type == "material"
            asset.update!(category: material_category)
          else
            asset.update!(category: equipment_category)
          end
        end

        Category.destroy(old_category_ids)
      end
    end
  end
end
