require 'csv'

namespace :import  do
  desc 'This task creates test users ## Temporary solution'
  task user_titles: [:environment] do |i, args|
    CSV.parse(STDIN.read) do |row|
      user = User.find_by(email: row[0])
      next if user.nil?
      user.update(title: row[1])
    end

  end
end


