namespace :asset  do
  desc 'Set default project to assets which do not belong to any project.'
  task set_default_project: :environment do
    Chewy.strategy(:bypass) do
      Asset.where(project_id: nil).each do |asset|
        asset.project = asset.company.project if ( asset.company and asset.company.project )
        if asset.valid?
          asset.save!
          puts "Asset found without project \n ID: #{asset.id} \n Company ID: #{asset.company_id} \n\n"
        else
          puts '=============INVALID ASSET=============='
          puts asset.inspect
          puts '=============ERROR MESSAGES============='
          puts asset.errors.messages
        end
      end
    end
  end
end
