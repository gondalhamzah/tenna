namespace :admin do
  desc 'This task creates new http://bsqr1.com urls with new uuid'

  task bsqr_urls: :environment do
    n = (ARGV[1].nil?) ? 10 : ARGV[1].to_i

    puts "Creating #{n} URL's for http://#{ENV["QR_DOMAIN"]} or specify number how many urls to generate?"

    urls = []
    tokens = []

    n.times do
      uuid = SecureRandom.uuid

      # replace '-' with space
      uuid = uuid.gsub!('-', '')

      # convert to binary, then base64 encode the binary, take first 22 chars, add '==' at the end before convert to Hex16
      str = Base64.encode64([uuid].pack('H*'))[0..21]

      # change non-URL supported characters to URL-supported characters,
      # the change-to-chars are not in the base64 spec there is no concern of duplicates
      str = str.gsub('+', '-')
      str = str.gsub('/', '~')

      tokens << str
      urls << "http://#{ENV["QR_DOMAIN"]}/#{str}"
    end

    TrackingCode.transaction do
      tokens.each do |token|
        TrackingCode.create(token: token)
      end
    end

    if TrackingCode.where(token: tokens).count == n
      BackofficeMailer.bsqr_urls(urls).deliver
    else
      puts "Data base transaction entries isn't correct, try again."
    end
  end
end
