namespace :import do
  desc 'Import categories and assets'
  task categories_and_assets: :environment do
    Chewy.strategy(:bypass) do
      JSON.parse( $stdin.read ).each do |category|
        old_wp = User.find_by(email: 'oldwp@tenna.com')
        cat = Category.create!(name: category['name'], category_type: category['category_type'].to_s)
        category['assets'].each do |asset|
          a = Asset.create!(
            category: cat,
            title: asset['post_title'],
            description: asset['post_content'],
            creator: old_wp,
            public: true,
            location_zip: asset['cp_zipcode'].empty? ? 10001 : asset['cp_zipcode'],
            price: asset['cp_price'].empty? ? nil : asset['cp_price'].gsub(',',''),
            expires_on: asset['cp_sys_expire_date'],
            make: category['category_type'] == 'material' ? nil : 'Unspecified',
            model: category['category_type'] == 'material' ? nil : 'Unspecified',
            type: category['category_type'].to_s.capitalize,
            action: 'for_sale',
            photos: asset['attachments'][0..3].map do |a|
              Photo.new(remote_file_url: "http://www.tenna.com/wp-content/uploads/#{a}")
            end
          )
          AssetsIndex::Asset.update_index(a)
        end
      end
    end
  end

  desc 'Import users'
  task users: :environment do
    JSON.parse( $stdin.read ).each do |user|
      begin
        User.create!({
          first_name: user['first_name'].empty? ? 'Unnamed' : user['first_name'],
          last_name: user['last_name'].empty? ? 'Unnamed' :  user['last_name'],
          role: :public_user,
          company_name: 'unnamed',
          phone_number: user['phonenumber'] || '212-555-1212',
          email: user['user_email'],
          password: SecureRandom.hex,
          confirmed_at: Time.now
        })
      rescue
        puts '*'*30
        puts 'Not loaded'
        puts user
        puts '*'*30
      end
    end
  end
end

