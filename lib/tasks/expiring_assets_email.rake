namespace :buildsourced do
  desc 'Notify users of expiring assets'
  task notify_expiring_assets: :environment do
    Asset.creators_with_expiring_assets.each do |creator, assets|
      Notifications.expiring_assets(creator, assets).deliver
    end

    # TODO: create separated rake task?
    WantedAsset.creators_with_expiring_wanted_assets.each do |creator, wanted_assets|
      Notifications.expiring_wanted_assets(creator, wanted_assets).deliver
    end
  end
end
