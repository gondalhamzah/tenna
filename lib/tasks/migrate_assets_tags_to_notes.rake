namespace :migrate do
  desc 'Populate Notes field of assets from tags table.'
  task migrate_tags: :environment do
		puts "Migration started..." 
		Asset.all.each do |asset|	     
		  if asset.tags.present?  
		    asset.update_column(:notes, asset.tags.pluck(:name).join("|"))  		   
		  end    
		end
		puts "Migration completed..." 
  end
end