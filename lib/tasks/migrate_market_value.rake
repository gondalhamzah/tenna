namespace :migrate  do
  desc 'Populate market value base on quantity and price for existing assets.'
  task market_value: :environment do
    Chewy.strategy(:atomic) do
      Asset.all.each do |a|
        if !a.deleted? && !a.expired? && a.quantity? && a.price?
          a.update_attribute :market_value, (a.price.to_i * a.quantity.to_i)
        else
          a.update_attribute :market_value, 1
        end
      end
    end
  end
end
