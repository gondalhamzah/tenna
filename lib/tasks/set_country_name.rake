namespace :set_country  do
  desc 'Set country name for existing addresses with country name nil to United States'
  task name: :environment do
    Chewy.strategy(:bypass) do
      Address.all.each do |address|
        if address.country == nil 
          address.update(country: "United States")
        end
      end
    end
  end
end
