namespace :asset_version  do
  desc 'Set whodunnit field in asset_versions.'
  task set_whodunnit: :environment do
    Chewy.strategy(:bypass) do
      asset_vers = AssetsVersion.where(whodunnit: nil)
      asset_vers.each do |asset_v|
        asset = asset_v.asset
        if asset.present?
          if asset.company
            asset_v.whodunnit = asset.company.contact.id
          else
            asset_v.whodunnit = asset.creator.id
          end

          if asset_v.valid?
            asset_v.save!
            puts "AssetVersion found with nil whodunnit \n 
                  AssetVersion ID: #{asset_v.id} \n
                  Asset ID: #{asset.id} \n\n"
          else
            puts '=============ERROR MESSAGES============='
            puts asset_v.errors.messages
          end
        end
      end
    end
  end
end
