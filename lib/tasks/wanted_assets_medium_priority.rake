namespace :set_wanted_assets  do
  desc 'Migrating all active Wanted Posts to Medium priority'
  task priority_as_medium_for_current_assets: :environment do
    Chewy.strategy(:bypass) do
      WantedAsset.all.each do |wanted|
        wanted.priority = "Medium"
        wanted.save(validate: false)
        puts "Priority set as 'Medium' for \n ID: #{wanted.id}\n"
      end  
    end
  end
end