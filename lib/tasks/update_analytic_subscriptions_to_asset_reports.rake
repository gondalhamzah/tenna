namespace :update_analytics  do
  desc 'Update subscription where category is analytics to asset reports for existing subscriptions'
  task to_asset_reports: :environment do
    Chewy.strategy(:bypass) do
      Subscription.where(category: 'analytics').each do |subscription|
          subscription.category = "asset reports"
          if subscription.valid?
            subscription.save!
            puts "subscription category name set as 'asset reports' for \n ID: #{subscription.id}\n"
          else
            puts '=============ERROR OCCURED=============='
            puts subscription.inspect
            puts '=============ERROR MESSAGES============='
            puts subscription.errors.messages
          end
      end
    end
  end
end