namespace :create  do
  desc 'This task creates test users ## Temporary solution'
  task test_users: :environment do
    if Rails.env.production?
      puts 'This task can only be run in development or staging and is disabled in production environment.'
      exit
    else
      demo_company = Company.find_by!(id: 4)

      5.times do |i|
        User.create!({company: demo_company, email: "ca#{i+1}@a.com", first_name: "ca#{i+1}", last_name: "bdc",
                      password: "password1", role: "company_admin", confirmed_at: Time.now}) unless User.where(email: "ca#{i+1}@a.com").count > 0
      end

      5.times do |i|
        User.create!({company: demo_company, email: "cu#{i+1}@a.com", first_name: "cu#{i+1}", last_name: "bdc",
                      password: "password1", role: "company_user", confirmed_at: Time.now}) unless User.where(email: "cu#{i+1}@a.com").count > 0
      end

      5.times do |i|
        User.create!({company_name: 'null', email: "pu#{i+1}@a.com", first_name: "pu#{i+1}", last_name: "bdc",
                      password: "password1", role: "public_user", confirmed_at: Time.now}) unless User.where(email: "pu#{i+1}@a.com").count > 0
      end
    end
  end
end

