namespace :migrate  do
  desc 'Migrate admins'
  task admins: :environment do
    User.where(role: "admin").each do |a|
      AdminUser.invite!({email: a.email, first_name: a.first_name, last_name: a.last_name})
    end
  end
end


