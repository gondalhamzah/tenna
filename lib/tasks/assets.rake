namespace :assets do
	desc 'Set Datetime on last_location_updated'
	task set_last_location_changed: :environment do
    success = 0
    default = 0
		failed = 0
		Chewy.strategy(:atomic) do
			Asset.all.each do |asset|
				av = AssetsVersion.where(asset: asset, field_name: 'current_lng').order('created_at desc').first
				if av
					puts "#{asset.title} #{av.created_at}"
					asset.last_location_changed = av.created_at
					asset.save
					success += 1
				else
					if asset.current_lat
						asset.last_location_changed = asset.created_at
						default += 1
						puts "#{asset.title} #{asset.created_at}"
					else
						failed += 1
						puts "#{asset.title} doesn't have any location change"
					end
				end
			end
		end
    puts "#{success} success"
    puts "#{default} default"
		puts "#{failed} failed"
	end

  desc 'Migrate Company Assets expires_on date to 2050-01-01'
  task extend_expires_on: :environment do
    Chewy.strategy(:bypass) do
      Asset.where.not(company_id: nil).each_with_index do |a, i|
        a.expires_on = DateTime.new(2050, 1, 1)
        a.save
        puts "#{i+1} --> #{a.title} --> (#{a.id}) updated"
      end
    end
  end

  # @WARNING this task is not safe because action column is not used anymore!
  desc 'Migrate Asset Paper_Trail version data to new AssetsVersion Model'
  task version_migration: :environment do
  	_cache = {}
  	old_verion_count = 0
  	new_version_count = 0
  	failed_asset_count = 0
  	invalid_project_count = 0
  	Chewy.strategy(:atomic) do
	  	AssetVersion.all.order('id desc').each do |v|
        old_verion_count += 1
	  		if _cache[v.item_id].nil?
	  			asset = Asset.find_by_id(v.item_id)
	  			if !asset
	  				failed_asset_count += 1
	  				_cache[v.item_id] = false
	  				puts "Asset ##{v.item_id} is not in DB"
	  				next
	  			end
          _cache[v.item_id] = {:project_id => asset.project_id, :company_id => asset.company_id, :action => asset.action, :public => asset.public} if asset
	  		end
	  		next if !_cache[v.item_id]
	  		AssetsVersion.transaction do
	      	v.changeset.each do |field_name, field_value|
            f = true
		        if Asset::HISTORY_FIELDS.include? field_name
		        	if v.event == 'update' && field_name == 'project_id' && field_value[0] && !Project.find_by_id(field_value[0])
	        			invalid_project_count += 1
	        			puts "AssetVersion ##{v.id} in Project ##{field_value[0]} is invalid"
	        			f = false
		        	end
              f = false if Asset::HISTORY_STRING_NIL_CHECK_FIELDS.include? field_name && ((field_value[0] || '' == '') && (field_value[1] || '' == ''))
              if f
  		        	new_version_count += 1
  		          	AssetsVersion.create!(asset_id: v.item_id, field_name: field_name, from: field_value[0], to: field_value[1], whodunnit: v.whodunnit, action: v.event,
  		          		project_id: _cache[v.item_id][:project_id], company_id: _cache[v.item_id][:company_id], created_at: v.created_at, updated_at: v.created_at)
  		          	if field_name == 'project_id'
  		          		_cache[:project_id] = field_value[0]
  		          	end
  		        end
            end
          end
          changed_field_names = v.changeset.collect{|k,v| k}
          if (changed_field_names.include? 'public') || (changed_field_names.include? 'action')
            to = Asset.get_label_from_action_public_value(_cache[v.item_id][:action], _cache[v.item.id][:public])

            from_public = _cache[v.item_id][:public]
            from_action = _cache[v.item.id][:action]
            if !v.changeset['public'].nil?
              from_public = v.changeset['public'][0]
              _cache[v.item_id][:public] = from_public
            end
            if !v.changeset['action'].nil?
              from_action = v.changeset['action'][0]
              _cache[v.item_id][:action] = from_action
            end
            from = Asset.get_label_from_action_public_value(from_action, from_public)
            AssetsVersion.create!(asset_id: v.item_id, field_name: 'action_public', from: from, to: to, whodunnit: v.whodunnit, action: v.event,
                project_id: _cache[v.item_id][:project_id], company_id: _cache[v.item_id][:company_id], created_at: v.created_at, updated_at: v.created_at)
	        end
          AssetsVersion.create!(asset_id: v.item_id, field_name: 'newly_created', from: '', to: '', whodunnit: v.whodunnit, action: v.event,
                project_id: _cache[v.item_id][:project_id], company_id: _cache[v.item_id][:company_id], created_at: v.created_at, updated_at: v.created_at) if v.event == 'create'
		    end
	  	end
	end
  	puts "#{old_verion_count} paper_trail rows are scanned"
  	puts "#{invalid_project_count} projects are invalid"
  	puts "#{new_version_count} new version rows are created"
  	puts "#{failed_asset_count} assets are not in DB but are in paper_trail"
  end
end
