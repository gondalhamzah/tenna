namespace :asset  do
  desc 'Set owned field to assets according to rental and public.'
  task set_owned_field_to_assets: :environment do
    Chewy.strategy(:atomic) do
      Asset.all.each do |asset|
        if asset.rental == true and asset.public == false
          asset.owned = false
          if asset.valid?
            asset.save!
            puts "Rental asset found \n ID: #{asset.id} \n Rental: #{asset.rental} \n Public: #{asset.public} \n Owned: #{asset.owned}\n\n"
          else
            puts '=============INVALID ASSET=============='
            puts asset.inspect
            puts '=============ERROR MESSAGES============='
            puts asset.errors.messages
          end
        else
          asset.owned = true
          if asset.valid?
            asset.save!
            puts "Owned field set true for \n ID: #{asset.id} \n Rental: #{asset.rental} \n Public: #{asset.public} \n Owned: #{asset.owned}\n\n"
          else
            puts '=============INVALID ASSET=============='
            puts asset.inspect
            puts '=============ERROR MESSAGES============='
            puts asset.errors.messages
          end
        end
      end
    end
  end
end
