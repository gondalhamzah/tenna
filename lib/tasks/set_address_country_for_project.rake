namespace :update_country  do
  desc 'Update country name for existing address with United States of America and Canada'
  task name_for_address: :environment do
    Chewy.strategy(:bypass) do
      Address.all.each do |address|
        if address.country == "United States" or address.country.nil? and !US_ZIP_CODE_REGEX.match(address.zip).nil?
           address.country = "United States of America"
            if address.valid?
              address.save!
              puts "Address country set as 'United States of America' for \n ID: #{address.id}\n"
            else
              puts '=============INVALID ADDRESS=============='
              puts address.inspect
              puts '=============ERROR MESSAGES============='
              puts address.errors.messages
            end
        elsif address.country.nil? and !CANADA_ZIP_CODE_REGEX.match(address.zip).nil?
           address.country = "Canada"
            if address.valid?
              address.save!
              puts "Address country set as 'Canada' for \n ID: #{address.id}\n"
            else
              puts '=============INVALID ADDRESS=============='
              puts address.inspect
              puts '=============ERROR MESSAGES============='
              puts address.errors.messages
            end
        end
      end
    end
  end
end