namespace :create  do
  desc 'This task creates a gauge and alert types'
  task gauges: :environment do
    gauges = [
      [1, 'Engine Oil Temperature', 'EngineTemp', 'when the Engine Oil Temperature exceeds [number] °F.', {'type': 'value_numeric', 'stats_unit': 'degrees', 'min': 200, 'max': 350}, false, 'halfSolid', true],
      [2, 'Coolant Temperature', 'CoolantTemp', 'when the Coolant Temperature exceeds [number] °F.', {'type': 'value_numeric', 'stats_unit': 'degrees', 'min': 170, 'max': 250}, false, 'halfSolid', true],
      [3, 'Battery Voltage', 'Battery_Voltage', 'when the Battery Voltage [conditional] [number] V [when/for] [seconds] second(s).', {'type': 'value_numeric','stats_unit': 'volts', 'min': 9, 'max': 24, 'conditional': 'exceeds,is below', 'when/for': 'every,after', 'seconds': '5,10,20,30,40,60,90,120,150,240,300,360,480,600'}, false, 'halfSolid', true],
      [4, 'Engine Run Time', 'EngineRuntime', 'Engine Run Time setting: [conditional] [number] [unit].', {'type': 'value_numeric','stats_unit': 'hrs', 'min': [1, 60], 'max': [24, 1440], 'conditional': 'every,exceeds', 'unit': 'hour(s),minute(s)', 'related_label': {'every': 'for', 'exceeds': 'when'}}, false, 'fullSolid', true],
      [5, 'Odometer', 'Odometer', 'Odometer setting: [conditional] [number] mile(s).', {'type': 'value_numeric','stats_unit': 'miles', 'min': 0, 'max': 1000000, 'conditional': 'every,exceeds', 'unit': 'mile(s)', 'related_label': {'every': 'for', 'exceeds': 'when'}}, false, 'odometer', true],
      [6, 'Speed', 'Speed', 'when Speed [conditional] [number] mph.', {'type': 'value_numeric', 'stats_unit': 'mph', 'min': 0, 'max': 250, 'conditional': 'exceeds,exceeds the posted limit by'}, false, 'halfSolid', false],
      [7, 'Fuel Level', 'Fuel_Level', 'when Fuel Level [conditional] [number] %.', {'type': 'value_numeric', 'stats_unit': '%', 'min': 0, 'max': 100, 'conditional': 'equals,is below,exceeds'}, false, 'fullSolid', false],
      [8, 'Fuel Consumption', 'FuelConsumption', 'when Fuel Consumption [conditional] [number] MPG.', {'type': 'value_numeric', 'stats_unit': 'mpg', 'min': 0, 'max': 50, 'conditional': 'equals,is below'}, false, 'fullSolid', false],
      [9, 'Engine Speed', 'EngineSpeed', 'when Engine Speed exceeds [number] rpm.', {'type': 'value_numeric', 'stats_unit': 'rpm', 'min': 1000, 'max': 7500}, false, 'halfSolid', false],
      [10, 'Moving', 'Moving', 'when the vehicle [conditional] moving.', {'type': 'value_boolean', 'stats_unit': '', 'conditional': 'is,is not'}, false, 'bar', false],
      [11, 'Idle Hours', 'IdleHours', 'when Idle Hours exceeds [number] minutes.', {'type': 'value_numeric', 'stats_unit': 'hrs', 'min': 0, 'max': 8192}, false, 'fullSolid', false],
      [12, 'Location', 'Geofence', '', {'type': 'value_object'}, false, 'geofence', false],
      [54, 'Hard Acceleration', 'Hard Acceleration', 'when the vehicle encounters hard acceleration.', {'type': 'value_numeric', 'threshold': 0.3}, true, 'progress', false],
      [55, 'Hard Braking', 'Hard braking', 'when the vehicle encounters hard breaking.', {'type': 'value_numeric', 'threshold': 0.4}, true, 'progress', false],
      [57, 'Towing', 'Towing', 'when the vehicle is moving without the ignition on.', {'type': 'value_boolean'}, true, 'bar', false],
      [59, 'Power', 'Power', 'when the tracker is plugged in and powered [conditional].', {'type': 'value_boolean', 'conditional': 'on,off'}, true, 'bar', false],
      [62, 'Quick Lane Change', 'Quick lane change', 'when the vehicle performs a quick lane change.', {'type': 'value_numeric', 'threshold': 0.3}, true, 'progress', false],
      [63, 'Sharp Turn', 'Sharp turn', 'when the vehicle performs a sharp turn.', {'type': 'value_numeric', 'threshold': 0.4}, true, 'progress', false],
      #[64, 'Fatigued Driving', 'Fatigued', 'when the vehicle has been operating for more than three hours without stopping.', {'type': 'value_numeric', 'threshold': 180, 'stats_unit': ''}, true, 'fullSolid', false],
      [65, 'Ignition On', 'Ignition', "when the vehicle's ignition is switched [conditional].", {'type': 'value_boolean', 'conditional': 'on,off'}, true, 'bar', false],
      [67, 'MIL Alarm', 'MIL', "when the vehicle's malfunction indicator light is illuminated.", {'type': 'value_boolean'}, true, 'bar', true],
      [70, 'Seat Belt', 'Seatbelt', 'when the driver seat belt is not buckled.', {'type': 'value_boolean'}, true, 'bar', true],
    ]

    ids = []
    gauges.each do |g|
      ids << g[0]
      gauge = Gauge.find_or_initialize_by(id: g[0])
      gauge.name = g[1]
      gauge.icon = g[2]
      gauge.template = g[3]
      gauge.meta = g[4]
      gauge.is_alarm = g[5]
      gauge.graph_type = g[6]
      gauge.can_request_service = g[7]
      gauge.save

      puts "Created: #{gauge}"
    end
    Gauge.where.not(id: ids).delete_all
  end
end
