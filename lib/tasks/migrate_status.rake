namespace :migrate  do
  desc 'Status'
  task status: :environment do
    Chewy.strategy(:atomic) do
      Asset.all.each do |a|
        next if a.status.nil?
        status = %w(available pending_approval expired sold deleted)
        a.update_attribute :state, status[a.status.to_i]
      end
      WantedAsset.all.each do |a|
        next if a.status.nil?
        status = %w(wanted fulfilled deleted)
        a.update_attribute :state, status[a.status.to_i]
      end
    end
  end
end


