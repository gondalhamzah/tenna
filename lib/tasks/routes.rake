namespace :routes do
  desc "Clean all routes"
  task clean: :environment do
    DriverStop.delete_all
    RouteRecurring.delete_all
    Event.delete_all
    Path.delete_all
    Route.delete_all
  end

end
