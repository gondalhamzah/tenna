namespace :create  do
  desc 'This task creates test admin users'
  task test_admin_users: :environment do
    if Rails.env.production?
      puts 'This task can only be run in development or staging and is disabled in production environment.'
      exit
    else
      5.times do |i|
        puts "#{i} user is created. \n"
        AdminUser.create!({
          email: "ca#{i+1}@a.com", first_name: "ca#{i+1}", last_name: "bdc", password: "password1",
          invitation_created_at: Time.now, invitation_sent_at: Time.now, invitation_accepted_at: Time.now,
          role: 0})
      end
    end
  end
end
