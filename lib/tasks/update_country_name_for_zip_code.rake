namespace :update_country  do
  desc 'Update country name for existing zip code with United States of America and Canada'
  task name_for_zipcode: :environment do
    Chewy.strategy(:bypass) do
      ZipCode.all.each do |zipcode|
        if zipcode.country == "United States" or zipcode.country.nil? and !US_ZIP_CODE_REGEX.match(zipcode.zip_code).nil?
           zipcode.country = "United States of America"
            if zipcode.valid?
              zipcode.save!
              puts "Zipcode country set as 'United States of America' for \n ID: #{zipcode.id}\n"
            else
              puts '=============INVALID ZIPCODE=============='
              puts zipcode.inspect
              puts '=============ERROR MESSAGES============='
              puts zipcode.errors.messages
            end
        elsif zipcode.country.nil? and !CANADA_ZIP_CODE_REGEX.match(zipcode.zip_code).nil?
           zipcode.country = "Canada"
            if zipcode.valid?
              zipcode.save!
              puts "Zipcode country set as 'Canada' for \n ID: #{zipcode.id}\n"
            else
              puts '=============INVALID ZIPCODE=============='
              puts zipcode.inspect
              puts '=============ERROR MESSAGES============='
              puts zipcode.errors.messages
            end
        end
      end
    end
  end
end