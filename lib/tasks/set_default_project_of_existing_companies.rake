namespace :company  do
  desc 'set default project of existing companies'
  task set_default_project: :environment do
    Chewy.strategy(:bypass) do
      Company.all.each do |company|
        unless company.project
          default_project = Project.new(name: 'Default Project')
          default_project.save(validate: false)
          company_address_attr = company.address.attributes
          company_address_attr['addressable_id'] = default_project.id
          company_address_attr['addressable_type'] = 'Project'
          company_address_attr.delete('id')
          default_project_address = Address.new(company_address_attr)
          default_project_address.save(validate: false)
          default_project.address = default_project_address
          default_project.company = company
          default_project.contact = company.contact

          if default_project.valid?
            default_project.save!

            company.project = default_project
            if company.valid?
              company.save!
            else
              puts '=============INVALID COMPANY============='
              puts company.inspect
              puts '=============ERROR MESSAGES=============='
              puts company.errors.messages
            end
          else
            puts '===========INVALID DEFAULT PROJECT========='
            puts default_project.inspect
            puts '==============ERROR MESSAGES==============='
            puts default_project.errors.messages
          end
        end
      end
    end
  end
end
