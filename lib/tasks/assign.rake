namespace :assign do
  desc "Assign Category Group to Categories"
  task category_group_to_categories: :environment do

    groupCategories = ["Heavy Equipment", "Heavy on Road", "Light on Road", "Attachments","Equipment",
     "Small Tools", "Portable", "Material"]
    
    CategoryGroup.delete_all if (CategoryGroup.count > 0)  
    created_groups = []
    groupCategories.each do |group|
      created_groups << CategoryGroup.create(name: group)
    end
    Category.update_all(category_group_id: created_groups.fifth.id )

    sub_categories = [
      ["Bidwell / Air Screed","Compactor","Crane","Drill Rig","Drill - Tie-back Drill","Excavator","Excavator - Mini",
        "Farm Tractor","Feller Buncher","Forklift","Forwarder","Lift - Scissor ","Lift - Spider ","Loader",
        "Loader backhoe","Log SKidder","Manlift","Milling machine","Motor Grader","Mower","Paver",
        "Pump- Concrete","Saw-Rock Wheel","Scissor Lift","Scraper","Screen - Material","Shovel Logger",
        "Shuttle Buggy - Asphalt","Skid Steer","Skid Steer - Mini","Sweeper","Tractor","Truck- Off Road",
        "Truck-Rock","Other Heavy Equipment"],
      ["Roller","Specialty Equipment","Log Truck","Log Truck/ Truck - Log ","Roll - OFF Truck ","Truck-Dump",
        "Other Heavy on Road"],
      ["Auto/Pick-Up Truck/Van","Boat","Bucket Truck / Aerial Lift","Fuel Truck/ Lube Truck","Pickup ","Rack Truck",
        "Traffic Truck ","Trailer - Dump","Trailer - Flat bed","TRailer - Truck","Trailer - Utility","Transporter Tank",
        "Truck","Truck- Boom","Truck-Box","Truck- One Ton","Truck - Rail ","Truck - Service ","Truck - Traffic","Truck-Vac",
        "TRuck-Water","Other Light on Road"],
      ["Attachments","Blades,Bits, and Busters","Cables","Catch systems","Engine/Engine parts","Lifting/Rigging",
        "Slasher","Winch","Other Attachments"],
      ["Other"],
      ["Arrow/VMB board","ATV","Blasting","Cement Mixer","Chipper","Chipper- Tracked","Compressor","Connex Box",
        "Conveyor","Generator","Hydra -Platform ","Hydroseeder","Light Tower","Line Striper","Office Trailer",
        "Pile/Vibro hammer/Leads","Power Buggy","Power Washer","Pump- Water or Trash","Saw-Concrete","Stair Towers",
        "Stump Grinder","Survey Supplies","Traffic Control","Tree Trimmer","Trench box","Trencher","UTV",
        "Vibration / Settlement Monitoring Instrumentations","Welder","Other Portable"],
      ["Grinder","Small Tools","Other Small Tools"],
      ["Metal structures ( other than steel)","Aggregates","Asphalts ","Concrete & Related Supplies","Conduit & Appurtenances",
        "Crane Mats/ Swamp mats /Mats","Disposal","Electrical & Related Supplies","Environmental Supplies","Erosion Control",
        "Fencing","Formwork & Related Supplies","Geotextile / Liners & Related Supplies","Guard Rail & Related Supplies",
        "HVAC / Plumbing & Related Supplies","Jersey Barrier / Barrier Curb","Landscaping & Related Supplies",
        "Lumber  & Related Supplies","Masonry & Related Supplies","Netting & Related Supplies","Office Furniture",
        "Paint / Coating & Related Supplies","Piling / Shoring & Related Supplies","Pipe/Connectors","Plastic Sheeting",
        "Precast Items & Related Supplies","Reinforcing Bars & Related Supplies","Retaining Walls & Related Supplies",
        "Safety Supplies","Signage","Steel / Beams","Storage Container","Structural Steel and Related supplies","Tarp / Blankets",
        "Welding Supplies"]
    ]

    sub_categories.each_with_index do |sub_category, index|
      input_data_array = []
      sub_category.each do |k|
        input_data_array << (k.downcase.delete "\s")
      end
      database_category_names_array = Category.all.pluck(:id, :name)
      database_category_names_array.each do |category_row|
        category = Category.find_by_id(category_row[0])
        if input_data_array.include? (category_row[1].downcase.delete "\s")
          category.category_group_id = created_groups[index].id
          Chewy.strategy(:atomic)  { category.save }
        end
        puts "----------------"
        puts "--Category name--#{category.name} -- assigned group #{created_groups[index].id}"
        puts "----------------"
      end
    end

  end
end