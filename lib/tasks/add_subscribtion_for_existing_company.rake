namespace :add_subscriptions  do
  desc 'Add Subscriptions for existing customers'
  task for_existing_customers: :environment do
    Chewy.strategy(:bypass) do
      Company.all.each do |company|
        if company.subscriptions.count == 0
          subscription = Subscription.create([{company_id: company.id,category: 'routing', state: 'active',payment: true},
            {company_id: company.id,category: 'asset reports', state: 'active',payment: true},
            {company_id: company.id,category: 'company reports', state: 'active',payment: true},
            {company_id: company.id,category: 'wantit' ,state: 'active',payment: true},
            {company_id: company.id,category: 'findit' ,state: 'active',payment: true},
            {company_id: company.id,category: 'scheduleit', state: 'active',payment: true},
            {company_id: company.id,category: 'maintenance', state: 'active',payment: true},
            {company_id: company.id,category: 'add tracking to asset', state: 'active',payment: true},
            {company_id: company.id,category: 'livedata', state: 'active',payment: true}])
          puts "subscriptions created for \n Company ID: #{company.id}\n"
        elsif company.subscriptions.count >= 1
          asset_report = company.subscriptions.where(category: 'asset reports').first
          routing = company.subscriptions.where(category: 'routing').first
          if asset_report and routing
            subscription = Subscription.create([{company_id: company.id,category: 'company reports', state: 'active',payment: true},
              {company_id: company.id,category: 'wantit', state: 'active',payment: true},
              {company_id: company.id,category: 'findit', state: 'active',payment: true},
              {company_id: company.id,category: 'scheduleit', state: 'active',payment: true},
              {company_id: company.id,category: 'maintenance', state: 'active',payment: true},
              {company_id: company.id,category: 'add tracking to asset', state: 'active',payment: true},
              {company_id: company.id,category: 'livedata',state: 'active',payment: true}])
            puts "subscriptions created for \n Company ID: #{company.id}\n"
          elsif asset_report
            subscription = Subscription.create([{company_id: company.id,category: 'routing', state: 'active',payment: true},
              {company_id: company.id,category: 'company reports', state: 'active',payment: true},
              {company_id: company.id,category: 'wantit', state: 'active',payment: true},
              {company_id: company.id,category: 'findit', state: 'active',payment: true},
              {company_id: company.id,category: 'scheduleit', state: 'active',payment: true},
              {company_id: company.id,category: 'maintenance', state: 'active',payment: true},
              {company_id: company.id,category: 'add tracking to asset', state: 'active',payment: true},
              {company_id: company.id,category: 'livedata', state: 'active',payment: true}])
            puts "subscriptions created for \n Company ID: #{company.id}\n"
          elsif routing
            subscription = Subscription.create([{company_id: company.id,category: 'asset reports', state: 'active',payment: true},
              {company_id: company.id,category: 'company reports', state: 'active',payment: true},
              {company_id: company.id,category: 'wantit', state: 'active',payment: true},
              {company_id: company.id,category: 'findit', state: 'active',payment: true},
              {company_id: company.id,category: 'scheduleit', state: 'active',payment: true},
              {company_id: company.id,category: 'maintenance', state: 'active',payment: true},
              {company_id: company.id,category: 'add tracking to asset', state: 'active',payment: true},
              {company_id: company.id,category: 'livedata', state: 'active',payment: true}])
            puts "subscriptions created for \n Company ID: #{company.id}\n"
          else
            puts '=============ERROR OCCURED=============='
            puts subscription.errors.messages
          end
        else
            puts '=============ERROR OCCURED=============='
            puts subscription.errors.messages
        end
      end
    end
  end
end