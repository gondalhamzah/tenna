namespace :tracking_info do
  desc "Add company id to assetversion tracking_info where its nil"
  task add_company_id: :environment do
  	assets_versions = AssetsVersion.where(company_id: nil).where.not(tracking_info: nil)
	AssetsVersion.skip_callback(:save, :after, :check_tracking_info)
  	Chewy.strategy(:bypass) do
	  	assets_versions.each do |asset_version|
	  		next if asset_version.asset_id.nil?
	  		asset = Asset.find asset_version.asset_id
	  		next if asset.company_id.nil?
	  		asset_version.company_id = asset.company_id
	  		asset_version.project_id = asset.project_id
	  		asset_version.save!
  		    # AssetsVersionsIndex.import(asset: @asset_version)
	  	end
	end
	AssetsVersion.set_callback(:save, :after, :check_tracking_info)
  end
end
