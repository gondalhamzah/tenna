namespace :clean  do
  desc 'This task is to clean all gauge related data'
  task gauge_alerts: :environment do
    GaugeAlertLog.all.delete_all
    GaugeCondition.all.delete_all
    GaugeAlertGroup.all.delete_all
  end
end


