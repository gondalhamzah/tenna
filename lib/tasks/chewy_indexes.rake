namespace :reset do
  desc 'Notify users of expiring assets'
  task indexes: :environment do
    Company.all.each do |company|
      puts "=============Updating Index for #{company.id}================="
	  # Rake::Task['chewy:reset:all'].invoke
     	# Rake::Task['chewy:reset:all'].invoke
      rake = "chewy:reset"
			ENV["COMPANY_ID"] = "#{company.id}"      
		  Rake::Task[rake].execute
      # system rake
    end
  end
end
