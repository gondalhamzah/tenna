module Elasticsearch
  module Transport
    module Transport
      module HTTP
        class Faraday
          # Performs the request by invoking {Transport::Base#perform_request}
          # with a block.
          #
          # @return [Response]
          # @see    Transport::Base#perform_request
          #
          def perform_request(method, path, params = {}, body = nil)
            super do |connection, url|
              body = body ? __convert_to_json(body) : nil
              request = Seahorse::Client::Http::Request.new(body: body, endpoint: url, http_method: method)
              credentials = Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'])
              region = ENV['AWS_REGION']
              signer = Aws::Signers::V4.new(credentials, 'es', region)
              auth_headers = signer.sign(request).headers

              # set content-type to JSON otherwise AWS complains with
              # "When Content-Type:application/x-www-form-urlencoded,
              # URL cannot include query-string parameters"
              # see https://github.com/elastic/elasticsearch-ruby/issues/226
              # in future, we should refer to
              # https://github.com/elastic/elasticsearch-ruby/pull/235
              # and just use the faraday middleware to sign requests for us
              response = connection.connection.run_request \
                method.downcase.to_sym,
                url,
                body,
                { "Content-Type" => "application/json" }.merge(auth_headers)
              Response.new response.status, response.body, response.headers
            end
          end
        end
      end
    end
  end
end
