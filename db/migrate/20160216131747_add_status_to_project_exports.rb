class AddStatusToProjectExports < ActiveRecord::Migration
  def change
    add_column :project_exports, :status, :integer, default: 0
  end
end
