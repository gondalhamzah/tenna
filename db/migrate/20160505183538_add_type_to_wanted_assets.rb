class AddTypeToWantedAssets < ActiveRecord::Migration
  def change
    add_column :wanted_assets, :type, :string
    add_column :wanted_assets, :project_id, :integer
    add_column :wanted_assets, :quantity, :integer
    add_column :wanted_assets, :quantity_unit, :string
    add_column :wanted_assets, :status, :integer
  end
end
