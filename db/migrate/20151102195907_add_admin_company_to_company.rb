class AddAdminCompanyToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :admin_company, :boolean, default: false, null: false
  end
end
