class AddCompanyIdToCategoryGroups < ActiveRecord::Migration
  def change
    unless column_exists?(:category_groups, :company_id, :integer)
      add_reference :category_groups, :company, index: true, foreign_key: true
    end
  end
end
