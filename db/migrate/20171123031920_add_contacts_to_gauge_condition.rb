class AddContactsToGaugeCondition < ActiveRecord::Migration
  def change
    add_column :gauge_conditions, :contacts, :json
  end
end
