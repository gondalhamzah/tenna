class SetDefaultValueForSerialNumber < ActiveRecord::Migration
  def change
    change_column_default(:assets, :serialnumber, 0)
  end
end
