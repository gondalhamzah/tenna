class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :url
      t.string :description
      t.string :page_name
      t.integer :relative_order
      t.integer :emphasis_indicator
      t.boolean :is_visible

      t.timestamps null: false
    end
  end
end
