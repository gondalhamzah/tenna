class AddActionToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :action, :integer, null: false
  end
end
