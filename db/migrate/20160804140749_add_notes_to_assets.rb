class AddNotesToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :notes, :string
  end
end
