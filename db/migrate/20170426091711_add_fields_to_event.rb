class AddFieldsToEvent < ActiveRecord::Migration
  def change
    add_column :events, :raido, :integer
    add_reference :events, :asset, index: true, foreign_key: true
  end
end
