class AddDefaultProjectToCompany < ActiveRecord::Migration
  def change
    add_reference :companies, :project, index: true, foreign_key: true
  end
end
