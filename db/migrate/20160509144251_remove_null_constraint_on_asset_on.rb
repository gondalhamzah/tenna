class RemoveNullConstraintOnAssetOn < ActiveRecord::Migration
  def change
    change_column_null(:assets, :available_on, true)
  end
end
