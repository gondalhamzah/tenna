class CreateAssetVersions < ActiveRecord::Migration
  TEXT_BYTES = 1_073_741_823

  def change
    create_table :asset_versions do |t|
      t.string   :item_type, :null => false
      t.integer  :item_id,   :null => false
      t.string   :event,     :null => false
      t.string   :whodunnit
      t.text     :object,    :limit => TEXT_BYTES
      t.text     :object_changes
      t.datetime :created_at
    end
    add_index :asset_versions, [:item_type, :item_id]
  end
end
