class CreateMarks < ActiveRecord::Migration
  def change
    create_table :marks do |t|
      t.references :marker, polymorphic: true, null: false
      t.references :markable, polymorphic: true, null: false
      t.string :mark, null: false

      t.timestamps null: false
    end

    add_index :marks, [:marker_id, :marker_type, :mark]
    add_index :marks, [:markable_id, :markable_type, :mark]
  end
end
