class CreateCompaniesVersions < ActiveRecord::Migration
  def change
    create_table :companies_versions do |t|
      t.string :field_name
      t.string :from
      t.string :to
      t.integer :whodunnit
      t.integer :company_id
      t.string :action

      t.timestamps null: false
    end
    add_index :companies_versions, :field_name
    add_index :companies_versions, :whodunnit
    add_index :companies_versions, :company_id
  end
end
