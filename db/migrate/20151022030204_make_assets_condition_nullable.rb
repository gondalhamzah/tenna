class MakeAssetsConditionNullable < ActiveRecord::Migration
  def change
    change_column_null :assets, :condition, true
  end
end
