class AddUserToProjectExports < ActiveRecord::Migration
  def change
    add_reference :project_exports, :user, index: true, foreign_key: true
  end
end
