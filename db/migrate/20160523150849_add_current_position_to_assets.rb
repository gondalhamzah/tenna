class AddCurrentPositionToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :current_lat, :float
    add_column :assets, :current_lng, :float
  end
end
