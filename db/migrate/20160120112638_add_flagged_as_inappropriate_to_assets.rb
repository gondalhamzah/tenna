class AddFlaggedAsInappropriateToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :flagged_as_inappropriate, :boolean, default: false
  end
end
