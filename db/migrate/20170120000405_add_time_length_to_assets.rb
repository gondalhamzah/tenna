class AddTimeLengthToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :time_length, :integer, :default => 0
  end
end
