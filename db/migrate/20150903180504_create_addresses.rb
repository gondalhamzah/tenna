class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :line_1, null: false
      t.string :line_2
      t.string :city, null: false
      t.string :state, null: false
      t.string :zip, null: false

      t.references :addressable, polymorphic: true, index: true, null: false

      t.timestamps null: false
    end
  end
end
