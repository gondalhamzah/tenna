class AddCategoryGroupIdToCategory < ActiveRecord::Migration
  def change
    unless column_exists?(:categories, :category_group_id, :integer)
      add_reference :categories, :category_group, index: true, foreign_key: true
    end
  end
end
