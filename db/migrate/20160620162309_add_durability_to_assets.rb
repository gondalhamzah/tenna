class AddDurabilityToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :durability, :string
  end
end
