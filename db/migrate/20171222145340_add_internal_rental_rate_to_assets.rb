class AddInternalRentalRateToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :internal_daily_rental_rate, :integer
  end
end
