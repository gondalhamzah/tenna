class AddWantedIdToAssetsVersion < ActiveRecord::Migration
  def change
    add_column :assets_versions, :wanted_id, :integer
  end
end
