class ChangeDoneAtToDoneTimeStampOnDriverStop < ActiveRecord::Migration
  def change
    rename_column :driver_stops, :done_at, :done_timestamp
  end
end
