class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name, null: false
      t.string :region
      t.string :website
      t.integer 'contact_id', null: false
      t.integer :admin_seats

      t.timestamps null: false
    end
  end
end
