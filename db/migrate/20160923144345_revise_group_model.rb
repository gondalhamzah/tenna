class ReviseGroupModel < ActiveRecord::Migration
  def change
    add_column :groups, :group_num, :string, default: ''
    add_column :groups, :client, :string, default: ''
    rename_column :groups, :note, :notes
    add_column :groups, :description, :string, default: ''
    add_column :groups, :deleted_at, :datetime
    add_column :groups, :state, :string, default: 'active'
  end
end
