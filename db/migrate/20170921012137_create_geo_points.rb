class CreateGeoPoints < ActiveRecord::Migration
  def change
    create_table :geo_points do |t|
      t.references :geo_fence, index: true, foreign_key: true
      t.float :lat
      t.float :lng

      t.timestamps null: false
    end
  end
end
