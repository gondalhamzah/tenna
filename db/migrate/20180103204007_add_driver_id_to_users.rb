class AddDriverIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :current_driver_id, :integer
  end
end
