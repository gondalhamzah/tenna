class CreateLeads < ActiveRecord::Migration
  def change
    create_table :leads do |t|
      t.string :lead_type
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone_num
      t.string :industry
      t.string :reason_contact
      t.string :title
      t.string :company_name
      t.string :comment

      t.timestamps null: false
    end
  end
end
