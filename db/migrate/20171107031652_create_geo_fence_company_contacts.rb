class CreateGeoFenceCompanyContacts < ActiveRecord::Migration
  def change
    create_table :geo_fence_company_contacts do |t|
      t.references :geo_fence, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
