class AddCompanyIdToAnalyticFilterLabels < ActiveRecord::Migration
  def change
    add_column :analytic_filter_labels, :company_id, :integer
  end
end
