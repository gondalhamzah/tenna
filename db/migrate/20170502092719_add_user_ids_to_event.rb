class AddUserIdsToEvent < ActiveRecord::Migration
  def change
    add_column :events, :user_ids, :string
  end
end
