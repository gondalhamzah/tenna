class AddStartAndEndTimeToRoute < ActiveRecord::Migration
  def change
    add_column :routes, :start_time, :time
    add_column :routes, :end_time, :time
  end
end
