class AddStatusAndActionToAsset < ActiveRecord::Migration
  def change
  	add_column :assets_versions, :action, :string, default: ''
  	add_column :assets_versions, :state, :string, default: ''
  end
end
