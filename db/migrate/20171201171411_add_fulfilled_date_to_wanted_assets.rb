class AddFulfilledDateToWantedAssets < ActiveRecord::Migration
  def change
    add_column :wanted_assets, :fulfilled_date, :datetime
  end
end
