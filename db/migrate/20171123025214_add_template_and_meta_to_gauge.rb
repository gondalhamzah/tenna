class AddTemplateAndMetaToGauge < ActiveRecord::Migration
  def change
    add_column :gauges, :template, :string
    add_column :gauges, :meta, :json
  end
end
