class AddReallocationCountToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :reallocation_count, :integer, null: false, default: 0
  end
end
