class AddLastLocationChangedToAsset < ActiveRecord::Migration
  def change
    add_column :assets, :last_location_changed, :datetime
  end
end
