class AddInUseUntilToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :in_use_until, :datetime
  end
end
