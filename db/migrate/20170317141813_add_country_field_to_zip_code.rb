class AddCountryFieldToZipCode < ActiveRecord::Migration
  def change
    add_column :zip_codes, :country, :string
  end
end
