class AddStateToWantedAssets < ActiveRecord::Migration
  def change
    add_column :wanted_assets, :state, :string
  end
end
