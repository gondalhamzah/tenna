class RemoveAttachmentsAndMediaFromAssets < ActiveRecord::Migration
  def change
    remove_column :assets, :attachments
    remove_column :assets, :media
  end
end
