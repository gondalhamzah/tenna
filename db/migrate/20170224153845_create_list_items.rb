class CreateListItems < ActiveRecord::Migration
  def change
    create_table :list_items do |t|
      t.integer :list_id
      t.integer :status
      t.integer :asset_id
      t.string :token

      t.timestamps null: false
    end
  end
end
