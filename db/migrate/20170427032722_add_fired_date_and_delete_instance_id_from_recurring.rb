class AddFiredDateAndDeleteInstanceIdFromRecurring < ActiveRecord::Migration
  def change
    add_column :route_recurrings, :fired_date, :string
    remove_column :route_recurrings, :instance_id
  end
end
