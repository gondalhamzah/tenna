class CreateAssetsTrips < ActiveRecord::Migration
  def change
    create_table :assets_trips do |t|
      t.integer :device_id
      t.integer :asset_id
      t.string :tracking_code
      t.string :device_name
      t.float :distance
      t.float :average_speed
      t.float :max_speed
      t.float :spent_fuel
      t.float :start_lat
      t.float :start_lon
      t.float :end_lat
      t.float :end_lon
      t.datetime :start_time
      t.datetime :end_time
      t.string :start_address
      t.string :end_address
      t.integer :duration

      t.timestamps null: false
    end
  end
end
