class ChangeAssetNumDefault < ActiveRecord::Migration
  def change
    change_column :assets, :asset_num, :string, :default => ''
  end
end
