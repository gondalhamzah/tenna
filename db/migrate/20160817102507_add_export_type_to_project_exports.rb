class AddExportTypeToProjectExports < ActiveRecord::Migration
  def change
    add_column :project_exports, :export_type, :integer, default: 0
  end
end
