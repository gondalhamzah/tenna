class AddApiTokenAndUdidToUsers < ActiveRecord::Migration
  def change
    add_column :users, :api_token, :string
    add_index :users, :api_token
    add_column :users, :udid, :string
    add_index :users, :udid
  end
end
