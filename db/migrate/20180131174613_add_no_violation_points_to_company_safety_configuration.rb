class AddNoViolationPointsToCompanySafetyConfiguration < ActiveRecord::Migration
  def change
    add_column :company_safety_configurations, :high_points_no_violations, :decimal
    add_column :company_safety_configurations, :medium_points_no_violations, :decimal
    add_column :company_safety_configurations, :low_points_no_violations, :decimal
  end
end
