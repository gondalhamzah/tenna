class ChangeNotesDefault < ActiveRecord::Migration
  def change
    change_column :assets, :notes, :string, :default => ''
  end
end
