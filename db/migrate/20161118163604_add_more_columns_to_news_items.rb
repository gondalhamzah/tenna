class AddMoreColumnsToNewsItems < ActiveRecord::Migration
  def change
    add_column :news_items, :deleted_at, :date
    add_column :news_items, :active, :boolean
    add_column :news_items, :created_by, :integer
    add_column :news_items, :last_updated_by, :integer
  end
end
