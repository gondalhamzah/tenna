class RemoveNullConstraintInCsvImportsForProjectId < ActiveRecord::Migration
  def up
    change_column :csv_imports, :project_id , :integer, :null => true
  end

  def down
    change_column :csv_imports, :project_id , :integer, :null => false
  end
end
