class CreateGeoFenceOtherContacts < ActiveRecord::Migration
  def change
    create_table :geo_fence_other_contacts do |t|
      t.references :geo_fence, index: true, foreign_key: true
      t.string :email
      t.string :phone

      t.timestamps null: false
    end
  end
end
