class AddReallocationCountToWantedAssets < ActiveRecord::Migration
  def change
    add_column :wanted_assets, :reallocation_count, :integer, default: 0
  end
end
