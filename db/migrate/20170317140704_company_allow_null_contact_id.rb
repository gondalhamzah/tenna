class CompanyAllowNullContactId < ActiveRecord::Migration
  def up
    change_column :companies, :contact_id, :integer, :null => true
  end

  def down
    change_column :companies, :contact_id, :integer, :null => false
  end
end
