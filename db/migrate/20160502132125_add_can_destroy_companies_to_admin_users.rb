class AddCanDestroyCompaniesToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :can_destroy_companies, :boolean, default: false
  end
end
