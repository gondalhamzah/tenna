class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.integer :company_id, index: true, foreign_key: true
      t.integer :project_id, index: true, foreign_key: true
      t.integer :asset_count

      t.timestamps
    end
  end
end
