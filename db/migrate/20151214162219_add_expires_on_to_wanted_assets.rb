class AddExpiresOnToWantedAssets < ActiveRecord::Migration
  def change
    add_column :wanted_assets, :expires_on, :date, null: false
  end
end
