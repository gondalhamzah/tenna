class CreateGfAlertLogs < ActiveRecord::Migration
  def change
    create_table :gf_alert_logs do |t|
      t.references :geo_fence, index: true, foreign_key: true
      t.references :asset, index: true, foreign_key: true
      t.boolean :enter
      t.boolean :dismissed

      t.timestamps null: false
    end
  end
end
