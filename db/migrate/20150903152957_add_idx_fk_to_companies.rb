class AddIdxFkToCompanies < ActiveRecord::Migration
  def change
    add_index 'companies', ['contact_id'], name: 'index_companies_on_contact_id', using: :btree
    add_foreign_key 'companies', 'users', column: :contact_id
  end
end
