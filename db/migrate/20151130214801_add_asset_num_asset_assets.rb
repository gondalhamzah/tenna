class AddAssetNumAssetAssets < ActiveRecord::Migration
  def change
  	add_column :assets, :asset_num, :string, unique: true
  end
end
