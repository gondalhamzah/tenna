class AddExportFilteredAssetsToProjectExports < ActiveRecord::Migration
  def change
    add_column :project_exports, :export_filtered_assets, :text
    add_column :project_exports, :is_filtered_asset, :boolean
  end
end
