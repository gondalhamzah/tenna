class AddContactSmsNumberAndContactEmailToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :contact_sms_number, :string
    add_column :projects, :contact_email, :string
  end
end
