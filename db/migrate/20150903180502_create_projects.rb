class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name, null: false
      t.string :description
      t.references :company, index: true, foreign_key: true, null: false
      t.integer 'contact_id', null: false
      t.string :duration, null: false
      t.date :closeout_date

      t.timestamps null: false
    end

    add_index 'projects', ['contact_id'], name: 'index_projects_on_contact_id', using: :btree
    add_foreign_key 'projects', 'users', column: :contact_id
  end
end
