class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :type, null: false
      t.string :file, null: false
      t.references :asset, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
