class AddZoomLevelToGeoFence < ActiveRecord::Migration
  def change
    add_column :geo_fences, :zoom_level, :integer
  end
end
