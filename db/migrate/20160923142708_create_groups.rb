class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.references :project, index: true, foreign_key: true
      t.string :name
      t.references :company, index: true, foreign_key: true
      t.string :note

      t.timestamps null: false
    end
  end
end
