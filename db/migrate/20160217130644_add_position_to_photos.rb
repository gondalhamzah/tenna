class AddPositionToPhotos < ActiveRecord::Migration
  def change
    add_column :attachments, :position, :integer, default: 0
  end
end
