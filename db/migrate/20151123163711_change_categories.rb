class ChangeCategories < ActiveRecord::Migration
  def up
    remove_foreign_key :categories, column: :parent_category_id
    remove_reference :categories, :parent_category, index: true
    add_column :categories, :category_type, :string
  end

  def down
    remove_column :categories, :category_type
    add_reference :categories, :parent_category, index: true
    add_foreign_key :categories, 'categories', column: :parent_category_id
  end
end
