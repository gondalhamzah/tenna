class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.integer :company_id, null: false
      t.string :api_key
      t.datetime :api_key_created_at
      t.datetime :api_key_updated_at
      t.string :category, null: false
      t.integer :number_of_units
      t.string :state, null: false
      t.text :note
      t.datetime :deleted_at
      t.string :vendor_id

      t.timestamps null: false
    end
  end
end
