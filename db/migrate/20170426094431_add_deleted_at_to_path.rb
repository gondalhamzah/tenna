class AddDeletedAtToPath < ActiveRecord::Migration
  def change
    add_column :paths, :deleted_at, :datetime
  end
end
