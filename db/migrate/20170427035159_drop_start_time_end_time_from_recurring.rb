class DropStartTimeEndTimeFromRecurring < ActiveRecord::Migration
  def change
    remove_column :route_recurrings, :start_time
    remove_column :route_recurrings, :end_time
  end
end
