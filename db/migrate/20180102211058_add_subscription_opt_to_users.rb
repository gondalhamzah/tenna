class AddSubscriptionOptToUsers < ActiveRecord::Migration
  def change
    add_column :users, :subscription_opt, :boolean, default: true
  end
end
