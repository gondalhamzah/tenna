class CreateCompanySafetyConfigurations < ActiveRecord::Migration
  def change
    create_table :company_safety_configurations do |t|
      t.references :gauge, index: true, foreign_key: true
      t.references :company, index: true, foreign_key: true
      t.integer :score_type, default: 0
      t.decimal :score_weight, default: 100
      t.decimal :high_points, default: 0
      t.decimal :medium_points, default: 0
      t.decimal :low_points, default: 0

      t.timestamps null: false
    end
  end
end
