class ChangeTitleLimitForUsers < ActiveRecord::Migration
  def change
    change_column :users, :title, :string, :limit => 255
  end
end
