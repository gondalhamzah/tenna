class AddProcessedToEvent < ActiveRecord::Migration
  def change
    add_column :events, :processed, :bool, :default=>false
  end
end
