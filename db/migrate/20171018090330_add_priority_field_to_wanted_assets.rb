class AddPriorityFieldToWantedAssets < ActiveRecord::Migration
  def change
    add_column :wanted_assets, :priority, :string
  end
end
