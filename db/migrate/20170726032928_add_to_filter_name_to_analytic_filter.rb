class AddToFilterNameToAnalyticFilter < ActiveRecord::Migration
  def change
    add_column :analytic_filters, :name, :string
  end
end
