class AddDefaultValuesToGroup < ActiveRecord::Migration
  def change
    change_column :groups, :group_num, :string, :default => ''
    change_column :groups, :notes, :string, :default => ''
    change_column :groups, :description, :string, :default => ''
  end
end
