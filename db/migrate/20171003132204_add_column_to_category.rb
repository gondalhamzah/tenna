class AddColumnToCategory < ActiveRecord::Migration
  def change
    unless column_exists?(:categories, :category_group_id, :integer)
      add_column :categories, :category_group_id, :integer
    end
  end
end
