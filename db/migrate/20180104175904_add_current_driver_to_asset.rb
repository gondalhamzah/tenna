class AddCurrentDriverToAsset < ActiveRecord::Migration
  def change
    add_column :assets, :current_driver_id, :integer
  end
end
