class ChangeColumnsInAddresses < ActiveRecord::Migration
  def change
    change_column_null :addresses, :line_1, true
    change_column_null :addresses, :city, true
    change_column_null :addresses, :state, true
  end
end
