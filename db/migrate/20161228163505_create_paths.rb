class CreatePaths < ActiveRecord::Migration
  def change
    create_table :paths do |t|
      t.integer :route_id
      t.text :address
      t.float :done_lat
      t.float :done_lng
      t.datetime :done_timestamp
      t.text :service_notes
      t.string :waypoint_type
      t.integer :waypoint_id
      t.integer :sequence
      t.integer :time_on_site
      t.integer :priority
      t.integer :load
      t.time :begin_window
      t.time :end_window
      t.boolean :notify_customer, default: false

      t.timestamps null: false
    end
  end
end
