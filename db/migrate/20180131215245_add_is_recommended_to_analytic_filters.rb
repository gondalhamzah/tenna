class AddIsRecommendedToAnalyticFilters < ActiveRecord::Migration
  def change
    add_column :analytic_filters, :is_recommended, :boolean, default: false
  end
end
