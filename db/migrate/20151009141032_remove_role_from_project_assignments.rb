class RemoveRoleFromProjectAssignments < ActiveRecord::Migration
  def change
    remove_column :project_assignments, :role, :string
  end
end
