class AddActionToAssetsVersions < ActiveRecord::Migration
  def change
    add_column :assets_versions, :action, :string, default: ''
  end
end
