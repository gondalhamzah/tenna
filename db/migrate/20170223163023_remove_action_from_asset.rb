class RemoveActionFromAsset < ActiveRecord::Migration
  def change
    remove_column :assets, :action, :bool
  end
end
