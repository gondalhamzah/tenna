class AddApiKeyStatusToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :api_key_status, :boolean
  end
end