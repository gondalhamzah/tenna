class ChangeCategoryTypeNull < ActiveRecord::Migration
  def change
    change_column_null :categories, :category_type, false
  end
end
