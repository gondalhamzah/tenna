class CreateAssetsVersions < ActiveRecord::Migration
  def change
    create_table :assets_versions do |t|
      t.integer :asset_id
      t.string :field_name
      t.string :from
      t.string :to
      t.integer :whodunnit
      t.integer :project_id
      t.integer :company_id

      t.timestamps null: false
    end
    add_index :assets_versions, :asset_id
    add_index :assets_versions, :field_name
    add_index :assets_versions, :whodunnit
    add_index :assets_versions, :project_id
    add_index :assets_versions, :company_id
  end
end
