class AddAnalyticFilterLabelIdToAnalyticFilters < ActiveRecord::Migration
  def change
    add_column :analytic_filters, :analytic_filter_label_id, :integer
  end
end
