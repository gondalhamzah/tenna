class RemoveNullConstraintsOnContactAndCompanyInProjects < ActiveRecord::Migration
  def up
    change_column :projects, :contact_id , :integer, :null => true
    change_column :projects, :company_id , :integer, :null => true
  end

  def down
    change_column :projects, :contact_id , :integer, :null => false
    change_column :projects, :company_id , :integer, :null => false
  end
end
