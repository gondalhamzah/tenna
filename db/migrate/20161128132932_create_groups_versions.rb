class CreateGroupsVersions < ActiveRecord::Migration
  def change
    create_table :groups_versions do |t|
      t.integer :group_id
      t.string :field_name
      t.string :from
      t.string :to
      t.string :action
      t.references :user, index: true, foreign_key: true
      t.references :company, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
