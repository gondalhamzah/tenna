class ChangeTypeToShape < ActiveRecord::Migration
  def change
    rename_column :geo_fences, :type, :shape
  end
end
