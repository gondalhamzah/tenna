class CreateGaugeConditions < ActiveRecord::Migration
  def change
    create_table :gauge_conditions do |t|
      t.references :gauge_alert_group, index: true, foreign_key: true
      t.jsonb :values
      t.boolean :active
      t.text :notes
      t.string :severe_level

      t.timestamps null: false
    end
  end
end
