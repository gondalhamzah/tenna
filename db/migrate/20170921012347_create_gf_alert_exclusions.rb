class CreateGfAlertExclusions < ActiveRecord::Migration
  def change
    create_table :gf_alert_exclusions do |t|
      t.references :geo_fence, index: true, foreign_key: true
      t.references :category_group, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true
      t.references :asset, index: true, foreign_key: true
      t.boolean :exclude
      t.boolean :alert_enter_site
      t.boolean :alert_exit_site

      t.timestamps null: false
    end
  end
end
