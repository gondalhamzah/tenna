class CreateGeoFences < ActiveRecord::Migration
  def change
    create_table :geo_fences do |t|
      t.references :company, index: true, foreign_key: true
      t.string :type
      t.string :name
      t.string :color
      t.float :radius
      t.boolean :alert_enter_site
      t.boolean :alert_exit_site
      t.text :notes

      t.timestamps null: false
    end
  end
end
