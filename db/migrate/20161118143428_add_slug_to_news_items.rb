class AddSlugToNewsItems < ActiveRecord::Migration
  def change
    add_column :news_items, :slug, :string, unique: :true
    add_index :news_items, :slug
  end
end
