class CreateCategoryGroups < ActiveRecord::Migration
  def change
    unless table_exists?(:category_groups)
      create_table :category_groups do |t|
        t.string :name
        t.timestamps null: false
      end
    end
  end
end
