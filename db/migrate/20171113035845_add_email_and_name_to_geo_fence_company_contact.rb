class AddEmailAndNameToGeoFenceCompanyContact < ActiveRecord::Migration
  def change
    add_column :geo_fence_company_contacts, :email, :bool
    add_column :geo_fence_company_contacts, :text, :bool
  end
end
