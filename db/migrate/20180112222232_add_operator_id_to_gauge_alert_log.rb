class AddOperatorIdToGaugeAlertLog < ActiveRecord::Migration
  def change
    add_column :gauge_alert_logs, :operator_id, :integer
  end
end
