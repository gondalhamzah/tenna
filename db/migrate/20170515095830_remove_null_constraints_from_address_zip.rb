class RemoveNullConstraintsFromAddressZip < ActiveRecord::Migration
  def change
  	change_column :addresses, :zip, :string, :null => true
  end
end
