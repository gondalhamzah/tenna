class AddTrackingCodesToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :qr, :integer
    add_column :assets, :rfid, :integer
    add_column :assets, :lora, :integer
    add_column :assets, :sigfox, :integer
    add_column :assets, :upc, :integer
    add_column :assets, :bt, :integer
    add_column :assets, :cellular, :integer
  end
end
