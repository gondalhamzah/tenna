class AddDurationToAnalyticFilters < ActiveRecord::Migration
  def change
    add_column :analytic_filters, :duration, :integer
  end
end
