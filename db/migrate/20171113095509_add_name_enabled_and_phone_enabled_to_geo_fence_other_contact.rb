class AddNameEnabledAndPhoneEnabledToGeoFenceOtherContact < ActiveRecord::Migration
  def change
    add_column :geo_fence_other_contacts, :email_enabled, :bool
    add_column :geo_fence_other_contacts, :phone_enabled, :bool
    add_column :geo_fence_other_contacts, :name, :string
  end
end
