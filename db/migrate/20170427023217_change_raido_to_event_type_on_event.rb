class ChangeRaidoToEventTypeOnEvent < ActiveRecord::Migration
  def change
    rename_column :events, :raido, :event_type
  end
end
