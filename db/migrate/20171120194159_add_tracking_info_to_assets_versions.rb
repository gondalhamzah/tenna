class AddTrackingInfoToAssetsVersions < ActiveRecord::Migration
  def change
    add_column :assets_versions, :tracking_info, :text
  end
end
