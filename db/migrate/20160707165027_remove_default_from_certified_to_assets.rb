class RemoveDefaultFromCertifiedToAssets < ActiveRecord::Migration
  def change
    change_column :assets, :certified, :boolean, :null => true
  end
end
