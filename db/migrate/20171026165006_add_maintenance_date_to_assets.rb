class AddMaintenanceDateToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :maintenance_date, :datetime
  end
end
