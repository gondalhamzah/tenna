class AddMarketValueToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :market_value, :integer
  end
end
