class AddCompanyIdToTrackingCodes < ActiveRecord::Migration
  def change
    add_column :tracking_codes, :company_id, :integer
  end
end
