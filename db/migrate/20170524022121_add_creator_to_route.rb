class AddCreatorToRoute < ActiveRecord::Migration
  def change
    add_column :routes, :creator_id, :integer
  end
end
