class ChangeUserForeignKey < ActiveRecord::Migration
  def change
    remove_foreign_key "users", "companies"
    add_foreign_key "users", "companies", on_delete: :cascade
  end
end
