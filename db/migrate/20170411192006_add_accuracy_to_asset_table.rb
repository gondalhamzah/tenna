class AddAccuracyToAssetTable < ActiveRecord::Migration
  def change
    add_column :assets, :accuracy, :float
  end
end
