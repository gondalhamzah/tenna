class AddTimezoneAndCreatorToEvent < ActiveRecord::Migration
  def change
    add_column :events, :timezone, :string
    add_reference :events, :user, index: true, foreign_key: true
  end
end
