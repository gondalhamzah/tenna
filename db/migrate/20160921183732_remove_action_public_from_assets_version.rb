class RemoveActionPublicFromAssetsVersion < ActiveRecord::Migration
  def change
    remove_column :assets_versions, :action
    remove_column :assets_versions, :state
  end
end
