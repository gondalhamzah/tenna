class AddProjectIdToCsvImports < ActiveRecord::Migration
  def change
    add_column :csv_imports, :project_id, :integer, null: false
    add_index :csv_imports, :project_id
    add_foreign_key :csv_imports, :projects, column: :project_id
  end
end
