class AddGroupIdToAssets < ActiveRecord::Migration
  def change
    add_reference :assets, :groups, index: true
  end
end
