class AddQuantityUnitToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :quantity_unit, :string
  end
end
