class AddIsInventoryToAsset < ActiveRecord::Migration
  def change
    add_column :assets, :is_inventory, :boolean, default: false
  end
end
