class ChangeDateTimeInUseUntilToAssets < ActiveRecord::Migration
  def change
    change_column :assets, :in_use_until, :date
  end
end
