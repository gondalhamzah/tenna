class AddFilterFieldToList < ActiveRecord::Migration
  def change
    add_column :lists, :filter, :string
  end
end
