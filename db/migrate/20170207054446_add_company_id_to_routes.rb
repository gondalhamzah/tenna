class AddCompanyIdToRoutes < ActiveRecord::Migration
  def change
    add_column :routes, :company_id, :integer
  end
end
