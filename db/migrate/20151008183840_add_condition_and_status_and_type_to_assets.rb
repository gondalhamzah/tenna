class AddConditionAndStatusAndTypeToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :condition, :integer, null: false
    add_column :assets, :status, :integer, null: false
    add_column :assets, :type, :integer, null: false
  end
end
