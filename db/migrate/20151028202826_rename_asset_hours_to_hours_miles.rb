class RenameAssetHoursToHoursMiles < ActiveRecord::Migration
  def change
    rename_column :assets, :hours, :hours_miles
  end
end
