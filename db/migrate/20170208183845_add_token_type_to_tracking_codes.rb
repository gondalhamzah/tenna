class AddTokenTypeToTrackingCodes < ActiveRecord::Migration
  def change
    add_column :tracking_codes, :token_type, :string, after: :token, default: 'qr'
  end
end
