class AddGroupIdToAssetsVersions < ActiveRecord::Migration
  def change
    add_reference :assets_versions, :group, index: true, foreign_key: true
  end
end
