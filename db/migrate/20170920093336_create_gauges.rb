class CreateGauges < ActiveRecord::Migration
  def change
    create_table :gauges do |t|
      t.string :name
      t.string :icon

      t.timestamps null: false
    end
  end
end
