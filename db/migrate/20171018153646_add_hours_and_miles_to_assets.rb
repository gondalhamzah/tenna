class AddHoursAndMilesToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :hours, :integer
    add_column :assets, :miles, :integer

    # reversible do |direction|
    #   Asset.reset_column_information
    #   Asset.find_each(batch_size: 500) do |asset|
    #     direction.up do
    #       hours_miles = asset.hours_miles
    #       asset.update(hours: hours_miles, miles: hours_miles)
    #     end
    #
    #     direction.down do
    #       hours_miles = asset.hours || asset.miles
    #       asset.update(hours_miles: hours_miles)
    #     end
    #   end
    # end
    # remove_column :assets, :hours_miles, :integer
  end
end
