class RemoveNullConstraintOnWantedAssetPrice < ActiveRecord::Migration
  def change
    change_column_null(:wanted_assets, :price, true)
  end
end
