class SetDefaultValuesTagsAndTrackers < ActiveRecord::Migration
  def change
    change_column_default(:assets, :qr, 0)
    change_column_default(:assets, :rfid, 0)
    change_column_default(:assets, :lora, 0)
    change_column_default(:assets, :sigfox, 0)
    change_column_default(:assets, :upc, 0)
    change_column_default(:assets, :bt, 0)
    change_column_default(:assets, :cellular, 0)
  end
end
