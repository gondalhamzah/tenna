class AddApiRpmKeyToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :api_rpm, :integer, index: true, default: 0
    add_column :companies, :api_key, :string, index: true, unique: true
  end
end
