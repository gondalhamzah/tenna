class ChangeTypeOfMetadataInNotifications < ActiveRecord::Migration
  def up
    change_column :notifications, :metadata, :text
  end

  def down
    change_column :notifications, :metadata, :string
  end
end
