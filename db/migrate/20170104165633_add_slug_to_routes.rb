class AddSlugToRoutes < ActiveRecord::Migration
  def change
    add_column :routes, :slug, :string, unique: :true
    add_index :routes, :slug
  end
end
