class Change < ActiveRecord::Migration
  def change
    change_column :assets, :status, :string, null: true
    change_column :wanted_assets, :status, :string, null: true
  end
end
