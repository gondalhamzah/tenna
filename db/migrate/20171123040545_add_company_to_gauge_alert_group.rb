class AddCompanyToGaugeAlertGroup < ActiveRecord::Migration
  def change
    add_reference :gauge_alert_groups, :company, index: true, foreign_key: true
  end
end
