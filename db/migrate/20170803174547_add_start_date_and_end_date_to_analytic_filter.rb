class AddStartDateAndEndDateToAnalyticFilter < ActiveRecord::Migration
  def change
    add_column :analytic_filters, :start_date, :date
    add_column :analytic_filters, :end_date, :date
  end
end
