class CreateGaugeAlertGroups < ActiveRecord::Migration
  def change
    create_table :gauge_alert_groups do |t|
      t.references :gauge, index: true, foreign_key: true
      t.references :category_group, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true
      t.references :asset, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
