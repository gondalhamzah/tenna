class AddPaymentFieldToSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :payment, :boolean
  end
end
