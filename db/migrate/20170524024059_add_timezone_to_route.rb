class AddTimezoneToRoute < ActiveRecord::Migration
  def change
    add_column :routes, :timezone, :string
  end
end
