class ChangeTokenTypeDefaultValue < ActiveRecord::Migration
  def up
    remove_column :tracking_codes, :token_type, :string
    add_column :tracking_codes, :token_type, :integer, index: true, default: 0
  end

  def down
    add_column :tracking_codes, :token_type, :string, after: :token, default: 'qr'
  end
end
