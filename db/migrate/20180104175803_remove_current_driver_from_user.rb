class RemoveCurrentDriverFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :current_driver_id
  end
end
