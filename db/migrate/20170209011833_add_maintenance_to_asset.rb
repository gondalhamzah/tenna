class AddMaintenanceToAsset < ActiveRecord::Migration
  def change
    add_column :assets, :maintenance, :boolean, default: false
  end
end
