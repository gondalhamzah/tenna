class AddGroupRefToAssets < ActiveRecord::Migration
  def change
    add_reference :assets, :group, index: true, foreign_key: true
  end
end
