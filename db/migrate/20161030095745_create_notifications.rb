class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :admin_user, index: true, foreign_key: true
      t.string :message
      t.datetime :notification_datetime
      t.integer :days_before
      t.integer :duration
      t.boolean :is_active
      t.string :notification_type
      t.string :notification_medium
      t.string :location
      t.string :metadata
      t.inet :ip
      t.integer :client_type

      t.timestamps null: false
    end
  end
end
