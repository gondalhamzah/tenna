class AddLatLngToPaths < ActiveRecord::Migration
  def change
    add_column :paths, :lat, :float
    add_column :paths, :lng, :float
  end
end
