class ChangeAssetDefaultValue < ActiveRecord::Migration
  def change
    change_column :assets, :time_length, :integer, default: 1
  end
end
