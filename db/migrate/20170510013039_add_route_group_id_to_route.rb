class AddRouteGroupIdToRoute < ActiveRecord::Migration
  def change
    change_table 'routes' do |t|
      t.references :route_group, index: true, foreign_key: true
    end
  end
end
