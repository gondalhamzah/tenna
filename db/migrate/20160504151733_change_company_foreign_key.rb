class ChangeCompanyForeignKey < ActiveRecord::Migration
  def change
    remove_foreign_key "companies", "contact"
    add_foreign_key "companies", "users", on_delete: :cascade, column: "contact_id"
  end
end
