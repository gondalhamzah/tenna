class AddTypeAndIsAlarmToGauge < ActiveRecord::Migration
  def change
    add_column :gauges, :type, :string
    add_column :gauges, :is_alarm, :bool
  end
end
