class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.string :title
      t.text :description
      t.date :post_date
      t.integer :priority
      t.boolean :featured

      t.timestamps null: false
    end
  end
end
