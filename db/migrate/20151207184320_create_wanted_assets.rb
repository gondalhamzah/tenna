class CreateWantedAssets < ActiveRecord::Migration
  def change
    create_table :wanted_assets do |t|
      t.string :title, null: false
      t.string :description
      t.string :make
      t.string :model
      t.integer :hours_miles
      t.integer :price, null: false
      t.date :needed_on
      t.integer :company_id, null: false
      t.integer :creator_id, null: false
      t.integer :category_id
      t.integer :zip_code_id

      t.timestamps null: false
    end
    add_index :wanted_assets, :company_id
    add_index :wanted_assets, :creator_id
    add_index :wanted_assets, :category_id
  end
end
