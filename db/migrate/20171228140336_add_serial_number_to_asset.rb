class AddSerialNumberToAsset < ActiveRecord::Migration
  def change
    add_column :assets, :serialnumber, :integer
  end
end
