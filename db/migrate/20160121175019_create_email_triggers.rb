class CreateEmailTriggers < ActiveRecord::Migration
  def change
    create_table :email_triggers do |t|
      t.string :trigger
      t.references :email_template, index: true, foreign_key: true
      t.boolean :enabled

      t.timestamps null: false
    end
  end
end
