class AddContactIdToWantedAssets < ActiveRecord::Migration
  def change
    add_column :wanted_assets, :contact_id, :integer
    add_index :wanted_assets, :contact_id
  end
end
