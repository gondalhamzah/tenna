class AddLatLng < ActiveRecord::Migration
  def change
    add_column :addresses, :lat, :float
    add_column :addresses, :lng, :float
    create_table :zip_codes do |t|
      t.string :zip_code, null: false
      t.float :lat
      t.float :lng
      t.string :city
      t.string :state

      t.timestamps null: false
    end
    add_reference :assets, :zip_code, foreign_key: true, null: false
    remove_column :assets, :location_zip, :string
  end
end
