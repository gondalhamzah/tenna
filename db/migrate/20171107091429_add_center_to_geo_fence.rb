class AddCenterToGeoFence < ActiveRecord::Migration
  def change
    add_column :geo_fences, :center, :string
  end
end
