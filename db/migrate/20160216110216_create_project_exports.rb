class CreateProjectExports < ActiveRecord::Migration
  def change
    create_table :project_exports do |t|
      t.string :file
      t.datetime :enqueued_at
      t.datetime :exported_at
      t.references :project, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
