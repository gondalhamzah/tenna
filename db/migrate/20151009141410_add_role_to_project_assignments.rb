class AddRoleToProjectAssignments < ActiveRecord::Migration
  def change
    add_column :project_assignments, :role, :integer
  end
end
