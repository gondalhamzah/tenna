class ChangeAssetPriceToInteger < ActiveRecord::Migration
  def change
    change_column :assets, :price, :integer
  end
end
