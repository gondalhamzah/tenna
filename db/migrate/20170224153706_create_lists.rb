class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.integer :company_id
      t.integer :list_type

      t.timestamps null: false
    end
  end
end
