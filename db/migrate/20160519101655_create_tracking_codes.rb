class CreateTrackingCodes < ActiveRecord::Migration
  def change
    create_table :tracking_codes do |t|
      t.string :token, index: true, unique: true
      t.references :resource, index: true, polymorphic: true
      t.integer :status, default: 0
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
