class AddGraphTypeToGauge < ActiveRecord::Migration
  def change
    add_column :gauges, :graph_type, :string
  end
end
