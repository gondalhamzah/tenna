class AddYearToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :year, :integer
  end
end
