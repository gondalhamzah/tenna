class ChangeTrackingInfoToJson < ActiveRecord::Migration
  def change
    remove_column :assets_versions, :tracking_info, :text
    add_column :assets_versions, :tracking_info, :jsonb
  end
end
