class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name, null: false
      t.integer 'parent_category_id'

      t.timestamps null: false
    end

    add_index 'categories', ['parent_category_id'], name: 'index_categories_on_parent_category_id', using: :btree
    add_foreign_key 'categories', 'categories', column: :parent_category_id
  end
end
