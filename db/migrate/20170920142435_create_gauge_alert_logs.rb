class CreateGaugeAlertLogs < ActiveRecord::Migration
  def change
    create_table :gauge_alert_logs do |t|
      t.references :gauge_condition, index: true, foreign_key: true
      t.boolean :dismissed
      t.references :asset, index: true, foreign_key: true
      t.jsonb :values

      t.timestamps null: false
    end
  end
end
