class CreateVideoUsages < ActiveRecord::Migration
  def change
    create_table :video_usages do |t|
      t.integer :user_id
      t.integer :video_id
      t.timestamp :when_watched

      t.timestamps null: false
    end
  end
end
