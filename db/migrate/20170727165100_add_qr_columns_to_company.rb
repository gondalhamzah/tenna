class AddQrColumnsToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :qr_status, :integer
    add_column :companies, :qr_print_type, :integer
    add_column :companies, :qr_format, :integer
  end
end
