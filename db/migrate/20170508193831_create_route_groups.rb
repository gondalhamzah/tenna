class CreateRouteGroups < ActiveRecord::Migration
  def change
    create_table :route_groups do |t|
      t.string :serialized_driver_metadata
      t.string :filters

      t.timestamps null: false
    end
  end
end
