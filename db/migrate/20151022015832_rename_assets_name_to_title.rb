class RenameAssetsNameToTitle < ActiveRecord::Migration
  def change
    rename_column :assets, :name, :title
  end
end
