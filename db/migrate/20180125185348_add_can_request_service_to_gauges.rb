class AddCanRequestServiceToGauges < ActiveRecord::Migration
  def change
    add_column :gauges, :can_request_service, :boolean
  end
end
