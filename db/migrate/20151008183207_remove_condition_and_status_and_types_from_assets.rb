class RemoveConditionAndStatusAndTypesFromAssets < ActiveRecord::Migration
  def up
    remove_column :assets, :condition
    remove_column :assets, :status
    remove_column :assets, :type
  end

  def down
    add_column :assets, :condition, :string, null: false
    add_column :assets, :status, :string, null: false
    add_column :assets, :type, :string, null: false    
  end
end
