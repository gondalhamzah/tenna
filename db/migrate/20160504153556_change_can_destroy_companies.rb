class ChangeCanDestroyCompanies < ActiveRecord::Migration
  def change
    remove_column :admin_users, :can_destroy_companies
    add_column :admin_users, :role, :integer, default: 0
  end
end
