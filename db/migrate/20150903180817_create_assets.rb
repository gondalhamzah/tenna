class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :type, null: false

      t.string :name, null: false
      t.string :description
      t.references :company, index: true, foreign_key: true
      t.references :project, index: true, foreign_key: true
      t.integer 'creator_id', null: false
      t.references :category, index: true, foreign_key: true, null: false
      t.string :location_zip, null: false
      t.string :make
      t.string :model
      t.integer :hours
      t.decimal :price
      t.date :available_on, null: false
      t.date :expires_on, null: false
      t.integer :quantity, null: false
      t.string :units
      t.boolean :certified, null: false, default: false
      t.boolean :rental, null: false, default: false
      t.string :condition, null: false
      t.boolean :public, null: false, default: false
      t.string :status, null: false

      t.json :media
      t.json :attachments

      t.timestamps null: false
    end

    add_index 'assets', ['creator_id'], name: 'index_assets_on_creator_id', using: :btree
    add_foreign_key 'assets', 'users', column: :creator_id
  end
end
