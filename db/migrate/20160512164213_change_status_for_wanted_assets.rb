class ChangeStatusForWantedAssets < ActiveRecord::Migration
  def change
    change_column :wanted_assets, :status, :integer, default: 0
  end
end
