class ChangeRoleInProjectAssignments < ActiveRecord::Migration
  def change
    change_column_null :project_assignments, :role, false
  end
end
