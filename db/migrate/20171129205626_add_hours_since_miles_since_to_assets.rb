class AddHoursSinceMilesSinceToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :miles_since, :integer
    add_column :assets, :hours_since, :integer
    add_column :assets, :use_j1939_miles, :boolean
    add_column :assets, :use_j1939_engine_hours, :boolean
  end
end
