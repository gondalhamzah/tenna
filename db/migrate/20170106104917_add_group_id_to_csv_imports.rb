class AddGroupIdToCsvImports < ActiveRecord::Migration
  def change
    add_reference :csv_imports, :group, index: true, foreign_key: true
  end
end
