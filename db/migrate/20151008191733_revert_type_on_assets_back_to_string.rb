class RevertTypeOnAssetsBackToString < ActiveRecord::Migration
  def change
    change_column :assets, :type, :string
  end
end
