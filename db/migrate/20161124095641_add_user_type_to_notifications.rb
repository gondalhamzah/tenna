class AddUserTypeToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :user_type, :integer
  end
end
