class AddSlugToWantedAssets < ActiveRecord::Migration
  def change
    add_column :wanted_assets, :slug, :string
    add_index :wanted_assets, :slug, unique: true
  end
end
