class ChangeColumnsInProjects < ActiveRecord::Migration
  def change
    change_column_null :projects, :duration, true
    change_column_null :projects, :closeout_date, false
  end
end
