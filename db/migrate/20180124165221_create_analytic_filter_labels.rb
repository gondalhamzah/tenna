class CreateAnalyticFilterLabels < ActiveRecord::Migration
  def change
    create_table :analytic_filter_labels do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
