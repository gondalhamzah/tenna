class CreateAnalyticFilters < ActiveRecord::Migration
  def change
    create_table :analytic_filters do |t|
      t.references :company, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.text :filter_json

      t.timestamps null: false
    end
  end
end
