class CreateDriverStops < ActiveRecord::Migration
  def change
    create_table :driver_stops do |t|
      t.references :route_recurring, index: true, foreign_key: true
      t.references :path, index: true, foreign_key: true
      t.datetime :done_at
      t.float :done_lat
      t.float :done_lng

      t.timestamps null: false
    end
  end
end
