class AddViewCountToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :view_count, :integer, null: false, default: 0
  end
end
