class CreateHooks < ActiveRecord::Migration
  def change
    create_table :hooks do |t|
    	t.string :event_name
		t.string :subscription_url
		t.string :target_url
		t.integer :company_id

		t.timestamps null: false
    end
  end
end
