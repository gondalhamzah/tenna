class CreateCsvImports < ActiveRecord::Migration
  def change
    create_table :csv_imports do |t|
      t.belongs_to :user, null: false
      t.string     :file, null: false
      t.integer    :state, default: 0, null: false
      t.integer    :job_type, default: 0, null: false
      t.text       :message
      t.timestamps null: false
    end
    add_index :csv_imports, :user_id
    add_foreign_key :csv_imports, :users, column: :user_id
  end
end
