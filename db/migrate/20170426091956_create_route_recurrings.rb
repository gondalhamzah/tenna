class CreateRouteRecurrings < ActiveRecord::Migration
  def change
    create_table :route_recurrings do |t|
      t.references :route, index: true, foreign_key: true
      t.integer :instance_id
      t.datetime :start_time
      t.datetime :end_time
      t.references :event, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
