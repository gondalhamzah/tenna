# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180206165854) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_stat_statements"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "addresses", force: :cascade do |t|
    t.string   "line_1"
    t.string   "line_2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.integer  "addressable_id",   null: false
    t.string   "addressable_type", null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.float    "lat"
    t.float    "lng"
    t.string   "country"
  end

  add_index "addresses", ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",      default: 0
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "role",                   default: 0
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["invitation_token"], name: "index_admin_users_on_invitation_token", unique: true, using: :btree
  add_index "admin_users", ["invitations_count"], name: "index_admin_users_on_invitations_count", using: :btree
  add_index "admin_users", ["invited_by_id"], name: "index_admin_users_on_invited_by_id", using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "ahoy_events", force: :cascade do |t|
    t.integer  "visit_id"
    t.integer  "user_id"
    t.string   "name"
    t.jsonb    "properties"
    t.datetime "time"
  end

  add_index "ahoy_events", ["name", "time"], name: "index_ahoy_events_on_name_and_time", using: :btree
  add_index "ahoy_events", ["user_id", "name"], name: "index_ahoy_events_on_user_id_and_name", using: :btree
  add_index "ahoy_events", ["visit_id", "name"], name: "index_ahoy_events_on_visit_id_and_name", using: :btree

  create_table "analytic_filter_labels", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "company_id"
  end

  create_table "analytic_filters", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.text     "filter_json"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "name"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "analytic_filter_label_id"
    t.boolean  "is_recommended",           default: false
    t.integer  "duration"
  end

  add_index "analytic_filters", ["company_id"], name: "index_analytic_filters_on_company_id", using: :btree
  add_index "analytic_filters", ["user_id"], name: "index_analytic_filters_on_user_id", using: :btree

  create_table "asset_versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.text     "object_changes"
    t.datetime "created_at"
  end

  add_index "asset_versions", ["item_type", "item_id"], name: "index_asset_versions_on_item_type_and_item_id", using: :btree

  create_table "assets", force: :cascade do |t|
    t.string   "title",                                      null: false
    t.string   "description"
    t.integer  "company_id"
    t.integer  "project_id"
    t.integer  "creator_id",                                 null: false
    t.integer  "category_id",                                null: false
    t.string   "make"
    t.string   "model"
    t.integer  "price"
    t.date     "available_on"
    t.date     "expires_on",                                 null: false
    t.integer  "quantity",                                   null: false
    t.string   "units"
    t.boolean  "certified",                  default: false
    t.boolean  "rental",                     default: false, null: false
    t.boolean  "public",                     default: false, null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "condition"
    t.string   "status"
    t.string   "type",                                       null: false
    t.integer  "year"
    t.integer  "zip_code_id",                                null: false
    t.string   "asset_num",                  default: ""
    t.integer  "reallocation_count",         default: 0,     null: false
    t.integer  "view_count",                 default: 0,     null: false
    t.string   "slug"
    t.boolean  "flagged_as_inappropriate",   default: false
    t.string   "quantity_unit"
    t.float    "current_lat"
    t.float    "current_lng"
    t.string   "state"
    t.date     "in_use_until"
    t.integer  "market_value"
    t.string   "durability"
    t.string   "notes",                      default: ""
    t.integer  "groups_id"
    t.integer  "group_id"
    t.boolean  "owned",                      default: false
    t.integer  "time_length",                default: 1
    t.boolean  "maintenance",                default: false
    t.float    "accuracy"
    t.integer  "qr",                         default: 0
    t.integer  "rfid",                       default: 0
    t.integer  "lora",                       default: 0
    t.integer  "sigfox",                     default: 0
    t.integer  "upc",                        default: 0
    t.integer  "bt",                         default: 0
    t.integer  "cellular",                   default: 0
    t.integer  "hours"
    t.integer  "miles"
    t.boolean  "is_inventory",               default: false
    t.datetime "maintenance_date"
    t.integer  "miles_since"
    t.integer  "hours_since"
    t.boolean  "use_j1939_miles"
    t.boolean  "use_j1939_engine_hours"
    t.datetime "last_location_changed"
    t.integer  "current_driver_id"
    t.integer  "internal_daily_rental_rate"
    t.integer  "serialnumber",               default: 0
  end

  add_index "assets", ["category_id"], name: "index_assets_on_category_id", using: :btree
  add_index "assets", ["company_id"], name: "index_assets_on_company_id", using: :btree
  add_index "assets", ["creator_id"], name: "index_assets_on_creator_id", using: :btree
  add_index "assets", ["group_id"], name: "index_assets_on_group_id", using: :btree
  add_index "assets", ["groups_id"], name: "index_assets_on_groups_id", using: :btree
  add_index "assets", ["project_id"], name: "index_assets_on_project_id", using: :btree
  add_index "assets", ["slug"], name: "index_assets_on_slug", unique: true, using: :btree

  create_table "assets_trips", force: :cascade do |t|
    t.integer  "device_id"
    t.integer  "asset_id"
    t.string   "tracking_code"
    t.string   "device_name"
    t.float    "distance"
    t.float    "average_speed"
    t.float    "max_speed"
    t.float    "spent_fuel"
    t.float    "start_lat"
    t.float    "start_lon"
    t.float    "end_lat"
    t.float    "end_lon"
    t.datetime "start_time"
    t.datetime "end_time"
    t.string   "start_address"
    t.string   "end_address"
    t.integer  "duration"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "assets_versions", force: :cascade do |t|
    t.integer  "asset_id"
    t.string   "field_name"
    t.string   "from"
    t.string   "to"
    t.integer  "whodunnit"
    t.integer  "project_id"
    t.integer  "company_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "action",        default: ""
    t.integer  "group_id"
    t.integer  "wanted_id"
    t.jsonb    "tracking_info"
  end

  add_index "assets_versions", ["asset_id"], name: "index_assets_versions_on_asset_id", using: :btree
  add_index "assets_versions", ["company_id"], name: "index_assets_versions_on_company_id", using: :btree
  add_index "assets_versions", ["field_name"], name: "index_assets_versions_on_field_name", using: :btree
  add_index "assets_versions", ["group_id"], name: "index_assets_versions_on_group_id", using: :btree
  add_index "assets_versions", ["project_id"], name: "index_assets_versions_on_project_id", using: :btree
  add_index "assets_versions", ["whodunnit"], name: "index_assets_versions_on_whodunnit", using: :btree

  create_table "attachments", force: :cascade do |t|
    t.string   "type",                   null: false
    t.string   "file",                   null: false
    t.integer  "asset_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "position",   default: 0
  end

  add_index "attachments", ["asset_id"], name: "index_attachments_on_asset_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",              null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "category_type",     null: false
    t.integer  "category_group_id"
  end

  create_table "category_groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "company_id"
  end

  add_index "category_groups", ["company_id"], name: "index_category_groups_on_company_id", using: :btree

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "data_fingerprint"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name",                           null: false
    t.string   "region"
    t.string   "website"
    t.integer  "contact_id"
    t.integer  "admin_seats"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "active",         default: true,  null: false
    t.boolean  "admin_company",  default: false, null: false
    t.string   "company_logo"
    t.integer  "company_type"
    t.integer  "project_id"
    t.integer  "api_rpm",        default: 0
    t.string   "api_key"
    t.boolean  "api_key_status"
    t.integer  "qr_status"
    t.integer  "qr_print_type"
    t.integer  "qr_format"
  end

  add_index "companies", ["contact_id"], name: "index_companies_on_contact_id", using: :btree
  add_index "companies", ["project_id"], name: "index_companies_on_project_id", using: :btree

  create_table "companies_versions", force: :cascade do |t|
    t.string   "field_name"
    t.string   "from"
    t.string   "to"
    t.integer  "whodunnit"
    t.integer  "company_id"
    t.string   "action"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "companies_versions", ["company_id"], name: "index_companies_versions_on_company_id", using: :btree
  add_index "companies_versions", ["field_name"], name: "index_companies_versions_on_field_name", using: :btree
  add_index "companies_versions", ["whodunnit"], name: "index_companies_versions_on_whodunnit", using: :btree

  create_table "company_safety_configurations", force: :cascade do |t|
    t.integer  "gauge_id"
    t.integer  "company_id"
    t.integer  "score_type",                  default: 0
    t.decimal  "score_weight",                default: 100.0
    t.decimal  "high_points",                 default: 0.0
    t.decimal  "medium_points",               default: 0.0
    t.decimal  "low_points",                  default: 0.0
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.decimal  "high_points_no_violations"
    t.decimal  "medium_points_no_violations"
    t.decimal  "low_points_no_violations"
  end

  add_index "company_safety_configurations", ["company_id"], name: "index_company_safety_configurations_on_company_id", using: :btree
  add_index "company_safety_configurations", ["gauge_id"], name: "index_company_safety_configurations_on_gauge_id", using: :btree

  create_table "csv_imports", force: :cascade do |t|
    t.integer  "user_id",                null: false
    t.string   "file",                   null: false
    t.integer  "state",      default: 0, null: false
    t.integer  "job_type",   default: 0, null: false
    t.text     "message"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "project_id"
    t.integer  "group_id"
  end

  add_index "csv_imports", ["group_id"], name: "index_csv_imports_on_group_id", using: :btree
  add_index "csv_imports", ["project_id"], name: "index_csv_imports_on_project_id", using: :btree
  add_index "csv_imports", ["user_id"], name: "index_csv_imports_on_user_id", using: :btree

  create_table "driver_stops", force: :cascade do |t|
    t.integer  "route_recurring_id"
    t.integer  "path_id"
    t.datetime "done_timestamp"
    t.float    "done_lat"
    t.float    "done_lng"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "driver_stops", ["path_id"], name: "index_driver_stops_on_path_id", using: :btree
  add_index "driver_stops", ["route_recurring_id"], name: "index_driver_stops_on_route_recurring_id", using: :btree

  create_table "email_templates", force: :cascade do |t|
    t.string   "name"
    t.string   "subject"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "email_triggers", force: :cascade do |t|
    t.string   "trigger"
    t.integer  "email_template_id"
    t.boolean  "enabled"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "email_triggers", ["email_template_id"], name: "index_email_triggers_on_email_template_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "text"
    t.string   "rec_type"
    t.integer  "event_length"
    t.integer  "event_pid"
    t.integer  "route_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "event_type"
    t.integer  "asset_id"
    t.string   "user_ids"
    t.boolean  "deleted",      default: false
    t.boolean  "processed",    default: false
    t.string   "timezone"
    t.integer  "user_id"
  end

  add_index "events", ["asset_id"], name: "index_events_on_asset_id", using: :btree
  add_index "events", ["route_id"], name: "index_events_on_route_id", using: :btree
  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "gauge_alert_groups", force: :cascade do |t|
    t.integer  "gauge_id"
    t.integer  "category_group_id"
    t.integer  "category_id"
    t.integer  "asset_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "company_id"
  end

  add_index "gauge_alert_groups", ["asset_id"], name: "index_gauge_alert_groups_on_asset_id", using: :btree
  add_index "gauge_alert_groups", ["category_group_id"], name: "index_gauge_alert_groups_on_category_group_id", using: :btree
  add_index "gauge_alert_groups", ["category_id"], name: "index_gauge_alert_groups_on_category_id", using: :btree
  add_index "gauge_alert_groups", ["company_id"], name: "index_gauge_alert_groups_on_company_id", using: :btree
  add_index "gauge_alert_groups", ["gauge_id"], name: "index_gauge_alert_groups_on_gauge_id", using: :btree

  create_table "gauge_alert_logs", force: :cascade do |t|
    t.integer  "gauge_condition_id"
    t.boolean  "dismissed"
    t.integer  "asset_id"
    t.jsonb    "values"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "operator_id"
  end

  add_index "gauge_alert_logs", ["asset_id"], name: "index_gauge_alert_logs_on_asset_id", using: :btree
  add_index "gauge_alert_logs", ["gauge_condition_id"], name: "index_gauge_alert_logs_on_gauge_condition_id", using: :btree

  create_table "gauge_conditions", force: :cascade do |t|
    t.integer  "gauge_alert_group_id"
    t.jsonb    "values"
    t.boolean  "active"
    t.text     "notes"
    t.string   "severe_level"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.json     "contacts"
    t.string   "cond_type"
  end

  add_index "gauge_conditions", ["gauge_alert_group_id"], name: "index_gauge_conditions_on_gauge_alert_group_id", using: :btree

  create_table "gauges", force: :cascade do |t|
    t.string   "name"
    t.string   "icon"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "template"
    t.json     "meta"
    t.string   "type"
    t.boolean  "is_alarm"
    t.string   "graph_type"
    t.boolean  "can_request_service"
  end

  create_table "geo_fence_company_contacts", force: :cascade do |t|
    t.integer  "geo_fence_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.boolean  "email"
    t.boolean  "text"
  end

  add_index "geo_fence_company_contacts", ["geo_fence_id"], name: "index_geo_fence_company_contacts_on_geo_fence_id", using: :btree
  add_index "geo_fence_company_contacts", ["user_id"], name: "index_geo_fence_company_contacts_on_user_id", using: :btree

  create_table "geo_fence_other_contacts", force: :cascade do |t|
    t.integer  "geo_fence_id"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.boolean  "email_enabled"
    t.boolean  "phone_enabled"
    t.string   "name"
  end

  add_index "geo_fence_other_contacts", ["geo_fence_id"], name: "index_geo_fence_other_contacts_on_geo_fence_id", using: :btree

  create_table "geo_fences", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "shape"
    t.string   "name"
    t.string   "color"
    t.float    "radius"
    t.boolean  "alert_enter_site"
    t.boolean  "alert_exit_site"
    t.text     "notes"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "center"
    t.integer  "zoom_level"
  end

  add_index "geo_fences", ["company_id"], name: "index_geo_fences_on_company_id", using: :btree

  create_table "geo_points", force: :cascade do |t|
    t.integer  "geo_fence_id"
    t.float    "lat"
    t.float    "lng"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "geo_points", ["geo_fence_id"], name: "index_geo_points_on_geo_fence_id", using: :btree

  create_table "gf_alert_exclusions", force: :cascade do |t|
    t.integer  "geo_fence_id"
    t.integer  "category_group_id"
    t.integer  "category_id"
    t.integer  "asset_id"
    t.boolean  "exclude"
    t.boolean  "alert_enter_site"
    t.boolean  "alert_exit_site"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "gf_alert_exclusions", ["asset_id"], name: "index_gf_alert_exclusions_on_asset_id", using: :btree
  add_index "gf_alert_exclusions", ["category_group_id"], name: "index_gf_alert_exclusions_on_category_group_id", using: :btree
  add_index "gf_alert_exclusions", ["category_id"], name: "index_gf_alert_exclusions_on_category_id", using: :btree
  add_index "gf_alert_exclusions", ["geo_fence_id"], name: "index_gf_alert_exclusions_on_geo_fence_id", using: :btree

  create_table "gf_alert_logs", force: :cascade do |t|
    t.integer  "geo_fence_id"
    t.integer  "asset_id"
    t.boolean  "enter"
    t.boolean  "dismissed"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "gf_alert_logs", ["asset_id"], name: "index_gf_alert_logs_on_asset_id", using: :btree
  add_index "gf_alert_logs", ["geo_fence_id"], name: "index_gf_alert_logs_on_geo_fence_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.integer  "project_id"
    t.string   "name"
    t.integer  "company_id"
    t.string   "notes",       default: ""
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "group_num",   default: ""
    t.string   "client",      default: ""
    t.string   "description", default: ""
    t.datetime "deleted_at"
    t.string   "state",       default: "active"
  end

  add_index "groups", ["company_id"], name: "index_groups_on_company_id", using: :btree
  add_index "groups", ["project_id"], name: "index_groups_on_project_id", using: :btree

  create_table "groups_versions", force: :cascade do |t|
    t.integer  "group_id"
    t.string   "field_name"
    t.string   "from"
    t.string   "to"
    t.string   "action"
    t.integer  "user_id"
    t.integer  "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "groups_versions", ["company_id"], name: "index_groups_versions_on_company_id", using: :btree
  add_index "groups_versions", ["user_id"], name: "index_groups_versions_on_user_id", using: :btree

  create_table "hooks", force: :cascade do |t|
    t.string   "event_name"
    t.string   "subscription_url"
    t.string   "target_url"
    t.integer  "company_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "provider"
    t.integer  "user_id"
  end

  create_table "inventories", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "project_id"
    t.integer  "asset_count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "inventories", ["company_id"], name: "index_inventories_on_company_id", using: :btree
  add_index "inventories", ["project_id"], name: "index_inventories_on_project_id", using: :btree

  create_table "leads", force: :cascade do |t|
    t.string   "lead_type"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone_num"
    t.string   "industry"
    t.string   "reason_contact"
    t.string   "title"
    t.string   "company_name"
    t.string   "comment"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "list_items", force: :cascade do |t|
    t.integer  "list_id"
    t.integer  "status"
    t.integer  "asset_id"
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lists", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "list_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "filter"
  end

  create_table "marks", force: :cascade do |t|
    t.integer  "marker_id",     null: false
    t.string   "marker_type",   null: false
    t.integer  "markable_id",   null: false
    t.string   "markable_type", null: false
    t.string   "mark",          null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "marks", ["markable_id", "markable_type", "mark"], name: "index_marks_on_markable_id_and_markable_type_and_mark", using: :btree
  add_index "marks", ["marker_id", "marker_type", "mark"], name: "index_marks_on_marker_id_and_marker_type_and_mark", using: :btree

  create_table "news_items", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.date     "post_date"
    t.integer  "priority"
    t.boolean  "featured"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "slug"
    t.date     "deleted_at"
    t.boolean  "active"
    t.integer  "created_by"
    t.integer  "last_updated_by"
  end

  add_index "news_items", ["slug"], name: "index_news_items_on_slug", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "admin_user_id"
    t.text     "message"
    t.datetime "notification_datetime"
    t.integer  "days_before"
    t.integer  "duration"
    t.boolean  "is_active"
    t.string   "notification_type"
    t.string   "notification_medium"
    t.string   "location"
    t.text     "metadata"
    t.inet     "ip"
    t.integer  "client_type"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "user_type"
  end

  add_index "notifications", ["admin_user_id"], name: "index_notifications_on_admin_user_id", using: :btree

  create_table "paths", force: :cascade do |t|
    t.integer  "route_id"
    t.text     "address"
    t.float    "done_lat"
    t.float    "done_lng"
    t.datetime "done_timestamp"
    t.text     "service_notes"
    t.string   "waypoint_type"
    t.integer  "waypoint_id"
    t.integer  "sequence"
    t.integer  "time_on_site"
    t.integer  "priority"
    t.integer  "load"
    t.time     "begin_window"
    t.time     "end_window"
    t.boolean  "notify_customer", default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.float    "lat"
    t.float    "lng"
    t.datetime "deleted_at"
  end

  create_table "project_assignments", force: :cascade do |t|
    t.integer  "project_id", null: false
    t.integer  "user_id",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "role",       null: false
  end

  add_index "project_assignments", ["project_id"], name: "index_project_assignments_on_project_id", using: :btree
  add_index "project_assignments", ["user_id"], name: "index_project_assignments_on_user_id", using: :btree

  create_table "project_exports", force: :cascade do |t|
    t.string   "file"
    t.datetime "enqueued_at"
    t.datetime "exported_at"
    t.integer  "project_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "user_id"
    t.integer  "status",                 default: 0
    t.integer  "export_type",            default: 0
    t.text     "export_filtered_assets"
    t.boolean  "is_filtered_asset"
  end

  add_index "project_exports", ["project_id"], name: "index_project_exports_on_project_id", using: :btree
  add_index "project_exports", ["user_id"], name: "index_project_exports_on_user_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "name",               null: false
    t.string   "description"
    t.integer  "company_id"
    t.integer  "contact_id"
    t.string   "duration"
    t.date     "closeout_date",      null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "project_num"
    t.datetime "deleted_at"
    t.string   "contact_sms_number"
    t.string   "contact_email"
  end

  add_index "projects", ["company_id"], name: "index_projects_on_company_id", using: :btree
  add_index "projects", ["contact_id"], name: "index_projects_on_contact_id", using: :btree

  create_table "route_groups", force: :cascade do |t|
    t.string   "serialized_driver_metadata"
    t.string   "filters"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "route_recurrings", force: :cascade do |t|
    t.integer  "route_id"
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "fired_date"
  end

  add_index "route_recurrings", ["event_id"], name: "index_route_recurrings_on_event_id", using: :btree
  add_index "route_recurrings", ["route_id"], name: "index_route_recurrings_on_route_id", using: :btree

  create_table "routes", force: :cascade do |t|
    t.string   "url"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "slug"
    t.integer  "company_id"
    t.time     "start_time"
    t.time     "end_time"
    t.string   "name"
    t.integer  "route_group_id"
    t.integer  "creator_id"
    t.string   "timezone"
  end

  add_index "routes", ["route_group_id"], name: "index_routes_on_route_group_id", using: :btree
  add_index "routes", ["slug"], name: "index_routes_on_slug", using: :btree

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "company_id",         null: false
    t.string   "api_key"
    t.datetime "api_key_created_at"
    t.datetime "api_key_updated_at"
    t.string   "category",           null: false
    t.integer  "number_of_units"
    t.string   "state",              null: false
    t.text     "note"
    t.datetime "deleted_at"
    t.string   "vendor_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.boolean  "payment"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "tracking_codes", force: :cascade do |t|
    t.string   "token"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.integer  "status",        default: 0
    t.datetime "deleted_at"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "token_type",    default: 0
    t.integer  "company_id"
  end

  add_index "tracking_codes", ["resource_type", "resource_id"], name: "index_tracking_codes_on_resource_type_and_resource_id", using: :btree
  add_index "tracking_codes", ["token"], name: "index_tracking_codes_on_token", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "company_id"
    t.string   "company_name"
    t.string   "email",                              default: "",   null: false
    t.string   "encrypted_password",                 default: "",   null: false
    t.string   "phone_number"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",                  default: 0
    t.integer  "role",                                              null: false
    t.boolean  "active",                             default: true, null: false
    t.datetime "deleted_at"
    t.string   "title",                  limit: 255, default: ""
    t.string   "api_token"
    t.string   "udid"
    t.boolean  "subscription_opt",                   default: true
  end

  add_index "users", ["api_token"], name: "index_users_on_api_token", using: :btree
  add_index "users", ["company_id"], name: "index_users_on_company_id", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["udid"], name: "index_users_on_udid", using: :btree

  create_table "video_usages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "video_id"
    t.datetime "when_watched"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "videos", force: :cascade do |t|
    t.string   "url"
    t.string   "description"
    t.string   "page_name"
    t.integer  "relative_order"
    t.integer  "emphasis_indicator"
    t.boolean  "is_visible"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "visits", force: :cascade do |t|
    t.string   "visit_token"
    t.string   "visitor_token"
    t.string   "ip"
    t.text     "user_agent"
    t.text     "referrer"
    t.text     "landing_page"
    t.integer  "user_id"
    t.string   "referring_domain"
    t.string   "search_keyword"
    t.string   "browser"
    t.string   "os"
    t.string   "device_type"
    t.integer  "screen_height"
    t.integer  "screen_width"
    t.string   "country"
    t.string   "region"
    t.string   "city"
    t.string   "postal_code"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.string   "utm_source"
    t.string   "utm_medium"
    t.string   "utm_term"
    t.string   "utm_content"
    t.string   "utm_campaign"
    t.datetime "started_at"
  end

  add_index "visits", ["user_id"], name: "index_visits_on_user_id", using: :btree
  add_index "visits", ["visit_token"], name: "index_visits_on_visit_token", unique: true, using: :btree

  create_table "wanted_assets", force: :cascade do |t|
    t.string   "title",                            null: false
    t.string   "description"
    t.string   "make"
    t.string   "model"
    t.integer  "hours_miles"
    t.integer  "price"
    t.date     "needed_on"
    t.integer  "company_id",                       null: false
    t.integer  "creator_id",                       null: false
    t.integer  "category_id"
    t.integer  "zip_code_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "slug"
    t.date     "expires_on",                       null: false
    t.string   "type"
    t.integer  "project_id"
    t.integer  "quantity"
    t.string   "quantity_unit"
    t.string   "status",             default: "0"
    t.integer  "contact_id"
    t.integer  "reallocation_count", default: 0
    t.string   "state"
    t.string   "priority"
    t.datetime "fulfilled_date"
  end

  add_index "wanted_assets", ["category_id"], name: "index_wanted_assets_on_category_id", using: :btree
  add_index "wanted_assets", ["company_id"], name: "index_wanted_assets_on_company_id", using: :btree
  add_index "wanted_assets", ["contact_id"], name: "index_wanted_assets_on_contact_id", using: :btree
  add_index "wanted_assets", ["creator_id"], name: "index_wanted_assets_on_creator_id", using: :btree
  add_index "wanted_assets", ["slug"], name: "index_wanted_assets_on_slug", unique: true, using: :btree

  create_table "zip_codes", force: :cascade do |t|
    t.string   "zip_code",   null: false
    t.float    "lat"
    t.float    "lng"
    t.string   "city"
    t.string   "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "country"
  end

  add_foreign_key "analytic_filters", "companies"
  add_foreign_key "analytic_filters", "users"
  add_foreign_key "assets", "categories"
  add_foreign_key "assets", "companies"
  add_foreign_key "assets", "groups"
  add_foreign_key "assets", "projects"
  add_foreign_key "assets", "users", column: "creator_id"
  add_foreign_key "assets", "zip_codes"
  add_foreign_key "assets_versions", "groups"
  add_foreign_key "attachments", "assets"
  add_foreign_key "category_groups", "companies"
  add_foreign_key "companies", "projects"
  add_foreign_key "companies", "users", column: "contact_id", on_delete: :cascade
  add_foreign_key "company_safety_configurations", "companies"
  add_foreign_key "company_safety_configurations", "gauges"
  add_foreign_key "csv_imports", "groups"
  add_foreign_key "csv_imports", "projects"
  add_foreign_key "csv_imports", "users"
  add_foreign_key "driver_stops", "paths"
  add_foreign_key "driver_stops", "route_recurrings"
  add_foreign_key "email_triggers", "email_templates"
  add_foreign_key "events", "assets"
  add_foreign_key "events", "routes"
  add_foreign_key "events", "users"
  add_foreign_key "gauge_alert_groups", "assets"
  add_foreign_key "gauge_alert_groups", "categories"
  add_foreign_key "gauge_alert_groups", "category_groups"
  add_foreign_key "gauge_alert_groups", "companies"
  add_foreign_key "gauge_alert_groups", "gauges"
  add_foreign_key "gauge_alert_logs", "assets"
  add_foreign_key "gauge_alert_logs", "gauge_conditions"
  add_foreign_key "gauge_conditions", "gauge_alert_groups"
  add_foreign_key "geo_fence_company_contacts", "geo_fences"
  add_foreign_key "geo_fence_company_contacts", "users"
  add_foreign_key "geo_fence_other_contacts", "geo_fences"
  add_foreign_key "geo_fences", "companies"
  add_foreign_key "geo_points", "geo_fences"
  add_foreign_key "gf_alert_exclusions", "assets"
  add_foreign_key "gf_alert_exclusions", "categories"
  add_foreign_key "gf_alert_exclusions", "category_groups"
  add_foreign_key "gf_alert_exclusions", "geo_fences"
  add_foreign_key "gf_alert_logs", "assets"
  add_foreign_key "gf_alert_logs", "geo_fences"
  add_foreign_key "groups", "companies"
  add_foreign_key "groups", "projects"
  add_foreign_key "groups_versions", "companies"
  add_foreign_key "groups_versions", "users"
  add_foreign_key "notifications", "admin_users"
  add_foreign_key "project_assignments", "projects"
  add_foreign_key "project_assignments", "users"
  add_foreign_key "project_exports", "projects"
  add_foreign_key "project_exports", "users"
  add_foreign_key "projects", "companies"
  add_foreign_key "projects", "users", column: "contact_id"
  add_foreign_key "route_recurrings", "events"
  add_foreign_key "route_recurrings", "routes"
  add_foreign_key "routes", "route_groups"
  add_foreign_key "users", "companies", on_delete: :cascade
end
