Rails.application.routes.draw do
  resources :company_safety_configurations
  get 'safety_reports/landing_page'

  require 'sidekiq/web'

  mount Sidekiq::Web, at: '/sidekiq'
  mount ZapierRestHooks::Engine, at: '/hooks'
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config
  devise_for :users, controllers: {invitations: 'invitations', registrations: 'registrations', passwords: 'passwords'}
  ActiveAdmin.routes(self)
  constraints DomainConstraint.new('bsqr1.com') do
    get '/:token', to: redirect { |path_params, req|
      "http://#{ENV['HOST']}/tracking_codes/#{path_params[:token]}"
    }
  end

  resources :tracking_codes do
    member do
      get :assign
      get :unassign
      get :unauthorized
    end
  end

  post 'tracking_ids/add_token', to: 'tracking_codes#add_token'

  #  zapier requests
  get '/zapier/sample_project', to: 'zapier#sample_project'
  get '/zapier/sample_video_view', to: 'zapier#sample_video_view'
  get '/zapier/sample_asset_reallocation', to: 'zapier#sample_asset_reallocation'
  get '/zapier/avaiable_on', to: 'zapier#sample_avaiable_on'
  get '/zapier/sample_maintenance', to: 'zapier#sample_maintenance_data'
  get '/zapier/sample_asset', to: 'zapier#sample_asset'
  get '/zapier/sample_marked_rental_or_public', to: 'zapier#sample_marked_rental_or_public'
  get '/zapier/sample_asset_remove_mp', to: 'zapier#sample_asset_remove_mp'
  get '/zapier/sample_route_stop', to: 'zapier#sample_route_stop'

  post '/zapier/create', to: 'zapier#create'
  post '/zapier/delete', to: 'zapier#destroy'

  get '/zapier/show', to: 'zapier#create_user'
  get '/lead/create', to: 'leads#create'
  post '/zapier/show', to: 'zapier#create_user'

  get '/zapier/update_hour_millage_asset', to: 'zapier#update_hour_millage_asset'
  post '/zapier/update_hour_millage_asset', to: 'zapier#update_hour_millage_asset'
  get '/zapier/update_lat_lng', to: 'zapier#update_lat_lng'
  post '/zapier/update_lat_lng', to: 'zapier#update_lat_lng'

  get 'assets/tokens', to: 'tracking_codes#get_tokens'
  post 'token/remove', to: 'tracking_codes#delete_token'
  post 'videos/add_video_usage', to: 'video_usages#add_video_usage'

  # Companies can only be created & deleted via /admin.
  resources :companies, only: [:show, :edit, :update] do
    resources :users, controller: 'company_assignments' do
      post :reinvite, to: 'company_assignments#reinvite_user'
      resources :project_assignments, controller: 'user_project_assignments', only: [:new, :create]
    end
  end

  get '/companies/:id/print_qr', to: 'companies#print_qr', as: 'print_company_qr'
  post '/companies/:id/qr_code_status', to: 'companies#qr_code_status', as: 'qr_code_status'
  post '/companies/:id/generate_printed_qr_codes', to: 'companies#generate_printed_qr_codes', as: 'generate_company_qr_codes'
  get '/companies/:id/download_printed_qr_codes', to: 'companies#download_printed_qr_codes', as: 'download_company_qr_codes'
  post '/companies/:id/email_printed_qr_codes', to: 'companies#email_printed_qr_codes', as: 'email_company_qr_codes'

  resources :projects, path: 'sites' do
    resources :assignments, controller: 'project_assignments', except: [:show, :edit]
    resources :csv_imports, controller: 'project_csv_imports', only: [:show, :create] do
      # No longer needed, as template is static; not generated dynamically anymore.
      # get :template, on: :collection
    end

    collection do
      post :upload_multiple
      get :search_by_name
    end
  end

  get '/assets/findit', to: 'assets#findit', :path => 'findit/inventory'
  get '/findit/checklist', to: 'assets#checklist'
  # get '/assets/get_assets_by_title', to: 'assets#get_assets_by_title'

  resources :assets do
    collection do
      put :update_multiple
      get :sold
      get :history_download
      get :search_by_title
    end

    member do
      patch :flag
      get :qr_scanning
      get :manage_inventory
      get :inventory_assets
      get :update_quantity
    end

    resources :marks, only: [:create, :destroy]
  end

  resources :groups do
    resources :csv_imports, controller: 'group_csv_imports', only: [:show, :create] do
    end
    collection do
      put :update_multiple
      post :upload_multiple
    end

    member do
      get :mark_as_closed
    end
  end
  resources :exports do
    collection do
      get :export_project
      get :export_assets
    end
  end

  resources :live_data, only: [:index] do
    collection do
      get :get_mqtt_token
      post :send_notification
      get '*path', action: :index
    end
  end

  resources :geo_fences do
  end

  resources :gauge_alerts do
    collection do
      get :search_assets_by_category
      get :category_assets
      get :asset_detail
      post :reset
    end
  end

  resources :exports, only: [:index, :show, :create]
  resources :news_items, only: [:index, :show], path: :newsroom

  get '/zip_validator' => 'maps#zip_validator', format: :json
  get '/zip_with_country_validator' => 'maps#zip_with_country_validator', format: :json
  get '/maps' => 'maps#index', :format => 'json'

  resources :wanted_assets, path: 'wanted', except: [:show] do
    collection do
      put :update_multiple
    end
  end

  post '/save_inventory', to: 'lists#save_inventory' , as: 'save_inventory'
  post '/save_checklist', to: 'lists#save_checklist' , as: 'save_checklist'
  get '/lists', to: 'lists#index' , as: 'lists'

  post '/get_asset', to: 'assets#asset'
  post 'reports/assets/generate', to: 'reports#generate_project_assets_report', as: 'reports_generate_project_assets_report'
  post 'reports/asset_location_audit/generate', to: 'reports#generate_asset_location_audit_report', as: 'reports_generate_asset_location_audit_report'
  get 'reports/:project_id/project_assets', to: 'reports#project_assets_report' , as: 'reports_project_assets_report'
  get 'reports', to: 'reports#index' , as: 'reports'
  #get 'csv_analytics', to: 'asset_reports#csv_analytics' , as: 'csv_analytics'

  get 'reports/assets', to: 'asset_reports#index', as: 'asset_reports'
  post 'reports/assets/filters', to: 'asset_reports#filter_post', as: 'asset_report_filter_post'
  get 'reports/assets/delete_myfilter/:id', to: 'asset_reports#delete_myfilter', as: 'asset_report_filter_delete'
  patch 'reports/assets/filters/:id', to: 'asset_reports#filter_patch', as: 'asset_report_filter_patch'
  get 'reports/assets/filters/:id', to: 'asset_reports#filter_get', as: 'asset_report_filter_get'
  get 'reports/assets/pre_filters/:key', to: 'asset_reports#pre_filter_get', as: 'asset_report_pre_filter_get'
  get 'reports/assets/download', to: 'asset_reports#download', as: 'asset_reports_download'
  patch 'reports/assets/filter_labels/:id', to: 'asset_reports#filter_label_patch', as: 'asset_report_filter_label_patch'
  post 'reports/assets/filter_labels', to: 'asset_reports#filter_label_post', as: 'asset_report_filter_label_post'

  get '/assets/:id/log_in', to: 'assets#log_in'
  get '/assets_versions/create'
  get '/user/profile' => 'profile#index'
  get '/dashboard' => 'dashboard#index'
  get '/marketplace' => 'marketplace#index'
  get '/favorites' => 'favorites#index'

  post '/update_cookies' => 'pages#update_cookies'

  get '/contact_us' => 'pages#contact_us'
  get '/terms' => 'pages#terms'
  get '/infringement_notice' => 'pages#infringement_notice'
  get '/privacy_policy' => 'pages#privacy_policy'
  get '/learn_more' => 'pages#learn_more'
  get '/market_place' => 'pages#market_place'
  get '/value' => 'pages#value'
  get '/industries_served' => 'pages#industries_served'
  get '/support' => 'pages#support'
  get '/learn_more' => 'pages#learn_more'
  get '/about_us' => 'pages#about_us'
  get '/reverse_geocode_map' => 'projects#reverse_geocode_map'

  post '/learn_more_mail' => 'pages#learn_more_email'
  post '/leads_mailer_mail' => 'pages#leads_mailer_email'

  # root 'pages#index'
  if Rails.env.development? || Rails.env.test?
    root to: 'pages#index'
  elsif Rails.env.staging? || Rails.env.production?
    root to: 'dashboard#index'
  end

  get '/robots', to: redirect('/robots.txt'), format: false
  get '/robots.:format' => 'robots#index'
  get '/resend_invitation' => 'companies#resend_invitation'
  resources :geo_positions, only: [:index]
  resources :clusters, only: [:index]
  resources :analytics, only: [:create] do
    collection do
      post :assets_filters
      post :asset_daily_filter
      post :more_most_recent
      post :asset_hour_filter
      post :asset_most_recent_filter
      get :search_groups_by_name
      get :search_users_by_name
      get :get_categories
      get :get_subcategories_by_category_id
      get :get_token_types
      get :get_analytic_filter_labels
    end
  end
  resources :map, only: [:index]

  resources :path_finder, only: [:index, :show, :create, :update] do
    get :show_multi, on: :collection
    post :route_history_search, on: :collection
    get :manage_routes, on: :collection
    # get :preview, on: :member
  end

  get '/route/:id/:fired_date', to: 'path_finder#show', as: 'route'
  get '/route_multi/:ids', to: 'path_finder#show_multi'
  get '/routes/search_by_name', to: 'path_finder#search_by_name'
  post '/path_finder/mark_as_done', to: 'path_finder#mark_as_done'
  post '/path_finder/mark_as_undone', to: 'path_finder#mark_as_undone'
  post '/path_finder/mark_all_as_undone', to: 'path_finder#mark_all_as_undone'
  post '/path_finder/send_route', to: 'path_finder#send_route'
  post '/path_finder/waypoint_options', to: 'path_finder#waypoint_options'
  post '/path_finder/project_assets/:id', to: 'path_finder#project_assets'
  post '/path_finder/:route_slug/:fired_date/preview', to: 'path_finder#preview', as: 'preview'
  get '/path_finder/:id/:fired_date/edit', to: 'path_finder#edit', as: 'path_finder_edit'
  get '/events', to: 'events#index', as: 'event_index'
  get '/events/db_action', to: 'events#db_action', as: 'event_action'
  get '/events/asset/:asset_id', to: 'events#index', as: 'event_asset'
  get '/events/:event_id', to: 'events#index', as: 'event'
  get '/users/search_by_name', to: 'users#search_by_name'
  get '/move_inventory', to: 'assets#move_inventory'
  get '/safety_reports/get_scorecard', to: 'safety_reports#get_scorecard'
  get '/safety_reports/not_subscribed', to: 'safety_reports#not_subscribed'


end
