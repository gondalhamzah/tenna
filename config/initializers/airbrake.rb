Airbrake.configure do |config|
  config.environment = Rails.env
  config.ignore_environments = %w(development test ahoy)
  config.project_id = ENV['AIRBRAKE_PROJECT_ID']
  config.project_key = ENV['AIRBRAKE_PROJECT_KEY']
end
