CarrierWave.configure do |config|
  if Rails.env.development? || Rails.env.test?
    config.storage = :file
  else
    config.fog_credentials = {
      provider: 'AWS',
      aws_access_key_id: ENV['AWS_ACCESS_KEY_ID'],
      aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
      region: ENV['AWS_REGION']
    }

    config.fog_directory = ENV['S3_BUCKET']
    config.fog_attributes = {'Cache-Control': 'max-age=#{365.day.to_i}'}

    # Default is /tmp, but there is no session affinity on Heroku, so we must cache on AWS.
    #config.cache_storage = :fog
    config.cache_dir = 'tmp/uploads'

    config.storage = :fog
  end

  # if Rails.env.test?
  #   config.cache_only = true
  # end

end
