require 'aws-sdk'
require 'aws-sdk-resources'

unless Rails.env.development? || Rails.env.test?
  s3_bucket = ENV['S3_BUCKET']
  credentials = Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'])
  region = ENV['AWS_REGION']
  s3 = Aws::S3::Resource.new(region: region, credentials: credentials)
  S3_OBJECT = s3.bucket(s3_bucket)
end
