require 'simple_form/components/labels'

# Force labels to render w/ active class as appropriate. Required for Materialize.
# Example, date picker labels appear on top of date input unless active class is set.

module SimpleForm
  module Components
    module Labels
      def label(wrapper_options = nil)
        label_options = merge_wrapper_options(label_html_options, wrapper_options)

        if object.respond_to?(attribute_name) && ([:material_date, :string, :select, :grouped_select].include? input_type)
          label_options[:class].push(:active)
        end

        if generate_label_for_attribute?
          @builder.label(label_target, label_text, label_options)
        else
          template.label_tag(nil, label_text, label_options)
        end
      end
    end
  end

end
