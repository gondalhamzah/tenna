# This class implements a cache with simple delegation to the the Dalli Memcached client
# https://github.com/mperham/dalli
#
# A TTL is set on initialization

class AutoexpireCacheDalli
  def initialize(store, ttl = 86400)
    @store = store
    @keys = 'GeocoderDalliClientKeys'
    @ttl = ttl
  end

  def [](url)
    res = @store.get(url)
    res = YAML::load(res) if res.present?
    res
  end

  def []=(url, value)
    if value.nil?
      del(url)
    else
      key_cache_add(url) if @store.add(url, YAML::dump(value), @ttl)
    end
    value
  end

  def keys
    key_cache
  end

  def del(url)
    key_cache_delete(url) if @store.delete(url)
  end

  private

  def key_cache
    the_keys = @store.get(@keys)
    if the_keys.nil?
      @store.add(@keys, YAML::dump([]))
      []
    else
      YAML::load(the_keys)
    end
  end

  def key_cache_add(key)
    @store.replace(@keys, YAML::dump(key_cache << key))
  end

  def key_cache_delete(key)
    tmp = key_cache
    tmp.delete(key)
    @store.replace(@keys, YAML::dump(tmp))
  end
end


if Rails.env.development? || Rails.env.test?
  Geocoder.configure(
    lookup: :google,
    cache: Rails.cache,
    cache_prefix: 'geocoder:'
  )
else
  Geocoder.configure(
    lookup: :google_premier,
    api_key: [ ENV['GOOGLE_CRYPTO_KEY'], ENV['GOOGLE_CLIENT_ID'], ENV['GOOGLE_CHANNEL'] ],
    http_proxy: ENV['QUOTAGUARD_URL'],
    timeout: 5,
    # Cache geocoder results to prevent repeat requests for the same address / zip code
    cache: AutoexpireCacheDalli.new(
      Dalli::Client.new(
        (ENV["MEMCACHIER_SERVERS"] || "").split(","), {
          username: ENV["MEMCACHIER_USERNAME"],
          password: ENV["MEMCACHIER_PASSWORD"],
          failover: true,
          compress: true,
          socket_timeout: 1.5,
          socket_failure_delay: 0.2,
          down_retry_delay:  60,
          pool_size: 16,
          value_max_bytes: 2000000
        }
      )
    ),
    cache_prefix: 'geocoder:'
  )
end
