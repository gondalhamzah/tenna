# Accepted postal code formats for selected countries

US_ZIP_CODE_REGEX = /^\d{5}(-\d{4})?$/  # US - example: '08817' or '08817-3334'
CANADA_ZIP_CODE_REGEX = /^([A-Z][0-9][A-Z])[\s]([0-9][A-Z][0-9])$/  # Canada - example: 'H4S 2A5'
