# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.

Rails.application.config.assets.precompile += %w( dhtmlx/main.js dhtmlx/main.css )
Rails.application.config.assets.precompile += %w( ink.css homepage.css homepage.js mailer/mailer.css )
Rails.application.config.assets.precompile += %w( ckeditor/* )
Rails.application.config.assets.precompile += %w( dhtmlxscheduler.css)
Rails.application.config.assets.precompile += %w( application_test.css )
Rails.application.config.assets.precompile += %w( react-select.min.css )
Rails.application.config.assets.precompile += %w( dhtmlxscheduler.js ext/dhtmlxscheduler_recurring.js ext/dhtmlxscheduler_editors.js ext/dhtmlxscheduler_multiselect.js ext/dhtmlxscheduler_quick_info.js)
# Rails asset pipeline prefix is /assets by default; however, we have an assets model, so we must change this default.
Rails.application.config.assets.prefix = '/static-assets'
