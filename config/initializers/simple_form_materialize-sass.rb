# Use this setup block to configure all options available in SimpleForm.
SimpleForm.setup do |config|
  config.button_class = 'waves-effect waves-light btn'
  config.boolean_label_class = nil

  config.wrappers :vertical_form, tag: 'div', class: 'input-field', error_class: 'has-error' do |b|
    b.use :html5
    b.use :placeholder
    b.optional :maxlength
    b.optional :pattern
    b.optional :min_max
    b.optional :readonly
    b.use :input
    b.use :label
    b.use :error, wrap_with: {tag: 'div', class: 'error-block'}
    b.use :hint, wrap_with: {tag: 'div', class: 'help-block'}
  end

  config.default_wrapper = :vertical_form

end
