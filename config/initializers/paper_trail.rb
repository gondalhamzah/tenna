# Limit: 51 versions per asset (50 most recent, plus a 'create' event)
PaperTrail.config.version_limit = 50
PaperTrail.config.track_associations = false

# Remove paper_trail limit
# PaperTrail.config.version_limit = nil
