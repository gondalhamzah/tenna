require 'sidekiq'
require 'sidekiq/web'

Sidekiq::Web.use ActionDispatch::Session::CookieStore, key: '_Side_kiq_session'
Sidekiq::Web.use Rack::Auth::Basic do |username, password|
  username == ENV['SIDEKIQ_USER'] && password == ENV['SIDEKIQ_PASS']
end

if Rails.env.development?
  Sidekiq.configure_client do |config|
    config.redis = { size: 1 }
  end

  Sidekiq.configure_server do |config|
    config.redis = { size: (Sidekiq.options[:concurrency] + 2) }
  end
else
  Sidekiq.configure_client do |config|
    config.redis = { size: 5 }
  end

  Sidekiq.configure_server do |config|
    config.redis = { size: (Sidekiq.options[:concurrency] * 4 + 2) }
  end
end

