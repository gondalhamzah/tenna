$(function () {
  if (isController('projects')) {
    if (isAction(['new', 'create', 'edit', 'update'])) {

      $lat = $('#project_address_attributes_lat');
      $lng = $('#project_address_attributes_lng');
      $addr_l1 = "project_address_attributes_line_1";
      $addr_city = "project_address_attributes_city";
      $addr_state = "project_address_attributes_state";
      $addr_zip = "project_address_attributes_zip";
      $addr_country = "project_address_attributes_country";
      $addr_change = "project_address_attributes_change_addr";
      $next_step = ".next-step";
      var count = 0;
      $('#'+ $addr_city + ', #' + $addr_l1 + ', #' + $addr_state + ', #' + $addr_zip + ', #' + $addr_country).change(function(){
        $('#' + $addr_change).val("change");
        $('label[for="'+$addr_change+'"]').addClass('active');
      });
      if($('#project_name').val().length == 0){
        $($next_step).addClass('next-disable');
      }
      else{
        $($next_step).removeClass('next-disable');
      }

      $('#project_name').keyup(function(){
        if($(this).val().length > 0){
          $($next_step).removeClass('next-disable');
        }
        else{
          $($next_step).addClass('next-disable');
        }
      });

      var change;
      var handler = Gmaps.build('Google');
      handler.buildMap({ map_options: { libraries: ["places"]},internal: {id: 'geolocation'} }, function(){
        if(navigator.geolocation)
          navigator.geolocation.getCurrentPosition(displayOnMap);
      });
      if(isAction(['edit','update'])){
        $('main').css('margin-top','61px');
      }

      function displayOnMap(position){
        var lat;
        var lng;
        if(position.coords){
          lat = position.coords.latitude;
          lng = position.coords.longitude;
        }
        else if(position.location){
          lat = position.location.lat();
          lng = position.location.lng();
        }
        if(isAction(['edit','update']) && change != "affect"){
          lat = parseFloat($lat.val());
          lng = parseFloat($lng.val());
          change = "affect";
        }
        $lat.val(lat);
        $lng.val(lng);
        // if(isAction(['edit']) && count == 1){
        //   rev_geocode(lat,lng);
        // }
        // else if(isAction(['new'])){
        //   rev_geocode(lat,lng);
        // }
         Gmaps.markers = handler.addMarkers(
            [
              {
                "lat": lat,
                "lng": lng,
              }
            ]
          ,{ draggable: true}
          );
          handler.map.centerOn(Gmaps.markers[0]);
          handler.fitMapToBounds();
          handler.getMap().setZoom(16);
          // Move existing marker
          google.maps.event.addListener(Gmaps.markers[0].serviceObject, 'dragend', function() {
            updateFormLocation(this.getPosition());
          });
          count = 1;
      };


      function initialize() {
        var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
          GeoCodeAddress();
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
      function GeoCodeAddress(){
          var geocoder =  new google.maps.Geocoder();
          geocoder.geocode( { 'address': $('#searchTextField').val()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              Gmaps.markers[0].serviceObject.setVisible(false);
              displayOnMap(results[0].geometry);
            }
            else{
              alert('invalid adddress')
            }
          });
      }

      function rev_geocode(lat,lng){
        $.ajax({
          method: 'get',
          url: '/reverse_geocode_map',
          data: {lat: lat, lng: lng},
          success: function(result){
            $("#" + $addr_l1).val(result.data.street);
            $('label[for="'+$addr_l1+'"]').addClass('active');
            $("#" + $addr_city).val(result.data.city);
            $('label[for="'+$addr_city+'"]').addClass('active');
            $('#' + $addr_state).val(result.data.state);
            $('label[for="'+$addr_state+'"]').addClass('active');
            $('#' + $addr_zip).val(result.data.zip);
            $('label[for="'+$addr_zip+'"]').addClass('active');
            $('#' + $addr_country).val(result.data.country)
          }
        });
      }

      function updateFormLocation(latLng) {
        // rev_geocode(latLng.lat(),latLng.lng())
        $lat.val(latLng.lat());
        $lng.val(latLng.lng());
        $('#' + $addr_change).val("");
      }

      $('#maping-tab').click(function(){
        // var currCenter = handler.map.getCenter();
        setTimeout(function(){
          google.maps.event.trigger(handler.getMap(), 'resize');
          // handler.map.centerOn(Gmaps.markers[0]);
          // handler.map.setCenter(currCenter);
        }, 50);

        $('.map_btn').removeClass("hide");
        $('.form_btn').addClass("hide");
      });

      $('#form-tab').click(function(){
        $('.map_btn').addClass("hide");
        $('.form_btn').removeClass("hide");
      });

      $('#searchTextField').keypress(function(event) {
          if (event.keyCode == 13) {
              event.preventDefault();
          }
      });
      $('.site-address-tab').click(function(){
        if ( $('#address').is(":visible") ){
          $('#site-header-new').text("Site Address");
        }
      });
      $('.site-detail-step-1').click(function(){
        $('#site-header-new').text("Site Details");
      });

      // $('#project_address_attributes_country').attr("data-trigger" ,"change");
      // $('#project_address_attributes_country').attr("data-parsley-remote-validator" ,"countrycode");
      // $('#project_address_attributes_country').attr("data-parsley-remote" , "");
      // $('#project_address_attributes_country').attr("data-parsley-required" ,"true");
      // $('#project_address_attributes_country').attr("data-parsley-required" ,"true");
      // $('#project_address_attributes_country').attr('data-parsley-error-message','Select a valid country or valid zip / postal code');
      $('#new_project').parsley(
        {
          trigger: "focusout",
          errorsWrapper: "<div class=\"error-block\"></div>",
          errorTemplate: "<span></span>"
        }
      );
      $('#next-step').click(function(){
        $('#address').trigger('click');
      });
      $('#prev-step').click(function(){
        $('#detail').trigger('click');
      });

      $('[id^=edit_project]').parsley(
        {
          trigger: "focusout",
          errorsWrapper: "<div class=\"error-block\"></div>",
          errorTemplate: "<span></span>"
        }
      );
    }

    $(".clear_location_btn").on('click', function(e){
       $('input[name="project[address_attributes][line_1]"]').val("");
       $('input[name="project[address_attributes][line_2]"]').val("");
       $('input[name="project[address_attributes][city]"]').val("");
       $('input[name="project[address_attributes][state]"]').val("");
       $('input[name="project[address_attributes][zip]"]').val("");
       $('select[name="project[address_attributes][country]"]').val("");
    });
    $(".clear_map_btn").on('click', function(e){
       $('#searchTextField').val("");
    });
 
  }
  if (isController('projects')) {
    if (isAction(['index'])) {
      $('.project-popup').on('click', function (e) {
        e.preventDefault();
        var data_project_id = $(this).attr("data_project_id");
        var project_id = data_project_id.split("_")[0];
        $('#project-closed-'+project_id).modal('open');
        // '$('.modal').modal('open');
        var project_id = $(this).attr("data_project_id");
        var has_asset = project_id.split("_");
        $('a[name=change-project-date]').attr('href','/sites/'+has_asset[0]+'/edit?back=/sites/'+has_asset[0]);
        if(has_asset[1] == 'assets') {
          $('.has-assets').css('display', 'block');
          $('a[name=reallocate-active-project]').attr('href','/sites/'+has_asset[0]);
        } else {
          $('.has-assets').css('display', 'none');
        }
      });
      $("#bulk-project-upload-modal-open").click(function(){
        $("#bulk-project-upload-modal").modal('open');
      });
    }
  }

  if (isController('projects')) {
    if (isAction(['show'])) {
      $('#modal-upload-spreadsheet-open').click(function() {
        $('#modal-upload-spreadsheet').modal('open');
      });
    }
  }
});
