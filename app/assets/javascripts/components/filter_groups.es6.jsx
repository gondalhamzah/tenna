import FilterGroup from './filter_group.es6.jsx';
import FilterReport from './filter_report.es6.jsx';
import FilteredAssets from './filtered_assets.es6.jsx';
import FilterRuleDivider from './filter_rule_divider.es6.jsx';
import Pagination from "react-js-pagination";

export default class FilterGroups extends React.Component {
  constructor(props) {
    super(props);
    this.$filterEl = $('#asset_filter_name');
    this.state = {
      filter_report_state: JSON.parse(props.filterObject.filter_json || "{}"),
      filter_name: props.filterObject ? props.filterObject.name : null,
      filterLabel: props.filterLabel || null,
      filter_result: {},
      start_date: this.props.startDate,
      end_date: this.props.endDate,
      duration: this.props.duration,
      offset: 1,
      activePage: 1,
      perPage: 10,
      filter_json: {}
    };

    this.filterNameChanged = this.filterNameChanged.bind(this);
    this.onFilterChanged = this.onFilterChanged.bind(this);
    this.saveOrUpdateFilter = this.saveOrUpdateFilter.bind(this);
  }

  componentDidMount() {
    this.$filterEl.on('change', this.filterNameChanged);
  }

  componentWillUnmount() {
    this.$filterEl.off('change', this.filterNameChanged);
  }

  filterNameChanged(ev) {
    this.setState({name: this.$filterEl.val()});
  }

  exportCSV(ev) {
    const {csvData} = this.props;
  }

  downloadCSV(ev) {
    let csvContent = "data:text/csv;charset=utf-8,";
    let regex = /[~`!#$%\^&*+=\-\[\]\\';.,/{}|\\":<>?null]/gi;
    csvContent += (this.props.assetAnalytics ? '' : 'Category, Sub Category, Asset Name, Date, Action, New, Old, User\n');
    this.props.csvData.forEach((rowArray) => {
      let newRow = [];
      rowArray.forEach((phrase) => {
        if (phrase != null && regex.test(phrase) == true) {
          newRow.push(JSON.stringify(phrase));
        } else {
          newRow.push(phrase);
        }
      });
      csvContent += newRow.join(",");
      csvContent += "\n";
    });
    const encodedUri = encodeURI(csvContent);
    const link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "AssetReport.csv");
    document.body.appendChild(link); // Required for FF
    link.click(); // This will download the data file named "AssetReport.csv".
  }

  saveOrUpdateFilter(typeStr, label_name, start_date, end_date, duration) {
    if (typeStr != 'POST' && typeStr != 'PATCH') {
      return;
    }
    const that = this;
    let url = '/reports/assets/filters';
    if (typeStr == 'PATCH') {
      url = `/reports/assets/filters/${this.state.filter.id}.json`
    }

    $.ajax({
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      url: url,
      type: typeStr,
      data: JSON.stringify({
        filter_json: JSON.stringify(this.state.filter_report_state),
        name: ($('#asset_filter_name').val() == "") ? null : $('#asset_filter_name').val(),
        label_name: label_name,
        start_date: start_date,
        end_date: end_date,
        duration: duration
      }),
      success: function (res, textStatus, jqXhr) {
        if (!res.success) {
          let errors = res.errors;
          let errorString = '';
          if(errors.name && errors.analytic_filter_label_id){
            errorString = errors.name[0] + ' And ' + errors.analytic_filter_label_id[0];
          }else if(errors.name && !errors.analytic_filter_label_id){
            errorString = errors.name[0];
          }else if(!errors.name && errors.analytic_filter_label_id){
            errorString = errors.analytic_filter_label_id[0];
          }else{
            errorString = 'Something went wrong... Please retry!';
          }
          swal({
            type: 'error',
            title: 'Oops...',
            text: errorString,
          })
        } else {
          // TODO: that.setState({filter_report_state: res.filter});
          swal({
            type: 'success',
            title: 'Success!',
            text: 'Changes Saved Successfully!',
          })
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(textStatus);
        swal({
          type: 'error',
          title: 'Oops...',
          text: 'Something went wrong... Please retry!',
        })
      }
    });
  }

  onFilterChanged(state, pageNumber) {
    let filter_json = {};
    if(state != this.state.filter_report_state){
      this.setState({
        filter_report_state: state,
        offset: 1,
        activePage: 1
      });

      // Convert state to required format
      let asset_filters = {};
      if (state['sitesSelected'].length > 0) {
        asset_filters['sites'] = state['sitesSelected'].map(function (s) {
          return s.id
        });
      }
      if (state['sitesWithinMiles'] && typeof(state['sitesWithinMiles']) == 'object' && state['sitesWithinMiles']['value'] != null) {
        asset_filters['within_miles'] = state['sitesWithinMiles']['value'];
      }
      if (state['categoriesSelected'].length > 0) {
        asset_filters['category_groups'] = state['categoriesSelected'].map(function (c) {
          return c.id
        });
      }
      if (Object.keys(state['subcategoriesSelected']).length > 0) {
        asset_filters['categories'] = Object.values(state['subcategoriesSelected'])[0].map(function (sc) {
          return sc.id
        });
      }
      if (state['groupsSelected'].length > 0) {
        asset_filters['groups'] = state['groupsSelected'].map(function (g) {
          return g.id
        });
      }
      if (state['assetNamesEntered'].length > 0) {
        asset_filters['asset_title'] = state['assetNamesEntered'].map(function (a) {
          return a.value
        });
      }
      if (state['trackingCodesChecked'].length > 0) {
        asset_filters['tracking_codes'] = state['trackingCodesChecked'];
      }
      if (state['usersSelected'].length > 0) {
        asset_filters['users'] = state['usersSelected'].map(function (u) {
          return u.id
        });
      }
      if (state['driversSelected'].length > 0) {
        asset_filters['drivers'] = state['driversSelected'].map(function (d) {
          return d.id
        });
      }
      if (state['assetManagementTypesChecked'].length > 0) {
        asset_filters['asset_management'] = state['assetManagementTypesChecked'];
      }
      if (state['locationsEntered'].length > 0) {
        asset_filters['locations'] = state['locationsEntered'].map(function (l) {
          return l.value
        });
      }

      let event_filters = {};
      if (state['alarmsSelected'].length > 0) {
        event_filters['alarms'] = state['alarmsSelected'].map(function (a) {
          return a.label
        });
      }
      if (state['maintenanceSelected'] && typeof(state['maintenanceSelected']) == 'object' && state['maintenanceSelected']['label'].length > 0) {
        event_filters['maintenance'] = state['maintenanceSelected'].label;
      }
      if (state['notesFilterSelected']['label'].length > 0) {
        event_filters['notes_filter'] = state['notesFilterSelected']['label'];
        if (state['notesFilterSelected']['label'] != 'All' && state['notesTextEntered'].length > 0) {
          event_filters['notes_filter_by'] = state['notesTextEntered'];
        }
      }
      if (state['reallocationsFilterSelected']['label'].length > 0) {
        event_filters['reallocations_filter'] = state['reallocationsFilterSelected']['label'];
        if (state['reallocationsFilterSelected']['label'] != 'All' && state['reallocationsTextEntered'].length > 0) {
          event_filters['reallocations_filter_by'] = state['reallocationsTextEntered'];
        }
      }
      if (state['gateScansFilterSelected']['label'].length > 0) {
        event_filters['gate_scans_filter'] = state['gateScansFilterSelected']['label'];
        if (state['gateScansFilterSelected']['label'] != 'All' && state['gateScansTextEntered'].length > 0) {
          event_filters['gate_scans_filter_by'] = state['gateScansTextEntered'];
        }
      }
      if (Object.keys(state['readingsFilters']).length > 0) {
        event_filters['readings'] = state['readingsFilters'];
      }
      if (Object.keys(state['valueFilters']).length > 0) {
        event_filters['value'] = state['valueFilters'];
      }

      filter_json['asset_filters'] = asset_filters;
      filter_json['event_filters'] = event_filters;
      this.setState({filter_json: filter_json});
    }else{
      filter_json = this.state.filter_json;
    }

    // Make ajax call
    const that = this;
    let url = '/analytics/assets_filters';
    var self = this;
    fadePage();
    $.ajax({
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      url: url,
      type: 'POST',
      data: JSON.stringify({
        filters: filter_json,
        start_date: this.props.startDate || $('input#asset_start_date.report-datepicker').val(),
        end_date: this.props.endDate || $('input#asset_end_date.report-datepicker').val(),
        limit: this.state.perPage,
        offset: pageNumber ? pageNumber : 1,
        current_company_id: this.props.companyId,
        asset_id: this.props.assetId
      }),
      success: function (res, textStatus, jqXhr) {
        self.setState({filter_result: res, pageCount: res.count, activePage: pageNumber, offset: pageNumber});
        unfadePage();
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('An error occurred while retrieving assets versions');
        unfadePage();
      }
    });
  }

  handlePageChange(pageNumber) {
    this.onFilterChanged(this.state.filter_report_state, pageNumber);
  }

  render() {
    /*
    let children = [];
    let categories = [];
    let liveData = [];
    let attributes = [];
    let tagtrackers = [];

    this.props.groups.forEach((group, i) => {
      if (group.groupType == "attrbtn") {
        attributes.push(
          <FilterGroup
            key={group.id}
            group={group}
            groupIdx={i+1}
            dropMenuType={this.props.dropMenuType}
            handleAddRule={this.props.handleAddRule}
            handleGroupRuleDelete={this.props.handleGroupRuleDelete}
            handleGroupRuleChange={this.props.handleGroupRuleChange} />
        )
      } else if (group.groupType == "livebtn") {
        liveData.push(
          <FilterGroup
            key={group.id}
            group={group}
            groupIdx={i+1}
            dropMenuType={this.props.dropMenuType}
            handleAddRule={this.props.handleAddRule}
            handleGroupRuleDelete={this.props.handleGroupRuleDelete}
            handleGroupRuleChange={this.props.handleGroupRuleChange} />
        )
      } else if (group.groupType == "tagbtn") {
        tagtrackers.push(
          <FilterGroup
            key={group.id}
            group={group}
            groupIdx={i+1}
            dropMenuType={this.props.dropMenuType}
            handleAddRule={this.props.handleAddRule}
            handleGroupRuleDelete={this.props.handleGroupRuleDelete}
            handleGroupRuleChange={this.props.handleGroupRuleChange} />
        )
      } else if (group.groupType == "categorybtn") {
        categories.push(
          <FilterGroup
            key={group.id}
            group={group}
            groupIdx={i+1}
            handleAddRule={this.props.handleAddRule}
            handleGroupRuleDelete={this.props.handleGroupRuleDelete}
            handleGroupRuleChange={this.props.handleGroupRuleChange}
            dropMenuType={this.props.dropMenuType}
            catKeys={this.props.catKeys}
            catQualifiers={this.props.catQualifiers} />
        )
      }
    });
    */

    return (
      <div className="rules-wrapper filter-wrapper">
        <div className="row">
          <FilterReport class="card-panel"
                        onFilterChanged={this.onFilterChanged}
                        filter_report_state={this.state.filter_report_state}
                        assetAnalytics={this.props.assetAnalytics}/>
          <FilteredAssets class="card-panel"
                          companyId={this.props.companyId}
                          filteredData={this.state.filter_result}
                          pageNumber={this.state.offset}
                          filter_name={this.state.filter_name}
                          filterLabel={this.state.filterLabel}
                          saveOrUpdateFilter={this.saveOrUpdateFilter}
                          filter_report_state={this.state.filter_report_state}
                          start_date={this.state.start_date}
                          end_date={this.state.end_date}
                          duration={this.state.duration}
                          assetAnalytics={this.props.assetAnalytics}
                          assetId={this.props.assetId}/>

          <div className="col s12">
          {this.state.filter_result.assets && this.state.filter_result.assets.length > 0 ?
            <Pagination
              activePage={this.state.activePage}
              itemsCountPerPage={10}
              totalItemsCount={this.state.pageCount}
              pageRangeDisplayed={10}
              onChange={this.handlePageChange.bind(this)}
            /> : null
          }
          </div>
        </div>

        {/*<div className="add-filter-button">*/}
        {/*<div className="row">*/}
        {/*<p className="sectionHeading margin_left">Filter Type</p>*/}
        {/*</div>*/}
        {/*{ document.body.id == "asset-reports-index" || document.body.id == "asset-reports-filter-get" || document.body.id == "asset-reports-pre-filter-get" ?*/}
        {/*<button className="waves-effect waves-light btn white margin-left btnOutline"*/}
        {/*onClick={(ev)=>this.props.handleAddGroup("categorybtn")}>*/}
        {/*<i className="fa fa-plus-circle" aria-hidden="true"></i>*/}
        {/*Category*/}
        {/*</button>*/}
        {/*:*/}
        {/*""*/}
        {/*}*/}
        {/*<button className="waves-effect waves-light btn white margin-left btnOutline"*/}
        {/*onClick={(ev)=>this.props.handleAddGroup("tagbtn")}>*/}
        {/*<i className="fa fa-plus-circle" aria-hidden="true"></i>*/}
        {/*Tag/Tracker*/}
        {/*</button>*/}

        {/*<button className="waves-effect waves-light btn white margin-left btnOutline"*/}
        {/*onClick={(ev)=>this.props.handleAddGroup("attrbtn")}>*/}
        {/*<i className="fa fa-plus-circle" aria-hidden="true"></i>*/}
        {/*Attribute*/}
        {/*</button>*/}

        {/*<button className="waves-effect waves-light btn white margin-left btnOutline"*/}
        {/*onClick={(ev)=>this.props.handleAddGroup("livebtn")}>*/}
        {/*<i className="fa fa-plus-circle" aria-hidden="true"></i>*/}
        {/*Live Data*/}
        {/*</button>*/}
        {/*</div>*/}

        {/*<div className="filterGroupsContainer">*/}
        {/*<div className={attributes.length == 0 ? "group-attributes hide" : "group-attributes"}>*/}
        {/*<div className="row">*/}
        {/*<p className="sectionHeading">Attributes</p>*/}
        {/*</div>*/}
        {/*<div>{attributes}</div>*/}
        {/*<hr*/}
        {/*className={(tagtrackers.length == 0 && categories.length == 0 && liveData.length == 0) ? "hide" : "filterhr"}/>*/}
        {/*</div>*/}

        {/*<div className={liveData.length == 0 ? "group-liveData hide" : "group-liveData"}>*/}
        {/*<div className="row">*/}
        {/*<p className="sectionHeading">Live Data</p>*/}
        {/*</div>*/}
        {/*<div>{liveData}</div>*/}
        {/*<hr className={(tagtrackers.length == 0 && categories.length == 0) ? "hide" : "filterhr"}/>*/}
        {/*</div>*/}

        {/*<div className={categories.length == 0 ? "group-categories hide" : "group-categories"}>*/}
        {/*<div className="row">*/}
        {/*<p className="sectionHeading">Categories</p>*/}
        {/*</div>*/}
        {/*<div>{categories}</div>*/}
        {/*<hr className={tagtrackers.length == 0 ? "hide" : "filterhr"}/>*/}
        {/*</div>*/}

        {/*<div className={tagtrackers.length == 0 ? "group-categories hide" : "group-tagtrackers "}>*/}
        {/*<div className="row">*/}
        {/*<p className="sectionHeading">Tag Tracker</p>*/}
        {/*</div>*/}
        {/*<div>{tagtrackers}</div>*/}
        {/*</div>*/}
        {/*</div>*/}

        {/*<div className={(categories.length == 0 && tagtrackers.length == 0 && liveData.length == 0 && attributes.length == 0) ? "hide" : "add-filter-button rule-cell" } >*/}
        {/*{ this.props.assetAnalytics ?*/}
        {/*<button className="waves-effect waves-light btn white text-blue margin-left"*/}
        {/*disabled={!this.props.groups.length > 0 || !this.state.name || !this.state.filter.id}*/}
        {/*onClick={(ev)=>this.updateFilter('PATCH')}>*/}
        {/*Update Filter*/}
        {/*</button> : null}*/}

        {/*{ this.props.assetAnalytics ?*/}
        {/*<button className="waves-effect waves-light btn white text-blue margin-left"*/}
        {/*disabled={!this.props.groups.length > 0 || !this.state.name || this.state.name == this.props.filterObject.name}*/}
        {/*onClick={(ev)=>this.updateFilter('POST')}>*/}
        {/*Save As New*/}
        {/*</button> : null }*/}
        {/*</div>*/}
        {/*<button className="waves-effect waves-light btn pull-right show-exp-btn but1"*/}
        {/*disabled={this.props.csvData.length == 0}*/}
        {/*onClick={(ev)=>this.downloadCSV()}>*/}
        {/*Export*/}
        {/*</button>*/}
      </div>
    );
  }
}

/*
FilterGroups.propTypes = {
  handleGroupRuleDelete: React.PropTypes.func,
  handleGroupRuleChange: React.PropTypes.func,
  handleAddRule: React.PropTypes.func,
  handleAddGroup: React.PropTypes.func,
  handleShowData: React.PropTypes.func,
  groups: React.PropTypes.array,
  filterObject: React.PropTypes.object,
  csvData: React.PropTypes.array,
  assetAnalytics: React.PropTypes.bool,
  dropMenuType: React.PropTypes.string,
  catKeys: React.PropTypes.array,
  catQualifiers: React.PropTypes.array
};

function makedata(prop)
{
  for (var i = prop.csvData.length - 1; i >= 0; i--) {
      if(prop.csvData[i][3])
        prop.csvData[i][3] = prop.csvData[i][3].replace(/\,/g,"");
      if(prop.csvData[i][4])
        prop.csvData[i][4] = prop.csvData[i][4].replace(/\,/g,"");
    }
    return prop;
}
*/
