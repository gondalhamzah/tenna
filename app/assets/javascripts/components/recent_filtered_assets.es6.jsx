import Collapsible from 'react-collapsible';
var randomize = require('randomatic');
import {Button} from 'react-materialize';

export default class RecentFilteredAssets extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          DailyexpandedRows: [],
          LoadMore: [],
        };
      this.sendLocation = this.sendLocation.bind(this);
    }
  sendLocation(location, whodunnit_name){
    this.props.getLocation(location, whodunnit_name, this.props.location.title, this.props.location.image_url);
  }

    handleRowClick(item) {
        const currentExpandedRows = this.state.DailyexpandedRows;
        if(!this.state.DailyexpandedRows.includes(item.id)){
            this.state.DailyexpandedRows.push(item.id);
            this.setState({DailyexpandedRows: this.state.DailyexpandedRows});
        }
        else{
            this.setState({DailyexpandedRows: currentExpandedRows});
        }
        if(!item.limit){
            item.limit = 10
        }
        else if(this.oddOrEven(item.limit) == 'odd'){
            item.limit = item.limit - 1;
            item.limit = item.limit + 10
        }
        else{
            item.limit = item.limit + 10
        }
    }

    renderRecentItem(item, created_at) {
        let time = moment(item.timestamp).format("hh:mm A");
        let date = moment(item.timestamp).format("MM/DD/YYYY");
        let whodunnit_name = this.props.location.whodunnit_name ? this.props.location.whodunnit_name : 'Auto';
        let location = this.props.location.location;
        const clickCallback = () => this.handleRowClick(item);
        const itemRows = [
            <tr key={randomize('0', 12) + this.props.location.id}>
                <td>{date}
                    <br/>
                    <span>
						{time + ' EST'}
					</span>
                </td>
                <td>{item.type}</td>
                <td>{this.props.ValueType(item)}<br/><br/>User: <span className="status-text available bb2 assets">{whodunnit_name}</span>
                </td>
                <td className="status-text available assets" onClick={() => this.sendLocation(location, whodunnit_name)}>
                    <a
                        href={'https://maps.googleapis.com/maps/api/staticmap?zoom=19&center='+ location +'&size=600x300&maptype=roadmap&markers=size:mid%7Ccolor:red%%7C'+ location +'&key=AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0'}
                        target="_blank">
                         <img
                            src={'https://maps.googleapis.com/maps/api/staticmap?zoom=19&center='+ location +'&size=600x300&maptype=roadmap&markers=size:mid%7Ccolor:red%%7C'+ location +'&key=AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0'}
                            height='85'
                            width='130'
                        />
                    </a>
                    {location}
                </td>
                <td className="load-td">
                    <div className="" onClick={clickCallback}>
                        {'see more ' + item.type.replace(".", "") + 's   '}
                        <i className="fa fa-angle-down" style={{'font-size': '22px', 'color': 'red'}}></i>
                    </div>
                </td>
            </tr>
        ];
        if (this.state.DailyexpandedRows.includes(item.id)) {
            if(! item.loadMore){
                this.loadMoreRecent(item);
            }
            if(item.limit == 20){
                item.limit = 21
                this.loadMoreRecent(item);
            }
            if(item.limit == 30){
                item.limit = 31
                this.loadMoreRecent(item);
            }
            if(item.limit == 40){
                item.limit = 41
                this.loadMoreRecent(item);
            }
            if(item.limit == 50){
                item.limit = 51
                this.loadMoreRecent(item);
            }
            if(item.limit == 60){
                item.limit = 61
                this.loadMoreRecent(item);
            }
            if(item.limit == 70){
                item.limit = 71
                this.loadMoreRecent(item);
            }
            if(item.limit == 80){
                item.limit = 81
                this.loadMoreRecent(item);
            }
            if(item.limit == 90){
                item.limit = 91
                this.loadMoreRecent(item);
            }
            if(item.limit == 100){
                item.limit = 101
                this.loadMoreRecent(item);
            }
            if(item.loadMore){
                item.loadMore.map((event,ind) => {
                let load_time = moment(event.timestamp).format("hh:mm A");
                let load_date = moment(event.timestamp).format("DD/MM/YYYY");
                    itemRows.push(
                        <tr key={randomize('0', 11) + this.props.location.id}>
                            <td>{load_date}
                                <br/>
                                <span>
                                    {load_time + ' EST'}
                                </span>
                            </td>
                            <td>{event.type}</td>
                            <td>{this.props.ValueType(event)}<br/><br/>User: <span className="status-text available bb2 assets">{this.props.location.whodunnit_name ? this.props.location.whodunnit_name : 'Auto'}</span>
                            </td>
                            <td className="status-text available assets">
                                <img
                                    src={'https://maps.googleapis.com/maps/api/staticmap?zoom=19&center='+ this.props.location.location +'&size=600x300&maptype=roadmap&markers=size:mid%7Ccolor:red%%7C'+ this.props.location.location +'&key=AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0'}
                                    height='85'
                                    width='130'
                                />
                                {this.props.location.location}
                            </td>
                            <td>
                            </td>
                        </tr>
                    )
                });
            }
        }
        return itemRows;
    }

    oddOrEven(x) {
      return ( x & 1 ) ? "odd" : "even";
    }

    loadMoreRecent(item) {
        const asset_id = item.asset_id;
        const event_name = item.type;
        let limit = item.limit;
        if(this.oddOrEven(limit) == "odd"){
            limit = limit - 1;
        }
        var self = this;
        $.ajax({
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          url: '/analytics/more_most_recent',
          type: 'POST',
          data: JSON.stringify({
            asset_id: asset_id,
            event_name: event_name,
            limit: limit,
          }),
          success: function (res, textStatus, jqXhr) {
            item.loadMore = res.assets_history;
            item.loadMore.splice(item.loadMore.length - 1)[0];
            item.loaded = true;
            self.setState({LoadMore: item});
          },
          error: function (jqXHR, textStatus, errorThrown) {
          }
        });
    }
    exportRecent(assetData){
        let csvContent = "data:text/csv;charset=utf-8,";
        csvContent += 'Asset, Site, Group, , Date, Time, Event, Value, User, Location\n';
        var asset = assetData[Object.keys(assetData)[0]].asset;
        var events = assetData[Object.keys(assetData)[0]].events;
        events.forEach((rowArray) => {
          let newRow = [];
          let assetname = asset.title;
          let site = asset.project_name;
          let group = asset.group_name;
          let date = moment(rowArray.timestamp).format('MM/DD/YYYY');
          let time = moment(rowArray.timestamp).format('hh:mm A');
          let eventtype = 'Recent Event';
          let user = rowArray.whodunnit ? rowArray.whodunnit : 'Auto';
          let location = asset.location;
            let unit = rowArray.type;
            let value = rowArray.value.toString().replace(/,/g, ' ');
            let data = [assetname, site, group, eventtype, date, time, unit, value, user, location]
            csvContent += data;
            csvContent += "\n";
        });
        const encodedUri = encodeURI(csvContent);
        const link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "AssetRecentEventReport.csv");
        document.body.appendChild(link);
        link.click();
    }

    render() {
        let recentItemRows = [];
        let min_lat;
        let min_lat_lng;
        return (
            <div>
                <table
                    className="striped analytic-table recent-analytic-table"
                    id={"recent-table-csv-" + this.props.location.id}>
                    <thead>
                    <tr>
                        <th>Date/Time</th>
                        <th>Event</th>
                        <th>Value</th>
                        <th>Location</th>
                        <th>
                        <Button
                            className="waves-effect waves-light btn pull-right btn-bg"
                            id={"recent-exp-bt-" + this.props.location.id}
                            style={{'pointer-events': 'all','right': '17px'}}
                            onClick={this.exportRecent.bind(this,this.props.assetData)}>
                            Export
                          </Button>
                        </th>
                    </tr>
                    </thead>
                    { this.props.recentAsset && this.props.recentAsset.length > 0 ? this.props.recentAsset.map(item => {
                        if(item && item.unit != "Geo Coordinates"){
                            if(!item.id){
                                item.id = randomize('0', 7) + randomize('0', 4)
                            }
                            item.asset_id = this.props.assetId
                            const perItemRows = this.renderRecentItem(item, item.timestamp);
                            recentItemRows = recentItemRows.concat(perItemRows);
                        }
                    }) : null}
                    <tbody>{recentItemRows}</tbody>
                </table>
            </div>
        )
    }
}
