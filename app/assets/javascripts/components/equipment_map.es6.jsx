const { compose } = require("recompose");
const {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
} = require("react-google-maps");
const { MarkerWithLabel } = require("react-google-maps/lib/components/addons/MarkerWithLabel");

const EquipmentMap = (props) => {
  const {lat, lng, location, title, operator, image} = props;
  const MapWithAMarkerWithLabel = compose(
    withScriptjs,
    withGoogleMap
  )(props =>
    <GoogleMap
      defaultZoom={17}
      defaultCenter={{lat: lat, lng: lng}}
    >
      <MarkerWithLabel
        position={{lat: lat, lng: lng}}
        labelAnchor={new google.maps.Point(0, 0)}
        labelStyle={{backgroundColor: "white", fontSize: "16px", padding: "10px", width: "300px", height: "80px"}}
      >
        <div className="row">
          <div className="col s3 m3 l3" hidden={image!="" ? false : true}>
            <img src ={image} style={{width: "60px", height: "60px" }}/>
          </div>
          <div className="col s9 m9 l9">
            <span><b>{title}</b></span><br/><span>{location}</span><br/><span>Operator: {operator}</span>
          </div>
        </div>
      </MarkerWithLabel>
    </GoogleMap>
  );

  return(
    <MapWithAMarkerWithLabel
      googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4R6AN7SmujjPUIGKdyao2Kqitzr1kiRg&v=3.exp&libraries=geometry,drawing,places"
      loadingElement={<div style={{ height: `100%` }} />}
      containerElement={<div style={{ height: `400px` }} />}
      mapElement={<div style={{ height: `100%` }} />}
    />
  );
};

export default EquipmentMap;
