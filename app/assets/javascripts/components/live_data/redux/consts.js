export const ASSET_FILTER_REQUEST = 'LiveData/ASSET_FILTER_REQUEST'
export const ASSET_FILTER_SUCCESS = 'LiveData/ASSET_FILTER_SUCCESS'
export const ASSET_FILTER_FAIL = 'LiveData/ASSET_FILTER_FAIL'

export const FILTER_ALERT_ASSETS_REQUEST = 'LiveData/FILTER_ALERT_ASSETS_REQUEST'
export const FILTER_ALERT_ASSETS_SUCCESS = 'LiveData/FILTER_ALERT_ASSETS_SUCCESS'
export const FILTER_ALERT_ASSETS_FAIL = 'LiveData/FILTER_ALERT_ASSETS_FAIL'

export const SET_BOUNDS = 'LiveData/SET_BOUNDS';

export const SET_FILTER_QUERY = 'LiveData/SET_FILTER_QUERY'
export const SET_FILTER_DAYS = 'LiveData/SET_FILTER_DAYS'

export const SEND_NOTIFICATION_REQUEST = 'LiveData/SEND_NOTIFICATION_REQUEST';
export const SEND_NOTIFICATION_SUCCESS = 'LiveData/SEND_NOTIFICATION_SUCCESS';
export const SEND_NOTIFICATION_FAIL = 'LiveData/SEND_NOTIFICATION_FAIL';

export const ADD_NOTIFICATION = 'LiveData/ADD_NOTIFICATION';
export const DISMISS_NOTIFICATION = 'LiveData/DISMISS_NOTIFICATION';
export const DISMISS_ALL_NOTIFICATION = 'LiveData/DISMISS_ALL_NOTIFICATION';

export const SHOW_ON_MAP = 'LiveData/SHOW_ON_MAP';

export const TOGGLE_FILTER = 'LiveData/TOGGLE_FILTER';

export const GET_MQTT_TOKEN_REQUEST = 'LiveData/GET_MQTT_TOKEN_REQUEST';
export const GET_MQTT_TOKEN_SUCCESS = 'LiveData/GET_MQTT_TOKEN_SUCCESS';

export const GET_MQTT_TOKEN_FAIL = 'LiveData/GET_MQTT_TOKEN_FAIL';

export const UPDATE_GEOFENCE_DRAWING = 'LiveData/UPDATE_GEOFENCE_DRAWING';
export const RESET_GEOFENCE_DRAWING = 'LiveData/RESET_GEOFENCE_DRAWING';

export const SET_GEOFENCE_DRAWING_ACTIVE = 'LiveData/SET_GEOFENCE_DRAWING_ACTIVE';

export const ADD_GEOFENCE_REQUEST = 'LiveData/ADD_GEOFENCE_REQUEST';
export const ADD_GEOFENCE_SUCCESS = 'LiveData/ADD_GEOFENCE_SUCCESS';
export const ADD_GEOFENCE_FAIL = 'LiveData/ADD_GEOFENCE_FAIL';

export const UPDATE_GEOFENCE_REQUEST = 'LiveData/UPDATE_GEOFENCE_REQUEST';
export const UPDATE_GEOFENCE_SUCCESS = 'LiveData/UPDATE_GEOFENCE_SUCCESS';
export const UPDATE_GEOFENCE_FAIL = 'LiveData/UPDATE_GEOFENCE_FAIL';

export const DELETE_GEOFENCE_REQUEST = 'LiveData/DELETE_GEOFENCE_REQUEST';
export const DELETE_GEOFENCE_SUCCESS = 'LiveData/DELETE_GEOFENCE_SUCCESS';
export const DELETE_GEOFENCE_FAIL = 'LiveData/DELETE_GEOFENCE_FAIL';

export const SET_GEOFENCE_NAV_SELECTED = 'LiveData/SET_GEOFENCE_NAV_SELECTED';

export const ADD_GAUGE = 'LiveData/ADD_GAUGE';
export const REMOVE_GAUGE = 'LiveData/REMOVE_GAUGE';
export const ADD_GAUGE_CONDITION = 'LiveData/ADD_GAUGE_CONDITION';
export const REMOVE_GAUGE_CONDITION = 'LiveData/REMOVE_GAUGE_CONDITION';
export const UPDATE_GAUGE_CONDITION_FIELD = 'LiveData/UPDATE_GAUGE_CONDITION_FIELD';

export const ADD_GAUGE_CONTACT = 'LiveData/ADD_GAUGE_CONTACT';
export const REMOVE_GAUGE_CONTACT = 'LiveData/REMOVE_GAUGE_CONTACT';

export const SELECT_GAUGE = 'LiveData/SELECT_GAUGE';
export const SET_GAUGE_ALERT_OBJECT = 'LiveData/SET_GAUGE_ALERT_OBJECT';

export const RESET_GAUGE_ALERT  = 'LiveData/RESET_GAUGE_ALERT';

export const GET_GAUGE_ALERT_REQUEST  = 'LiveData/GET_GAUGE_ALERT_REQUEST';
export const GET_GAUGE_ALERT_SUCCESS  = 'LiveData/GET_GAUGE_ALERT_SUCCESS';
export const GET_GAUGE_ALERT_FAIL     = 'LiveData/GET_GAUGE_ALERT_FAIL';

export const SAVE_GAUGE_ALERT_REQUEST = 'LiveData/SAVE_GAUGE_ALERT_REQUEST';
export const SAVE_GAUGE_ALERT_SUCCESS = 'LiveData/SAVE_GAUGE_ALERT_SUCCESS';
export const SAVE_GAUGE_ALERT_FAIL    = 'LiveData/SAVE_GAUGE_ALERT_FAIL';

export const RESET_GAUGE_ALERT_REQUEST = 'LiveData/RESET_GAUGE_ALERT_REQUEST';
export const RESET_GAUGE_ALERT_SUCCESS = 'LiveData/RESET_GAUGE_ALERT_SUCCESS';
export const RESET_GAUGE_ALERT_FAIL    = 'LiveData/RESET_GAUGE_ALERT_FAIL';
export const RESET_FULL_GAUGE_ALERT    = 'LiveData/RESET_FULL_GAUGE_ALERT';

export const GET_CATEGORY_ASSETS_REQUEST  = 'LiveData/GET_CATEGORY_ASSETS_REQUEST';
export const GET_CATEGORY_ASSETS_SUCCESS  = 'LiveData/GET_CATEGORY_ASSETS_SUCCESS';
export const GET_CATEGORY_ASSETS_FAIL     = 'LiveData/GET_CATEGORY_ASSETS_FAIL';

export const SELECT_ALERT_GEOFENCE = 'LiveData/SELECT_ALERT_GEOFENCE';
export const ADD_ALERT_GEOFENCE = 'LiveData/ADD_ALERT_GEOFENCE';
export const TOGGLE_ALERT_GEOFENCE = 'LiveData/TOGGLE_ALERT_GEOFENCE';

export const FETCH_NOTIF_ASSET_DETAIL_REQUEST  = 'LiveData/FETCH_NOTIF_ASSET_DETAIL_REQUEST';
export const FETCH_NOTIF_ASSET_DETAIL_SUCCESS  = 'LiveData/FETCH_NOTIF_ASSET_DETAIL_SUCCESS';
export const FETCH_NOTIF_ASSET_DETAIL_FAILED     = 'LiveData/FETCH_NOTIF_ASSET_DETAIL_FAIL';

export const UPDATE_ASSET_DATA = 'LiveData/UPDATE_ASSET_DATA';
export const UPDATE_NOTIFICATION_ASSET_LAST_UPDATE = 'LiveData/UPDATE_NOTIFICATION_ASSET_LAST_UPDATE';
export const ON_RECEIVE_NEW_PAYLOAD = 'LiveData/ON_RECEIVE_NEW_PAYLOAD';
