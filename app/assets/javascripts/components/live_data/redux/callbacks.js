export function requestCb(state, payload) {
  return Object.assign({}, state, {loading: true});
};

export function failedCb(state, payload) {
  return Object.assign({}, state, {loading: false, error: payload});
};

export function resetCb(state, payload) {
  const data = payload.results || payload

  return Object.assign({}, state, {loading: false, data: data});
};

export function addCb(state, payload) {
  return Object.assign({}, state,
    {
      data: [...state.data,  payload],
      loaded: true,
      loading: false,
      error: false
    });
}

export function updateCb(state, payload) {
  const idx = _.findIndex(state.data, {id: payload.id})

  const data = [
    ...state.data.slice(0, idx),
    payload,
    ...state.data.slice(idx+1)
  ]

  return Object.assign({}, state,
    {
      data: data,
      loaded: true,
      loading: false,
      error: false
    });
}

export function deleteCb(state, payload) {
  const idx = _.findIndex(state.data, {id: payload.id})
  const data = [
    ...state.data.slice(0, idx),
    ...state.data.slice(idx+1)
  ]

  return Object.assign(
    {},
    state,
    {
      data: data,
      loaded: true,
      loading: false,
      error: false
    }
  );
}

