import { DateTime } from 'luxon';
import { createReducer } from './utils';
import * as types from './consts';

const initialState = {
  data: [],
  assetId: null,
  asset: {
    assetId: 2262,
    asset_num: "15",
    geo: [40.524879, -74.390487],
    hours: 0,
    img_url: "/uploads/photos/774/R9-08-Home_screen-Dismiss_LOw_Prty.jpg",
    lastUpdate: "12/12/17 10:20 PM",
    location: "Edison, NJ 08817",
    miles: 0,
    site: { id: 45, name: "Bridge Project" },
    title:"Asset on 6/61",
    tracking_code: 'QR',
    notes: "Test Notes"
  },
  gaugeAlerts: [
  ],
  notification: {
    loading: false,
    error: '',
  },
  newNotification: null,
};
export function addNotification(state, payload) {
  const gaugeAlerts = state.gaugeAlerts;
  if (state.assetId) {
    const index = _.findIndex(gaugeAlerts, (item) => item.label == payload.type);
    if (index > -1) {
      const gaugeAlert = gaugeAlerts[index];
      switch(gaugeAlert.type) {
        case 'fullSolid':
          if (!gaugeAlert.max || gaugeAlert.max.value < payload.value) {
            gaugeAlert.max = {
              value: payload.value,
              datetime: payload.timestamp,
            };

            if (!gaugeAlert.min) {
              gaugeAlert.min = {
                value: 0,
                datetime: payload.timestamp,
              };
            }
          } else if (!gaugeAlert.min || gaugeAlert.min.value == 0 || gaugeAlert.min.value > payload.value) {
            gaugeAlert.min = {
              value: payload.value,
              datetime: payload.timestamp,
            };
          }
          gaugeAlert.value = payload.value;
          gaugeAlert.unit = payload.unit;
          break;
        case 'halfSolid':
          if (!gaugeAlert.max || gaugeAlert.max.value < payload.value) {
            gaugeAlert.max = {
              value: payload.value,
              datetime: payload.timestamp,
            };
          }
          gaugeAlert.value = payload.value;
          gaugeAlert.unit = payload.unit;
          break;
        case 'odometer':
          gaugeAlert.value = payload.value;
          gaugeAlert.unit = payload.unit;
          gaugeAlert.last = {
            datetime: payload.timestamp,
          };
        case 'progress':
          gaugeAlert.value = payload.value;
          gaugeAlert.unit = payload.unit;
          gaugeAlert.last = {
            datetime: payload.timestamp,
          };
          break;
        case 'bar':
          gaugeAlert.value = payload.value;
          gaugeAlert.last = {
            datetime: payload.timestamp,
          };
          break;
      }
      gaugeAlert.severity = payload.severity;
      gaugeAlerts[index] = gaugeAlert;
    }
  }
  return Object.assign({}, state, {
    data: [payload, ...state.data].slice(0, 200),
    newNotification: payload,
    gaugeAlerts: gaugeAlerts,
  });
}
export function updateNotificationAssetLastUpdate(state, payload) {
  const { asset } = state;
  asset.lastUpdate = payload;
  return Object.assign({}, state, { asset: asset });
}
export function dismissNotification(state, payload) {
  const data = _.reject(state.data, function(pl){
    return pl.message_uniq_id == payload;
  });
  return Object.assign({}, state, { data: data });
}
export function dismissAllNotification(state) {
  return Object.assign({}, state, { data: [] });
}

export function sendNotificationRequestCb(state, payload) {
  return Object.assign({}, state, { notification: { loading: true, error: '' } });
}

export function sendNotificationSuccessCb(state, payload) {
  return Object.assign({}, state, { notification: { loading: false, error: '' } });
}

export function sendNotificationFailedCb(state, payload) {
  return Object.assign({}, state, { notification: { loading: false, error: payload } });
}

export function fetchNotifAssetDetailFailedCb(state, payload) {
  return Object.assign({}, state, { asset: {}, gaugeAlerts: [] });
}

export function fetchNotifAssetDetailRequestCb(state, payload) {
  return Object.assign({}, state, { assetId: payload, asset: {}, gaugeAlerts: [] });
}

export function fetchNotifAssetDetailSuccessCb(state, payload) {
  return Object.assign({}, state, { asset: payload.data, gaugeAlerts: payload.gauges });
}

export function onReceiveNewPayloadCb(state, payload) {
  const {gaugeAlerts} = state;
  const asset = Object.assign({}, state.asset);

  if (asset && gaugeAlerts && gaugeAlerts.length > 0 && asset.assetId == payload.assetId) {

    let newGaugeAlerts = [...gaugeAlerts];

    _.each(payload.value, (p) => {
      const idx = _.findIndex(newGaugeAlerts, {label: p.type});
      if (idx > -1) {
        const gaugeAlert = newGaugeAlerts[idx];
        const dtStr = DateTime.fromFormat(
          p['timestamp'],
          'yyyy.LL.dd HH.mm.ss'
        ,{zone: 'utc'}).toLocal().toFormat('L/d/yy t');

        if (gaugeAlert.type == 'bar') {
          gaugeAlert.value = p['value_boolean']
          gaugeAlert.last.value = p['value_numeric'];
        } else {
          gaugeAlert.value = parseFloat(p['value_numeric']);
          gaugeAlert.last.value = parseFloat(p['value_numeric']);
          if (gaugeAlert.min && gaugeAlert.min.value && gaugeAlert.min.value > p['value_numeric']) {
            gaugeAlert.min.value = p['value_numeric'];
            gaugeAlert.min.datetime = dtStr;
          }

          if (gaugeAlert.max && gaugeAlert.max.value && gaugeAlert.max.value < p['value_numeric']) {
            gaugeAlert.max.value = p['value_numeric'];
            gaugeAlert.max.datetime = dtStr;
          }
        }

        gaugeAlert.last.datetime = dtStr
      }
    });

    const locationPayload = _.find(payload.value, {type: 'Location'});
    if (locationPayload) {
      asset.geo[0] = parseFloat(locationPayload["value_object"]["latitude"]);
      asset.geo[1] = parseFloat(locationPayload["value_object"]["longitude"]);
    }
    return Object.assign({}, state, { gaugeAlerts: newGaugeAlerts, asset });

  } else {
    return state;
  }


}

export default createReducer(initialState, {
  [types.ADD_NOTIFICATION]: addNotification,
  [types.DISMISS_NOTIFICATION]: dismissNotification,
  [types.DISMISS_ALL_NOTIFICATION]: dismissAllNotification,
  [types.SEND_NOTIFICATION_REQUEST]: sendNotificationRequestCb,
  [types.SEND_NOTIFICATION_SUCCESS]: sendNotificationSuccessCb,
  [types.SEND_NOTIFICATION_FAILED]: sendNotificationFailedCb,
  [types.FETCH_NOTIF_ASSET_DETAIL_REQUEST]: fetchNotifAssetDetailRequestCb,
  [types.FETCH_NOTIF_ASSET_DETAIL_SUCCESS]: fetchNotifAssetDetailSuccessCb,
  [types.FETCH_NOTIF_ASSET_DETAIL_FAILED]: fetchNotifAssetDetailFailedCb,
  [types.ON_RECEIVE_NEW_PAYLOAD]: onReceiveNewPayloadCb,
  [types.UPDATE_NOTIFICATION_ASSET_LAST_UPDATE]: updateNotificationAssetLastUpdate,
});

export const fetchNotifAssetDetailAction = (payload) => ({
  type: types.FETCH_NOTIF_ASSET_DETAIL_REQUEST,
  payload,
})

export const addNotificationAction = (payload) => ({
  type: types.ADD_NOTIFICATION,
  payload,
})

export const dismissNotificationAction = (payload) => ({
  type: types.DISMISS_NOTIFICATION,
  payload,
})

export const dismissAllNotificationAction = () => ({
  type: types.DISMISS_ALL_NOTIFICATION,
})

export const sendNotificationAction = (payload) => ({
  type: types.SEND_NOTIFICATION_REQUEST,
  payload,
})

export const updateNewPayloadAction = (payload) => ({
  type: types.ON_RECEIVE_NEW_PAYLOAD,
  payload
})

export const updateNotificationAssetLastUpdateAction = (payload) => ({
  type: types.UPDATE_NOTIFICATION_ASSET_LAST_UPDATE,
  payload
})
