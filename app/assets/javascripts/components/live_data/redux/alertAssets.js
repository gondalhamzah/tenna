import { createReducer } from './utils';
import { requestCb, failedCb, resetCb } from './callbacks';
import * as types from './consts';

const initialState = {
  data: [],
  loading: false,
  error: '',
};

export function resetAlertAssets(state, payload) {
  return Object.assign({}, state, { data: [] });
}

export function filterAlertAssetsSuccess(state, payload) {
  const data = _.each(payload, (item) => {
    item.value = item.id;
    item.label = item.title;
    return item;
  });
  return Object.assign({}, state, { isLoading: false, data: data });
}

export default createReducer(initialState, {
  [types.FILTER_ALERT_ASSETS_REQUEST]: requestCb,
  [types.FILTER_ALERT_ASSETS_SUCCESS]: filterAlertAssetsSuccess,
  [types.FILTER_ALERT_ASSETS_FAILED]: failedCb,
  [types.CLEAR_ALERT_ASSETS_REQUEST]: resetAlertAssets,
});

export const filterAlertAssetsAction = (filters) => ({
  type: types.FILTER_ALERT_ASSETS_REQUEST,
  filters
})

export const clearAlertAssetsAction = (payload) => ({
  type: types.CLEAR_ALERT_ASSETS_REQUEST,
  filters
})
