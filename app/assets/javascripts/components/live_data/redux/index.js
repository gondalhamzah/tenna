import { combineReducers } from 'redux';
import sites from './sites';
import assets from './assets';
import contacts from './contacts';
import gauges from './gauges';
import gaugeAlert from './gaugeAlert';
import notifications from './notifications';
import mqtt from './mqtt';
import geofences from './geofences';
import categoryGroups from './categoryGroups';
import categories from './categories';
import alertAssets from './alertAssets';
import { reducer as toastrReducer } from 'react-redux-toastr'

const appReducer = combineReducers({
  toastr: toastrReducer,
  sites,
  assets,
  contacts,
  notifications,
  mqtt,
  geofences,
  gauges,
  gaugeAlert,
  categoryGroups,
  categories,
  alertAssets,
});

export default function(state, action) {
  return appReducer(state, action)
}
