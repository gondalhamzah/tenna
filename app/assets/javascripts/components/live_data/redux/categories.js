import { createReducer } from './utils';

const initialState = {
  data: {},
};

export default createReducer(initialState, {
});
