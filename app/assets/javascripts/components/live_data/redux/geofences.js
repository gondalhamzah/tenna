import { createReducer } from './utils';
import { requestCb, failedCb, addCb, updateCb, deleteCb } from './callbacks';
import * as types from './consts';

const initialState = {
  data: [],
  drawing: {
    shape: {},
    color: '#fd4f00'
  },
  drawingActive: false,
  navSelected: false
};


export default createReducer(initialState, {
  [types.UPDATE_GEOFENCE_DRAWING]: (state, payload) => {
    const drawing = Object.assign({}, state.drawing || {}, payload.info)
    return Object.assign({}, state, {drawingActive: true, drawing})
  },
  [types.SET_GEOFENCE_DRAWING_ACTIVE]: (state, payload) => {
    return Object.assign({}, state, {drawingActive: payload.value})
  },
  [types.RESET_GEOFENCE_DRAWING]: (state, payload) => {
    return Object.assign({}, state, {drawingActive: false, drawing: {shape: {}, color: '#fd4f00', assets: []}})
  },
  [types.ADD_GEOFENCE_REQUEST]: requestCb,
  [types.ADD_GEOFENCE_FAIL]: failedCb,
  [types.ADD_GEOFENCE_SUCCESS]: (state, payload) => {
    const drawing = Object.assign({}, state.drawing, {id: payload.id})
    return Object.assign({}, addCb(state, payload), {drawing})
  },

  [types.UPDATE_GEOFENCE_REQUEST]: requestCb,
  [types.UPDATE_GEOFENCE_FAIL]: failedCb,
  [types.UPDATE_GEOFENCE_SUCCESS]: updateCb,

  [types.DELETE_GEOFENCE_REQUEST]: requestCb,
  [types.DELETE_GEOFENCE_FAIL]: failedCb,
  [types.DELETE_GEOFENCE_SUCCESS]: deleteCb,

  [types.SET_GEOFENCE_NAV_SELECTED]: (state, payload) => {
    return Object.assign({}, state, {navSelected: payload});
  }
});

export const resetDrawing = () => ({
  type: types.RESET_GEOFENCE_DRAWING
});

export const updateDrawingInfoAction = (info) => ({
  type: types.UPDATE_GEOFENCE_DRAWING,
  payload: {info}
});

export const setDrawingActiveAction = (value) => ({
  type: types.SET_GEOFENCE_DRAWING_ACTIVE,
  payload: {value}
});

export const addGeofenceAction = (data) =>({
  type: types.ADD_GEOFENCE_REQUEST,
  payload: {data}
});

export const updateGeofenceAction = (data) =>({
  type: types.UPDATE_GEOFENCE_REQUEST,
  payload: {data}
});

export const setGeofenceNavSelectedAction = (val) =>({
  type: types.SET_GEOFENCE_NAV_SELECTED,
  payload: val
});

export const deleteGeofenceAction = (id) => ({
  type: types.DELETE_GEOFENCE_REQUEST,
  payload: {id}
});
