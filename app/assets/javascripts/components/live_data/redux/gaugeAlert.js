import { createReducer } from './utils';
import { generate } from 'shortid';
import { requestCb, failedCb, resetCb } from './callbacks';
import * as types from './consts';

const initialState = {
  data: {},
  gauges: [],
  selectedGauges: [],
  availableGauges: [],
  activeGauge: null,
  activeGeofence: null,
  filters: null,
  object: null,
  isLoading: false,
  isInitial: false,
  shouldSave: false,
  deletedConditions: [],
  deletedGauges: [],
  error: '',
  conditionIndex: 0,
  version: 0,
};

export function resetCurrentGaugeAlert(state, payload) {
  return Object.assign({}, state, {
    isLoading: true,
    activeGauge: null,
    activeGeofence: null,
    selectedGauges: [],
    gauges: [],
    data: {},
    deletedConditions: [],
    deletedGauges: [],
  });
}

export function resetFullGaugeAlert(state, payload) {
  return Object.assign({}, state, {
    data: {},
    gauges: [],
    selectedGauges: [],
    activeGauge: null,
    activeGeofence: null,
    filters: null,
    object: null,
    isLoading: false,
    isInitial: false,
    shouldSave: false,
    deletedConditions: [],
    deletedGauges: [],
    error: '',
    conditionIndex: 0,
    version: 0,
  });
}

export function getGaugeAlertRequest(state, payload) {
  return Object.assign({}, state, {
    isLoading: true,
    filters: payload.filters,
    object: payload.object,
    activeGauge: null,
    activeGeofence: null,
    selectedGauges: [],
    gauges: [],
    data: {},
    deletedConditions: [],
    deletedGauges: [],
  });
}

export function getGaugeAlertSuccess(state, payload) {
  const { availableGauges } = state;
  let isInitial = false;
  const gauges = payload.initials.gauges || payload.gauges;
  if (payload.initials.data) {
    isInitial = true;
  }
  const data = payload.initials.data || payload.data;
  const selectedGauges = _.map(gauges, (gaugeId) => {
    return _.find(availableGauges, { id: gaugeId });
  })
  return Object.assign({}, state, {
    isLoading: false,
    data: data,
    gauges: gauges,
    selectedGauges: selectedGauges,
    isInitial: isInitial,
    version: payload.version,
  });
}

export function getCategoryAssetsRequest(state, payload) {
  return Object.assign({}, state, {
    categoryAssets: [],
  });
}

export function getCategoryAssetsSuccess(state, payload) {
  return Object.assign({}, state, {
    categoryAssets: payload,
  });
}

export function saveGaugeAlertRequest(state, payload) {
  return Object.assign({}, state, {
    isSaving: true,
    error: '',
  });
}

export function saveGaugeAlertSuccess(state, payload) {
  return Object.assign({}, state, {
    isInitial: false,
    isSaving: false,
    error: '',
    data: payload.data,
    deletedConditions: [],
    deletedGauges: [],
    version: payload.version,
  });
}

export function saveGaugeAlertFail(state, payload) {
  return Object.assign({}, state, {
    isSaving: false,
    error: payload,
  });
}

export function resetGaugeAlertRequest(state, payload) {
  return Object.assign({}, state, {
    isSaving: true,
    error: '',
    activeGauge: null,
  });
}

export function resetGaugeAlertSuccess(state, payload) {
  const { availableGauges } = state;
  const gauges = payload.gauges;
  const data = payload.data;
  const selectedGauges = _.map(gauges, (gaugeId) => {
    return _.find(availableGauges, { id: gaugeId });
  })
  return Object.assign({}, state, {
    isInitial: false,
    isSaving: false,
    error: '',
    data: data,
    gauges: gauges,
    selectedGauges: selectedGauges,
  });
}

export function resetGaugeAlertFail(state, payload) {
  return Object.assign({}, state, {
    isSaving: false,
    error: payload,
  });
}

export function selectGauge(state, payload) {
  let { data } = state;
  if (data[payload.id]["alerts"].length == 0) {
    let values = {};
    const { meta } = payload;
    if (meta.conditional) {
      values.conditional = meta.conditional.split(',')[0];
    }
    if (meta.unit) {
      values.unit = meta.unit.split(',')[0];
    }

    data[payload.id]["alerts"].push({
      active: true,
      values,
      severe_level: 'L',
      contacts: {
        useOtherContact: false,
        company: [],
        other: []
      },
    });
  }

  return Object.assign({}, state, {  data, activeGauge: payload });
}

export function selectAlertGeofence(state, payload) {
  return Object.assign({}, state, {  activeGeofence: payload });
}

export function addAlertGeofence(state, payload) {
  const { data, activeGauge } = state;
  data[activeGauge.id].geofences.values = payload
  return Object.assign({}, state, {
    data: data,
    conditionIndex: -2,
  });
}

export function toggleAlertGeofence(state, payload) {
  const { data, activeGauge } = state;
  data[activeGauge.id].geofences.active = payload
  return Object.assign({}, state, {
    data: data,
    conditionIndex: -2,
  });
}

export function addGauge(state, payload) {
  const { data } = state;
  data[payload.id] = {
    alerts: [],
    services: [],
  };
  if (payload.name == 'Location') {
    data[payload.id].geofences = {
      active: true,
      values: [],
    };
  }
  return Object.assign({}, state, {
    selectedGauges: [...state.selectedGauges, payload],
    gauges: [...state.gauges, payload.id],
    data: data,
    deletedGauges: [],
    conditionIndex: -1,
  });
}

export function removeGauge(state, payload) {
  const { data, gauges, deletedGauges, selectedGauges } = state;
  const newSelectedGauges = _.reject(selectedGauges, { id: payload });
  const newGauges = _.reject(gauges, (gauge) => { return gauge == payload; });
  if (data[payload]) {
    deletedGauges.push(payload);
  }
  delete data[payload];
  return Object.assign({}, state, {
    selectedGauges: newSelectedGauges,
    gauges: newGauges,
    deletedGauges: deletedGauges,
    activeGauge: null,
    conditionIndex: -1,
  });
}

export function addGaugeCondition(state, payload) {
  const { data, activeGauge } = state;
  const { meta } = activeGauge;
  const { type } = payload;
  let values = {};
  //initialize if blank
  if ( _.isEmpty(data) ) {
    data[activeGauge.id] = {};
    data[activeGauge.id][type] = [];
  };
  
  if (meta.conditional) {
    values.conditional = meta.conditional.split(',')[0];
  }
  if (meta.unit) {
    values.unit = meta.unit.split(',')[0];
  }

  data[activeGauge.id][type].push({
    active: true,
    values: values,
    severe_level: 'L',
    contacts: {
      useOtherContact: false,
      company: [],
      other: []
    },
  });
  return Object.assign({}, state, {
    data: data,
    conditionIndex: data[activeGauge.id][type].length - 1,
  });
}

export function removeGaugeCondition(state, payload) {
  const { data, activeGauge } = state;
  let deletedConditions = [...state.deletedConditions];
  const { type, index } = payload;
  const conditions = data[activeGauge.id][type];
  if (conditions[index].id) {
    deletedConditions.push(conditions[index].id);
  }
  conditions.splice(index, 1);
  data[activeGauge.id][type] = conditions;
  return Object.assign({}, state, {
    data,
    deletedConditions,
    conditionIndex: index - 1,
  });
}

export function addGaugeContact(state, payload) {
  const { data, activeGauge } = state;
  const { type, index, value, contactType } = payload;
  const condition = data[activeGauge.id][type][index];
  condition.contacts[contactType] = value;
  data[activeGauge.id][type][index] = condition
  return Object.assign({}, state, {
    data: data,
    conditionIndex: index,
  });
}

export function removeGaugeContact(state, payload) {
  const { data, activeGauge } = state;
  const { type, index } = payload;
  const conditions = data[activeGauge.id][type];
  const newConditions = _.reject(conditions, index);
  data[activeGauge.id][type] = newConditions;
  return Object.assign({}, state, {
    data,
    conditionIndex: index,
  });
}

export function updateGaugeConditionField(state, payload) {
  const { data, activeGauge } = state;
  const { type, index, field, value } = payload;
  const conditions = data[activeGauge.id][type];
  if (field == 'active' || field == 'severe_level' || field == 'notes') {
    conditions[index][field] = value;
  } else {
    conditions[index]['values'][field] = value;
  }
  data[activeGauge.id][type] = conditions;
  return Object.assign({}, state, {
    conditionIndex: index,
    data,
  });
}

export default createReducer(initialState, {
  [types.RESET_FULL_GAUGE_ALERT]: resetFullGaugeAlert,
  [types.RESET_GAUGE_ALERT]: resetCurrentGaugeAlert,
  [types.GET_GAUGE_ALERT_REQUEST]: getGaugeAlertRequest,
  [types.GET_GAUGE_ALERT_SUCCESS]: getGaugeAlertSuccess,
  [types.GET_GAUGE_ALERT_FAIL]: failedCb,
  [types.SAVE_GAUGE_ALERT_REQUEST]: saveGaugeAlertRequest,
  [types.SAVE_GAUGE_ALERT_SUCCESS]: saveGaugeAlertSuccess,
  [types.SAVE_GAUGE_ALERT_FAIL]: saveGaugeAlertFail,
  [types.RESET_GAUGE_ALERT_REQUEST]: resetGaugeAlertRequest,
  [types.RESET_GAUGE_ALERT_SUCCESS]: resetGaugeAlertSuccess,
  [types.RESET_GAUGE_ALERT_FAIL]: resetGaugeAlertFail,
  [types.ADD_GAUGE]: addGauge,
  [types.REMOVE_GAUGE]: removeGauge,
  [types.ADD_GAUGE_CONDITION]: addGaugeCondition,
  [types.REMOVE_GAUGE_CONDITION]: removeGaugeCondition,
  [types.UPDATE_GAUGE_CONDITION_FIELD]: updateGaugeConditionField,
  [types.ADD_GAUGE_CONTACT]: addGaugeContact,
  [types.SELECT_GAUGE]: selectGauge,
  [types.SELECT_ALERT_GEOFENCE]: selectAlertGeofence,
  [types.ADD_ALERT_GEOFENCE]: addAlertGeofence,
  [types.TOGGLE_ALERT_GEOFENCE]: toggleAlertGeofence,
  [types.GET_CATEGORY_ASSETS_REQUEST]: getCategoryAssetsRequest,
  [types.GET_CATEGORY_ASSETS_SUCCESS]: getCategoryAssetsSuccess,
  // [types.GET_CATEGORY_ASSETS_FAIL]: getCategoryAssetsFail,
});

export const resetFullGaugeAlertAction = (payload) => ({
  type: types.RESET_FULL_GAUGE_ALERT,
  payload,
})

export const resetCurrentGaugeAlertAction = (payload) => ({
  type: types.RESET_GAUGE_ALERT,
  payload,
})

export const getGaugeAlertAction = (payload) => ({
  type: types.GET_GAUGE_ALERT_REQUEST,
  payload,
})

export const saveGaugeAlertAction = (payload) => ({
  type: types.SAVE_GAUGE_ALERT_REQUEST,
  payload,
})

export const resetGaugeAlertAction = (payload) => ({
  type: types.RESET_GAUGE_ALERT_REQUEST,
  payload,
})

export const addGaugeAction = (payload) => ({
  type: types.ADD_GAUGE,
  payload,
})

export const removeGaugeAction = (payload) => ({
  type: types.REMOVE_GAUGE,
  payload,
})

export const addGaugeConditionAction = (payload) => ({
  type: types.ADD_GAUGE_CONDITION,
  payload,
})

export const removeGaugeConditionAction = (payload) => ({
  type: types.REMOVE_GAUGE_CONDITION,
  payload,
})

export const updateGaugeConditionFieldAction = (payload) => ({
  type: types.UPDATE_GAUGE_CONDITION_FIELD,
  payload,
})

export const addGaugeContactAction = (payload) => ({
  type: types.ADD_GAUGE_CONTACT,
  payload,
})

export const selectGaugeAction = (payload) => ({
  type: types.SELECT_GAUGE,
  payload,
})

export const selectAlertGeofenceAction = (payload) => ({
  type: types.SELECT_ALERT_GEOFENCE,
  payload,
})

export const addAlertGeofenceAction = (payload) => ({
  type: types.ADD_ALERT_GEOFENCE,
  payload,
})

export const toggleAlertGeofenceAction = (payload) => ({
  type: types.TOGGLE_ALERT_GEOFENCE,
  payload,
})

export const getCategoryAssetsAction = (payload) => ({
  type: types.GET_CATEGORY_ASSETS_REQUEST,
  payload,
})
