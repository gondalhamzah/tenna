import { createReducer } from './utils';
import { requestCb, failedCb } from './callbacks';
import * as types from './consts';

const initialState = {
  token: null,
  isLoading: false,
  error: '',
};

export function successCb(state, payload) {
  return Object.assign({}, state, { token: payload });
}


export default createReducer(initialState, {
  [types.GET_MQTT_TOKEN_REQUEST]: requestCb,
  [types.GET_MQTT_TOKEN_SUCCESS]: successCb,
  [types.GET_MQTT_TOKEN_FAILED]: failedCb,
});

export const getMqttTokenRequest = (payload) => ({
  type: types.GET_MQTT_TOKEN_REQUEST,
  payload,
})
