import { createReducer } from './utils';
import { requestCb, failedCb, resetCb } from './callbacks';
import * as types from './consts';

const initialState = {
  all: [],
  data: [],
  bounds: null,
  loading: false,
  selectedAsset: null,
  selectAssetClickCount: 0,
  showFilter: true,
  query: '',
  days: 0
};

export function showOnMap(state, payload) {
  return Object.assign({}, state, { selectedAsset: payload.assetId, selectAssetClickCount: state.selectAssetClickCount+1 });
}

export function setBounds(state, payload) {
  return Object.assign({}, state, { bounds: payload });
}
export function setQueryCb(state, query) {
  return Object.assign({}, state, {query});
}
export function toggleFilter(state, payload) {
  return Object.assign({}, state, { showFilter: payload });
}
export function updateAssetData(state, payload) {
  const assetIndex = _.findIndex(state.data, {assetId: payload.assetId});
  const newData = [
    ...state.data.slice(0, assetIndex),
    Object.assign({}, state.data[assetIndex], payload),
    ...state.data.slice(assetIndex + 1)
  ];
  return Object.assign({}, state, {data: newData});
}

export default createReducer(initialState, {
  [types.ASSET_FILTER_REQUEST]: requestCb,
  [types.ASSET_FILTER_SUCCESS]: resetCb,
  [types.ASSET_FILTER_FAILED]: failedCb,
  [types.SET_FILTER_QUERY]: setQueryCb,
  [types.SET_BOUNDS]: setBounds,
  [types.SHOW_ON_MAP]: showOnMap,
  [types.TOGGLE_FILTER]: toggleFilter,
  [types.SET_FILTER_DAYS]: (state, days) => {
    return Object.assign({}, state, {days});
  },
  [types.UPDATE_ASSET_DATA]: updateAssetData
});

export const assetFilterAction = (filters) => ({
  type: types.ASSET_FILTER_REQUEST,
  filters
})

export const setQueryAction = (payload) => ({
  type: types.SET_FILTER_QUERY,
  payload
})

export const setDaysAction = (payload) => ({
  type: types.SET_FILTER_DAYS,
  payload
})

export const setBoundsAction = (payload) => ({
  type: types.SET_BOUNDS,
  payload
})

export const showOnMapAction = (payload) => ({
  type: types.SHOW_ON_MAP,
  payload
})

export const toggleFilterAction = (payload) => ({
  type: types.TOGGLE_FILTER,
  payload
})

export const updateAssetDataAction = (payload) => ({
  type: types.UPDATE_ASSET_DATA,
  payload
})
