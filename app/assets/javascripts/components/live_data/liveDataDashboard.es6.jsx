import { Provider } from 'react-redux';

import LiveDataDashboardFilter from './dashboard/filter.es6.jsx';
import LiveDataDashboardMap from './dashboard/map.es6.jsx';
import LiveDataDashboardSubNav from './dashboard/sub_navigation.es6.jsx';
import LiveDataAssetList from './dashboard/asset_list.es6.jsx';
import LiveDataNavigation from './dashboard/navigation.es6.jsx';
import ConfigureAlerts from './configAlerts/configure_alerts.es6.jsx';

import configureStore from './store/configureStore';

export default class LiveDataDashboard extends React.Component {
  toggleFilter() {
    $("#filters").toggle();
  }
  render () {
    return (
      <div className="liveData">
          <LiveDataNavigation
            selectedTab="dashboard"
          />
          <div>
            <div className="row liveData__filterContainer">
              <div id="filters" className="col liveData__filter">
                <LiveDataDashboardFilter filter={null}/>
              </div>
              <div id="map" className="col liveData__map">
                <LiveDataDashboardMap/>
                <a className="waves-effect waves-light btn liveData__homeLink but1 mt-sm" href="/dashboard">Site Data</a>
                <i
            className="ldDashboardFilter__chevron fa fa-filter"
            aria-hidden="true"
            onClick={this.toggleFilter}
          ></i>
              </div>
            </div>
            <LiveDataDashboardSubNav
            />
            <LiveDataAssetList/>
          </div>
      </div>
    );
  }
}
