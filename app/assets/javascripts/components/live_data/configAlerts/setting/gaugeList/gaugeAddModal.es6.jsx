import { connect } from 'react-redux';
import { generate } from 'shortid';
import { Button, Row, Col, Modal } from 'react-materialize';
import cx from 'classnames';
import {
  addGaugeAction,
} from 'components/live_data/redux/gaugeAlert';

class GaugeAddModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null,
      selectedItems: [],
    };
    this.onClickItem = this.onClickItem.bind(this);
    this.onAddGaugeAlertGroup = this.onAddGaugeAlertGroup.bind(this);
  }

  onClickItem(item) {
    const { selectedItems } = this.state;
    const index = _.findIndex(selectedItems, (o) => o.id === item.id);
    if (index > -1) {
      _.remove(selectedItems, (o) => o.id === item.id);
      this.setState({ selectedItems: selectedItems });
    } else {
      selectedItems.push(item);
      this.setState({ selectedItems: selectedItems });
    }
  }

  onAddGaugeAlertGroup() {
    const { selectedItem, selectedItems } = this.state;
    if (selectedItems === null) {
      return;
    }
    _.each(selectedItems, (item) => this.props.dispatch(addGaugeAction(item)));
    setTimeout(() => {
      this.props.saveGaugeAlert();
    }, 10);
    this.setState({ selectedItems: [] });
  }

  render () {
    const { gauges, selectedGauges } = this.props;
    const { selectedItem, selectedItems } = this.state;
    const filteredGauges = _.filter(gauges, (gauge) => {
      const index = _.findIndex(selectedGauges, (item) => gauge.id === item.id);
      return index === -1;
    });
    return (
      <Modal
        header={<div className="h1">Choose below to add alerts</div>}
        trigger={<a href="javascript:void(0);"><i className="fa fa-plus" aria-hidden="true"></i><div className="caGaugeList__text">Add <br/> Live Data</div></a>}
        actions={[
          <Button
            waves="light"
            className="but2 mr-sm btns_modal"
            modal="close"
            flat
          >Cancel</Button>,
          <Button
            waves="light"
            className="but1 btns_modal"
            modal="close"
            flat
            disabled={selectedItems.length === 0}
            onClick={this.onAddGaugeAlertGroup}
          >Add Live Data</Button>,
        ]}
      >
        <div className="alertTypeModal">
          <div className="alertTypeModal__content">
            {filteredGauges.length > 0 ?
              <Row>
                {filteredGauges.map((item, key) => {
                  const index = _.findIndex(selectedItems, (o) => o.id === item.id);
                  const isActive = index > -1;
                  return (
                    <Col
                      key={key}
                      className="col m3 guage_items"
                    >
                      <div
                        className={cx('alertType__item')}
                      >
                        <img
                          src={isActive ? item.active : item.inactive}
                          onClick={() => { this.onClickItem(item); }}
                        />
                      </div>
                    </Col>
                  );
                })}
              </Row>
              :
              <div className="text-center b8">
                No gauges
              </div>
            }
          </div>
        </div>
      </Modal>
    );
  }
}

GaugeAddModal.propTypes = {
  gauges: React.PropTypes.array,
  selectedGauges: React.PropTypes.array,
};

const mapStateToProps = (state) => ({
  gauges: state.gaugeAlert.availableGauges,
  selectedGauges: state.gaugeAlert.selectedGauges,
})


export default connect(mapStateToProps)(GaugeAddModal)
