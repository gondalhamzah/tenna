import { connect } from 'react-redux';
import cx from 'classnames';
import Slider from 'react-slick';
import GaugeAddModal from './gaugeAddModal.es6.jsx';
import MaterialIcon from 'components/common/MaterialIcon/index.es6.jsx';
import qs from 'qs';
import { withRouter } from 'react-router-dom';
import {
  removeGaugeAction,
  selectGaugeAction,
  saveGaugeAlertAction,
  resetGaugeAlertAction,
  getCategoryAssetsAction,
} from 'components/live_data/redux/gaugeAlert';

function saveToLS(key, value) {
  if (global.localStorage && key) {
    const ls = JSON.parse(global.localStorage.getItem("rgl-8-layouts")) || {};
    ls[key] = value
    global.localStorage.setItem(
      "rgl-8-layouts",
      JSON.stringify(ls)
    );
  }
}

class GaugeList extends React.Component {
  constructor(props) {
    super(props);
    this.onGaugeItemClicked = this.onGaugeItemClicked.bind(this);
    this.onRemoveGauge = this.onRemoveGauge.bind(this);
    this.saveGaugeAlert = this.saveGaugeAlert.bind(this);
    this.resetAlertSetting = this.resetAlertSetting.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {filters} = this.props;
    const newFilters = nextProps.filters;
    console.log(filters, newFilters);
    if (newFilters.gauge_id !== filters.gauge_id || this.props.selectedGauges.length !== nextProps.selectedGauges.length) {
      this.selectGauge(newFilters.gauge_id, nextProps.selectedGauges);
    }
  }

  onGaugeItemClicked(item) {
    const { filters } = this.props;
    const newFilters = Object.assign({}, filters, {gauge_id: item.id});
    this.props.dispatch(selectGaugeAction(item));
    this.props.history.push({pathname: '/live_data/config', search: `?${qs.stringify(newFilters, {strictNullHandling: true})}`});
  }

  selectGauge(gaugeId, selectedGauges) {
    const gauge = _.find(selectedGauges, {id: parseInt(gaugeId)});
    if (gauge) {
      this.props.dispatch(selectGaugeAction(gauge));
    }
  }

  onRemoveGauge(item) {
    const that = this;
    const { gaugeAlert, filters } = this.props;
    const assetId = gaugeAlert.filters.asset_id;
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      that.props.dispatch(removeGaugeAction(item.id));
      that.saveGaugeAlert();
      saveToLS(assetId, {});
      const newFilters = Object.assign({}, filters, {gauge_id: null});
      that.props.history.push({pathname: '/live_data/config', search: `?${qs.stringify(newFilters, {strictNullHandling: true})}`});

    });
  }

  saveGaugeAlert() {
    const { gaugeAlert: { isInitial, data, gauges, filters, deletedConditions, deletedGauges, version } } = this.props;
    if (isInitial) {
      return;
    }
    const payload = {
      data: {
        data: data,
        gauges: gauges,
        deleted_conditions: deletedConditions,
        deleted_gauges: deletedGauges,
        version: version,
      },
      filters: filters,
    };

    this.props.dispatch(saveGaugeAlertAction(payload));
    saveToLS(filters.asset_id, {});
  }

  resetAlertSetting(level) {
    const that = this;
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reset!'
    }).then(function () {
      const { gaugeAlert, gaugeAlert: { data, gauges, filters, deletedConditions, deletedGauges } } = that.props;
      const payload = {
        data: {
          level,
        },
        filters: {
          category_group_id: filters.category_group_id,
          category_id: filters.category_id,
          asset_id: filters.asset_id,
        },
      };

      that.props.dispatch(resetGaugeAlertAction(payload));

      saveToLS(filters.asset_id, {});
    });
  }

  render () {

    let settings = {
      className: 'caGaugeList__carousel center',
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 5,
      responsive: [
        {
          breakpoint: 200,
          settings: { slidesToShow: 1 }
        },
        {
          breakpoint: 768,
          settings: { slidesToShow: 1 }
        },
        {
          breakpoint: 1024,
          settings: { slidesToShow: 5 }
        },
        {
          breakpoint: 100000,
          settings: { slidesToShow: 5 }
        }
      ],
    };

    const { gauges, selectedGauges, activeGauge, gaugeAlert } = this.props;

    return (
      <div className="caGaugeList">
        <div className="caGaugeList__title">
          <div className="h1">{ gaugeAlert.object.name || gaugeAlert.object.title }</div>
          <div className="caGaugeList__action">
            {gaugeAlert.filters.asset_id &&
              <div>
                <a
                  href="javascript:void(0);"
                  onClick={() => this.resetAlertSetting('sub_category')}
                >Reset to Subcategory</a>
              </div>
            }
            {(gaugeAlert.filters.asset_id || gaugeAlert.filters.category_id) &&
              <div>
                <a
                  href="javascript:void(0);"
                  onClick={() => this.resetAlertSetting('category')}
                >Reset to Category</a>
              </div>
            }
          </div>
        </div>
        <div className="caGaugeList__container">
          <div className='caGaugeList__add'>
            <div className='caGaugeList__addButton'>
              <div>
                <GaugeAddModal saveGaugeAlert={this.saveGaugeAlert} />
              </div>
            </div>
          </div>
          {selectedGauges && selectedGauges.length > 0 &&
            <div className="caGaugeList__list">
              <Slider
                {...settings}
              >
                {selectedGauges.map((item, key) => (
                  <div
                    key={key}
                    onClick={() => { this.onGaugeItemClicked(item) }}
                  >
                    <div className={cx("caGaugeList__card", {'caGaugeList__card--active': activeGauge && activeGauge.id === item.id})}>
                        <MaterialIcon
                          name="close"
                          className="caGaugeList__deleteIcon btn-floating caGaugeList__deleteCard"
                          onClick={() => { this.onRemoveGauge(item) }}
                        />
                      <img
                        className="caGaugeList__cardImage"
                        src={activeGauge && activeGauge.id === item.id ? item.active : item.inactive}
                      />
                    </div>
                  </div>
                ))}
              </Slider>
            </div>
          }
        </div>
      </div>
    );
  }
}

GaugeList.propTypes = {
  gauges: React.PropTypes.array,
  selectedGauges: React.PropTypes.array,
  gaugeAlert: React.PropTypes.object,
  activeGauge: React.PropTypes.object,
};

const mapStateToProps = (state, props) => ({
  gaugeAlert: state.gaugeAlert,
  gauges: state.gaugeAlert.availableGauges,
  selectedGauges: state.gaugeAlert.selectedGauges || [],
  activeGauge: state.gaugeAlert.activeGauge,
  filters: qs.parse(props.location.search, {ignoreQueryPrefix: true, strictNullHandling: true})
})


export default withRouter(connect(mapStateToProps)(GaugeList));
