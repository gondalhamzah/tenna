import cx from 'classnames';
import IdleAlertNotes from './idle_alert_notes.es6.jsx';
import AlertContactList from './contactList/alertContactList.es6.jsx';
import SeverityList from './severityList.es6.jsx';
import MaterialIcon from 'components/common/MaterialIcon/index.es6.jsx';
import Toggle from 'react-toggle';
import Select from 'react-select';
import { Input } from 'react-materialize';

export default class AlertItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: '',
      error: {},
      inputValue: '',
    };
    this.inputTimeout = null;
    this.onTabChange = this.onTabChange.bind(this);
    this.onSeverityChange = this.onSeverityChange.bind(this);
    this.onActiveChange = this.onActiveChange.bind(this);
    this.onFieldChange = this.onFieldChange.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.buildTemplate = this.buildTemplate.bind(this);
    this.onContactChange = this.onContactChange.bind(this);
    this.validateInput = this.validateInput.bind(this);
  }

  componentDidMount() {
    const { condition } = this.props;
    this.setState({
      inputValue: condition.values['number'],
      error: {},
    });
  }

  componentWillReceiveProps(newProps) {
    const { gauge } = this.props;
    if (gauge.id !== newProps.gauge.id) {
      this.setState({
        inputValue: newProps.condition.values['number'],
        error: {},
      });
    }
  }

  onTabChange(tabName) {
    if(tabName === this.state.selectedTab) {
      this.setState({ selectedTab: '' })
    }
    else {
      this.setState({ selectedTab: tabName })
    }
  }

  onFieldChange(value, name) {
    const { condition } = this.props;
    $('#' + value.labelid).html(value.key);
    if (name === 'unit') this.validateInput(condition.values['number'], value.value, 'number');

    this.props.onFieldChange(value.value, name);
  }

  onInputChange(e, name) {
    const { condition } = this.props;
    const value = e.target.value;

    if (name === 'number') {
      if (($('.Select-value-label')[1].innerHTML == "minute(s)" && value > 60)||($('.Select-value-label')[1].innerHTML == "hour(s)" && value > 24)){
        e.preventDefault();
        e.stopPropagation();
      }else{
        this.setState({ inputValue: value });
      }
    }

    const isValid = this.validateInput(value, condition.values['unit'], name);

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }
    const timeoutId = setTimeout(() => {
      if (isValid) {
        this.props.onFieldChange(value, name);
      }
    }, 500);
    this.inputTimeout = timeoutId;
  }

  validateInput(value, unitVal, name) {
    const { gauge } = this.props;
    const { error } = this.state;
    const unit = gauge.meta['unit'] ? gauge.meta['unit'].split(',') : [];

    let min = gauge.meta.min;
    let max = gauge.meta.max;

    if (unit && unit.length > 1 && unitVal) {
      const unitIndex = _.findIndex(unit, (o) => o === unitVal);
      if (unitIndex > -1) {
        min = gauge.meta.min[unitIndex];
        max = gauge.meta.max[unitIndex];
      } else {
        min = null;
        max = null;
      }
    }
    error[name] = '';
    if (min !== null && parseInt(min) > parseInt(value)) {
      error[name] = `Should exceed ${min}`;
    }

    if (max !== null && max < value) {
      error[name] = `Should not exceed ${max}`;
    }

    if (error[name]) {
      this.setState({ error: error });
      return false;
    } else {
      return true;
    }
  }

  onContactChange(value, type) {
    this.props.onContactChange(value, type);
  }

  onActiveChange(e) {
    this.props.onFieldChange(e.target.checked, 'active');
  }

  onSeverityChange(severity) {
    this.props.onFieldChange(severity, 'severe_level');
  }

  buildTemplate(uniqid) {
    const { template, gauge, condition, contacts } = this.props;
    const { inputValue } = this.state;
    const { error } = this.state;
    const content = template && template.map((item, key) => {
      if (item.type === 'string') {
        if( item.value && item.value.trim().length > 0) {
          return (<div key={key} className="alertItem__condition">{item.value}</div>);
        }
      } else if (item.type === 'field' && item.value === 'number') {
        const name = item.value;
        return (
          <div key={key} className="alertItem__condition">
            <Input
              type="number"
              className={cx('alertItem__input inputfielD', {'alertItem__input--error inputfielD': error[name]})}
              value={inputValue}
              onChange={(e) => { this.onInputChange(e, name); }}
              error={error[name]}
            />
            <div className="alertItem__inputError">
              {error[name]}
            </div>
          </div>
        );
      } else {
        const options = gauge.meta ?
            gauge.meta[item.value].split(',').map((item) => {
              let related_label = '';
              if(gauge.meta.related_label) {
                related_label = gauge.meta.related_label[item];
              }
              return {label: item, value: item, key: related_label, labelid: uniqid};
            }) : [];
        return (
          <div key={key} className="alertItem__condition alertItem__condition--select">
            <Select
              clearable={false}
              onChange={(e) => { this.onFieldChange(e, item.value); }}
              options={options}
              name="form-field-name"
              value={condition.values[item.value]}
            />
          </div>
        );
      }
    })
    return content;
  }

  render () {
    const { selectedTab } = this.state;
    const { gauge, template, condition, contacts, isSaved } = this.props;
    const companyContactCnt = (condition.contacts.company || []).length;
    const otherContactCtn = (condition.contacts.other || []).length;
    const contactStr = companyContactCnt + otherContactCtn > 0 ?
      (
        (companyContactCnt > 0 ? `${companyContactCnt} company contact${companyContactCnt>1 ? 's, ' : ', '}` : '') +
        (otherContactCtn > 0 ? `${otherContactCtn} other contact${otherContactCtn>1 ? 's' : ''}` : '')
      ) : 'No contacts...';
    const { notes } = condition;
    const noteStr = notes ? `${notes.length > 23 ? notes.substring(0, 20)+'...' : notes}` : 'No notes...';
    const uniqid = _.uniqueId('related_label_');
    return (
      <div className="alertItem">
        <div className="alertItem__notification">
          {isSaved && 'Autosaved'} &nbsp;
        </div>
        <div className="alertItem__conditionList">
          <div className="alertItem__condition">
            <Toggle
              checked={condition.active}
              icons={false}
              onChange={this.onActiveChange}
            />
          </div>
          <div className="alertItem__condition">
            Send
          </div>
          <div className="alertItem__condition">
            <SeverityList
              condition={condition}
              onChange={this.onSeverityChange}
            />
          </div>
          <div className="alertItem__condition">
            Alert
          </div>
          <div className="alertItem__condition related-label" id={uniqid}>{gauge.meta.related_label ? gauge.meta.related_label[condition.values.conditional] : ''}</div>
          {this.buildTemplate(uniqid)}
          <div className="alertItem__condition alertItem__condition--delete">
            <MaterialIcon
              name="delete"
              onClick={this.props.onRemoveCondition}
            />
          </div>
        </div>
        <div className="row">
          <div className="row idleHourAlert__subNav padding-left-5">
            <div className="col s12 m12 l12">
              <ul className="idleHourAlert__group">
                <li className={cx('idleHourAlert__tab col s12 m6 l6', { 'idleHourAlert__tab--active': selectedTab === 'notes' })}>
                  <a
                    className="idleHourAlert__tabLink b21"
                    onClick={() => {this.onTabChange('notes')}}
                  >
                    <div className="idleHourAlert__pullLeft">
                      Notes
                    </div>
                    <div className="idleHourAlert__pullRight b13">
                      {noteStr}
                    </div>
                    <i className="idleHourAlert__chevron material-icons">
                      {selectedTab === 'notes' ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}
                    </i>
                  </a>
                </li>
                <li className={cx('idleHourAlert__tab col s12 m6 l6', { 'idleHourAlert__tab--active': selectedTab === 'contact' })}>
                  <a
                    className="idleHourAlert__tabLink b21"
                    onClick={() => {this.onTabChange('contact')}}
                  >
                    <div className="idleHourAlert__pullLeft">
                      Contact
                    </div>
                    <div className="idleHourAlert__pullRight b13">
                      {contactStr}
                    </div>
                    <i className="idleHourAlert__chevron material-icons">
                      {selectedTab === 'contact' ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}
                    </i>
                  </a>
                </li>
              </ul>

              <div className={cx('idleHourAlert__tabContent', { 'idleHourAlert__tabContent--active' : selectedTab === 'notes' })}>
                <IdleAlertNotes
                  value={condition.notes}
                  onChange={(e) => { this.onInputChange(e, 'notes'); }}
                />
              </div>
              <div  className={cx('idleHourAlert__tabContent', { 'idleHourAlert__tabContent--active' : selectedTab === 'contact' })}>
                <AlertContactList
                  onContactChange={(value, type) => { this.onContactChange(value, type); }}
                  contacts={contacts}
                  condition={condition}
                />
              </div>
            </div>
          </div>
          <div className="padding-left-5">
            <hr />
          </div>
        </div>
      </div>
    );
  }
}
