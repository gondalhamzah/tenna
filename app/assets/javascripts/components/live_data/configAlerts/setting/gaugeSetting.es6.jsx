import { connect } from 'react-redux';
import cx from 'classnames';
import AlertItem from './alertItem.es6.jsx';
import {
  addGaugeConditionAction,
  removeGaugeConditionAction,
  updateGaugeConditionFieldAction,
  saveGaugeAlertAction,
  addGaugeContactAction,
} from 'components/live_data/redux/gaugeAlert';
import { updateGaugeConditionField } from '../../redux/gaugeAlert';

class GaugeSetting extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 'alerts',
      components: [1],
      template: null,
      eventIndex: 0,
      isSaved: false,
    };
    this.notifTimeout = null;
    this.onTabChange = this.onTabChange.bind(this);
    this.addAlert = this.addAlert.bind(this);
    this.parseTemplate = this.parseTemplate.bind(this);
    this.onFieldChange = this.onFieldChange.bind(this);
    this.onContactChange = this.onContactChange.bind(this);
    this.onRemoveCondition = this.onRemoveCondition.bind(this);
  }

  componentDidMount() {
    const { gaugeAlert: { activeGauge } } = this.props;
    this.parseTemplate(activeGauge);
  }

  componentWillReceiveProps(nextProps) {
    const {
      gaugeAlert: {
        activeGauge,
        conditionIndex,
        isSaving,
        error,
        isInitial,
      },
    } = nextProps;
    if ((!this.props.activeGauge && activeGauge) ||
        this.props.activeGauge && activeGauge && this.props.activeGauge.id != activeGauge.id) {
      this.parseTemplate(activeGauge);
    }
    const wasSaving = this.props.gaugeAlert.isSaving;
    const isSaved = wasSaving && !isSaving;

    if (conditionIndex >= 0) {
      if (isSaved && !error && !this.props.gaugeAlert.isInitial) {
        // toastr.success('Success!', 'Alert setting is saved successfully.');
        this.setState({ isSaved: true });
        if (this.notifTimeout) {
          clearTimeout(this.notifTimeout);
        }
        const timeoutId = setTimeout((that=this) => {
          that.setState({ isSaved: false });
        }, 3000);
        this.notifTimeout = timeoutId;
      }
    }
  }

  onTabChange(tabName) {
    if(tabName === this.state.selectedTab) {
      this.setState({ selectedTab: '' })
    }
    else {
      this.setState({ selectedTab: tabName })
    }
  }

  onFieldChange(index, value, name) {
    const { selectedTab } = this.state;
    const payload = {
      type: selectedTab,
      index: index,
      value: value,
      field: name,
    };
    this.props.dispatch(updateGaugeConditionFieldAction(payload))
    this.props.saveGaugeAlert();
  }

  onContactChange(index, value, type) {
    const { selectedTab } = this.state;
    const payload = {
      type: selectedTab,
      index: index,
      contactType: type,
      value: value,
    };
    this.props.dispatch(addGaugeContactAction(payload))
    this.props.saveGaugeAlert();
  }

  onRemoveCondition(index) {
    const { selectedTab } = this.state;
    const payload = {
      type: selectedTab,
      index: index,
    };
    const that = this;

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      that.props.dispatch(removeGaugeConditionAction(payload));
      that.props.saveGaugeAlert();
    });
  }

  addAlert() {
    const { selectedTab } = this.state;
    if (!selectedTab) return;
    const payload = { type: selectedTab }
    this.props.dispatch(addGaugeConditionAction(payload));
    this.props.saveGaugeAlert();
  }

  parseTemplate(activeGauge) {
    if (!activeGauge) return;

    let templateStr = activeGauge.template;
    const templateArr = [];
    let sIndex, eIndex;

    while(templateStr) {
      sIndex = templateStr.indexOf('[');
      eIndex = templateStr.indexOf(']');
      if (sIndex >= 0 && eIndex >= 0) {
        templateArr.push({
          type: 'string',
          value: templateStr.substr(0, sIndex).trim(),
        });
        templateArr.push({
          type: 'field',
          value: templateStr.substr(sIndex + 1, eIndex - sIndex - 1).trim(),
        });
        templateStr = templateStr.substr(eIndex + 1, templateStr.length);
      } else {
        templateArr.push({
          type: 'string',
          value: templateStr.trim(),
        });
        templateStr = '';
      }
    }
    this.setState({ template: templateArr });
  }

  render () {

    let idleContacts = [];

    const { components, selectedTab, template, isSaved } = this.state;
    const {
      gaugeAlert: {
        activeGauge,
        conditionIndex,
      },
      data,
      contacts,
    } = this.props;

    if (!activeGauge) return null;

    const alerts = data[activeGauge.id] ? data[activeGauge.id].alerts : [];
    const services = data[activeGauge.id] ? data[activeGauge.id].services : [];

    return (
      <div className="row liveDataAlerts">
        <div className="col s12 m12 l12">
          <ul className="liveDataAlerts__group">
            <li className={cx('liveDataAlerts__tab col s12 m6 l6', { 'liveDataAlerts__tab--active': selectedTab === 'alerts' })}>
              <a
                className="liveDataAlerts__tabLink h3"
                onClick={() => {this.onTabChange('alerts')}}
              >
                {activeGauge.name} - Alert
                <i className="liveDataAlerts__chevron material-icons">
                  {selectedTab === 'notification' ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}
                </i>
              </a>
            </li>
            <li className={cx('liveDataAlerts__tab col s12 m6 l6', { 'liveDataAlerts__tab--active': selectedTab === 'services' }, { 'hidden': !activeGauge.can_request_service })}>
              <a
                className="liveDataAlerts__tabLink h1"
                onClick={() => {this.onTabChange('services')}}
              >
                Service Request
                <i className="liveDataAlerts__chevron material-icons">
                  {selectedTab === 'geofence' ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}
                </i>
              </a>
            </li>
          </ul>

          <div className={cx('liveDataAlerts__tabContent', { 'liveDataAlerts__tabContent--active' : selectedTab === 'alerts' })}>
            {alerts.map((condition, key) => (
              <AlertItem
                key={condition.id}
                contacts={contacts}
                gauge={activeGauge}
                template={template}
                isSaved={isSaved && conditionIndex === key}
                condition={condition}
                onContactChange={(value, type) => { this.onContactChange(key, value, type); }}
                onFieldChange={(value, name) => { this.onFieldChange(key, value, name); }}
                onRemoveCondition={() => { this.onRemoveCondition(key); }}
              />
            ))}

            <a className="pointer_cursor" onClick={() => {this.addAlert()}}>
              <i className="fa fa-plus-circle mr-sm" aria-hidden="true"></i>
              <span className="b35">Add New Alert</span>
            </a>
            <hr />
          </div>
          <div  className={cx('liveDataAlerts__tabContent', { 'liveDataAlerts__tabContent--active' : selectedTab === 'services' })}>
            {services.map((condition, key) => (
              <AlertItem
                key={condition.id}
                contacts={contacts}
                gauge={activeGauge}
                template={template}
                isSaved={isSaved && conditionIndex === key}
                condition={condition}
                onContactChange={(value, type) => { this.onContactChange(key, value, type); }}
                onFieldChange={(value, name) => { this.onFieldChange(key, value, name); }}
                onRemoveCondition={() => { this.onRemoveCondition(key); }}
              />
            ))}

            <a className="pointer_cursor" onClick={() => {this.addAlert()}}>
              <i className="fa fa-plus-circle mr-sm" aria-hidden="true"></i>
              <span className="b35">Add New Alert</span>
            </a>
            <hr />
          </div>
        </div>
      </div>
    );
  }
}

GaugeSetting.propTypes = {
  data: React.PropTypes.object,
  activeGauge: React.PropTypes.object,
};

const mapStateToProps = (state) => ({
  contacts: state.contacts.data,
  data: state.gaugeAlert.data,
  gauges: state.gaugeAlert.gauges,
  filters: state.gaugeAlert.filters,
  gaugeAlert: state.gaugeAlert,
  deletedConditions: state.gaugeAlert.deletedConditions,
})


export default connect(mapStateToProps)(GaugeSetting)
