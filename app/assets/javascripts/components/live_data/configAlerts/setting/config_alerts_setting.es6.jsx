import { connect } from 'react-redux';

import GaugeList from './gaugeList/gaugeList.es6.jsx';
import AlertSetting from './alertSetting.es6.jsx';


class ConfigAlertSetting extends React.Component {
  constructor(props) {
    super(props);
      this.state = {
      itemName: this.props.itemName,
    };
  }

  render () {
    const { gaugeAlert } = this.props;
    return (
      <div className = "caSetting" >
        { gaugeAlert.object &&
          <div>
            <GaugeList />
            <AlertSetting />
          </div>
        }
      </div>
    );
  }
}

ConfigAlertSetting.propTypes = {
  gaugeAlert: React.PropTypes.object,
};

const mapStateToProps = (state) => ({
  gaugeAlert: state.gaugeAlert,
})


export default connect(mapStateToProps)(ConfigAlertSetting)
