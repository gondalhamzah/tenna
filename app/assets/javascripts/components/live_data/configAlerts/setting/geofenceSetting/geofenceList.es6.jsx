import { connect } from 'react-redux';
import cx from 'classnames';
import Slider from 'react-slick';

import MaterialIcon from 'components/common/MaterialIcon/index.es6.jsx';

class GeofenceList extends React.Component {
  constructor(props) {
    super(props);
    this.onGeofenceItemClicked = this.onGeofenceItemClicked.bind(this);
  };

  onGeofenceItemClicked(item) {
    this.props.onSelectGeofence(item);
    // this.props.dispatch(selectGaugeAction(item));
  }

  render () {
    const { geofences, gaugeAlert: { activeGeofence } } = this.props;

    let settings = {
      className: 'liveDataAssetCarousel caGeofenceList__carousel center',
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      responsive: [ 
        {
          breakpoint: 200,
          settings: { slidesToShow: 1 }
        },
        {
          breakpoint: 768,
          settings: { slidesToShow: 2 }
        },
        {
          breakpoint: 1024,
          settings: { slidesToShow: 4 }
        },
        {
          breakpoint: 100000,
          settings: { slidesToShow: 4 }
        }
      ],
    };
    return (
      <div className="caGeofenceList">
        <Slider
          {...settings}
        >
          {geofences.map((item, key) => (
            <div
              key={key}
              onClick={() => { this.onGeofenceItemClicked(item) }}
            >
              <div className={cx("caGeofenceList__card", {'caGeofenceList__card--active': activeGeofence && activeGeofence.id == item.id })}>
                <img
                  className="caGeofenceList__cardImage"
                  src={item.map_img}
                />
                <div className="caGeofenceList__cardTitle b21">
                  {item.name}
                </div>
              </div>
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}

GeofenceList.propTypes = {
  geofences: React.PropTypes.array,
};

export default GeofenceList