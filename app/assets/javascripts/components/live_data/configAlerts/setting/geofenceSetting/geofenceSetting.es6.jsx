import { connect } from 'react-redux';
import Toggle from 'react-toggle';

import MaterialIcon from 'components/common/MaterialIcon/index.es6.jsx';

import GeofenceList from './geofenceList.es6.jsx';
import GeofenceConfig from './geofenceConfig.es6.jsx';
import { selectAlertGeofenceAction, addAlertGeofenceAction, toggleAlertGeofenceAction } from 'components/live_data/redux/gaugeAlert';

class GeofenceSetting extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSetting: true,
      isSaved: false,
    };
    this.onActiveChange = this.onActiveChange.bind(this);
    this.toggleSetting = this.toggleSetting.bind(this);
    this.onSelectGeofence = this.onSelectGeofence.bind(this);
    this.onOptionChange = this.onOptionChange.bind(this);
    this.notifTimeout = null;
  };

  onActiveChange(e) {
    const value = e.target.checked;
    
    this.props.dispatch(toggleAlertGeofenceAction(value));
    this.props.saveGaugeAlert();
  }

  componentWillReceiveProps(nextProps) {
    const {
      gaugeAlert: {
        conditionIndex,
        isSaving,
        error,
        isInitial,
      },
    } = nextProps;
    const wasSaving = this.props.gaugeAlert.isSaving;
    const isSaved = wasSaving && !isSaving;

    if (conditionIndex === -2 && isSaved && !error && !this.props.gaugeAlert.isInitial) {
      // toastr.success('Success!', 'Alert setting is saved successfully.');
      this.setState({ isSaved: true });
      if (this.notifTimeout) {
        clearTimeout(this.notifTimeout);
      }
      const timeoutId = setTimeout((that=this) => {
        that.setState({ isSaved: false });
      }, 3000);
      this.notifTimeout = timeoutId;
    }
  }

  toggleSetting() {
    const { showSetting } = this.state;
    this.setState({ showSetting: !showSetting });
  }

  onSelectGeofence(item) {
    this.props.dispatch(selectAlertGeofenceAction(item));
    // this.props.saveGaugeAlert();
  }

  onOptionChange(item) {
    const { gaugeAlert, gaugeAlert: { activeGauge } } = this.props;
    const geofenceConditions = gaugeAlert.data[activeGauge.id].geofences.values || [];
    const index = _.findIndex(geofenceConditions, { geofence_id: item.geofence_id });
    if (index == -1) {
      geofenceConditions.push(item);
    } else {
      geofenceConditions[index] = item;
    }
    this.props.dispatch(addAlertGeofenceAction(geofenceConditions));
    this.props.saveGaugeAlert();
  }
  render () {
    const { geofences, gaugeAlert, gaugeAlert: { activeGauge } } = this.props;
    const { showSetting, isSaved } = this.state;
    const geofenceActive = gaugeAlert.data[activeGauge.id].geofences.active;

    return (
      <div className="caGeofenceSetting">
        <div className="caGeofenceSetting__header">
          <div className="caGeofenceSetting__title h1">
            Advance Geofence
          </div>
          <div className="caGeofenceSetting__action">
            <Toggle
              className="caGeofenceSetting__activeButton"
              icons={false}
              checked={geofenceActive}
              onChange={this.onActiveChange}
            />
          </div>
          <div className="caGeofenceSetting__notification">
            {isSaved && 'Autosaved'}&nbsp;
          </div>
        </div>
        {showSetting &&
          <div className="caGeofenceSetting__content">
            <div className="caGeofenceSetting__subtitle b9">
              This alert page is used to exclude certain assets from specific geofences.
            </div>
            <GeofenceList
              gaugeAlert={gaugeAlert}
              geofences={geofences}
              onSelectGeofence={this.onSelectGeofence}
            />
            <GeofenceConfig
              gaugeAlert={gaugeAlert}
              onOptionChange={this.onOptionChange}
            />
          </div>
        }
        {isSaved &&
          <div className="success-text">
            Autosaved
          </div>
        }
      </div>
    );
  }
}

GeofenceSetting.propTypes = {
  geofences: React.PropTypes.array,
  gaugeAlert: React.PropTypes.object,
};

const mapStateToProps = (state) => ({
  geofences: state.geofences.data,
  gaugeAlert: state.gaugeAlert,
})

export default connect(mapStateToProps)(GeofenceSetting)