import { connect } from 'react-redux';
import Toggle from 'react-toggle';

class GeofenceConfig extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      geofenceCondition: null,
    }
    this.onOptionChange = this.onOptionChange.bind(this);
    this.loadCondition = this.loadCondition.bind(this);
  };

  componentWillMount() {
    const { gaugeAlert } = this.props;
    if (gaugeAlert.activeGeofence) {
      this.loadCondition(gaugeAlert);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { gaugeAlert, gaugeAlert: { activeGauge, activeGeofence } } = nextProps;
    const oldActiveGeofence = this.props.gaugeAlert.activeGeofence;
    if ((!oldActiveGeofence && activeGeofence) ||
        oldActiveGeofence && activeGeofence && oldActiveGeofence.id != activeGeofence.id) {
      this.loadCondition(gaugeAlert);
    }
  }

  loadCondition(gaugeAlert) {
    const { activeGauge, activeGeofence } = gaugeAlert;
    const geofenceConditions = gaugeAlert.data[activeGauge.id].geofences.values;
    let geofenceCondition = _.find(geofenceConditions, {geofence_id: activeGeofence.id});

    if (!geofenceCondition) {
      geofenceCondition = {
        geofence_id: activeGeofence.id,
        alert_enter_site: activeGeofence.alert_enter_site,
        alert_exit_site: activeGeofence.alert_exit_site,
      };
    }

    this.setState({ geofenceCondition });
  }

  onOptionChange(e, type) {
    const { geofenceCondition } = this.state;
    const value = e.target.checked;
    geofenceCondition[type] = value;
    this.props.onOptionChange(geofenceCondition);
  }
  render () {
    const { gaugeAlert: { activeGeofence } } = this.props;
    const { geofenceCondition } = this.state;
    if (!activeGeofence) return null;
    return (
      <div className="caGeofenceConfig row">
        <div className="col s6 text-center">
          <div className="caGeofenceConfig__label">
            Alert when asset enters site
          </div>
          <div className="caGeofenceConfig__value">
            <Toggle
              icons={false}
              checked={geofenceCondition.alert_enter_site}
              onChange={(e) => { this.onOptionChange(e, 'alert_enter_site'); }}
            />
          </div>
        </div>
        <div className="col s6 text-center">
          <div className="caGeofenceConfig__label">
            Alert when asset exits site
          </div>
          <div className="caGeofenceConfig__value">
            <Toggle
              icons={false}
              checked={geofenceCondition.alert_exit_site}
              onChange={(e) => { this.onOptionChange(e, 'alert_exit_site'); }}
            />
          </div>
        </div>
      </div>
    );
  }
}

GeofenceConfig.propTypes = {
  gaugeAlert: React.PropTypes.object,
};

export default GeofenceConfig