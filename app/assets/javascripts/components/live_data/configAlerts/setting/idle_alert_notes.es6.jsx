import {Input} from 'react-materialize';

export default class IdleHoursAlert extends React.Component {

  render () {
    return (
      <div className="col s12 m12 alertNotes">
        <Input 
        	name="notes" 
        	type="textarea"
          cols="200"
          onChange={this.props.onChange}
          defaultValue={this.props.value}
        />
      </div>
    );
  }

}