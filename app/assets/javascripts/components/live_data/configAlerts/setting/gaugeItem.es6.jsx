import { connect } from 'react-redux';
import Slider from 'react-slick';
import { AlertTypes } from './AlertTypes.es6.jsx';
import AlertTypesModal from './alert_types_modal.es6.jsx';
import cx from 'classnames';

class GaugeItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "idlehour",
    };
    this.onAlertIemClicked = this.onAlertIemClicked.bind(this);
  }
  render () {
    return (
      <div
        className={cx('gaugeItem', item.state, { 'alertType__item--active': active == item.state })}
        onClick={this.props.onClick}
        >
      </div>
    );
  }
}

GaugeItem.propTypes = {
  data: React.PropTypes.object,
  onClick: React.PropTypes.func,
};

export default GaugeItem
