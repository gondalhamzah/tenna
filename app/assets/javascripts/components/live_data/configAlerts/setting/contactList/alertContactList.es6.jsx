import CompanyContactList from './companyContactList.es6.jsx';
import OtherContactList from './otherContactList.es6.jsx';
import {Input} from 'react-materialize';

export default class AlertContactList extends React.Component {
  constructor(props) {
    super(props);
  }
  render () {
    const { contacts, condition } = this.props;
    const companyContacts = condition.contacts.company;
    const otherContacts = condition.contacts.other;
    const useOtherContact = condition.contacts.useOtherContact ? condition.contacts.useOtherContact : false;
    return (
      <div>
        <div className="row idleAletcompanyContact">
            <div className="col m4 b8 ">
              Company Contacts:
            </div>
            <div className="col m8 s12">
              <CompanyContactList
                onChange={(value) => { this.props.onContactChange(value, 'company'); }}
                contacts={contacts}
                selectedContacts={companyContacts}
              />
            </div>
        </div>
        <div className="row idleAletcompanyContact">
          <div className="col m4 b8">
            <Input 
              name="other_rec"
              type="checkbox"
              className="filled-in"
              checked={useOtherContact}
              onChange={(e) => { this.props.onContactChange(e.target.checked, 'useOtherContact'); }}
              label="Other Recipient"
              labelClassName="boolean cb1"
            />
          </div>
          <div className="col m8 s12">
            {useOtherContact &&
              <OtherContactList
                useOtherContact={useOtherContact}
                selectedContacts={otherContacts}
                onChange={(value) => { this.props.onContactChange(value, 'other'); }}
              />
            }
          </div>
        </div>
      </div>
    );
  }
}