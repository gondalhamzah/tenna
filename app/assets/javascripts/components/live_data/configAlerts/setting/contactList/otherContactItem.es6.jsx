import {Input} from 'react-materialize';
import MaterialIcon from 'components/common/MaterialIcon/index.es6.jsx';

export default class OtherContactItem extends React.Component {
  constructor(props) {
    super(props);
    this.inputTimeout = null;
    this.onOptionChange = this.onOptionChange.bind(this);
  }
  
  onOptionChange(e, name) {
    const { contact } = this.props;
    const value = e.target.value;
    
    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }
    const timeoutId = setTimeout(() => {
      contact[name] = value;
      this.props.onOptionChange(contact);
    }, 200);
    this.inputTimeout = timeoutId;
  }

  render () {
    const { contact } = this.props;
    return (
      <div className="companyContactList">
        <div className="col s12 m12">
          <div className="col s5 m5">
            <Input
              name="email" 
              type="email" 
              className="filled-in"
              label="Email" 
              labelClassName="boolean cb1"
              defaultValue={contact.email}
              onChange={(e) => { this.onOptionChange(e, 'email'); }}
            />
          </div>
          <div className="col s5 m5">
            <div className="">
              <Input 
                name="text" 
                type="text" 
                className="filled-in"
                label="Text" 
                defaultValue={contact.mobile}
                labelClassName="boolean cb1"
                onChange={(e) => { this.onOptionChange(e, 'mobile'); }}
              />
            </div>
          </div>
          <div className="col s2 m2">
            <div className="alertItem__condition alertItem__condition--delete">
              <MaterialIcon name="delete" onClick={this.props.onOptionDelete} />
            </div>
          </div>
        </div>
        <hr/>
      </div>
    );
  }
}