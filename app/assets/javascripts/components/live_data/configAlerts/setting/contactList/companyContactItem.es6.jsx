import {Input} from 'react-materialize';

export default class CompanyContactItem extends React.Component {
  constructor(props) {
    super(props);
  }
  
  onOptionChange(e, name) {
    const { contact } = this.props;
    const checked = e.target.checked;
    contact[name] = checked;
    this.props.onOptionChange(contact);
  }

  render () {
    const { contact } = this.props;
    return (
      <div className="companyContactList">
        <div className="col s12 m12">
          <div className="col s4 m5 l7 b8">
            {contact.name}
          </div>
          <div className="col s8 m7 l5 companyContactList__contactList">
            <div>
              <div className="col s6 m6">
                <Input
                  name="email" 
                  type="checkbox" 
                  className="filled-in"
                  label="Email" 
                  labelClassName="boolean cb1"
                  checked={contact.email}
                  onChange={(e) => { this.onOptionChange(e, 'email'); }}
                />
              </div>
              <div className="col s6 m6">
                <div className="">
                  <Input 
                    name="text" 
                    type="checkbox" 
                    className="filled-in"
                    label="Text" 
                    checked={contact.mobile}
                    labelClassName="boolean cb1"
                    onChange={(e) => { this.onOptionChange(e, 'mobile'); }}
                  />
                </div>
              </div>
            </div>
          </div>
          <hr/>
        </div>
      </div>
    );
  }
}