import CompanyContactItem from './companyContactItem.es6.jsx';
import Select from 'react-select';

export default class CompanyContactList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedValue: [],
      selectedOptions: [],
      options: [],
    };
    this.onSelectChange = this.onSelectChange.bind(this);
    this.onOptionChange = this.onOptionChange.bind(this);      
    this.transformContacts = this.transformContacts.bind(this);      
  }

  componentWillMount() {
    const { contacts } = this.props;
    const options = _.map(contacts, (item) => {
      return {
        label: `${item.first_name} ${item.last_name}`,
        value: item.id,
      };
    });
    this.setState({ options: options });
  }

  onSelectChange (value) {
    const { selectedContacts } = this.props;
    const newSelectedContacts = _.map(value, (item) => {
      let selectedContact = _.find(selectedContacts, { id: item.value });
      if (!selectedContact) {
        selectedContact = { id: item.value, email: false, mobile: false };
      }
      return selectedContact;
    });
    this.props.onChange(newSelectedContacts);
  }

  onOptionChange (contact, index) {
    const { selectedContacts } = this.props;
    const newSelectedContacts = _.map(selectedContacts, (item) => {
      if (item.id === contact.id) {
        item.email = contact.email;
        item.mobile = contact.mobile;
      }
      return item;
    });
    this.props.onChange(newSelectedContacts);
  }

  componentWillReceiveProps(nextProps) {
    const { selectedContacts } = nextProps;
    this.transformContacts(selectedContacts);
  }

  transformContacts(selectedContacts) {
    const { options } = this.state;
    let selectedValue = [];
    let selectedOptions = [];
    if (selectedContacts) {
      selectedValue = _.map(selectedContacts, (item) => {
        return _.find(options, { value: item.id });
      });
      selectedOptions = _.map(selectedContacts, (item) => {
        const selectedItem = _.find(options, { value: item.id });
        item.name = selectedItem.label;
        return item;
      });
      this.setState({ selectedValue: selectedValue, selectedOptions: selectedOptions });
    }
  }

  render () {
    const { options, selectedValue, selectedOptions } = this.state;
    const { contacts, selectedContacts } = this.props;
    return (
      <div>
        <div className="col m12 s12">
          <Select
            multi
            className="select2"
            onChange={this.onSelectChange}
            options={options}
            value={selectedValue}
            placeholder="Select your favourite(s)"
            removeSelected={true}
            rtl={false}
          />
        </div>
        <div className="col m12 companyContact">
          <hr/>
          {selectedOptions.map((item, key) => (
            <CompanyContactItem
              key={key}
              contacts={contacts}
              contact={item}
              onChange={this.props.onChange}
              onOptionChange={(contact) => {
                this.onOptionChange(contact, key);
              }}
            />
          ))}
        </div>
      </div>
    );
  }
}