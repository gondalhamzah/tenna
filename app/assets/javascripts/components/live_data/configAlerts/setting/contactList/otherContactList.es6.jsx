import OtherContactItem from './otherContactItem.es6.jsx';
import Link from 'components/common/Link/index.es6.jsx';

export default class OtherContactList extends React.Component {
  constructor(props) {
    super(props);
    this.onOptionChange = this.onOptionChange.bind(this);
    this.addOption = this.addOption.bind(this);
    this.onOptionDelete = this.onOptionDelete.bind(this);
  }

  onOptionChange(contact, index) {
    const { selectedContacts } = this.props;
    const item = selectedContacts[index];
    
    item.email = contact.email;
    item.mobile = contact.mobile;
    this.props.onChange(selectedContacts);
  }

  onOptionDelete(index) {
    let { selectedContacts } = this.props;
    delete selectedContacts[index];

    if (!selectedContacts) {
      selectedContacts = [];
    }

    this.props.onChange(selectedContacts);
  }

  addOption() {
    let { selectedContacts } = this.props;
    if (!selectedContacts) {
      selectedContacts = []
    }
    selectedContacts.push({ email: '', mobile: '' });
    this.props.onChange(selectedContacts);
  }

  render () {
    const { selectedContacts } = this.props;
    return (
      <div className="row">
        <div className="col m12 s12">
          <Link onClick={this.addOption}>
            New Other Contact
          </Link>
        </div>
        <div className="col m12 companyContact">
          <hr/>
          {selectedContacts && selectedContacts.map((item, key) => (
            <OtherContactItem
              key={key}
              contact={item}
              onOptionChange={(contact) => {
                this.onOptionChange(contact, key);
              }}
              onOptionDelete={() => { this.onOptionDelete(key) }}
            />
          ))}
        </div>
      </div>
    );
  }
}