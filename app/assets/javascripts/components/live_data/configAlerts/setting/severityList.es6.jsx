import { connect } from 'react-redux';
import cx from 'classnames';

const severityList = [
  {
    label: 'H',
    value: 'H',
  },
  {
    label: 'M',
    value: 'M',
  },
  {
    label: 'L',
    value: 'L',
  },
]
class SeverityList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null,
    }
    this.onItemClicked = this.onItemClicked.bind(this);
  }

  onItemClicked(item) {
    const { condition: { severe_level } } = this.props;
    if (item.value !== severe_level) {
      this.props.onChange(item.value);
    }
  }

  render () {
    const { selectedItem } = this.state;
    const { condition } = this.props;
    return (
      <div className="severityList">
        {severityList.map((item, key) => {
          let itemClassName = '';
          if (condition.severe_level === item.value) {
            itemClassName = `severityList__item--${item.value}`;
          }
          return (
            <div
              key={key}
              onClick={() => {this.onItemClicked(item);}}
              className={cx('severityList__item', itemClassName)}
            >
              {item.label}
            </div>
          );
        })}
      </div>
    );
  }
}

SeverityList.propTypes = {
  onChange: React.PropTypes.func,
};

export default SeverityList;
