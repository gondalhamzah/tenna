import { connect } from 'react-redux';

import {toastr} from 'react-redux-toastr'
import { isEqual } from 'lodash';

import { withRouter } from 'react-router-dom';

import GaugeSetting from './gaugeSetting.es6.jsx';
import GeofenceSetting from './geofenceSetting/geofenceSetting.es6.jsx';
import {
  saveGaugeAlertAction,
  getCategoryAssetsAction,
  resetGaugeAlertAction,
  resetFullGaugeAlertAction,
  getGaugeAlertAction
} from 'components/live_data/redux/gaugeAlert';
class AlertSetting extends React.Component {
  constructor(props) {
    super(props);
    this.saveGaugeAlert = this.saveGaugeAlert.bind(this);
    this.cancel = this.cancel.bind(this);
    this.forceSaveGaugeAlert = this.forceSaveGaugeAlert.bind(this);
  }

  cancel() {
    this.props.dispatch(resetFullGaugeAlertAction());
    this.props.history.push({pathname: '/live_data/config', search: ''});
  }

  saveGaugeAlert() {
    const { gaugeAlert: { data, gauges, filters, deletedConditions, deletedGauges, version } } = this.props;
    const payload = {
      data: {
        data: data,
        gauges: gauges,
        deleted_conditions: deletedConditions,
        deleted_gauges: deletedGauges,
        version: version,
      },
      filters: filters,
    };
    
    this.props.dispatch(saveGaugeAlertAction(payload));
  }

  forceSaveGaugeAlert() {
    const { gaugeAlert: { isInitial } } = this.props;
    if (!isInitial) {
      this.saveGaugeAlert();
    }
  }

  componentWillReceiveProps(newProps) {
    const {
      gaugeAlert,
      gaugeAlert: {
        conditionIndex,
        isSaving,
        error,
        filters,
        object,
        isInitial,
      }
    } = newProps;

    const wasSaving = this.props.gaugeAlert.isSaving;
    const isSaved = wasSaving && !isSaving;
    if ( isSaved && !error && filters.category_id) {
      this.props.dispatch(getCategoryAssetsAction({ category_id: filters.category_id }));
    }

    if (isSaved && !error && this.props.gaugeAlert.isInitial) {
      swal(
        'Success!',
        'Alert setting is saved successfully.',
        'success'
      );
    }

    if (isSaved && error) {
      if (error.message === 'Conflict') {
        swal({
          title: 'Data is out of sync.',
          text: "Data on this page was changed after you loaded it. Please reload to the last known settings.",
          type: 'warning',
          confirmButtonText: 'Reload'
        }).then((result) => {
          if (result) {
            this.props.dispatch(getGaugeAlertAction({
              filters,
              object,
            }));
          }
        });
      } else {
        swal(
          'Error',
          'Something went wrong. Please try again later.',
          'error'
        );
      }
    }
  }
  render () {
    const { gaugeAlert, gaugeAlert: { filters, isInitial } } = this.props;
    const isGeofenceGauge = (gaugeAlert.activeGauge && gaugeAlert.activeGauge.name == 'Location');
    return (
      <div>
        { isGeofenceGauge ?
          <GeofenceSetting saveGaugeAlert={this.forceSaveGaugeAlert} />
          :
          <GaugeSetting saveGaugeAlert={this.forceSaveGaugeAlert} />
        }
        { isInitial &&
          <div className="row">
            <a 
              href="#!" 
              className="waves-effect waves-light btn modal-close but1 liveDataAlerts__saveButton"
              onClick={this.saveGaugeAlert}
            >
              Save
            </a>
            <a 
              href="#!" 
              className="modal-action modal-close btn white liveDataAlerts__cancelButton"
              onClick={this.cancel}
            >
            Cancel</a>
          </div>
        }
      </div>
    );
  }
}

AlertSetting.propTypes = {
  gaugeAlert: React.PropTypes.object,
};

const mapStateToProps = (state) => ({
  gaugeAlert: state.gaugeAlert,
})


export default withRouter(connect(mapStateToProps)(AlertSetting));