import { connect } from 'react-redux';
import Select from 'react-select';
import cx from 'classnames';
import FaIcon from 'components/common/FaIcon/index.es6.jsx';
import qs from 'qs';
import { withRouter } from 'react-router-dom';
import Link from 'components/common/Link/index.es6.jsx';
class FilterSearch extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: null,
    }
    this.onSelectChange = this.onSelectChange.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onValueClick = this.onValueClick.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.onLinkClicked = this.onLinkClicked.bind(this);
    this.onResetClicked = this.onResetClicked.bind(this);
    this.getAssetGaugeAlert = this.getAssetGaugeAlert.bind(this);
  }

  onSelectChange(value) {
    this.setState({ value });
    if (value) {
      this.getAssetGaugeAlert(value);
    }
  }

  getAssetGaugeAlert(asset) {
    const { category, group } = this.props;
    const filters = {
      category_group_id: asset.category_group_id,
      category_id: asset.category_id,
      asset_id: asset.id,
    };
    // this.props.getGaugeAlert({
    //   filters,
    //   object: asset,
    // });
    this.props.history.push({pathname: '/live_data/config', search: `?${qs.stringify(filters, {strictNullHandling: true})}`});
  }

  onInputChange(value) {
    const { objectId, type } = this.props;
    const filter = {
      query: value,
    };
    // this.setState({
    //   value,
    // })
    const filter_name = `${type}_id`;
    filter[filter_name] = objectId;
    this.props.filterAlertAssets(filter);
  }

  onFocus(value) {
    const { objectId, type } = this.props;
    const filter = {
      query: '',
    };
    const filter_name = `${type}_id`;
    filter[filter_name] = objectId;
    this.props.filterAlertAssets(filter);
  }

  onValueClick(value, e) {

  }

  onResetClicked(asset) {
    this.props.resetAssetAlert(asset);
  }

  onLinkClicked(asset) {
    this.getAssetGaugeAlert(asset);
  }

  render () {
    const { value } = this.state;
    const { alertAssets, categoryAssets, filters, type, className } = this.props;
    const isCategorySearch = type === 'category';
    return (
      <div className={cx('caFilterSearch', className)}>
        <div className="caFilterSearch__searchWrapper">
          <FaIcon
            className="caFilterSearch__searchToggle"
            name="chevron-right"
          />
          <Select
            className="caFilterSearch__search"
            complete
            placeholder="Search"
            onInputChange={this.onInputChange}
            onChange={this.onSelectChange}
            onFocus={this.onFocus}
            options={alertAssets}
            name="form-field-name"
            value={value}
          />
        </div>
        <div className="caFilterSearch__assets">
          {isCategorySearch && categoryAssets && categoryAssets.map((asset, key) => (
            <div
              className="caFilterSearch__item"
              key={key}
              className={cx('caFilterSearch__item', { 'caFilterSearch__item--active': filters && asset.id == filters.asset_id })}
            >
              <FaIcon
                className="caFilterSearch__itemToggle"
                name="chevron-right"
              />
              <Link
                onClick={() => { this.onLinkClicked(asset); }}
                className={cx('caFilterSearch__itemLink', { 'caFilterSearch__itemLink--active': filters && asset.id == filters.asset_id })}
              >
                { asset.title }
              </Link>
              <FaIcon
                className="caFilterSearch__editIcon"
                name="bell"
                onClick={() => { this.onLinkClicked(asset); }}
              />
            </div>
          ))}
        </div>
      </div>
    );
  }
}
FilterSearch.propTypes = {
  type: React.PropTypes.string,
  objectId: React.PropTypes.number,
  alertAssets: React.PropTypes.array,
  categoryAssets: React.PropTypes.array,
  filters: React.PropTypes.object,
};

export default withRouter(FilterSearch);
