import { connect } from 'react-redux';
import cx from 'classnames';
import qs from 'qs';
import { withRouter } from 'react-router-dom';
import FilterSearch from './filterSearch.es6.jsx';
import FaIcon from 'components/common/FaIcon/index.es6.jsx';
import MaterialIcon from 'components/common/MaterialIcon/index.es6.jsx';
import Link from 'components/common/Link/index.es6.jsx';

class CaFilterCategory extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      itemName: this.props.itemName,
      showSubNav: false,
    };
    this.onClickEdit = this.onClickEdit.bind(this);
    this.onItemClicked = this.onItemClicked.bind(this);
  }

  componentWillMount() {
    const { isOpened, data } = this.props;

    if (data && isOpened) {
      this.props.getCategoryAssets({ category_id: data.id });
    }
  }

  onItemClicked() {
    const { data } = this.props;
    this.props.onSelectItem(data);
  }

  componentWillReceiveProps(nextProps) {
    const { isOpened, data } = this.props;

    if (data && !isOpened && nextProps.isOpened) {
      this.props.getCategoryAssets({ category_id: data.id });
    }
  }

  onClickEdit() {
    const { data, group } = this.props;
    const filters = {
      category_group_id: group.id,
      category_id: data.id,
      asset_id: null,
    };
    // this.props.getGaugeAlert({
    //   filters,
    //   object: data,
    // });
    this.props.history.push({pathname: '/live_data/config', search: `?${qs.stringify(filters, {strictNullHandling: true})}`});
  }

  render () {
    const { data, isOpened, group, alertAssets, categoryAssets, filters } = this.props;
    const isEditing = filters &&
                        filters.category_id == data.id &&
                        filters.asset_id == null;
    return (
      <div className="caFilterCategory">
        <div
          className={cx(
            'caFilterCategory__item dd1',
            { 'caFilterCategory__item--active': isEditing },
          )}
        >
          <FaIcon
            className="caFilterCategory__addIcon"
            name={isOpened ? 'chevron-down' : 'chevron-right'}
            onClick={this.onItemClicked}
          />
          <Link
            onClick={this.onItemClicked}
            className={cx(
              'caFilterCategory__itemLink',
              { 'caFilterCategory__itemLink--active': isEditing },
              { 'caFilterCategory__itemLink--open': isOpened },
            )}
          >
            { data.name }
          </Link>
          <FaIcon
            className="caFilterCategory__removeIcon"
            name="bell"
            onClick={this.onClickEdit}
          />
        </div>
        {isOpened &&
          <FilterSearch
            type="category"
            category={data}
            group={group}
            objectId={data.id}
            filters={filters}
            alertAssets={alertAssets}
            categoryAssets={categoryAssets}
            getGaugeAlert={this.props.getGaugeAlert}
            filterAlertAssets={this.props.filterAlertAssets}
            resetAssetAlert={this.props.resetAssetAlert}
          />
        }
      </div>
    );
  }
}

CaFilterCategory.propTypes = {
  isOpened: React.PropTypes.bool,
  data: React.PropTypes.object,
  filters: React.PropTypes.object,
  alertAssets: React.PropTypes.array,
  categoryAssets: React.PropTypes.array,
};

export default withRouter(CaFilterCategory);
