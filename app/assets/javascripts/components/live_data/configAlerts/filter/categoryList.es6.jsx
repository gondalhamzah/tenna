import Category from './category.es6.jsx';
import cx from 'classnames';

class CaFilterCategoryList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null,
    };
    this.onSelectItem = this.onSelectItem.bind(this);
    this.openCategory = this.openCategory.bind(this);
  }

  componentWillMount() {
    const { filters } = this.props;
    if (filters) {
      this.openCategory(filters);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { filters } = nextProps;
    const oldFilters = this.props.filters;
    if ((!oldFilters && filters) ||
        (oldFilters && filters && oldFilters.asset_id != filters.asset_id)) {
      this.openCategory(filters);
    }
  }

  openCategory(filters) {
    const { selectedItem } = this.state;
    if (filters.category_id && selectedItem != filters.category_id) {
      this.setState({ selectedItem: filters.category_id })
    }
  }

  onSelectItem(item) {
    const { selectedItem } = this.state;
    if (selectedItem == item.id) {
      this.setState({ selectedItem: null });
    } else {
      this.setState({ selectedItem: item.id });
    }
  }

  render () {
    const { categories, group, filters, alertAssets, categoryAssets } = this.props;
    const { selectedItem } = this.state;
    return (
      <div>
        {categories && categories.map((item, key) => (
          <Category
            key={key}
            data={item}
            group={group}
            isOpened={selectedItem == item.id}
            onSelectItem={this.onSelectItem}
            filters={filters}
            alertAssets={alertAssets}
            categoryAssets={categoryAssets}
            // getGaugeAlert={this.props.getGaugeAlert}
            getCategoryAssets={this.props.getCategoryAssets}
            filterAlertAssets={this.props.filterAlertAssets}
            resetAssetAlert={this.props.resetAssetAlert}
          />
        ))}
      </div>
    );
  }
}

CaFilterCategoryList.propTypes = {
  categories: React.PropTypes.array,
  group: React.PropTypes.object,
  filters: React.PropTypes.object,
  alertAssets: React.PropTypes.array,
  categoryAssets: React.PropTypes.array,
};

export default CaFilterCategoryList;
