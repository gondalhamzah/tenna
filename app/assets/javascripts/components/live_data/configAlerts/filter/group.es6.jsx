import { connect } from 'react-redux';
import cx from 'classnames';
import qs from 'qs';
import { withRouter } from 'react-router-dom';
import FilterSearch from './filterSearch.es6.jsx';
import CategoryList from './categoryList.es6.jsx';
import FaIcon from 'components/common/FaIcon/index.es6.jsx';
class CaFilterGroup extends React.Component {
  constructor(props) {
    super(props);
    this.onItemClicked = this.onItemClicked.bind(this);
    this.onClickEdit = this.onClickEdit.bind(this);
  }

  onItemClicked() {
    const { data } = this.props;
    this.props.onSelectItem(data);
  }

  onClickEdit() {
    const { data } = this.props;
    // console.log(data);
    const filters = {
      category_group_id: data.id,
      category_id: null,
      asset_id: null,
    };
    this.props.history.push({pathname: '/live_data/config', search: `?${qs.stringify(filters, {strictNullHandling: true})}`});
  }

  render () {
    const { isOpened, categories, data, filters, alertAssets, categoryAssets } = this.props;
    const isEditing = filters &&
                        filters.category_group_id == data.id &&
                        !filters.category_id &&
                        !filters.asset_id;
    return (
      <div className="caFilterGroup">
        <div
          className="caFilterGroup__item b32"
          className={cx(
            'caFilterGroup__item b32',
            { 'caFilterGroup__item--active': isEditing },
          )}
        >
          <FaIcon
            className="caFilterGroup__addIcon"
            name={isOpened ? 'chevron-down' : 'chevron-right'}
            onClick={this.onItemClicked}
          />
          <a
            className={cx('caFilterGroup__itemLink', { 'caFilterGroup__itemLink--active': isEditing })}
            href="#"
            onClick={this.onItemClicked}
          >
            { data.name }
          </a>
          <FaIcon
            className="caFilterGroup__editIcon"
            name="bell"
            onClick={this.onClickEdit}
          />
        </div>
        {isOpened == true &&
          <div className="caFilterGroup__category">
            {/* <FilterSearch
              type="category_group"
              group={data}
              objectId={data.id}
              filters={filters}
              alertAssets={alertAssets}
              categoryAssets={categoryAssets}
              getGaugeAlert={this.props.getGaugeAlert}
              filterAlertAssets={this.props.filterAlertAssets}
            /> */}
            <CategoryList
              categories={categories}
              group={data}
              filters={filters}
              alertAssets={alertAssets}
              categoryAssets={categoryAssets}
              // getGaugeAlert={this.props.getGaugeAlert}
              getCategoryAssets={this.props.getCategoryAssets}
              filterAlertAssets={this.props.filterAlertAssets}
              resetAssetAlert={this.props.resetAssetAlert}
            />
          </div>
        }
      </div>
    );
  }
}
CaFilterGroup.propTypes = {
  isOpened: React.PropTypes.bool,
  categories: React.PropTypes.array,
  data: React.PropTypes.object,
  filters: React.PropTypes.object,
  alertAssets: React.PropTypes.array,
  categoryAssets: React.PropTypes.array,
};

export default withRouter(CaFilterGroup);
