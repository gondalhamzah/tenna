import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import FilterSearch from './filterSearch.es6.jsx';
import GroupItem from './group.es6.jsx';
import qs from 'qs';
import { filterAlertAssetsAction, clearAlertAssetsAction } from 'components/live_data/redux/alertAssets';
import { getGaugeAlertAction, getCategoryAssetsAction, saveGaugeAlertAction } from 'components/live_data/redux/gaugeAlert';

class CaFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null,
    };
    this.onSelectItem = this.onSelectItem.bind(this);
    this.getGaugeAlert = this.getGaugeAlert.bind(this);
    this.getCategoryAssets = this.getCategoryAssets.bind(this);
    this.filterAlertAssets = this.filterAlertAssets.bind(this);
    this.clearAlertAssets = this.clearAlertAssets.bind(this);
    this.openGroup = this.openGroup.bind(this);
    this.resetAssetAlert = this.resetAssetAlert.bind(this);
  }

  componentWillMount() {
    const { filters } = this.props;
    this.getGaugeAlert(filters);
    this.openGroup(filters);
  }

  onSelectItem(item) {
    const { selectedItem } = this.state;
    if (selectedItem == item.id) {
      this.setState({ selectedItem: null });
    } else {
      this.setState({ selectedItem: item.id });
    }
  }

  getGaugeAlert(filters) {
    const payload = {filters};
    if (filters.asset_id) {
      payload['object'] = _.find(this.props.assets, {assetId: parseInt(filters.asset_id)});
    } else if (filters.category_id) {
      const group = this.props.categories[parseInt(filters.category_group_id)];
      payload['object'] = _.find(group, {id: parseInt(filters.category_id)});
    } else if (filters.category_group_id) {
      payload['object'] = _.find(this.props.categoryGroups, {id:parseInt(filters.category_group_id)});
    } else {
      return;
    }
    this.props.dispatch(getGaugeAlertAction(payload));
  }

  getCategoryAssets(payload) {
    this.props.dispatch(getCategoryAssetsAction(payload));
  }

  filterAlertAssets(payload) {
    this.props.dispatch(filterAlertAssetsAction(payload));
  }

  clearAlertAssets(payload) {
    this.props.dispatch(clearAlertAssetsAction(payload));
  }

  resetAssetAlert(asset) {
    const { gaugeAlert: { data, gauges, filters, deletedConditions, deletedGauges, version } } = this.props;
    const payload = {
      data: {
        data: {},
        gauges: [],
        deleted_conditions: [],
        deleted_gauges: [],
        deleted_assets: [asset.id],
        version: version,
      },
      filters: {
        category_group_id: asset.category_group_id,
        category_id: asset.category_id,
        asset_id: asset.id,
      },
    };

    this.props.dispatch(saveGaugeAlertAction(payload));
    setTimeout(() => {
      this.props.dispatch(getCategoryAssetsAction({ category_id: asset.category_id }));
    }, 100);
  }

  componentWillReceiveProps(nextProps) {
    const { filters } = nextProps;
    const oldFilters = this.props.filters;
    if ((!oldFilters && filters) ||
        (oldFilters && filters && oldFilters.asset_id != filters.asset_id)) {
      this.openGroup(filters);
    }

    if ((!oldFilters && filters) ||
      (oldFilters && filters && (
        oldFilters.asset_id != filters.asset_id ||
        oldFilters.category_group_id != filters.category_group_id ||
        oldFilters.category_id != filters.category_id
      )
      )) {
        this.getGaugeAlert(filters);
    }

  }

  openGroup(filters) {
    const { selectedItem } = this.state;
    if (filters.category_group_id && selectedItem != filters.category_group_id) {
      this.setState({ selectedItem: filters.category_group_id })
    }
  }

  render () {
    const { categoryGroups, categories, categoryAssets, alertAssets, filters } = this.props;
    // const filters = qs.parse(this.props.location.search, {ignoreQueryPrefix: true});
    const { selectedItem } = this.state;
    return (
      <div className="caFilter">
        <div className="caFilter__globalSearch">
          <FilterSearch
            className="global"
            type="global"
            filters={filters}
            alertAssets={alertAssets}
            getGaugeAlert={this.getGaugeAlert}
            filterAlertAssets={this.filterAlertAssets}
          />
        </div>
        {categoryGroups.map((item, key) => (
          <GroupItem
            key={key}
            data={item}
            categories={categories[item.id]}
            isOpened={selectedItem == item.id}
            onSelectItem={this.onSelectItem}
            alertAssets={alertAssets}
            categoryAssets={categoryAssets}
            filters={filters}
            // getGaugeAlert={this.getGaugeAlert}
            getCategoryAssets={this.getCategoryAssets}
            filterAlertAssets={this.filterAlertAssets}
            resetAssetAlert={this.resetAssetAlert}
          />
        ))}
      </div>
    );
  }
}

CaFilter.propTypes = {
  categoryGroups: React.PropTypes.array,
  categories: React.PropTypes.object,
  alertAssets: React.PropTypes.array,
  categoryAssets: React.PropTypes.array,
  gaugeAlert: React.PropTypes.object,
  filters: React.PropTypes.object,
};

const mapStateToProps = (state, props) => ({
  categoryGroups: state.categoryGroups.data,
  assets: state.assets.data,
  categories: state.categories.data,
  alertAssets: state.alertAssets.data,
  gaugeAlert: state.gaugeAlert,
  categoryAssets: state.gaugeAlert.categoryAssets,
  filters: qs.parse(props.location.search, {ignoreQueryPrefix: true, strictNullHandling: true})
})

export default withRouter(connect(mapStateToProps)(CaFilter));
