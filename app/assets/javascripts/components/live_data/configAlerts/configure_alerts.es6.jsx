import Filter from './filter/filter.es6.jsx';
import ConfigAlertSetting from './setting/config_alerts_setting.es6.jsx';

export default class ConfigureAlerts extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedNavBar: "Attachments",
    };
    this.getSelectedNav = this.getSelectedNav.bind(this);
  }

  getSelectedNav(item) {
    this.setState({ selectedNavBar: item });
  }

  render () {
    const { selectedNavBar } = this.state;
    return (
      <div className="row">
        <div className="col l4 m12 s12">
          <Filter navBarItem = {this.getSelectedNav}/>
        </div>
        <div className="col l8 m12 s12">
          <ConfigAlertSetting itemName={selectedNavBar}/>
        </div>
      </div>
    );
  }
}


