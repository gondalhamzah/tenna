// import GoogleMap from "react-google-map"
// import GoogleMapLoader from "react-google-maps-loader"
import { connect } from 'react-redux';

import fetch from "isomorphic-fetch";
import { compose, withProps, withStateHandlers } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  Rectangle,
  InfoWindow,
} from "react-google-maps";

// import MarkerClusterer from 'react-google-maps/lib/components/addons/MarkerClusterer';

import { MAP, MARKER, INFO_WINDOW, MARKER_CLUSTERER } from 'react-google-maps/lib/constants';

import { setBoundsAction } from '../redux/assets';
import GeoFenceDrawingView from '../geoFence/drawing.es6.jsx';
import GeoFenceOverlay from '../geoFence/overlay.es6.jsx';

const getIcons = (info) => {
  const tracking_code = info.tracking_code ? info.tracking_code.toLowerCase() : 'project';
  return markerIcons[tracking_code];
}

const latestMsgs = {};
const getInfoWindowContent = (assetData) => {
  return `
    <div class="liveDataMap__iwContainer">
      <div class="photo_wrapper">
        <img src="${assetData.img_url}"/>
      </div>
      <div class="liveDataMap__iwInfoSection">
        <div class="liveDataMap__iwTitle">
          <a href="/live_data/assets/${assetData.assetId}">
            ${assetData.title}
          </a>
        </div>
        <strong>${latestMsgs[assetData.assetId]}</strong>
        <div class="liveDataMap__iwLastUpdate">
          <div>Last Update:</div>
          <div>${moment(assetData.lastUpdate).format('MM/DD/YYYY hh:mm A')}</div>
        </div>
        <a
          class="liveDataMap__iwMoreInfo"
          href="/live_data/assets/${assetData.assetId}"
        >
          Details <i class="material-icons liveDataMap__iwMoreInfoIcon">keyboard_arrow_right</i>
        </a>
      </div>
    </div>
  `;
}

const MY_API_KEY = "AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0" // fake

class LiveDataDashboardMap extends React.Component {
  constructor(props) {
    super(props);
    this.buildMarkerData = this.buildMarkerData.bind(this);

    this.onClusterMouseOver = this.onClusterMouseOver.bind(this);
    this.onClusterMouseOut = this.onClusterMouseOut.bind(this);
    this.getClusterStyle = this.getClusterStyle.bind(this);

    this.setMapInstance = this.setMapInstance.bind(this);
    this.onMapTypeIdChanged = this.onMapTypeIdChanged.bind(this);
    this.onMapBoundsChanged = this.onMapBoundsChanged.bind(this);
    this.setMapCenter = this.setMapCenter.bind(this);

    this.showOnMap = this.showOnMap.bind(this);
    this.resizeMap = this.resizeMap.bind(this);
    this.onToggleInfoWindow = this.onToggleInfoWindow.bind(this);

    this.setMarkerInstance = this.setMarkerInstance.bind(this);
    this.onMarkerMouseOver = this.onMarkerMouseOver.bind(this);
    this.onMarkerMouseOut = this.onMarkerMouseOut.bind(this);
    this.onMarkerClicked = this.onMarkerClicked.bind(this);
    this.buildMarkers = this.buildMarkers.bind(this);

    this.setInfoWindowInstance = this.setInfoWindowInstance.bind(this);


    this.state = {
      markers: {},
      markerData: [],
      coordinates: [],
      isOpen: false,
      mapTypeId: google.maps.MapTypeId.TERRAIN,
    };
    this.infoWindow = null;
    this.markers = {};
    this.gmMarkers = {};
    this.map = null;
    this.oms = null;
    this.gmMarkerCluster = null;
  }

  componentWillMount() {
    const { geoData, notificationData } = this.props;
    this.buildMarkerData(geoData, notificationData);
    this.mapBoundsTimeout = 0;
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeMap);
  }

  componentWillReceiveProps(nextProps) {
    const { geoData, notificationData, selectedAsset, newNotification, showFilter } = nextProps;
    if (this.props.geoData.length !== geoData.length) {
      this.buildMarkerData(geoData, notificationData, true);
    }

    // if location chagne detected...
    if (this.props.geoData.length == geoData.length) {
      let flag = false;
      for(var i=0; i<geoData.length; i++) {
        const newAsset = geoData[i];
        const oldAsset = this.props.geoData[i];
        if (newAsset.geo[0] !== oldAsset.geo[0] || newAsset.geo[1] !== oldAsset.geo[1]) {

          const gmMarker = this.gmMarkers[newAsset.assetId];
          if (!gmMarker._updated) {
            gmMarker.currentIcon = gmMarker.icons.updateIcon;
          } else {
            gmMarker.currentIcon = gmMarker.icons.hoverIcon;
          }

          gmMarker.setIcon(gmMarker.currentIcon);
          gmMarker.setPosition(new google.maps.LatLng(newAsset.geo[0], newAsset.geo[1]));
          gmMarker._location = true;
          flag = true;
        }
      }

      if (flag) {
        this.refreshClusters();
      }
    }

    const notification = this.props.newNotification;
    if (newNotification.asset_id !== notification.asset_id || newNotification.msg !== notification.msg) {
      latestMsgs[newNotification.asset_id] = newNotification.msg;
      const gmMarker = this.gmMarkers[newNotification.asset_id];
      setTimeout(() => {
        this.onMarkerClicked(gmMarker, newNotification.asset_id);
      }, 0);
    }
    if (showFilter !== this.props.showFilter) {
      this.resizeMap();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { selectedAsset, notificationData, newNotification, showFilter, selectAssetClickCount,
      geoFenceNavSelected,  drawingActive} = nextProps;
    if (showFilter !== this.props.showFilter) {
      return false;
    } else if (selectedAsset !== this.props.selectedAsset || selectAssetClickCount !== this.props.selectAssetClickCount) {
      this.showOnMap(nextProps.selectedAsset, true);
      return false;
    } else if (this.props.newNotification.asset_id !== newNotification.asset_id) {
      this.handleNewNotification(newNotification.asset_id);
      this.refreshClusters();
      return false;
    }

    if (this.props.drawingActive !== drawingActive || this.props.geoFenceNavSelected !== geoFenceNavSelected) {
      return true;
    }

    return false;
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeMap);
  }

  resizeMap() {
    google.maps.event.trigger(this.gmMap, "resize");
  }

  setMapInstance(instance) {
    if (instance) {
      if (!this.map) {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(this.setMapCenter);
        }
      }

      this.map = instance;
      this.gmMap = instance.context[MAP];
      this.buildMarkers();
    }
  }

  setMapCenter(position) {
    const pos = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    };

    this.gmMap.setCenter(pos);
  }

  setInfoWindowInstance(instance) {
    if (instance) {
      this.infoWindow = instance;
      this.gmInfoWindow = instance.state[INFO_WINDOW];
      this.gmInfoWindow.close();
    }
  }

  onMapBoundsChanged(e) {
    if (this.gmMap) {
      const bounds = this.gmMap.getBounds();
      if (this.mapBoundsTimeout) {
        clearTimeout(this.mapBoundsTimeout);
      }
      const timeoutId = setTimeout(() => {
        this.props.dispatch(setBoundsAction(bounds));
      }, 200);

      this.mapBoundsTimeout = timeoutId;
    }
  }

  setMarkerInstance(instance, assetId) {
    if (instance) {
      this.markers[assetId] = instance;
      this.gmMarkers[assetId] = instance.state[MARKER];
    }
  }

  buildMarkerData(geoData, notificationData, shouldUpdate = false) {
    console.log('buildMarkerData...');
    const mapTypeId = (this.gmMap ? this.gmMap.mapTypeId : 'terrain') ;
    const markerData = _.map(geoData, (item) => {
      item.icons = getIcons(item);
      const notifIndex = _.findIndex(notificationData, (o) => { return o.asset_id === item.assetId; });
      item.animation = null;
      if (notifIndex > -1) {
        item.hasNotification = true;
        item.animation = google.maps.Animation.BOUNCE;
        item.currentIcon = item.icons.updateIcon;
      } else if (mapTypeId == 'satellite' || mapTypeId == 'hybrid') {
        item.currentIcon = item.icons.satelliteIcon;
      } else {
        item.currentIcon = item.icons.originalIcon;
      }
      return item;
    });
    this.setState({ markerData: markerData }, ()=>{
      if (shouldUpdate) {
        this.buildMarkers();
      }
    });
  }

  clustorCalculator(markers, numStyles) {
    const hasUpdatedMarker = _.filter(markers, {_updated: true}).length > 0;
    const hasLocationMarker = _.filter(markers, {_location: true}).length > 0;
    return {text: markers.length, index: hasUpdatedMarker ? 2 : (hasLocationMarker ? 3 : 1)};
  }

  refreshClusters() {
    console.log('refreshClusters...');

    if (this.gmMarkerCluster) {
      this.gmMarkerCluster.setMap(null);
    }

    this.gmMarkerCluster = new MarkerClusterer(
      this.gmMap,
      _.map(this.gmMarkers, (m)=>m),
      {styles: this.getClusterStyle(), maxZoom: 17}
    );
    this.gmMarkerCluster.setCalculator(this.clustorCalculator.bind(this));
  }

  buildMarkers() {
    console.log('buildMarkers...');
    const { markerData } = this.state;
    let markers = [];
    if (this.oms) {
      this.oms.clearMarkers();
    }

    this.oms = new OverlappingMarkerSpiderfier(this.gmMap, {
      markersWontMove: true,
      markersWontHide: true,
      basicFormatEvents: true,
      circleFootSeparation: 46,
      spiralFootSeparation: 52,
    });

    if (this.gmMarkerCluster) {
      this.gmMarkerCluster.setMap(null);
    }

    this.infoWindow = new google.maps.InfoWindow({
      content: ''
    });

    _.each(this.gmMarkers, (gmMarker, key) => {
      gmMarker.setMap(null);
    })
    this.gmMarkers = {};

    _.each(markerData, (item) => {
      const finalLatLng = new google.maps.LatLng(item.geo[0], item.geo[1]);
      const gmMarker = new google.maps.Marker({
        position: finalLatLng,
        icon: item.currentIcon,
        animation: item.animation,
        currentIcon: item.currentIcon,
        icons: item.icons,
        map: this.gmMap,
        title: item.title
      });

      if (item.hasNotification) {
        gmMarker._updated = true;
      }

      google.maps.event.addListener(gmMarker, 'spider_click', (e) => { this.onMarkerClicked(gmMarker, item.assetId) });
      google.maps.event.addListener(gmMarker, 'mouseover', (e) => { this.onMarkerMouseOver(gmMarker) });
      google.maps.event.addListener(gmMarker, 'mouseout', (e) => { this.onMarkerMouseOut(gmMarker) });
      this.gmMarkers[item.assetId] = gmMarker;
      markers.push(gmMarker)
      this.oms.addMarker(gmMarker);
    });

    this.gmMarkerCluster = new MarkerClusterer(this.gmMap, markers,
            {styles: this.getClusterStyle(), maxZoom: 17});
    this.gmMarkerCluster.setCalculator(this.clustorCalculator.bind(this));

}

  onToggleInfoWindow(markerId) {
    this.setState({ selectedMarkerId: markerId });
  }
  onClusterMouseOut(e) {
    e.markerClusterer_.setStyles(this.getClusterStyle(true));
  }
  onClusterMouseOver(e) {
    e.markerClusterer_.setStyles(this.getClusterStyle());
  }

  onMarkerClicked(gmMarker, assetId) {
    const { markerData } = this.state;
    const data = _.find(markerData, (o) => { return o.assetId == assetId; });
    const dataIndex = _.findIndex(markerData, (o) => { return o.assetId == assetId; });
    if (data) {
      const content = getInfoWindowContent(data);
      this.infoWindow.close();
      this.infoWindow.setContent(content);
      this.infoWindow.open(this.gmMap, gmMarker);
    }
  }

  onMarkerMouseOver(gmMarker) {
    gmMarker.setIcon(gmMarker.icons.hoverIcon);
  }

  onMarkerMouseOut(gmMarker) {
    gmMarker.setIcon(gmMarker.currentIcon);
  }

  getClusterStyle(isMouseOver = false) {
    const mapTypeId = (this.gmMap ? this.gmMap.mapTypeId : 'terrain') ;
    const markerPrefix = 'static-assets/live_data/marker-group';
    let iconIndex = 'originalIcon';
    const isSatelliteType = mapTypeId == 'satellite' || mapTypeId == 'hybrid';
    if (isSatelliteType) {
      iconIndex = 'satelliteIcon';
    }
    const styles = [
      {
        url: markerIcons.group[iconIndex],
        height: 53,
        width: 53,
        anchor: [3, 0],
        textColor: (isSatelliteType ? '#000000' : '#ffffff'),
        textSize: 20
      },
      {
        url: markerIcons.group['stateHoverIcon'],
        height: 53,
        width: 53,
        anchor: [3, 0],
        textColor: (isSatelliteType ? '#000000' : '#ffffff'),
        textSize: 20
      },
      {
        url: markerIcons.group['updateIcon'],
        height: 53,
        width: 53,
        anchor: [3, 0],
        textColor: (isSatelliteType ? '#000000' : '#ffffff'),
        textSize: 20
      }
    ]
    return styles;
  }

  onMapTypeIdChanged(e) {
    const { geoData, notificationData } = this.props;
    this.buildMarkerData(geoData, notificationData);
    this.buildMarkers();
  }

  showOnMap(assetId, moveCenter = false) {
    const gmMarker = this.gmMarkers[assetId];
    if (moveCenter) {
      this.gmMap.setZoom(18);
      this.gmMap.setCenter(gmMarker.getPosition());
    }
    setTimeout(() => {
      this.onMarkerClicked(gmMarker, assetId);
    }, 0);
  }

  handleNewNotification(assetId) {
    const gmMarker = this.gmMarkers[assetId];
    if (gmMarker) {
      // this.gmMap.setZoom(16);
      // this.gmMap.setCenter(gmMarker.getPosition());
      gmMarker.currentIcon = gmMarker.icons.updateIcon;
      gmMarker.setIcon(gmMarker.currentIcon);
      gmMarker._updated = true;
    }
  }

  // GoogleMap component has a 100% height style.
  // You have to set the DOM parent height.
  // So you can perfectly handle responsive with differents heights.
  render () {
    const { geoData } = this.props;
    const { selectedMarkerId, mapTypeId, markerData } = this.state;

    return (
      <GoogleMap
        defaultZoom={3}
        defaultCenter={{ lat: 38.8422822, lng: -102.2509999 }}
        mapTypeId="terrain"
        ref={(instance) => { this.setMapInstance(instance); }}
        componentDidMount={this.mapMounted}
        onMapTypeIdChanged={this.onMapTypeIdChanged}
        onBoundsChanged={this.onMapBoundsChanged}
      >

        {this.props.drawingActive && this.props.geoFenceNavSelected && <GeoFenceDrawingView />}
        {this.props.geoFenceNavSelected && !this.props.drawingActive &&
          <GeoFenceOverlay map={this.gmMap} />
        }
      </GoogleMap>
    );
  }
}

LiveDataDashboardMap.propTypes = {
  notificationData: React.PropTypes.array,
  geoData: React.PropTypes.array,
  selectedAsset: React.PropTypes.number,
  newNotification: React.PropTypes.object,
}

const mapStateToProps = (state) => ({
  geoData: state.assets.data,
  selectedAsset: state.assets.selectedAsset,
  selectAssetClickCount: state.assets.selectAssetClickCount,
  showFilter: state.assets.showFilter,
  notificationData: state.notifications.data,
  drawingActive: state.geofences.drawingActive,
  geoFenceNavSelected: state.geofences.navSelected,
  newNotification: state.notifications.newNotification || {asset_id: null},
})

export default compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${MY_API_KEY}&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div id="map" style={{width: '100%', height: '500px'}} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap,
  connect(mapStateToProps),
)(LiveDataDashboardMap);

