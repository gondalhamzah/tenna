import cx from 'classnames';

class SliderPagination extends React.Component {
  render() {
    const { total, perPage, activePage } = this.props;
    const totalPages = Math.ceil(parseFloat(total) / perPage);
    const pages = [];
    for (var i = 1; i <= totalPages; i ++) {
      pages.push(i);
    }
    return (
      <ul className="sliderPagination">
        {pages.map(page => (
          <li
            className={
              cx(
                "sliderPagination__item", 
                {'sliderPagination__item--active': activePage === page}
              )
            }
            onClick={() => this.props.onPageChange(page)}
          >
            <button>{page}</button>
          </li>
        ))}
        
      </ul>
    )
  }
}


SliderPagination.propTypes = {
  total: React.PropTypes.number,
  perPage: React.PropTypes.number,
  activePage: React.PropTypes.number,
};

export default SliderPagination;
