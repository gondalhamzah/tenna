import Select from 'react-select';
import { CSVLink } from 'react-csv';
import { connect } from 'react-redux';
import cx from 'classnames';
import LiveDataNotificationModal from './notification_modal.es6.jsx';
import { Button, Card, Row, Col, Modal, Input } from 'react-materialize';

const severityOptions = [
  { label: 'All Severity Levels', value: '' },
  { label: 'High', value: 'high' },
  { label: 'Medium', value: 'medium' },
  { label: 'Low', value: 'low' },
]
class LiveDataNotificationList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      siteOptions: [],
      typeOptions: [],
      selectedSeverity: severityOptions[0],
      selectedSite: null,
      selectedType: null,
      confirmKey: null,
      showModal: false
    }
    this.setFilterData = this.setFilterData.bind(this);
    this.confirmDismissNotification = this.confirmDismissNotification.bind(this);
    this.dismissNotification = this.dismissNotification.bind(this);
    this.showModal = this.showModal.bind(this);
  }

  componentWillMount() {
    const { data } = this.props;
    this.setFilterData(data);
  }

  componentDidMount() {
    this.props.setNotificationNumber();
  }

  componentDidUpdate() {
    this.props.setNotificationNumber();
  }

  componentWillReceiveProps(nextProps) {
    const { data } = nextProps;
  }

  setFilterData(data) {
    let siteOptions, typeOptions;
    typeOptions = [];
    siteOptions = [];
    _.each(this.props.sitesData, (site) => {
      siteOptions.push({ label: site[0], value: site[1] })
    });
    _.each(this.props.gaugesData, (gauge) => {
      typeOptions.push({ label: gauge.name, value: gauge.name })
    });
    this.setState({ typeOptions: typeOptions, siteOptions: siteOptions });
  }

  confirmDismissNotification(key) {
    this.setState({ confirmKey: key });
  }

  dismissNotification(key) {
    this.setState({ confirmKey: null });
    this.props.handleDismissNotification(key);
  }

  handleSeverityChange(option) {
    if (option !== this.state.selectedSeverity) {
      this.setState({ selectedSeverity: option });
    }
    this.props.setNotificationNumber();
  }

  handleSiteChange(option) {
    if (option !== this.state.selectedSite) {
      this.setState({ selectedSite: option });
    }
    this.props.setNotificationNumber();
  }

  handleTypeChange(option) {
    if (option !== this.state.selectedType) {
      this.setState({ selectedType: option });
    }
    this.props.setNotificationNumber();
  }

  showModal(item) {
    this.setState({showModal: true, item}, ()=>{
      $('#live-data-notification-modal').modal('open');
    });
  }

  getExportData(data) {
    let exportData = [['Level', 'Type', 'Message', 'Site', 'Time and Date']];
    exportData = _.concat(
      exportData,
      _.map(
        data,
        (item) => {
          return [
            item.severity,
            item.type,
            item.value,
            (item.site ? item.site.name : ''),
            item.timestamp
          ];
        }
      )
    );
    return exportData;
  }

  render () {
    const { data, contactsData } = this.props;
    const { typeOptions, siteOptions, selectedType, selectedSite, selectedSeverity, confirmKey } = this.state;
    const { showModal, item } = this.state;

    let filteredData = data;
    if (selectedType) {
      filteredData = _.filter(filteredData, (item) => { return item.type === selectedType.value });
    }
    if (selectedSite) {
      filteredData = _.filter(filteredData, (item) => { return item.site && item.site.id === selectedSite.value });
    }
    if (selectedSeverity && selectedSeverity.value) {
      filteredData = _.filter(filteredData, (item) => { return item.severity === selectedSeverity.value });
    }

    return (
      <div className="liveDataNotification">
        <div className="row">
          <div className="col m2 s12">
            <Select
              name="site"
              clearable={false}
              onChange={(option)=>this.handleSeverityChange(option)}
              value={selectedSeverity}
              placeholder="All"
              noResultsText="No option yet"
              options={severityOptions}
            />
          </div>
          <div className="col m3 s12">
            <Select
              name="site"
              onChange={(option)=>this.handleSiteChange(option)}
              value={selectedSite}
              placeholder="All Sites"
              noResultsText="No option yet"
              options={siteOptions}
            />
          </div>
          <div className="col m3 s12">
            <Select
              name="type"
              onChange={(option)=>this.handleTypeChange(option)}
              value={selectedType}
              placeholder="All Notifications"
              noResultsText="No option yet"
              options={typeOptions}
            />
          </div>
          <div className="col m4 s12 text-right">
            { window.IS_ADMIN &&
              <a
                className="waves-effect waves-light btn white but2 mr-sm mb-sm"
                onClick={() => {this.props.handleDismissNotification(null)}}
              >
                Clear All
              </a>
            }
            <CSVLink data={this.getExportData(data)}
              filename={"notifications-export.csv"}
              className="waves-effect waves-light btn but1 mb-sm"
              target="_blank">
                Export Notifications
            </CSVLink>
          </div>
        </div>
        <div className="liveDataNotification__table notification-table">
          <table>
            <thead>
              <tr>
                <th></th>
                <th>Type</th>
                <th>Message</th>
                <th>Site/Asset</th>
                <th>Time & date</th>
                <th>Actions</th>
                <th>Map</th>
              </tr>
            </thead>
            <tbody id="liveDataNotification__list" className="liveDataNotification__list">
              {filteredData.length === 0 &&
                <tr id="liveDataNotification__emptyRow" className="liveDataNotification__emptyRow">
                  <td colSpan="7">No notifications yet</td>
                </tr>
              }
              {filteredData.map((item, key) => (
                <tr key={key} className="filtered-notification">
                  <td>
                    {item.severity &&
                      <div
                        className={
                          classNames(
                            'liveDataNotification__severity',
                            `liveDataNotification__severity--${item.severity.toLowerCase()}`
                          )
                        }
                      >
                        {item.severity[0]}
                      </div>
                    }
                  </td>
                  <td>{item.type}</td>
                  <td>{item.msg}</td>
                  <td>{item.site.name}/{item.asset_name}</td>
                  <td>{moment(item.timestamp).format('MM/DD/YYYY hh:mm A')}</td>
                  <td className={cx({'liveDataNotification__action': confirmKey === key})}>

                    {confirmKey === key ?
                      window.IS_ADMIN && <a
                        className="but3 liveDataNotification__confirmButton"
                        onClick={() => { this.dismissNotification(item.message_uniq_id); }}>
                        Dismiss ?
                      </a>
                      :
                      window.IS_ADMIN &&
                      <div>
                        <a
                          href="javascript:void(0);"
                          onClick={() => { this.confirmDismissNotification(key); }}
                        >Dismiss</a>&nbsp;|&nbsp;
                        <a className="liveDataNotification__actionButton"
                          onClick={()=>{ this.showModal(item);} }
                          href="javascript:void(0);">Action</a>
                      </div>
                    }
                  </td>
                  <td>
                    <a
                      href="javascript:void(0);"
                      className="liveDataNotification__viewOnMap"
                    >
                      <i
                        className="fa fa-map-o"
                        onClick={(e) => {this.props.showOnMap({assetId: item.asset_id});}}
                      ></i>
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        {
          showModal &&
          <LiveDataNotificationModal
            data={item}
            contactsData={contactsData}
            onSubmitForm={this.props.onSendNotification}
          />
        }
      </div>
    );
  }
}


// const mapStateToProps = (state) => ({
//   geoData: state.assets.data
// })

LiveDataNotificationList.propTypes = {
  data: React.PropTypes.array,
  contactsData: React.PropTypes.array,
  handleDismissNotification: React.PropTypes.func,
  onSendNotification: React.PropTypes.func,
  setNotificationNumber: React.PropTypes.func,
  showOnMap: React.PropTypes.func,
};

export default LiveDataNotificationList
