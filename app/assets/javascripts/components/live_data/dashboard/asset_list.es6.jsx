import LiveDataAssetCarousel from './asset_carousel.es6.jsx';
import LiveDataAssetTable from './asset_table.es6.jsx';
import { CSVLink } from 'react-csv';
import cx from 'classnames';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { showOnMapAction } from '../redux/assets';
import $ from 'jquery';

class LiveDataAssetList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showOption: (((($(window).width() < $(window).height()) && $(window).width()<=425)||(($(window).width() > $(window).height()) && $(window).width()<=750)) ? 'carousel' : 'list'),
      filteredData: [],
    };
    console.log(this.state);
  }

  componentWillMount() {
    const { data, bounds } = this.props;
    this.filterAssets(data, bounds);
  }

  componentWillReceiveProps(nextProps) {
    const { data, bounds } = nextProps;
    this.filterAssets(data, bounds);
  }

  filterAssets(data, bounds) {
    let filteredData = [];
    if (bounds) {
      filteredData = _.filter(data, (item) => {
        return bounds.contains({lat: item.geo[0], lng: item.geo[1]});
      });
    }
    this.setState({ filteredData: filteredData });
  }

  onToggleView(option) {
    this.setState({ showOption: option });
  }

  getExportData(data) {
    let exportData = [['Name', 'ID', 'Location', 'Last Found', 'Geofences', 'Hours', 'Miles']];
    exportData = _.concat(
      exportData,
      _.map(
        data,
        (item) => {
          return [
            item.title,
            item.asset_num,
            item.location,
            item.lastUpdate,
            'Geofence',
            item.hours,
            item.miles,
          ];
        }
      )
    );
    return exportData;
  }

  render () {
    const { data } = this.props;
    const { showOption, filteredData } = this.state;
    return (
      <div className="liveDataAssets">
        <div className="liveDataAssets__header">
          <div className="row margin-bottom-0">
            <div className="liveDataAssets__title h1 col l8 m5 s12">
              Assets
            </div>
            <div className="liveDataAssets__showOptions col l4 m7 s12">
              <CSVLink data={this.getExportData(filteredData)}
                filename={"assets-export.csv"}
                className="waves-effect waves-light btn but1 pull-left mt-tn export-data-btn"
                target="_blank">
                  Export Assets
              </CSVLink>
              <div className="liveDataAssets__tab view_list">
                <a
                  className={cx('liveDataAssets__tabLink', {'liveDataAssets__tabLink--active': showOption === 'list'})}
                  onClick={() => this.onToggleView('list')}
                >
                  <i className="material-icons">view_list</i>
                </a>
              </div>
              <div className="liveDataAssets__tab">
                <a
                  className={cx('liveDataAssets__tabLink', {'liveDataAssets__tabLink--active': showOption === 'carousel'})}
                  onClick={() => this.onToggleView('carousel')}
                >
                  <i className="material-icons">view_carousel</i>
                </a>
              </div>
            </div>
          </div>
        </div>
        {filteredData && filteredData.length > 0 ?
          <div>
            {showOption === 'list' &&
              <LiveDataAssetTable
                data={filteredData}
                showOnMap={(data) => { this.props.dispatch(showOnMapAction(data)); }}
              />
            }
            {showOption === 'carousel' &&
              <LiveDataAssetCarousel 
                data={filteredData} 
                showOnMap={(data) => { this.props.dispatch(showOnMapAction(data)); }}
              />
            }
          </div>
          :
          <div className="liveDataAssets__noAssets">
            No assets found in the map
          </div>
        }
      </div>
    );
  }
}



LiveDataAssetList.propTypes = {
  data: React.PropTypes.array,
  bounds: React.PropTypes.object,
  showOnMap: React.PropTypes.func,
};

const mapStateToProps = (state) => ({
  data: state.assets.data,
  bounds: state.assets.bounds
})


export default connect(mapStateToProps)(LiveDataAssetList)

