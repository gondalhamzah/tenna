import { connect } from 'react-redux';
import { Button, Card, Row, Col, Modal, Input } from 'react-materialize';
import Select from 'react-select';

class LiveDataNotificationModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model: {
        contact: null,
        useOtherContact: false,
        useEmail: false,
        email: '',
        usePhone: false,
        phone: '',
        notes: '',
        requestMaintainService: false,
        submitting: false,
      },
      modelError: '',
      modelSuccess: '',
      contactOptions: [],
      modalId: ''
    };

    this.onFieldChange = this.onFieldChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.isFormValid = this.isFormValid.bind(this);
  }

  componentWillMount() {
    const { contactsData } = this.props;
    let uniqModalId = _.uniqueId('notify_modal_');

    const contactOptions = _.map(contactsData, (item) => {
      return {
        value: item.id,
        label: `${item.first_name} ${item.last_name}`,
      };
    });

    this.setState({ contactOptions: contactOptions, modalId: uniqModalId });
  }

  onFieldChange(name, value) {
    const { model } = this.state;
    model[name] = value;
    this.setState({ model: model });
  }

  isFormValid() {
    const { model } = this.state;

    if (model.contact ||
      (model.useOtherContact && model.useEmail && model.email) ||
      (model.useOtherContact && model.usePhone && model.phone)) {
      return true;
    }
    return false;
  }

  transformData() {
    const { model } = this.state;
    const { data } = this.props;
    let newData = { data };
    newData.to = { notes: model.notes, requestMaintainService: model.requestMaintainService };
    if (model.contact) {
      newData.to.user_id = model.contact.value;
    }
    if (model.useOtherContact && model.useEmail && model.email) {
      newData.to.email = model.email;
    }
    if (model.useOtherContact && model.usePhone && model.phone) {
      newData.to.phone = model.phone;
    }
    newData.modalId = this.state.modalId;
    return newData;
  }

  submitForm() {
    const { model } = this.state;
    if (this.isFormValid()) {
      this.props.onSubmitForm(this.transformData());
      this.setState({
        modelError: '',
        model: {
          contact: null,
          useOtherContact: false,
          useEmail: false,
          email: '',
          usePhone: false,
          phone: '',
          notes: '',
          requestMaintainService: false,
          submitting: false,
        },
      });
    } else {
      this.setState({ modelError: 'Please provide at least one contact.', modelSuccess: '' });
    }
  }

  render () {
    const { data } = this.props;
    const { model, contactOptions, modelError, modelSuccess, submitting } = this.state;
    return (
      <Modal
        id="live-data-notification-modal"
        header={<div className="h1">Action</div>}
        actions={[
          <Button
            waves='light'
            className="but2 mr-sm"
            modal='close'
            flat
          >Cancel</Button>,
          <Button
            waves='light'
            className="but1"
            flat
            onClick={this.submitForm}
          >Send</Button>,
        ]}
      >
        <div className="liveDataNotificationModal">
          <div className="liveDataNotificationModal__content">
            <Row className="">
              <Col className="subh2">
                {data.msg}
              </Col>
            </Row>
            {modelError &&
              <Row className="">
                <Col className="parsley-required">
                  {modelError}
                </Col>
              </Row>
            }
            <Row className="no-mb">
              <Col>
                <label
                  forHtml="company_contacts"
                  className="b3"
                >
                  Company Contacts
                </label>
              </Col>
              <Col s={6}>
                <Select
                  name="company_contacts"
                  onChange={(option)=>this.onFieldChange('contact', option)}
                  value={model.contact}
                  placeholder="Search Contacts"
                  noResultsText="No contacts yet"
                  options={contactOptions}
                />
              </Col>
            </Row>
            <Row className="no-mb">
              <Input
                s={12}
                name='useOtherContact'
                type='checkbox'
                value={model.useOtherContact}
                label='Use Other Contact'
                className='filled-in liveDataNotificationModal__checkbox'
                labelClassName="liveDataNotificationModal__checkboxLabel"
                onChange={(e, value) => {
                  this.onFieldChange('useOtherContact', value);
                }}
              />
            </Row>
            {model.useOtherContact === true &&
              <Row className="no-mb">
                <Input
                  name='useEmail'
                  type='checkbox'
                  value={model.useEmail}
                  className='filled-in liveDataNotificationModal__checkbox'
                  labelClassName="liveDataNotificationModal__checkboxLabel"
                  label=' '
                  onChange={(e, value) => {
                    this.onFieldChange('useEmail', value);
                  }}
                />
                <Input
                  name='email'
                  type='text'
                  value={model.email}
                  disabled={model.useEmail !== true}
                  placeholder='Email'
                  className='filled-in'
                  onChange={(e, value) => {
                    this.onFieldChange('email', value);
                  }}
                />
                <Input
                  name='usePhone'
                  type='checkbox'
                  value={model.usePhone}
                  className='filled-in liveDataNotificationModal__checkbox'
                  labelClassName="liveDataNotificationModal__checkboxLabel"
                  label=' '
                  onChange={(e, value) => {
                    this.onFieldChange('usePhone', value);
                  }}
                />
                <Input
                  name='phone'
                  type='text'
                  value={model.phone}
                  disabled={model.usePhone !== true}
                  placeholder='Phone'
                  onChange={(e, value) => {
                    this.onFieldChange('phone', value);
                  }}
                />
              </Row>
            }
            <Row className="no-mb">
              <Col s={12}>
                <label
                  forHtml="notes"
                  className="liveDataNotificationModal__label b5"
                >
                  Notes
                </label>
              </Col>
              <Input
                s={12}
                name='notes'
                type='textarea'
                value={model.notes}
                placeholder="Enter notes"
                multiple={true}
                onChange={(e, value) => {
                  this.onFieldChange('notes', value);
                }}
              />
            </Row>
            <Row className="no-mb">
              <Input
                name='requestMaintainService'
                type='checkbox'
                value={model.requestMaintainService}
                label='Request Maintenance'
                className='filled-in liveDataNotificationModal__checkbox'
                labelClassName="liveDataNotificationModal__checkboxLabel"
                onChange={(e, value) => {
                  this.onFieldChange('requestMaintainService', value);
                }}
              />
            </Row>
          </div>
        </div>
      </Modal>
    );
  }
}
LiveDataNotificationModal.propTypes = {
  data: React.PropTypes.object,
  contactsData: React.PropTypes.array,
  onSendNotification: React.PropTypes.func,
};

export default LiveDataNotificationModal
