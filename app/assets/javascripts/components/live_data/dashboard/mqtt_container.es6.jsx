import { connect } from 'react-redux';
import { addNotificationAction, updateNewPayloadAction, updateNotificationAssetLastUpdateAction } from '../redux/notifications';
import { updateAssetDataAction } from '../redux/assets';
import { getMqttTokenRequest } from '../redux/mqtt';

class MqttContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onConnectSuccess = this.onConnectSuccess.bind(this);
    this.onConnectFail = this.onConnectFail.bind(this);
    this.onNewMessageArrive = this.onNewMessageArrive.bind(this);
    this.onConnectionLost = this.onConnectionLost.bind(this);
    this.lostVal = 1;
  }
  componentWillMount() {
    const { mqttToken } = this.props;
    console.log('mqttToken', mqttToken);
    this.defaultOptions = {
      timeout: 3,
      useSSL: true,
      userName: mqttToken,
      keepAliveInterval: 999999,
      password: 'jwt2',

      //Gets Called if the connection has sucessfully been established
      onSuccess: this.onConnectSuccess,

      //Gets Called if the connection could not be established
      onFailure: this.onConnectFail
    };
  }
  componentDidMount() {
    const mqttClient = new Messaging.Client(
      'mqtt.buildsourced.com',
      34687,
      'jwt_test' + parseInt(Math.random() * 1000, 10)
    );
    this.mqttClient = mqttClient;

    this.connectServer();
  }

  componentWillReceiveProps(newProps) {
    const { mqttToken } = this.props;
    if (mqttToken !== newProps.mqttToken) {
      console.log('========tokenChanged========');
      this.defaultOptions.userName = newProps.mqttToken;
      this.defaultOptions.password = 'jwt2';
      console.log(this.defaultOptions);
      this.mqttClient.connect(this.defaultOptions);
    }
  }

  connectServer() {
    const options = this.defaultOptions;
    this.mqttClient.connect(options);
    console.log('connecting');

    this.mqttClient.onConnectionLost = (response) => {
      this.onConnectionLost(response);
    };

    this.mqttClient.onMessageArrived = (message) => {
      this.onNewMessageArrive(message);
    };
  }

  onConnectionLost(response) {
    this.tryReconnect();
    if (response.errorCode !== 0) {
      console.log('onConnectionLost:' + response.errorMessage);
    }
  }

  onNewMessageArrive(message) {
    const { geoData, notificationAsset } = this.props;
    const data = JSON.parse(message.payloadString);
    console.log('***************************');
    console.log(data);
    _.each(data, (payload) => {
      const index = _.findIndex(geoData, function(o) { return o.assetId === payload.asset_id; });
      if (index > -1) {
        if (payload['type'] == 'GeoLocation') {
          this.props.dispatch(updateAssetDataAction({assetId: payload.asset_id, geo: [payload.value.latitude, payload.value.longitude]}));
        } else if (payload['type'] == 'Payload') {
          this.props.dispatch(updateNewPayloadAction({assetId: payload.asset_id, value: payload.value}));
        } else {
          const assetData = geoData[index];
          if (assetData.site) {
            payload.site = assetData.site;
          }

          this.props.dispatch(addNotificationAction(payload));
        }
        const assetData = geoData[index];
        let lastUpdate = assetData.lastUpdate;
        if (payload.timestamp) {
          lastUpdate = payload.timestamp;
        } else if (payload['type'] == 'Payload') {
          const value = payload.value;
          lastUpdate = value[value.length - 1].timestamp;
        }
        assetData.lastUpdate = lastUpdate;
        if (notificationAsset.assetId === payload.asset_id) {
          this.props.dispatch(updateNotificationAssetLastUpdateAction(lastUpdate));
        }
        
        this.props.dispatch(updateAssetDataAction(assetData));
      }
    });
  }

  onConnectSuccess() {
    console.log('Connected');
    this.mqttClient.subscribe(`/${window.COMPANY_ID}/notifications`, {qos: 2});
    console.log('Subscribed');
  }

  onConnectFail(message) {
    console.log('Connection failed: ', message.errorMessage);
    this.tryReconnect();
  }

  tryReconnect() {
    const timeout = this.lostVal * this.lostVal * 5;
    const that = this;
    console.log(`reconnecting after ${timeout} seconds...`);
    window.setTimeout(function() {
      that.props.dispatch(getMqttTokenRequest());
    }, timeout * 1000);
    this.lostVal += 1;
  }

  render () {
    const { children } = this.props;
    return (
      <div>
        {children}
      </div>
    );
  }
}

MqttContainer.propTypes = {
  children: React.PropTypes.any,
  geoData: React.PropTypes.array,
  notificationAsset: React.PropTypes.object,
  mqttToken: React.PropTypes.string,
  handleNewNotification: React.PropTypes.func,
};

const mapStateToProps = (state) => ({
  geoData: state.assets.data,
  mqttToken: state.mqtt.token,
  notificationAsset: state.notifications.asset,
})

export default connect(mapStateToProps)(MqttContainer);

