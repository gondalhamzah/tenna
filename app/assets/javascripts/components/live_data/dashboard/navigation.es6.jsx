import Slider from 'rc-slider';
import { connect } from 'react-redux';
import cx from 'classnames';
import { setQueryAction, setDaysAction } from '../redux/assets';

import { Link } from 'react-router-dom';

const marks = {
  0: 'All',
  1: '1yr',
  2: '6m',
  3: '3m',
  4: '1m',
  5: '1w',
  6: '1d',
  7: '1hr'
};

function log(value) {
  console.log(value); //eslint-disable-line
}
class LiveDataNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      queryValue: '',
    }
    this.inputTimeout = null;
    this.onQueryChange = this.onQueryChange.bind(this);
  }

  onQueryChange(e) {
    const value = e.target.value;

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout);
    }
    this.setState({ queryValue: value });
    const timeoutId = setTimeout(() => {
      this.props.dispatch(setQueryAction(value));
    }, 200);
    this.inputTimeout = timeoutId;
  }

  render () {
    const { selectedTab } = this.props;
    const { queryValue } = this.state;
    return (
      <div className="liveDataNav">
        <div className="liveDataNav__mainNav">
          <div>
            <Link
              to="/live_data"
              className={cx(
                'liveDataNav__linkItem',
                { 'liveDataNav__linkItem--active': selectedTab === 'dashboard' }
              )}
            >
              Live Data
            </Link>
            {window.IS_ADMIN &&
              <Link
                to="/live_data/config"
                className={cx(
                  'liveDataNav__linkItem',
                  { 'liveDataNav__linkItem--active': selectedTab === 'config' }
                )}
              >
                Configure Alerts
              </Link>
            }
          </div>
        </div>
        { selectedTab === 'dashboard'
          ?
          <div className="liveDataNav__searchNav">
            <div className="row margin-bottom-0">
              <div className="col s5 m8">
                <div className="liveDataNav__searchWrapper">
                    <div className="input-field">
                      <input
                        id="nav-search-livedata"
                        type="search"
                        name="query"
                        className="ip3 liveDataNav__searchInput"
                        placeholder="Search"
                        onChange={(e) => this.onQueryChange(e)}
                        value={queryValue}
                      />
                      <label htmlFor="search" className="active">
                        <i className="material-icons">search</i>
                      </label>
                      <i className="material-icons nav-close"
                        onClick={(ev)=>this.props.dispatch(setQueryAction(''))}>
                        close
                      </i>
                    </div>
                </div>
              </div>
              <div className="col s7 m4 liveDataNav__timeline">
                <Slider
                  min={0}
                  max={7}
                  marks={marks}
                  step={null}
                  value={this.props.days}
                  onChange={ (val)=>this.props.dispatch(setDaysAction(val)) }
                  defaultValue={0} />
              </div>
            </div>
          </div>
          :
          ''
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  days: state.assets.days,
  query: state.assets.query
})


export default connect(mapStateToProps)(LiveDataNavigation)
