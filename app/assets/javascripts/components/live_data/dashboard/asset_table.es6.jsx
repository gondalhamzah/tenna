import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Table, Thead, Tr, Td, Th } from 'reactable';
import Select from 'react-select';
import {Input} from 'react-materialize';

const metaOptions = [
  {
    label: 'Hours',
    value: 'hours',
  },
  {
    label: 'Miles',
    value: 'miles',
  },
]

export default class LiveDataAssetTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedMetaOption: metaOptions[0],
    }
    this.handleMetaOptionChange = this.handleMetaOptionChange.bind(this);
  }

  handleMetaOptionChange(option) {
    if (option !== this.state.selectedMetaOption) {
      this.setState({ selectedMetaOption: option });
    }
  }

  render () {
    const { data } = this.props;
    const { selectedMetaOption } = this.state;

    return (
      <div className="liveDataAssetsTable">
        <Table
          sortable={[
            'name',
            'id',
            'location',
            'lastFound',
            'geofence',
          ]}
          itemsPerPage={8}
          pageButtonLimit={10}
        >
          <Thead>
            <Th column="name">
              <strong className="name-header">Name</strong>
            </Th>
            <Th column="id" width="10%">
              <strong className="age-header">ID</strong>
            </Th>
            <Th column="location" width="25%">
              <strong className="age-header">Location</strong>
            </Th>
            <Th column="lastFound" width="15%">
              <strong className="age-header">Last Found</strong>
            </Th>
            <Th column="geofence" width="10%">
              <strong className="age-header">Geofences</strong>
            </Th>
            <Th column="hours" width="10%">
              <Select
                name="metaOption"
                onChange={(option)=>this.handleMetaOptionChange(option)}
                value={selectedMetaOption}
                noResultsText="No meta option yet"
                options={metaOptions}
                clearable={false}
              />
            </Th>
          </Thead>
          {data.map((item, key) => (
            <Tr key={key}>
              <Td
                column="name"
                value={item.title}
              >
                <div className="liveDataAssetsTable__checkDiv">
                  <div className="photo_wrapper">
                    <img src={item.img_url}/>
                  </div>
                  <Link to={`/live_data/assets/${item.assetId}`}>
                    {item.title}
                  </Link>
                </div>
              </Td>
              <Td column="id">{item.asset_num}</Td>
              <Td column="location">{item.location}</Td>
              <Td column="lastFound">{moment(item.lastUpdate).format('MM/DD/YYYY hh:mm A')}</Td>
              <Td column="geofence">
                {item.geofences.length === 0 ?
                  <div>N/A</div>
                :
                  <div>
                    {item.geofences.map((geofence) => (
                      <div>{geofence.name}</div>
                    ))}
                  </div>
                }
              </Td>
              <Td
                column="hours"
              >
                <div style={{ position: 'relative', width: '132px' }}>
                  {selectedMetaOption && selectedMetaOption.value === 'hours' ? 
                    `${item.hours} Hours`
                    :
                    `${item.miles} Miles`
                  }
                  <a
                    href="#map"
                    className="liveDataAssetsTable__viewOnMap"
                  >
                    <i
                      className="fa fa-map-o"
                      onClick={(e) => {this.props.showOnMap(item);}}
                    ></i>
                  </a>
                </div>
                </Td>
            </Tr>
          ))}
        </Table>
      </div>
    );
  }
}



LiveDataAssetTable.propTypes = {
  data: React.PropTypes.array,
  showOnMap: React.PropTypes.func,
};