import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import cx from 'classnames';
import Collapsible from 'react-collapsible';
import {Input, Button} from 'react-materialize';
import Select from 'react-select';

import {
  setQueryAction,
  setDaysAction,
  assetFilterAction,
  toggleFilterAction
} from '../redux/assets';

const MILES_OPTIONS = [
  {label: 'Exact', value: 'Exact'},
  {label: '5', value: 5},
  {label: '10', value: 10},
  {label: '25', value: 25},
  {label: '50', value: 50},
  {label: '100', value: 100},
  {label: '250', value: 250}
];
class FilterCheckbox extends React.Component{
  render() {
    const {name, label, onChange, value} = this.props;

    return (
    <div className="row">
      <Input name={name} type='checkbox' className='filled-in'
        label={label} labelClassName='boolean cb1'
        checked={value ? true : false}
        onChange={(ev, val)=>{onChange(val)}}
      />
    </div>)
  }
}
class LiveDataDashboardFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showFilter: true,
      filterVals: {}
    };
    this.toggleFilter = this.toggleFilter.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.days != nextProps.days || this.props.query != nextProps.query) {
      this.handleFilter(nextProps);
    }
  }

  toggleFilter() {
    this.props.dispatch(toggleFilterAction(!this.props.showFilter));
  }

  handleFilterChange(name, val) {
    const newFilterVals = Object.assign(
      {},
      this.state.filterVals,
      {[name]: val}
    );

    this.setState({filterVals:  newFilterVals})
  }

  handleFilter(props) {
    const vals = Object.assign(
      {dummy: 1},
      this.state.filterVals,
      {
        location_changed_in: props.days,
        query: props.query
      }
    )
    this.props.dispatch(assetFilterAction(vals));
  }

  clearFilter() {
    this.setState({filterVals: {}});
    this.props.dispatch(setQueryAction(''));
    this.props.dispatch(setDaysAction(0));
    this.props.dispatch(assetFilterAction({dummy: 1}));
  }

  render () {
    const { filterVals } = this.state;
    const { showFilter } = this.props;
    const sites = this.props.sites.map((s)=>{
      return {label: s[0], value: s[1]}
    })
    return (
      <div className={cx('ldDashboardFilter', {'ldDashboardFilter--minimized': !showFilter})}>
        <div className="ldDashboardFilter__header h1">
          <div className="ldDashboardFilter__title">
            Filters
          </div>
          {/*<i
            className="ldDashboardFilter__chevron fa fa-filter"
            aria-hidden="true"
            onClick={this.toggleFilter}
          ></i>*/}
        </div>
        <div className="ldDashboardFilter__body">
          <Collapsible trigger="Assets" transitionTime="100">
            <FilterCheckbox name='my_assets'  label='Assets Created By Me' value={filterVals.created_by_me}
              onChange={(val)=>{this.handleFilterChange('created_by_me', val === true ? 1 : null)}} />
          </Collapsible>

          <Collapsible trigger="Asset Type"  transitionTime="100">
            <FilterCheckbox name='equipment'  label='Equipment' value={filterVals.equipment}
              onChange={(val)=>{this.handleFilterChange('equipment', val === true ? 1 : null)}} />
            <FilterCheckbox name='material'  label='Material' value={filterVals.material}
              onChange={(val)=>{this.handleFilterChange('material', val === true ? 1 : null)}} />
          </Collapsible>

          <Collapsible trigger="Tags | Trackers"  transitionTime="100">
            <FilterCheckbox name='has_qr_code'  label='Tags' value={filterVals.has_qr_code}
              onChange={(val)=>{this.handleFilterChange('has_qr_code', val === true ? 1 : null)}} />
            <FilterCheckbox name='has_trackers'  label='Trackers' value={filterVals.has_trackers}
              onChange={(val)=>{this.handleFilterChange('has_trackers', val === true ? 1 : null)}} />
          </Collapsible>

          <div className="ldDashboardFilter__buttons">
            <Button waves='red' className="but2" onClick={(ev)=>this.clearFilter()}>
              Reset
            </Button>
            <Button waves='light' className="but1" onClick={(ev)=>this.handleFilter(this.props)}>
              Apply Filters
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

LiveDataDashboardFilter.propTypes = {
};

const mapStateToProps = (state) => ({
  sites: state.sites.data,
  days: state.assets.days,
  query: state.assets.query,
  showFilter: state.assets.showFilter,
})

export default connect(mapStateToProps)(LiveDataDashboardFilter)
