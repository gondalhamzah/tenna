import LiveDataNotificationList from './notificationList.es6.jsx';
import GeoFenceView from '../geoFence/index.es6.jsx';

import cx from 'classnames';
import { connect } from 'react-redux';
import {
  dismissNotificationAction,
  dismissAllNotificationAction,
  sendNotificationAction
} from '../redux/notifications';
import { showOnMapAction } from '../redux/assets';
import { setGeofenceNavSelectedAction } from '../redux/geofences';
import { Button, Card, Row, Col, Modal } from 'react-materialize';

class LiveDataDashboardSubNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: '',
    };
    this.handleDismissNotification = this.handleDismissNotification.bind(this);
    this.onSendNotification = this.onSendNotification.bind(this);
  }

  selectTab(tabName) {
    if (tabName === this.state.selectedTab) {
      this.setState({ selectedTab: '' });
    } else {
      this.setState({ selectedTab: tabName });
      this.props.dispatch(setGeofenceNavSelectedAction(tabName == 'geofence'));
    }
  }

  handleDismissNotification(key) {
    if (key || key === 0) {
      this.props.dispatch(dismissNotificationAction(key));
    } else {
      this.props.dispatch(dismissAllNotificationAction());
    }
  }

  onSendNotification(data) {
    this.props.dispatch(sendNotificationAction(data));
    $('#live-data-notification-modal').modal('close');
  }

  setNotificationNumber() {
    $('#ldDashboardSubNav__count__notifications').html($('.filtered-notification').size());
  }

  render() {
    const { selectedTab } = this.state;
    const { notificationData, contactsData, sitesData, gaugesData } = this.props;
    return (
      <div className="row ldDashboardSubNav">
        <div className="col s12 m12 l12">
          <ul className="ldDashboardSubNav__group">
            <li className={cx('col s6 l6 ldDashboardSubNav__tab', { 'ldDashboardSubNav__tab--active': selectedTab === 'notification' })}>
              <a
                className="ldDashboardSubNav__tabLink"
                onClick={() => {this.selectTab('notification')}}
              >
                <span>Notifications</span>
                <span className="ldDashboardSubNav__count" id="ldDashboardSubNav__count__notifications"></span>
                <i className="ldDashboardSubNav__chevron material-icons">
                  {selectedTab === 'notification' ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}
                </i>
              </a>
            </li>
            <li className={cx('col s6 l6 ldDashboardSubNav__tab', { 'ldDashboardSubNav__tab--active': selectedTab === 'geofence' })}>
              <a
                className="ldDashboardSubNav__tabLink"
                onClick={() => {this.selectTab('geofence')}}
              >
                <span>Geofences</span>
                <span className="ldDashboardSubNav__count">
                  {this.props.geoFenceCount}
                </span>
                <i className="ldDashboardSubNav__chevron material-icons">
                  {selectedTab === 'geofence' ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}
                </i>
              </a>
            </li>
          </ul>

          <div className={cx('ldDashboardSubNav__tabContent', { 'ldDashboardSubNav__tabContent--active' : selectedTab === 'notification' })}>
            <LiveDataNotificationList
              data={notificationData}
              contactsData={contactsData}
              sitesData={sitesData}
              gaugesData={gaugesData}
              handleDismissNotification={this.handleDismissNotification}
              onSendNotification={this.onSendNotification}
              setNotificationNumber={this.setNotificationNumber}
              showOnMap={(data) => { this.props.dispatch(showOnMapAction(data)); }}
            />
          </div>
          <div  className={cx('ldDashboardSubNav__tabContent', { 'ldDashboardSubNav__tabContent--active' : selectedTab === 'geofence' })}>
            <GeoFenceView />
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  notificationData: state.notifications.data,
  contactsData: state.contacts.data,
  sitesData: state.sites.data,
  gaugesData: state.gauges.data,
  geoFenceCount: state.geofences.data.length,
})

LiveDataDashboardSubNav.propTypes = {
  notificationData: React.PropTypes.array,
  contactsData: React.PropTypes.array,
  handleDismissNotification: React.PropTypes.func
};

export default connect(mapStateToProps)(LiveDataDashboardSubNav);
