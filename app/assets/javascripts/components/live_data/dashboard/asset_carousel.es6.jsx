import { connect } from 'react-redux';
import Slider from 'react-slick';
import {Input} from 'react-materialize';
import { updateDrawingInfoAction } from '../redux/geofences';
import { Link } from 'react-router-dom';

import SliderPagination from './sliderPagination';

export default class LiveDataAssetCarousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      settings: {
        className: 'liveDataAssetCarousel center',
        centerMode: false,
        focusOnSelect: true,
        slidesToShow: 7,
        slidesToScroll: 7,
        // initialSlide: 2,
        infinite: false,
        speed: 500,
        responsive: [
          { breakpoint: 384, settings: { slidesToShow: 1, slidesToScroll: 1 } },
          { breakpoint: 769, settings: { slidesToShow: 3, slidesToScroll: 3 } },
          { breakpoint: 1025, settings: { slidesToShow: 4, slidesToScroll: 4 } },
          { breakpoint: 1367, settings: { slidesToShow: 5, slidesToScroll: 5 } },
          { breakpoint: 1921, settings: { slidesToShow: 7, slidesToScroll: 7 } }
        ]
      },
    };
    this.onPageChange = this.onPageChange.bind(this);
    this.afterSliderChange = this.afterSliderChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { data } = nextProps;
    const { settings } = this.state;
    const dataCount = data.length;

    settings.slidesToShow = Math.min(7, dataCount);
    settings.responsive = [
      { breakpoint: 768, settings: { slidesToShow: Math.min(1, dataCount), slidesToScroll: 1 } },
      { breakpoint: 1024, settings: { slidesToShow: Math.min(3, dataCount), slidesToScroll: 1 } },
      { breakpoint: 100000, settings: { slidesToShow: Math.min(7, dataCount), slidesToScroll: 1 } }
    ]
    this.setState({ settings: settings });
  }

  onPageChange(page) {
    this.slider.slickGoTo((page - 1) * 7);
  }

  afterSliderChange(index) {
    const page = Math.floor(index / 7) + 1;
    this.setState({ activePage: page });
  }

  render () {
    const { data } = this.props;
    const { settings, activePage } = this.state;
    const total = data.length;
    return (
      <div className="liveDataAssetCarousel">
        <Slider
          {...settings}
          ref={instance => this.slider = instance}
          afterChange={this.afterSliderChange}
        >
          {data.map((item, key) => (
            <div
              key={key}
            >
              <div className="liveDataAssetCarousel__card">
                <div className="liveDataAssetCarousel__assetPhoto">
                  <img src={item.img_url} />
                </div>
                <div className="liveDataAssetCarousel__cardTitle h2">
                  <Link to={`/live_data/assets/${item.assetId}`}>
                    {item.title}
                  </Link>
                </div>
                <div className="ip3">
                  ID - {item.assetId}
                </div>
                <div className="liveDataAssetCarousel__cardRow dd2">
                  <div className="dd1">Last found:&nbsp;</div>
                  <div className="liveDataAssetCarousel__cardRow__date">{moment(item.lastUpdate).format('MM/DD/YYYY hh:mm A')}</div>
                </div>
                <div className="liveDataAssetCarousel__cardRow dd2">
                  <div className="dd1">Hours:&nbsp;</div>
                  <div>{item.hours}</div>
                </div>
                <div className="liveDataAssetCarousel__cardRow dd2">
                  <div className="dd1">Mileage:&nbsp;</div>
                  <div>{item.miles} miles</div>
                </div>
                <a
                  href="#map"
                  className="liveDataAssetCarousel__viewOnMap"
                >
                  <i
                    className="fa fa-map-o"
                    onClick={(e) => {this.props.showOnMap(item);}}
                  ></i>
                </a>
              </div>
            </div>
          ))}
        </Slider>
        <SliderPagination
          total={total}
          perPage={7}
          activePage={activePage}
          onPageChange={this.onPageChange}
        />
      </div>
    );
  }
}



LiveDataAssetCarousel.propTypes = {
  data: React.PropTypes.array,
};
