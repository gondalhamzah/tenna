import DrawingManager from "react-google-maps/lib/components/drawing/DrawingManager";
import { DRAWING_MANAGER } from 'react-google-maps/lib/constants';
import { connect } from 'react-redux';
import { updateDrawingInfoAction } from '../redux/geofences.js';
import { generateGoogleMapShapeObj } from './utils.js';

const shapeOption = {
  fillColor: '#fd4f00',
  fillOpacity: 0.5,
  strokeWeight: 1,
  draggable: true,
  clickable: true,
  editable: true,
  zIndex: 2,
}

class GeoFenceDrawingView extends React.Component {
  getCenterStr(c) {
    return `${c.lat()},${c.lng()}`;
  }

  getShapeInfo() {
    let info = {zoom_level: this.manager.map.getZoom()};

    if (this.shapeType == google.maps.drawing.OverlayType.RECTANGLE) {
      const bounds = this.shapeObj.getBounds();
      const ne = bounds.getNorthEast();
      const sw = bounds.getSouthWest();

      info['points'] = [
        ne,
        Object.assign({}, {lat:sw.lat(), lng: ne.lng()}),
        sw,
        Object.assign({}, {lat: ne.lat(), lng: sw.lng()}),
        ne
      ]

      info['type'] = 'polygon';
      info['centerStr'] = this.getCenterStr(bounds.getCenter());
    }

    if (this.shapeType == google.maps.drawing.OverlayType.CIRCLE) {
      info['type'] = 'circle';
      info['center'] = this.shapeObj.getCenter();
      info['points'] = [info['center']];
      info['radius'] = this.shapeObj.getRadius();
      info['centerStr'] = this.getCenterStr(this.shapeObj.getCenter());
    }

    if (this.shapeType == google.maps.drawing.OverlayType.POLYGON) {
      info['type'] = 'polygon';
      info['points'] = [];
      for (var i=0; i<this.shapeObj.getPath().getLength(); i++) {
        info['points'][i] = this.shapeObj.getPath().getAt(i).toJSON();
      }
      info['points'].push(info['points'][0])
      const bounds = new google.maps.LatLngBounds();
      this.shapeObj.getPath().forEach(function (element, index) { bounds.extend(element); });
      info['centerStr'] = this.getCenterStr(bounds.getCenter());

    }

    return info
  }

  dispatchShapeInfo() {
    this.props.dispatch(updateDrawingInfoAction({shape: this.getShapeInfo()}))
  }

  componentWillUnmount() {
    this.manager.setMap(null);
    if (this.shapeObj) {
      this.shapeObj.setMap(null);
    }
    // delete this.manager;
  }

  handleShapeEvent(shape) {
    let isBeingDragged = false;
    const that = this;
    //if  the overlay is being dragged, set_at gets called repeatedly, so either we can debounce that or igore while dragging, ignoring is more efficient
    google.maps.event.addListener(shape, 'dragstart', function () {
        isBeingDragged = true;
    });
    //if the overlay is dragged
    google.maps.event.addListener(shape, 'dragend', function () {
        isBeingDragged = false;
        that.dispatchShapeInfo();
    });

    if (this.shapeType == google.maps.drawing.OverlayType.CIRCLE) {
      google.maps.event.addListener(shape, 'radius_changed', function() {
        that.dispatchShapeInfo();
      });
      google.maps.event.addListener(shape, 'center_changed', function() {
        if (!isBeingDragged) {
          that.dispatchShapeInfo();
        }
      });
    }

    if (this.shapeType == google.maps.drawing.OverlayType.RECTANGLE) {
      google.maps.event.addListener(shape, 'bounds_changed', function() {
        if (!isBeingDragged) {
          that.dispatchShapeInfo();
        }
      });
    }

    if (this.shapeType == google.maps.drawing.OverlayType.POLYGON) {
      const thePath = shape.getPath();
      google.maps.event.addListener(thePath, 'set_at', function() {
        if (!isBeingDragged) {
          that.dispatchShapeInfo();
        }
      });

      google.maps.event.addListener(thePath, 'insert_at', function() {
        that.dispatchShapeInfo();
      });

      google.maps.event.addListener(thePath, 'remove_at', function() {
        that.dispatchShapeInfo();
      });
    }
  }

  generateShape() {
    const { drawing } = this.props;
    if (drawing.id) {
      this.manager.map.setZoom(15);

      const pts = drawing.center_pt.split(',')
      this.manager.map.setCenter({lat: parseFloat(pts[0]), lng: parseFloat(pts[1])});
      const res = generateGoogleMapShapeObj(this.manager.map, drawing, true);
      this.shapeObj = res.obj;
      this.shapeType = res.shapeType;
      this.handleShapeEvent(this.shapeObj);
      this.manager.map.setZoom(drawing.zoom_level);
    }
  }

  setDrawingManager(instance) {
    if (instance) {
      this.manager = instance.state[DRAWING_MANAGER];
      this.shapeObj = null;
      this.shapeType = null;
      this.generateShape();
      const fillColor = this.props.drawing.color;
      this.manager.setOptions({
        circleOptions: Object.assign({}, shapeOption, {fillColor}),
        rectangleOptions: Object.assign({}, shapeOption, {fillColor}),
        polygonOptions: Object.assign({}, shapeOption, {fillColor})
      });

      this.props.dispatch(updateDrawingInfoAction({zoom_level: this.manager.map.getZoom()}));

      const that = this;
      google.maps.event.addListener(this.manager, 'overlaycomplete', function(event) {
        if (that.shapeObj != null) {
          that.shapeObj.setMap(null)
        }

        that.shapeType = event.type
        that.shapeObj = event.overlay;
        that.dispatchShapeInfo();
        that.handleShapeEvent(that.shapeObj);
      });

      google.maps.event.addListener(this.manager, 'drawingmode_changed', function() {
        if (that.manager.getDrawingMode() != null && that.shapeObj != null) {
          that.shapeObj.setMap(null);
          that.shapeObj = null;
          that.props.dispatch(updateDrawingInfoAction({shape:null}));
        }
      });

      google.maps.event.addListener(this.manager.map, 'zoom_changed', function() {
        if (that.manager && that.manager.map) {
          that.props.dispatch(updateDrawingInfoAction({zoom_level: that.manager.map.getZoom()}));
        }
      });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.drawing.color != this.props.drawing.color) {
      const fillColor = nextProps.drawing.color
      this.manager.setOptions({
        circleOptions: Object.assign({}, shapeOption, {fillColor}),
        rectangleOptions: Object.assign({}, shapeOption, {fillColor}),
        polygonOptions: Object.assign({}, shapeOption, {fillColor})
      });
    }

    return nextProps.drawingActive != this.props.drawingActive
  }

  render() {
    const {color} = this.props.drawing;
    return (
      <DrawingManager
        ref={(instance) => { this.setDrawingManager(instance); }}
        defaultOptions={{
          drawingControl: true,
          drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [
              google.maps.drawing.OverlayType.CIRCLE,
              google.maps.drawing.OverlayType.POLYGON,
              google.maps.drawing.OverlayType.RECTANGLE,
            ],
            circleOptions: shapeOption,
            rectangleOptions: shapeOption,
            polygonOptions: shapeOption
          }
        }}
      />
    )
  }
}

const mapStateToProps = (state) => ({
  drawingActive: state.geofences.drawingActive,
  drawing: state.geofences.drawing,
})


export default connect(mapStateToProps)(GeoFenceDrawingView)
