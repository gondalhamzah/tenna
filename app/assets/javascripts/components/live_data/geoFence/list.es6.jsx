import { connect } from 'react-redux';
import { setDrawingActiveAction, updateDrawingInfoAction, deleteGeofenceAction } from '../redux/geofences';

class GeoFenceListView extends React.Component {
  handleEditGeoFence(g) {
    const info = {
      id: g.id,
      color: g.color,
      name: g.name,
      alert_enter_site: g.alert_enter_site,
      alert_exit_site: g.alert_exit_site,
      company_contacts: g.company_contacts,
      other_contacts: g.other_contacts,
      center_pt: g.center,
      assets: g.assets,
      zoom_level: g.zoom_level || 15,
      shape: {
        type: g.shape,
        radius: g.radius,
        points: g.points,
        centerStr: g.center
      }
    }
    this.props.dispatch(updateDrawingInfoAction(info));
  }

  handleDeleteGeoFence(g) {
    const that = this;

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      that.props.dispatch(deleteGeofenceAction(g.id));
    });
  }

  render() {
    const { genFences } = this.props;

    return (
      <div className="geoFenceList">
        { window.IS_ADMIN &&
          <div className="geoFenceList__new"
              onClick={(ev)=>this.props.dispatch(setDrawingActiveAction(true))}>
            <div className="geoFenceList__new--child">
              <i className="fa fa-plus"></i>
              <br/>
              Add
              <br/>
              Geofence
            </div>
          </div>
        }
        {
          genFences.map((g)=>
            <div className="geoFenceList__item hvr-grow">
              <img src={g.map_img} />
              {
                window.IS_ADMIN &&
                <button className="btn-floating" onClick={(ev)=>this.handleDeleteGeoFence(g)}>
                  <i className="material-icons">close</i>
                </button>
              }
              <div className="geoFenceList__item--info">
                <p className="l5">{g.name}</p>
                {
                  window.IS_ADMIN &&
                  <i className="fa fa-pencil"
                      onClick={(ev)=>this.handleEditGeoFence(g)}>
                  </i>
                }
              </div>
            </div>
          )
        }
      </div>
    )
  }
}


const mapStateToProps = (state) => ({
  genFences: state.geofences.data || []
});

export default connect(mapStateToProps)(GeoFenceListView)
