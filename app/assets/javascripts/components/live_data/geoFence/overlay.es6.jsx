import { connect } from 'react-redux';
import { generateGoogleMapShapeObj } from './utils.js';

class GeoFenceOverlay extends React.Component {
  generateShapes(props) {
    const {map} = props;
    console.log('=======generate', props.geoFences);
    this.shapes = props.geoFences.map((g)=>{
      const res = generateGoogleMapShapeObj(
        map,
        {
          color: g.color,
          shape: {
            points: g.points,
            radius: g.radius,
            type: g.shape
          }
        },
        false
      );

      return res.obj;
    });
  }

  destroyShapes() {
    this.shapes.forEach((s)=>s.setMap(null));
    delete this.shapes;
  }
  componentDidMount() {
    this.generateShapes(this.props);
  }

  componentWillUnmount() {
    this.destroyShapes();
  }

  shouldComponentUpdate(nextProps) {
    if (JSON.stringify(this.props.geoFences) != JSON.stringify(nextProps.geoFences)) {
      this.destroyShapes();
      this.generateShapes(nextProps);
    }
  }

  render() {
    return (<div></div>)
  }
}

const mapStateToProps = (state) => ({
  geoFences: state.geofences.data || []
});
export default connect(mapStateToProps)(GeoFenceOverlay);
