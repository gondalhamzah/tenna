export function generateGoogleMapShapeObj(map, drawing, editable) {
  if (drawing.shape.type == 'circle') {
    return {
      obj: new google.maps.Circle({
        fillColor: drawing.color,
        fillOpacity: 0.5,
        map,
        center: drawing.shape.points[0],
        radius: drawing.shape.radius,
        strokeWeight: 1,
        editable: editable,
        draggable: editable
      }),
      shapeType: google.maps.drawing.OverlayType.CIRCLE
    };
  }

  if (drawing.shape.type == 'polygon') {
    const pts = drawing.shape.points;
    if (pts.length == 5 && pts[0].lng == pts[1].lng && pts[1].lat == pts[2].lat
        && pts[2].lng == pts[3].lng && pts[3].lat == pts[0].lat) {
      return {
        obj: new google.maps.Rectangle({
          fillColor: drawing.color,
          fillOpacity: 0.5,
          strokeWeight: 1,
          editable: editable,
          draggable: editable,
          map,
          bounds: {
            north: pts[0].lat,
            south: pts[2].lat,
            east: pts[0].lng,
            west: pts[2].lng
          }
        }),
        shapeType: google.maps.drawing.OverlayType.RECTANGLE
      };
    } else {
      return {
        obj: new google.maps.Polygon({
          fillColor: drawing.color,
          fillOpacity: 0.5,
          strokeWeight: 1,
          editable: editable,
          draggable: editable,
          map,
          paths: drawing.shape.points
        }),
        shapeType: google.maps.drawing.OverlayType.POLYGON
      };
    }
  }
}
