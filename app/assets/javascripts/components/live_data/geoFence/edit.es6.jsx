import { connect } from 'react-redux';
import reactCSS from 'reactcss'
import Select from 'react-select';
import { Button, Card, Row, Col, Modal, Input, Icon, Table } from 'react-materialize';
import { ChromePicker } from 'react-color';
import {
  updateDrawingInfoAction,
  addGeofenceAction,
  updateGeofenceAction,
  resetDrawing
} from '../redux/geofences.js';

class GeoFenceEditView extends React.Component {
  constructor(props) {
    super(props);
    const drawing = props.drawing;
    this.state = {
      title: drawing.name || '',
      displayColorPicker: false,
      color: drawing.color || '#fd4f00',
      alert_enter_site: drawing.alert_enter_site,
      alert_exit_site: drawing.alert_exit_site,
      company_contacts: drawing.company_contacts,
      other_contacts: drawing.other_contacts,
      other_recipient: drawing.other_contacts && drawing.other_contacts.length > 0,
      showWarning: false
    };
  }

  setValue(name, val) {
    this.setState({[name]: val});
    if (name == 'other_recipient' && val && !this.state.other_contacts) {
      this.setState({other_contacts: [{email: '', phone: '', name: ''}]});
    }
  }

  handleToggleColorPicker() {
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  }

  handleCloseColorPicker() {
    this.setState({ displayColorPicker: false })
  }

  handleColorChange(color) {
    this.setState({ color: color.hex })
    this.props.dispatch(updateDrawingInfoAction({color: color.hex}));
  }

  onContactChange(name, val, index) {
    const otherContacts = this.state.other_contacts;
    const tmp = Object.assign({}, otherContacts[index], {[name]: val})
    otherContacts.splice(index, 1, tmp)
    this.setState({other_contacts: [...otherContacts]})
  }

  deleteOtherContact(index) {
    const otherContacts = this.state.other_contacts;
    otherContacts.splice(index, 1)
    this.setState({other_contacts: [...otherContacts]})
  }

  addOtherContact() {
    const otherContacts = this.state.other_contacts;
    this.setState({other_contacts: [...otherContacts, {email: '', phone: '', name: ''}]})
  }

  // isValidToSave() {
  //   const {color, title, company_contacts, other_recipient, other_contacts} = this.state;
  //   return color && title && (company_contacts || (other_recipient && other_contacts.length > 0)) && Object.keys(this.props.drawing.shape).length > 0;
  // }
  showErrors() {
    const {color, title, company_contacts, other_recipient, other_contacts} = this.state;
    if (!title && !(company_contacts || (other_recipient && other_contacts.length > 0)) && Object.keys(this.props.drawing.shape || {}).length == 0) {
      return "Please add a title, draw a Geofence in the map and provide at least one contact.";
    }

    if (!title && !(company_contacts || (other_recipient && other_contacts.length > 0))) {
      return "Please add a title and provide at least one contact.";
    }

    if (!title && Object.keys(this.props.drawing.shape || {}).length == 0) {
      return "Please add a title and draw a Geofence in the map.";
    }

    if (!(company_contacts || (other_recipient && other_contacts.length > 0)) && Object.keys(this.props.drawing.shape || {}).length == 0) {
      return "Please draw a Geofence in the map and provide at least one contact.";
    }

    if (!title) {
      return "Please add a title.";
    }

    if (Object.keys(this.props.drawing.shape || {}).length == 0) {
      return "Please draw a Geofence in the map.";
    }

    if(!(company_contacts || (other_recipient && other_contacts.length > 0))) {
      return "Please provide at least one contact.";
    }
  }

  saveGeoFence() {
    const {color, title, company_contacts, other_recipient, other_contacts, alert_enter_site, alert_exit_site} = this.state;
    const { shape, id, zoom_level } = this.props.drawing;
    this.setState({showWarning: true});

    if (color && title && (company_contacts || (other_recipient && other_contacts.length > 0)) && Object.keys(this.props.drawing.shape).length > 0) {
      let data = {
        id,
        name: title,
        color,
        alert_enter_site,
        alert_exit_site,
        shape: shape.type,
        radius: shape.radius,
        company_contacts,
        zoom_level: zoom_level
      }

      data['points'] = [...shape.points];
      data['other_contacts'] = [...(other_contacts || [])];
      data['center'] = shape.centerStr;
      if (data.id) {
        this.props.dispatch(updateGeofenceAction(data));
      } else {
        this.props.dispatch(addGeofenceAction(data));
      }
    }
  }

  cancelEdit() {
    this.props.dispatch(resetDrawing());
  }

  handleCompanyContactChange(idx, field) {
    const {company_contacts} = this.state;
    let new_contacts = [...company_contacts];
    new_contacts[idx][field] = !new_contacts[idx][field];
    this.setState({company_contacts: new_contacts});
  }

  render() {
    const {
      title,
      color,
      alert_enter_site,
      alert_exit_site,
      company_contacts,
      other_recipient,
      other_contacts
    } = this.state;

    const styles = reactCSS({
      'default': {
        color: {
          width: '50px',
          height: '30px',
          borderRadius: '2px',
          background: `${ this.state.color }`,
        },
        swatch: {
          padding: '5px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
          position: 'absolute',
          zIndex: '2',
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
      },
    });

    const contacts = _.map(this.props.contacts, (item) => {
      return {
        user_id: item.id,
        name: `${item.first_name} ${item.last_name}`,
      };
    });
    return (
      <div className="geoFenceEdit">
        <div className="geoFenceEdit__header">
          <a className="b2" href="javascript:void(0);"
            onClick={(ev)=>this.cancelEdit()}>
            <i className="fa fa-chevron-left"></i>&nbsp;&nbsp;
            All Geofences
          </a>
        </div>
        <div className="geoFenceEdit__body">
          <div className="row">
            <div className="col l6 s12 geoFenceEdit__settings">
              <p className="h1">Settings</p>
              <Input label="Title *" required
                value={title} s={12}
                onChange={(e,val)=>this.setValue('title', val)} />
              <div className="col s4 input-field geoFenceEdit__label">
                Select color:
              </div>
              <div className="col s8 input-field">
                <div style={ styles.swatch }  className="geoFenceEdit__swatch" onClick={ this.handleToggleColorPicker.bind(this) }>
                  <div style={ styles.color } />
                </div>
                { this.state.displayColorPicker ? <div style={ styles.popover }>
                  <div style={ styles.cover } onClick={ this.handleCloseColorPicker.bind(this) }/>
                  <ChromePicker disableAlpha={true} color={ this.state.color } onChangeComplete={ (color)=>this.handleColorChange(color) } />
                </div> : null }
                <Input value={ this.state.color } onChange={(ev)=>this.handleColorChange({hex: ev.target.value})} />
              </div>
              <div className="col s12">
                <div className="row">
                  <div className="col l6 m6 s6 b6">
                    <div className="switch  geoFenceEdit__switch">
                      Alert when asset enters site
                      <label className="geoFenceEdit__checkbox">
                        <input type="checkbox"
                        onChange={(ev)=>this.setValue('alert_enter_site', ev.target.checked)}
                        checked={alert_enter_site} />
                        <span className="lever"></span>
                      </label>
                    </div>
                  </div>
                  <div className="col l6 m6 s6 b6">
                    <div className="switch  geoFenceEdit__switch">
                      Alert when asset exit site
                      <label className="geoFenceEdit__checkbox">
                        <input type="checkbox" checked={alert_exit_site}
                          onChange={(ev)=>this.setValue('alert_exit_site', ev.target.checked)} />
                        <span className="lever"></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col s12 l6 divContacts">
              <p className="h1">Contacts</p>
              <div className="geoFenceEdit__contacts">
                <Row>
                  <Col s={4}>
                    <label
                      forHtml="company_contacts"
                      className="b8"
                    >
                      Company Contacts
                    </label>
                  </Col>
                  <Col s={8}>
                    <Select
                      name="company_contacts"
                      multi={true}
                      labelKey="name"
                      valueKey="user_id"
                      onChange={(val)=>this.setValue('company_contacts', val)}
                      value={company_contacts}
                      placeholder="Search Contacts"
                      noResultsText="No contacts yet"
                      options={contacts}
                    />
                  </Col>
                </Row>
                {
                  company_contacts && company_contacts.length > 0 &&
                  <Row>
                    <Table striped={true} responsive={true} bordered={true} className="geoFenceEdit__contactTable">
                      <tbody>
                        {
                          company_contacts.map((c, idx)=>
                          <tr>
                            <td className="b8">{c.name}</td>
                            <td>
                            <Input
                              name={`email${c.user_id}`}
                              type='checkbox'
                              checked={c.email}
                              className='filled-in'
                              label='Email'
                              labelClassName='dd1'
                              onChange={(e, value) => {
                                this.handleCompanyContactChange(idx, 'email')
                              }} />
                            </td>
                            <td>
                            <Input
                              name={`text${c.user_id}`}
                              type='checkbox'
                              checked={c.text}
                              className='filled-in'
                              labelClassName='dd1'
                              label='Text'
                              onChange={(e, value) => {
                                this.handleCompanyContactChange(idx, 'text')
                              }} />
                            </td>
                          </tr>
                          )
                        }
                      </tbody>
                    </Table>
                  </Row>
                }
                <Row className="mt-md tb-md">
                  <Input
                    name='useEmail'
                    type='checkbox'
                    checked={other_recipient}
                    className='filled-in'
                    labelClassName='b8'
                    label='Other Recipient'
                    onChange={(e, value) => {
                      this.setValue('other_recipient', !this.state.other_recipient);
                    }} />
                </Row>
                {
                  other_recipient === true && other_contacts &&
                  other_contacts.map((model, i)=>
                    <div className="geoFenceEdit__otherContact" key={`other-contact${i}`}>
                      <Row className="no-mb">
                        <Input name='name'
                          type='text'
                          value={model.name}
                          placeholder="Name"
                          className='ip2'
                          s={11}
                          onChange={(e, value)=>this.onContactChange('name', value, i)} />
                        <Col s={1}>
                          <span onClick={(ev)=>this.deleteOtherContact(i)}>
                            <Icon>delete</Icon>
                          </span>
                        </Col>
                      </Row>
                      <Row className="no-mb">
                        <Input name={`email-check-${i}`}
                          type="checkbox"
                          checked={model.email_enabled}
                          className='filled-in'
                          labelClassName='dd1'
                          label=' '
                          s={1}
                          onChange={(e, value) => this.onContactChange('email_enabled', !model.email_enabled,i)} />
                        <Input
                          name='email'
                          type='text'
                          value={model.email}
                          placeholder='Email'
                          className='ip2'
                          className='filled-in'
                          disabled={!model.email_enabled}
                          s={5}
                          onChange={(e, value) => {
                            this.onContactChange('email', value, i);
                          }}
                        />
                        <Input name={`phone-check-${i}`}
                          type="checkbox"
                          checked={model.text_enabled}
                          className='filled-in'
                          labelClassName='dd1'
                          label=' '
                          s={1}
                          onChange={(e, value) => this.onContactChange('phone_enabled', !model.phone_enabled,i)} />
                        <Input
                          name='phone'
                          type='tel'
                          className='ip2'
                          value={model.phone}
                          disabled={!model.phone_enabled}
                          placeholder='Cell Number'
                          s={5}
                          onChange={(e, value) => {
                            this.onContactChange('phone', value, i);
                          }}
                        />
                      </Row>
                    </div>
                  )
                }
                { other_recipient === true &&
                  <Row className="mt-md geoFenceEdit__otherContact__btnRow">
                    <a href="javascript:void(0);" className="b18" onClick={(ev)=>this.addOtherContact()}>
                      <button
                          className="btn-floating">
                        <i className="material-icons">add</i>
                      </button>
                      Add New Contact
                    </a>
                  </Row>
                }
                <Row className="mt-md pull-right">
                  <Button wave="teal" className="but2 mr-md"
                    onClick={(ev)=>this.cancelEdit()}>Cancel</Button>
                  <Button wave="light" className="but1"
                      onClick={this.saveGeoFence.bind(this)}>
                    Save
                  </Button>
                </Row>
                <div>
                  {
                    this.state.showWarning &&
                    <div className="geoFenceEdit__warning__txt">
                      {this.showErrors()}
                    </div>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  contacts: state.contacts.data,
  drawing: state.geofences.drawing || {}
})


export default connect(mapStateToProps)(GeoFenceEditView)

