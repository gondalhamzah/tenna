import { connect } from 'react-redux';
import GeoFenceListView from './list.es6.jsx';
import GeoFenceEditView from './edit.es6.jsx';
import {setDrawingActiveAction} from '../redux/geofences.js';

class GeoFenceView extends React.Component {
  render () {
    return (
      <div>
      {
        this.props.drawingActive ?
          <GeoFenceEditView /> :
          <GeoFenceListView /> 
      }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  drawingActive: state.geofences.drawingActive
});

export default connect(mapStateToProps)(GeoFenceView)
