import { Provider } from 'react-redux';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import ReduxToastr from 'react-redux-toastr'

import LiveDataDashboard from './liveDataDashboard.es6.jsx';
import LiveDataConfigAlert from './liveDataConfigAlert.es6.jsx';
import LiveDataAsset from './liveDataAsset.es6.jsx';
import configureStore from './store/configureStore';
import MqttContainer from './dashboard/mqtt_container.es6.jsx';
export default class LiveData extends React.Component {
  constructor(props) {
    super(props);
    this.store = configureStore(window.INITIAL_STATE)
  }

  render () {
    const store = this.store;
    return (
      <div>
        <Provider store={store}>
          <MqttContainer>
            <BrowserRouter>
              <Switch>
                <Route path="/live_data/assets/:id" component={LiveDataAsset}/>
                <Route path="/live_data/config" render={()=>(
                  window.IS_ADMIN ?
                    <LiveDataConfigAlert />
                    : <Redirect to="/live_data" />
                )}/>
                <Route path="/live_data" component={LiveDataDashboard}/>
              </Switch>
            </BrowserRouter>
            <ReduxToastr
              newestOnTop={true}
              position="top-right"
              transitionIn="fadeIn"
              transitionOut="fadeOut"
              progressBar/>
          </MqttContainer>
        </Provider>
      </div>
    );
  }
}
