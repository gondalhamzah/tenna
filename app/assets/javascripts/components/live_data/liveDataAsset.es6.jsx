import LiveDataAssetDetail from './assetDetail';
import LiveDataNavigation from './dashboard/navigation.es6.jsx';
import MqttContainer from './dashboard/mqtt_container.es6.jsx';

export default class LiveDataAsset extends React.Component {

  render () {
    const assetId = this.props.match.params.id;

    return (
      <div>
        <LiveDataNavigation />
        <LiveDataAssetDetail id={assetId} />
      </div>
    );
  }
}
