import 'regenerator-runtime/runtime';
import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from "redux-saga";
import rootSaga from '../sagas';
import rootReducer from '../redux';
import DevTools from '../DevTools';

export default function configureStore(initialState) {
    const sagamiddle = createSagaMiddleware();
    const createStoreWithMiddleware = compose(
        applyMiddleware(sagamiddle),
        window.devToolsExtension ? window.devToolsExtension() : DevTools.instrument(),
    );

    const store = createStoreWithMiddleware(createStore)(rootReducer, initialState);
    sagamiddle.run(rootSaga);
    return store;
}
