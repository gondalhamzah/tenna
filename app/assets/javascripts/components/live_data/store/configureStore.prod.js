import 'regenerator-runtime/runtime';
import createSagaMiddleware from "redux-saga";
import { applyMiddleware, compose, createStore } from 'redux';
import { watchFilterAsset } from '../sagas/asset';

import rootReducer from '../redux';

export default function configureStore(initialState) {
    // Add so dispatched route actions to the history
    // const reduxRouterMiddleware = routerMiddleware(history);
    const middleware = [
        createSagaMiddleware(watchFilterAsset),
    ];

    const createStoreWithMiddleware = compose(
        applyMiddleware(...middleware),
    );

    const store = createStoreWithMiddleware(createStore)(rootReducer, initialState);
    return store;
}
