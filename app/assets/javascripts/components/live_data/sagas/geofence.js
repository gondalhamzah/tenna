import { put, call, takeLatest } from 'redux-saga/effects';
import {
  addNewGeofence,
  updateGeofence,
  deleteGeofence
} from '../api/geofence';
import * as types from '../redux/consts';

export function* addNewGeofenceSaga(data) {
  try {
    const res = yield call(addNewGeofence, data);
    yield put({ type: types.ADD_GEOFENCE_SUCCESS, payload: res.body })
    swal(
      'Success!',
      'New geofence is created.',
      'success'
    )
    yield put({ type: types.RESET_GEOFENCE_DRAWING });
  } catch (error) {
    swal(
      'Error!',
      error+'',
      'error'
    )
    yield put({ type: types.ADD_GEOFENCE_FAIL, error });
  }
}

export function* updateGeofenceSaga(data) {
  try {
    const res = yield call(updateGeofence, data);
    yield put({ type: types.UPDATE_GEOFENCE_SUCCESS, payload: res.body })
    swal(
      'Success!',
      'The geofence is updated.',
      'success'
    )
    yield put({ type: types.RESET_GEOFENCE_DRAWING });
  } catch (error) {
    swal(
      'Error!',
      error+'',
      'error'
    )
    yield put({ type: types.UPDATE_GEOFENCE_FAIL, error });
  }
}

export function* deleteGeofenceSaga({payload}) {
  try {
    const res = yield call(deleteGeofence, payload)
    yield put({ type: types.DELETE_GEOFENCE_SUCCESS, payload: {id: payload.id} })
    // swal(
    //   'Success!',
    //   'The geofence is deleted.',
    //   'success'
    // )
  } catch (error) {
    swal(
      'Error!',
      error+'',
      'error'
    )
    yield put({ type: types.DELETE_GEOFENCE_FAIL, error})
  }
}

export function* watchGeofence() {
  yield takeLatest(types.ADD_GEOFENCE_REQUEST, addNewGeofenceSaga);
  yield takeLatest(types.UPDATE_GEOFENCE_REQUEST, updateGeofenceSaga);
  yield takeLatest(types.DELETE_GEOFENCE_REQUEST, deleteGeofenceSaga);
}
