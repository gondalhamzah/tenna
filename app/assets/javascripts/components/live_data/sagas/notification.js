import { put, call, takeLatest } from 'redux-saga/effects';
import { sendNotification, fetchNotifAssetDetail } from '../api/notification';
import * as types from '../redux/consts';

export function* sendNotificationSaga({ payload }) {
  console.log('sendNotificationSaga');
  try {
    const notfication = yield call(sendNotification, payload);
    yield put({ type: types.SEND_NOTIFICATION_SUCCESS })
    $('#' + payload.modalId).modal('close');
    swal(
      'Success!',
      'Notification is successfully sent',
      'success'
    )

  } catch (error) {
    console.log(error)
    yield put({ type: types.SEND_NOTIFICATION_FAILED, error });
  }
}

export function* fetchNotifAssetDetailSaga({ payload }) {
  console.log('fetchNotifAssetDetail');
  try {
    const assetDetail = yield call(fetchNotifAssetDetail, payload);
    yield put({ type: types.FETCH_NOTIF_ASSET_DETAIL_SUCCESS, payload: JSON.parse(assetDetail.text) })
  } catch (error) {
    console.log(error)
    yield put({ type: types.FETCH_NOTIF_ASSET_DETAIL_FAILED, error });
  }
}

export function* watchSendNotification() {
  yield takeLatest(types.SEND_NOTIFICATION_REQUEST, sendNotificationSaga);
  yield takeLatest(types.FETCH_NOTIF_ASSET_DETAIL_REQUEST, fetchNotifAssetDetailSaga);
}
