import { put, call, takeLatest } from 'redux-saga/effects';
import { filterAssets } from '../api/assets';
import * as types from '../redux/consts';

export function* filterAssetSaga({ filters }) {
  try {
    const assets = yield call(filterAssets, filters);
    yield put({ type: types.ASSET_FILTER_SUCCESS, payload: JSON.parse(assets.text).data })
  } catch (error) {
    console.log(error)
    yield put({ type: types.ASSET_FILTER_FAILED, error });
  }
}

export function* watchFilterAsset() {
  yield takeLatest(types.ASSET_FILTER_REQUEST, filterAssetSaga);
}
