import { put, call, takeLatest } from 'redux-saga/effects';
import { filterAlertAssets } from '../api/alertAssets';
import * as types from '../redux/consts';

export function* filterAlertAssetsSaga({ filters }) {
  try {
    const alertAssets = yield call(filterAlertAssets, filters);
    yield put({ type: types.FILTER_ALERT_ASSETS_SUCCESS, payload: JSON.parse(alertAssets.text) })
  } catch (error) {
    console.log(error)
    yield put({ type: types.FILTER_ALERT_ASSETS_FAILED, error });
  }
}


export function* watchFilterAlertsAsset() {
  yield takeLatest(types.FILTER_ALERT_ASSETS_REQUEST, filterAlertAssetsSaga);
}
