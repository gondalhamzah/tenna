import { watchFilterAsset } from './asset';
import { watchGetMqttToken } from './mqtt';
import { watchGeofence } from './geofence';
import { watchSendNotification } from './notification';
import { watchFilterAlertsAsset } from './alertAsset';
import { watchGetGaugeAlert } from './gaugeAlert';

export default function* rootSaga() {
    yield [
        watchFilterAsset(),
        watchSendNotification(),
        watchGetMqttToken(),
        watchGeofence(),
        watchFilterAlertsAsset(),
        watchGetGaugeAlert(),
    ]
}
