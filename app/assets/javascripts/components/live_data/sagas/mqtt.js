import { put, call, takeLatest } from 'redux-saga/effects';
import { getMqttToken } from '../api/mqtt';
import * as types from '../redux/consts';

export function* getMqttTokenSaga({}) {
  console.log('getMqttTokenSaga');
  try {
    const mqtt = yield call(getMqttToken, null);
    yield put({ type: types.GET_MQTT_TOKEN_SUCCESS, payload: JSON.parse(mqtt.text).token })
  } catch (error) {
    console.log(error)
    yield put({ type: types.GET_MQTT_TOKEN_SUCCESS, error });
  }
}

export function* watchGetMqttToken() {
  yield takeLatest(types.GET_MQTT_TOKEN_REQUEST, getMqttTokenSaga);
}
