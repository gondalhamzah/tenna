import { put, call, takeLatest } from 'redux-saga/effects';
import { getGaugeAlert, getCategoryAssets, saveGaugeAlert, resetGaugeAlert } from '../api/gaugeAlert';
import * as types from '../redux/consts';

export function* getGaugeAlertSaga({ payload }) {
  console.log('getGaugeAlertSaga');
  try {
    const response = yield call(getGaugeAlert, payload.filters);
    yield put({ type: types.GET_GAUGE_ALERT_SUCCESS, payload: JSON.parse(response.text) })
  } catch (error) {
    console.log(error)
    yield put({ type: types.GET_GAUGE_ALERT_FAIL, error });
  }
}

export function* getCategoryAssetsSaga({ payload }) {
  console.log('getCategoryAssetsSaga');
  try {
    const response = yield call(getCategoryAssets, payload);
    yield put({ type: types.GET_CATEGORY_ASSETS_SUCCESS, payload: JSON.parse(response.text) })
  } catch (error) {
    console.log(error)
    yield put({ type: types.GET_CATEGORY_ASSETS_FAIL, error });
  }
}

export function* saveGaugeAlertSaga({ payload }) {
  console.log('saveGaugeAlertSaga');
  try {
    const response = yield call(saveGaugeAlert, payload);
    console.log(response);
    yield put({ type: types.SAVE_GAUGE_ALERT_SUCCESS, payload: JSON.parse(response.text) })
    // yield put({ type: types.GET_CATEGORY_ASSETS_REQUEST, payload: JSON.parse(response.text) })
  } catch (error) {
    console.log(error)
    yield put({ type: types.SAVE_GAUGE_ALERT_FAIL, payload: error });
  }
}

export function* resetGaugeAlertSaga({ payload }) {
  console.log('resetGaugeAlertSaga');
  try {
    const response = yield call(resetGaugeAlert, payload);
    yield put({ type: types.RESET_GAUGE_ALERT_SUCCESS, payload: JSON.parse(response.text) })
    // yield put({ type: types.GET_CATEGORY_ASSETS_REQUEST, payload: JSON.parse(response.text) })
  } catch (error) {
    console.log(error)
    yield put({ type: types.RESET_GAUGE_ALERT_FAIL, error });
  }
}


export function* watchGetGaugeAlert() {
  yield takeLatest(types.GET_GAUGE_ALERT_REQUEST, getGaugeAlertSaga);
  yield takeLatest(types.GET_CATEGORY_ASSETS_REQUEST, getCategoryAssetsSaga);
  yield takeLatest(types.SAVE_GAUGE_ALERT_REQUEST, saveGaugeAlertSaga);
  yield takeLatest(types.RESET_GAUGE_ALERT_REQUEST, resetGaugeAlertSaga);
}
