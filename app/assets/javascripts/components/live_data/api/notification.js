import request from 'superagent';

export const sendNotification = (data) => {
  const token = $('meta[name="csrf-token"]').attr('content');
  return request.post('/live_data/send_notification').send({ notification: data })
    .set('X-CSRF-Token', token)
    .set('X-Requested-With', 'XMLHttpRequest')
}
export const fetchNotifAssetDetail = (data) => {
  return request.get('/gauge_alerts/asset_detail').query({ asset_id: data })
    .set('X-CSRF-Token', window.CSRF_TOKEN)
    .set('X-Requested-With', 'XMLHttpRequest')
}