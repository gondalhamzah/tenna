import request from 'superagent';

export const getGaugeAlert = (filters) => {
  return request.get('/gauge_alerts').query(filters)
    .set('X-CSRF-Token', window.CSRF_TOKEN)
    .set('X-Requested-With', 'XMLHttpRequest')
}

export const getCategoryAssets = (filters) => {
  return request.get('/gauge_alerts/category_assets').query(filters)
    .set('X-CSRF-Token', window.CSRF_TOKEN)
    .set('X-Requested-With', 'XMLHttpRequest')
}

export const saveGaugeAlert = ({ data, filters }) => {
  return request.post('/gauge_alerts?').query(filters)
    .type('application/json')
    .set('X-Requested-With', 'XMLHttpRequest')
    .set('X-CSRF-Token', window.CSRF_TOKEN)
    .send(data)
}

export const resetGaugeAlert = ({ data, filters }) => {
  return request.post('/gauge_alerts/reset?').query(filters)
    .type('application/json')
    .set('X-Requested-With', 'XMLHttpRequest')
    .set('X-CSRF-Token', window.CSRF_TOKEN)
    .send(data)
}
