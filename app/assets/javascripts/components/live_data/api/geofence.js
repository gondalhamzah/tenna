import request from 'superagent';

export const addNewGeofence = ({payload}) => {
  return request.post('/geo_fences')
    .type('application/json')
    .set('X-Requested-With', 'XMLHttpRequest')
    .set('X-CSRF-Token', window.CSRF_TOKEN)
    .send(payload.data)
}

export const updateGeofence = ({payload}) => {
  const {data} = payload;

  return request.put(`/geo_fences/${data.id}`)
    .type('application/json')
    .set('X-Requested-With', 'XMLHttpRequest')
    .set('X-CSRF-Token', window.CSRF_TOKEN)
    .send(data)
}

export const deleteGeofence =({id}) => {
  return request.del(`/geo_fences/${id}`)
    .type('application/json')
    .set('X-Requested-With', 'XMLHttpRequest')
    .set('X-CSRF-Token', window.CSRF_TOKEN)
    .send()
}
