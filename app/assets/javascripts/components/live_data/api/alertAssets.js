import request from 'superagent';

export const filterAlertAssets = (filters) => {
  return request.get('/gauge_alerts/search_assets_by_category').query(filters)
    .set('X-CSRF-Token', window.CSRF_TOKEN)
    .set('X-Requested-With', 'XMLHttpRequest')
}
