import request from 'superagent';

export const filterAssets = (filters) => {
  return request.get('/live_data').query({filter_search: filters})
    .set('X-CSRF-Token', window.CSRF_TOKEN)
    .set('X-Requested-With', 'XMLHttpRequest')
}
