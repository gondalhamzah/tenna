import request from 'superagent';

export const getMqttToken = () => {
  const token = $('meta[name="csrf-token"]').attr('content');
  return request.get('/live_data/get_mqtt_token').query({})
    .set('X-CSRF-Token', token)
    .set('X-Requested-With', 'XMLHttpRequest')
}
