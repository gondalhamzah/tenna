import { Provider } from 'react-redux';

import LiveDataDashboardFilter from './dashboard/filter.es6.jsx';
import LiveDataDashboardMap from './dashboard/map.es6.jsx';
import LiveDataDashboardSubNav from './dashboard/sub_navigation.es6.jsx';
import LiveDataAssetList from './dashboard/asset_list.es6.jsx';
import LiveDataNavigation from './dashboard/navigation.es6.jsx';
import ConfigureAlerts from './configAlerts/configure_alerts.es6.jsx';

import MqttContainer from './dashboard/mqtt_container.es6.jsx';
import configureStore from './store/configureStore';

export default class LiveDataDashboard extends React.Component {

  render () {
    return (
      <div className="liveData">
        <LiveDataNavigation
          selectedTab="config"
        />
        <ConfigureAlerts />
      </div>
    );
  }
}
