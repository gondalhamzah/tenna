import HalfSolidGauge from 'components/charts/halfSolidGauge';
import FullSolidGauge from 'components/charts/fullSolidGauge';
import OdometerGauge from 'components/charts/odometerGauge';
import BarGauge from 'components/charts/barGauge';
import ProgressGauge from 'components/charts/progressGauge';
import GaugeStats from './assetGaugeStats';

export default class AssetGauge extends React.Component {
  render () {
    const { data } = this.props;
    return (
      <div className="ldAssetGauge text-center">
        <div className="h1 mb-sm text-center">
          {data.label}
        </div>
        <div className="ldAssetGauge__graph">
          {data.type === 'fullSolid' &&
            <FullSolidGauge data={data} />
          }
          {data.type === 'halfSolid' &&
            <HalfSolidGauge data={data} />
          }
          {data.type === 'odometer' &&
            <OdometerGauge data={data} />
          }
          {data.type === 'bar' &&
            <BarGauge data={data} />
          }
          {data.type === 'progress' &&
            <ProgressGauge data={data} />
          }
        </div>
        <div className="ldAssetGauge__stats">
          <GaugeStats data={data} />
        </div>
      </div>
    );
  }
}
