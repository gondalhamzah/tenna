import AssetGauge from './assetGauge';
import AssetGeofence from './assetGeofence';
import ReactGridLayout from 'react-grid-layout';
import { generate } from 'shortid';
import { WidthProvider, Responsive } from "react-grid-layout";

const ResponsiveReactGridLayout = WidthProvider(Responsive);

const originalLayouts = getFromLS("layouts") || {};

function getFromLS(key) {
  let ls = {};
  if (global.localStorage) {
    try {
      ls = JSON.parse(global.localStorage.getItem("rgl-8-layouts")) || {};
      // ls = {};
    } catch (e) {
      /*Ignore*/
    }
  }
  return ls[key];
}

function saveToLS(key, value) {
  if (global.localStorage) {
    const ls = JSON.parse(global.localStorage.getItem("rgl-8-layouts")) || {};
    ls[key] = value
    global.localStorage.setItem(
      "rgl-8-layouts",
      JSON.stringify(ls)
    );
  }
}

export default class AssetGaugeList extends React.Component {
  constructor(props) {
    super(props);
    const layouts = getFromLS(props.id) || {}
    this.state = {
      layouts: JSON.parse(JSON.stringify(layouts))
    };
    this.onLayoutChange = this.onLayoutChange.bind(this);
  }

  onLayoutChange(layout, layouts) {
    // console.log('=======new layouts', layouts)
    saveToLS(this.props.id, layouts);
    this.setState({ layouts });
  }
  render () {
    const { data } = this.props;
    const { layouts } = this.state;
    const totalHeight = [2, 2, 0, 0];
    return (
      <ResponsiveReactGridLayout
        className="layout"
        rowHeight={225}
        cols={{lg: 4, md: 3, sm: 2, xs: 1, xxs: 1}}
        layouts={layouts}
        isResizable={false}
        onLayoutChange={(layout, layouts) =>
          this.onLayoutChange(layout, layouts)
        }
      >
        <div
          key={0}
          data-grid={{x: 0, y: 0, w: 2, h: 2, minW: 1, minH: 1, static: true}}
        >
          <AssetGeofence />
        </div>
        {data && data.map((item, key) => {
          const isHalf = item.type === 'bar' || item.type === 'progress';
          const index = (key + 2) % 4;
          const height = isHalf ? 1 : 2;
          const layout = { x: index, y: totalHeight[index], w: 1, h: height, minW: 1, minH: 1 };
          totalHeight[index] += height;
          return (
            <div
              key={key + 1}
              data-grid={layout}
              isResizable={false}
            >
              <AssetGauge data={item}/>
            </div>
          );
        })}
      </ResponsiveReactGridLayout>
    );
  }
}
