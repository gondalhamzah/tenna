import numeral from 'numeral';
export default class AssetGaugeStats extends React.Component {
  render () {
    const { data } = this.props;
    const valueString = numeral(data.value).format('0.0a');
    return (
      <div className="ldAssetGaugeStats">
        {data.min &&
          <div className="text-center">
            <div className="ldAssetGaugeStats__label">
              Lowest
            </div>
            <div className="ldAssetGaugeStats__value">
              {numeral(data.min.value).format('0,0')}
            </div>
            <div className="ldAssetGaugeStats__date">
              {data.min.datetime}
            </div>
          </div>
        }
        {data.max &&
          <div className="text-center">
            <div className="ldAssetGaugeStats__label">
              Highest
            </div>
            <div className="ldAssetGaugeStats__value">
              {numeral(data.max.value).format('0,0')}
            </div>
            <div className="ldAssetGaugeStats__date">
              {data.max.datetime}
            </div>
          </div>
        }
        {data.last &&
          <div className="text-center">
            <div className="ldAssetGaugeStats__label ldAssetGaugeStats__label--small">
              Last Occurred
            </div>
            <div className="ldAssetGaugeStats__date">
              {data.last.datetime}
            </div>
          </div>
        }
      </div>
    );
  }
}
