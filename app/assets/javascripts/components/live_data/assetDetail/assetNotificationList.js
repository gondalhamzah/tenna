import Select from 'react-select';
import cx from 'classnames';
import LiveDataNotificationModal from '../dashboard/notification_modal.es6.jsx';

const severityOptions = [
  { label: 'All', value: '' },
  { label: 'High', value: 'high' },
  { label: 'Medium', value: 'medium' },
  { label: 'Low', value: 'low' },
];

class AssetNotificationList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      siteOptions: [],
      typeOptions: [],
      selectedType: null,
      selectedSeverity: severityOptions[0],
      confirmKey: null,
      showModal: false,
    }
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleSeverityChange = this.handleSeverityChange.bind(this);
    this.setFilterData = this.setFilterData.bind(this);
    this.confirmDismissNotification = this.confirmDismissNotification.bind(this);
    this.dismissNotification = this.dismissNotification.bind(this);
    this.showModal = this.showModal.bind(this);
    this.renderSeverity = this.renderSeverity.bind(this);
  }

  componentWillMount() {
    const { data } = this.props;
    this.setFilterData(data);
  }

  componentWillReceiveProps(nextProps) {
    const { data } = nextProps;
    this.setFilterData(data);
  }

  handleTypeChange(option) {
    if (option !== this.state.selectedType) {
      this.setState({ selectedType: option });
    }
  }

  handleSeverityChange(option) {
    if (option !== this.state.selectedSeverity) {
      this.setState({ selectedSeverity: option });
    }
  }

  renderSeverity(option) {
    if (option.value === '') return 'All';
    return (<div className={cx('ldAssetNotifList__severity', `ldAssetNotifList__severity--${option.value}`)}>
      {option.value[0]}
    </div>);
  }

  confirmDismissNotification(key) {
    this.setState({ confirmKey: key });
  }

  dismissNotification(key) {
    this.setState({ confirmKey: null });
    this.props.handleDismissNotification(key);
  }

  showModal(item) {
    this.setState({showModal: true, item}, ()=>{
      $('#live-data-notification-modal').modal('open');
    });
  }

  setFilterData(data) {
    let typeOptions;
    typeOptions = [];
    _.each(data, (item) => {
      const typeIndex = _.findIndex(typeOptions, function(o) { return o.value === item.type; });
      if (typeIndex == -1) {
        typeOptions.push({ label: item.type, value: item.type });
      }
    });
    this.setState({ typeOptions: typeOptions });
  }

  render () {
    const { data, contactsData } = this.props;
    const { typeOptions, selectedType, selectedSeverity, confirmKey } = this.state;
    const { showModal, item } = this.state;

    let filteredData = _.filter(data, (d)=>{
      return d.asset_id == this.props.assetId;
    });
    if (selectedType) {
      filteredData = _.filter(filteredData, (item) => { return item.type === selectedType.value });
    }
    if (selectedSeverity && selectedSeverity.value) {
      filteredData = _.filter(filteredData, (item) => { return item.severity === selectedSeverity.value });
    }
    return (
      <div className="ldAssetNotifList">
        <div className="ldAssetNotifList__wrapper">
          <div className="ldAssetNotifList__header">
            <div className="h1 ldAssetNotifList__title">Notifications</div>
            <div className="ldAssetNotifList__filter">
              <Select
                name="type"
                onChange={(option)=>this.handleTypeChange(option)}
                value={selectedType}
                placeholder="All Notifications"
                noResultsText="No option yet"
                options={typeOptions}
              />
            </div>
          </div>
          <div className="ldAssetNotifList__table">
            <table>
              <thead>
                <tr className="tableHead">
                  <th colSpan="2">
                    <div className="ldAssetNotifList__inlineLabel">
                      Message
                    </div>
                    <Select
                      className="ldAssetNotifList__severityList"
                      name="site"
                      clearable={false}
                      onChange={(option)=>this.handleSeverityChange(option)}
                      value={selectedSeverity}
                      placeholder="All"
                      noResultsText="No option yet"
                      options={severityOptions}
                      optionRenderer={this.renderSeverity}
                      valueRenderer={this.renderSeverity}
                    />
                  </th>
                  <th>Time and date</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody id="liveDataNotification__list" className="liveDataNotification__list">
                {filteredData.length === 0 &&
                  <tr id="liveDataNotification__emptyRow" className="liveDataNotification__emptyRow">
                    <td colSpan="4">No notifications yet</td>
                  </tr>
                }
                {filteredData.map((item, key) => (
                  <tr key={key}>
                    <td>
                      {item.severity &&
                        <div className={classNames(
                          'ldAssetNotifList__severity',
                          `ldAssetNotifList__severity--${item.severity.toLowerCase()}`
                          )}
                        >
                          {item.severity[0]}
                        </div>
                      }
                    </td>
                    <td>
                      {item.msg}
                    </td>
                    <td>{item.timestamp}</td>
                    <td className={cx({'liveDataNotification__action': confirmKey === key})}>
                      {confirmKey === key ?
                        <a
                          className="but3 liveDataNotification__confirmButton"
                          onClick={() => { this.dismissNotification(item.message_uniq_id); }}>
                          Dismiss ?
                        </a>
                        :
                        <div>
                          <a
                            href="javascript:void(0);"
                            onClick={() => { this.confirmDismissNotification(key); }}
                          >Dismiss</a>&nbsp;|&nbsp;
                          <a className="liveDataNotification__actionButton"
                            onClick={()=>{ this.showModal(item);} }
                            href="javascript:void(0);">Action</a>
                        </div>
                      }
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <div className="mt-md text-right">
            <a
              className="waves-effect waves-light btn white but2 mr-sm"
              onClick={() => {this.props.handleDismissNotification(null)}}
            >
              Clear All
            </a>
          </div>
        </div>
        {
          showModal &&
          <LiveDataNotificationModal
            data={item}
            contactsData={contactsData}
            onSubmitForm={this.props.onSendNotification}
          />
        }

      </div>
    );
  }
}

AssetNotificationList.propTypes = {
  notificationData: React.PropTypes.array,
  contactsData: React.PropTypes.array,
  handleDismissNotification: React.PropTypes.func,
  assetId: React.PropTypes.number,
};

export default AssetNotificationList;
