import AssetGeofenceMap from './assetGeofenceMap';
export default class AssetGeofence extends React.Component {
  render () {
    const { data } = this.props;
    return (
      <div className="ldAssetGeofence">
        <div className="text-center h1 mb-sm">
          Location
        </div>
        <div className="">
          <AssetGeofenceMap />
        </div>
      </div>
    );
  }
}
