export default class LiveDataAssetInfo extends React.Component {
  render () {
    const { asset } = this.props;
    return (
      <div className="ldAssetInfo">
        <div className="ldAssetInfo__infoWrapper">
          <img src={asset.img_url} className="ldAssetInfo__image" />
          <div className="ldAssetInfo__info">
            <div className="h1">
              {asset.title}
            </div>
            <div className="b32">
              ID - {asset.asset_num}
            </div>
            <div className="b32">
              {asset.location}
            </div>
            <div className="b32">
              Last Found: {moment(asset.lastUpdate).format('MM/DD/YYYY hh:mm A')}
            </div>
            <div className="b32">
              <span className="b21">Hours:</span> {asset.hours} hours
            </div>
            <div className="b32">
              <span className="b21">Mileage:</span> {asset.miles} miles
            </div>
          </div>
          <div className="ldAssetInfo__noteWrapper">
            <div className="b21">
              Notes:
            </div>
            <div className="b13 ldAssetInfo__note">
              {asset.notes}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
