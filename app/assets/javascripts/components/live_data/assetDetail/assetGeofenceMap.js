// import GoogleMap from "react-google-map"
// import GoogleMapLoader from "react-google-maps-loader"
import { connect } from 'react-redux';

import fetch from "isomorphic-fetch";
import { compose, withProps, withStateHandlers } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  Rectangle,
  InfoWindow,
} from "react-google-maps";

// import MarkerClusterer from 'react-google-maps/lib/components/addons/MarkerClusterer';

import { MAP, MARKER, INFO_WINDOW, MARKER_CLUSTERER } from 'react-google-maps/lib/constants';

import { setBoundsAction } from '../redux/assets';
import GeoFenceDrawingView from '../geoFence/drawing.es6.jsx';
import GeoFenceOverlay from '../geoFence/overlay.es6.jsx';

const getIcons = (info) => {
  const tracking_code = info.tracking_code ? info.tracking_code.toLowerCase() : 'project';
  return markerIcons[tracking_code];
}

const getInfoWindowContent = (assetData) => {
  return `
    <div class="liveDataMap__iwContainer">
      <div class="photo_wrapper">
        <img src="${assetData.img_url}"/>
      </div>
      <div class="liveDataMap__iwInfoSection">
        <div class="liveDataMap__iwTitle">${assetData.title}</div>
        <div class="liveDataMap__iwLastUpdate">
          <div>Last Update:</div>
          <div>${assetData.lastUpdate}</div>
        </div>
        <a class="liveDataMap__iwMoreInfo" href="/assets/${assetData.assetId}">
          Details <i class="material-icons liveDataMap__iwMoreInfoIcon">keyboard_arrow_right</i>
        </a>
      </div>
    </div>
  `;
}

const MY_API_KEY = "AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0" // fake

class AssetGeofenceMap extends React.Component {
  constructor(props) {
    super(props);
    this.buildMarkerData = this.buildMarkerData.bind(this);

    this.setMapInstance = this.setMapInstance.bind(this);
    this.onMapTypeIdChanged = this.onMapTypeIdChanged.bind(this);
    this.onMapBoundsChanged = this.onMapBoundsChanged.bind(this);

    this.resizeMap = this.resizeMap.bind(this);

    this.onMarkerMouseOver = this.onMarkerMouseOver.bind(this);
    this.onMarkerMouseOut = this.onMarkerMouseOut.bind(this);
    this.onMarkerClicked = this.onMarkerClicked.bind(this);
    this.buildMarkers = this.buildMarkers.bind(this);

    this.setInfoWindowInstance = this.setInfoWindowInstance.bind(this);


    this.state = {
      markers: {},
      markerData: {},
      coordinates: [],
      isOpen: false,
      mapTypeId: google.maps.MapTypeId.TERRAIN,
    };
    this.infoWindow = null;
    this.gmMarker = false;
    this.map = null;
  }

  shouldComponentUpdate(props) {
    const newAsset = props.asset;
    const oldAsset = this.props.oldAsset;

    if (newAsset && this.gmMarker) {
      const pos = new google.maps.LatLng(newAsset.geo[0], newAsset.geo[1])
      this.gmMarker.setPosition(pos);
    }

    if ((newAsset && !oldAsset) || (newAsset && oldAsset && newAsset.assetId !== oldAsset.assetId)) {
      return true;
    }

    return false;
  }

  componentWillMount() {
    const { asset } = this.props;
    this.buildMarkerData(asset);
    this.mapBoundsTimeout = 0;
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeMap);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeMap);
  }

  resizeMap() {
    google.maps.event.trigger(this.gmMap, "resize");
  }

  setMapInstance(instance) {
    if (instance) {
      this.map = instance;
      this.gmMap = instance.context[MAP];
      this.buildMarkers();
    }
  }

  setInfoWindowInstance(instance) {
    if (instance) {
      this.infoWindow = instance;
      this.gmInfoWindow = instance.state[INFO_WINDOW];
      this.gmInfoWindow.close();
    }
  }

  onMapBoundsChanged(e) {
    if (this.gmMap) {
      const bounds = this.gmMap.getBounds();
      if (this.mapBoundsTimeout) {
        clearTimeout(this.mapBoundsTimeout);
      }
      const timeoutId = setTimeout(() => {
        this.props.dispatch(setBoundsAction(bounds));
      }, 200);

      this.mapBoundsTimeout = timeoutId;
    }
  }


  buildMarkerData(asset) {
    const mapTypeId = (this.gmMap ? this.gmMap.mapTypeId : 'terrain') ;

    asset.icons = getIcons(asset);
    asset.animation = null;
    if (mapTypeId == 'satellite' || mapTypeId == 'hybrid') {
      asset.currentIcon = asset.icons.satelliteIcon;
    } else {
      asset.currentIcon = asset.icons.originalIcon;
    }
    this.setState({ markerData: asset });
  }

  buildMarkers() {
    const { markerData } = this.state;
    this.infoWindow = new google.maps.InfoWindow({
      content: ''
    });

    if (this.gmMarker) {
      this.gmMarker.setMap(null);
    }


    const finalLatLng = new google.maps.LatLng(markerData.geo[0], markerData.geo[1]);
    const gmMarker = new google.maps.Marker({
      position: finalLatLng,
      icon: markerData.currentIcon,
      animation: markerData.animation,
      currentIcon: markerData.currentIcon,
      icons: markerData.icons,
      map: this.gmMap,
      title: markerData.title
    });
    google.maps.event.addListener(gmMarker, 'click', (e) => { this.onMarkerClicked(gmMarker) });
    google.maps.event.addListener(gmMarker, 'mouseover', (e) => { this.onMarkerMouseOver(gmMarker) });
    google.maps.event.addListener(gmMarker, 'mouseout', (e) => { this.onMarkerMouseOut(gmMarker) });
    this.gmMarker = gmMarker;
    this.gmMap.setCenter(finalLatLng);
  }

  onMarkerClicked(gmMarker) {
    const { markerData } = this.state;
    if (markerData) {
      const content = getInfoWindowContent(markerData);
      this.infoWindow.close();
      this.infoWindow.setContent(content);
      this.infoWindow.open(this.gmMap, gmMarker);
    }
  }

  onMarkerMouseOver(gmMarker) {
    gmMarker.setIcon(gmMarker.icons.hoverIcon);
  }

  onMarkerMouseOut(gmMarker) {
    gmMarker.setIcon(gmMarker.currentIcon);
  }

  onMapTypeIdChanged(e) {
    const { asset } = this.props;
    this.buildMarkerData(asset);
    this.buildMarkers();
  }

  // GoogleMap component has a 100% height style.
  // You have to set the DOM parent height.
  // So you can perfectly handle responsive with differents heights.
  render () {
    const { mapTypeId } = this.state;

    return (
      <GoogleMap
        defaultZoom={12}
        defaultCenter={{ lat: 38.8422822, lng: -102.2509999 }}
        mapTypeId="terrain"
        ref={(instance) => { this.setMapInstance(instance); }}
        componentDidMount={this.mapMounted}
        onMapTypeIdChanged={this.onMapTypeIdChanged}
        onBoundsChanged={this.onMapBoundsChanged}
      />
    );
  }
}

AssetGeofenceMap.propTypes = {
  asset: React.PropTypes.object,
}

const mapStateToProps = (state) => ({
  asset: state.notifications.asset,
})

export default compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${MY_API_KEY}&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div id="map" style={{width: '100%', height: '395px'}} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap,
  connect(mapStateToProps),
)(AssetGeofenceMap);

