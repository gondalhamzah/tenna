import { connect } from 'react-redux';
import {
  dismissNotificationAction,
  dismissAllNotificationAction,
  fetchNotifAssetDetailAction,
  sendNotificationAction
} from '../redux/notifications';

import LiveDataAssetInfo from './assetInfo';
import AssetNotificationList from './assetNotificationList';
import AssetGaugeList from './assetGaugeList';
import AssetGeofence from './assetGeofence';

class LiveDataAssetDetail extends React.Component {
  constructor(props) {
    super(props);
    this.handleDismissNotification = this.handleDismissNotification.bind(this);
    this.onSendNotification = this.onSendNotification.bind(this);
  }

  componentWillMount() {
    const { id } = this.props;
    this.props.dispatch(fetchNotifAssetDetailAction(id));
  }

  handleDismissNotification(key) {
    if (key) {
      this.props.dispatch(dismissNotificationAction(key));
    } else {
      this.props.dispatch(dismissAllNotificationAction());
    }
  }

  onSendNotification(data) {
    this.props.dispatch(sendNotificationAction(data));
    $('#live-data-notification-modal').modal('close');
  }
  render () {
    const { notifications, asset, gaugeAlerts, geofences, contactsData } = this.props;
    return (
      <div className="ldAssetDetail row">
        <div className="col s12 m12 l6">
          <LiveDataAssetInfo asset={asset} />
        </div>
        <div className="col s12 m12 l6">
          <AssetNotificationList
            data={notifications}
            assetId={parseInt(this.props.id)}
            contactsData={contactsData}
            handleDismissNotification={this.handleDismissNotification}
            onSendNotification={this.onSendNotification}
          />
        </div>
        <div className="col s12 m12">
          {gaugeAlerts && gaugeAlerts.length > 0 &&
            <AssetGaugeList
              data={gaugeAlerts}
              id={parseInt(this.props.id)}
            />
          }
        </div>
      </div>
    );
  }
}

LiveDataAssetDetail.propTypes = {
  notifications: React.PropTypes.array,
  gaugeAlerts: React.PropTypes.array,
  geofences: React.PropTypes.array,
  asset: React.PropTypes.object,
  contactsData: React.PropTypes.array,
};

const mapStateToProps = (state) => ({
  notifications: state.notifications.data,
  asset: state.notifications.asset,
  gaugeAlerts: state.notifications.gaugeAlerts,
  geofences: state.geofences.data,
  contactsData: state.contacts.data,
})

export default connect(mapStateToProps)(LiveDataAssetDetail);
