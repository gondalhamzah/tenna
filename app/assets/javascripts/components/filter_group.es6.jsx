import FilterRuleDivider from './filter_rule_divider.es6.jsx';
import FilterRule from './filter_rule.es6.jsx';

class FilterGroup extends React.Component {
  render () {
    let childs = []
    this.props.group.rules.forEach((r, i) => {
      if (i>0) {
        childs.push(<FilterRuleDivider word="and" key={`divider-${r.id}`} />)
      }
      childs.push(
        <FilterRule
          key={r.id}
          rule={r}
          index={i+1}
          groupId={this.props.group.id}
          groupIdx={this.props.groupIdx}
          handleRuleDelete={this.props.handleGroupRuleDelete}
          handleRuleChange={this.props.handleGroupRuleChange}
          dropMenuType={this.props.group.groupType}
          catKeys={this.props.catKeys}
          catQualifiers={this.props.catQualifiers}
        />
      )
    });
    return (
      <div className="flow-rules-group rule-table">
        {childs}
      </div>
    );
  }
}

FilterGroup.propTypes = {
  handleGroupRuleChange: React.PropTypes.func,
  handleGroupRuleDelete: React.PropTypes.func,
  handleAddRule: React.PropTypes.func,
  group: React.PropTypes.object,
  dropMenuType: React.PropTypes.string,
  groupIdx: React.PropTypes.number,
  catKeys: React.PropTypes.array,
  catQualifiers: React.PropTypes.array
};

module.exports = FilterGroup;
