import Select from 'react-select';
import React from 'react';
import cx from 'classnames';
import {Input} from 'react-materialize';

import Collapsible from 'react-collapsible';

class FilterDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: this.props.filters,
      key: this.props.key
    };
    this.labelName = this.labelName.bind(this);
    this.deleteFilter = this.deleteFilter.bind(this);
    this.extractFilterData = this.extractFilterData.bind(this);
    this.enterLabel = this.enterLabel.bind(this);
  }

  labelName(key) {
    if (key == "") {
      return "Others";
    } else {
      return this.props.filter_label_obj[Number(key)];
    }
  }

  enterLabel(val, key, self) {
    if (key == "") {
      $.ajax({
        type: "POST",
        url: `/reports/assets/filter_labels`,
        data: {
          "name": val
        },
        success: function (res) {
          $(`#${key}`).hide();
          self.props.update_state(res);
        }
      });

    } else {
      $.ajax({
        type: "PATCH",
        url: `/reports/assets/filter_labels/${Number(key)}`,
        data: {
          "name": val
        },
        success: function (res) {
          $(`#${key}`).hide();
          self.props.update_state(res);
        }
      });
    }

  }

  deleteFilter(filter, e) {
    let that = this;
    //let id = e.target.dataset.valueId;
    let id = filter.id;

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      let url = `/reports/assets/delete_myfilter/${id}`;
      let a = fetch(`${url}`, {credentials: 'same-origin'});
      that.props.update_filters(filter);
    });
  }

  // Keep extractFilterData in sync with updateCurrentFiltersState in filter_report.es6.jsx
  extractFilterData(filter_state) {
    let selected_filters = JSON.parse(filter_state);
    let parsed_filters = [];
    if(selected_filters != null) {
      (selected_filters.sitesSelected || []).map(function (s) {
        parsed_filters = parsed_filters.concat(`Site: ${s.name}`);
      });

      if (selected_filters.sitesWithinMiles) {
        parsed_filters = parsed_filters.concat(`Sites within: ${selected_filters.sitesWithinMiles.label}`);
      }

      (selected_filters.categoriesSelected || []).map(function (c) {
        if (selected_filters.subcategoriesSelected[c.name] && selected_filters.subcategoriesSelected[c.name].length > 0) {
          selected_filters.subcategoriesSelected[c.name].map(function (sc) {
            parsed_filters = parsed_filters.concat(`Category: ${c.name} > Subcategory: ${sc.name}`);
          });
        } else {
          parsed_filters = parsed_filters.concat(`Category: ${c.name}`);
        }
      });

      (selected_filters.groupsSelected || []).map(function (g) {
        parsed_filters = parsed_filters.concat(`Group: ${g.name}`);
      });

      (selected_filters.assetNamesEntered || []).map(function (an) {
        parsed_filters = parsed_filters.concat(`Asset Name: ${an.label}`);
      });

      (selected_filters.trackingCodesChecked || []).map(function (tc) {
        parsed_filters = parsed_filters.concat(tc);
      });

      (selected_filters.usersSelected || []).map(function (u) {
        parsed_filters = parsed_filters.concat(`User: ${u.name}`);
      });

      (selected_filters.driversSelected || []).map(function (d) {
        parsed_filters = parsed_filters.concat(`Driver: ${d.name}`);
      });

      (selected_filters.assetManagementTypesChecked || []).map(function (am) {
        parsed_filters = parsed_filters.concat(am);
      });

      (selected_filters.locationsEntered || []).map(function (l) {
        parsed_filters = parsed_filters.concat(`Location: ${l.label}`);
      });

      Object.keys(selected_filters.readingsFilters || {}).map(function (f) {
        let readings = selected_filters.readingsFilters[f];

        Object.keys(readings).map(function (r) {
          let reading = readings[r];
          let filter;
          if (reading.filterOption) {
            switch (reading.filterOption.label) {
              case 'All':
                parsed_filters = parsed_filters.concat(`${reading.name}: All`);
                break;
              case 'Greater Than':
                if (reading.filterValue.length > 0) {
                  parsed_filters = parsed_filters.concat(`${reading.name} > ${reading.filterValue}`);
                }
                break;
              case 'Less Than':
                if (reading.filterValue.length > 0) {
                  parsed_filters = parsed_filters.concat(`${reading.name} < ${reading.filterValue}`);
                }
                break;
              case 'Equal To':
                if (reading.filterValue.length > 0) {
                  parsed_filters = parsed_filters.concat(`${reading.name} = ${reading.filterValue}`);
                }
                break;
              case 'Moving':
                parsed_filters = parsed_filters.concat(`${reading.name} is Moving`);
                break;
              case 'Not Moving':
                parsed_filters = parsed_filters.concat(`${reading.name} is Not Moving`);
                break;
              case 'Text Contains':
                if (reading.filterValue.length > 0) {
                  parsed_filters = parsed_filters.concat(`${reading.name} contains '${reading.filterValue}'`);
                }
                break;
              case 'Exactly Matches':
                if (reading.filterValue.length > 0) {
                  parsed_filters = parsed_filters.concat(`${reading.name} matches '${reading.filterValue}'`);
                }
                break;
              case 'On':
                parsed_filters = parsed_filters.concat(`${reading.name} is On`);
                break;
              case 'Off':
                parsed_filters = parsed_filters.concat(`${reading.name} is Off`);
                break;
            }
          }
        });
      });

      (selected_filters.alarmsSelected || []).map(function (a) {
        parsed_filters = parsed_filters.concat(`Alarm: ${a.label}`);
      });

      if (selected_filters.notesFilterSelected && selected_filters.notesFilterSelected.label.length > 0) {
        let label = '';
        if (selected_filters.notesFilterSelected.value == 'all') {
          label = `Notes`;
        } else {
          label = `Notes contains: '${selected_filters.notesTextEntered}'`
        }
        parsed_filters = parsed_filters.concat(label);
      }

      if (selected_filters.maintenanceSelected && selected_filters.maintenanceSelected.label.length > 0) {
        parsed_filters = parsed_filters.concat(`Maintenance: ${selected_filters.maintenanceSelected.label}`);
      }

      if (selected_filters.reallocationsFilterSelected && selected_filters.reallocationsFilterSelected.label.length > 0) {
        let label = '';
        if (selected_filters.reallocationsFilterSelected.value == 'all') {
          label = `Reallocations`;
        } else {
          label = `Reallocations contains: '${selected_filters.reallocationsTextEntered}'`
        }
        parsed_filters = parsed_filters.concat(label);
      }

      if (selected_filters.gateScansFilterSelected && selected_filters.gateScansFilterSelected.label.length > 0) {
        let label = '';
        if (selected_filters.gateScansFilterSelected.value == 'all') {
          label = `Gate Scan`;
        } else {
          label = `Gate Scan contains: '${selected_filters.gateScansTextEntered}'`
        }
        parsed_filters = parsed_filters.concat(label);
      }

      Object.keys(selected_filters.valueFilters || {}).map(function (f) {
        let values = selected_filters.valueFilters[f];

        Object.keys(values).map(function (v) {
          let value = values[v];
          let filter;
          if (value.filterOption) {
            switch (value.filterOption.label) {
              case 'All':
                parsed_filters = parsed_filters.concat(`${value.name}: All`);
                break;
              case 'Greater Than':
                if (value.filterValue.length > 0) {
                  parsed_filters = parsed_filters.concat(`${value.name} > ${value.filterValue}`);
                }
                break;
              case 'Less Than':
                if (value.filterValue.length > 0) {
                  parsed_filters = parsed_filters.concat(`${value.name} < ${value.filterValue}`);
                }
                break;
              case 'Equal To':
                if (value.filterValue.length > 0) {
                  parsed_filters = parsed_filters.concat(`${value.name} = ${value.filterValue}`);
                }
                break;
            }
          }
        });
      });
    }

    // return('<span class="filter">' + parsed_filters.join('</span><span class="filter">') + '</span>');
    return (
      <div style={{lineHeight: 'normal'}} dangerouslySetInnerHTML={{__html: '<span class="myfilters-filter">' + parsed_filters.join('</span><span class="myfilters-filter">') + '</span>'}}></div>
    );
  }

  render() {
    let self = this;
    return (<div className="col s12">
      {Object.keys(this.props.filters).map(function (key) {
        let label_name = self.labelName(key);
        let uniq_label_id = _.uniqueId('filter_label_');
        if ((self.props.filters)[key].length > 0) {

          return (
            <div className="row saved-my-filter-label" key={_.uniqueId('filter_label_')}>
              <div className="col s12">
                <div className="card-panel saved-my-filter-label-items">
                  <Collapsible trigger={<div>{label_name}<i className={cx("material-icons", {hidden: self.props.recommended})} style={{fontSize: 'initial', marginLeft: '5px', cursor: 'pointer'}}
                                                            data-value-id={key} onClick={(e) => { $('#' + uniq_label_id).toggle(); e.preventDefault(); e.stopPropagation();}}>edit</i></div>} open={true}>
                    <div className="row" id={uniq_label_id} style={{display: 'none'}}>
                      <div className="col s3">
                        <input type="text" className="filter-label-change" defaultValue={label_name}/>
                      </div>
                      <div className="col s1">
                        <button style={{marginTop: '5px'}} className="btn waves-effect waves-light" onClick={() => self.enterLabel($(`#${uniq_label_id} input:first`).val(), key, self)}>Save<i className="material-icons right">save</i></button>
                      </div>
                    </div>

                    {(self.props.filters)[key].map(function (filter) {
                      var time_period;
                      if (self.props.recommended) {
                        time_period = 'Last ' + filter.duration + ' Days';
                      } else {
                        time_period = moment(filter.start_date).format("MM/DD/YYYY") + ' - ' + moment(filter.end_date).format("MM/DD/YYYY");
                      }

                      return (
                        <div className="row saved-my-filter" key={_.uniqueId('saved_filter_')}>
                          <div className="col s3">
                            {filter.name}
                          </div>
                          <div className="col s4">
                            {self.extractFilterData(filter.filter_json)}
                          </div>
                          <div className={cx("col", {s3: self.props.recommended}, {s2: !self.props.recommended})}>
                            {time_period}
                          </div>
                          <div className="col s2">
                            <a href={`/reports/assets/filters/${filter.id}#asset-reports`} onClick={() => location.reload(true)}>View Report</a>
                          </div>
                          <div className={cx("col", "s1", {hidden: self.props.recommended})}>
                            <i className="material-icons float-right delete-saved-filter delete-icon" aria-hidden="true"
                               onClick={(e) => self.deleteFilter(filter, e)}>delete</i>
                          </div>
                        </div>);
                    })}
                  </Collapsible>
                </div>
              </div>
            </div>
          );
        }
      })}

    </div>);
  }
}

class Myfilters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: this.props.filters || {},
      filter_labels: this.props.filter_labels || [],
      filter_label_obj: this.props.filter_label_obj || {},
      filters_arr: this.props.filters_arr,
      value: null,
      final_obj: null
    };

    this.onChange = this.onChange.bind(this);
    this.filterRecords = this.filterRecords.bind(this);
    this.updateFilters = this.updateFilters.bind(this);
    this.updateState = this.updateState.bind(this);
  }

  onChange(value) {
    this.setState({value: value});
    if (value == null || value.length == 0) {
      this.setState({final_obj: this.props.filters});
    } else {
      this.filterRecords(value);
    }
  }

  updateFilters(filter) {
    let filters_arr = this.state.filters_arr.filter(item => item.id !== filter.id);
    this.setState({filters_arr: filters_arr});
    if (this.state.value) {
      let value = this.state.value.filter(item => item.id !== filter.id);
      this.setState({value: value});
      this.filterRecords(value);
    } else {
      let id = filter.analytic_filter_label_id;
      let id_val = "";
      if (id) {
        id_val = id;
      }
      let fts = this.props.filters;
      let arr = fts[id_val];
      arr = arr.filter(item => item.id !== filter.id);
      fts[id_val] = arr;
      this.setState({filters: fts});
    }
  }

  filterRecords(value) {
    let fin = {};
    for (i = 0; i < value.length; i++) {
      if (Object.keys(this.props.filters).includes((value[i].analytic_filter_label_id || "").toString())) {
        let label_id = value[i].analytic_filter_label_id;
        if (label_id) {
          label_id = label_id.toString();
        } else {
          label_id = "";
        }
        if (Object.keys(fin || {}).includes(label_id)) {
          let existing_arr = fin[label_id];
          let final_arr = existing_arr.concat(value[i]);
          fin[label_id] = final_arr;
        } else {
          fin[label_id] = [].concat(value[i]);
        }
      }
    }
    this.setState({final_obj: fin});
  }

  updateState(response) {
    if (response.filters && response.filter_labels && response.filter_label_obj && response.filters_arr) {
      this.setState({
        filters: response.filters,
        filter_labels: response.filter_labels,
        filter_label_obj: response.filter_label_obj,
        filters_arr: response.filters_arr,
      });
    }

  }

  render() {
    let self = this;
    return (<div className="row">
      <div className="col s12">
        <div className="row">
          <div className="col s12">
            <div className="card-panel">
              <div className="card-panel-heading">{self.props.title}</div>
              <div>
                <Select
                  multi={true}
                  placeholder='Search Reports'
                  valueKey='id'
                  labelKey='name'
                  value={this.state.value}
                  onChange={this.onChange}
                  options={this.state.filters_arr}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <FilterDisplay
        filters={self.state.final_obj || self.props.filters}
        filter_label_obj={this.state.filter_label_obj}
        update_filters={this.updateFilters}
        recommended={this.props.recommended}
        update_state={this.updateState}
      />
    </div>);
  }

}

module.exports = Myfilters;

