import cx from 'classnames';

class MaterialIcon extends React.Component {
  constructor(props) {
    super(props);
    this.onItemClicked = this.onItemClicked.bind(this);
  }

  onItemClicked(e) {
    this.props.onClick(e);
  }

  render () {
    const { className, name } = this.props;
    return (
      <i
        className={cx('material-icons', className)}
        aria-hidden="true"
        onClick={this.onItemClicked}
      >
        {name}
      </i>
    );
  }
}

MaterialIcon.propTypes = {
  className: React.PropTypes.string,
  name: React.PropTypes.string,
  onClick: React.PropTypes.func,
};

export default MaterialIcon;
