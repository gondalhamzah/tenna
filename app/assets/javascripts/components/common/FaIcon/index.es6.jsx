import cx from 'classnames';

class FaIcon extends React.Component {
  constructor(props) {
    super(props);
    this.onItemClicked = this.onItemClicked.bind(this);
  }

  onItemClicked(e) {
    this.props.onClick(e);
  }

  render () {
    const { className, name } = this.props;
    return (
      <i
        className={cx('fa', className, `fa-${name.toLowerCase()}`)}
        aria-hidden="true"
        onClick={this.onItemClicked}
      />
    );
  }
}

FaIcon.propTypes = {
  className: React.PropTypes.string,
  name: React.PropTypes.string,
  onClick: React.PropTypes.func,
};

export default FaIcon;
