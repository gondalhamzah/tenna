import cx from 'classnames';

class Link extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    const { className, name, to, children } = this.props;
    return (
      <a
        href={to ? to : 'javascript:void(0);'}
        onClick={this.props.onClick}
        className={className}
      >
        {children}
      </a>
    );
  }
}

Link.propTypes = {
  to: React.PropTypes.string,
  className: React.PropTypes.string,
  name: React.PropTypes.string,
  onClick: React.PropTypes.func,
  children: React.PropTypes.any,
};

export default Link;
