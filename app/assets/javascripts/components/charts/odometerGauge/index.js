import numeral from 'numeral';
export default class OdometerGauge extends React.Component {
  render () {
    const { data } = this.props;
    const valueString = numeral(data.value).format('0.0a');
    return (
      <div className="odometerGauge">
        <div className="odometerGauge__chart">
          <div className="odometerGauge__value">
            {valueString}
          </div>
          <div className="odometerGauge__unit">
            {data.unit}
          </div>
        </div>
      </div>
    );
  }
}
