import cx from 'classnames';
export default class BarGauge extends React.Component {
  render () {
    const { data } = this.props;
    return (
      <div className="barGauge">
        <div className={cx("barGauge__chart", {'barGauge__chart--active': data.value === true})}>
          &nbsp;
        </div>
      </div>
    );
  }
}
