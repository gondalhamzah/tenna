import Highcharts from 'highcharts';
import {
  HighchartsChart, Series, XAxis, YAxis, Title, Tooltip, withHighcharts
} from 'react-jsx-highcharts';
import MoreModule from 'highcharts/highcharts-more';
import SolidGaugeModule from 'highcharts/modules/solid-gauge';

MoreModule(Highcharts);
SolidGaugeModule(Highcharts);
window.Highcharts = Highcharts;

class HalfSolidGauge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      plotOptions: {
        solidgauge: {
          innerRadius: '80%',
          dataLabels: {
            y: 5,
            borderWidth: 0,
            useHTML: true
          },
        },
      },
      paneOptions: {
        center: ['50%', '70%'],
        size: '110%',
        startAngle: -90,
        endAngle: 90,
        background: {
          backgroundColor: '#EEE',
          innerRadius: '80%',
          outerRadius: '100%',
          shape: 'arc'
        },
      },
    };
  }
  render () {
    const { plotOptions, paneOptions } = this.state;
    const { data } = this.props;
    let color = '#00cd34';
    switch(data.severity) {
      case 'high':
        color = '#cc0001';
        break;
      case 'medium':
        color = '#cc9900';
        break;
      case 'low':
        color = '#00cd34';
        break;
    }
    return (
      <div
        style={{
          borderBottom: '1px solid #cccccc',
        }}
      >
        <HighchartsChart
          chart={{
            type: 'solidgauge',
            height: 250,
          }}
          plotOptions={plotOptions}
          pane={paneOptions}
        >
          <XAxis />

          <YAxis
            id="myAxis"
            min={data.min ? data.min.value : 0}
            max={data.max ? data.max.value : 0}
            lineWidth={0}
            minorTickInterval={null}
            tickAmount={2}
            title={null}
            labels={{ enabled: false }}
            stops={[
              [0, color], // green
            ]}
          >

            <Series
              id="gauge-test"
              name="Speed"
              data={[data.value]}
              dataLabels= {{
                y: -40,
                format: '<div style="text-align:center"><span style="font-size:40px;color:' +
                  'black' + '">{y}</span><br/>' +
                    '<span style="font-size:12px;color:696969">' + data.unit + '</span></div>'
              }}
              tooltip={{
                valueSuffix: ` ${data.unit}`
              }}
              type="solidgauge" />
          </YAxis>
        </HighchartsChart>
      </div>
    );
  }
}
export default withHighcharts(HalfSolidGauge, Highcharts);