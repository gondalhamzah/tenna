import cx from 'classnames';
export default class ProgressGauge extends React.Component {
  render () {
    const { data } = this.props;
    const graphData = [];
    const activeItemClass = `progressGauge__item--${data.severity || 'low'}`;
    for (let i = 0; i < 15; i ++) {
      if ((i + 1) * 100 / 15 < data.value * 100) {
        graphData.push(true);
      } else {
        graphData.push(false);
      }
    }
    
    return (
      <div className="progressGauge">
        <div className="progressGauge__chart">
          {graphData.map(item => (
            <div className={cx('progressGauge__item', item && activeItemClass)}>
              &nbsp;
            </div>
          ))}
        </div>
      </div>
    );
  }
}
