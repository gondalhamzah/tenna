export default class FilterRuleDivider extends React.Component {
  render () {
    return (
      <div className="rule-row">
        <div className={`rule-divider rule-cell rule-divider-${this.props.word}`}>
          <div className="divider-line-element"></div>
          <div className="divider-word">{this.props.word}</div>
        </div>
      </div>
    );
  }
}

FilterRuleDivider.propTypes = {
  word: React.PropTypes.string
};
