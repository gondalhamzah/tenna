import FilterGroups from './filter_groups.es6.jsx';

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for(var i=0; i < 5; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

class Analytics extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dropMenuType: "",
      start_date: props.filterObject ? props.filterObject.start_date : null,
      end_date: props.filterObject ? props.filterObject.end_date : null,
      duration: props.filterObject ? props.filterObject.duration : null,
      filterObject: props.filterObject || {},
      filterLabel: props.filterLabel || null
    };

    if (props.chat && props.series) {
      this.renderData(props.html, JSON.parse(props.chart), JSON.parse(props.series));
    }

    if (props.geoData) {
      window.historyGEOData = JSON.parse(props.geoData);
    } else {
      window.historyGEOData = [];
    }
    const that = this;

    // $('#asset_start_date, #asset_end_date').change(function() {
    //   if ($(this).val()) {
    //     that.handleShowData();
    //   }
    //
    //   that.setState({
    //     start_date: $('input[name="asset[start_date]"]').val(),
    //     end_date: $('input[name="asset[end_date]"]').val(),
    //   });
    // });

    $(document).on('click', '#go-table-view' , function() {
      $('a[href="#table-view"]').trigger('click')
    });
    // this.handleShowData();
    this.pendingRequestId = null
  }

  // componentWillMount() {
  //   var categoryGroups = [];
  //   var categories = [];
  //   this.props.categoryGroup.map(value => {
  //     categoryGroups.push({
  //       label: value.name,
  //       value: value.name,
  //       type: value.id.toString()
  //     });
  //   });
  //   this.props.category.map(value => {
  //     categories.push({
  //       label: value.name,
  //       value: value.name,
  //       type: value.category_group_id.toString()
  //     });
  //   });
  //   Analytics.qualifiers = $.merge(Analytics.qualifiers, categories)
  //
  //   this.setState({
  //     catKeys: categoryGroups
  //   });
  // }

  // handleGroupRuleDelete(groupId, ruleId) {
  //   const groupIdx = _.findIndex(this.state.groups, {id: groupId});
  //   const ruleIdx = _.findIndex(this.state.groups[groupIdx].rules, {id: ruleId});
  //   if (this.state.groups[groupIdx].rules.length == 1) {
  //     this.setState({
  //       groups: [
  //         ...this.state.groups.slice(0, groupIdx),
  //         ...this.state.groups.slice(groupIdx+1)
  //       ]
  //     }, ()=>this.handleShowData());
  //   } else {
  //     this.setState({
  //       groups: [
  //         ...this.state.groups.slice(0, groupIdx),
  //         Object.assign(
  //           {},
  //           this.state.groups[groupIdx],
  //           {
  //             rules: [
  //               ...this.state.groups[groupIdx].rules.slice(0, ruleIdx),
  //               ...this.state.groups[groupIdx].rules.slice(ruleIdx+1)
  //             ]
  //           }
  //         ),
  //         ...this.state.groups.slice(groupIdx+1)
  //       ]
  //     }, ()=>this.handleShowData());
  //   }
  // }
  //
  // handleGroupRuleChange(groupId, ruleId, field, val) {
  //   const groupIdx = _.findIndex(this.state.groups, {id: groupId});
  //   const ruleIdx = _.findIndex(this.state.groups[groupIdx].rules, {id: ruleId});
  //   const currentRule = Object.assign({}, this.state.groups[groupIdx].rules[ruleIdx], {[field]: val});
  //   if (field == 'key' && !currentRule.oper) {
  //     currentRule['oper'] = 'exists';
  //   }
  //
  //   var currentGroup = this.state.groups[groupIdx];
  //   if (currentGroup.groupType=="categorybtn" && field=='key') {
  //     currentRule['catType'] = 'gcat';
  //   } else if (currentGroup.groupType=="categorybtn" && field=='oper') {
  //     currentRule['catType'] = 'cat';
  //   }
  //
  //   this.setState({
  //     groups: [
  //       ...this.state.groups.slice(0, groupIdx),
  //       Object.assign(
  //         {},
  //         this.state.groups[groupIdx],
  //         {
  //           rules: [
  //             ...this.state.groups[groupIdx].rules.slice(0, ruleIdx),
  //             currentRule,
  //             ...this.state.groups[groupIdx].rules.slice(ruleIdx+1)
  //           ]
  //         }
  //       ),
  //       ...this.state.groups.slice(groupIdx+1)
  //     ]
  //   }, ()=> {
  //     if (currentRule.oper && currentRule.key &&
  //       currentRule.key.length > 0 && currentRule.oper.length > 0 &&
  //       (['exists', 'not_exists', 'req', 'comp','contributed','fulfilled','rfid','qr','upc','cellular','lora','sigfox','bluetooth'].indexOf(currentRule.oper) > -1 ||
  //       currentRule.value.length > 0 || currentRule['catType'] == 'cat')) {
  //       this.handleShowData();
  //     }
  //   })
  // }
  //
  // handleAddRule(groupId) {
  //   const groupIdx = _.findIndex(this.state.groups, {id: groupId});
  //   this.setState({
  //     groups: [
  //       ...this.state.groups.slice(0, groupIdx),
  //       Object.assign(
  //         {},
  //         this.state.groups[groupIdx],
  //         {
  //           rules: [
  //             ...this.state.groups[groupIdx].rules,
  //             {id: makeid(), key: '', oper: '', value: ''}
  //           ]
  //         }
  //       ),
  //       ...this.state.groups.slice(groupIdx+1)
  //     ]
  //   });
  // }
  //
  // handleAddGroup(filterBtn) {
  //   if (filterBtn == "categorybtn"){
  //     this.setState({
  //       dropMenuType: filterBtn,
  //       groups: [
  //         ...this.state.groups,
  //         {id: makeid(), rules: [{id: makeid(), key: '', oper: '', value: '', catType: ''}], groupType: filterBtn}
  //       ]
  //     });
  //   } else {
  //     this.setState({
  //       dropMenuType: filterBtn,
  //       groups: [
  //         ...this.state.groups,
  //         {id: makeid(), rules: [{id: makeid(), key: '', oper: '', value: ''}], groupType: filterBtn}
  //       ]
  //     });
  //   }
  // }

  renderData(html, chart, series) {
    series = series.map((s)=>{
      return {
        name: s.name,
        data: s.data.map((d)=>[Date.parse(d[0]), d[1]])
      }
    });

    $('#table-view').html(html);

    if (series.length > 0) {
      chart['series'] = [...series];
      const columnViewShow = $('#column-view').css('display');
      const lineViewShow = $('#line-view').css('display');
      const scatterViewShow = $('#scatter-view').css('display');
      $('#line-view').show();
      $('#line-view').highcharts(chart);

      chart.title.text = 'Column Chart';
      chart['chart']['type'] = 'column';
      $('#column-view').show();
      $('#column-view').highcharts(chart);

      chart.title.text = 'Scatter Plot Chart';
      chart['chart'] = {
        type: 'scatter',
        zoomType: 'xy'
      };
      chart['plotOptions']['scatter'] = {
        tooltip: {
          headerFormat: '<b>{series.name}</b><br>',
          pointFormat: '{point.y}'
        }
      };
      $('#scatter-view').show();
      $('#scatter-view').highcharts(chart);
      $('.tabs a.active').trigger('click');
      $('#line-view').css('display', lineViewShow);
      $('#column-view').css('display', columnViewShow);
      $('#scatter-view').css('display', scatterViewShow);
    } else {
      $('.numeric-view').html('<br>No numeric data is available. Click '+
        '<a href="javascript:void(0);" id="go-table-view">here</a>'+
        ' to see text data.');
    }
  }

  // handleShowData() {
  //   if (this.pendingRequestId) {
  //     clearTimeout(this.pendingRequestId)
  //   }
  //   this.pendingRequestId = setTimeout(()=>{
  //     this.pendingRequestId = null;
  //     var url = '';
  //     if (this.props.assetId) {
  //       url = '/analytics.json?asset_id='+this.props.assetId;
  //     }
  //     if (this.props.companyId) {
  //       url = '/analytics.json?company_id='+this.props.companyId;
  //     }
  //
  //     $.ajax({
  //       type: 'POST',
  //       url: url,
  //       data: JSON.stringify({
  //         groups: this.state.groups,
  //         start_date: $('input[name="asset[start_date]"]').val(),
  //         end_date: $('input[name="asset[end_date]"]').val()
  //       }),
  //       contentType: 'application/json',
  //       dataType: 'json',
  //       success: (data) => {
  //         this.renderData(data.html, data.chart, data.series);
  //         this.setState({csvData: this.makedata(data.csvData)});
  //         if (window.historyMapInited) {
  //           window.clearHistoryMarkers();
  //           _.each(data.geoData, (info)=>addHistoryMarker(info));
  //         } else {
  //           window.historyGEOData = [...data.geoData];
  //         }
  //       }
  //     })
  //   })
  // }

  makedata(prop) {
    for (var i = prop.length - 1; i >= 0; i--) {
      if(prop[i][3])
        prop[i][3] = prop[i][3].replace(/\,/g,"");
      if(prop[i][4])
        prop[i][4] = prop[i][4].replace(/\,/g,"");
    }
    return prop;
  }

  showCurrentDate() {
    this.daypicker.showMonth(this.state.month);
  }

  render () {
    const { selectedDay } = this.state;
    return (
      <div>
        <FilterGroups
          // catKeys={this.state.catKeys}
          // catQualifiers={this.state.catQualifiers}
          // dropMenuType={this.state.dropMenuType}
          // groups={this.state.groups}
          // handleShowData={this.handleShowData.bind(this)}
          // handleAddGroup={this.handleAddGroup.bind(this)}
          // handleAddRule={this.handleAddRule.bind(this)}
          // handleGroupRuleDelete={this.handleGroupRuleDelete.bind(this)}
          // handleGroupRuleChange={this.handleGroupRuleChange.bind(this)}
          filterObject={this.state.filterObject}
          filterLabel={this.state.filterLabel}
          // csvData={this.state.csvData}
          assetAnalytics={this.props.assetAnalytics}
          startDate={this.state.start_date}
          endDate={this.state.end_date}
          duration={this.state.duration}
          companyId={this.props.companyId}
          assetId={this.props.assetId}
        />
      </div>
    );
  }
}

/*
Analytics.attr_keys = [
  {label: 'RFID Gate', value: 'rfid gate', type: 'gate'},
  {label: 'Maintenance', value: 'mt', type: 'mt'},
  {label: 'Notes', value: 'notes', type: 'nt'},
  {label: 'Site', value: 'project_id', type: 'pro'},
  {label: 'Group', value: 'group_id', type: 'gr'},
  {label: 'User', value: 'whodunnit_name', type: 'user'},
  {label: 'Price', value: 'price', type: 'pri'},
  {label: 'Wanted', value: 'want', type: 'want'},
  {label: 'Market Value', value: 'market_value', type: 'mv'},
];

Analytics.qualifiers = [
  {label: 'Exists', value: 'exists', type: 'mt,hm,nt,ilu,tags,trackers,gr,pri,mv,gate,want'},
  {label: 'Requested', value: 'req', type: 'mt'},
  {label: 'Contributed', value: 'contributed', type: 'want'},
  {label: 'Fulfilled', value: 'fulfilled', type: 'want'},
  {label: 'Completed', value: 'comp', type: 'mt'},
  {label: 'RFID', value: 'rfid', type: 'tags'},
  {label: 'QR', value: 'qr', type: 'tags'},
  {label: 'UPC', value: 'upc', type: 'tags'},
  {label: 'SerialNumber', value: 'serialnumber', type: 'tags'},
  {label: 'Cellular', value: 'cellular', type: 'trackers'},
  {label: 'LoRa', value: 'lora', type: 'trackers'},
  {label: 'Bluetooth', value: 'bluetooth', type: 'trackers'},
  {label: 'Sigfox', value: 'sigfox', type: 'trackers'},
  {label: 'Equal to(Number)', value: 'eq', type: 'mv,qt'},
  {label: 'Greater Than(Number)', value: 'gt', type: 'hm,pri,mv,qt'},
  {label: 'Less Than(Number)', value: 'lt', type: 'hm,pri,mv,qt'},
  {label: 'Does Not Exist', value: 'not_exists', type: 'ilu'},
  {label: 'Text Contains(Text)', value: 'contains', type: 'nt,pro,gr,user,gate'},
  {label: 'Does Not Contain(Text)', value: 'not_contains', type: 'user'},
  {label: 'Exactly Matches', value: 'matches', type: 'nt,user,gate'},
  // {label: 'Starts With', value: 'starts_with', type: ''},
  // {label: 'Ends With', value: 'ends_with', type: ''},
];

Analytics.live_keys = [
  {label: 'Inventory/Location Update', value: 'location', type: 'ilu'},
  {label: 'Hours', value: 'hours', type: 'hm'},
  {label: 'Miles', value: 'miles', type: 'hm'},
  {label: 'Quantity', value: 'quantity', type: 'qt'},
  // {label: 'Hours/Miles', value: 'hours_miles', type: 'hm'},
];

Analytics.tag_keys = [
  {label: 'Tags', value: 'tags', type: 'tags'},
  {label: 'Trackers', value: 'trackers', type: 'trackers'},
];

Analytics.propTypes = {
  assetId: React.PropTypes.number,
  companyId: React.PropTypes.number,
  dateStart: React.PropTypes.string,
  dateEnd: React.PropTypes.string,
  groups: React.PropTypes.array,
  chart: React.PropTypes.string,
  html: React.PropTypes.string,
  series: React.PropTypes.string,
  geoData: React.PropTypes.string,
  filterObject: React.PropTypes.any,
  assetAnalytics: React.PropTypes.bool,
  categoryGroup: React.PropTypes.array,
  category: React.PropTypes.array,
};
*/

module.exports = Analytics;
