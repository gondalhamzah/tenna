import Select from 'react-select'

export default class FilterRule extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      keys: []
    };
    this.state_status = [];
  }

  componentWillMount() {
    if (this.props.dropMenuType == "attrbtn") {
      this.setState({
        keys: Analytics.attr_keys
      });
    } else if (this.props.dropMenuType == "livebtn") {
      this.setState({
        keys: Analytics.live_keys
      });
    } else if (this.props.dropMenuType == "categorybtn") {
      this.setState({
        keys: this.props.catKeys
      });
    } else if (this.props.dropMenuType == "tagbtn") {
      this.setState({
        keys: Analytics.tag_keys
      });
    }
  }

  handleRuleChange(field, option) {
    if (option) {
      this.props.handleRuleChange(this.props.groupId, this.props.rule.id, field, option.value)
    } else {
      this.props.handleRuleChange(this.props.groupId, this.props.rule.id, field, null)
    }
  }

  render () {
    const {groupId} = this.props;
    const ruleId = this.props.rule.id;
    const _type = (_.find(this.state.keys, {value: this.props.rule.key}) || {type: ''}).type
    const operators = _.filter(Analytics.qualifiers, (q)=>q.type.indexOf(_type)>-1)

    return (
      <div className={"flow-rule rule-row "}>
        <div className="flowform rule-cell">
          <div className="row filterSection">
            <div className="col l4 m4 s12">
              <Select
                name="rule-key"
                onChange={(option)=>this.handleRuleChange('key', option)}
                value={this.props.rule.key}
                options={this.state.keys} />
            </div>
            <div className="col l4 m4 s12 analytics-padding">
              <Select
                disabled={_type.length == 0}
                name="rule-match"
                onChange={(option)=>this.handleRuleChange('oper', option)}
                value={this.props.rule.oper}
                options={operators} />
            </div>
            {(this.props.rule.oper == 'not_exists' ||
              this.props.rule.oper == "exists" ||
              ['bool', 'mt','want','tags','trackers'].indexOf(this.props.rule.key) > -1 ||
              this.props.rule.catType ||
              _type.length == 0 ||
              _type.indexOf('bool') > -1) ? null :
              <div className="col l4 m4 s12 analytics-padding">
                <input
                  type="text"
                  onChange={(ev)=>this.props.handleRuleChange(groupId, ruleId, 'value', ev.target.value)}
                  value={this.props.rule.value}
                  className="string required parsley-success" />
              </div>
            }
          </div>
        </div>
        <div className="rule-cell">
          <a className="btn-floating btn waves-effect waves-light red"
            onClick={(ev)=>this.props.handleRuleDelete(groupId, ruleId)}>
            <i className="material-icons">delete</i>
          </a>
        </div>
      </div>
    );
  }
}

FilterRule.propTypes = {
  handleRuleChange: React.PropTypes.func,
  handleRuleDelete: React.PropTypes.func,
  index: React.PropTypes.number,
  rule: React.PropTypes.object,
  groupId: React.PropTypes.string,
  groupIdx: React.PropTypes.number,
  catKeys: React.PropTypes.array,
  catQualifiers: React.PropTypes.array
};
