import Collapsible from 'react-collapsible';
import {Input} from 'react-materialize';
import Select from 'react-select';
import {TagBox} from 'react-tag-box';
import cx from 'classnames';

class SmartMultiSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value
    };
    this.getInitialOptions();

    this.onChange = this.onChange.bind(this);
    this.getInitialOptions = this.getInitialOptions.bind(this);
  }

  getInitialOptions() {
    fetch(`${this.props.dataUrl}`, {credentials: 'same-origin'})
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          options: json
        })
      });
  }

  onChange(value) {
    this.setState({value});
    this.props.updateFilterReportState(this.props.selectName, value);
  }

  render() {
    return (<div>
      <div className="row">
        <div className="col s12">
          <Select
            multi={true}
            placeholder={this.props.placeholder}
            valueKey={this.props.valueKey}
            labelKey={this.props.labelKey}
            value={this.props.value}
            onChange={this.onChange}
            options={this.state.options}
          />
        </div>
      </div>
    </div>);
  }
}

class SimpleSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: null,
      options: this.props.options
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    this.setState({value});
    this.props.updateFilterReportState(this.props.selectName, value);
  }

  render() {
    return (<div>
      <div className="row">
        <div className="col s12">
          <Select
            placeholder={this.props.placeholder}
            valueKey={this.props.valueKey}
            labelKey={this.props.labelKey}
            value={this.props.value}
            onChange={this.onChange}
            options={this.state.options}
            multi={this.props.multi}
          />
        </div>
      </div>
    </div>);
  }
}

class CreatableSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: null,
      options: this.props.options
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    this.setState({value});
    this.props.updateFilterReportState(this.props.selectName, value);
  }

  render() {
    return (<div>
      <div className="row">
        <div className="col s12">
          <Select.Creatable
            placeholder={this.props.placeholder}
            valueKey={this.props.valueKey}
            labelKey={this.props.labelKey}
            value={this.props.value}
            onChange={this.onChange}
            options={this.state.options}
            multi={true}
          />
        </div>
      </div>
    </div>);
  }
}

class CategorySearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: null
    };
    this.getInitialOptions();

    this.onChange = this.onChange.bind(this);
    this.removeSelection = this.removeSelection.bind(this);
    this.getInitialOptions = this.getInitialOptions.bind(this);
  }

  getInitialOptions() {
    fetch(`${this.props.dataUrl}`, {credentials: 'same-origin'})
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          options: json
        })
      });
  }

  onChange(value) {
    this.setState({
      value: value
    });
    this.props.updateFilterReportState(this.props.selectName, value);
  }

  removeSelection(e) {
    let valueId = e.target.dataset.valueId;
    let valueName = e.target.dataset.valueName;
    let valueSub = this.props.valueSub;
    delete valueSub[valueName];

    let oldValues = this.props.value;
    let newValues = oldValues.filter(v => v.id != valueId);

    this.setState({
      value: newValues
    });
    this.props.updateFilterReportState(this.props.selectName, newValues);
    this.props.updateFilterReportState(this.props.selectNameSub, valueSub);
  }

  render() {
    let self = this;

    return (
      <div>
        <div className="row">
          <div className="col s12">
            <Select
              multi={true}
              placeholder={this.props.placeholder}
              valueKey={this.props.valueKey}
              labelKey={this.props.labelKey}
              value={this.props.value}
              onChange={this.onChange}
              options={this.state.options}
            />
          </div>
        </div>
        {(this.props.value || []).map(function (category) {
            let uniq_category_id = _.uniqueId('category_filter_');
            return (
              <div key={uniq_category_id}>
                <div className="row">
                  <div className="col s12">
                    {category.name}
                    <span style={{'float': 'right'}} onClick={self.removeSelection}>
                      <i className="material-icons delete-icon" aria-hidden="true"
                         data-value-id={category.id} data-value-name={category.name}
                         style={{padding: '5px', marginTop: '-2px', fontSize: '12px', marginRight: '5px'}}>delete</i>
                    </span>
                  </div>
                </div>
                <div className="row">
                  <div className="col s12">
                    <SubcategorySearch
                      placeholder="Search Subcategories"
                      valueKey="id"
                      labelKey="name"
                      catName={category.name}
                      selectNameSub={self.props.selectNameSub}
                      valueSub={self.props.valueSub}
                      updateFilterReportState={self.props.updateFilterReportState}
                      dataUrl={`/analytics/get_subcategories_by_category_id?category_id=${category.id}`}/>
                  </div>
                </div>
              </div>
            )
          }
        )}
      </div>
    );
  }
}

class SubcategorySearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value
    };
    this.getInitialOptions();

    this.onChange = this.onChange.bind(this);
    this.getInitialOptions = this.getInitialOptions.bind(this);
  }

  getInitialOptions() {
    fetch(`${this.props.dataUrl}`, {credentials: 'same-origin'})
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          options: json
        })
      });
  }

  onChange(value) {
    this.setState({value});
    let subvalue = this.props.valueSub;
    subvalue[this.props.catName] = value;
    this.props.updateFilterReportState(this.props.selectNameSub, subvalue);
  }

  render() {
    let catName = this.props.catName;
    return (<div>
      <div className="row">
        <div className="col s12">
          <Select
            multi={true}
            placeholder={this.props.placeholder}
            valueKey={this.props.valueKey}
            labelKey={this.props.labelKey}
            value={this.props.valueSub[catName]}
            onChange={this.onChange}
            options={this.state.options}
          />
        </div>
      </div>
    </div>);
  }
}

class CheckboxGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      options: [],
      value: []
    };

    this.getInitialOptions = this.getInitialOptions.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.getInitialOptions();
  }

  getInitialOptions() {
    if (this.props.dataUrl) {
      fetch(`${this.props.dataUrl}`, {credentials: 'same-origin'})
        .then((response) => response.json())
        .then((json) => {
          this.setState({
            options: json
          });
        });
    } else {
      this.setState({
        options: this.props.dataSet
      });
    }
  }

  onChange(value) {
    let oldValues = this.props.value;
    let newValues = [];

    if (oldValues.indexOf(value.target.name) > -1) {
      newValues = oldValues.filter(v => v != value.target.name);
      this.setState({
        value: newValues
      });
      this.props.updateFilterReportState(this.props.selectName, newValues);
    } else {
      oldValues = oldValues.concat(value.target.name);
      this.setState({
        value: oldValues
      });
      this.props.updateFilterReportState(this.props.selectName, oldValues);
    }
  }

  render() {
    let self = this;
    return (<div>
        <div className="row">
          {
            self.state.options.map(function (option) {
              return (
                <div className="col s6 reports-checkbox" key={option.value} style={{padding: "7px 0 7px 15px"}}>
                  <Input
                    name={`${option.value}`}
                    type="checkbox"
                    className="filled-in"
                    label={option.label}
                    onChange={self.onChange}
                    checked={self.props.value.indexOf(option.value) > -1}
                  />
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}

class HybridSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: null,
      filters: {}
    };

    this.onChange = this.onChange.bind(this);
    this.addOptionsFilter = this.addOptionsFilter.bind(this);
    this.removeOptionsFilter = this.removeOptionsFilter.bind(this);
    this.setSuboptionValue = this.setSuboptionValue.bind(this);
  }

  setSuboptionValue(uuid, filter_type, filter_option, filter_value) {
    let filter = Object.assign({}, this.state.filters[filter_type][uuid]);

    filter['filterOption'] = filter_option;
    filter['filterValue'] = filter_value;

    let new_filters = Object.assign({}, this.state.filters);
    new_filters[filter_type][uuid] = filter;

    this.setState({
      filters: new_filters
    });
    this.props.updateFilterReportState(this.props.selectName, new_filters);
  }

  onChange(value) {
    this.addOptionsFilter(value);
  }

  addOptionsFilter(value) {
    let filter_type = value.id;
    let filters = this.state.filters;
    let filters_of_type = filters[filter_type];

    let uniqid = _.uniqueId(filter_type + '_');

    if (!filters_of_type) {
      filters[filter_type] = {};
    }
    filters[filter_type][uniqid] = value;

    this.setState({
      filters: filters
    });
    this.props.updateFilterReportState(this.props.selectName, filters);
  }

  removeOptionsFilter(el) {
    let el_data = el.target.dataset;
    let filter_to_be_removed = el_data.valueId;
    let filter_type = el_data.filterType;

    let filters = this.state.filters;

    let filters_of_type = filters[filter_type];
    delete filters_of_type[filter_to_be_removed];

    if (Object.keys(filters_of_type).length > 0) {
      filters[filter_type] = filters_of_type;
    } else {
      delete filters[filter_type];
    }

    this.setState({
      filters: filters
    });
    this.props.updateFilterReportState(this.props.selectName, filters);
  }

  getComponent(id, options) {
    if (this.state.showModal) {  // show the modal if state showModal is true
      this.setState({
        showModal: !this.state.showModal
      });
      return <HybridOptions placeholder="Select Value" valueKey="id" labelKey="name" id={id} options={options}/>;
    } else {
      return null;
    }
  }

  render() {
    let self = this;

    return (<div>
      <div className="row">
        <div className="col s12">
          <Select
            placeholder={this.props.placeholder}
            valueKey={this.props.valueKey}
            labelKey={this.props.labelKey}
            value={this.state.value}
            onChange={this.onChange}
            options={this.props.options}
          />
        </div>
      </div>

      {Object.keys(this.props.values || {}).map(function (key) {
          let options = self.props.values[key];
          return (
            <div>
              <div className="row">
                <div className="col s12">
                  {options[Object.keys(options)[0]].name}
                </div>
              </div>
              {Object.keys(options).map(function (option) {
                let option_value = options[option];
                return (
                  <div className="row">
                    <div className="col s12">
                      <HybridOptions
                        placeholder="Select Value"
                        valueKey="value"
                        labelKey="label"
                        id={option}
                        type={option_value.id}
                        options={option_value.suboptions}
                        key={option}
                        removeOptionsFilter={self.removeOptionsFilter}
                        setSuboptionValue={self.setSuboptionValue}
                        filterOption={option_value.filterOption || {label: '', value: null}}
                        filterValue={option_value.filterValue || ''}
                      />
                    </div>
                  </div>
                )
              })}
              <hr/>
            </div>
          )
        }
      )}
    </div>);
  }
}

class HybridOptions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.filterOption,
      textInputDisabled: (this.props.filterOption.label == 'All' || this.props.filterOption.label == ''),
      options: this.props.options,
      optionFilterValue: this.props.filterValue
    };

    this.onChange = this.onChange.bind(this);
    this.onTextChanged = this.onTextChanged.bind(this);
  }

  onChange(value) {
    if (value == null) {
      value = {label: '', value: ''}
    }
    let textInputDisabled = (value.label == '' || value.label == 'All');
    this.setState({
      value: value,
      textInputDisabled: textInputDisabled
    });
    this.props.setSuboptionValue(this.props.id, this.props.type, value, this.state.optionFilterValue);
  }

  onTextChanged(e) {
    let value = e.target.value;
    this.setState({
      optionFilterValue: value
    });
    this.props.setSuboptionValue(this.props.id, this.props.type, this.state.value, value);
  }
  render() {
    return (<div>
      <div className="row">
        <div className="col s10">
          <Select
            placeholder={this.props.placeholder}
            valueKey={this.props.valueKey}
            labelKey={this.props.labelKey}
            value={this.state.value.label == "" ? "" : this.state.value}
            onChange={this.onChange}
            options={this.state.options}
            id={this.props.id}
          />
        </div>
      </div>
      <div className="row">
        <div className="col s10 full-width-col">
          <Input
            type="text"
            value={this.state.optionFilterValue}
            disabled={this.state.textInputDisabled ? "disabled" : false}
            onChange={this.onTextChanged}
            key={`${this.props.id}_value`}
          />
        </div>
        <div className="col s2">
          <i className="material-icons delete-icon" aria-hidden="true"
             data-value-id={this.props.id}
             data-filter-type={this.props.type}
             onClick={this.props.removeOptionsFilter}
             style={{padding: '5px', fontSize: '27px', marginLeft: '-11px', marginTop: '7px'}}>delete</i>
        </div>
      </div>
    </div>);
  }
}

class TextSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
      textValue: this.props.textValue
    };

    this.onChange = this.onChange.bind(this);
    this.onTextChanged = this.onTextChanged.bind(this);
  }

  onChange(value) {
    if (value == null) {
      value = {label: '', value: ''}
    }
    this.setState({
      value: value
    });
    this.props.updateFilterReportState(this.props.selectName, value);
  }

  onTextChanged(e) {
    let value = e.target.value;
    this.setState({
      textValue: value
    });
    this.props.updateFilterReportState(this.props.textName, value);
  }

  render() {
    let textInputDisabled = this.props.value.label == '' || this.props.value.label == 'All' || this.props.value == ""
    return (<div>
      <div className="row">
        <div className="col s12">
          <Select
            placeholder={this.props.placeholder}
            valueKey={this.props.valueKey}
            labelKey={this.props.labelKey}
            value={this.props.value}
            onChange={this.onChange}
            options={this.props.options}
          />
        </div>
      </div>
      <div className="row" style={{display: textInputDisabled ? 'none' : true}}>
        <div className="col s12 full-width-col">
          <Input type="text" value={this.props.textValue} onChange={this.onTextChanged}/>
        </div>
      </div>
    </div>);
  }
}

class CurrentFilters extends React.Component {
  constructor(props) {
    super(props);

    this.getAllFilterTypes = this.getAllFilterTypes.bind(this);
  }

  getAllFilterTypes() {
    let all_filter_types = [];
    this.props.filters.map(function (f) {
      all_filter_types = all_filter_types.concat(JSON.parse(f.value).type);
    });
    return (_.uniq(all_filter_types));
  }

  render() {
    let allFilterTypes = this.getAllFilterTypes();

    return (
      <div id="current_filters" className="">
        <div className="card-panel-heading filter-zone">Current Filters</div>
        <div>
          <TagBox
            tags={this.props.filters}
            selected={this.props.filters}
            removeTag={this.props.removeCurrentFilter}
            backspaceDelete={false}
          />
        </div>
        <div className="fixed-filters-container">
          <ul className="fixed-filters">
            <li className={cx("fixed-filter", {hidden: allFilterTypes.includes('sitesSelected')}, {hidden: allFilterTypes.includes('sitesWithinMiles')}, {hidden: this.props.assetAnalytics})}>All Sites</li>
            <li className={cx("fixed-filter", {hidden: allFilterTypes.includes('groupsSelected')}, {hidden: this.props.assetAnalytics})}>All Groups</li>
            <li className={cx("fixed-filter", {hidden: allFilterTypes.includes('assetNamesEntered')}, {hidden: this.props.assetAnalytics})}>All Asset Names</li>
            <li className={cx("fixed-filter", {hidden: allFilterTypes.includes('usersSelected')}, {hidden: this.props.assetAnalytics})}>All Customer ID</li>
            <li className={cx("fixed-filter", {hidden: allFilterTypes.includes('categoriesSelected')}, {hidden: allFilterTypes.includes('subcategoriesSelected')}, {hidden: this.props.assetAnalytics})}>All Categories</li>
            <li className={cx("fixed-filter", {hidden: allFilterTypes.includes('trackingCodesChecked')}, {hidden: this.props.assetAnalytics})}>All Tracking Codes</li>
            <li className={cx("fixed-filter", {hidden: allFilterTypes.includes('driversSelected')}, {hidden: this.props.assetAnalytics})}>All Drivers/Operators</li>
            <li className={cx("fixed-filter", {hidden: allFilterTypes.includes('assetManagementTypesChecked')}, {hidden: this.props.assetAnalytics})}>All Asset Management</li>
            <li className="float-right" id="clear_all_filters" onClick={this.props.clearAllFilters}>Clear All</li>
          </ul>
        </div>
      </div>
    )
  }
}

export default class FilterReport extends React.Component {
  constructor(props) {
    super(props);

    this.state = Object.keys(this.props.filter_report_state).length > 0 ? this.props.filter_report_state : {
      sitesSelected: [],
      sitesWithinMiles: '',
      categoriesSelected: [],
      subcategoriesSelected: {},
      groupsSelected: [],
      assetNamesEntered: [],
      trackingCodesChecked: [],
      usersSelected: [],
      driversSelected: [],
      assetManagementTypesChecked: [],
      locationsEntered: [],

      readingsFilters: {},
      alarmsSelected: [],
      notesFilterSelected: {label: '', value: ''},
      notesTextEntered: '',
      maintenanceSelected: {label: '', value: ''},
      reallocationsFilterSelected: {label: '', value: ''},
      reallocationsTextEntered: '',
      gateScansFilterSelected: {label: '', value: ''},
      gateScansTextEntered: '',
      valueFilters: {},

      currentFilters: []
    };

    this.updateFilterReportState = this.updateFilterReportState.bind(this);
    this.removeCurrentFilter = this.removeCurrentFilter.bind(this);
    this.updateCurrentFiltersState = this.updateCurrentFiltersState.bind(this);
    this.clearAllFilters = this.clearAllFilters.bind(this);
  }

  componentDidMount() {
    this.props.onFilterChanged(this.state);
    this.updateCurrentFiltersState();
  }

  clearAllFilters() {
    this.setState({
      sitesSelected: [],
      sitesWithinMiles: '',
      categoriesSelected: [],
      subcategoriesSelected: {},
      groupsSelected: [],
      assetNamesEntered: [],
      trackingCodesChecked: [],
      usersSelected: [],
      driversSelected: [],
      assetManagementTypesChecked: [],
      locationsEntered: [],

      readingsFilters: {},
      alarmsSelected: [],
      notesFilterSelected: {label: '', value: ''},
      notesTextEntered: '',
      maintenanceSelected: {label: '', value: ''},
      reallocationsFilterSelected: {label: '', value: ''},
      reallocationsTextEntered: '',
      gateScansFilterSelected: {label: '', value: ''},
      gateScansTextEntered: '',
      valueFilters: {},

      currentFilters: []
    });
    let Clearstate = {
      sitesSelected: [],
      sitesWithinMiles: '',
      categoriesSelected: [],
      subcategoriesSelected: {},
      groupsSelected: [],
      assetNamesEntered: [],
      trackingCodesChecked: [],
      usersSelected: [],
      driversSelected: [],
      assetManagementTypesChecked: [],
      locationsEntered: [],

      readingsFilters: {},
      alarmsSelected: [],
      notesFilterSelected: {label: '', value: ''},
      notesTextEntered: '',
      maintenanceSelected: {label: '', value: ''},
      reallocationsFilterSelected: {label: '', value: ''},
      reallocationsTextEntered: '',
      gateScansFilterSelected: {label: '', value: ''},
      gateScansTextEntered: '',
      valueFilters: {},

      currentFilters: []
    }
    this.props.onFilterChanged(Clearstate);
  }

  updateFilterReportState(key, value) {
    let newState = this.state;
    newState[key] = value;

    this.setState(newState);

    this.updateCurrentFiltersState();

    this.props.onFilterChanged(newState);
  }

  // Keep updateCurrentFiltersState in sync with extractFilterData in myfilters.es6.jsx
  updateCurrentFiltersState() {
    let currentFilters = [];
    let that = this;

    this.state.sitesSelected.map(function (s) {
      let filter = {
        label: `Site: ${s.name}`,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'sitesSelected', id: s.id})
      };
      currentFilters = currentFilters.concat(filter);
    });

    if (this.state.sitesWithinMiles) {
      let filter = {
        label: `Sites within: ${this.state.sitesWithinMiles.label}`,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'sitesWithinMiles', id: this.state.sitesWithinMiles.value})
      };
      currentFilters = currentFilters.concat(filter);
    }

    this.state.categoriesSelected.map(function (c) {
      if (that.state.subcategoriesSelected[c.name] && that.state.subcategoriesSelected[c.name].length > 0) {
        that.state.subcategoriesSelected[c.name].map(function (sc) {
          let filter = {
            label: `Category: ${c.name} > Subcategory: ${sc.name}`,
            value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'subcategoriesSelected', id: sc.id, parent_id: c.id})
          };
          currentFilters = currentFilters.concat(filter);
        });
      } else {
        let filter = {
          label: `Category: ${c.name}`,
          value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'categoriesSelected', id: c.id})
        };
        currentFilters = currentFilters.concat(filter);
      }
    });

    this.state.groupsSelected.map(function (g) {
      let filter = {
        label: `Group: ${g.name}`,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'groupsSelected', id: g.id})
      };
      currentFilters = currentFilters.concat(filter);
    });

    this.state.assetNamesEntered.map(function (an) {
      let filter = {
        label: `Asset Name: ${an.label}`,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'assetNamesEntered', id: an.value})
      };
      currentFilters = currentFilters.concat(filter);
    });

    this.state.trackingCodesChecked.map(function (tc) {
      let filter = {
        label: tc,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'trackingCodesChecked', id: tc})
      };
      currentFilters = currentFilters.concat(filter);
    });

    this.state.usersSelected.map(function (u) {
      let filter = {
        label: `User: ${u.name}`,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'usersSelected', id: u.id})
      };
      currentFilters = currentFilters.concat(filter);
    });

    this.state.driversSelected.map(function (d) {
      let filter = {
        label: `Driver: ${d.name}`,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'driversSelected', id: d.id})
      };
      currentFilters = currentFilters.concat(filter);
    });

    this.state.assetManagementTypesChecked.map(function (am) {
      let filter = {
        label: am,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'assetManagementTypesChecked', id: am})
      };
      currentFilters = currentFilters.concat(filter);
    });

    this.state.locationsEntered.map(function (l) {
      let filter = {
        label: `Location: ${l.label}`,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'locationsEntered', id: l.value})
      };
      currentFilters = currentFilters.concat(filter);
    });

    Object.keys(this.state.readingsFilters).map(function (f) {
      let readings = that.state.readingsFilters[f];

      Object.keys(readings).map(function (r) {
        let reading = readings[r];
        let filter;
        if (reading.filterOption) {
          switch (reading.filterOption.label) {
            case 'All':
              filter = {
                label: `${reading.name}: All`,
                value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'readingsFilters', id: [f, r, reading]})
              };
              currentFilters = currentFilters.concat(filter);
              break;
            case 'Greater Than':
              if (reading.filterValue.length > 0) {
                filter = {
                  label: `${reading.name} > ${reading.filterValue}`,
                  value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'readingsFilters', id: [f, r, reading]})
                };
                currentFilters = currentFilters.concat(filter);
              }
              break;
            case 'Less Than':
              if (reading.filterValue.length > 0) {
                filter = {
                  label: `${reading.name} < ${reading.filterValue}`,
                  value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'readingsFilters', id: [f, r, reading]})
                };
                currentFilters = currentFilters.concat(filter);
              }
              break;
            case 'Equal To':
              if (reading.filterValue.length > 0) {
                filter = {
                  label: `${reading.name} = ${reading.filterValue}`,
                  value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'readingsFilters', id: [f, r, reading]})
                };
                currentFilters = currentFilters.concat(filter);
              }
              break;
            case 'Moving':
              filter = {
                label: `${reading.name} is Moving`,
                value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'readingsFilters', id: [f, r, reading]})
              };
              currentFilters = currentFilters.concat(filter);
              break;
            case 'Not Moving':
              filter = {
                label: `${reading.name} is Not Moving`,
                value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'readingsFilters', id: [f, r, reading]})
              };
              currentFilters = currentFilters.concat(filter);
              break;
            case 'Text Contains':
              if (reading.filterValue.length > 0) {
                filter = {
                  label: `${reading.name} contains '${reading.filterValue}'`,
                  value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'readingsFilters', id: [f, r, reading]})
                };
                currentFilters = currentFilters.concat(filter);
              }
              break;
            case 'Exactly Matches':
              if (reading.filterValue.length > 0) {
                filter = {
                  label: `${reading.name} matches '${reading.filterValue}'`,
                  value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'readingsFilters', id: [f, r, reading]})
                };
                currentFilters = currentFilters.concat(filter);
              }
              break;
            case 'On':
              filter = {
                label: `${reading.name} is On`,
                value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'readingsFilters', id: [f, r, reading]})
              };
              currentFilters = currentFilters.concat(filter);
              break;
            case 'Off':
              filter = {
                label: `${reading.name} is Off`,
                value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'readingsFilters', id: [f, r, reading]})
              };
              currentFilters = currentFilters.concat(filter);
              break;
          }
        }
      });
    });

    this.state.alarmsSelected.map(function (a) {
      let filter = {
        label: `Alarm: ${a.label}`,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'alarmsSelected', id: a.value})
      };
      currentFilters = currentFilters.concat(filter);
    });

    if (this.state.notesFilterSelected.label.length > 0) {
      let label = '';
      if (this.state.notesFilterSelected.value == 'all') {
        label = `Notes`;
      } else {
        label = `Notes contains: '${this.state.notesTextEntered}'`
      }
      let filter = {
        label: label,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'notesFilterSelected'})
      };
      currentFilters = currentFilters.concat(filter);
    }

    if (this.state.maintenanceSelected && this.state.maintenanceSelected.label.length > 0) {
      let filter = {
        label: `Maintenance: ${this.state.maintenanceSelected.label}`,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'maintenanceSelected', id: this.state.maintenanceSelected.value})
      };
      currentFilters = currentFilters.concat(filter);
    }

    if (this.state.reallocationsFilterSelected.label.length > 0) {
      let label = '';
      if (this.state.reallocationsFilterSelected.value == 'all') {
        label = `Reallocations`;
      } else {
        label = `Reallocations contains: '${this.state.reallocationsTextEntered}'`
      }
      let filter = {
        label: label,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'reallocationsFilterSelected'})
      };
      currentFilters = currentFilters.concat(filter);
    }

    if (this.state.gateScansFilterSelected.label.length > 0) {
      let label = '';
      if (this.state.gateScansFilterSelected.value == 'all') {
        label = `Gate Scan`;
      } else {
        label = `Gate Scan contains: '${this.state.gateScansTextEntered}'`
      }
      let filter = {
        label: label,
        value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'gateScansFilterSelected'})
      };
      currentFilters = currentFilters.concat(filter);
    }

    Object.keys(this.state.valueFilters).map(function (f) {
      let values = that.state.valueFilters[f];

      Object.keys(values).map(function (v) {
        let value = values[v];
        let filter;
        if (value.filterOption) {
          switch (value.filterOption.label) {
            case 'All':
              filter = {
                label: `${value.name}: All`,
                value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'valueFilters', id: [f, v, value]})
              };
              currentFilters = currentFilters.concat(filter);
              break;
            case 'Greater Than':
              if (value.filterValue.length > 0) {
                filter = {
                  label: `${value.name} > ${value.filterValue}`,
                  value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'valueFilters', id: [f, v, value]})
                };
                currentFilters = currentFilters.concat(filter);
              }
              break;
            case 'Less Than':
              if (value.filterValue.length > 0) {
                filter = {
                  label: `${value.name} < ${value.filterValue}`,
                  value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'valueFilters', id: [f, v, value]})
                };
                currentFilters = currentFilters.concat(filter);
              }
              break;
            case 'Equal To':
              if (value.filterValue.length > 0) {
                filter = {
                  label: `${value.name} = ${value.filterValue}`,
                  value: JSON.stringify({uuid: _.uniqueId('current_filter_value_'), type: 'valueFilters', id: [f, v, value]})
                };
                currentFilters = currentFilters.concat(filter);
              }
              break;
          }
        }
      });
    });

    this.setState({
      currentFilters: currentFilters
    });
  }

  removeCurrentFilter(tag) {
    let that = this;
    let value = JSON.parse(tag.value);
    let filter_type = value.type;

    switch (filter_type) {
      case 'sitesSelected':
        let sitesSelected = _.reject(this.state.sitesSelected, function (o) {
          return o.id == value.id;
        });
        this.setState({sitesSelected: sitesSelected}, this.updateCurrentFiltersState);
        break;
      case 'sitesWithinMiles':
        this.setState({sitesWithinMiles: ''}, this.updateCurrentFiltersState);
        break;
      case 'categoriesSelected':
        let categoriesSelected = _.reject(this.state.categoriesSelected, function (o) {
          return o.id == value.id;
        });
        this.setState({categoriesSelected: categoriesSelected}, this.updateCurrentFiltersState);
        break;
      case 'subcategoriesSelected':
        let category_name = _.find(this.state.categoriesSelected, function (o) {
          return o.id == value.parent_id;
        }).name;
        let subcategoriesSelected = {};
        Object.keys(this.state.subcategoriesSelected).map(function (cat_name) {
          if (cat_name == category_name) {
            subcategoriesSelected[cat_name] = _.reject(that.state.subcategoriesSelected[cat_name], function (o) {
              return o.id == value.id;
            });
          } else {
            subcategoriesSelected[cat_name] = that.state.subcategoriesSelected[cat_name];
          }
        });
        this.setState({subcategoriesSelected: subcategoriesSelected}, this.updateCurrentFiltersState);
        break;
      case 'groupsSelected':
        let groupsSelected = _.reject(this.state.groupsSelected, function (o) {
          return o.id == value.id;
        });
        this.setState({groupsSelected: groupsSelected}, this.updateCurrentFiltersState);
        break;
      case 'assetNamesEntered':
        let assetNamesEntered = _.reject(this.state.assetNamesEntered, function (o) {
          return o.value == value.id;
        });
        this.setState({assetNamesEntered: assetNamesEntered}, this.updateCurrentFiltersState);
        break;
      case 'trackingCodesChecked':
        let trackingCodesChecked = _.reject(this.state.trackingCodesChecked, function (tc) {
          return tc == value.id;
        });
        this.setState({trackingCodesChecked: trackingCodesChecked}, this.updateCurrentFiltersState);
        break;
      case 'usersSelected':
        let usersSelected = _.reject(this.state.usersSelected, function (o) {
          return o.id == value.id;
        });
        this.setState({usersSelected: usersSelected}, this.updateCurrentFiltersState);
        break;
      case 'driversSelected':
        let driversSelected = _.reject(this.state.driversSelected, function (o) {
          return o.id == value.id;
        });
        this.setState({driversSelected: driversSelected}, this.updateCurrentFiltersState);
        break;
      case 'assetManagementTypesChecked':
        let assetManagementTypesChecked = _.reject(this.state.assetManagementTypesChecked, function (am) {
          return am == value.id;
        });
        this.setState({assetManagementTypesChecked: assetManagementTypesChecked}, this.updateCurrentFiltersState);
        break;
      case 'locationsEntered':
        let locationsEntered = _.reject(this.state.locationsEntered, function (o) {
          return o.value == value.id;
        });
        this.setState({locationsEntered: locationsEntered}, this.updateCurrentFiltersState);
        break;
      case 'readingsFilters':
        let readingsFilters = {};
        Object.keys(this.state.readingsFilters).map(function (readings_type) {
          let readings_of_type = Object.assign({}, that.state.readingsFilters[readings_type]);
          if (value.id[0] == readings_type) {
            delete readings_of_type[value.id[1]];
          }
          if (Object.keys(readings_of_type).length > 0) {
            readingsFilters[readings_type] = readings_of_type;
          }
        });
        this.setState({readingsFilters: readingsFilters}, this.updateCurrentFiltersState);
        break;
      case 'alarmsSelected':
        let alarmsSelected = _.reject(this.state.alarmsSelected, function (o) {
          return o.value == value.id;
        });
        this.setState({alarmsSelected: alarmsSelected}, this.updateCurrentFiltersState);
        break;
      case 'notesFilterSelected':
        this.setState({notesFilterSelected: {label: '', value: ''}, notesTextEntered: ''}, this.updateCurrentFiltersState);
        break;
      case 'maintenanceSelected':
        this.setState({maintenanceSelected: {label: '', value: ''}}, this.updateCurrentFiltersState);
        break;
      case 'reallocationsFilterSelected':
        this.setState({reallocationsFilterSelected: {label: '', value: ''}, reallocationsTextEntered: ''}, this.updateCurrentFiltersState);
        break;
      case 'gateScansFilterSelected':
        this.setState({gateScansFilterSelected: {label: '', value: ''}, gateScansTextEntered: ''}, this.updateCurrentFiltersState);
        break;
      case 'valueFilters':
        let valueFilters = {};
        Object.keys(this.state.valueFilters).map(function (value_type) {
          let values_of_type = Object.assign({}, that.state.valueFilters[value_type]);
          if (value.id[0] == value_type) {
            delete values_of_type[value.id[1]];
          }
          if (Object.keys(values_of_type).length > 0) {
            valueFilters[value_type] = values_of_type;
          }
        });
        this.setState({valueFilters: valueFilters}, this.updateCurrentFiltersState);
        break;
    }
  }

  render() {
    const miles = [
      {label: 'Exact', value: 'Exact'},
      {label: '5', value: 5},
      {label: '10', value: 10},
      {label: '25', value: 25},
      {label: '50', value: 50},
      {label: '100', value: 100},
      {label: '250', value: 250}
    ];
    const asset_management_options = [
      {label: 'Owned', value: 'owned'},
      {label: 'Available', value: 'available'},
      {label: 'Rentals', value: 'rentals'},
      {label: 'In Use', value: 'in_use'},
      {label: 'Sold', value: 'sold'},
      {label: 'For Rent', value: 'for_rent'},
      {label: 'Abandoned', value: 'abandoned'},
      {label: 'For Sale', value: 'for_sale'}
    ];
    const alarms_options = [
      {label: 'Speeding', value: 'speeding'},
      {label: 'Low Voltage', value: 'low_voltage'},
      {label: 'High Coolant Temp', value: 'high_coolant_temp'},
      {label: 'Hard Acceleration', value: 'hard_acceleration'},
      {label: 'Hard Braking', value: 'hard_braking'},
      {label: 'Idle Engine', value: 'idle_engine'},
      {label: 'Towing', value: 'towing'},
      {label: 'High RPM', value: 'high_rpm'},
      {label: 'Power On', value: 'power_on'},
      {label: 'Power Off', value: 'power_off'},
      {label: 'Quick Lane Change', value: 'quick_lane_change'},
      {label: 'Sharp Turn', value: 'sharp_turn'},
      {label: 'Fatigued Driving', value: 'fatigued_driving'},
      {label: 'Ignition On', value: 'ignition_on'},
      {label: 'Ignition Off', value: 'ignition_off'}
    ];
    const maintenance_options = [
      {label: 'All', value: 'all'},
      {label: 'Requested', value: 'requested'},
      {label: 'Completed', value: 'completed'}
    ];
    const readings = [
      {
        id: 'coolant_temperature',
        name: 'Coolant temperature',
        suboptions: [{label: 'All', value: null}, {label: 'Greater Than', value: null}, {label: 'Less Than', value: null}, {label: 'Equal To', value: null}]
      },
      {id: 'odometer', name: 'Odometer', suboptions: [{label: 'All', value: null}, {label: 'Greater Than', value: null}, {label: 'Less Than', value: null}, {label: 'Equal To', value: null}]},
      {id: 'speed', name: 'Speed', suboptions: [{label: 'All', value: null}, {label: 'Greater Than', value: null}, {label: 'Less Than', value: null}, {label: 'Equal To', value: null}]},
      {id: 'fuel_level', name: 'Fuel Level', suboptions: [{label: 'All', value: null}, {label: 'Greater Than', value: null}, {label: 'Less Than', value: null}, {label: 'Equal To', value: null}]},
      {
        id: 'fuel_consumption',
        name: 'Fuel Consumption',
        suboptions: [{label: 'All', value: null}, {label: 'Greater Than', value: null}, {label: 'Less Than', value: null}, {label: 'Equal To', value: null}]
      },
      {id: 'engine_speed', name: 'Engine Speed', suboptions: [{label: 'All', value: null}, {label: 'Greater Than', value: null}, {label: 'Less Than', value: null}, {label: 'Equal To', value: null}]},
      {id: 'moving', name: 'Moving', suboptions: [{label: 'Moving', value: null}, {label: 'Not Moving', value: null}]},
      {id: 'idle_hours', name: 'Idle Hours', suboptions: [{label: 'All', value: null}, {label: 'Greater Than', value: null}, {label: 'Less Than', value: null}, {label: 'Equal To', value: null}]},
      {id: 'current_street_address', name: 'Current Street Address', suboptions: [{label: 'All', value: null}, {label: 'Text Contains', value: null}, {label: 'Exactly Matches', value: null}]},
      {id: 'course', name: 'Course', suboptions: [{label: 'Degrees?', value: null}]},
      {
        id: 'odometer_total_miles',
        name: 'Odometer (Total Miles Recorded)',
        suboptions: [{label: 'All', value: null}, {label: 'Greater Than', value: null}, {label: 'Less Than', value: null}, {label: 'Equal To', value: null}]
      },
      {id: 'seat_belt', name: 'Seat Belt', suboptions: [{label: 'On', value: null}, {label: 'Off', value: null}]}
    ];
    const price_values = [
      {
        id: 'market_value',
        name: 'Market Value',
        suboptions: [{label: 'All', value: null}, {label: 'Greater Than', value: null}, {label: 'Less Than', value: null}, {label: 'Equal To', value: null}]
      },
      {
        id: 'price',
        name: 'Price',
        suboptions: [{label: 'All', value: null}, {label: 'Greater Than', value: null}, {label: 'Less Than', value: null}, {label: 'Equal To', value: null}]
      }
    ];
    const text_options = [{label: 'All', value: 'all'}, {label: 'Text Contains', value: 'text_contains'}];
    return (
      <div className="col s12 m4 l3 card-panel">
        <div id="current-filters">
          <CurrentFilters
            filters={this.state.currentFilters}
            removeCurrentFilter={this.removeCurrentFilter}
            clearAllFilters={this.clearAllFilters}
            assetAnalytics={this.props.assetAnalytics} />

        </div>
        <div className={cx('card-panel-heading', 'filter-zone', 'line-separated', {hidden: this.props.assetAnalytics})}>Filter by Assets</div>
        <div id="collapStyle" className={cx({hidden: this.props.assetAnalytics})}>
          <Collapsible trigger="Sites" overflowWhenOpen="visible">
            <SmartMultiSearch placeholder="Search Sites" valueKey="id" labelKey="name" dataUrl="/sites/search_by_name" selectName="sitesSelected" updateFilterReportState={this.updateFilterReportState}
                              value={this.state.sitesSelected}/>

            <div className="row">
              <div className="col s6">
                <label>
                  Within Miles
                </label>
                <SimpleSelect
                  name="filter-miles"
                  options={miles}
                  placeholder="Search Sites"
                  valueKey="value"
                  labelKey="label"
                  selectName="sitesWithinMiles"
                  updateFilterReportState={this.updateFilterReportState}
                  value={this.state.sitesWithinMiles}
                />
              </div>
            </div>
          </Collapsible>
          <Collapsible trigger="Category" overflowWhenOpen="visible">
            <CategorySearch placeholder="Select a Category" valueKey="id" labelKey="name" dataUrl="/analytics/get_categories" selectName="categoriesSelected" selectNameSub="subcategoriesSelected"
                            updateFilterReportState={this.updateFilterReportState} value={this.state.categoriesSelected} valueSub={this.state.subcategoriesSelected}/>
          </Collapsible>
          <Collapsible trigger="Groups" overflowWhenOpen="visible">
            <SmartMultiSearch placeholder="Search Groups" valueKey="id" labelKey="name" dataUrl="/analytics/search_groups_by_name" selectName="groupsSelected"
                              updateFilterReportState={this.updateFilterReportState} value={this.state.groupsSelected}/>
          </Collapsible>
          <Collapsible trigger="Asset Name" overflowWhenOpen="visible">
            <CreatableSelect
              name="filter-asset-name"
              options={[]}
              placeholder="Asset Name"
              valueKey="value"
              labelKey="label"
              selectName="assetNamesEntered"
              updateFilterReportState={this.updateFilterReportState}
              value={this.state.assetNamesEntered}
            />
          </Collapsible>
          <Collapsible trigger="Tracking Code">
            <CheckboxGroup name="Tracking Code" dataUrl="/analytics/get_token_types" selectName="trackingCodesChecked" updateFilterReportState={this.updateFilterReportState}
                           value={this.state.trackingCodesChecked}/>
          </Collapsible>
          <Collapsible trigger="User" overflowWhenOpen="visible">
            <SmartMultiSearch placeholder="Search Users" valueKey="id" labelKey="name" dataUrl="/analytics/search_users_by_name" selectName="usersSelected"
                              updateFilterReportState={this.updateFilterReportState} value={this.state.usersSelected}/>
          </Collapsible>
          <Collapsible trigger="Driver/Operator" overflowWhenOpen="visible">
            <SmartMultiSearch placeholder="Search Driver/Operator" valueKey="id" labelKey="name" dataUrl="/analytics/search_users_by_name" selectName="driversSelected"
                              updateFilterReportState={this.updateFilterReportState} value={this.state.driversSelected}/>
          </Collapsible>
          <Collapsible trigger="Asset Management" overflowWhenOpen="visible">
            <CheckboxGroup name="asset-management" dataSet={asset_management_options} selectName="assetManagementTypesChecked" updateFilterReportState={this.updateFilterReportState}
                           value={this.state.assetManagementTypesChecked}/>
          </Collapsible>
          <Collapsible trigger="Location" overflowWhenOpen="visible">
            <CreatableSelect
              name="filter-location"
              options={[]}
              placeholder="Location"
              valueKey="value"
              labelKey="label"
              selectName="locationsEntered"
              updateFilterReportState={this.updateFilterReportState}
              value={this.state.locationsEntered}
            />
          </Collapsible>
        </div>
        <div className="card-panel-heading filter-zone line-separated">Filter by Events</div>
        <div id="collapStyle">
          <Collapsible trigger="Readings" overflowWhenOpen="visible">
            <p className="label_event_filters">Add Reading</p>
            <HybridSearch
              placeholder="Search Readings"
              valueKey="id"
              labelKey="name"
              options={readings}
              values={this.state.readingsFilters}
              selectName="readingsFilters"
              updateFilterReportState={this.updateFilterReportState}
            />
          </Collapsible>
          <Collapsible trigger="Alarms" overflowWhenOpen="visible">
            <p className="label_event_filters">Add Alarm</p>
            <SimpleSelect
              name="filter-alarms"
              options={alarms_options}
              placeholder="Select Alarms"
              valueKey="value"
              labelKey="label"
              multi={true}
              selectName="alarmsSelected"
              updateFilterReportState={this.updateFilterReportState}
              value={this.state.alarmsSelected}
            />
          </Collapsible>
          <Collapsible trigger="Notes" overflowWhenOpen="visible">
            <p className="label_event_filters">Add Notes</p>
            <TextSearch
              placeholder="Search Notes"
              valueKey="value"
              labelKey="label"
              options={text_options}
              selectName="notesFilterSelected"
              textName="notesTextEntered"
              updateFilterReportState={this.updateFilterReportState}
              value={this.state.notesFilterSelected.value == "" ? '' : this.state.notesFilterSelected }
              textValue={this.state.notesTextEntered}
            />
          </Collapsible>
          <Collapsible trigger="Maintenance" overflowWhenOpen="visible">
            <p className="label_event_filters">Add Maintenance</p>
            <SimpleSelect
              name="filter-maintenance"
              options={maintenance_options}
              placeholder="Select Maintenance"
              valueKey="value"
              labelKey="label"
              selectName="maintenanceSelected"
              updateFilterReportState={this.updateFilterReportState}
              value={this.state.maintenanceSelected.value == "" ? "" : this.state.maintenanceSelected}
            />
          </Collapsible>
          <Collapsible trigger="Reallocations" overflowWhenOpen="visible">
            <p className="label_event_filters">Add Reallocation</p>
            <TextSearch
              placeholder="Search Reallocations"
              valueKey="value"
              labelKey="label"
              options={text_options}
              selectName="reallocationsFilterSelected"
              textName="reallocationsTextEntered"
              updateFilterReportState={this.updateFilterReportState}
              value={this.state.reallocationsFilterSelected.value == "" ? "" : this.state.reallocationsFilterSelected}
              textValue={this.state.reallocationsTextEntered}
            />
          </Collapsible>
          <Collapsible trigger="Gate Scans" overflowWhenOpen="visible">
            <p className="label_event_filters">Add Gate Scan</p>
            <TextSearch
              placeholder="Search Gate Scans"
              valueKey="value"
              labelKey="label"
              options={text_options}
              selectName="gateScansFilterSelected"
              textName="gateScansTextEntered"
              updateFilterReportState={this.updateFilterReportState}
              value={this.state.gateScansFilterSelected.value == "" ? "" : this.state.gateScansFilterSelected}
              textValue={this.state.gateScansTextEntered}
            />
          </Collapsible>
          <Collapsible trigger="Value" overflowWhenOpen="visible">
            <p className="label_event_filters">Add Value</p>
            <HybridSearch
              placeholder="Select Value"
              valueKey="id"
              labelKey="name"
              options={price_values}
              values={this.state.valueFilters}
              selectName="valueFilters"
              updateFilterReportState={this.updateFilterReportState}
            />
          </Collapsible>
          <br/>
          <br/>
        </div>
      </div>
    )
  }
}
