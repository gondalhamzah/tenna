import Collapsible from 'react-collapsible';
import {Input} from 'react-materialize';
import {Button} from 'react-materialize';
import Select from 'react-select';
import RecentFilteredAssets from './recent_filtered_assets.es6.jsx'
import {CSVLink, CSVDownload} from 'react-csv';
import cx from 'classnames';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import Autocomplete from 'react-google-autocomplete';
import { RingLoader,RotateLoader,CircleLoader ,SyncLoader} from 'react-spinners';
import EquipmentMap from "./equipment_map.es6.jsx"
import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps";


var randomize = require('randomatic');

class CreatableSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value || null
    };

    this.getInitialOptions();
    this.onChange = this.onChange.bind(this);
    this.getInitialOptions = this.getInitialOptions.bind(this);
  }

  getInitialOptions() {
    fetch(`${this.props.dataUrl}`, {credentials: 'same-origin'})
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          options: json
        })
      });
  }

  onChange(value) {
    this.setState({value});
    if (value) {
      this.props.updateLabelName(value.name);
    } else {
      this.props.updateLabelName(null);
    }
  }

  render() {
    return (<div style={{"overflowWhenOpen": "visible"}}>
      <Select.Creatable
        placeholder={this.props.placeholder}
        valueKey={this.props.valueKey}
        labelKey={this.props.labelKey}
        value={this.state.value}
        onChange={this.onChange}
        options={this.state.options}
        multi={false}
      />
    </div>);
  }
}

export default class FilteredAssets extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expandedRows: [],
      whichLeave: [],
      allLeaves: [],
      filterLabel: this.props.filterLabel,
      filterLabelName: (props.filterLabel != null) ? props.filterLabel.name : null,
      allFilteredData: [],
      filteredData: [],
      pageNumber: 0,
      dailyRows: [],
      dailyTabState: [],
      filter_name: this.props.filter_name || null,
      start_date: this.props.start_date || null,
      end_date: this.props.end_date || null,
      duration: this.props.duration || null,
      Autocomplete: [],
      AssetIds: [],
      lat: 40.518715,
      lng: -74.412095,
      location: "Asset Location",
      title: "Asset Title",
      operator: "operator",
      image: "",
    };

    this.saveOrUpdateFilter = this.saveOrUpdateFilter.bind(this);
    this.updateLabelName = this.updateLabelName.bind(this);
    this.getLocation = this.getLocation.bind(this);
  }

  componentWillReceiveProps(props) {
    this.state.filteredData = [];
    if (props.filteredData.assets && props.filteredData.assets.length > 0) {
      let page = props.pageNumber * 2;
      this.setState({allFilteredData: props.filteredData.assets, filteredData: props.filteredData.assets})
    }
  }

  componentDidMount() {
    let today = new Date();
    let NumMonthAgo = new Date();
    if(this.state.duration != null){
      NumMonthAgo.setDate(NumMonthAgo.getDate() - this.state.duration);
    }else{
      NumMonthAgo.setDate(NumMonthAgo.getDate() - 90);
    }

    let $input_start = $('.react-datepicker-start').pickadate({
      format: 'mm-dd-yyyy',
      close: 'SAVE',
      onClose: this.handleChangeStart.bind(this)
    });

    let picker_start = $input_start.pickadate('picker');
    picker_start.set('select', NumMonthAgo);

    let $input_end = $('.react-datepicker-end').pickadate({
      format: 'mm-dd-yyyy',
      max: today,
      close: 'SAVE',
      onClose: this.handleChangeEnd.bind(this)
    });

    let picker_end = $input_end.pickadate('picker');
    picker_end.set('select', today);
    this.setState({
      start_date: $('.react-datepicker-start').val(),
      end_date: $('.react-datepicker-end').val()
    });
    if(this.state.duration == null){
      if((this.state.start_date != null) && (this.state.end_date !=null)){
        let start_date = moment(this.state.start_date), end_date = moment(this.state.end_date);
        let duration = end_date.diff(start_date, 'days');
        this.setState({
          start_date: start_date.format("MM-DD-YYYY"),
          end_date: end_date.format("MM-DD-YYYY"),
          duration: duration + " days"
        });
      }else{
        let duration = moment(today).diff(moment(NumMonthAgo), 'days');
        this.setState({
          duration: duration + " days"
        });
      }
    }else{
      this.setState({
        duration: this.state.duration + " days"
      });
    }
  }

  handleRowClick(item) {
    if (item.hourevents) {
      item.hourevents[this.ObjectKey(item.hourevents)].map(hour => {
        this.state.whichLeave.map((state, index) => {
          if (hour.hour_id == state.hour_id) {
            hour.clicked = "false";
            this.state.whichLeave.splice(index, 1);
          }
        });
      });
    }
    const currentExpandedRows = this.state.expandedRows;
    const isRowCurrentlyExpanded = currentExpandedRows.includes(item.id);

    const newExpandedRows = isRowCurrentlyExpanded ?
      currentExpandedRows.filter(id => id !== item.id) :
      currentExpandedRows.concat(item.id);

    this.setState({expandedRows: newExpandedRows});
  }


  renderItem(item, index) {
    const clickCallback = () => this.handleRowClick(item);
    const whichLeaveState = this.state.whichLeave;
    var $selected_daily_node = $("#selected-daily-" + item.id);
    var $daily_round = $("#daily-round-" + item.id);
    var eventDate = this.ObjectKey(item);
    var item_id = item.id;
    var asset_id = item.asset_id;
    var events = item[this.ObjectKey(item)];
    var rowClass = ''
    var d_class = 'analytics_history_daily'
    var d_icon = ''
    var min_lat;
    var min_lat_lng;
    if (index % 2) {
      rowClass = 'greyRow'
    } else {
      rowClass = 'whiteRow'
    }
    if (this.state.expandedRows.includes(item_id)) {
      d_class = "analytics_history_selected"
      d_icon = 'selected-event'
    }
    let last_date = moment(events.last_event.created_at).format("hh:mm A");
    let first_date = moment(events.first_event.created_at).format("hh:mm A");
    const itemRows = [
      <tr className={"daily-ev-rows " + rowClass}>
        <td className="entries-font">
          <div className={"daily-icon event-circle " + d_icon} id={"daily-round-" + item_id}>D</div>
          {eventDate.replace(/-/g,'/')}
          <br/>
          <span>
						<b>Last Entry</b>: {events.last_event.type}
							</span>
          <br/>
          <span>{last_date + ' EST'}
							</span>
          <br/>
          <span className="first-entry"><b>First Entry</b>: {events.first_event.type}
							</span><br/>
          <span>{first_date + ' EST'}</span>
          <br/>
        </td>
        <td>
          {events.last_event.type}
          ({this.EntryCount(events.event_count, this.EntryType(events.last_event.type))})
          <br/>
          <span className="last_event_count">
          {events.first_event.type}
          ({this.EntryCount(events.event_count, this.EntryType(events.first_event.type))})
          </span>
        </td>
        <td>{this.ValueType(events.last_event)}<br/><br/><span className="user_label"> User: </span> <span className="status-text available bb2 assets">{events.user ? events.user : 'Auto'}</span>
        </td>
        <td className="status-text available assets">
          <a
            href={'https://maps.googleapis.com/maps/api/staticmap?zoom=19&center='+ events.location +'&size=600x300&maptype=roadmap&markers=size:mid%7Ccolor:red%%7C'+ events.location +'&key=AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0'}
            target="_blank">
            <img
                src={'https://maps.googleapis.com/maps/api/staticmap?zoom=19&center='+ events.location +'&size=600x300&maptype=roadmap&markers=size:mid%7Ccolor:red%%7C'+ events.location +'&key=AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0'}
                height='85'
                width='130'
            />
          </a>
          {events.location}
        </td>
        <td>
          <div>
          <div className={d_class} onClick={clickCallback} id={"selected-daily-" + item_id}></div>
          <Button
            className="waves-effect waves-light btn pull-right btn-bg"
            id={"daily-exp-bt-"}
            style={{'pointer-events': 'all'}}
            onClick={this.exportDaily.bind(this,item)}>
            Export
          </Button>
          </div>
        </td>
      </tr>
    ];
    if (this.state.expandedRows.includes(item_id)) {
      var self = this;
      $daily_round.addClass('selected-event');
      if (!item.hourevents) {
        $.ajax({
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          url: '/analytics/asset_hour_filter',
          type: 'POST',
          data: JSON.stringify({
            asset_id: asset_id,
            date: eventDate,
          }),
          success: function (res, textStatus, jqXhr) {
            self.state.dailyRows.map(item => {
              if (item.id == item_id) {
                item.hourevents = res
              }
            });
            self.setState({dailyRows: self.state.dailyRows});
          },
          error: function (jqXHR, textStatus, errorThrown) {
          }
        });
      }
      if (item.hourevents) {
        item.hourevents[asset_id].map(hour => {
          if (!hour.hour_id) {
            hour.hour_id = randomize('0', 9);
            if (item.expandAllNodes == true) {
              const leave = {hour_id: hour.hour_id, clicked: "true"};
              this.state.whichLeave.push(leave);
            }
          }
          itemRows.push(
            self.hourlyItems(hour, undefined, item_id)
          );
          whichLeaveState.map(leave => {
            if (leave.hour_id == hour.hour_id && leave.clicked == "true") {
              $("#hourly-round-" + hour.hour_id).addClass('selected-event');
              hour[this.ObjectKey(hour)].reverse().map(hour_ev => {
                var min_time = this.ObjectKey(hour_ev);
                hour_ev[min_time].events.map(min => {
                  this.ValueType(min);
                    min_lat = "";
                    itemRows.push(
                      this.minutesItems(min_time, hour_ev[min_time], min, hour.hour_id)
                    );
                });
              });
            }
            else {
              $("#hourly-round-" + hour.hour_id).removeClass('selected-event');
            }
          });
        });
      }
    }
    else {
      $daily_round.removeClass('selected-event');
    }
    return itemRows;
  }

  ValueLatlng(value){
  }

  ValueType(value){
    if(value.value_string){
      if (value.unit === 'Time') {
        value.value = moment(value.value_string).format('MM/DD/YYYY hh:mm A') + ' EST';
      } else {
        value.value = value.value_string;
      }
    }
    else if(value.value_numeric) {
      value.value = value.value_numeric;
    }
    else if (value.value_boolean){
     value.value = value.value_boolean;
    }
    else if (value.value_object){
      value.value = 'lat:' + value.value_object.latitude + ',' + 'lng: ' + value.value_object.longitude
    }
    else{
     value.value = value.value;
    }
    return value.value
  }

  renderRecentItem(item) {
    const itemRows = [
      <tr>
        <td>{item.date}
          <br/>
          <span>
					   {item.entry_time}
					</span>
        </td>
        <td>{item.event}</td>
        <td>{item.value}<br/><br/><span className="status-text available bb2 assets">User: {item.user}</span>
        </td>
        <td className="status-text available bb2 assets event-loc" key={"row-data-" + item.id}>{item.location}
          <div className="analytics_history_daily"></div>
        </td>
      </tr>
    ];
    return itemRows;
  }

  ObjectKey(key) {
    return Object.keys(key)[0]
  }

  EntryType(entry){
    if(entry == 'Current Lng Update.' || entry == 'Current Lng Update.'){
      entry = 'Location'
    }
    return entry
  }

  EntryCount(obj,value){
    for (var key in obj) {
        if(value == "Location"){
          return  obj["Current Lng Update."];
        }
        else if (key.includes(value)) {
            return  obj[key];
        }
    }
  }

  hourlyItems(itemHours, asset_id, item_id) {
    var length = itemHours[this.ObjectKey(itemHours)].length - 1
    var hour = itemHours[this.ObjectKey(itemHours)][length][Object.keys(itemHours[this.ObjectKey(itemHours)][length])].events[0]
    var allevents = itemHours[this.ObjectKey(itemHours)][length][Object.keys(itemHours[this.ObjectKey(itemHours)][length])].events
    var hourlocation = itemHours[this.ObjectKey(itemHours)][length][Object.keys(itemHours[this.ObjectKey(itemHours)][length])].asset
    var houruser = itemHours[this.ObjectKey(itemHours)][length][Object.keys(itemHours[this.ObjectKey(itemHours)][length])].user
    var events_count = itemHours[this.ObjectKey(itemHours)][length][Object.keys(itemHours[this.ObjectKey(itemHours)][length])].event_count
    var time = this.ObjectKey(itemHours)
    var first_entry_time = Object.keys(itemHours[this.ObjectKey(itemHours)][0])[0]
    var last_entry_time = Object.keys(itemHours[this.ObjectKey(itemHours)][length])[0]
    // var entries = itemHours[this.ObjectKey(itemHours)][0][Object.keys(itemHours[this.ObjectKey(itemHours)][0])][0]._source;
    var entries = itemHours[this.ObjectKey(itemHours)][0][Object.keys(itemHours[this.ObjectKey(itemHours)][0])].events[0];
    var hourlyRowBG = $("#selected-daily-" + item_id).closest('tr').hasClass('greyRow') ? 'greyRow' : 'whiteRow'

    var h_class = 'analytics_history_hourly'
    var h_icon = ''
    this.state.whichLeave.map(leave => {
      if (leave.hour_id == itemHours.hour_id && leave.clicked == "true") {
        h_class = 'analytics_history_selected'
        h_icon = 'selected-event'
      }
    });

    return <tr key={"row-" + randomize('0', 10)} className={"hourly-rows " + hourlyRowBG}>
      <td className="hour-event entries-font">
        <div className={"daily-icon event-circle hour-event " + h_icon} id={"hourly-round-" + itemHours.hour_id}>H</div>
        {this.ObjectKey(itemHours).split(':')[0] + ":00" + "-" + this.ObjectKey(itemHours).split(':')[0] + ":59"}
        <br/>
        <span>
						<b>Last Entry: </b>{this.EntryType(hour.type)}
					</span>
        <br/>
        <span>{last_entry_time}
					</span>
        <br/>
        <span className="first-entry"><b>First Entry: </b>{this.EntryType(entries.type)}
					</span><br/>
        <span>{first_entry_time}</span>
      </td>
      <td>
      {hour.type + '(' + this.EntryCount(events_count,this.EntryType(hour.type)) + ')'}
      <br/>
      <span className="last_event_count">
        {entries.type + '(' + this.EntryCount(events_count,this.EntryType(entries.type)) + ')'}
      </span>
      </td>
      <td>{this.ValueType(hour)}
      <br/><br/><span>User: <span
        className="status-text bb2 available assets">{houruser ? houruser : 'Auto'}</span></span></td>
      <td className="status-text available assets" key={"row-data-" + hour.hour_id}>
        <a
          href={'https://maps.googleapis.com/maps/api/staticmap?zoom=19&center='+ hourlocation.location +'&size=600x300&maptype=roadmap&markers=size:mid%7Ccolor:red%%7C'+ hourlocation.location +'&key=AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0'}
          target="_blank">
          <img
              src={'https://maps.googleapis.com/maps/api/staticmap?zoom=19&center='+ hourlocation.location +'&size=600x300&maptype=roadmap&markers=size:mid%7Ccolor:red%%7C'+ hourlocation.location +'&key=AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0'}
              height='85'
              width='130'
          />
        </a>
        {hourlocation.location}
      </td>
      <td>
        <div className={h_class} onClick={this.LeaveClicked.bind(this, itemHours)} id={"selected-hourly-" + itemHours.hour_id}></div>
      </td>
    </tr>
  }

  minutesItems(min_time, source, min, hour_id) {
    let min_ev_time = moment(min.timestamp).format("hh:mm A");
    var minClass = $("#selected-hourly-" + hour_id).closest('tr').hasClass('greyRow') ? 'greyRow' : 'whiteRow'
    return <tr key={"row-expanded-" + randomize(source.asset.location, 10) + randomize('0', 4)} className={"minutes-rows " + minClass}>
      <td className="minute-event">
        <div className="min-icon event-circle minute-event">M</div>
        {min_ev_time}
        <br/><br/><br/><br/><br/>

      </td>
      <td>{min.type}</td>
      <td>{min.value}<br/>
        <span>User: <span className="status-text bb2 available assets">{source.user ? source.user : 'Auto'}</span></span>
      </td>
      <td className="status-text available assets">
        <a
          href={'https://maps.googleapis.com/maps/api/staticmap?zoom=19&center='+ source.asset.location +'&size=600x300&maptype=roadmap&markers=size:mid%7Ccolor:red%%7C'+ source.asset.location +'&key=AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0'}
          target="_blank">
          <img
              src={'https://maps.googleapis.com/maps/api/staticmap?zoom=19&center='+ source.asset.location +'&size=600x300&maptype=roadmap&markers=size:mid%7Ccolor:red%%7C'+ source.asset.location +'&key=AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0'}
              height='85'
              width='130'
          />
        </a>
        {source.asset.location}
      </td>
      <td>
        <div className="analytics_history_minute"></div>
      </td>
    </tr>
  }

  LeaveClicked(event, hour) {
    if (this.state.allLeaves.length > 0) {
      this.state.allLeaves.map((leave, index) => {
        if (leave.hour_id == event.hour_id) {
          this.state.allLeaves.splice(index, 1);
        }
      });
    }
    if (event.clicked == undefined) {
      event.clicked = "true";
    }
    else if (event.clicked == "true") {
      event.clicked = "false";
    } else {
      event.clicked = "true";
    }
    const leave = {hour_id: event.hour_id, clicked: event.clicked};
    this.state.allLeaves.push(leave);
    this.setState({whichLeave: this.state.allLeaves});
  }

  dailyEventEndPoint(url, item_id, expandAll) {
    const asset_id = item_id;
    var self = this;
    var items = [];
    if (self.state.dailyRows.length > 0) {
      for (let i = self.state.dailyRows.length - 1; i >= 0; i -= 1) {
        if (self.state.dailyRows[i].asset_id === asset_id) {
          self.state.dailyRows.splice(i, 1);
        }
      }
    }
    $.ajax({
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      url: url,
      type: 'POST',
      data: JSON.stringify({
        asset_id: asset_id,
      }),
      success: function (res, textStatus, jqXhr) {
        res[asset_id].map(daily => {
          daily.id = randomize('0', 8);
          daily.asset_id = asset_id;
          if (expandAll == true) {
            self.state.expandedRows.push(daily.id)
            daily.expandAllNodes = true
          }
          self.state.dailyRows.push(daily);
        });
        if (expandAll == true) {
          self.setState({dailyRows: self.state.dailyRows, expandedRows: self.state.expandedRows});
        }
        else {
          self.setState({dailyRows: self.state.dailyRows});
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
      }
    });
  }

  switchRecentFiltered(item) {
    const asset_id = item.id;
    var $recent = $("#most-recent-" + asset_id);
    var $recent_bt = $("#recent-exp-bt-" + asset_id);
    var $daily = $("#daily-" + asset_id);
    var $daily_bt = $("#daily-exp-bt-" + asset_id);
    $("#daily-span-" + asset_id).removeClass("span-clr");
    $("#recent-span-" + asset_id).addClass("span-clr");
    $recent.show();
    $recent_bt.show();
    $daily.hide();
    $daily_bt.hide();
  }

  switchDailyFiltered(item, expandAll) {
    let url = '/analytics/asset_daily_filter';
    const asset_id = item;
    if (!this.state.dailyTabState.includes(asset_id) || expandAll == true) {
      this.state.dailyTabState.push(asset_id);
      this.dailyEventEndPoint(url, asset_id, expandAll);
    }
    var $daily = $("#daily-" + asset_id);
    var $daily_bt = $("#daily-exp-bt-" + asset_id);
    var $recent = $("#most-recent-" + asset_id);
    var $recent_bt = $("#recent-exp-bt-" + asset_id);
    $daily.removeClass("hide");
    $daily_bt.removeClass("hide");
    $("#daily-span-" + asset_id).addClass("span-clr");
    $("#recent-span-" + asset_id).removeClass("span-clr");
    $daily.show();
    $daily_bt.show();
    $recent.hide();
    $recent_bt.hide();
  }

  NewswitchRecentFiltered(){
    // this.state.dailyTabState = []; To fix tabs switching
    var $recent;
    var $recent_bt;
    var $daily;
    var $daily_bt;
    this.state.AssetIds.map(asset_id =>{
      $recent = $("#most-recent-" + asset_id);
      $recent_bt = $("#recent-exp-bt-" + asset_id);
      $daily = $("#daily-" + asset_id);
      $daily_bt = $("#daily-exp-bt-" + asset_id);
      // $("#daily-span-" + asset_id).removeClass("span-clr");
      $(".daily-span").removeClass("span-clr");
      // $("#recent-span-" + asset_id).addClass("span-clr");
      $(".recent-span").addClass("span-clr");
      $recent.show();
      $('.asset-detail-panel').show();
      $recent_bt.show();
      $daily.hide();
      $daily_bt.hide();
    });
  }

  NewswitchDailyFiltered(){
    let url = '/analytics/asset_daily_filter';
    this.state.AssetIds.map(asset_id =>{
      // if (!this.state.dailyTabState.includes(asset_id) || expandAll == true) {
      if (!this.state.dailyTabState.includes(asset_id)) {
        this.state.dailyTabState.push(asset_id);
        // this.dailyEventEndPoint(url, asset_id, expandAll);
        this.dailyEventEndPoint(url, asset_id, false);
      }
      var $daily = $("#daily-" + asset_id);
      var $daily_bt = $("#daily-exp-bt-" + asset_id);
      var $recent = $("#most-recent-" + asset_id);
      var $recent_bt = $("#recent-exp-bt-" + asset_id);
      $daily.removeClass("hide");
      $daily_bt.removeClass("hide");
      // $("#daily-span-" + asset_id).addClass("span-clr");
      $(".daily-span").addClass("span-clr");
      // $("#recent-span-" + asset_id).removeClass("span-clr");
      $(".recent-span").removeClass("span-clr");
      $daily.show();
      $daily_bt.show();
      $('.asset-detail-panel').hide();
      $recent.hide();
      $recent_bt.hide();
    });
  }

  expandAll() {
    this.state.expandedRows = [];
    var ln = this.state.filteredData.length - 1
    this.state.filteredData.map((asset, i) => {
      let assetId = this.ObjectKey(asset)
      this.switchDailyFiltered(assetId, true);
    });
  }

  exportPage(asset){
    let csvContent = "data:text/csv;charset=utf-8,";
    csvContent += 'Asset, Site, Group, , Date, Time, Event, Value, User, Location\n';
    asset.forEach((rowArray) => {
      let data = rowArray[this.ObjectKey(rowArray)].events;
      let asset_id = rowArray[this.ObjectKey(rowArray)].asset.id;
      let asset = rowArray[this.ObjectKey(rowArray)].asset;
      // let isDaily = this.state.dailyTabState.includes(asset_id);
      if(this.state.dailyTabState.includes(asset_id.toString()))
      {
          let dailyEvents = this.state.dailyRows.filter(function( obj ) {
            return obj.asset_id == asset_id;
          });
          let assetname = asset.title;
          let site = asset.project_name;
          let group = asset.group_name;
          dailyEvents.map(event => {
            let newRow = [];
            let eventtype = 'Daily';
            let unit = event[this.ObjectKey(event)].first_event.type;
            let value = event[this.ObjectKey(event)].first_event.value;
            let user = event[this.ObjectKey(event)].user;
            let date = moment(event[this.ObjectKey(event)].first_event.created_at).format('MM/DD/YYYY');
            let time = moment(event[this.ObjectKey(event)].first_event.created_at).format('hh:mm A');
            let location = event[this.ObjectKey(event)].location;
            let data = [assetname, site, group, eventtype, date, time, unit, value, user, location]
            csvContent += data;
            csvContent += "\n";
            if(this.state.expandedRows.includes(event.id)){
                event.hourevents[asset.id].map(hour => {
                    let length = hour[this.ObjectKey(hour)].length - 1
                    let hour_ev = hour[this.ObjectKey(hour)][length][Object.keys(hour[this.ObjectKey(hour)][length])].events;
                    let hour_time = Object.keys(hour[Object.keys(hour)[0]][0])[0];
                    let hourly_data = [assetname, site, group, ' Hourly', date, hour_time, hour_ev.type, hour_ev.value, user, location]
                    csvContent += hourly_data;
                    csvContent += "\n";
                    this.state.whichLeave.map(leave => {
                        if (leave.hour_id == hour.hour_id && leave.clicked == "true") {
                            hour[this.ObjectKey(hour)].map(hour_ev => {
                                var min_time = this.ObjectKey(hour_ev);
                                hour_ev[min_time].events.map(min_ev => {
                                  var source = min_ev._source;
                                  let minute_data = [assetname, site, group, '  Minutes', date, min_time, min_ev.type, min_ev.value, user, location]
                                  csvContent += minute_data;
                                  csvContent += "\n";
                                });
                            });
                        }
                    });
                });
            }
          });
      }
      else
      {
        data.map(recent => {
          let assetname = asset.title;
          let site = asset.project_name;
          let group = asset.group_name;
          let date = moment(recent.timestamp).format('MM/DD/YYYY');
          let time = moment(recent.timestamp).format('hh:mm A');
          let eventtype = 'Recent Event';
          let user = recent.whodunnit ? recent.whodunnit : 'Auto';
          let location = asset.location;
         // if(recent._source.tracking_info.array){
            // recent._source.tracking_info.array.map(event => {
              let unit = recent.type;
              let value = recent.value;
              let data = [assetname, site, group, eventtype, date, time, unit, value, user, location]
              csvContent += data;
              csvContent += "\n";
            // });
          // }
        });
      }
    });
    const encodedUri = encodeURI(csvContent);
    const link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "AssetRecentEventReport.csv");
    document.body.appendChild(link);
    link.click();
  }

  saveOrUpdateFilter() {
    // if (Object.keys(this.props.filter_report_state).length === 0) {
    this.props.saveOrUpdateFilter('POST', this.state.filterLabelName, this.state.start_date, this.state.end_date, this.state.duration);
    // } else {
    //     this.props.saveOrUpdateFilter('PATCH', this.state.labelName);
    // }
    //TODO: POST or PATCH?
  }

  updateLabelName(label_name) {
    this.setState({
      filterLabelName: label_name
    });
  }

  handleChangeStart() {
    if ($('#asset_start_date').val() && (new Date($('#asset_start_date').val()) > new Date($('#asset_end_date').val()))) {
      swal({
        type: 'error',
        title: 'Oops...',
        text: 'start date cannot be greater than end date',
      }).then(function () {
        $('#asset_start_date').val(null);
      });
    } else {
      let dateDiff = moment($('#asset_end_date').val()).diff(moment($('#asset_start_date').val()), 'days');
      if(isNaN(dateDiff)){
        $('#asset_reports_duration').hide();
      }else {
        let duration = dateDiff + " days";
        this.setState({
          start_date: $('#asset_start_date').val(),
          duration: duration
        });
        $('#asset_reports_duration').show();
      }
    }
    $('.react-datepicker-start').blur();
    $('.picker').blur();
  }

  handleChangeEnd() {
    if ($('#asset_end_date').val() && (new Date($('#asset_end_date').val()) < new Date($('#asset_start_date').val()))) {
      swal({
        type: 'error',
        title: 'Oops...',
        text: 'end date cannot be lesser than start date',
      }).then(function () {
        $('#asset_end_date').val(null);
      });
    } else {
      let dateDiff = moment($('#asset_end_date').val()).diff(moment($('#asset_start_date').val()), 'days');
      if(isNaN(dateDiff)){
        $('#asset_reports_duration').hide();
      }else {
        let duration = dateDiff + " days";
        this.setState({
          end_date: $('#asset_end_date').val(),
          duration: duration
        });
        $('#asset_reports_duration').show();
      }
    }
    $('.react-datepicker-end').blur();
    $('.picker').blur();
  }

  exportRecent(assetData,eventData) {

    let csvContent = "data:text/csv;charset=utf-8,";
    csvContent += 'Asset, Site, Group, , Date, Time, Event, Value, User, Location\n';
    var asset = assetData[Object.keys(assetData)[0]].asset;
    var events = assetData[Object.keys(assetData)[0]].events;
    events.reverse().forEach((rowArray) => {
      let newRow = [];
      let assetname = asset.title;
      let site = asset.project_name;
      let group = asset.group_name;
      let date = moment(rowArray.timestamp).format('MM/DD/YYYY');
      let time = moment(rowArray.timestamp).format('hh:mm A');
      let eventtype = 'Recent Event';
      let user = rowArray.whodunnit ? rowArray.whodunnit : 'Auto';
      let location = asset.location;
        let unit = rowArray.type;
        let value = rowArray.value;
        let data = [assetname, site, group, eventtype, date, time, unit, value, user, location]
        csvContent += data;
        csvContent += "\n";
    });
    const encodedUri = encodeURI(csvContent);
    const link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "AssetRecentEventReport.csv");
    document.body.appendChild(link);
    link.click();
  }

  exportDaily(dailyEvent, asset) {
    asset = dailyEvent.asset[this.ObjectKey(dailyEvent.asset)].asset;
    let csvContent = "data:text/csv;charset=utf-8,";
    csvContent += 'Asset, Site, Group, , Date, Time, Event, Value, User, Location\n';
    let newRow = [];
    let assetname = asset.title;
    let site = asset.project_name;
    let group = asset.group_name;
    let eventtype = 'Daily';
    let unit = dailyEvent[this.ObjectKey(dailyEvent)].last_event.type;
    let value = this.ValueType(dailyEvent[this.ObjectKey(dailyEvent)].last_event).toString().replace(/,/g, ' ');
    let user = dailyEvent[this.ObjectKey(dailyEvent)].user;
    let date = moment(dailyEvent[this.ObjectKey(dailyEvent)].last_event).format('MM/DD/YYYY');
    let time = moment(dailyEvent[this.ObjectKey(dailyEvent)].last_event).format('hh:mm A');
    let location = asset.location.replace(/,/g, ' ');
    let data = [assetname, site, group, eventtype, date, time, unit, value, user, location]
    csvContent += data;
    csvContent += "\n";
    if(this.state.expandedRows.includes(dailyEvent.id)){
        dailyEvent.hourevents[asset.id].map(hour => {
            let length = hour[this.ObjectKey(hour)].length - 1
            let hour_ev = hour[this.ObjectKey(hour)][length][Object.keys(hour[this.ObjectKey(hour)][length])].events[0];
            let hour_time = Object.keys(hour[Object.keys(hour)[0]][0])[0];
            let hourly_data = [assetname, site, group, ' Hourly', date, hour_time, hour_ev.type, hour_ev.value.toString().replace(/,/g, ' '), user, location]
            csvContent += hourly_data;
            csvContent += "\n";
            this.state.whichLeave.map(leave => {
                if (leave.hour_id == hour.hour_id && leave.clicked == "true") {
                    hour[this.ObjectKey(hour)].map(hour_ev => {
                        var min_time = this.ObjectKey(hour_ev);
                        hour_ev[min_time].events.map(min_ev => {
                          var source = min_ev._source;
                          let minute_data = [assetname, site, group, '  Minutes', date, min_time, min_ev.type, min_ev.value.toString().replace(/,/g, ' '), user, location]
                          csvContent += minute_data;
                          csvContent += "\n";
                        });
                    });
                }
            });
        });
    }
    const encodedUri = encodeURI(csvContent);
    const link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "AssetDailyEventReport.csv");
    document.body.appendChild(link);
    link.click();
  }

  getLocation(location, operator, title, image){
    let geocoder = new google.maps.Geocoder();
    let latitude = 0;
    let longitude = 0;
    let that = this;
    geocoder.geocode( { 'address': location}, function(results, status) {
      debugger

      if (status == google.maps.GeocoderStatus.OK) {
        latitude = results[0].geometry.location.lat();
        longitude = results[0].geometry.location.lng();
      }
      that.setState({
        lat: latitude,
        lng: longitude,
        location: location,
        title: title,
        operator: operator,
        image: image
      });
    });

  }

  render() {
    const style = {
       width: '100%',
       height: '400px'
     }
     const styleDiv = {
       height: '430px'
     }

     const latLng = [];
     let self = this;

    return (
      <div className="col s12 m8 l9 border_box main-filtered-assets">
        <div className="preloader-background">
          <div className="preloader-wrapper big active hidden">
            <div className="spinner-layer spinner-blue-only">
              <div className="circle-clipper left">
                <div className="circle"></div>
              </div>
              <div className="gap-patch">
                <div className="circle"></div>
              </div>
              <div className="circle-clipper right">
                <div className="circle"></div>
              </div>
            </div>
          </div>
        </div>
        <div>
        {document.getElementById("assets-show") == null ?
          <div className="fixedArea">
          <div className="row margin-bottom-0">
              <div className="col s6 bbn">
                <div className={cx({hidden: this.props.assetAnalytics})}>
                  <Input id="asset_filter_name" s={12} type="text" required="true" label="Report Name"
                         defaultValue={this.state.filter_name}/>
                </div>
              </div>
              <div className="col s6">
                <div className={cx({hidden: this.props.assetAnalytics})} style={{'padding-top': '38px'}}>
                  <Button onClick={this.saveOrUpdateFilter} className="btnOutline white btnsizedwhite right">Save Report</Button>
                </div>
              </div>
            </div>
            <Collapsible trigger="" overflowWhenOpen="visible" open>
              <div className="row margin-bottom-0">
                <div className="col s6" style={{'margin-top': '18px','padding-left': '19px'}}>
                  <div className={cx({hidden: this.props.assetAnalytics})}>
                    <span style={{'font-size': '14pt','color': 'black'}}>Report Label</span>
                    <CreatableSelect name="filter-label-name" options={[]} placeholder="filter label Name"
                                     valueKey="id" labelKey="name" value={this.state.filterLabel}
                                     dataUrl="/analytics/get_analytic_filter_labels"
                                     updateLabelName={this.updateLabelName}/>
                  </div>
                </div>
                <div className="col s2">
                  <div className="input-field string asset_start_date ip1 asset-report-range">
                    <Input type="text" className="react-datepicker-start report-datepicker fontsized" name="asset_start_date" required="false" id="asset_start_date"
                           value={this.state.start_date} label="Start Date"/>
                  </div>
                </div>
                <div className="col s2">
                  <div className="input-field string asset_end_date ip1 asset-report-range">
                    <Input type="text" className="react-datepicker-end report-datepicker fontsized" name="asset_end_date" required="false" id="asset_end_date"
                           value={this.state.end_date} label="End Date"/>
                  </div>
                </div>
                <div className="col s2"><div style={{margin: '0 auto', width: 'max-content', paddingTop: '35px'}}>Duration <br/>{this.state.duration}</div></div>
              </div>
              <div className="row">
                <div className="col l4 m4 s4">
                  {/*<div className={cx({hidden: this.props.assetAnalytics})}>
                    <Button onClick={this.saveOrUpdateFilter} className="btnOutline white btnsizedwhite">Save Report</Button>
                  </div>*/}
                  <Tabs>
                    <TabList>
                      <Tab>
                        <i className="fa fa-th-list"></i>
                      </Tab>
                      {/*<Tab>*/}
                        {/*<i className="fa fa-line-chart"></i>*/}
                      {/*</Tab>*/}
                      {/*<Tab>*/}
                        {/*<i className="fa fa-bar-chart"></i>*/}
                      {/*</Tab>*/}
                      <Tab>
                        <i className="fa fa-table"></i>
                      </Tab>
                      <Tab>
                        <i className="fa fa-map-o"></i>
                      </Tab>
                      <Tab>
                        <i className="fa fa-road"></i>
                      </Tab>
                    </TabList>
                    <TabPanel>
                    </TabPanel>
                    <TabPanel>
                    </TabPanel>
                    <TabPanel>
                    </TabPanel>
                    <TabPanel>
                    </TabPanel>
                    <TabPanel>
                      <EquipmentMap
                        lat={this.state.lat}
                        lng={this.state.lng}
                        location={this.state.location}
                        title={this.state.title}
                        operator={this.state.operator}
                        image={this.state.image}/>
                    </TabPanel>
                    <TabPanel>
                    <Autocomplete
                        style={{width: '90%'}}
                        onPlaceSelected={(place) => {
                          latLng.push({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()})
                          this.setState({Autocomplete: latLng})
                        }}
                    />
                      <div
                        className="col s12 m12 l12 liveData__map"
                        style={styleDiv}>
                        <Map
                          google={window.google}
                          style={style}
                          zoom={14}
                          center={this.state.Autocomplete[0]}>
                          <Marker position={this.state.Autocomplete[0]}></Marker>
                        </Map>
                      </div>
                    </TabPanel>
                  </Tabs>
                </div>
                <div className="col l8 m8 s8" style={{'padding-top': '15px'}}>
                  <Button
                    onClick={this.exportPage.bind(this, this.state.allFilteredData)}
                    className="waves-effect waves-light btn pull-right btnsized m6 btn-bg">
                    Export Page
                  </Button>

                  <Button
                    onClick={this.expandAll.bind(this)}
                    id="margin_button_25"
                    className="waves-effect waves-light btn pull-right btnsized m6 btn-bg">
                    Expand All
                  </Button>
                </div>
              </div>
                <div className="row re-border" style={{'padding': '5px'}}>
                  {/*<div className="col s6">
                    <h5 className="update_header">Updates:</h5>
                    <h6 className="text-grey">
                      <span
                        className="event-toggle span-clr helvetica-font recent-span"
                        id={"recent-span-"}
                        onClick={this.NewswitchRecentFiltered.bind(this)}>
                        Most Recent
                      </span> |
                      <span
                        id={"daily-span-"}
                        className="event-toggle helvetica-font daily-span"
                        onClick={this.NewswitchDailyFiltered.bind(this)}>
                        Daily
                      </span>
                    </h6>
                  </div>*/}
                  <div className="col s1 update_header">Updates:</div>
                  <div className="col s6">
                      <span
                        className="event-toggle span-clr helvetica-font recent-span"
                        id={"recent-span-"}
                        onClick={this.NewswitchRecentFiltered.bind(this)}>
                        Most Recent
                      </span> |
                      <span
                        id={"daily-span-"}
                        className="event-toggle helvetica-font daily-span"
                        onClick={this.NewswitchDailyFiltered.bind(this)}>
                        &nbsp;Daily
                      </span>
                  </div>
                  <div className="col s2"></div>
                </div>
            </Collapsible>
            </div>
          :
            <div>
              <div className="row margin-bottom-0">
                <div className="col s2">
                  <div className="input-field string asset_start_date ip1 asset-report-range">
                    <Input type="text" className="react-datepicker-start report-datepicker fontsized" name="asset_start_date" required="false" id="asset_start_date"
                           value={this.state.start_date} label="Start Date"/>
                  </div>
                </div>
                <div className="col s2">
                  <div className="input-field string asset_end_date ip1 asset-report-range">
                    <Input type="text" className="react-datepicker-end report-datepicker fontsized" name="asset_end_date" required="false" id="asset_end_date"
                           value={this.state.end_date} label="End Date"/>
                  </div>
                </div>
                <div className="col s2"><div style={{margin: '0 auto', width: 'max-content', paddingTop: '35px'}}>Duration <br/>{this.state.duration}</div></div>
              </div>
                            <div className="row">
                <div className="col l4 m4 s4">
                  {/*<div className={cx({hidden: this.props.assetAnalytics})}>
                    <Button onClick={this.saveOrUpdateFilter} className="btnOutline white btnsizedwhite">Save Report</Button>
                  </div>*/}
                  <Tabs>
                    <TabList>
                      <Tab>
                        <i className="fa fa-th-list"></i>
                      </Tab>
                      {/*<Tab>*/}
                        {/*<i className="fa fa-line-chart"></i>*/}
                      {/*</Tab>*/}
                      {/*<Tab>*/}
                        {/*<i className="fa fa-bar-chart"></i>*/}
                      {/*</Tab>*/}
                      <Tab>
                        <i className="fa fa-table"></i>
                      </Tab>
                      <Tab>
                        <i className="fa fa-map-o"></i>
                      </Tab>
                      <Tab>
                        <i className="fa fa-road"></i>
                      </Tab>
                    </TabList>
                    <TabPanel>
                    </TabPanel>
                    <TabPanel>
                    </TabPanel>
                    <TabPanel>
                    </TabPanel>
                    <TabPanel>
                    </TabPanel>
                    <TabPanel>
                      <EquipmentMap
                        lat={this.state.lat}
                        lng={this.state.lng}
                        location={this.state.location}
                        title={this.state.title}
                        operator={this.state.operator}
                        image={this.state.image}/>
                    </TabPanel>
                    <TabPanel>
                    <Autocomplete
                        style={{width: '90%'}}
                        onPlaceSelected={(place) => {
                          latLng.push({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()})
                          this.setState({Autocomplete: latLng})
                        }}
                    />
                      <div
                        className="col s12 m12 l12 liveData__map"
                        style={styleDiv}>
                        <Map
                          google={window.google}
                          style={style}
                          zoom={14}
                          center={this.state.Autocomplete[0]}>
                          <Marker position={this.state.Autocomplete[0]}></Marker>
                        </Map>
                      </div>
                    </TabPanel>
                  </Tabs>
                </div>
                <div className="col l8 m8 s8" style={{'padding-top': '15px'}}>
                  <Button
                    onClick={this.exportPage.bind(this, this.state.allFilteredData)}
                    className="waves-effect waves-light btn pull-right btnsized m6 btn-bg">
                    Export Page
                  </Button>

                </div>
              </div>
              <div className="row re-border" style={{'padding': '5px'}}>
                {/*<div className="col s6">
                  <h5 className="update_header">Updates:</h5>
                  <h6 className="text-grey">
                    <span
                      className="event-toggle span-clr helvetica-font recent-span"
                      id={"recent-span-"}
                      onClick={this.NewswitchRecentFiltered.bind(this)}>
                      Most Recent
                    </span> |
                    <span
                      id={"daily-span-"}
                      className="event-toggle helvetica-font daily-span"
                      onClick={this.NewswitchDailyFiltered.bind(this)}>
                      Daily
                    </span>
                  </h6>
                </div>*/}
                <div className="col s1 update_header">Updates:</div>
                <div className="col s6">
                    <span
                      className="event-toggle span-clr helvetica-font recent-span"
                      id={"recent-span-"}
                      onClick={this.NewswitchRecentFiltered.bind(this)}>
                      Most Recent
                    </span> |
                    <span
                      id={"daily-span-"}
                      className="event-toggle helvetica-font daily-span"
                      onClick={this.NewswitchDailyFiltered.bind(this)}>
                      &nbsp;Daily
                    </span>
                </div>
                <div className="col s2"></div>
              </div>
            </div>
          }
          </div>
            {this.state.filteredData.map((asset, i) => {
              let tracking_info = [];
              let assetId = this.ObjectKey(asset);
              this.state.AssetIds.push(assetId);
              let assetData = asset[this.ObjectKey(asset)].asset;
              let eventData = asset[this.ObjectKey(asset)].events;
              let allItemRows = [];
              let recentItemRows = [];
              return <div className={"col s12 card-panel right main-panel-node" + (i > 0 ? ' panel-mg' : '')}>
                      <div className="row row-pad consize asset-detail-panel">
                        <div className="col s12">
                          <div className="row">
                            <div className="col s6 m3 l3 asset_img">
                              <div className="sample_asset">
                                <div className={"asset-type " + assetData.asset_kind}></div>
                                <img src ={assetData.image_url} className="sample_asset imagesized"/>
                              </div>
                            </div>
                            <div className="col s6 m6 l6 asset_desc textsize textsized">
                              <div className="h2 t-capitalize">{assetData.title}</div>
                              <span className="text-grey">{"ID - " + assetData.id}</span>
                              <div className="b10">{assetData.project_name}</div>
                              <p className="text-grey">{assetData.description}</p>
                              <div className="status-text available bb2 assets padding-bottom-10">{assetData.status.charAt(0).toUpperCase() + assetData.status.slice(1)}</div>
                            </div>
                            <div className="col s12 m3 l3 text-right">
                              <div className="row price-marketvalue">
                                <div className="col s4 hide-on-med-and-up">&nbsp;</div>
                                <div className="col s4 m12 l12"><div className="b15">Price</div><div className="b10">${assetData.price}</div></div>
                                <div className="col s4 m12 l12"><div className="b15">Market Value</div><div className="b10">${assetData.market_value}</div></div>
                              </div>
                              <div className="status-text available bb2 assets">{assetData.site_location}</div>
                            </div>
                            <div className="col s12 m6 l6 float-right text-right">
                              <div className="row">
                                <div className="col s6 hide-on-med-and-up">&nbsp;</div>
                                <div className="col s6 m12 l12" id="scan-icons">
                                  {assetData.qr > 0 ?
                                    (<a className="tracking-ids tracking-icon">
                                      <i className="fa fa-qrcode qr-icon"
                                         aria-hidden="true"></i></a>)
                                    : null}
                                  {assetData.rfid > 0 ?
                                    (<a className="tracking-ids tracking-icon">
                                      <i className="material-icons">cast</i></a>)
                                    : null}
                                  {assetData.lora > 0 ?
                                    (<a className="tracking-ids tracking-icon">
                                      <i className="material-icons">settings_input_antenna</i></a>)
                                    : null}
                                  {assetData.sigfox > 0 ?
                                    (<a className="tracking-ids tracking-icon">
                                      <i className="material-icons">wifi_tethering</i></a>)
                                    : null}
                                  {assetData.upc > 0 ?
                                    (<a className="tracking-ids tracking-icon"><i
                                      className="material-icons">line_weight</i></a>)
                                    : null}
                                  {assetData.bt > 0 ?
                                    (<a className="tracking-ids tracking-icon">
                                      <i className="material-icons">bluetooth_searching</i></a>)
                                    : null}
                                  {assetData.cellular > 0 ?
                                    (<a className="tracking-ids tracking-icon">
                                      <i className="material-icons">speaker_phone</i></a>)
                                    : null}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row re-border">
                        {/*<div className="col s6">
                          <h5 className="update_header">Updates:</h5>
                          <h6 className="text-grey"><span className="event-toggle span-clr helvetica-font" id={"recent-span-" + assetData.id}
                                                          onClick={this.switchRecentFiltered.bind(this, assetData)}>Most Recent</span> |
                            <span id={"daily-span-" + assetData.id} className="event-toggle helvetica-font"
                                  onClick={this.switchDailyFiltered.bind(this, assetId, false)}>Daily</span></h6>
                        </div>
                        <div className="col s12">
                          <br/>


                        </div>*/}
                      </div>
                      <div id={"daily-" + assetData.id} className="hide">
                        <table className="striped analytic-table daily-analytic-table"
                               id={"daily-table-csv-" + assetData.id}>
                          <thead>
                          <tr>
                            <th>Date/Time</th>
                            <th>Event</th>
                            <th>Value</th>
                            <th>Location</th>
                            <th>
                              <Button
                                className="waves-effect waves-light btn pull-right hide btn-bg"
                                id={"daily-exp-bt-" + assetData.id}
                                style={{'pointer-events': 'all','right': '17px', 'visibility': 'hidden'}}>
                                Export Daily
                              </Button>
                            </th>
                          </tr>
                          </thead>
                          {this.state.dailyRows.length > 0 ? this.state.dailyRows.map((data, index) => {
                            if (data.asset_id == assetId) {
                              data.asset = asset
                              const perItemRows = this.renderItem(data, index);
                              allItemRows = allItemRows.concat(perItemRows);
                            }
                          }) : null}
                          <tbody>{allItemRows}</tbody>
                        </table>
                      </div>
                      <div id={"most-recent-" + assetData.id}>
                        <RecentFilteredAssets recentAsset={eventData} assetData={asset} assetId={assetId}
                                              location={assetData}
                                              getLocation={self.getLocation}
                                              ValueType={this.ValueType} ValueLatlng={this.ValueLatlng}></RecentFilteredAssets>
                      </div>
              </div>
            })}
      </div>
    )
  }
}
