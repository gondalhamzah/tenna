$(function () {
  if ((isController('groups') && isAction(['index'])) ||
      (isController('projects') && isAction(['show']))) {
    // Hide equipment specific fields for materials.
    $('#group-select-toggle').on('change', function (e) {
      e.preventDefault();
      $('input[name^="group-selected-"]').prop('checked', $(this).prop('checked'));
    });

    $('.groups-header-action').on('click', function () {
      var action = $(this).data('action');
      var selected = _.map($('input[name^="group-selected-"]:checked'), function(group) {
        return $(group).data('id');
      });

      if (selected.length > 0) {
        $('#group_action').val(action);
        $('#group_ids').val(selected);
        if (action === 'move') {
          $('#reallocate-group').modal('open');
        } else if (action === 'delete' || action == 'mark_as_closed') {
          var total = 0;
          _.each($('input[name^="group-selected-"]:checked'), function(group) {
            total += parseInt($(group).data('count'));
          });
          if (total > 0) {
            var msg = 'Some groups have associated assets. In order to ';
                msg += (action === 'delete' ? 'delete' : 'close');
                msg += ' those groups you must first reallocate all assets currently assigned to those groups';

            alert(msg);
            return;
          }
          if(window.confirm('Are you sure?')) {
            $(this).closest('form').submit();
          }
        } else {
          $(this).closest('form').submit();
        }
      } else {
        $('#select-group-error').modal('open');
      }
    });
  }

  if (isController('groups')) {
    if (isAction(['edit', 'update'])) {
      $('.select-project').on('change', function () {
        $('#modal-group-edit').modal('open');
        $('#group-edit-notes').css('display', 'block');
      });
    }
  }

  if (isController('groups') && isAction(['show'])) {
    $('#reallocate-asset-mass-action').hide();
    $('#remove-realllocate-content').click(function(){
      $('#reallocate-asset-mass-action').hide();
    });
    $('#btn-move-group').click(function() {
      $('#reallocate-group').modal('open');
    });
    $('#closed-group').click(function() {
      $('#mark-as-closed-modal').modal('open');
    });
    $('#modal-upload-spreadsheet-open').click(function() {
      $('#modal-upload-spreadsheet').modal('open');
    });
  }

  if (isController('projects') && isAction(['show'])) {
    $('#reallocate-asset-mass-action').hide();
    $('#remove-realllocate-content').click(function(){
        $('#reallocate-asset-mass-action').hide();
    });
    $('.tabs.groups .tab a').click(function() {
      var href = $(this).attr('href');
      $('#filters .card-panel').removeClass('active');
      if (href == '#tab-assets') {
        $('#filters .card-panel.project-asset-filter').addClass('active');
        $('.asset-search-nav').removeClass('hide');
        $('.group-search-nav').addClass('hide');
      } else {
        $('#filters .card-panel.project-group-filter').addClass('active');
        $('.asset-search-nav').addClass('hide');
        $('.group-search-nav').removeClass('hide');
      }
    });
  }

  $("#bulk-group-upload-modal-open").click(function(){
    $('#bulk-group-upload-modal').modal().modal('open');
  });
});
