$(function () {
  $(document).on('click', '.my-filter_delete', function(e){
    var link = $(this).attr('href');
    var value = $(this).attr('value');
    var name = $(this).attr('name');
    var rem = $(this).parent().parent().parent();
    e.preventDefault();
    e.stopPropagation();
    swal({
      title: 'Are you sure you want to delete ' + name + '?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: "No",
      confirmButtonText: 'Yes',
      closeOnConfirm: false
      }).then(function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "/reports/assets/delete_myfilter",
            type: 'POST',
            data: {id: value},
            success: function(data){
              rem.remove();
              rem = "";
            }
          });
        }
    });
      });
  var date = new Date;
  var dateToday = date.toLocaleDateString("en-au", {year: "numeric", month: "short",day: "numeric"}).replace(/\s/g,'-');

  $.each($('.report-datepicker'), function( index, value ) {
    $(value).pickadate('picker').set('max',dateToday);
  });
  if(isController("reports")){
    var date = new Date;
    var dateToday = date.toLocaleDateString("en-au", {year: "numeric", month: "short",day: "numeric"}).replace(/\s/g,'-');

    $.each($('.datepicker'), function( index, value ) {
      $(value).pickadate('picker').set('max',dateToday);
    });

    $('.date_from').change(function(){
          $('.date_to').val("");
          $('.date_to').pickadate('picker').set('min',$(this).val());
    });

    $('.date_from_asset_audit').change(function(){
      $('.date_to_asset_audit').val("");
      $('.date_to_asset_audit').pickadate('picker').set('min',$(this).val());
    });
    $cost_report = $('#report_assets_search_calculate_cost');
    $cost_report.next().attr('style','color: #a3a49e !important');
    $rental_rate = $('#report_assets_search_internal_rental_rate');
    $cost_report.attr('disabled',true);
    $($rental_rate).click(function(){
      if(this.checked){
        $cost_report.attr('disabled',false);
        // $cost_report.prop('checked',true);
        $cost_report.next().attr('style','color: #2d2a26');
      }
      else{
        $cost_report.attr('disabled',true);
        $cost_report.prop('checked',false);
        $cost_report.next().attr('style','color: #a3a49e !important');
      }
    });

    $($cost_report).click(function(){
      if(!this.checked){
        // $(this).attr('disabled',true);
        // $rental_rate.prop('checked',false);
      }
    });

    // Material checkbox hide/show
    var $filterEquipment = $('#report_filter_search_equipment');
    var $filterMaterial = $('#report_filter_search_material');
    var $materialCategories = $('.report-assets-material-categories', '.category-filters');
    var $equipmentCategories = $('.report-assets-equipment-categories', '.category-filters');
    var $allCategories = $('.report-assets-all-categories', '.category-filters');
    $filterEquipment.add($filterMaterial).click(function(){
      var mChecked = $filterMaterial.is(':checked');
      var eChecked = $filterEquipment.is(':checked');
      var $categoryFilters = $('.category-filters');
      if(mChecked && !eChecked) {
        $('.non-material-fields').slideUp();
        $categoryFilters.children().detach().end().append($materialCategories);
      } else if (eChecked && !mChecked) {
        $('.non-material-fields').slideDown();
        $categoryFilters.children().detach().end().append($equipmentCategories);
      } else {
        $('.non-material-fields').slideDown();
        $categoryFilters.children().detach().end().append($allCategories);
      }
    });
    setTimeout(function(){ // delay for dropdown initialization
      $filterEquipment.triggerHandler('click');
    }, 0);

    var $assetCategoryFilter = $('#asset-category-filter');
    var $projectFilter = $('#project-filter');
    var $reportFilterSearchEquipment = $('#report_filter_search_equipment');
    var $reportFilterSearchMaterial = $('#report_filter_search_material');
    var $projectsCollection = $('#projects-collection');
    var $categoryFilters = $('.category-filters select');

    function enableCategoryFilter() {
      var projChecked = $projectFilter.is(':checked');

      $reportFilterSearchEquipment.prop('checked', true);
      $reportFilterSearchMaterial.prop('checked', true);
    }

    $assetCategoryFilter.prop('checked', true);
    enableCategoryFilter();

    $assetCategoryFilter.click(function(){
      enableCategoryFilter();
    });

    $projectFilter.click(function(){
      var catChecked = $assetCategoryFilter.is(':checked');
      $projectsCollection.removeAttr('disabled');
    });

    $('#asset-category-filter').click(function(){
      if($(this).prop('checked')== false){
        $('#report_filter_search_equipment').prop('checked', false);
        $('#report_filter_search_material').prop('checked', false);
      }
    });

    $('.report_assets_search_asset_categories').change(function(){
      $('.select2-selection__choice').each(function(index,val){
        if(val.title == "All Categories")
        {
          $('#report_assets_search_asset_categories').select2({
            maximumSelectionLength: 1,
          });
        }
      });
    });

    $(document).on('click','.select2-selection__choice__remove',function(){
      if(this.parentElement.title == "All Categories"){
        $('#report_assets_search_asset_categories').select2({
          maximumSelectionLength: 50,
        });
      }
    });

    $('.report_assets_search_projects').change(function(){
      $('.select2-selection__choice').each(function(index,val){
        if(val.title == "All Projects")
        {
          $('#projects-collection').select2({
            maximumSelectionLength: 1,
          });
        }
      });
    });

    $(document).on('click','.select2-selection__choice__remove',function(){
      if(this.parentElement.title == "All Projects"){
        $('#projects-collection').select2({
          maximumSelectionLength: 50,
        });
      }
    });

    $('.asset-location-audit-btn').click(function() {
      var selectedProjectIds = $('#projects-collection').val();
      if (selectedProjectIds && selectedProjectIds.length >= 1) {
        $('.multi-projects-msg').show();
      } else {
        $('.multi-projects-msg').hide();
      }
    });
  }
});
$(document).ready(function(){
  if(isController("asset-reports")){
    fixAssetReportHeader();
    function fixAssetReportHeader() {
      var fixmeTop = $('.fixedArea').offset().top; // get initial position of the element
      $(window).scroll(function() {                // assign scroll event listener
        var currentScroll = $(window).scrollTop(); // get current position
        if (currentScroll >= fixmeTop) {           // apply position: fixed if you
          $('.fixedArea').css({                    // scroll to that element or below it
            position: 'fixed',
            top: '50px',
            left: '29%',
            right: '6%',
            'z-index': '10',
            background: '#fff'
          });
          $('.main-filtered-assets .Collapsible__trigger').css({
            right: '-23px',
            top: '35px',
          });
        } else {                   // apply position: static
          $('.fixedArea').css({  // if you scroll above it
            position: 'static'
          });
          $('.main-filtered-assets .Collapsible__trigger').css({
            right: '0',
            top: '60px',
          });
        }
      });
    }
  }
});