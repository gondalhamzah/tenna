/*  
  below are the data attributes that you can pass from the element
  1- data-cancel-btn
    value = true
      if its value is sent as 'true' clear button will work as the cancel button old value of field 
      will be persistent
    value = false
      if its value is sent as 'false' clear button will work as the original clear button given by
      the materialize plugin. 

  2- data-mindate
    value = true
      if its value is sent as 'true' minimum date will be set as today.
    value = false
      if its value is sent as 'true' there will be no limit of the date.
    value = 'yyyy-mm-dd'
      if its value is sent as 'the given pattern' minimum date will be set as given
    value = wrong value
      if given value is give could not be converted as date "Invalid minDate passed" will be displayed and
      there will be no limit applied on the date.
  
  3- data-hide-today
    will display the today button always if this data will not be present in element.
    value = true
      if value is true today button will be displayed.
    value = false
      if value is false today button will not be displayed.

*/

(function( $ ) {

  $.fn.buildDatePicker = function() {
    var element = $(this)
    var currentDate = element.val() || ''
    var minDate = minDateValue();
    var todayBtn = todayButtonState();
    var cancelBtn = cancelButtonState();

    var $dateElement = element.pickadate({
      format: 'mmmm d, yyyy',
      formatSubmit: 'yyyy-mm-dd',
      hiddenName: true,
      today: todayBtn,
      clear: cancelBtn,
      close: 'Save',
      selectMonths: true,
      selectYears: 10,
      onStart: function () {
        // console.log("Start is called")
        if (this.$node.val() !== '') {
          var parts = this.$node.val().split('-');
          var date = new Date(parts[0], parts[1] - 1, parts[2]);
          this.set('select', date);
          this.set('min', minDate);
          currentDate = date;
        }
        currentDate = this.$node.val();
      },
      onSet: function () {
        // console.log("onSet is called")
         if ( element.data('cancel-btn') == true ){
          $(".picker__clear").click(function(e){
            e.preventDefault();
            e.stopPropagation();
            $dateElement.pickadate('picker').set('select', currentDate);
            $dateElement.pickadate('picker').close()
          })
         }
      },
      onClose: function () {
        currentDate = $dateElement.pickadate('picker').get('select')
      }
    });

    function todayButtonState(){
      // console.log("todayButtonState called up")
      if ( element.data('hide-today') == true )
      {
        return '' 
      }
      else{
        return 'Today'
      }
    };


    function cancelButtonState(){
      // console.log("cancelButtonState called up")
      if ( element.data('cancel-btn') == true )
      {
        return 'Cancel' 
      }
      else{
        return 'Clear'
      }
    };


    function minDateValue(){
      // console.log("minDateValue called up")
      if ( element.data('mindate') ) 
      { 
        if ( element.data('mindate') == true )
        {
          return true
        }
        if ( element.data('mindate') == false )
        {
          return false
        }
        if ( element.data('mindate') != false )
        {
          var newDate = new Date( element.data('mindate') )
          if ( newDate == "Invalid Date" )
          {
            alert("Invalid minDate passed")
            return false
          }
          else{
            return newDate
          }
        }
      }
      else{
        return false
      }
    }
  
}
}( jQuery ));



$(function () {
  if( $('.datepicker').length > 0 ){
    $('.datepicker').buildDatePicker()
  }
});



// var getMinDate = function (){

// }
