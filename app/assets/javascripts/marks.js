$(function () {
    $('.mark-form').find('i').on('click', function (e) {
        e.preventDefault();

        $(this).closest('form').submit();
    });

    $('.mark-form').on('ajax:success', function (event, data) {
        $(this).attr('action', data.action);
        $(this).attr('method', data.method);
        $(this).find('input[name="_method"]').attr('value', data.method);

        if (data.method == 'post') {
            $(this).find('i').removeClass('icon-favorite-star-fill').addClass('icon-favorite-star-outline');
            Materialize.toast('Removed from favorites.', 2000);
            // If we are on the favorites index page
            // remove the row
            if ($('.controller-favorites.action-index').length > 0) {
              $(this).closest('tr').fadeOut('fast');
            }
        } else {
            $(this).find('i').removeClass('icon-favorite-star-outline').addClass('icon-favorite-star-fill');
            Materialize.toast('Added to favorites.', 2000);
        }
    });

    $('.mark-form').on('ajax:error', function () {
        Materialize.toast('An error has occurred.', 2000);
    });

});
