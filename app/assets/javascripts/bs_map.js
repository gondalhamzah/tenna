;(function(){
  "use strict";

  window.BsMap = BsMap;

  function BsMap (options) {
    var self = this;

    //properties
    self.map;

    self.map_id   = options.map_id   || 'bs-map';
    self.sidebar  = options.sidebar  || 'bs-map-sidebar';

    self.minZoom = 4;
    self.maxZoom = 13;
    self.zoom    = options.zoom    || 7;//14 for prod
    self.center  = { lat: null, lng: null };

    self.$appContainer = $(options.appContainer);
    self.$dataListContainer = $('#bs-data-list');

    self.zipCodeSearch = new ZipCodeInput({formContainer:'#location-zip-code'});

    self.markers     = self.$appContainer.data('markers');
    self.positionUrl = self.$appContainer.data('position-url');
    self.apiKey      = self.$appContainer.data('api-key');
    self.companyId   = self.$appContainer.data('company-id');
    self.creatorId   = self.$appContainer.data('creator-id');

    self.isPrivate = self.$appContainer.data('private');

    self.markerLayers = {};

    self.markerClusterGroup   = {};
    self.offCastClustersGroup = {};

    self.sidebar = {};

    self.$pinInfoContainer = $('#sidebar-marker-info');
    self.$pinInfoErrorContainer = $('#pin-info-error');

    self.timer;

    self.markersData;

    self.$currentActive = null;

    self.filters = {
      rental_assets:        true,
      owned_assets:         true,
      public_assets:        self.isPrivate,
      projects:             self.isPrivate,
      public_public_assets: true
    };

    self.init = function () {
      self.defineCenter()
        .then(function () {
          self.initMap();
          self.initMediaQuery();
          self.initMobileMapHeader();
          self.initMapScroller();
          self.initZipCodeSearch();
          self.updateMarkers()
            .then(function() {
              self.initControls();
            })
          ;
        })
      ;
    };

    self.initMapScroller = function(){
      $(".widgets .map-scroller").on("click", function(){
        var height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        $('html, body').animate({
          scrollTop: height*0.8
        }, 1000);
      });
    }

    self.initMediaQuery = function(){
      if (matchMedia) {
        var mq = window.matchMedia("screen and (max-width: 600px), screen and (max-height: 767px)");
        mq.addListener(self.mediaQueryHandler);
        if(mq.matches){
          //self.map.removeControl(self.map.zoomControl);
          $("#bs-filters").appendTo(".mobile-filters");
        }
      }
    }
    self.mediaQueryHandler = function(mq){
      if(mq.matches){
        //self.map.removeControl(self.map.zoomControl);
          $("#bs-filters").appendTo(".mobile-filters");
      }else {
        //self.map.addControl(self.map.zoomControl);
          $("#bs-filters").appendTo("#sidebar-filters");
      }
    }

    self.initMobileMapHeader = function(){
      $(".fa-filter").on("click", function(){
        $(".mobile-map-overlay").addClass("expanded")
        $(".mobile-filters, .mobile-filter-title, .mobile-filter-close").removeClass("hidden");
        $(".mobile-filter-status").addClass("hidden");
      })
      $(".fa-map-o").on("click", function(){
        $(".mobile-map-overlay").removeClass("expanded")
        $(".mobile-filters, .mobile-filter-title, .mobile-filter-close").addClass("hidden");
        $(".mobile-filter-status").removeClass("hidden");
      })
      self.updateMobileMapHeaderFilters()
    }

    self.updateMobileMapHeaderFilters = function(){
      $(".filter-owned").toggle(self.filters.owned_assets);
      $(".filter-rental").toggle(self.filters.rental_assets);
      $(".filter-public").toggle(self.filters.public_assets);
      $(".filter-others").toggle(self.filters.public_public_assets);
      $(".filter-project").toggle(self.filters.projects);
    }

    self.initZipCodeSearch = function() {
      self.zipCodeSearch.$formContainer.submit(function (e) {
        e.preventDefault();

        if (self.zipCodeSearch.isValid()) {
          self.zipCodeSearch.hideError();

          self.findPosition({ addr: self.zipCodeSearch.zipCode() }).done(function (data, status) {
            if (!data.success) {
              self.zipCodeSearch.showError();
              return;
            }
            self.driveTo(data.coordinates.lat, data.coordinates.lng);
            self.updateMarkersWithTimer();
          });
        } else {
          self.zipCodeSearch.showError();
        }
      });
    };

    self.clearPreviousActive = function() {
      if (!self.$currentActive) return;

      self.$currentActive
        .removeClass('active-cluster')
        .removeClass('active-marker')
      ;
    };

    self.driveTo = function(lat, lng, zoom) {
      self.map.setView(
        [lat, lng],
        zoom || self.map.getZoom(),
        {
          animate: true
        }
      );
    };

    self.initFilters = function() {
      var assetFiltersTrigger = $('#all_assets');
      var assetFilters = $('input.asset-filter');
      var otherAssetFilter = $('#public_public_assets');

      var updateAssetFiltersTrigger = function() {
        if (assetFilters.length === $('input.asset-filter:checked').length) {
          assetFiltersTrigger.prop('checked', true);
        } else {
          assetFiltersTrigger.prop('checked', false);
        }
        self.updateMobileMapHeaderFilters();
      };

      assetFiltersTrigger.off('click');
      assetFiltersTrigger.click(function() {
        $.each(assetFilters, function() {
          if ($(this).prop('checked') != assetFiltersTrigger.prop('checked')) {
            $(this).prop('checked', assetFiltersTrigger.prop('checked'));

            var filter_id = $(this).attr('id');

            if ($(this).prop('checked')) {
              self.markerClusterGroup.addLayer(self.markerLayers[filter_id]);
              self.filters[filter_id] = true;
            } else {
              self.markerClusterGroup.removeLayer(self.markerLayers[filter_id]);
              self.filters[filter_id] = false;
            }
          }
        });

        self.updateAllClustersEvents();
        self.updateActivePin();
      });

      $.each(self.markerLayers, function(layer_name, layer) {
        var $filter = $('input#' + layer_name);
        $filter.off('click');
        $filter.click(function() {
          if ($(this).prop('checked')) {
            self.markerClusterGroup.addLayer(layer);
            self.filters[layer_name] = true;
          } else {
            self.markerClusterGroup.removeLayer(layer);
            self.filters[layer_name] = false;
          }
          updateAssetFiltersTrigger();

          self.updateAllClustersEvents();
          self.updateActivePin();
        });
      });

      otherAssetFilter.off('click');
      otherAssetFilter.click(function() {
        var filter_id = $(this).attr('id');

        if ($(this).prop('checked')) {
          self.markerClusterGroup.addLayer(self.markerLayers[filter_id]);
          self.filters[filter_id] = true;
        } else {
          self.markerClusterGroup.removeLayer(self.markerLayers[filter_id]);
          self.filters[filter_id] = false;
        }

        self.updateAllClustersEvents();
        self.updateActivePin();
      });
    };

    self.initControls = function() {
      self.sidebar = L.control.sidebar('bs-sidebar', {position: 'right'}).addTo(self.map);

      self.initFilters();
    };
    self.defineCenter = function() {
      var d = $.Deferred();

      var coordinates = Cookies.getJSON('coordinates');
      if (coordinates) {
        self.center = { lat: coordinates.lat, lng: coordinates.lng };
        d.resolve();
      } else {
        self.getHTMLGeoPosition().done(function (pos) {
          self.findPosition(pos).done(function (data, status) {
            self.center = { lat: data.coordinates.lat, lng: data.coordinates.lng};
            d.resolve();
          });
        });
      }

      return d.promise();
    };

    self.getHTMLGeoPosition = function() {
      var d = $.Deferred();

      var pos = null;
      var timeOut = 5000;
      // Try HTML5 geolocation.
      try { //to fix ipad geolocation bug
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(
            function (position) {
              d.resolve({
                lat: position.coords.latitude,
                lng: position.coords.longitude
              });
            },
            function(error) {
              d.resolve(pos);
            },
            { maximumAge: 600000, timeout: timeOut }
          );
        } else {
          d.resolve(pos);
        }
        setTimeout(function () {//to fix firefox geolocation bug
          if(!pos){
            d.resolve(pos);
          }
        }, timeOut + 1000);
      } catch(error) {
        d.resolve(pos);
      }

      return d.promise();
    };

    self.findPosition = function(params) {
      return $.get(self.positionUrl, params);
    };

    self.defineElasticsearchZoom = function() {
      var zoom = 3;

      var currentZoom = self.map.getZoom();

      if(currentZoom >= 5 && currentZoom <= 8){
        zoom =4;
      }
      else if(currentZoom >= 9 && currentZoom <= 11){
        zoom =5;
      }
      else if(currentZoom >= 12 && currentZoom <= 14){
        zoom =6;
      }
      else if(currentZoom >= 15 && currentZoom <= 17){
        zoom =7;
      }
      else if(currentZoom >= 18){
        zoom =8;
      }

      return zoom;
    };

    self.aggregateClusterMarkers = function(cluster) {
      var aggregation = {
        owned_assets:         { count:0, firstFour: [], buckets: []},
        rental_assets:        { count:0, firstFour: [], buckets: []},
        public_assets:        { count:0, firstFour: [], buckets: []},
        projects:             { count:0, firstFour: [], buckets: []},
        public_public_assets: { count:0, firstFour: [], buckets: []}
      };

      var markers = cluster.getAllChildMarkers();

      $.each(markers, function(key, marker) {
        var markerType = marker.type;
        var currentAggr = aggregation[markerType];

        currentAggr.count += marker.markersCount;

        if (currentAggr.firstFour.length < 4) {
          currentAggr.firstFour = currentAggr.firstFour.concat(marker.firstFour);
        }
        currentAggr.buckets = currentAggr.buckets.concat(marker.buckets);
      });

      return aggregation;
    };

    self.clusterIconConstructor = function(cluster) {
      var aggregation = self.aggregateClusterMarkers(cluster);
      var activeTypes = [];
      $.each(aggregation, function(type, params) {
        if (params.count > 0) activeTypes.push(type);
      });

      switch (activeTypes.length) {

        case 1:
          var activeType = aggregation[activeTypes[0]];

          var width         = self.countSimpleClusterWidth(activeType.count),
            textColor       = options.textColor       || 'white',
            backgroundColor = options.backgroundColor || '#E69138'
            ;
          switch (activeTypes[0]) {
            case 'public_public_assets':
              backgroundColor = '#6DAA00';
              break;
            case 'projects':
              backgroundColor = '#747684';
              break;
          }

          return new L.DivIcon({
            iconSize: [width, width],
            html: '<div class="cluster-' + activeTypes[0] + '" style="text-align:center;border-radius: 50%;width:' + width + 'px;height:' + width + 'px;line-height:' + width + 'px;color:' + textColor + ';" data-first-four="' + aggregation[activeTypes[0]].firstFour + '" data-buckets="' + aggregation[activeTypes[0]].buckets + '">' + activeType.count + '</div>',
            className: 'cluster-icon'
          });
          break;

        case 2:
          var count_1 = aggregation[activeTypes[0]].count;
          var count_2 = aggregation[activeTypes[1]].count;

          var d1 = self.countSimpleClusterWidth(count_1);
          var d2 = self.countSimpleClusterWidth(count_2);

          var d1_left   = d1 - 4;
          var d1_bottom = Math.floor(d2/2 + d1/2);

          var icon = new L.DivIcon({
            iconSize:   [0, 0],
            iconAnchor: [d1, d1],
            html: '<div><div class="cluster-circle cluster-' + activeTypes[0] + '" style="width: ' + d1 + 'px;height:' + d1 + 'px;line-height:' + d1 + 'px;" data-first-four="' + aggregation[activeTypes[0]].firstFour + '" data-buckets="' + aggregation[activeTypes[0]].buckets + '">' + count_1 + '</div><div class="cluster-circle cluster-' + activeTypes[1] + '" style="width: ' + d2 + 'px;height:' + d2 + 'px;line-height:' + d2 + 'px;right:' + d1_left + 'px;bottom:' + d1_bottom + 'px" data-first-four="' + aggregation[activeTypes[1]].firstFour + '" data-buckets="' + aggregation[activeTypes[1]].buckets + '">' + count_2 + '</div></div>',
            className: 'cluster-icon'
          });
          break;

        default:
          var counts = [];
          var layers = [];
          var rs = [];
          var lefts = [];
          var bottoms = [];
          $.each(['owned_assets', 'rental_assets', 'public_assets', 'public_public_assets', 'projects'], function(key, layer_name) {
            if (aggregation[layer_name] && aggregation[layer_name].count) {
              counts.push(aggregation[layer_name].count);
              layers.push(layer_name);
              rs.push(self.countSimpleClusterWidth(aggregation[layer_name].count));
            }
          });

          lefts.push(0);
          bottoms.push(0);
          lefts.push(-(rs[2] - 4));
          bottoms.push(Math.floor(rs[1]/2 + rs[0]/2));

          switch(counts.length) {
            case 3:
              lefts.push(- Math.floor(rs[2]/2 - (rs[0] + rs[1] - 4)/2));
              bottoms.push(rs[1] + 4);
              break;

            case 4:
              bottoms[1] = Math.floor(rs[1] + rs[0]/2);
              lefts.push(0);
              bottoms.push(rs[1] + 4);
              lefts.push(-(rs[2] - 4));
              bottoms.push(bottoms[2]+rs[3]*1.5);
            case 5:
              bottoms[1] = Math.floor(rs[1] + rs[0]/2);
              lefts.push(0);
              bottoms.push(rs[1] + 4);
              lefts.push(-(rs[2] - 4));
              bottoms.push(bottoms[2]+rs[3]*1.5);
              lefts.push(rs[3] - 4);
              bottoms.push(bottoms[3]+4);
          }
          var htmlStr = '<div>';
            $.each(layers, function(i, layer_name) {
              htmlStr += ('<div class="cluster-circle cluster-'+layer_name+'" style="width: ' + rs[i] + 'px;height:' + rs[i] + 'px;line-height:'+rs[i]+'px;');
              htmlStr += ('bottom:' + bottoms[i] + 'px;');
              if (i % 2 == 0) {
                htmlStr += ('left:' + lefts[i] + 'px;');
              } else {
                htmlStr += ('right:' + lefts[i] + 'px;');
              }
              htmlStr += '" data-first-four="' + aggregation[layer_name].firstFour + '" data-buckets="' + aggregation[layer_name].buckets + '">' + counts[i] + '</div>'
            });
          htmlStr += '</div>';

          var icon = new L.DivIcon({
            iconSize:   [0, 0],
            iconAnchor: [rs[0], rs[0]],
            html: htmlStr,
            className: 'cluster-icon'
          });
      }
      return icon;
    };

    self.loadMarkerInfo = function(entity, elementsList, total, buckets, groupType) {
      $.get('/map.js', {entity: entity, render_element_id: self.$pinInfoContainer.attr('id'), elements_list: elementsList, total: total, buckets: buckets, group_type: groupType }, function(data) {
        self.sidebar.open('sidebar-marker-info');
      });
    };

    self.initPreventDragging = function(element) {
      element.on('mouseover', function () {
        self.map.dragging.disable();
      });

      // Re-enable dragging when user's cursor leaves the element
      element.on('mouseout', function () {
        self.map.dragging.enable();
      });
    };

    self.createMarker = function(markerData, coordinates, markerType, bucket) {
      var type = markerType;

      var markersCount = markerData.doc_count;
      if (markersCount == 0) {
        return;
      }
      var firstFour    = markerData.first_four.hits.hits.map(function(hit) {
        return hit._id;
      });

      if (markersCount === 1) {
        var icon = self.singleMarkerIconConstructor(type, bucket);
      } else {
        var icon = self.simpleClusterIconConstructor(type, markersCount, firstFour, bucket);
      }

      var marker = L.marker([coordinates.latitude, coordinates.longitude], {icon: icon});
      marker.markersCount = markersCount;
      marker.type = type;
      marker.firstFour = firstFour;
      marker.buckets = [bucket];

      var activeClass = markersCount > 1 ? 'active-cluster' : 'active-marker';

      var groupType = type;

      self.initPreventDragging(marker);

      if (markersCount === 1) {
        marker.on('click', function() {
          if (/assets/.test(type)) {
            self.loadMarkerInfo('Asset', marker.firstFour, markersCount, marker.buckets, groupType);
            self.clearPreviousActive();
            self.$currentActive = $(marker._icon).addClass(activeClass);
          } else {
            self.loadMarkerInfo('Project', marker.firstFour, markersCount, marker.buckets, groupType);
            self.clearPreviousActive();
            self.$currentActive = $(marker._icon).addClass(activeClass);
          }
        });
      }

      self.markerLayers[type].addLayer(marker);

      if (self.filters[type]) {
        self.markerClusterGroup.addLayer(marker);
      }
    };

    self.countSimpleClusterWidth = function(markersCount) {
      var width = 28;
      var countLength = markersCount.toString().length;

      if (countLength > 3) {
        width = 5 * (countLength - 3);
      }
      return width;
    };

    self.simpleClusterIconConstructor = function(marker_type, markersCount, firstFour, buckets) {
      var width         = self.countSimpleClusterWidth(markersCount),
        textColor       = options.textColor       || 'white';

      // if (marker_type === 'public_assets' || marker_type === 'private_assets') { marker_type = 'my_assets' }

      return new L.DivIcon({
        iconSize: [width, width],
        html: '<div class="cluster-' + marker_type +  '" style="text-align:center;border-radius: 50%;width:' + width + 'px;height:' + width + 'px;line-height:' + width + 'px;color:' + textColor + ';" data-first-four="' + firstFour + '" data-buckets="' + buckets + '">' + markersCount + '</div>',
        className: 'cluster-icon'
      });
    };

    self.singleMarkerIconConstructor = function(marker_type, identity) {
      var iconProperties = {
        markerColor: 'gray',
        extraClasses: 'empty-pin',
        dataIdentity: identity
      };

      switch (marker_type) {
        case 'owned_assets':
          iconProperties = {
            markerColor: 'orange',
            extraClasses: self.isPrivate ? 'material-icons tiny custom-lock' : 'empty-pin',
            iconText: self.isPrivate ? 'lock' : '',
            dataIdentity: identity
          };
          break;
        case 'rental_assets':
          iconProperties = {
            markerColor: self.isPrivate ? 'beige' : 'lightgreen',
            extraClasses: self.isPrivate ? 'material-icons tiny custom-lock' : 'empty-pin',
            iconText: self.isPrivate ? 'lock' : '',
            dataIdentity: identity
          };
          break;
        case 'public_assets':
          iconProperties = {
            markerColor: 'lightgreen',
            extraClasses: 'material-icons tiny',
            iconText: 'remove_red_eye',
            dataIdentity: identity
          };
          break;
        case 'public_public_assets':
          iconProperties = {
            markerColor: 'darkpurple',
            extraClasses: 'material-icons tiny',
            iconText: 'local_offer',
            dataIdentity: identity
          };
          break;
        case 'projects':
          iconProperties = {
            markerColor: 'gray',
            extraClasses: 'material-icons tiny',
            iconText: 'work',
            dataIdentity: identity
          };
          break;
      }
      return L.AwesomeMarkers.icon(iconProperties);
    };

    self.convertFirstFourToArray = function(string) {
      return string
        .toString()
        .split(',')
        .map(function(value) {
          return parseInt(value);
        })
      ;
    };

    self.updateCompanyAssetsClusters = function(groupType) {
      $('.cluster-'+groupType).each(function(key, cluster) {
        var firstFour = self.convertFirstFourToArray($(this).data('first-four'));
        var buckets   = $(this).data('buckets').split(',');
        // var groupType = 'rental_assets';
        // if (!self.isPrivate) { groupType = 'public_assets'; }

        self.initPreventDragging($(this));

        $(this).off('click');
        $(this).click(function() {
          self.loadMarkerInfo('Asset', firstFour, $(this).text(), buckets, groupType);
          self.clearPreviousActive();
          self.$currentActive = $(this).addClass('active-cluster');
        });
      });
    };

    self.updateProjectsClusters = function() {
      $('.cluster-projects').each(function(key, cluster) {
        var firstFour = self.convertFirstFourToArray($(this).data('first-four'));
        var buckets   = $(this).data('buckets').split(',');

        self.initPreventDragging($(this));

        $(this).off('click');
        $(this).click(function() {
          self.loadMarkerInfo('Project', firstFour, $(this).text(), buckets, 'projects');
          self.clearPreviousActive();
          self.$currentActive = $(this).addClass('active-cluster');
        });
      });
    };

    self.updatePublicPublicClusters = function() {
      $('.cluster-public_public_assets').each(function(key, cluster) {
        var firstFour = self.convertFirstFourToArray($(this).data('first-four'));
        var buckets   = $(this).data('buckets').split(',');

        self.initPreventDragging($(this));

        $(this).off('click');
        $(this).click(function() {
          self.loadMarkerInfo('Asset', firstFour, $(this).text(), buckets, 'public_public_assets');
          self.clearPreviousActive();
          self.$currentActive = $(this).addClass('active-cluster');
        });
      });
    };

    self.updateAllClustersEvents = function() {
      // self.updateMyAssetsClusters();
      self.updateCompanyAssetsClusters('owned_assets');
      self.updateCompanyAssetsClusters('rental_assets');
      self.updateCompanyAssetsClusters('public_assets');
      self.updateProjectsClusters();
      self.updatePublicPublicClusters();
    };

    self.updateActivePin = function() {
      self.updateMobileMapHeaderFilters()
      if (!self.$currentActive) return;

      var activeClass = '';
      var prevActiveElement = self.$currentActive.clone();


      if (prevActiveElement.hasClass('active-marker')) {
        activeClass = 'active-marker';
        prevActiveElement.removeClass(activeClass);
      }
      if (prevActiveElement.hasClass('active-cluster')) {
        activeClass = 'active-cluster';
        prevActiveElement.removeClass(activeClass);
      }

      var identity = prevActiveElement.attr('data-identity'); //tmp for awesome icons
      if (identity) {
        var activeElement = $('[class="' + prevActiveElement.attr('class') + '"][data-identity="' + identity + '"]').first();
      } else {
        var activeElement = $('[class="' + prevActiveElement.attr('class') + '"][data-buckets="' + prevActiveElement.attr('data-buckets') + '"]').first();
      }

      if (!_.isEmpty(activeElement)) {
        self.$currentActive = activeElement.addClass(activeClass);
      }
    };

    self.updateMarkersWithTimer = function() {
      if (self.timer){
        clearTimeout(self.timer);
      }

      self.timer = setTimeout(function(){
        self.updateMarkers().then(function() {
          self.initFilters();
        });
      }, 1000);
    };

    self.updateMarkers = function() {
      var d = $.Deferred();

      self.loadMarkers()
        .then(function() {
          self.markerClusterGroup.clearLayers();

          $.each(['owned_assets', 'rental_assets', 'public_assets', 'public_public_assets', 'projects'], function(key, layer_name) {
            self.markerLayers[layer_name] = {};
            self.markerLayers[layer_name] = L.layerGroup();
          });


          $.each(self.markersData.assets, function(key, cluster) {
            var coordinates = geohash.decode(cluster.key);

            var myCompanyAssets = cluster.my_company;

            if (myCompanyAssets.doc_count > 0) {
              // $.each(myCompanyAssets.grouped.buckets, function(key, marker) {
              //   var markerType = marker.key === "T" ? 'public_assets' : 'private_assets';
              //
              //   self.createMarker(marker, coordinates, markerType, cluster.key);
              // });
              var owned = myCompanyAssets.owned;
              self.createMarker(owned, coordinates, 'owned_assets', cluster.key);

              var rental = myCompanyAssets.rental;
              self.createMarker(rental, coordinates, 'rental_assets', cluster.key);

              var _public = myCompanyAssets.public;
              self.createMarker(_public, coordinates, 'public_assets', cluster.key);
            }

            var publicPublicAssets = cluster.public.not_my;

            if (publicPublicAssets.doc_count > 0) {
              self.createMarker(publicPublicAssets, coordinates, 'public_public_assets', cluster.key);
            }
          });

          $.each(self.markersData.projects, function(key, cluster) {
            var coordinates = geohash.decode(cluster.key);

            if (cluster.doc_count > 0) {
              self.createMarker(cluster, coordinates, 'projects', cluster.key);
            }
          });

          self.updateAllClustersEvents();

          self.updateActivePin();

          d.resolve();
        })
      ;

      return d.promise();
    };

    self.loadMarkers = function() {
      var bounds = self.map.getBounds();

      return $.get(
        '/clusters.json',
        {
          creator_id: self.creatorId,
          company_id: self.companyId,
          bounds: {
            top_left: {
              lat: 1.0006 * bounds.getNorthWest().lat,
              lng: 1.0006 * bounds.getNorthWest().lng
            },
            bottom_right: {
              lat: 0.9994 * bounds.getSouthEast().lat,
              lng: 0.9994 * bounds.getSouthEast().lng
            }
          },
          //zoom: self.map._zoom
          zoom: self.defineElasticsearchZoom()
        },
        function(data) {
          self.markersData = {
            assets:   data.assets.aggregations.grid.buckets,
            projects: data.projects.aggregations.grid.buckets
          }
        }
      )
    };

    self.initMarkerClusterGroup = function() {
      self.markerClusterGroup = new L.MarkerClusterGroup({
        chunkedLoading:      true, //to resolve slow script issue
        spiderfyOnMaxZoom:   false,
        zoomToBoundsOnClick: false,
        showCoverageOnHover: false,
        iconCreateFunction: function(cluster) {
          return self.clusterIconConstructor(cluster);
        }
      }).addTo(self.map);
    };

    self.initMap = function() {
      self.map = L.map(self.map_id, {
        center: [self.center.lat, self.center.lng],
        zoom:   self.zoom,
        minZoom: self.minZoom,
        maxZoom: self.maxZoom,
        attributionControl: false,
        scrollWheelZoom: true//false for prod
      });

      self.map.zoomControl.setPosition('bottomleft');

      var googleLayer = L.tileLayer( 'https://api.mapbox.com/v4/mapbox.light/{z}/{x}/{y}.png?access_token=' + self.apiKey);

      self.map.addLayer(googleLayer);

      self.initMarkerClusterGroup();

      self.map.on('zoomend dragend', function() {
        self.updateMarkersWithTimer();
      });

      self.map.on('zoomend', function () {
        self.map.dragging.enable(); //to fix bug with dragging after zoom atop the cluster
      });
    };
  }

})();
