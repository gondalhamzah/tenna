$(function () {
    $('.project_assignments select').change(function (e) {
        if (confirm("Are you sure you would like to change " + $(this.form).data("username") + " to " + $(e.target).children('option:selected').text() + "?")) {
            this.form.submit();
        } else {
            location.reload(true);
        }
    });
});
