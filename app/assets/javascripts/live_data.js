// //google map for live data assets
// $(function() {
//   if (isController('live-data') && isAction('index')) {

//     // window.mqttClient = new Messaging.Client("mqtt.buildsourced.com", 34687, "jwt_test" + parseInt(Math.random() * 100, 10));

//     // var options = {
//     //   timeout: 3,
//     //   useSSL: true,
//     //   userName: "jwt",
//     //   password: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MDM5MzE3NTQsImV4cCI6MTUzNTQ2Nzc1NCwiYXVkIjoid3d3LnRlbm5hLmNvbSIsInN1YiI6Im1jb29rQHRlbm5hLmNvbSIsIkdpdmVuTmFtZSI6IkpvaG5ueSIsIlN1cm5hbWUiOiJSb2NrZXQiLCJFbWFpbCI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJSb2xlIjpbIk1hbmFnZXIiLCJQcm9qZWN0IEFkbWluaXN0cmF0b3IiXX0.bNvpj1aosiKLyqpJmAfHql2sJGRUs4dXIWFosSynyNc",
     
//     //   //Gets Called if the connection has sucessfully been established
//     //   onSuccess: function () {
//     //     console.log("Connected");
//     //     mqttClient.subscribe('/4/notifications/1234/live_data/#', {qos: 2}); console.log('Subscribed');
//     //   },
     
//     //   //Gets Called if the connection could not be established
//     //   onFailure: function (message) {
//     //     alert("Connection failed: " + message.errorMessage);
//     //   }
//     // };

//     // mqttClient.connect(options);
    
//     // mqttClient.onMessageArrived = function (message) {
//     //  //Do something with the push message you received
//     //   var msgDestination = message.destinationName.split('/');
//     //   var destinationId = msgDestination[2];
//     //   if ($('#liveDataNotification__emptyRow')) {
//     //     $('#liveDataNotification__emptyRow').remove();
//     //   }
//     //   console.log(message.payloadString);
//     //   var messageId = $('#liveDataNotification__list').children().length + 1;
//     //   var nowString = moment().format('MM/DD/YY hh:mm a');
//     //   $('#liveDataNotification__list').append(
//     //     '<tr data-id="' + messageId + '">' +
//     //       '<td>Topic: ' + message.destinationName + ' | ' + message.payloadString + '</td>' +
//     //       '<td>Dallas Bridge Reoair</td>' +
//     //       '<td>' + nowString + '</td>' +
//     //       '<td><a href="javascript:void(0);" onclick="onDismissNotification(' + messageId + ')">Dismiss</a> | <a href="javascript:void(0);">Action</a></td>' +
//     //     '</tr>'
//     //   );
//     // };

//     // onDismissNotification = function(messageId) {
//     //   var parentTR = $('.liveDataNotification tr[data-id='+messageId+']');
//     //   var notificationList = $('#liveDataNotification__list');
//     //   if (parentTR) {
//     //     parentTR.remove();
//     //   }
//     //   if (notificationList.children().length == 0) {
//     //     notificationList.append(
//     //       '<tr id="liveDataNotification__emptyRow" class="liveDataNotification__emptyRow">' +
//     //       '<td colspan="4">No notifications yet</td>' +
//     //       '</tr>'
//     //     );
//     //   }
//     // }
//     window.liveDataMapInited = false;
//     // $('#liveData-map-link').click(function() {
//     //   console.log('map clicked...');
        
//     // });

//     window.liveDataMarkers = [];
//     setLiveDataMapOnAll = function (map) {
//       if (!window.liveDataMapInited) {
//         return;
//       }
//       for (var i = 0; i < liveDataMarkers.length; i++) {
//         console.log('removed');
//         liveDataMarkers[i].setMap(map);
//       }
//     }

//     var infowindow = new google.maps.InfoWindow({
//       content: ''
//     });

//     google.maps.event.addListener(infowindow, 'domready', function() {

//       // Reference to the DIV that wraps the bottom of infowindow
//       var iwOuter = $('.gm-style-iw');

//       /* Since this div is in a position prior to .gm-div style-iw.
//        * We use jQuery and create a iwBackground variable,
//        * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
//       */
//       var iwBackground = iwOuter.prev();

//       // Removes background shadow DIV
//       iwBackground.children(':nth-child(2)').css({'display' : 'none'});

//       // Removes white background DIV
//       iwBackground.children(':nth-child(4)').css({'display' : 'none'});

//       // Moves the infowindow 115px to the right.
//       // iwOuter.parent().parent().css({top: '30px'});

//       // Moves the shadow of the arrow 76px to the left margin.
//       iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

//       // Moves the arrow 76px to the left margin.
//       iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

//       // Changes the desired tail shadow color.
//       iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

//       // Reference to the div that groups the close button elements.
//       var iwCloseBtn = iwOuter.next();

//       // Apply the desired effect to the close button
//       iwCloseBtn.css({opacity: '1', right: '35px', top: '20px'});

//       // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
//       if($('.iw-content').height() < 140){
//         $('.iw-bottom-gradient').css({display: 'none'});
//       }

//       // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
//       iwCloseBtn.mouseout(function(){
//         $(this).css({opacity: '1'});
//       });
//     });

//     addLiveDataMarker = function (info, mapTypeId) {
//       if (!window.liveDataMapInited) {
//         return;
//       }
//       var tracking_code = info.tracking_code ? info.tracking_code : 'project-site';
//       var iconPrefix = 'static-assets/live_data/marker-'+ tracking_code;
//       var markerIcon = iconPrefix + "-map-view.png";
//       var satelliteIcon = iconPrefix + "-satellite-view.png";
//       var updateIcon = iconPrefix + "-latest-update.png";
//       var stateHoverIcon = iconPrefix + "-hover-selected-state.png";
//       var hoverIcon = iconPrefix + "-selected-hover.png";
//       var originalIcon = markerIcon;

//       if (mapTypeId == 'satellite' || mapTypeId == 'hybrid') {
//         originalIcon = satelliteIcon;
//       }
//       var finalLatLng = new google.maps.LatLng(info.geo[0], info.geo[1]);

//       var marker = new google.maps.Marker({
//         position: finalLatLng,
//         icon: originalIcon,
//         normalIcon: markerIcon,
//         hoverIcon: hoverIcon,
//         satelliteIcon: satelliteIcon,
//         updateIcon: updateIcon,
//         stateHoverIcon: stateHoverIcon,
//         satelliteIcon: satelliteIcon,
//         originalIcon: originalIcon,
//         map: liveDataMap,
//         title: info.dt
//       });
//       oms.addMarker(marker);

//       (function(marker, info) {
//         // add click event
//         google.maps.event.addListener(marker, 'spider_click', function(e) {  // 'spider_click', not plain 'click'
//           var content = '<div class="liveDataMap__iwContainer">' +
//             info.img +
//             '<div class="liveDataMap__iwInfoSection">' +
//               '<div class="liveDataMap__iwTitle">' + info.dt + '</div>' +
//               '<div class="liveDataMap__iwLastUpdate"><div>Last Update:</div>' +
//               '<div>' + info.lastUpdate + '</div></div>' +
//               '<a class="liveDataMap__iwMoreInfo" href="/assets/' + info.assetId + '" target="_blank">Details <i class="material-icons liveDataMap__iwMoreInfoIcon">keyboard_arrow_right</i></a>' +
//             '</div>' +
//           '</div>';
//           infowindow.close();
//           infowindow.setContent(content);
//           infowindow.open(liveDataMap, marker);
//           this.setIcon(this.hoverIcon);
//         });
//         google.maps.event.addListener(marker, "mouseover", function() {
//              this.setIcon(this.hoverIcon);
//         });
//         google.maps.event.addListener(marker, "mouseout", function() {
//              //you have to retreive the original icon cause with the mouse hover you change the marker icon attribute
//              this.setIcon(this.originalIcon);
//         });
//       })(marker, info);
//       liveDataMarkers.push(marker);
//     }

//     clearLiveDataMarkers = function() {
//       setLiveDataMapOnAll(null);
//       liveDataMarkers = [];
//     }

//     initLiveDataMap = function() {
//       var options = {
//           zoom: 5,
//           center: new google.maps.LatLng(39.909736, -98.522109), // centered US
//           mapTypeId: google.maps.MapTypeId.TERRAIN,
//           mapTypeControl: true
//       };
//       window.liveDataMap = new google.maps.Map(document.getElementById('liveDataMap'), options);


//       window.oms = new OverlappingMarkerSpiderfier(liveDataMap, {
//         markersWontMove: true,
//         markersWontHide: true,
//         basicFormatEvents: true,circleFootSeparation: 46, spiralFootSeparation: 52
//       });

//       var mapTypeId = liveDataMap.getMapTypeId();
//       for(var i=0; i<liveDataGEOData.length; i++) {
//         addLiveDataMarker(liveDataGEOData[i], mapTypeId);
//       }

//       google.maps.event.addListener( liveDataMap, 'maptypeid_changed', function() { 
//         redrawLiveDataMarkers();
//       } );
//       initCluster();
      
//     }

//     redrawLiveDataMarkers = function() {
//       var mapTypeId = liveDataMap.getMapTypeId();

//       var markerPostfix = '-map-view.png';
//       if (mapTypeId == 'satellite' || mapTypeId == 'hybrid') {
//         markerPostfix = '-satellite-view.png';
//       }
//       for(var i=0; i<liveDataMarkers.length; i++) {
//         var iconUrl = liveDataMarkers[i].normalIcon;
//         if (mapTypeId == 'satellite' || mapTypeId == 'hybrid') {
//           iconUrl = liveDataMarkers[i].satelliteIcon;
//         }
//         liveDataMarkers[i].setIcon(iconUrl);
//         liveDataMarkers[i].originalIcon = iconUrl;
//       }
//       initCluster();
//     }

//     initCluster = function() {
//       var mapTypeId = liveDataMap.getMapTypeId();

//       var markerPostfix = '-map-view.png';
//       if (mapTypeId == 'satellite' || mapTypeId == 'hybrid') {
//         markerPostfix = '-satellite-view.png';
//       }
//       var styles = [{
//         url: 'static-assets/live_data/marker-group' + markerPostfix,
//         height: 53,
//         width: 53,
//         anchor: [3, 0],
//         textColor: '#ffffff',
//         textSize: 20
//       }]

//       if (window.markerCluster) {
//         markerCluster.setMap(null);
//       }

//       window.markerCluster = new MarkerClusterer(liveDataMap, liveDataMarkers,
//             {styles: styles, maxZoom: 15});
//     }
    
//     if (!liveDataMapInited) {
//       liveDataMapInited = true;
//       initLiveDataMap();
//     }
//     $('#new_filter_search').on("ajax:complete", function(data, status, xhr) {
//       var result = JSON.parse(status.responseText);
//       $("#documents-search-results").html(result.content);
//       $('#assets').hide();
//       $('#assets-cor').remove();
//       load_js();
//       $("#documents-search").html(result.content2);
//     });
//   }
// });

// function load_js()
//  {
//     var head= document.getElementsByTagName('head')[0];
//     var script= document.createElement('script');
//     script.type= 'text/javascript';
//     script.src= 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.10/flickity.pkgd.js';
//     head.appendChild(script);
//  }

