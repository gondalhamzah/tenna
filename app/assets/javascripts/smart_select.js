$(document).ready(function() {
	$('.select2').select2();
	var $openedSelect = false;
    $(".select2").on("select2:open", 
      function (e) { 
        //console.log('opened');
        $openedSelect = $(this);
      });
    $(".select2").on("select2:closing", 
      function (e) {
        //console.log('closing...');
        $openedSelect = false;
        return true;
      });

    $(".select2").on("select2:selecting", 
      function (e) { 
        //console.log('selecting...');
        $openedSelect = false;
        return true;
      });
    $(document).on('focusout', '.select2-search__field', function(ev){
      if (!ev.relatedTarget && window.$openedSelect) {
        //console.log('should be closed');
        window.setTimeout(function() {
          if ($openedSelect) {
            //console.log('force to close now...');
            $openedSelect.select2('close');
          }
        }, 150);
      }
    });
});