//= require active_admin/base

$lat = $('#project_address_attributes_lat');
$lng = $('#project_address_attributes_lng');
$addr_l1 = "project_address_attributes_line_1";
$addr_city = "project_address_attributes_city";
$addr_state = "project_address_attributes_state";
$addr_zip = "project_address_attributes_zip";
$addr_country = "project_address_attributes_country";
$addr_change = "project_address_attributes_change_addr";
var count = 0;
var map;
var change;
var marker;
function initialize() {
  $('#'+ $addr_city + ', #' + $addr_l1 + ', #' + $addr_state + ', #' + $addr_zip + ', #' + $addr_country).change(function(){
    $('#' + $addr_change).val("change");
  });
 var input = document.getElementById('searchTextField');
    var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
      GeoCodeAddress();
    });
    var myOptions = {
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
    if(navigator.geolocation)
      navigator.geolocation.getCurrentPosition(displayOnMap);
}
function displayOnMap(position){
    var lat;
    var lng;
    if(position.coords){
      lat = position.coords.latitude;
      lng = position.coords.longitude;
    }
    else if(position.location){
      lat = position.location.lat();
      lng = position.location.lng();
    }
    if($('form').attr('id') == "edit_project" && $('#project_address_attributes_lat').val() && $('#project_address_attributes_lng').val() && change != "affect"){
      lat = $('#project_address_attributes_lat').val();
      lng = $('#project_address_attributes_lng').val();
      change = "affect"
    }
    var latlng = new google.maps.LatLng(lat,lng);
    marker = new google.maps.Marker({
      position: latlng,
      map: map,
      draggable: true,
    });
    $('#project_address_attributes_lat').val(lat);
    $('#project_address_attributes_lng').val(lng);

    if($('form').attr('id') == "edit_project" && count == 1)
    {
      rev_geocode(lat,lng);
    }
    else if($('form').attr('id') == "new_project"){
      rev_geocode(lat,lng);
    }
    map.panTo(latlng);
    google.maps.event.addListener(marker, 'dragend', function() {
      updateFormLocation(this.getPosition());
    });
    count = 1;
}
function updateFormLocation(latLng){
  $('#project_address_attributes_lat').val(latLng.lat());
  $('#project_address_attributes_lng').val(latLng.lng());
  rev_geocode(latLng.lat(),latLng.lng());
  $('#' + $addr_change).val("");
}
function GeoCodeAddress(){
    var geocoder =  new google.maps.Geocoder();
    geocoder.geocode( { 'address': $('#searchTextField').val()}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        marker.setVisible(false)
        displayOnMap(results[0].geometry);
      }
      else{
        alert('invalid adddress')
      }
    });
}

function rev_geocode(lat,lng){
  $.ajax({
    method: 'get',
    url: '/reverse_geocode_map',
    data: {lat: lat, lng: lng},
    success: function(result){
      $("#" + $addr_l1).val(result.data.street);
      $('label[for="'+$addr_l1+'"]').addClass('active');
      $("#" + $addr_city).val(result.data.city);
      $('label[for="'+$addr_city+'"]').addClass('active');
      $('#' + $addr_state).val(result.data.state);
      $('label[for="'+$addr_state+'"]').addClass('active');
      $('#' + $addr_zip).val(result.data.zip);
      $('label[for="'+$addr_zip+'"]').addClass('active');
      $('#' + $addr_country).val(result.data.country)
    }
  });
}

function togglePublic(value, init) {
    if (value) {
        $('#user_company_input').hide();
        $('#user_company_name_input').show();

        if (!init) {
            $('#user_company_input select').val('');
            $('#user_role_input option[value="public_user"]').attr('selected', 'selected');
        }
    } else {
        $('#user_company_input').show();
        $('#user_company_name_input').hide();

        if (!init) {
            $('#user_company_name_input input').val('');
            $('#user_role_input option[value="company_user"]').attr('selected', 'selected');
        }
    }
}

$(function () {
 var myKey = "AIzaSyD_6U-7TBtJjTwYvK7Kv-VVsct3QMQLjs0&libraries";
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = "https://maps.googleapis.com/maps/api/js?key=" + myKey + "&sensor=false&callback=initialize&libraries=geometry,places";
            document.body.appendChild(script);
  $('#notification_subject_input').addClass('hide');
    $('#notification_notification_type, #notification_notification_medium').change(function(){
      if($('#notification_notification_type').val() == "Maintenance" && $('#notification_notification_medium').val() == "Email"){
        $('#notification_subject_input').removeClass('hide');
      }
      else{
          $('#notification_subject_input').addClass('hide');
      }
    });

  var asset_type;
  if($('form').attr('id') == "new_material" || $('form').attr('id') == "edit_material"){
    asset_type = "material";
  }
  else{
    asset_type = "equipment";
  }

  if($('#'+asset_type+'_asset_kind').val()== 'rental'){
      $('#'+asset_type+'_units_input, #'+asset_type+'_time_length_input').removeClass('hide');
  }
  else{
      $('#'+asset_type+'_units_input, #'+asset_type+'_time_length_input').addClass('hide');
  }

  $('#'+asset_type+'_asset_kind').change(function(){
    if($(this).val() == "rental"){
      $('#'+asset_type+'_units_input, #'+asset_type+'_time_length_input').removeClass('hide');
    }
    else{
        $('#'+asset_type+'_units_input, #'+asset_type+'_time_length_input').addClass('hide');
    }
  });

  var proj_create_btn = $("#project_submit_action > input");
  if(proj_create_btn.val() == "Create Project")
  {
    proj_create_btn.val("Create Site");
  }
  else{
    proj_create_btn.val("Update Site");
  }
    togglePublic($('#user_public_user\\?').is(':checked'), true);

    $('#user_public_user\\?').change(function () {
        return togglePublic(this.checked);
    });
    if($('#subscription_submit_action input').val() == "Create Subscription"){
      $('#subscription_state').attr("style", "pointer-events: none;color:graytext");
    }
    $('#subscription_category').change(function(){
      if($(this).val() == 'asset reports' || $(this).val() == 'routing' ){
        $('#subscription_state').attr("style", "pointer-events: none;color:graytext");
      }
      else{
       $('#subscription_state').attr("style","pointer-events: cursor;color:#000000");
      }
    });

    var $body=$('body');
    if((
          $body.hasClass('admin_materials') ||
          $body.hasClass('admin_equipment')
        )
        &&
        (
          $body.hasClass('edit') || $body.hasClass('new')
        )) {

      var $company = $('#equipment_company_id, #material_company_id'),
          $project = $('#equipment_project_id, #material_project_id'),
          $group = $('#equipment_group_id, #material_group_id');

      function onCompanyChange(companyId) {
        $project.find('option').hide();
        $project.find("option[data-company='"+companyId+"']").show();
        $project.find('option:first').show();
      }

      function onProjectChange(projectId) {
        $group.find('option').hide();
        $group.find("option[data-project='"+projectId+"']").show();
        $group.find('option:first').show();
      }

      if ($company.val()) {
        onCompanyChange($company.val());
      }
      if ($project.val()) {
        onProjectChange($project.val());
      }

      $company.change(function() {
        $project.val('');
        onCompanyChange($company.val());
      });

      $project.change(function() {
        $group.val('');
        onProjectChange($project.val());
      });
    }
    $($('.download_links a')[1]).hide();
    $($('.download_links a')[2]).hide();
});