$(function () {

  function setEmailTemplate(){
    if ( $("#notification_notification_medium").val() && $("#notification_notification_type").val() ){
      var notificationMedium = $("#notification_notification_medium").val().toLowerCase()
      var notificationType = $("#notification_notification_type").val().toLowerCase()
      
      if( notificationMedium == "email"){

        $.ajax({
          url: "/admin/notifications/get_template",
          type: 'POST',
          data: {notification_type: notificationType}
        }).done(function(data) {
          CKEDITOR.instances.notification_message.setData(data);
        });
      }
    }
  }

  function showEmailFields(){
    $('.popup-field').hide();
  }

  function showPopupFields(){
    $('.popup-field').show();
  }

  function setFields(){
    if ( $("#notification_notification_medium").val() && $("#notification_notification_type").val() ){
      if ( $("#notification_notification_medium").val().toLowerCase() == "email" ){
        showEmailFields();
      }
      else if( $("#notification_notification_medium").val().toLowerCase() == "popup" ){
        showPopupFields();
      }
    }
    if ( $('#notification_submit_action').children().val() ){
      $('#notification_submit_action').children().val("Save as Draft");
    }
 
  }

  setFields();

  $("#notification_notification_type").change(function () {
      setEmailTemplate();
  });

  $("#notification_notification_medium").change(function () {
      setEmailTemplate();
      setFields();

  });

  $('form.notification').submit(function(){
   
  });

  $( "#send_email_button" ).click(function(event) {
    event.preventDefault();
    this.form.action = "/admin/notifications/send_email";
    this.form.submit();
  });

});