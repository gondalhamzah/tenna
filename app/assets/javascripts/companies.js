$(function () {
  if (isController('companies')) {
    if (isAction(['show'])) {
      $("table.company_directory .expander").on("click", function() {
        $(this).parents("td").find(".expander, .other_projects").addClass("expanded");
      });

      $("table.company_directory tr").on("click", function(e) {
        if ($(e.target).is("a")) {
          return;
        }
        if ($(this).find(".expander, .other_projects").hasClass("expanded")) {
          $(this).find(".expander, .other_projects").removeClass("expanded");
        }
      })
    } else if (isAction(['print-qr'])) {
      var thermal_printed_options = { "4 x 1"  : "4_x_1", "4 x 2"  : "4_x_2",
        "4 x 2.5": "4_x_2.5", "4 x 3"  : "4_x_3",
        "4 x 4"  : "4_x_4" }
      var laser_printed_options = { "Avery 5160" : "avery_5160", "Avery 5163" : "avery_5163",
        "Avery 5963" : "avery_5963", "Avery 6572" : "avery_6572",
        "Avery 6871" : "avery_6871" }

      var current_path = window.location.pathname;
      var company_id = current_path.split('/')[2];

      var download_file_button = "<a class='btn but1 btn-qr waves-effect waves-light request_submit_button' id='download_company_qr_codes' target='_blank' href='/companies/"+ company_id +"/download_printed_qr_codes'>GET QR TAGS</a>";
      var send_email_button = "<a class='btn btn-qr but1 waves-effect waves-light request_submit_button' id='email_company_qr_codes' href='#'>GET QR TAGS</a>";
      var generated_qr_label = '';

      $(document).on("ready", function() {

        var used_codes_count = $('b#used_codes_count');
        var unused_codes_count = $('b#unused_codes_count');
        var request_submit_button = $('button.request_submit_button');

        var qr_requested = function (){
          var is_requested = ($("div#preloaderBkground").hasClass("preloader-background") && $('div.preloader-wrapper').hasClass('active'));
          return is_requested;
        };

        var is_requested_qr = qr_requested.bind(null);

        $(window).load(function(){
          if($("div#preloaderBkground").hasClass("preloader-background")){
            $('#flash').empty();
            $('#flash').html("<div class='chip notice'>"+"Your QR codes are being generated."+"<i class='close material-icons'>close</i></div>");
            $("div.chip.notice").fadeOut(5000, function(){
              $(this).remove();
            });
          }
        });

        $('input[type=radio][name=printedType]').change(function() {
          var $label_format_drop_menu = $("#label_format");
          $label_format_drop_menu.empty();
          if (this.value == 'thermalPrint') {
            $.each(thermal_printed_options, function(key,value) {
              $label_format_drop_menu.append($("<option></option>")
                .attr("value", value).text(key));
            });
          } else if (this.value == 'laserPrint') {
            $.each(laser_printed_options, function(key,value) {
              $label_format_drop_menu.append($("<option></option>")
                .attr("value", value).text(key));
            });
          }
        });

        $('#label_format').change(function(){
          var generate_qr_btn = "<button class='btn waves-effect waves-teal btnQRTags request_submit_button' id='generate-qr-codes'>GENERATE QR CODES</button>";
          $('a.request_submit_button').replaceWith(generate_qr_btn);
        });

        $('input[type=radio][name=receivingSource]').change(function() {
          if (this.value == 'download') {
            $('a.request_submit_button').replaceWith(download_file_button);
            var html = $.parseHTML(download_file_button);
            var _href = $("a#download_company_qr_codes").attr("href");
            $("a#download_company_qr_codes").attr("href", _href + '?label_file_name=' + $('#label_format').val());
          } else if (this.value == 'email') {
            $('a.request_submit_button').replaceWith(send_email_button);
          }
        });

        function check_qr_status() {
          var intervalId = setInterval(function() {
            if (is_requested_qr()) {
              $.ajax({
                type: "POST",
                url: "/companies/"+company_id+"/qr_code_status",
                dataType: "json",
                success: function(data) {
                  if (data['status']) {
                    clearInterval(intervalId);
                    var download_file_button = "<a class='btn waves-effect waves-teal btnQRTags btnGetQr request_submit_button' id='download_company_qr_codes' target='_blank' href='/companies/"+ company_id +"/download_printed_qr_codes'>GET QR TAGS</a>";
                    var send_email_button = "<a class='btn waves-effect waves-teal btnQRTags btnGetQr request_submit_button' id='email_company_qr_codes' href='#'>GET QR TAGS</a>";
                    $('div.preloader-wrapper').removeClass('active');
                    $('div.preloader-wrapper').addClass('inactive');
                    $('div#preloaderBkground').removeClass('preloader-background');
                    $("#generate-qr-codes").attr("disabled", false);
                    $("button#generate-qr-codes").remove();
                    $('#flash').empty();
                    $('#flash').html("<div class='chip notice'>QR Codes generated successfully.<i class='close material-icons'>close</i></div>");
                    $("div.chip.notice").fadeOut(5000, function() {
                      $(this).remove();
                    });
                    if ($('input[name="receivingSource"]:checked').val() == 'download') {
                      $('.sbmt-btn').append(download_file_button);
                      var html = $.parseHTML(download_file_button);
                      var _href = $("a#download_company_qr_codes").attr("href");
                      $("a#download_company_qr_codes").attr("href", _href + '?label_file_name=' + $('#label_format').val());
                    } else if ($('input[name="receivingSource"]:checked').val() == 'email') {
                      $('.sbmt-btn').append(send_email_button);
                    }
                    used_codes_count.text(data['used_codes_count']);
                    unused_codes_count.text(data['unused_codes_count']);
                  }
                }
              });
            } else {
              clearInterval(intervalId);
            }
          }, 5000);
        }

        check_qr_status();

        $("body").on("click", "#generate-qr-codes", function(event) {
          event.preventDefault();
          $(this).attr("disabled", true);

          var printedType = $("input[name=printedType]:checked").val();
          var label_format = $('#label_format').val();

          $('div.preloader-wrapper').removeClass('inactive');
          $('div.preloader-wrapper').addClass('active');
          $('div#preloaderBkground').addClass('preloader-background');

          check_qr_status();

          $.ajax({
            type: "POST",
            url: "/companies/"+company_id+"/generate_printed_qr_codes",
            data: { printedType: printedType,
              label_format: label_format },
            dataType: "json",
            success: function(data) {
              $('#flash').empty();
              if (data['status'] == 'success') {
                generated_qr_label = label_format;
                $('#flash').html("<div class='chip success'>"+data['message']+"<i class='close material-icons'>close</i></div>");
                if ($("input[name=receivingSource]:checked").val() == 'download') {
                  request_submit_button.replaceWith(download_file_button);
                  var html = $.parseHTML(download_file_button);
                  var _href = $("a#download_company_qr_codes").attr("href");
                  $("a#download_company_qr_codes").attr("href", _href + '?label_file_name=' + generated_qr_label);
                } else if ($("input[name=receivingSource]:checked").val() == 'email') {
                  request_submit_button.replaceWith(send_email_button);
                }
              } else {
                $('#flash').html("<div class='chip notice'>"+data['message']+"<i class='close material-icons'>close</i></div>");
                $("div.chip.notice").fadeOut(5000, function(){
                  $(this).remove();
                });
              }
            }
          });
        });

        $('body').on('click', 'a#email_company_qr_codes', function(event) {
          event.preventDefault();
          $(this).attr("disabled", true);

          var label_format = $('#label_format').val();
          $('div.preloader-wrapper').removeClass('inactive');
          $('div.preloader-wrapper').addClass('active');
          $('div#preloaderBkground').addClass('preloader-background');

          $.ajax({
            type: "POST",
            url: "/companies/"+company_id+"/email_printed_qr_codes",
            data: { label_format: label_format },
            dataType: "json",
            success: function(data){
              $(this).removeAttr("disabled");
              $('div.preloader-wrapper').removeClass('active');
              $('div.preloader-wrapper').addClass('inactive');
              $('div#preloaderBkground').removeClass('preloader-background');

              $('#flash').empty();
              if (data['status'] == 'success') {
                $('#flash').html("<div class='chip success'>"+data['message']+"<i class='close material-icons'>close</i></div>");
              } else {
                $('#flash').html("<div class='chip notice'>"+data['message']+"<i class='close material-icons'>close</i></div>");
                $("div.chip.notice").fadeOut(5000, function(){
                  $(this).remove();
                });
              }
            }
          });
        });
      });
    }
  }
});
