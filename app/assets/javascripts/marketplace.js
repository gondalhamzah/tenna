function buildMarketplaceMap() {
  var handler = Gmaps.build('Google', {markers: {clusterer: {hideLabel: true}}});

  var map = handler.buildMap({
    provider: {
      disableDefaultUI: true,
      panControl: false,
      zoomControl: true,
      scrollwheel: false
    },
    internal: {id: 'dashboard-map'}
  }, function () {
    markers = handler.addMarkers(markers);
    handler.bounds.extendWith(markers);
    handler.fitMapToBounds();

    var dynamicInfoWindow = new google.maps.InfoWindow({content: 'Loading...'});
    var lastMarker;
    var lastInfoWindow;

    _.each(markers, function (marker) {
      google.maps.event.addListener(marker.getServiceObject(), 'click', function () {
        if (!_.isUndefined(lastMarker) && lastMarker !== marker) {
          lastInfoWindow.close();
        }

        if (_.isUndefined(marker.infowindow)) {
          dynamicInfoWindow.setContent('Loading...');
          dynamicInfoWindow.open(map.serviceObject, marker.serviceObject);
          lastMarker = marker;
          lastInfoWindow = dynamicInfoWindow;

          var p = queryStringToObject();
          p['filter_search[distance_miles]'] = 'Exact';
          p['filter_search[distance_zip]'] = marker.serviceObject.title;

          var q = objectToQueryString(p);

          $.get('/maps.json' + q).done(function (data) {
            data = data[0];
            data.price = _.isUndefined(data.price) ? "0" : data.price;

            var content = assetInfoTemplateCompiled(data);
            dynamicInfoWindow.setContent(content);
          });
        } else {
          lastMarker = marker;
          lastInfoWindow = marker.infowindow;
        }
      })
    });

    if (markers.length == 0) {
      var buildSourcedHQ = new google.maps.LatLng(40.524108, -74.389580);
      map.serviceObject.setCenter(buildSourcedHQ);
    }

    if (map.serviceObject.getZoom() > 11) {
      map.serviceObject.setZoom(11);
    }
  });

  var serviceObject = map.serviceObject;
  google.maps.event.addDomListener(window, 'resize', function () {
    var center = serviceObject.getCenter();
    google.maps.event.trigger(serviceObject, 'resize');
    serviceObject.setCenter(center);
  });
}

var assetInfoTemplate = '<div class="map-pin-info-asset map-pin-info-asset-single">' +
    '<div class="row callout-row-regular">' +
    '<div class="col s12">' +
    '<img src="<%= image %>" width="50" class="callout-img"/>' +
    '<p class="callout-project-name"><%= title %></p>' +
    '<p class="callout-project-name price">$<%= price %></p>' +
    '</div>' +
    '</div>' +
    '<div class="row callout-row-view-details">' +
    '<div class="col s12 center-align ">' +
    '<a class="callout-view-details" href="/assets/<%= id %>">View Details</a>' +
    '</div>' +
    '</div>' +
    '</div>';

var assetInfoTemplateCompiled = _.template(assetInfoTemplate);

$(function () {
  if (isController('marketplace')) {
    // Hide equipment specific fields for materials.
    $("input.integers-only.price-input").numeric(
      {
        allowPlus:    false,
        allowMinus:   false,
        allowThouSep: false,
        allowDecSep:  false,
        min:          1,
        max:          999999
      }
    );
  }
});
