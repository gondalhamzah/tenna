$(function () {
  if (isController('wanted-assets')) {

    if (isAction(['new', 'create', 'edit', 'update'])) {

      $('#wanted_asset_country').attr("data-trigger" ,"change");
      $('#wanted_asset_country').attr("data-parsley-remote-validator" ,"countrycode");
      $('#wanted_asset_country').attr("data-parsley-remote" , "");
      $('#wanted_asset_country').attr("data-parsley-required" ,"true");
      $('#wanted_asset_country').attr("data-parsley-required" ,"true");
      $('#wanted_asset_country').attr('data-parsley-error-message','Select a valid country or valid zip / postal code');

      $('form').parsley({
        trigger: "focusout",
        errorsWrapper: "<div class=\"error-block\"></div>",
        errorTemplate: "<span></span>",
        excluded: "select:hidden"
      });
      $("#wanted_asset_country").change(function() {
        $(this).parsley().validate();
      });
    }

    // Hide equipment specific fields for materials.
    if (isAction(['new', 'create', 'edit', 'update'])) {
      $('input[name="wanted_asset[type]"]').change(function () {
        var form = $(this).closest('form');

        form.removeClass('type-equipment').removeClass('type-material');
        form.addClass('type-' + this.value.toLowerCase());
      });

      $('form').submit(function () {
        $('.type-equipment-only, .type-material-only').filter(':hidden').remove();
      });

      var $project = $('select[name="wanted_asset[project_id]"]');
      if ($project.length) {
        var CountryData;
        var zipData = $project.parents('.projects').data('zips'),
            $zip = $('input[name="wanted_asset[location_zip]"]'),
            $country = $('#wanted_asset_country')
            temp = {};

        CountryData = zipData;
        for (var i=0; i<zipData.length; i++) {
          temp[zipData[i].project_id] = zipData[i].zip;
        }
        zipData = temp;
         temp = {};
        for (var i=0; i<CountryData.length; i++) {
          temp[CountryData[i].project_id] = CountryData[i].country;
        }
        CountryData = temp;

        function projectChanged() {
          if (zipData[$project.val()]) {
            $zip.focus();
            $zip.val(zipData[$project.val()]).trigger('change');
          }

          if (CountryData[$project.val()]) {
            $country.focus();
            $country.val(CountryData[$project.val()]).trigger('change');
          }
        }

        if (isAction(['new', 'create'])) {
          if (!$zip.val()) {
            projectChanged();
          }
        $('#wanted_asset_priority_medium').attr('checked',true);
        }

        $project.change(function() {
          projectChanged();
        });
      }
    }
  }
});
