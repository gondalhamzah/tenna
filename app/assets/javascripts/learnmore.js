$(function () {
  if (isController('pages')) {

    $(document).on('submit','#learn_more1', function() {
      lead_pixel();
    });

    $('form').parsley(
        {
          trigger: "focusout",
          errorsWrapper: "<div class=\"error-block\"></div>",
          errorTemplate: "<span></span>",
          excluded: "select:hidden"
        }
      );

  setTimeout(function(){
    $('#email-sent-notification').remove();
  }, 25000);

  }
});
