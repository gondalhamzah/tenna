$(function () {
  // Hide equipment specific fields for materials.
  $("input.integers-only").numeric(
    {
      allowPlus:    false,
      allowMinus:   false,
      allowThouSep: false,
      allowDecSep:  false,
    }
  );
});

