$(function () {
  if (isController('registrations') || isController('invitations')) {
    if (isAction(['new', 'edit'])) {
      $('#user_terms_of_service').on('click', function(event){
        console.log("user_terms_of_service");
        if ($(this).prop('checked')){
          event.preventDefault();
          $('#user_terms_of_service_modal').modal('open');
        }
        else{
          $('#agree_to_terms').prop("checked", false);
        }
      });
      $('#tos_link').on('click', function(event){
        event.preventDefault();
        $('#user_terms_of_service_modal').modal('open');
      });
      $('#agree_to_terms').click(function(){
        if ($(this).prop('checked')){
          $('#user_terms_of_service_modal').modal('close');
          $('#user_terms_of_service').prop("checked", true);
          $("#user_terms_of_service_modal").scrollTop(0);
        }
        else {
          $('#user_terms_of_service_modal').modal('close');
          $('#user_terms_of_service').prop("checked", false);
          $("#user_terms_of_service_modal").scrollTop(0);
        }
      });
    }
  }
});
