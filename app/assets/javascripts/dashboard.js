$(function () {
  if (isController('dashboard') && isAction('index')) {
     var bsMap = new BsMap({
       appContainer: '#map-wrapper',
       map_id: 'dashboard-map'
     });

    bsMap.init();
    $(".fixed-action-btn").addClass('hide-on-small-and-down-map');
    $(window).scroll(function(){
      if($(this).scrollTop() > 20){
        $(".map-scroller").hide();
        $(".fixed-action-btn").removeClass("hide-on-small-and-down-map");
      }else{
        $(".map-scroller").show();
        $(".fixed-action-btn").addClass("hide-on-small-and-down-map");
      }
    });
    Highcharts.setOptions({lang: {thousandsSep: ','}});
    $('.dashboard-chart .card-panel').each(function(index) {
      var $this = $(this),
          options = $this.data('chart');
      if ($this.hasClass('small-chart') && $this.width() < 300) {
        options['plotOptions']['pie']['dataLabels']['rotation'] = 15
      }
      if (typeof options != 'undefined') {
        if (options.series[0].id == "pie_chart") {
          options.plotOptions.series.point.events.click = function () {location.href = this.options.url;};
        }
      }

      $this.highcharts(options);
    });
    
    $('.notification').each(function(index) {
      if (index != 0) {
        var notification = $(this)
        var previousNotification = notification.prev();
        var marginBetweenNotifications = 0;
        var previousNotificationTopValue = previousNotification.offset().top
        var topValue = previousNotificationTopValue + previousNotification.height() + marginBetweenNotifications;
        notification.css({top: topValue});
      }
    });
    $('.notification .close-btn').click(function() {
      var $this = $(this)
      var notification = $this.parent().parent().parent().parent().parent();
      notification.remove();
    });

  }
});
