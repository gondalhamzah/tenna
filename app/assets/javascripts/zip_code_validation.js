$(function () {
	if (isController('assets')) {
	 var zip_code = $('#asset_location_zip').val();
	 $('#asset_location_zip').change(function(){
		zip_code = $(this).val();
	 });
	  $('#asset_project_id').change(function(){
			if($('#asset_location_zip').trigger('change')){
				zip_code = $('#asset_location_zip').val();
      }
    });
	}
	if (isController('projects')) {
	 var zip_code = $('#project_address_attributes_zip').val();
	 $('#project_address_attributes_zip').change(function(){
			zip_code = $(this).val();
	 });
	}

	if (isController('wanted-assets')) {
	 var zip_code = $('#wanted_asset_location_zip').val();
	 $('#wanted_asset_location_zip').change(function(){
			zip_code = $(this).val();
	 });
	 $('#wanted_asset_project_id').change(function(){
			if($('#wanted_asset_location_zip').trigger('change')){
				zip_code = $('#wanted_asset_location_zip').val();
      }
     });
	}

	window.Parsley.addAsyncValidator('zipcode', function(xhr){
	   var data = $.parseJSON(xhr.responseText);
	   return data.valid;
	}, "/zip_with_country_validator?zip_country={value}");

	 window.Parsley.addAsyncValidator('countrycode', function(xhr){
	    var data = $.parseJSON(xhr.responseText);
	    return data.valid;
	  }, "/zip_validator?country_code={value}",
		{
		  data: function () {
		      return {
		          zip_code: zip_code
		      };
		  }
		}
	  )
});