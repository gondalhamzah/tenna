$(function () {
  if (isController('marketplace')) {
    var slider = document.getElementById('slider-price-range');
    noUiSlider.create(slider, {
     start: [1,1000000],
     connect: true,
     step: 1,
     range: {
      min: 1,
      max: 1000000
      },
     format: wNumb({
       decimals: false
     })
    });

    slider.noUiSlider.on('change', function(){
      var priceValues = slider.noUiSlider.get();
      var filter_search_price_min = document.getElementById('filter_search_price_min');
      var filter_search_price_max = document.getElementById('filter_search_price_max');
      filter_search_price_min.value = parseInt(priceValues[0]);
      filter_search_price_max.value = parseInt(priceValues[1]);
      filter_search_price_min.nextSibling.classList.add("active");
      filter_search_price_max.nextSibling.classList.add("active");
    });
  }
});
