(function( $ ) {
  $.fn.refreshExports = function() {
    this.filter( "tr" ).each(function() {
      if($(this).data("ready")==true)
        return;
      exponentialBackoff(checkExportStatus, this, 100, 5000, function(elem){
        $.ajax({
          url: $(elem).data("url")
        })
      });
    });
    return this;
  };

  function exponentialBackoff(toTry, elem, max, delay, callback) {
    var result = toTry(elem);
    if (!result) {
      callback(elem);
      if (max > 0) {
        setTimeout(function() {
          exponentialBackoff(toTry, elem, --max, delay, callback);
        }, delay);
      }
    }
  }

  function checkExportStatus(elem){
    return $(elem).data("ready");
  }
}( jQuery ));

$(function () {
  if(isController("exports") && isAction(["index", "export-project", "export-assets"])){
    $("tr.export-row").refreshExports();
  }
});
