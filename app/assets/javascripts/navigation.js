$(function () {
  if ($(window).width() < 993) {
    $('#filters').hide();
  }

  $('#toggle-filters').click(toggleFilters);

  enquire.register('screen and (min-width:993px)', {
    match: function () {
      $('#filters').show();
    }
  });

  $('#toggle-map').click(toggleMap);

  var open_mobile_nav = function() {
    $('#mobile_main_nav_btn').addClass('active');
    $('#mobile_main_nav_dropdown').height(function (index, height) {
      if(window.innerHeight <= 480) {
        $(this).css("height", "480px");
      }
      else {
        return '1000px';
      }
    });
    //$('#mobile_main_nav_dropdown').css("height", "450px");
    $('#mobile_main_nav_dropdown').slideDown('fast', 'linear');
    $('.fixed-action-btn').fadeOut(200);
  }

  var close_mobile_nav = function() {
    $('#mobile_main_nav_btn').removeClass('active');
    $('#mobile_main_nav_dropdown').slideUp('fast', 'linear');
    $('.fixed-action-btn').fadeIn(200);
  }

  $('#mobile_main_nav_btn').click(function () {
    if ($('#mobile_main_nav_dropdown').is(':visible')) {
      close_mobile_nav();
    } else {
      open_mobile_nav();
    }
  });

  $('#mobile_main_nav_dropdown a').on('click', function() {
    close_mobile_nav();
  });

  $('.mobile-subnav-button').on('click', function () {
    var target = $(this).data('subnavTarget');
    $('#main_mobile_nav').effect('slide', {direction: 'left', mode: 'hide'}, 100);
    $('#' + target + '_mobile_nav').effect('slide', {direction: 'right', mode: 'show'}, 100);
  });

  $('.back-to-mobile-main-nav').on('click', function () {
    var current_tab = $($(this).parents('.mobile-carousel-item')[0]);
    $('#main_mobile_nav').effect('slide', {direction: 'left', mode: 'show'}, 100);
    current_tab.effect('slide', {direction: 'right', mode: 'hide'}, 100);
  });

  $('#desktop_main_vertical_nav_btn').on('click', function() {
    $(document).unbind('click.desktop_main_vertical_nav_dropdown touchstart.desktop_main_vertical_nav_dropdown');
    $('.desktop-subnav-button').unbind('click');
    $('.desktop-subnav-button').on('click', function() {
      $(event.target).dropdown('open');
    });
    if ($("#desktop_main_vertical_nav_btn").attr("class").split(' ').includes("active") == true){
      $('.fab-helper-sub').hide();
    }else{
      $('.fab-helper-sub').show();
    }
  });

  $(document).on('click', function(event) {
    if(($(event.target).parents('#desktop_main_vertical_nav_dropdown').size() == 0 && $(event.target).parents('#desktop_main_vertical_nav_btn').size() == 0) || $(event.target).hasClass('desktop-nav-item')) {
      $('#desktop_main_vertical_nav_dropdown').hide();
      $('#desktop_main_vertical_nav_btn').removeClass('active');
      $('.fab-helper-sub').show();
    }
  });
});

function toggleFilters(e) {
  e.preventDefault();
  $('#filters').slideToggle();
  $("#toggle-filters").addClass('but2');
  $("#toggle-filters").attr('style', 'color: #fd4f00 !important');
}

function toggleMap(e) {
  e.preventDefault();

  var btn = $(e.target), list = $('#panel-list'), map = $('#panel-map');

  if (map.is(':visible')) {
    btn.text('Map');
    map.fadeOut('fast', function () {
      list.fadeIn('fast');
    });
  } else {
    btn.text('List');
    list.fadeOut('fast', function () {
      map.fadeIn('fast', function () {
        if (!map.attr('data-initialized')) {
          buildMarketplaceMap();
          map.attr('data-initialized', true);
        }
      });
    });
  }
}
