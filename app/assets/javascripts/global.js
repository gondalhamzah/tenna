var pathname = window.location.pathname;
$(function () {
  $(document).on("click", "#flash .material-icons", function(){
    $('.flast-notice').remove();
  });

  ///////////add video usages///////////
  $(document).on("click", ".video-help-link", function() {
    var video_id = $(this).data('video-id');
    $('#videos-box').hide();
    $.ajax({
      url: "/videos/add_video_usage",
      type: "POST",
      data: {video_id: video_id},
      statusCode: {
        200: function () {
        },
        422: function () {
        },
        500: function () {
        }
      }
    });
  });
  $('#videos-box-link').on('click', function() {
    $('#videos-box').show();
  });
  $('.videos-box-cancel').on('click', function() {
    $('#videos-box').hide();
  });

  window.setTimeout(function() {
    $('#flash .chip.notice').remove();
  }, 5000);
  Waves.displayEffect();

  $('.dropdown-button').dropdown();
  $('.button-collapse').sideNav();

  // $('.modal-trigger').leanModal();
  $('.modal').modal();
  $('select').material_select(function() {
    $('input.select-dropdown').trigger('close');
  });
  var onMouseClick = function(e) {
    if (e.clientX >= e.target.clientWidth || e.clientY >= e.target.clientHeight) {
      e.preventDefault();
    }
  };
  $('select').siblings('input.select-dropdown').on('mousedown', onMouseClick);

  setTimeout(function () {
    $('input[autofocus]').siblings('label, i').addClass('active');
  }, 0);

  $('.input-field i:last').click(function () {
    $(this).siblings('input').val('').focus();
  });

  $('#apply-filters').on('click', function (e) {
    e.preventDefault();
    if (isController('groups'))
      $('#new_group_filter_search').submit();
    else
      $('#new_filter_search').submit();
  });

  $('#search-form').on('submit', function (e) {
    e.preventDefault();
    $('#new_filter_search').submit();
  });
  $('#nav-search-clear').click(function(){
    $("#filter_search_query").val("");
  });

  $('#group-nav-search-clear').click(function(){
    $('#group_filter_search_query').val("");
  });

  $('#group-search-form').on('submit', function (e) {
      e.preventDefault();
      $('#new_group_filter_search').submit();
  });

  $('#nav-search').on('keyup change', function () {
    $('#filter_search_query').val($(this).val().replace(/[^a-zA-Z ]/g, ""));
  });

  $('#nav-search-live').on('keyup change', function () {
    $('#filter_search_query').val($(this).val().replace(/[^a-zA-Z ]/g, ""));
  });

  $('#group-nav-search').on('keyup change', function () {
    $('#group_filter_search_query').val($(this).val().replace(/[^a-zA-Z ]/g, ""));
  });

  function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  }

  $('#page-size-sel').on('change', function() {
    $('<input />').attr('type', 'hidden')
                    .attr('name', "filter_search[page_size]")
                    .attr('value', $(this).val())
                    .appendTo('#new_filter_search');
    if (location.search.indexOf("tracking_code=") !== -1) {
      $('<input />').attr('type', 'hidden')
                    .attr('name', "tracking_code")
                    .attr('value', getUrlVars()["tracking_code"])
                    .appendTo('#new_filter_search');
    }
    $('#new_filter_search').submit();
  });

  $('#group-page-size-sel').on('change', function() {
    $('#group_filter_search_page_size').val($(this).val());
    $('#new_group_filter_search').submit();
  });

  $('#group-header-sort').on('change', function () {
    var $input = $(this);
    $('#group_filter_search_sort').val($input.val());
    $('#new_group_filter_search').submit();
  });

  $('#header-sort').on('change', function() {
    $('<input />').attr('type', 'hidden')
                    .attr('name', "filter_search[sort]")
                    .attr('value', $(this).val())
                    .appendTo('#new_filter_search');
    if (location.search.indexOf("tracking_code=") !== -1) {
      $('<input />').attr('type', 'hidden')
                    .attr('name', "tracking_code")
                    .attr('value', getUrlVars()["tracking_code"])
                    .appendTo('#new_filter_search');
    }
    $('#new_filter_search').submit();
  });

  $('#header-sort').on('change', function () {
    var $input = $(this);
    $('#filter_search_sort').val($input.val());
    if($input.val() == 'distance' && !$.trim($('#filter_search_coordinate').val()).length) {
      var alertT;
      var successF = function(){
        if($.trim($('#filter_search_coordinate').val()).length) {
          clearTimeout(alertT);
          $input.val('distance');
          $('#new_filter_search').submit();
        }
      }
      if(geolocateForFilter(successF)) {
        alertT = setTimeout(function(){ // let geolocation request run first
          if(!$.trim($('#filter_search_coordinate').val()).length) {// when geolocation coordinate is not available yet
            alert('You need to share your current location');
            $input.closest('form').trigger('reset');
          }
        }, 4000)
      } else {
        alert('Unable to get your current location');
      }
    } else {
      $('#new_filter_search').submit();
    }
  });

  if( $('#new_filter_search').length){
    $('#new_filter_search').parsley(
      {
        trigger: "focusout",
        errorsWrapper: "<div class=\"error-block\"></div>",
        errorTemplate: "<span></span>"
      }
    );
  }

  var geoSuccess = function (position) {
    var pos = {
      lat: position.coords.latitude,
      lng: position.coords.longitude,
      accuracy: position.coords.accuracy
    };
    $.post('/update_cookies.json', pos);
  }

  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(geoSuccess);
    return true;
  }
});

function isController(value) {
  return isControllerOrAction('controller', value);
}

function isAction(value) {
  return isControllerOrAction('action', value);
}

function isControllerOrAction(controllerOrAction, value) {
  var body = $('body');
  var valueArray = _.isArray(value) ? value : [value];

  for (var i = 0; i < valueArray.length; i++) {
    if (body.hasClass(controllerOrAction + '-' + valueArray[i])) {
        return true;
    }
  }
  return false;
}

function restrictFileInputSize(selector, maxSize, errorMessage) {
  selector.on('change', function () {
    var input = $(this);
    $.each(this.files, function (index, file) {
      if (file.size > maxSize) {
        $('#modal > .modal-content').html('<h4>Error</h4><p>' + errorMessage + '</p>');
        $('#modal').modal('open');
        input.val('');

        return false;
      }
    });
  });
}

function queryStringToObject() {
  var pairs = location.search.slice(1).split('&');

  var result = {};
  pairs.forEach(function (pair) {
    pair = pair.split('=');
    result[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
  });

  return JSON.parse(JSON.stringify(result));
}

function objectToQueryString(obj) {
  var pairs = [];

  _.forIn(obj, function (value, key) {
    pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(value));
  });

  return '?' + pairs.join('&');
}

function toggleGroupSelect($project, $group, $groupRow, showAllOnEmptyProject) {
  function updateGroupSelect(project_id) {
    $group.find("option").prop('disabled', true);
    // find options for selected project
    var $options = $group.find("option[data-project='"+project_id+"']");
    // if there are valid groups for selected groups then we should
    if ($options.length > 0) {
      $options.prop('disabled', false);
      $group.find("option:first-child").prop('disabled', false);
      $groupRow.show();
      // refresh select2 based select element
      $group.select2();
    } else {
    // otherwise hide grooups select2
      $groupRow.hide();
    }
  }

  if (!$group.val()) {
    if ($project.val()) {
      updateGroupSelect($project.val());
    }
  }

  $project.on('change', function() {
    var project_id = $project.val();
    // whenever project_id is changed, reset group_id
    $group.val('');

    if (project_id) {
      updateGroupSelect(project_id);
    } else {
      if (showAllOnEmptyProject) {
        $group.find("option").prop('disabled', false);
        $groupRow.show();
        $group.select2();
      } else {
        $groupRow.hide();
      }

    }
  });
}

function adRoll() {
  try{
    __adroll.record_user({"adroll_segments": "0a31ab3e"})
  } catch(err) {}
}

function lead_pixel() {
  var capterra_vkey = '048c0fee16683c4ffe1817adfd5c87c0',

    capterra_vid = '2112575',
    capterra_prefix = (('https:' == document.location.protocol) ? 'https://ct.capterra.com' : 'http://ct.capterra.com');

  (function()
    { var ct = document.createElement('script'); ct.type = 'text/javascript'; ct.async = true; ct.src = capterra_prefix + '/capterra_tracker.js?vid=' + capterra_vid + '&vkey=' + capterra_vkey; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s); }

  )();
}