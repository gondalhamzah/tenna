module SmsMessageHelper
  def get_controller
    controller = ApplicationController.new.tap do |ctrl|
      ctrl.request  = ActionDispatch::Request.new({})
      def ctrl.default_url_options
        if Rails.env.development?
          host = 'localhost:3000'
        else
          host = ENV['HOST']
        end

        {host: host}
      end
    end
    controller
  end

  def sms_path_visited_notification(route, project, asset, driver_stop, path)
    sms_message = get_controller.render_to_string(
      'sms/path_visited_notification.txt',
      :locals => {
        :path => path,
        :route => route,
        :asset => asset,
        :project => project,
        :driver_stop => driver_stop
      }
    )
    send_sms(project.contact_sms_number, sms_message)
  end

  def sms_route_generated_notification(driver, phone_number, route, fired_date)
    sms_message = get_controller.render_to_string(
      'sms/route_generated_notification.txt',
      :locals => {
        :driver => driver,
        :route => route,
        :fired_date => fired_date,
        :route_url => route_url(route.slug, fired_date)
      }
    )
    send_sms(phone_number, sms_message)
  end

  def sms_live_data_notification(phone_number, notification, asset, notes)
    sms_message = get_controller.render_to_string(
      'sms/live_data_notification.txt',
      :locals => {
        :notification => notification,
        :asset => asset,
        :notes => notes
      }
    )
    send_sms(phone_number, sms_message)
  end

  def sms_maint_event_notification(user, asset, event)
    sms_message = get_controller.render_to_string(
      'sms/maint_event_notification.txt',
      :locals => {
        :event => event,
        :asset => asset,
        :user => user
      }
    )
    send_sms(user.phone_number, sms_message)
  end

  def sms_maint_complete_event_notification(user, asset, event)
    sms_message = get_controller.render_to_string(
      'sms/maint_complete_event_notification.txt',
      :locals => {
        :event => event,
        :asset => asset,
        :user => user
      }
    )
    send_sms(user.phone_number, sms_message)
  end

  def sms_other_event_notification(user, event)
    sms_message = get_controller.render_to_string(
      'sms/other_event_notification.txt',
      :locals => {
        :event => event,
        :user => user
      }
    )
    send_sms(user.phone_number, sms_message)
  end

  def sms_recurring_route_gen_notification(user, recurring_route, event)
    sms_message = get_controller.render_to_string(
      'sms/recurring_route_gen_notification.txt',
      :locals => {
        :recurring_route => recurring_route,
        :event => event,
        :user => user
      }
    )
    send_sms(user.phone_number, sms_message)
  end

  def sms_live_data_alert(name, number, message, asset, cond, date_time)
    sms_message = get_controller.render_to_string(
      'sms/live_data_alert.txt',
      :locals => {
        :message => message,
        :asset => asset,
        :cond => cond,
        :gauge => cond.gauge_alert_group.gauge,
        :date_time => date_time
      }
    )
    send_sms(number, sms_message)
  end

  def sms_live_data_geofence_alert(name, number, message, asset, date_time)
    sms_message = get_controller.render_to_string(
      'sms/live_data_geofence_alert.txt',
      :locals => {
        :message => message,
        :asset => asset,
        :date_time => date_time
      }
    )

    send_sms(number, sms_message)
  end
end
