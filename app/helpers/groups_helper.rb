module GroupsHelper
  # Used for bulk action drop-down menu
  def group_actions_for_user(user)
    actions = [
      ['Move', 'move'],
      ['Mark as Closed', 'mark_as_closed'],
      ['Delete', 'delete']
    ]
    if @group_filter_search.is_closed?
      actions.delete(['Mark as Closed', 'mark_as_closed'])
    end

    actions
  end

  def group_version_action_label(version)
    if version.field_name == 'project_id'
      'Group Moved'
    elsif version.field_name == 'notes'
      'Notes changed'
    elsif version.field_name == 'asset_id'
      if version.action == 'delete'
        'Asset Unlinked'
      else
        'Asset Linked'
      end
    else
      version.field_name.underscore.humanize
    end
  end

  def group_version_value(version, f)
    value = version.try(f.to_sym)
    if version.field_name == 'project_id'
      Project.find_by_id(value).try(:name)
    elsif version.field_name == 'asset_id'
      Asset.find_by_id(value).try(:title)
    else
      value
    end
  end
end
