module ProjectsHelper

  def closeout_date_label(date)
    date >= Date.today ? 'Closes' : 'Closed'
  end

end
