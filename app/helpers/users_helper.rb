module UsersHelper
  def phony_formatted(number)
    if number.present?
      # see https://github.com/floere/phony/blob/master/qed/format.md#format--string-template
      number.phony_formatted(format: '+%{cc} %{trunk}%{ndc}.%{local}', local_spaces: '.')
    end
  end
end

