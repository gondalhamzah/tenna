require 'browser/aliases'
module ApplicationHelper
  # Used for body id
  def page_id
    "#{controller_name}_#{action_name}".dasherize
  end

  # Used for body class
  def page_classes
    page_classes = "controller-#{controller_name.dasherize} action-#{action_name.dasherize}"
    page_classes += ' devise' if %W(confirmations invitations passwords registrations sessions).include? controller_name

    page_classes
  end

  def fmt_flash_name(name)
    (%w[alert notice success].exclude? name) ? 'notice' : name
  end

  def fmt_flash_msg(msg)
    msg.is_a?(Array) ? safe_join((msg.html_safe unless msg.blank?), '<br />') : msg.html_safe unless msg.blank?
  end

  # Renders a link back to previous page, if there is a back param
  # Used for asset, project, etc. create/edit screens
  def link_to_cancel opts = {class: 'links-li2'}
    unless params[:back].nil? && !current_user.private?
      link_to 'Cancel', params[:back], opts
    end
  end

  # Used to optimize SEO value of pages

  def title(text)
    content_for :title, text
  end

  def meta_tag(tag, text)
    content_for :"meta_#{tag}", text
  end

  def yield_meta_tag(tag, default_text='')
    content_for?(:"meta_#{tag}") ? content_for(:"meta_#{tag}") : default_text
  end

  def project_name_and_role(assignment)
    abr = case assignment.role
          when 'project_admin'
            'L'
          when 'project_user'
            'M'
          end

    "#{assignment.project.name} (#{abr})"
  end

  def browser
    Browser::Base.include(Browser::Aliases)
    Browser.new(request.env['HTTP_USER_AGENT'])
  end

  def pluralize_sentence(count, i18n_id, plural_i18n_id = nil)
    if count == 1
      I18n.t(i18n_id, :count => count).html_safe
    else
      I18n.t(plural_i18n_id || (i18n_id + '_plural'), :count => count).html_safe
    end
  end
end
