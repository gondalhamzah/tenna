module PathFinderHelper
  def self.verify_start_end_times(visits)
    visits.each do |name, details|
      if details['start'].present? && details['end'].present? && Time.parse(details['start']) > Time.parse(details['end'])
        return { error: 'Please check appointment begin and end windows' }
      end
    end

    return {}
  end

  def project_assets(project_id)
    if current_user.private?
      search_options = {company: current_user.company}
    else
      search_options = {creator: current_user}
    end

    # params[:filter_search] = JSON.parse(params[:filter_options])
    params[:filter_search]['project'] = project_id

    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: search_options,
      params: params,
      zip_code: cookies[:zip_code]
    )

    params[:page] ||= 1
    filterable_assets.filter_search
    filterable_assets.routing_assets.page(params[:page]).per(9999)
  end
end
