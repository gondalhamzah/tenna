module FiltersHelper

  # Used to generate URL for View Assets within zip code info window on maps
  def marketplace_zip_code_url(zip_code)
    URI.encode("/marketplace?filter_search[distance_miles]=Exact&filter_search[distance_zip]=#{zip_code}")
  end

end
