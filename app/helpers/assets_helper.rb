module AssetsHelper

  def format_date(date)
    I18n.localize(date, format: :month_day_year)
  end

  def format_time(date)
    I18n.localize(date, format: :month_day_year_and_time)
  end

  def format_date_or_today(date)
    date.future? ? format_date(date) : 'Today'
  end

  def custom_asset_path(asset, tracking_code)
    if tracking_code.nil?
      asset.becomes(Asset)
    else
      assign_tracking_code_path(tracking_code, asset_id: asset)
    end
  end

  def asset_status_text(asset, current_user)
    text = ''
    cls = ''
    if can?(:update, asset) && asset.expiring_within?(Asset::EXPIRE_THRESHOLD) && asset.public? && !asset.company.present?
      text = "Expiring #{format_date_or_today(asset.expires_on)}"
      cls = 'expiring b5'
    elsif Time.zone.today > asset.expires_on && asset.public? && !asset.company.present?
      text = 'Expired'
      cls = 'expired b2'
    elsif asset.sold?
      text = 'Sold'
      cls = 'text b2'
    elsif can?(:update, asset) && asset.expires_on.present? && Time.zone.today < asset.expires_on && asset.public? && !asset.company.present?
      text = "Expiring #{format_date(asset.expires_on)}"
      cls = 'expiring'
    elsif asset.in_use_until.present? && Time.zone.today < asset.in_use_until && !current_page?(marketplace_path)
      text = "In Use Until #{format_date(asset.in_use_until)}"
      cls = 'b1'
    else
      text = 'Available'
      cls = 'available b2'
    end

    content_tag 'div', class: "status-text #{cls} #{controller_name}" do
      text
    end
  end

  def move_quantities(asset, move_from_asset)
    second_asset = Asset.find_by_id move_from_asset.to_i
    second_asset.quantity = second_asset.quantity - asset.quantity
    second_asset.is_inventory = false if (second_asset.quantity == 0)
    second_asset.save
  end

  def asset_status(asset, current_user)
    status = asset_status_text(asset, current_user)
    _cls = ''
    if status == 'Sold'
      _cls = 'sold'
    elsif status == 'Available'
      _cls = 'available'
    elsif status.present? && status.start_with?('In Use Until')
      _cls = 'available'
    end
    content_tag 'p', class: "status #{_cls}" do
      concat status
      if current_user.present? && (current_user.public? && asset.creator_id == current_user.id)
        concat( content_tag('span', class: 'my-public-asset'){' - My Asset' }) if current_page?(marketplace_path)
      end
    end
  end

  def this_company_assets(current_asset)
    !current_asset.id.nil? ? Asset.where.not(id: current_asset.id).where(category_id: current_asset.category.id, company_id: current_user.company_id, state: 'available') : Asset.is_inventory.where(company_id: current_user.company_id, state: 'available')
  end

  def asset_action(asset)
    content_tag 'p' do
      asset.action.humanize
    end
  end

  def asset_project(asset)
    # Return empty space to avoid div collapse
    return '&nbsp;'.html_safe  unless asset.creator.private?
    content_tag 'div', class: 'asset_project' do
      if asset.project.nil?
        'No Associated Site'
      else
        'Site: ' + asset.project.name
      end
    end
  end

  def asset_group(asset)
    # Return empty space to avoid div collapse
    return '&nbsp;'.html_safe  unless asset.creator.private?
    content_tag 'div', class: 'asset_project' do
      if asset.group.nil?
        'No Associated Group'
      else
        'Group: ' + asset.group.name
      end
    end
  end

  def year_make_model_quantity(asset)
    arr = [asset.make, asset.model]
    arr.insert(0, asset.year) if asset.year.present?
    arr << "(#{asset.quantity}x)" if asset.quantity > 1
    arr.join(' ')
  end

  def photo_url(asset)
    image_src = asset.photos.any? ? asset.photos[0].file.thumb.url : '/img-no-image-list-view.jpg'

    if Rails.env.development?
      begin
        File.open(Dir.pwd + '/public' + image_src)
      rescue
        image_src = '/uploads/tmp/128x128.jpg'
      end
    end

    image_src
  end

  def photo_thumb_link(asset, tracking_code, size = 100)
    image_src = asset.photos.any? ? asset.photos[0].file.thumb.url : '/img-no-image-list-view.jpg'

    if Rails.env.development?
      begin
        File.open(Dir.pwd + '/public' + image_src)
      rescue
        image_src = '/uploads/tmp/128x128.jpg'
      end
    end

    link_to custom_asset_path(asset, tracking_code), onClick: "ga('send', 'Internal Link', '#{asset.becomes(Asset)}');".html_safe,
            data: { confirm: tracking_code.nil? ? nil : 'Are you sure you want to link this asset to this QR code?'} do
      content_tag 'div', class: 'photo_wrapper' do
        #MARS-315-UI-changes
        #concat(add_company_badge(asset))
        concat(image_tag image_src, size: "#{size}")
      end
    end
  end

  def photo_thumb(asset, size = 100)
    image_src = asset.photos.any? ? asset.photos[0].file.thumb.url : '/img-no-image-list-view.jpg'
    if Rails.env.development?
      begin
        File.open(Dir.pwd + '/public' + image_src)
      rescue
        image_src = '/uploads/tmp/128x128.jpg'
      end
    end

    content_tag 'div', class: 'photo_wrapper' do
      concat(image_tag image_src, size: "#{size}")
    end
  end

  def add_company_badge(asset)
    if current_user.present?
      if current_user.private?
        if asset.company.present? && asset.company == current_user.company
          image_tag asset.public? ? 'icn-public-with-border.png' : 'icn-private-with-border.png', class: 'photo-badge', size: '24'
        end
      else
        if asset.creator == current_user
          image_tag 'icn-public-with-border.png', class: 'photo-badge', size: '24'
        end
      end
    end
  end

  # Used for bulk action drop-down menu
  def actions_for_user(user)
    actions = [
      ['Mark as Sold', 'sold'],
      ['Mark as Public', 'public'],
      ['Reallocate', 'reallocate'],
      ['Link to Group', 'link_to_group'],
      ['Delete', 'delete']
    ]

    unless current_user.company_admin? || current_user.can_assign_projects?
      unless current_user.public?
        actions.delete(['Mark as Sold', 'sold'])
      end
      actions.delete(['Mark as Public', 'public'])
    end

    # Public user cannot reallocate an asset
    if current_user.public?
      actions.delete(%w(Reallocate reallocate))
      actions.delete(['Link to Group', 'link_to_group'])
    end

    # Field member can't delete any asset
    if current_user.role == 'field_member'
      actions.delete(['Delete', 'delete'])
    end

    # Delete link to group action if no group is there
    if @groups && @groups.length == 0
      actions.delete(['Link to Group', 'link_to_group'])
    end

    # Delete Sold action for rental assets
    if params[:filter_search] && params[:filter_search]['rental'].to_i == 1
      actions.delete(['Mark as Sold', 'sold'])
      actions.delete(['Mark as Public', 'public'])
    end

    actions
  end

  # Used for bulk action drop-down menu
  def wanted_assets_actions_for_user(user)
    actions = []

    if !params[:filter_search] or (params[:filter_search] and params[:filter_search][:fulfilled].to_i == 0)
      actions << ['Mark as Fulfilled', 'fulfill']
    end

    actions << ['Delete', 'delete']
  end

  def get_asset_type(asset, current_user)
    if asset
      asset.asset_kind
    end
  end

  def is_our_asset(asset, current_user)
    (current_user && current_user.public? && asset.creator_id == current_user.id) ||
        (current_user && current_user.private? && asset.company_id == current_user.company_id)
  end

  def to_map_data(assets)
    assets.map do |asset|
      {
        geo: [asset.current_lat, asset.current_lng],
        title: asset.title,
        site: (asset.project.present? ? {
          id: asset.project_id,
          name: asset.project.name
        } : nil),
        location: asset.get_address_by_lat_lng,
        # img: photo_thumb(asset),
        img_url: photo_url(asset),
        asset_num: asset.asset_num,
        lastUpdate: asset.updated_at.strftime('%m/%d/%y %l:%M %p'),
        assetId: asset.id,
        hours: asset.hours || 0,
        miles: asset.miles || 0,
        tracking_code: asset.tracking_code_name,
        notes: asset.notes,
        lastFound: asset.last_location_changed,
        geofences: asset.all_included_geofences
      } if asset && asset.current_lat && asset.current_lng
    end
    .compact
  end

  def filter_description(params, is_csv)
    if params
      filters = []
      filters << "Equipment"                 if params[:equipment].to_i == 1
      filters << "Material"                  if params[:material].to_i == 1
      filters << params[:category]           if params[:category].present?
      filters << "Maintenance"               if params[:req_maintenance].to_i == 1
      filters << "QR Tracked"                if params[:has_qr_code].to_i == 1
      filters << "Owned"                     if params[:buy].to_i == 1
      filters << "Rentals"                   if params[:rental].to_i == 1
      filters << "Sold"                      if params[:sold].to_i == 1
      filters << "Abandoned"                 if params[:abandoned].to_i == 1
      filters << "Available"                 if params[:available].to_i == 1
      filters << "In Use"                    if params[:in_use].to_i == 1
      filters << "For Sale"                  if params[:for_sale].to_i == 1
      filters << "For Rent"                  if params[:for_rent].to_i == 1
      filters << params[:distance_zip].to_i  if params[:distance_zip].present?

      filters_without_zip = filters - [params[:distance_zip].to_i]
      if is_csv
        filters = params[:hide_filters].present? ? filters_without_zip.join(', ') : filters.join(', ')
      else
        filters = params[:hide_filters].present? ? "#{filters_without_zip.join(', ')} Assets - " :  "Filtered Assets - #{filters.join(', ')}."
      end
      return filters
    end
  end

  def export_assets_description(is_filtered, exported_assets)
    is_filtered ? "Filtered Assets - #{exported_assets}" : "#{exported_assets} Assets - "
  end

  def qr_versions_updated(assets, field_name)
    assets.assets_versions.where(field_name: field_name).last
  end

  def qr_versions_whodunnit(whodunnit)
    User.find(whodunnit)
  end

  def qr_scanning_time_ago(asset,field)
    version = qr_versions_updated(asset,field)
    if version
      time = version.updated_at
      time_ago = time_ago_in_words(time)
    end

    if time_ago.include? 'less than a minute'
      time_ago = 'just now'
    elsif time_ago.include?'minute' or time_ago.include?'minutes' or time_ago.include?'hours'
      time_ago = time_ago_in_words(time).gsub('minutes','min').gsub('about','').gsub('minute','min')
      time_ago + ' ago'
    elsif time_ago.include?'days' and time_ago.to_i > 8 or time_ago.include?'month'
      time_ago = time.strftime('%B')[0..2] + time.strftime(' %d')
    elsif time_ago.include?'years'
      time_ago = time.strftime('%B')[0..2] + time.strftime(' %d %y')
    else
      time_ago.gsub('minute','min').gsub('about','').gsub('over','')
    end
  end

end
