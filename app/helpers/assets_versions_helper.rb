module AssetsVersionsHelper
  ACTION_PUBLIC = {
    'for_rent': 'public market place asset for rent',
    'for_sale': 'public market place asset for sale',
    'owned': 'owned company asset',
    'rental': 'rental company asset'
  }

  # Used for asset version action label
  def version_action_label(version)
    if version.field_name == 'project_id' and version.action != "wanted"
      if version.action == 'update'
        'Reallocated'
      else
        'Allocated'
      end
    elsif version.field_name == 'action_public'
      'Status'
    elsif version.field_name == 'hours'
      'Hours'
    elsif version.action == "wanted"
      if version.field_name == "wanted_state_contributed" or version.field_name == "wanted_state_fulfilled"
        'Wanted'
      elsif version.field_name == "contact_id"
        'Contact'
      elsif version.field_name == "title"
        'Title'
      elsif version.field_name == "type"
        'Type'
      elsif version.field_name == "project_id"
        'Project'
      end
    elsif version.field_name == 'miles'
      'Miles'
    elsif version.field_name == 'quantity'
      'Quantity'
    elsif version.field_name == 'photo'
      "Photo #{version.action == 'create' ? 'Created' : 'Deleted'}"
    elsif version.field_name == 'group_id'
      if version.from and !version.to
        'Unlinked'
      elsif version.from and version.to
        'Linked to a different group'
      else
        'Linked to group'
      end
    else
      version.field_name.underscore.humanize
    end
  end

  # Used for asset version action value
  def version_value(version, f)
    value = version.try(f.to_sym)
    if version.field_name == 'project_id'
      if value && version.try(:project_id) == value
        version.project_name_not_analyzed
      elsif value
        Project.find_by_id(value).try(:name)
      else
        ''
      end
    elsif version.field_name == 'photo'
      ''
    elsif version.field_name == 'action_public'
      if value
        ACTION_PUBLIC[value.to_sym].humanize
      else
        ''
      end
    elsif version.field_name == 'group_id'
      Group.find_by_id(value).try(:name)
    elsif version.field_name == 'category_id'
      Category.find_by_id(value).try(:name)
    elsif version.field_name == 'contact_id'
      User.find_by_id(value).try(:name)
    else
      value
    end
  end

end
