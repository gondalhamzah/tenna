module CategoriesHelper

  def parseToGroupCategories(data)
    data.inject({}) do |result, item|
      if group_id = item.category_group_id
        result[group_id] ||= []
        result[group_id] << item
      end
      result
    end
  end

end
