module NavigationHelper

  # Determines whether to mark a navigation tab as active (darker styling)

  def tab_active(name)
    if controller_name == 'assets'
      if (%w[findit checklist].include? action_name) && name == 'findit'
        return 'active'
      end
      if action_name == "checklist" && name == 'findit_checklist'
        return 'active'
      end
      if action_name == "findit" && name == 'assets_findit'
        return 'active'
      end
      if action_name == "index" && name == 'assets'
        return 'active'
      end
      if (%w[show edit].include? action_name) && name == 'assets'
        return 'active'
        # (name == 'company' && show_as_company) ||
        #   (name == 'marketplace' && !show_as_company) ? 'active' : ''
      end
    elsif controller_name == "companies" && action_name == "print_qr"
      name == "print_qr" ? 'active' : ''
    elsif controller_name == "companies"
      name == "company_profile" ? 'active' : ''
    elsif controller_name == 'wanted_assets' && (%w[index show edit].include? action_name)
      name == 'wanted' ? 'active' : ''
    elsif controller_name == 'asset_reports' || controller_name == 'reports'
      if (name == 'reports') || (controller_name == 'asset_reports' && name == 'asset_reports') || (controller_name == 'reports' && name == 'company_reports')
        return 'active'
      end
    elsif controller_name == 'path_finder'
      if name == 'paths'
        return 'active'
      else
        if request.original_fullpath == "/path_finder"
          if name == 'create_route'
            return 'active'
          else
            return ''
          end
        end
        if request.original_fullpath == "/path_finder/manage_routes"
          name == 'manage_routes' ? 'active' : ''
        end
      end
    else
      company_controllers = %w[projects assets wanted_assets]
      user_controllers = %w[confirmations invitations passwords registrations sessions companies]

      controller_name == name ||
        (name == 'company' && (company_controllers.include? controller_name)) ||
        (name == 'user' && (user_controllers.include? controller_name)) ? 'active' : ''
    end
  end

  private

  def show_as_company
    current_user.present? && (@asset.creator == current_user || (@asset.company.present? && @asset.company == current_user.company))
  end

  def route_it_subscription
    user_signed_in? && current_user.company && current_user.company.subscriptions.where(category: 'routing').first
  end

  def asset_reports_subscription
    user_signed_in? && current_user.company && current_user.company.subscriptions.where(category: 'analytics').first
  end

end
