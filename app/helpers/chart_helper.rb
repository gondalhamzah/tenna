module ChartHelper
  def basic_chart(title: '', show_legend: true, flat: false, colors: ["#007FBF", "#91E300"], dollar: false,legend_vertical_align:'top')
    chart = {
      colors: colors,
      chart: {
        style: {
          color: '#65675e',
          fontSize: '12px',
          fontFamily: "'Helvetica Neue', Helvetica, Arial, sans-serif"
        }
      },
      credits: { enabled: false }
    }

    if title and title.length
      chart[:title] = {
        useHTML: true,
        text: title,
        style: {fontSize: '12px', fontWeight: 'bold', lineHeight: '1.02em'}
      }
    else
      chart[:title] = {text: ''}
    end

    if show_legend
      chart[:legend] = {
        layout: 'horizontal',
        align: 'center',
        verticalAlign: legend_vertical_align,
        floating: false,
        backgroundColor: '#FFFFFF',
        itemStyle: {fontSize: '12px', color: '#65675e', fontWeight: 'normal'},
        symbolWidth: 12,
        symbolRadius: 6
      }
    end

    chart[:chart][:options3d] = {
      enabled: true,
      alpha: 35,
      beta: 0
    } unless flat

    chart[:chart][:marginTop] =  30
    chart
  end

  def pie_chart(title: '', show_legend: true, flat: true, colors: ["#007FBF", "#91E300"],  cats: [], name: '', values: [], dollar: false)
    chart = basic_chart title: title, show_legend: show_legend, colors: colors, flat:flat, dollar:dollar, legend_vertical_align:'top'
    chart[:plotOptions] = {
      pie: {
        dataLabels: {
          enabled: false,
          useHTML: true,
          distance: -50,
          style: {
            fontWeight: 'bold',
            fontSize: '12px',
            width: '50px',
            color: 'white',
            textShadow: '0px 1px 2px black',
            marginLeft: '30px'
          },
          x: 25,
          y: -15,
          format: dollar ? '${y:,.0f}' : '{y:,.0f}'
        }
      },
      series: {
        allowPointSelect: true,
        cursor: 'pointer',
        point: {
          events: {
            click: ''
          }
        }
      }
    }
    chart[:chart][:marginTop] =  50

    sold_url = "/assets?utf8=%E2%9C%93&filter_search%5Bquery%5D=&filter_search%5Bsort%5D=date_created&filter_search%5Bpage_size%5D=&filter_search%5Bcoordinate%5D=&filter_search%5Bequipment%5D=0&filter_search%5Bmaterial%5D=0&filter_search%5Bcategory%5D=&filter_search%5Byear_min%5D=&filter_search%5Byear_max%5D=&filter_search%5Bhours_min%5D=&filter_search%5Bhours_max%5D=&filter_search%5Bhas_qr_code%5D=0&filter_search%5Bbuy%5D=0&filter_search%5Brental%5D=0&filter_search%5Bsold%5D=0&filter_search%5Bsold%5D=1&filter_search%5Babandoned%5D=0&filter_search%5Bavailable%5D=0&filter_search%5Bin_use%5D=0&filter_search%5Bpublic%5D=0&filter_search%5Bfor_sale%5D=0&filter_search%5Bfor_rent%5D=0&filter_search%5Bproject%5D=&filter_search%5Bdistance_miles%5D=&filter_search%5Bdistance_zip%5D=67430&filter_search%5Bfavorites%5D=0&filter_search%5Bhide_filters%5D=true&filter_search%5Bassets_title%5D=sold&button="
    abandoned_url = "/assets?utf8=%E2%9C%93&filter_search%5Bquery%5D=&filter_search%5Bsort%5D=date_created&filter_search%5Bpage_size%5D=&filter_search%5Bcoordinate%5D=&filter_search%5Bequipment%5D=0&filter_search%5Bmaterial%5D=0&filter_search%5Bcategory%5D=&filter_search%5Byear_min%5D=&filter_search%5Byear_max%5D=&filter_search%5Bhours_min%5D=&filter_search%5Bhours_max%5D=&filter_search%5Bhas_qr_code%5D=0&filter_search%5Bbuy%5D=0&filter_search%5Brental%5D=0&filter_search%5Bsold%5D=0&filter_search%5Babandoned%5D=0&filter_search%5Babandoned%5D=1&filter_search%5Bavailable%5D=0&filter_search%5Bin_use%5D=0&filter_search%5Bpublic%5D=0&filter_search%5Bfor_sale%5D=0&filter_search%5Bfor_rent%5D=0&filter_search%5Bproject%5D=&filter_search%5Bdistance_miles%5D=&filter_search%5Bdistance_zip%5D=67430&filter_search%5Bfavorites%5D=0&filter_search%5Bhide_filters%5D=true&filter_search%5Bassets_title%5D=abandoned&button="
    in_use_url = "/assets?utf8=%E2%9C%93&filter_search%5Bquery%5D=&filter_search%5Bsort%5D=date_created&filter_search%5Bpage_size%5D=&filter_search%5Bcoordinate%5D=&filter_search%5Bequipment%5D=0&filter_search%5Bmaterial%5D=0&filter_search%5Bcategory%5D=&filter_search%5Byear_min%5D=&filter_search%5Byear_max%5D=&filter_search%5Bhours_min%5D=&filter_search%5Bhours_max%5D=&filter_search%5Bhas_qr_code%5D=0&filter_search%5Bbuy%5D=0&filter_search%5Brental%5D=0&filter_search%5Bsold%5D=0&filter_search%5Babandoned%5D=0&filter_search%5Bavailable%5D=0&filter_search%5Bin_use%5D=0&filter_search%5Bin_use%5D=1&filter_search%5Bpublic%5D=0&filter_search%5Bfor_sale%5D=0&filter_search%5Bfor_rent%5D=0&filter_search%5Bproject%5D=&filter_search%5Bdistance_miles%5D=&filter_search%5Bdistance_zip%5D=67430&filter_search%5Bfavorites%5D=0&filter_search%5Bhide_filters%5D=true&filter_search%5Bassets_title%5D=in_use&button="
    available_url = "/assets?utf8=%E2%9C%93&filter_search%5Bquery%5D=&filter_search%5Bsort%5D=date_created&filter_search%5Bpage_size%5D=&filter_search%5Bcoordinate%5D=&filter_search%5Bequipment%5D=0&filter_search%5Bmaterial%5D=0&filter_search%5Bcategory%5D=&filter_search%5Byear_min%5D=&filter_search%5Byear_max%5D=&filter_search%5Bhours_min%5D=&filter_search%5Bhours_max%5D=&filter_search%5Bhas_qr_code%5D=0&filter_search%5Bbuy%5D=0&filter_search%5Brental%5D=0&filter_search%5Bsold%5D=0&filter_search%5Babandoned%5D=0&filter_search%5Bavailable%5D=0&filter_search%5Bavailable%5D=1&filter_search%5Bin_use%5D=0&filter_search%5Bpublic%5D=0&filter_search%5Bfor_sale%5D=0&filter_search%5Bfor_rent%5D=0&filter_search%5Bproject%5D=&filter_search%5Bdistance_miles%5D=&filter_search%5Bdistance_zip%5D=67430&filter_search%5Bfavorites%5D=0&filter_search%5Bhide_filters%5D=true&filter_search%5Bassets_title%5D=available&button="

    chart[:plotOptions][:pie][:depth] = 35 unless flat
    series_data = []

    cats.map.each_with_index do |c, i|
      if c.downcase == 'sold'
        series_data << { name:c, y:values[i],url: sold_url }
      elsif c.downcase == 'in use'
        series_data << { name:c, y:values[i],url: in_use_url }
      elsif c.downcase == 'abandoned'
        series_data << { name:c, y:values[i],url: abandoned_url }
      elsif c.downcase == 'available'
        series_data << { name:c, y:values[i],url: available_url }
      else
        series_data << { name:c, y:values[i] }
      end
    end

    chart['series'] = [{
       type: 'pie',
       name: name,
       data: series_data,
       showInLegend: show_legend,
       id: 'pie_chart'
     }]

    chart
  end

  def column_chart(title: '', show_legend: true, flat: true, colors: ["#007FBF", "#91E300"], cats: [], names: [], values: [], dollar: false, series: false, stacked: false, show_data_labels: true)
    chart = basic_chart title: title, show_legend: show_legend, colors: colors, flat: flat, dollar: dollar
    chart[:xAxis] =  {
      categories: cats,
      labels: {
        style: {
          color: '#65675e',
          fontSize: '12px',
          fontWeight: 'normal'
        }
      }
    }
    chart[:yAxis] = {
      min: 0,
      title: { text: '' },
      labels: {
        style: {
          color: '#65675e',
          fontSize: '12px',
          fontWeight: 'normal'
        }
      }
    }
    chart[:chart][:options3d] =  {
      enabled: true,
      alpha: 15,
      beta: 15,
      depth: 50,
      viewDistance: 25
    } unless flat
    chart[:chart][:defaultSeriesType] = 'column'
    chart[:plotOptions] = {
      column: {
      }
    }

    chart[:plotOptions][:column][:dataLabels] = {
          enabled: true,
          color: 'white',
          useHTML: true,
          style: {
            textShadow: '0 0 3px black',
            fontSize: '12px',
            width: '50px'
          },
          format: dollar ? '${y:,.0f}' : '{y:,.0f}'
        } if show_data_labels

    chart[:plotOptions][:column][:depth] = 25 unless flat
    if !series
      series = names.map.each_with_index do |name, i|
        {
          name: name,
          data: values[i],
          showInLegend: show_legend
        }
      end
    end
    chart[:series] = series

    if names.length > 1 || stacked
      chart[:chart][:marginTop] =  80
      chart[:plotOptions][:column][:stacking] =  'normal'
      chart[:yAxis][:stackLabels] = {
        enabled: true,
        style: { fontWeight: 'bold', fontSize: '12px', color: '#2d2a26' }
      }
      chart[:yAxis][:stackLabels][:format] = "${total:,.0f}" if dollar
    else
      chart[:plotOptions][:column][:dataLabels][:color] = 'black'
      chart[:plotOptions][:column][:dataLabels][:style][:textShadow] = '0 0 3px white'
    end

    chart
  end
end
