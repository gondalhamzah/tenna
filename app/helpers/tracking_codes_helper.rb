module TrackingCodesHelper
  def qr_codes_file_name(company_id, format_label)
    "#{company_id.to_s}_#{format_label}.pdf"
  end

  def qr_codes_file_path(company_id, format_label)
    if Rails.env.development?
      "#{Rails.root}/tmp/#{company_id.to_s}_#{format_label}.pdf"
    else
      "company_qr_codes/#{company_id.to_s}_#{format_label}.pdf"
    end
  end

  def qr_image_file_path(company_id)
    "#{Rails.root}/private/company_qr_images/#{company_id.to_s}_qr_code.png"
  end

  def files_details(company_id, format_label)
    {
      pdf_file_path: qr_codes_file_path(company_id, format_label),
      qr_image_path: qr_image_file_path(company_id),
    }
  end

  def prawn_label(label)
    { "#{label}" => {
        'paper_size' => [printed_sizes[label][1], printed_sizes[label][0]],
        'top_margin' => 10,
        'bottom_margin' => 10,
        'left_margin' => 10,
        'right_margin' => 10,
        'columns' => 1,
        'rows' => 1,
        'column_gutter' => 10,
        'row_gutter' => 10
      }
    }
  end

  def patch_file_path(label, key)
    "#{Rails.root}/private/#{template_directory_name(label)}/#{key.to_s}.jpg"
  end

  def template_directory_name(label)
    "#{printed_sizes[label][0].to_s}px by #{printed_sizes[label][1].to_s}px"
  end

  def printed_types
    ['laserPrint', 'thermalPrint']
  end

  def thermal_printed
    sizes = ['4 x 1', '4 x 2', '4 x 2.5', '4 x 3', '4 x 4']
    sizes.map { |value| [value, value.gsub(' ', '_')] }
  end

  def laser_printed
    sizes = ['Avery 5160', 'Avery 5163', 'Avery 5963', 'Avery 6572', 'Avery 6871']
    sizes.map { |value| [value, value.downcase.gsub(' ', '_')] }
  end

  def printed_sizes
    { '4_x_1' => [72, 288],
      '4_x_2' => [144, 288],
      '4_x_2.5' => [180, 288],
      '4_x_3' => [216, 288],
      '4_x_4' => [288, 288],
      'avery_5160' => [72, 190],
      'avery_5163' => [144, 288],
      'avery_5963' => [144, 288],
      'avery_6572' => [144, 190],
      'avery_6871' => [90, 172] }
  end

  def printed_patches
    { '4_x_1' => [:qr => [[10,55], 60], :right_content => [[72,59], 200]],
      '4_x_2' => [:qr => [[10,120], 120], :right_content => [[135,127], 133]],
      '4_x_2.5' => [:qr => [[18,100], 100], :right_content => [[113,90], 150], :top_content => [[10,155],250]],
      '4_x_3' => [:qr => [[17,125], 120], :right_content => [[138,117], 117], :top_content => [[10,200], 250]],
      '4_x_4' => [:qr => [[5,145], 150], :right_content => [[153,113], 115], :top_content => [[5,270], 259]],
      'avery_5160' => [:qr => [[2,58], 55], :right_content => [[58,61], 120]],
      'avery_5163' => [:qr => [[10,120], 120], :right_content => [[135,127], 133]],
      'avery_5963' => [:qr => [[10,120], 120], :right_content => [[135,127], 133]],
      'avery_6572' => [:qr => [[12,75], 72], :right_content => [[85,76], 82], :top_content => [[1,130], 170]],
      'avery_6871' => [:qr => [[2,72], 72], :right_content => [[78,77], 80]] }
  end

  def qr_sizes
    { '4_x_1' => [300,300],
      '4_x_2' => [600,600],
      '4_x_2.5' => [450,450],
      '4_x_3' => [600,525],
      '4_x_4' => [600,600],
      'avery_5160' => [263,300],
      'avery_5163' => [600,600],
      'avery_5963' => [600,600],
      'avery_6572' => [395,375],
      'avery_6871' => [357,375] }
  end

  def get_printed_size (key)
    printed_sizes[key]
  end

  def is_valid_print_type_and_label(type, label)
    if type.in? printed_types
      if type.eql? 'laserPrint'
        laser_printed.map { |array| array[1] }.include? label
      elsif type.eql? 'thermalPrint'
        thermal_printed.map { |array| array[1] }.include? label
      end
    end
  end

  def get_response action, status
    response = Hash.new
    case action
      when 'email_printed_qr_codes'
        case status
          when 'success'
            response['status'] = 'success'
            response['message'] = 'QR codes sent successfully.'
          when 'failed'
            response['status'] = 'failed'
            response['message'] = 'QR codes could not send successfully.'
          when 'not_generated'
            response['status'] = 'not_generated'
            response['message'] = 'QR codes not generated. Please generate qr codes first.'
        end
      when 'generate_printed_qr_codes'
        case status
          when 'success'
            response['status'] = 'success'
            response['message'] = 'QR Codes generated successfully.'
          when 'invalid_print_or_label'
            response['status'] = 'invalid_print_or_label'
            response['message'] = 'Invalid Label or Print Type'
          when 'generating'
            response['status'] = 'generating'
            response['message'] = 'Your QR codes are being generated.'
          when 'failed'
            response['status'] = 'failed'
            response['message'] = 'Something went wrong. QR codes are not being generated.'
        end
    end
    response
  end
end
