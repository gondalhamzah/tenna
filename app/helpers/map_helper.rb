module MapHelper
  def render_selected_icon(group_type, total, current_user)
    width = 23
    length = total.to_s.length

    if length > 3
      width = 5 * (length - 3)
    end

    cls_name = ''
    case group_type
      when 'owned_assets'
        if current_user.public?
          cls_name = 'for-sale'
        else
          cls_name = 'owned'
        end
      when 'rental_assets'
        if current_user.public?
          cls_name = 'for-rent'
        else
          cls_name = 'rental'
        end
      when 'public_assets'
        cls_name = 'public-asset'
      when 'public_public_assets'
        cls_name = 'public-public-asset'
      when 'projects'
        cls_name = 'project'
    end

    content_tag :div, total, class: "sidebar-multi-pin #{cls_name}-multi-pin b12", style: "width: #{width}px; height: #{width}px; line-height: #{width}px;"
  end
end
