module StaticMapHelper
    
      # Generates the static map (image) shown on asset details pages
    
      def asset_static_map(asset, options = {})
    
        coordinates = if asset.has_current_coordinates?
                        [asset.current_lng, asset.current_lat].join(",")
                      else
                        [asset.zip_code.lng, asset.zip_code.lat].join(",")
                      end
    
        # Must use image on S3 otherwise the map will not work in dev because the url has to be accessible by MAPBOX servers
        pin_url = CGI.escape( "http://s3.amazonaws.com/buildsourced-production-1/static-map-pin.png")
    
        pin_color = "fff"
        pin = "url-#{pin_url}(#{coordinates})"
        zoom_level = 18
        size = '650x400'
        access_token = Rails.application.secrets['keys']['map_box_api_key']
    
        query_string = "#{pin}/#{coordinates},#{zoom_level}/#{size}.png?access_token=#{access_token}"
    
        image_tag "//api.mapbox.com/v4/mapbox.streets/#{query_string}", alt: asset.title, class: 'responsive-img'
      end
    
      def wanted_asset_static_map(wanted_asset, options = {})
        asset_static_map(wanted_asset, options)
      end
    end