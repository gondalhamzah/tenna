module FabHelper

  # This helper is responsible for creating the FAB (fixed action button in Material Design) that appears on asset,
  # project, company & wanted screens. It creates the FAB with the appropriate actions for the current screen, taking
  # into consideration the current user's permissions.
  # Example: For a company user viewing an asset on a project they're not on, don't show edit or delete. Only show
  # create if that user is assigned to at least one project, thus able to create an asset for that project.

  NEW = 'icon-fab-add'
  EDIT = 'icon-fab-edit'
  DESTROY = 'icon-fab-trash'

  MAPPINGS = {
    index: [:new],
    findit: [:new],
    checklist: [:new],
    sold: [],
    show: [:edit, :new, :destroy],
    edit: [:new, :destroy],
    update: [:new, :destroy]
  }

  def fab(object, tracking_code = nil)
    action = action_name.to_sym
    mappings = get_mappings(action, object)
    return if mappings.empty?

    if user_signed_in? || request.host == 'localhost'
      content_tag :div, class: 'fixed-action-btn asset-btn' do
        btn = build_btn(mappings[0], object, true, tracking_code)
        concat(btn)
        other_actions = mappings.drop(1)
        if other_actions.present?
          ul = content_tag :ul do
            other_actions.each do |a|
              li = content_tag :li do
                other_btn = build_btn(a, object, false, tracking_code)
                concat(other_btn)
              end
              concat(li)
            end
          end
          concat(ul)
        end
      end
    end
  end

  private

  def build_btn(action, object, large, tracking_code = nil)
    link_options = {class: 'btn-floating'}
    link_options[:class].concat(' btn-large') if large

    case action
      when :edit
        link_url = url_for([:edit, get_sti_object(object), :back => request.fullpath])
        icon = EDIT
      when :destroy
        link_url = url_for(get_sti_object(object))
        link_options[:method] = :delete
        link_options[:data] ||= {}
        link_options[:data][:confirm] = 'Are you sure?'
        icon = DESTROY
      else
        link_url = url_for([:new, get_sti_class_sym(object), :back => request.fullpath, tracking_code: tracking_code])
        icon = NEW
    end

    link_to link_url, link_options, method: :delete do
      content_tag :i, '', class: icon
    end
  end

  def get_mappings(action, object)
    MAPPINGS[action].reject do |a|
      case a
        when :new
          klass = object.class == Class ? object : object.class
          (cannot? :create, klass unless klass == Asset) || klass == Company
        when :edit
          cannot? :update, object
        else
          cannot? a, object
      end
    end
  end

  def get_sti_object(object)
    ([Equipment, Material].include? object.class) ? object.becomes(Asset) : object
  end

  def get_sti_class_sym(object)
    klass = object.class == Class ? object : object.class
    klass = ([Equipment, Material].include? klass) ? Asset : klass
    klass.to_s.underscore.to_sym
  end

end
