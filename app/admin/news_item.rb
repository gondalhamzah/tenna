ActiveAdmin.register NewsItem do
  menu if: proc { current_admin_user.super_admin? | current_admin_user.business_admin? }

  permit_params :title, :description, :post_date, :priority, :featured, :slug, :active, :created_by, :last_updated_by

  controller do
    defaults finder: :find_by_slug!
  end

  index do
    column :title
    column :description do |news_item|
      truncate Loofah.fragment(news_item.description).scrub!(:strip).to_text, length: 250, separator: ' ', omission: '... (more)'
    end
    column :post_date
    column :priority
    column 'Published', :active
    column :created_by, sortable: :created_by do |news_item|
      AdminUser.where(id: news_item.created_by).first!.name
    end
    column :last_updated_by, sortable: :last_updated_by do |news_item|
      AdminUser.where(id: news_item.last_updated_by).first!.name
    end
    column :preview do |news_item|
      link_to 'Preview', news_item_path(news_item, flash_message: 'Preview'), target: '_blank'
    end
    actions
  end

  show do
    attributes_table do
      row :title
      row :description do |news_item|
        Loofah.fragment(news_item.description).scrub!(:strip).to_s.html_safe
      end
      row :post_date
      row :priority
      row 'Published', :active do |news_item|
        (news_item.active? ? "<div class='green bold'>Yes</div>" : "<div class='red'>No</div>").html_safe
      end
      row :created_by do |news_item|
        AdminUser.where(id: news_item.created_by).first!.name
      end
      row :last_updated_by do |news_item|
        AdminUser.where(id: news_item.last_updated_by).first!.name
      end
      row 'Custom URL', :slug do |news_item|
        news_item.slug
      end
      row 'Preview' do |news_item|
        link_to 'Click here', news_item_path(news_item, flash_message: 'Preview'), target: '_blank'
      end
    end
  end

  form do |f|
    semantic_errors

    inputs do
      input :title
      input :description, as: :html_editor
      input :post_date, as: :datepicker, input_html: { style: "width: 100px;" }
      input :priority, as: :select, collection: 1..20
      input :active, label: 'Published', as: :boolean
      if object.new_record?
        input :created_by, as: :hidden, input_html: { value: current_admin_user.id }
      end
      input :last_updated_by, as: :hidden, input_html: { value: current_admin_user.id }
      input :slug, label: 'Custom URL'
    end

    actions
  end
end
