ActiveAdmin.register Notification do
  menu priority: 10, if: proc { current_admin_user.super_admin? | current_admin_user.admin? }

  permit_params :duration, :message, :notification_datetime, :days_before, :is_active, :notification_type, :notification_medium, :location, :client_type, :admin_user_id, :ip, :metadata, :subject

  collection_action :get_template , method: :post do
    if params[:notification_type]
      if params[:notification_type].downcase == 'maintenance'
        template = render_to_string( template: 'admin/email_templates/notifications/maintenance', locals: { }, layout: false)
      elsif params[:notification_type].downcase == 'release notes'
        template = render_to_string( template: 'admin/email_templates/notifications/release_notes', locals: { }, layout: false)
      else
        render :json => { :errors => 'unprocessable entity' }, :status => 422
      end
      render json: template.to_json
    else
      render :json => { :errors => 'unprocessable entity' }, :status => 422
    end
  end

  collection_action :send_email ,method: [:patch, :post] do
    params_local = params
    if params_local[:notification_id]
      notification = Notification.find_by_id(params_local[:notification_id])
      params_tmp = {"notification": notification.as_json}
      params_local = ActionController::Parameters.new(params_tmp)
    end
    send_email = Notification.send_email?(params_local)
    if send_email
      if params[:notification][:notification_medium] == "Email" and params[:notification][:notification_type] == "Maintenance"
        email_subject =  params[:notification][:subject]
      else
        email_subject = Notification.get_subject_of_email(params_local[:notification][:message])
      end
      company_users_emails = Notification.get_email_ids(params_local[:notification][:client_type], params_local[:notification][:user_type])
      if company_users_emails.collect{|c| c.second.count}.sum > 0
        company_users_emails.each do |company_name, company_users_emails_array|
          if company_users_emails_array.count > 0
            AdminNotifications.send_email(company_users_emails_array,params_local,email_subject).deliver
          end
        end
        flash[:success] = 'Email sent successfully.'
        redirect_to admin_notifications_path
      else
        flash[:error] = 'No user found.'
        redirect_to admin_notifications_path
      end
    else
      flash[:error] = 'Unable to send email, please make sure you have selected valid options for email and message is not empty.'
      redirect_to admin_notifications_path
    end
  end

  index do
    selectable_column
    column :message do |notification|
      Loofah.fragment(notification.message).scrub!(:whitewash).to_s.html_safe
    end
    column :notification_datetime
    column :days_before
    column :duration
    column :is_active
    column :notification_type
    column :notification_medium
    column :location
    column :client_type
    column 'Send Email' do |notification|
      link_to 'Send Email', send_email_admin_notifications_path(notification_id: notification.id), method: :post if notification.notification_medium.downcase == 'email'
    end
    actions
  end

  index as: :block do |notification|
    div for: notification do
      resource_selection_cell notification
      h2 Loofah.fragment(notification.message).scrub!(:whitewash).to_s.html_safe
      hr
      div do
        if notification.is_active
          status_tag 'active', :ok
        else
          status_tag 'inactive', :error
        end
      end
    end
  end

  show do
    attributes_table do
      row :id
      row :admin_user_id
      row :message do |notification|
        Loofah.fragment(notification.message).scrub!(:whitewash).to_s.html_safe
      end
      row :notification_datetime
      row :days_before
      row :duration
      row :is_active
      row :notification_type
      row :notification_medium
      row :location
      row :metadata
      row :client_type
      row :created_at
      row :updated_at
      div link_to 'Send Email', send_email_admin_notifications_path(notification_id: notification.id), method: :post, class: 'waves-effect waves-light btn send-email-btn' if notification.notification_medium.downcase == 'email'
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :notification_medium, as: :select, include_blank: false, collection: Notification::NOTIFICATION_MEDIUM
      f.input :notification_datetime , :as => :datetime_picker, :wrapper_html => { class: 'popup-field' }
      f.input :days_before , input_html: { min: '0'}, :wrapper_html => { class: 'popup-field' }
      f.input :duration , input_html: { min: '0'}, :wrapper_html => { class: 'popup-field' }
      f.input :is_active, :wrapper_html => { class: 'popup-field' }
      f.input :notification_type, as: :select, include_blank: false, collection: Notification::NOTIFICATION_TYPE
      f.input :location, as: :select, include_blank: false, collection: Notification::LOCATIONS, :wrapper_html => { class: 'popup-field'}
      f.input :client_type, label: 'USER TYPE', as: :select, include_blank: false, collection: Notification.client_types.reject{|r| r == 'admin' }.map { |k, _| [k.titleize, k] }
      f.input :subject, input_html: {value: 'Technology Update for'}
      f.input :admin_user_id , input_html: {value: current_admin_user.id}, :as => :hidden
      f.input :ip , as: :string , input_html: {value: request.remote_ip}, wrapper_html: {style: 'display:none;'}
      f.input :metadata , input_html: {value: "browser: \n #{request.env['HTTP_USER_AGENT']} \n\n city: \n #{(request.location.present? && request.location.city.present?) ? request.location.city : ''} \n\n country: \n #{(request.location.present? && request.location.country.present?) ? request.location.country : ''} \n\n "}, :as => :hidden
      f.input :message , :as => :ckeditor, :toolbar => 'mini', label:false
    end
    f.button 'Send Email', id: 'send_email_button'
    f.actions
  end
end
