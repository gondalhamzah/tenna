ActiveAdmin.register AdminUser do
  menu if: proc { current_admin_user.super_admin? }

  filter :first_name
  filter :last_name
  filter :email

  permit_params :first_name, :last_name, :email

  index do
    selectable_column
    column :email
    column :first_name
    column :last_name

    actions
  end

  show do
    attributes_table do
      row :id
      row :email
      row :first_name
      row :last_name

      row :created_at
      row :updated_at
    end

    active_admin_comments
  end

  collection_action :new_invitation do
    @user = AdminUser.new
  end

  collection_action :send_invitation, :method => :post do
    @user = AdminUser.invite!(params[:user], current_user)
    if @user.errors.empty?
      flash[:success] = "User has been successfully invited."
      redirect_to admin_users_path
    else
      messages = @user.errors.full_messages.map { |msg| msg }.join
      flash[:error] = "Error: " + messages
      redirect_to new_invitation_admin_users_path
    end
  end
  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :email
      f.input :first_name
      f.input :last_name
    end
    actions
  end

  controller do
    def create
      @user = AdminUser.invite!(permitted_params[:admin_user], current_user)
      if @user.valid?
        flash[:success] = "User has been successfully invited."
        redirect_to admin_users_path
      else
        render :new
      end

    end
  end

end

