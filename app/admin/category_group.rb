ActiveAdmin.register CategoryGroup do

 config.filters = false
  # menu priority: 8, if: proc { current_admin_user.super_admin? | current_admin_user.admin? }

  permit_params :name

  index do
    selectable_column
    column :name

    actions
  end

  show do
    attributes_table do
      row :id
      row :name

      row :created_at
      row :updated_at
    end

    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :name

    end

    f.actions
  end


end
