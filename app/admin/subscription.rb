ActiveAdmin.register Subscription do
  menu if: proc { current_admin_user.super_admin? }

  permit_params :company_id, :category, :number_of_units, :state, :note, :payment

  index do
    column :company, sortable: :company_id
    column :api_key do |subscription|
      subscription.api_key.present? ? (link_to 'Click to see', admin_subscription_path(subscription)) : 'Blank'
    end
    column :api_key_created_at
    column :api_key_updated_at
    column :category
    column :number_of_units
    column :state
    column :note

    actions
  end

  member_action :generate_new_api_key, method: :get do
    subscription = Subscription.find params[:id]
    subscription.update_api_key!

    if subscription.errors.present?
      flash[:error] = subscription.errors.full_messages.to_sentence
    end
    redirect_to admin_subscription_path(subscription)
  end

  action_item :generate_api_key, only: [:show, :edit] do
    link_to 'Generate New API Key', generate_new_api_key_admin_subscription_path(params[:id]) if subscription.api_key.blank?
  end

  member_action :soft_delete, method: :get do
    subscription = Subscription.find params[:id]

    subscription.soft_delete
    flash[:success] = 'Subscription inactivated successfully'

    redirect_to admin_subscription_path(subscription)
  end

  member_action :reactivate, method: :get do
    subscription = Subscription.find params[:id]

    subscription.reactivate
    flash[:success] = 'Subscription activated successfully'

    redirect_to admin_subscription_path(subscription)
  end

  action_item :soft_delete_or_reactivate, only: [:show] do
    if subscription.state == 'active'
      link_to 'Soft Delete', soft_delete_admin_subscription_path(params[:id])
    else
      link_to 'Reactivate', reactivate_admin_subscription_path(params[:id])
    end
  end

  show do
    attributes_table do
      row :company
      row :api_key
      row :api_key_created_at
      row :api_key_updated_at
      row :category do |subscription|
        subscription.category&.capitalize
      end
      row :number_of_units
      row :state do |subscription|
        subscription.state&.capitalize
      end
      row :payment
      row :note
      row :deleted do |subscription|
        if subscription.deleted_at.present?
          "<span class='red'>Deleted at #{subscription.deleted_at.strftime('%l:%M%P').strip} on #{subscription.deleted_at.strftime('%e %B %Y').strip}.</span>".html_safe
        else
          "<span class='green'>No</span>".html_safe
        end
      end
    end
  end

  form do |f|
    semantic_errors

    inputs do
      input :company, as: :select, collection: Hash[Company.all.sort_by(&:name).map { |c| ["#{c.name} (#{c.id})", c.id] }], selected: (params[:company_id] || subscription.company_id)
      input :category, as: :select, collection: Subscription.categories, include_blank: false
      input :number_of_units, label: 'number of units (for routing only)'
      input :state, as: :select, collection: Subscription.states, include_blank: false
      input :payment, as: :select, collection: ['true','false'], include_blank: false
      input :note
      input :api_key, input_html: { disabled: true }
      input :api_key_created_at, input_html: { disabled: true }, as: :datepicker
      input :api_key_updated_at, input_html: { disabled: true }, as: :datepicker
    end

    actions
  end
end
