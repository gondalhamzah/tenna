ActiveAdmin.register Material do

  filter :title
  filter :location_zip
  filter :price

  menu label: 'Material', priority: 2, parent: 'Assets', parent_priority: 7, if: proc { current_admin_user.super_admin? | current_admin_user.admin? }

  permit_params :title, :asset_num, :description, :company_id, :project_id,
    :creator_id, :category_id, :location_zip, :price, :available_on, :expires_on,:units,
    :quantity, :units,:country_name, :certified, :condition, :status, :media, :attachments,:group_id,
    :asset_kind, :rental, :public, :owned, :tag_list, :market_value,:time_length,
    pdfs_attributes: [:id, :file, :file_cache, :'_destroy'],
    photos_attributes: [:id, :file, :file_cache, :'_destroy']

  index do
    selectable_column
    column :title

    column :asset_kind do |material|
      material.asset_kind.humanize.titleize unless material.asset_kind.blank?
    end

    column :company
    column :project
    column :creator

    column :status do |equipment|
      equipment.state.humanize.titleize unless equipment.state.blank?
    end

    actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :asset_num
      if material.asset_kind == "rental"
        row :units
        row :time_length
      end
      row :asset_kind do |material|
        material.asset_kind.humanize.titleize unless material.asset_kind.blank?
      end
      row :description
      row :company_id
      row :project_id
      row :group_id
      row :creator_id
      row :category_id
      row :location_zip
      row :country_name
      row :price
      row :market_value
      row :available_on
      row :expires_on
      row :quantity
      # row :units
      row :certified
      row :owned
      row :rental
      row :condition
      row :public
      row :status do |material|
        material.state unless material.state.blank?
      end
      row :media
      row :photos do
        material.photos.map do |photo|
          image_tag photo.file.url, width: '128px;'
        end.join("<br />").html_safe
      end
      row :pdfs do
        material.pdfs.map do |pdf|
          link_to pdf.file.file.filename, pdf.file.url
        end.join("<br />").html_safe
      end

      row :created_at
      row :updated_at
    end

    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :title
      f.input :asset_num
      f.input :asset_kind, as: :select, include_blank: false, collection: Asset::ASSET_KINDS.map { |c, _| [c.titleize, c.to_sym] }
      f.input :units,collection: Asset::RENT_UNITS
      f.input :time_length
      f.input :description
      f.input :company, required: true
      f.input :project, collection: Project.available.map { |p| [p.name, p.id, {'data-company' => p.company_id}] }
      f.input :group, collection: Group.available.map { |g| [g.name, g.id, {'data-project' => g.project_id}] }
      f.input :creator, required: true
      f.input :category, include_blank: false, required: true
      if f.object.new_record?
        f.object.build_zip_code
      end
      f.input :location_zip
      f.input :country_name,required: true, as: :select,prompt: "Select country" ,collection: ZipCode.country_lists
      f.input :price
      f.input :market_value
      f.input :available_on
      f.input :expires_on
      f.input :quantity
      # f.input :units
      f.input :certified
      #f.input :status, as: :select, include_blank: false, collection: Asset.statuses.map { |s, _| [s.titleize, s] }

      # f.input :media

      f.input :tag_list, as: :string
      f.has_many :photos, heading: 'Photos', allow_destroy: true do |p|
        p.input :file
      end
      f.has_many :pdfs, heading: 'Pdfs', allow_destroy: true do |p|
        p.input :file
      end
    end
    f.actions
  end
end
