ActiveAdmin.register Company do
  filter :name
  filter :contact
  filter :id

  menu priority: 3, if: proc { current_admin_user.super_admin? | current_admin_user.admin? }

  permit_params :company_type, :name, :company_logo, :region, :website, :contact_id, :admin_seats, :active, :project_id,:api_rpm, address_attributes: [:id, :line_1, :line_2, :city, :state, :zip, :country], contact_attributes: [:first_name, :last_name, :email, :phone_number], project_attributes: [:id, :name, :project_num, :description, :duration, :closeout_date, address_attributes: [:line_1, :line_2, :city, :state, :zip, :country]], project_address_attributes: [:line_1, :line_2, :city, :state, :zip, :country]

  member_action :new_api_key, method: :get do
    company = Company.find(params[:id])
    company.api_key = SecureRandom.hex
    company.api_rpm = 100
    company.save
    redirect_to admin_companies_path, notice: 'New API key has been generated successfully.'
  end

  member_action :api_key_status, method: :get do
    company = Company.find(params[:id])
    if params[:type] == 'enabled' and company.api_key_status.nil?
      company.api_key = SecureRandom.hex
      company.api_key_status = true
    elsif params[:type] == 'enabled'
      company.api_key_status = true
    elsif params[:type] == 'disabled'
      company.api_key_status = false
    end
    company.save
    redirect_to admin_companies_path, notice: "API key has been successfully #{params[:type]}."
  end

  member_action :resend_api_key, method: :get do
    company = Company.find(params[:id])
    AdminNotifications.resend_api_key(company).deliver_now
    redirect_to admin_companies_path, notice: 'API key has been sent successfully.'
  end

  action_item :soft_delete_company, only: [:show, :edit]  do
    link_to 'Soft Delete', { action: :destroy }, method: :delete, data: { confirm: 'Are you sure?' }
  end

  action_item :destroy_company, only: [:show, :edit]  do
    if(AdminAbility.new(current_admin_user).can? :hard_destroy, company)
      link_to('Destroy', admin_company_path(company, hard: true), method: :delete, data: { confirm: 'Are you sure?' })
    end
  end

  index do
    selectable_column
    column :name
    column :region
    column :contact
    column :admin_seats
    column :company_type
    column :status do |company|
      company.active? ? status_tag('Active', :ok) : status_tag('Inactive')
    end

    column :address do |company|
      simple_format(company.address.formatted_address) unless company.address.nil?
    end

    actions defaults: false do |c|
      acts = link_to('View', admin_company_path(c)) +
             link_to('Edit', edit_admin_company_path(c)) +
             link_to('Soft Delete', admin_company_path(c), method: :delete, data: { confirm: 'Are you sure?' })+
             link_to('Generate New API Key', new_api_key_admin_company_path(c))
      if(AdminAbility.new(current_admin_user).can? :hard_destroy, c)
        acts += link_to('Destroy', admin_company_path(c, hard: true), method: :delete, data: { confirm: 'Are you sure?' })
      else
        acts
      end
      acts += link_to('Add Subscription', new_admin_subscription_path(company_id: c.id))
      acts += link_to('Resend API key', resend_api_key_admin_company_path(id: c)) if c.api_key.present?
      if c.api_key_status
        acts += link_to('Disable API key', api_key_status_admin_company_path(id: c.id, type: 'disabled'), data: { confirm: 'Are you sure?' })
      else
        acts += link_to('Enable API key', api_key_status_admin_company_path(id: c, type: 'enabled'), data: { confirm: 'Are you sure?' })
      end
    end

    column :subscriptions do |company|
      company.subscriptions.collect { |sub| link_to sub.category.capitalize, admin_subscription_path(sub) }.join('<br>').html_safe
    end
  end

  action_item :add_subscription, only: [:show, :edit] do
    link_to('Add Subscription', new_admin_subscription_path(company_id: company.id))
  end

  show do
    attributes_table do
      row :id
      row :name
      row :image do |company|
        image_tag(company.company_logo.url(:thumb) ) unless company.company_logo.nil?
      end
      row :region
      row :website
      row :contact

      row :admin_seats
      row :company_type
      row :project_id
      row :status do |company|
        company.active? ? status_tag('Active', :ok) : status_tag('Inactive')
      end

      row :address do
        simple_format(company.address.formatted_address) unless company.address.nil?
      end

      row :created_at
      row :updated_at

      row :subscriptions do |company|
        company.subscriptions.collect { |sub| link_to sub.category.capitalize, admin_subscription_path(sub), class: sub.state == 'active' ? 'green' : 'red' }.join('<br>').html_safe
      end

      row :api_rpm
      row :api_key
    end

    active_admin_comments
  end

  form do |f|
    company = Company.find(params[:id]) unless f.object.new_record?
    f.semantic_errors :name,
                      :company_logo,
                      :region,
                      :website,
                      :admin_seats,
                      :active,
                      :api_rpm,
                      :'contact.email',
                      :'contact.first_name',
                      :'contact.last_name',
                      :'address.line_1',
                      :'address.line_2',
                      :'address.city',
                      :'address.state',
                      :'address.zip',
                      :'address.country',
                      :'project.name',
                      :'project.closeout_date',
                      :'project_address.line_1',
                      :'project_address.city',
                      :'project_address.state',
                      :'project_address.zip'

    f.inputs 'Details' do
      f.input :name
      f.input :region
      f.input :website
      f.input :admin_seats
      f.input :company_type
      (f.input :project_id, as: :select , include_blank: false, collection: Hash[Project.where(company_id: f.object.id).map { |p| ["#{p.name} (#{p.id})", p.id] }]) unless f.object.new_record?
      f.input :active
      f.input :api_rpm
      f.input :api_key, input_html: { readonly: true, disabled: true }

      f.inputs do
        f.input :company_logo, :as => :file
      end
    end

    if f.object.new_record?
      f.inputs 'Contact*', for: [:contact, f.object.contact || User.new] do |contact|
        contact.input :email
        contact.input :first_name
        contact.input :last_name
        contact.input :phone_number
      end
    else
      f.inputs 'Contact*' do
        f.input :contact_id, as: :select, collection: Hash[User.all.map { |u| ["#{u.name} (#{u.id})", u.id] }]
      end
    end

    f.inputs 'Address*', for: [:address, f.object.address || Address.new] do |address|
      address.input :line_1
      address.input :line_2
      address.input :city
      address.input :state
      address.input :zip
      address.input :country,required: true, as: :select,prompt: 'Select country' ,collection: ZipCode.country_lists
      # address.input :country, required: true, include_blank: false, as: :select, collection: [ "Canada", "United States" ]
    end

      f.inputs 'Default Project*', for: [:project_attributes, f.object.project || Project.new] do |project|
        project.input :id, as: :hidden
        project.input :name, required: true
        project.input :project_num
        project.input :description
        project.input :duration
        project.input :closeout_date, required: true
      end

      if f.object.new_record?
        f.inputs 'Project Address*', for: [:project_address_attributes,Address.new] do |address_fields|
          address_fields.input :line_1, required: true
          address_fields.input :line_2
          address_fields.input :city, required: true
          address_fields.input :state, required: true
          address_fields.input :zip, required: true
          address_fields.input :country,required: true, as: :select, prompt: 'Select country', collection: ZipCode.country_lists
        end
      else
        f.inputs 'Project Address*', for: [:project_address_attributes, f.object.project ? f.object.project.address : Address.new] do |address_fields|
          address_fields.input :line_1, required: true
          address_fields.input :line_2
          address_fields.input :city, required: true
          address_fields.input :state, required: true
          address_fields.input :zip, required: true
          address_fields.input :country,required: true, as: :select,prompt: 'Select country', collection: ZipCode.country_lists
        end
      end

    f.actions
  end

  controller do
    def create

      params[:company][:project_attributes][:address_attributes] =  params[:company][:project_address_attributes]
      params[:company].delete(:project_address_attributes)
      company = build_resource

      contact = company.contact
      company.api_key = SecureRandom.hex
      company.api_key_status = false
      company.api_rpm = 100

      project = company.project

      contact.role = User.roles[:company_admin]
      contact.skip_confirmation!

      # Because company requires a contact and a private user requires a company,
      # we have to save these entities without AR performing validation on save.
      # Instead, we explicitly validate here, removing expected errors.
      ActiveRecord::Base.transaction do
        contact.validate
        contact.errors.delete(:password)
        contact.errors.delete(:company_id)
        if contact.errors.count == 0
          contact.save(validate: false)
        end

        project.validate
        project.errors.delete(:company)
        project.errors.delete(:contact)
        if project.errors.count == 0
          project.save(validate: false)
        end

        company.contact = contact

        company.validate
        company.errors.delete(:'contact.password')
        company.errors.delete(:'contact.company_id')
        company.errors.delete(:'contact_id')
        company.errors.delete(:'project.company')
        company.errors.delete(:'project.contact')
        if company.errors.count == 0
          company.save(validate: false)
        end

        contact.company = company
        project.company = company
        project.contact = contact

        contact.save!
        project.save!
        company.save!

        contact.invite!
      end

      redirect_to admin_companies_path
    rescue ActiveRecord::RecordInvalid => e
      render :edit
    end

    def update(options={}, &block)
      params[:company][:project_attributes][:address_attributes] =  params[:company][:project_address_attributes]
      params[:company].delete(:project_address_attributes)
      # This is taken from the active_admin code
      super do |success, failure|
        block.call(success, failure) if block
        failure.html { render :edit }
      end
    end

    def destroy
      @company = Company.find(params[:id])
      if params[:hard]
        raise CanCan::AccessDenied unless AdminAbility.new(current_admin_user).can?(:hard_destroy, @company)
        if @company.project
          default_proj = @company.project
          @company.project = nil
          default_proj.company = nil
          default_proj.save(validate: false)
          @company.save(validate: false)
          Project.destroy(default_proj.id)
        end
        if @company.contact
          company_contact = @company.contact
          @company.contact = nil
          company_contact.company = nil
          company_contact.save(validate: false)
          @company.save(validate: false)
          User.destroy(company_contact.id)
        end

        if @company.destroy
          flash[:success] = 'Company deleted successfully.'
        else
          flash[:error] = 'Something went wrong.'
        end
      else
        @company.update(active: false)
        @company.users.each{|u| u.soft_delete}
      end
      redirect_to admin_companies_path
    end
  end

  after_save do |company|
    company.update_active_state!
  end
end
