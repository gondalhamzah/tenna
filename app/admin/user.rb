include UsersHelper

ActiveAdmin.register User do
  filter :first_name
  filter :last_name
  filter :email
  filter :phone_number

  menu priority: 2, if: proc { current_admin_user.super_admin? | current_admin_user.admin? }


  permit_params :first_name, :last_name, :company_id, :company_name, :role, :email, :password, :password_confirmation, :phone_number, :title
  config.clear_action_items!

  member_action :resend_invitation, method: :get do
    user = User.find(params[:id])
    user.invite!(current_user)
    redirect_to admin_users_path, notice: "Reinvitation email sent successfully."
  end

  action_item :soft_delete_user, only: [:show, :edit]  do
    link_to "Soft Delete", { action: :destroy }, method: :delete, data: { confirm: 'Are you sure?' }
  end

  member_action :reset_password, method: :get do
    user = User.find_by_id params[:id]
    if user
      if user.send_reset_password_instructions
        redirect_to resource_path(params[:id]), notice: "Reset password email sent successfully."
      else
        redirect_to resource_path(params[:id]), notice: "Something went wrong."
      end
    end
  end

  index do
    selectable_column
    column :first_name
    column :last_name

    column :company do |user|
      if user.company.nil?
        user.company_name
      else
        link_to user.company.name, admin_company_path(user.company)
      end
    end

    column :role do |user|
      if user.role.present?
        user.role.humanize.titleize
      else
        'User'
      end
    end

    column :email

    column :phone_number do |user|
      link_to phony_formatted(user.phone_number), "tel:#{user.phone_number.phony_normalized}" unless user.phone_number.blank?
    end

    column :status do |user|
      user.status == 'Active' ? status_tag(user.status, :ok) : status_tag(user.status)
    end

    # column :current_sign_in_at
    # column :sign_in_count
    # column :created_at

    actions defaults: false do |u|
      link_to("View", admin_user_path(u)) +
      link_to("Edit", edit_admin_user_path(u)) +
      link_to("Soft Delete", admin_user_path(u), method: :delete, data: { confirm: 'Are you sure?' }) +
      if u.invitation_accepted_at.nil?
        link_to("Resend invitation", resend_invitation_admin_user_path(u.id))
      end
    end
  end

  show do
    attributes_table do
      row :id
      row :first_name
      row :last_name
      row :company
      row :company_name
      row :email
      row :encrypted_password
      row :phone_number do |user|
        phony_formatted(user.phone_number)
      end
      row :reset_password_token
      row :reset_password_sent_at
      row :remember_created_at
      row :sign_in_count
      row :current_sign_in_at
      row :last_sign_in_at
      row :current_sign_in_ip
      row :last_sign_in_ip
      row :confirmation_token
      row :confirmation_sent_at
      row :unconfirmed_email
      row :created_at
      row :updated_at
      row :invitation_token
      row :invitation_created_at
      row :invitation_sent_at
      row :invitation_accepted_at
      row :invitation_limit
      row :invited_by
      row :invited_by_type
      row :invitations_count
      row :role
      row :title
      row :subscription_opt

      row :status do |user|
        user.status == 'Active' ? status_tag(user.status, :ok) : status_tag(user.status)
      end

      if user.active
        row :reset_password do |user|
          link_to('Forgot your password?',reset_password_admin_user_path(id: user.id))
        end
      end

    end
  end

  controller do
    def update
      if params[:user][:password].blank?
        params[:user].delete('password')
        params[:user].delete('password_confirmation')
      end

      super
    end

    def destroy
      @user = User.find(params[:id])
      @user.soft_delete
      redirect_to admin_users_path
    end
  end



  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :first_name
      f.input :last_name

      f.input :public_user?, as: :boolean, label: 'Public'
    end

    f.inputs 'Company' do
      f.input :company, :label => 'Private', :as => :select, :collection => Hash[Company.all.map { |c| ["#{c.name} (#{c.id})", c.id] }]
      f.input :company_name, :label => 'Public'
      f.input :role, as: :select, include_blank: false, collection: User.roles.reject{|r| r == "admin" }.map { |k, _| [k.titleize, k] }
      f.input :title, :label => 'Title'
    end

    f.inputs do
      f.input :email
      f.input :phone_number, input_html: { value: phony_formatted(f.object.phone_number) }
      f.input :password
      f.input :password_confirmation
    end

    f.actions
  end

end
