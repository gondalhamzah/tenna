ActiveAdmin.register Project ,as: "site" do
  filter :name
  filter :company
  filter :contact
  filter :id

  menu priority: 4, if: proc { current_admin_user.super_admin? | current_admin_user.admin? }

  permit_params :name, :project_num, :description, :company_id, :contact_id, :duration, :closeout_date,
                address_attributes: [:id, :line_1, :line_2, :city, :state, :zip, :country,:lat,:lng,:change_addr,:current_coords],
                project_assignments_attributes: [:id, :user_id, :role, :_destroy]

  index do
    selectable_column
    column :name
    column :company
    column :contact

    column :address do |project|
      simple_format(project.address.formatted_address) unless project.address.nil?
    end

    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row "site num", :project_num
      row :description
      row :company
      row :contact
      row :duration
      row :closeout_date

      row :address do
        simple_format(site.address.formatted_address) unless site.address.nil?
      end

      row :created_at
      row :updated_at
    end

    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :name
      f.input :project_num, label: 'site num'
      f.input :description, as: :text
      f.input :company, as: :select, collection: Hash[Company.all.map { |c| ["#{c.name} (#{c.id})", c.id] }]
      f.input :contact, as: :select, collection: Hash[User.all.map { |u| ["#{u.name} (#{u.id})", u.id] }]
      f.input :duration
      f.input :closeout_date, as: :date_select
    end

    inputs 'Map' do
      "<input type='text' id='searchTextField' placeholder='Enter Location' style='position: absolute;z-index: 1;
      margin-top: 5px;height: 23px;width: 50%;'><div id='map_canvas' style='width: 100%;height: 300px'></div>".html_safe
    end

    f.inputs 'Address', for: [:address, f.object.address || Address.new] do |address|
      address.input :line_1
      address.input :line_2
      address.input :city
      address.input :state
      address.input :zip
      # address.input :country, required: true, include_blank: false, as: :select, collection: [ "Canada", "United States" ]
      address.input :country,required: true, as: :select,prompt: "Select country" ,collection: ZipCode.country_lists
      address.input :lat, as: :hidden
      address.input :lng, as: :hidden
      address.input :change_addr, as: :hidden
      address.input :current_coords, :input_html => { :value => cookies[:coordinates]}, as: :hidden
    end

    f.inputs 'Users' do
      f.has_many :project_assignments, allow_destroy: true, heading: false, new_record: 'Add User' do |a|
        a.input :project_id, as: :hidden
        a.input :user, as: :select, collection: Hash[User.all.map { |u| ["#{u.name} (#{u.id})", u.id] }]
        a.input :role, required: true, include_blank: false, as: :select, collection: ProjectAssignment.roles.map { |r, _| [ProjectAssignment.role_lookup.fetch(r, r.titleize), r] }
      end
    end

    f.actions
  end
  controller do
    def update
      super
      flash[:notice] = 'Site was successfully updated.'
    end

    def create
      super
      flash[:notice] = 'Site was successfully created.'
    end

    def destroy
      super
      flash[:notice] = 'Site was successfully deleted.'
    end
  end
end
