ActiveAdmin.register Group do
  filter :name
  filter :company
  filter :project
  filter :client
  filter :id

  menu priority: 5

  permit_params :name, :group_num, :project_id, :company_id, :notes, :description, :client

  index do
    selectable_column
    column :id
    column :name
    column :group_num
    column :client
    column :company
    column :project
    column :state

    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :group_num
      row :description
      row :company
      row :project
      row :created_at
      row :updated_at
      row :state
      row :client
      row :notes
    end

    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :name
      f.input :group_num
      f.input :description, as: :text
      f.input :notes
      f.input :client
      f.input :company_id, as: :select, collection: Hash[Company.all.map { |c| ["#{c.name} (#{c.id})", c.id] }]
      f.input :project_id, as: :select, collection: Hash[Project.all.map { |c| ["#{c.name} (#{c.id})", c.id] }]
    end
    f.actions
  end


end
