ActiveAdmin.register TrackingCode do
    menu label: 'qr-urls', priority: 12, if: proc { current_admin_user.super_admin? | current_admin_user.admin? | current_admin_user.hr_admin? }
    actions :all, :except => [:edit]
    permit_params :token, :no_of_urls

    form do |f|
	    f.inputs do
	      f.input :no_of_urls, as: :number, input_html: { required: true }
	      f.submit 'Generate'
	    end
  	end

	  controller do
	    def create
	    	urls = []
    		tokens = []
    		n =  params[:tracking_code][:no_of_urls].to_i
		    n.times do
		      uuid = SecureRandom.uuid
					uuid = uuid.gsub!('-', '')
		      str = Base64.encode64([uuid].pack('H*'))[0..21]
		      str = str.gsub('+', '-')
		      str = str.gsub('/', '~')
		      tokens << str
		      urls << "http://#{ENV["QR_DOMAIN"]}/#{str}"
		    end

				TrackingCode.transaction do
		      tokens.each do |token|
		        TrackingCode.create(token: token)
		      end
		    end

		    if TrackingCode.where(token: tokens).count == n
		      BackofficeMailer.bsqr_urls(urls).deliver
		    else
  				flash.now[:alert] = "Data base transaction entries isn't correct, try again"
		    end
    	  redirect_to admin_tracking_codes_path
	    end
	  end
end
