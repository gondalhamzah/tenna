ActiveAdmin.register Video do

  permit_params :url, :description, :page_name, :relative_order, :emphasis_indicator, :is_visible
  
  index do
    selectable_column
    column :description
    column :url
    column :emphasis_indicator
    column :relative_order
    column :page_name
    column :is_visible
    actions defaults: false do |v|
      link_to("View", admin_video_path(v)) +
      link_to("Edit", edit_admin_video_path(v)) +
      link_to("Delete", admin_video_path(v), method: :delete, data: { confirm: 'Are you sure?' })
    end
  end

  form do |f|
    semantic_errors

    inputs do
      input :description
      input :url
      input :emphasis_indicator,  as: :select , include_blank: false, collection: ["HOT","NEW","POPULAR","IMPORTANT"]
      input :relative_order
      input :page_name
      input :is_visible, as: :select , include_blank: false, collection: [true,false]
    end
    actions
  end
end
