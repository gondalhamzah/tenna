ActiveAdmin.register Category do
  config.filters = false
  menu priority: 8, if: proc { current_admin_user.super_admin? | current_admin_user.admin? }

  permit_params :name, :category_type , :category_group_id

  index do
    selectable_column
    column :name
    column :category_type
    column :category_group_id

    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :category_type
      row :category_group_id

      row :created_at
      row :updated_at
    end

    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :name
      f.input :category_type, as: :select, collection: Category::CATEGORY_TYPES
      f.input :category_group_id, as: :select, collection: CategoryGroup.all, label: 'CATEGORY GROUP*'
    end

    f.actions
  end

end
