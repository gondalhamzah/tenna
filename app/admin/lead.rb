ActiveAdmin.register Lead do
	permit_params :lead_type, :first_name, :last_name, :email,
	 :phone_num, :industry, :reason_contact, :title, :company_name, :comment

	csv do
	  column ("type"){ |lead| lead.lead_type }
	  column :first_name
	  column :last_name
	  column :email
	  column :phone_num
	  column :industry
	  column :reason_contact
	  column :title
	  column :company_name
	  column :comment
	end

  filter :lead_type , label: "type"

  index do
    selectable_column
    column 'type', :lead_type 
    column :first_name
    column :last_name
    column :email
    column :phone_num
    column :industry
    column :reason_contact
    column :title
    column :company_name
    column :comment
    actions defaults: false do |lead|
      link_to("View", admin_lead_path(lead)) +
      link_to("Edit", edit_admin_lead_path(lead)) +
      link_to("Delete", admin_lead_path(lead), method: :delete, data: { confirm: 'Are you sure?' })
    end
  end
  form do |f|
  	f.inputs do
  		f.input :lead_type ,label: "type"
  		f.input :first_name
  		f.input :last_name
  		f.input :email
  		f.input :phone_num
  		f.input :industry, as: :select, collection: ['Construction','Facility & Property Management','Transportation & Transport','Utilities','Other']
  		f.input :reason_contact, as: :select, collection: ['Contact Me','Schedule Face to Face Meeting','Schedule Demo','Other']
  		f.input :title
  		f.input :company_name
  		f.input :comment
		end
		f.actions
	end

	show do
    attributes_table do
      row('Type'){ |r| r.lead_type }
      row :first_name
      row :last_name
      row :email
      row :phone_num
      row :industry
      row :reason_contact
      row :title
      row :company_name
      row :comment
    end
  end
end