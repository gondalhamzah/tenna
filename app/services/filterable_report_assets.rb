class FilterableReportAssets
  attr_reader :filter_search, :user, :search_options


  def initialize(user:, search_options: {}, params:, zip_code:)
    @user = user
    @search_options = search_options
    @filter_search = ReportAssetsSearch.new(filter_search_params(params))
  end

  def report_project_assets_and_reallocations
    AssetsVersionSearch.report_assets_reallocations_for_project(filter_search, user)
  end

  def asset_location_audit_by_project(current_user, zip_code, both_reports_flag)
    asset_location_audit_by_project_report(current_user, zip_code, self, both_reports_flag)
  end

  def asset_location_audit_by_category(current_user)
    asset_location_audit_by_category_report(current_user, self)
  end

  private

  def filter_search_params(params)
    if params[:report_assets_search].present?
      params.require(:report_assets_search).permit(:assets_report,:sort_order,:date_from,:date_to,:export_type,:project, {:projects => []}, {:asset_categories => []}, :material, :equipment, :asset_category_filter, :project_filter)
    end
  end

  def asset_location_audit_by_project_report(current_user, zip_code, filterable_assets, both_reports_flag)
    project_ids = filterable_assets.filter_search.projects
    categories_names = filterable_assets.filter_search.asset_categories if both_reports_flag

    projects_names = {}

    projects = ProjectsIndex::Project
    projects = projects.filter{ (id == project_ids) } if project_ids && project_ids.count > 0
    projects = projects.filter({
                          term: {
                            company_id: current_user.company.id
                          }
                        })
                        .filter{ !deleted_at }
                        .limit(10000)

    projects.collect{|project| projects_names[project.id.to_s] = project.name}

    date_from = filterable_assets.filter_search.date_from.to_date if filterable_assets.filter_search.date_from.present?
    date_to = filterable_assets.filter_search.date_to.to_date if filterable_assets.filter_search.date_to.present?

    assets = AssetsIndex::Asset
    assets = assets.filter{ (project_id == project_ids) } if project_ids && project_ids.count > 0
    assets = assets.filter{ (category_exact == categories_names) } if categories_names && categories_names.count > 0

    assets = assets.filter({
                      not: {
                        terms: {
                          status: ['deleted', 'expired', 'sold']
                        }
                      }
                    })
                   .filter({
                      term: {
                        company_id: current_user.company.id
                      }
                    })
                   .filter{(created_at >= date_from)}
                   .filter{(created_at <= date_to)}
                   .limit(10000)

    assets_grouped_by_proj = assets.group_by{ |asset| asset.project_id }
    location_match_mismatch = {}

    assets_with_tracking_code = assets.filter { has_tracking_code == true }
    assets_ids = assets_with_tracking_code.collect{ |asset| asset.id }.compact

    assets_grouped_by_proj.each do |project_id, project_assets|
      location_match_mismatch_arr = project_assets.collect{ |asset| asset.same_location}
      location_match = location_match_mismatch_arr.count(true)
      location_mismatch = location_match_mismatch_arr.count(false)
      location_match_mismatch[project_id] = { location_match: location_match, location_mismatch: location_mismatch }
    end

    location_match_total = location_match_mismatch.collect{|project_id, location_match_mismatch_hash| location_match_mismatch_hash[:location_match]}.sum
    location_mismatch_total = location_match_mismatch.collect{|project_id, location_match_mismatch_hash| location_match_mismatch_hash[:location_mismatch]}.sum

    location_versions = AssetsVersionSearch.last_scan_location_and_date(assets_ids).to_a
    loc_ver_group_by_asset_id = location_versions.group_by{|location_version| location_version.asset_id}
    [assets_grouped_by_proj, location_match_mismatch, location_match_total, location_mismatch_total, projects_names, loc_ver_group_by_asset_id]
  end

  def asset_location_audit_by_category_report(current_user, filterable_assets)
    categories_names = filterable_assets.filter_search.asset_categories

    assets = AssetsIndex::Asset
    date_from = filterable_assets.filter_search.date_from.to_date if filterable_assets.filter_search.date_from.present?
    date_to = filterable_assets.filter_search.date_to.to_date if filterable_assets.filter_search.date_to.present?

    assets = assets.filter{ (category_exact == categories_names) } if categories_names && categories_names.count > 0
    assets = assets.filter({
                     not: {
                       terms: { status: ['deleted', 'expired', 'sold'] }
                     }
                   })
                   .filter({
                     term: {
                       company_id: current_user.company.id
                     }
                   })
                   .filter{(created_at >= date_from)}
                   .filter{(created_at <= date_to)}
                   .limit(10000)

    assets_grouped_by_category = assets.group_by { |asset| asset.category_exact }

    assets_with_tracking_code = assets.filter { has_tracking_code == true }
    assets_ids = assets_with_tracking_code.collect{ |asset| asset.id }.compact

    location_match_mismatch_arr = assets.collect{ |asset| asset.same_location }
    location_match_total = location_match_mismatch_arr.count(true)
    location_mismatch_total = location_match_mismatch_arr.count(false)

    location_versions = AssetsVersionSearch.last_scan_location_and_date(assets_ids).to_a
    loc_ver_group_by_asset_id = location_versions.group_by{ |location_version| location_version.asset_id }

    [assets_grouped_by_category, location_match_total, location_mismatch_total, loc_ver_group_by_asset_id]
  end
end
