require 'jwt'

class MqttJwtGenerator

  def initialize(company_id, user_id = nil)
    @secret = ENV['MQTT_JWT_HMAC_SECRET']

    @payload = {
      iat: (Time.now - 1.hours).to_i,
      exp: (Time.now + 3.hours).to_i,
      topic: "/#{company_id}/notifications"
    }

    @type = 'HS256'
  end

  def perform
    return generate
  end

  private
  attr_accessor :payload, :secret, :type

  def generate
    token = JWT.encode payload, secret, type, { typ: 'JWT', alg: type }
    token
  end
end
