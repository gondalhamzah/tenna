class FilterableWantedAssets
  attr_reader :filter_search, :user, :search_options

  # Service used in many controllers to perform asset filtering

  def initialize(user:, search_options: {}, params:, zip_code:)
    @user = user
    @search_options = search_options
    @filter_search = FilterSearch.new(filter_search_params(params))
    @filter_search.distance_zip ||= zip_code
    @filter_search.sort = :date_posted if !filter_search.sort || (filter_search.sort.to_sym == :distance && filter_search.coordinate.blank?)
  end

  def filtered_assets_scope
    WantedAssetSearch.search(filter_search, user, search_options)
    .load(project: {scope: -> { includes( :company ) }})
  end

  def assets
    filtered_assets_scope
      .only(:id)
      .load(wanted_asset: {scope: -> { includes(:category, :company, :zip_code) }})
  end

  private

  def filter_search_params(params)
    if params[:filter_search].present?
      params.require(:filter_search).permit(:category, :sort, :project, :my_wanted, :fulfilled, :query,:high,:medium,:low)
    end
  end

end

