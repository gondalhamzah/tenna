class FilterableAssets
  attr_reader :filter_search, :user, :search_options

  # Service used in many controllers to perform asset filtering
  def initialize(user:, search_options: {}, params:, zip_code:,live_data: false)
    @user = user
    @search_options = search_options
    if params[:filter_search].present?
      params[:filter_search][:is_live_data] = live_data
    end
    @filter_search = FilterSearch.new(filter_search_params(params))
    @filter_search.distance_zip ||= zip_code
    @filter_search.sort = :name if !filter_search.sort || (filter_search.sort.to_sym == :distance && filter_search.coordinate.blank?)
  end

  def filtered_assets_scope
    AssetSearch.search(filter_search, user, search_options)
      .load(project: {scope: -> { includes( :company ) }})
  end

  def filtered_routing_assets_scope
    AssetSearch.routing_search(filter_search, user, search_options)
      .load(project: {scope: -> { includes( :company ) }})
  end

  def api_filtered_assets_scope
    AssetSearch.api_search(filter_search, user, search_options)
      .load(project: {scope: -> { includes( :company ) }})
  end

  def filtered_public_assets_scope
    AssetSearch.search_public_assets(filter_search, user, search_options)
    .load(project: {scope: -> { includes( :company ) }})
  end

  def assets
    filtered_assets_scope
      .only(:id)
      .load(asset: {scope: -> { includes(:category, :company, :marks, :photos, :zip_code) }})
  end

  def routing_assets
    filtered_routing_assets_scope
      .only(:id)
      .load(asset: {scope: -> { includes(:category, :company, :marks, :photos, :zip_code) }})
  end

  def api_assets
    api_filtered_assets_scope
      .only(:id)
      .load(asset: {scope: -> { includes(:category, :company, :marks, :photos, :zip_code) }})
  end

  def api_search_assets_by_category
    AssetSearch.api_search(filter_search, user, search_options)
      .only(:id)
      .load(asset: {scope: -> { includes(:category, :company, :marks, :photos, :zip_code) }})
  end

  def report_assets
    AssetSearch.report_assets_search(filter_search, user)
  end

  def public_assets
    filtered_public_assets_scope
    .only(:id)
    .load(asset: {scope: -> { includes(:category, :company, :marks, :photos, :zip_code) }})
  end

  private

  def filter_search_params(params)
    if params[:filter_search].present?
      params.require(:filter_search).permit(:zipcode,:assets_report,:sort_order,:date_from,:date_to,:export_type,:favorites,:expiring_mine,:expiring_others,
        :buy_mine,:buy_others,:rental_others,:rental_mine,:my_assets,:abandoned,:marketplace,:for_sale,:for_rent,
        :equipment,:material,:location_changed_in,:is_live_data,:buy,:sold,:in_use,:available,:req_maintenance,:has_qr_code,:has_trackers,:rental,:has_media,:category,:price_min,
        :price_max,:hours_min,:hours_max,:miles_min,:miles_max,:year_min,:year_max,:condition,:available_on,:query,:sort,:distance_miles,
        :distance_zip,:certified,:expiring,:coordinate,:status,:expires_on,:public,:is_mine,:is_others,:category_id, :category_group_id,
        :without_tracking_code,:page_size,:assets_title,:hide_filters,:project,:group,:country,geohash:[],current_driver:[])
    end
  end
end
