class ClustersAggregator::Asset < ClustersAggregator::Base
  index_name AssetsIndex::Asset.index_name
  document_type AssetsIndex::Asset.type_name

  def aggregations
    {

      my_company: {
        filter: {
          term: company_or_creator_term
        },
        aggs: {
          public: {
            filter: {
              terms: {
                asset_kind: ['for_rent', 'for_sale']
              }
            },
            aggs: {
              first_four: {
                top_hits: {
                  sort: [
                    {
                      title: {
                        order: 'asc'
                      }
                    }
                  ],
                  _source: {
                    include: [
                      'id'
                    ]
                  },
                  size: 4
                }
              }
            }
          },
          owned: {
            filter: {
              term: {
                asset_kind: should_public ? 'for_sale' : 'owned'
              }
            },
            aggs: {
              first_four: {
                top_hits: {
                  sort: [
                    {
                      title: {
                        order: 'asc'
                      }
                    }
                  ],
                  _source: {
                    include: [
                      'id'
                    ]
                  },
                  size: 4
                }
              }
            }
          },
          rental: {
            filter: {
              term: {
                asset_kind: should_public ? 'for_rent' : 'rental'
              }
            },
            aggs: {
              first_four: {
                top_hits: {
                  sort: [
                    {
                      title: {
                        order: 'asc'
                      }
                    }
                  ],
                  _source: {
                    include: [
                      'id'
                    ]
                  },
                  size: 4
                }
              }
            }
          },
        }
      },
      public: {
        filter: {
          and: [
            {
              range: {
                expires_on: {
                  gte: "now/d"
                }
              }
            },
            {
              term: {
                public: true
              }
            }
          ]
        },
        aggs: {
          not_my: {
            filter: {
              not: {
                term: company_or_creator_term
              }
            },
            aggs: {
              first_four: {
                top_hits: {
                  sort: [
                    {
                      title: {
                        order: 'asc'
                      }
                    }
                  ],
                  _source: {
                    include: [
                      'id'
                    ]
                  },
                  size: 4
                }
              }
            }
          }
        }
      }
    }
  end

  def filters
    [
      {
        term: {
          status: 'available'
        }
      },
      {
        range: {
          expires_on: {
            gte: 'now'
          }
        }
      }
    ]
  end
end
