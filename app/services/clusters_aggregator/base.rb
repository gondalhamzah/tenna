class ClustersAggregator::Base
  attr_reader :company_id, :creator_id, :bounds, :zoom

  def initialize(company_id:, creator_id:, bounds:, zoom:)
    @company_id = company_id
    @creator_id = creator_id
    @bounds     = bounds
    @zoom       = zoom
  end

  def results
    @results ||= client.search(search_params)
  end

  def company_or_creator_term
    company_id.present? ? company_term : creator_term
  end

  def should_public
    !company_id.present?
  end

  def company_term
    {
      company_id: company_id
    }
  end

  def creator_term
    {
      creator_id: creator_id
    }
  end

  private

  def self.index_name(index_name)
    @index_name = index_name
  end

  def self.document_type(document_type)
    @document_type = document_type
  end

  def index_name
    self.class.instance_variable_get(:@index_name)
  end

  def document_type
    self.class.instance_variable_get(:@document_type)
  end

  def search_params
    {
      search_type: 'count',
      index: index_name,
      type:  document_type,
      query_cache: true,
      body: query_params
    }
  end

  def query_params
    @query = query_structure

    if respond_to?(:filters)
      filters_context = @query[:query][:filtered][:filter][:bool][:must]
      filters_context << filters
      filters_context.flatten!
    end

    if respond_to?(:aggregations)
      @query[:aggs][:grid][:aggs] = aggregations
    end

    @query
  end

  def query_structure
    {
      query: {
        filtered: {
          filter: {
            bool: {
              must: [
                {
                  geo_bounding_box: {
                    coordinates: {
                      top_left: {
                        lat: bounds['top_left']['lat'],
                        lon: bounds['top_left']['lng']
                      },
                      bottom_right: {
                        lat: bounds['bottom_right']['lat'],
                        lon: bounds['bottom_right']['lng']
                      }
                    }
                  }
                }
              ]
            }
          }
        }
      },
      aggs: {
        grid: {
          geohash_grid: {
            field: 'coordinates',
            precision: zoom,
          }
        }
      }
    }
  end

  def client
    @client ||= Chewy.client
  end
end
