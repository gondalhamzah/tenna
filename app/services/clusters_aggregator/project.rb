class ClustersAggregator::Project < ClustersAggregator::Base
  index_name ProjectsIndex::Project.index_name
  document_type ProjectsIndex::Project.type_name

  def aggregations
    {
      first_four: {
        top_hits: {
          sort: [
            {
              name: {
                order: 'asc'
              }
            }
          ],
          _source: {
            include: [
              'id'
            ]
          },
          size: 4
        }
      }
    }
  end

  def filters
    [
      {
        term: company_or_creator_term
      },
      {
        range: {
          closeout_date: {
            gte: 'now'
          }
        }
      },
      {missing: {field: "deleted_at", "existence": true, "null_value": false}}
    ]
  end
end
