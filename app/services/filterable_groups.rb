class FilterableGroups
  attr_reader :group_filter_search, :user, :search_options
  attr_writer :sort_by

  # Service used perform project filtering

  def initialize(user:, search_options: {}, params:)
    @user = user
    @search_options = search_options
    @group_filter_search = GroupFilterSearch.new(filter_search_params(params))
    @group_filter_search.sort = :name if !group_filter_search.sort || (group_filter_search.sort.to_sym == :distance)
  end

  def filtered_groups_scope
    GroupSearch.search(group_filter_search, user, search_options)
    .load(project: {scope: -> { includes( :company ) }})
  end

  def groups
    filtered_groups_scope
      .order(sort_by)
      .only(:id)
      .load(group: {scope: -> { includes( :company ) }})
  end

  private

  def sort_by
    @sort_by || GroupSearch.sorts()[(group_filter_search.sort || :date_posted).to_sym]
  end

  def filter_search_params(params)
    if params[:group_filter_search].present?
      params.require(:group_filter_search).permit(:sort, :query, :page_size, :is_active, :is_closed, :default_project, :project)
    end
  end
end
