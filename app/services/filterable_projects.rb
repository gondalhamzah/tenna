class FilterableProjects
  attr_reader :filter_search, :user, :search_options
  attr_writer :sort_by

  # Service used perform project filtering

  def initialize(user:, search_options: {}, params:, zip_code:)
    @user = user
    @search_options = search_options
    @filter_search = FilterSearch.new(filter_search_params(params))
    @filter_search.distance_zip ||= zip_code
    @filter_search.sort = :name if !filter_search.sort || (filter_search.sort.to_sym == :distance && filter_search.coordinate.blank?)
  end

  def filtered_projects_scope
    ProjectSearch.search(filter_search, user, search_options)
  end

  def api_filtered_projects_scope
    ProjectSearch.api_search(filter_search, user, search_options)
  end

  def projects
    filtered_projects_scope
      .order(sort_by)
      .only(:id)
      .load(project: {scope: -> { includes( :company ) }})
  end

  def api_projects
    api_filtered_projects_scope
      .only(:id)
      .load(project: {scope: -> { includes( :company ) }})
  end

  private

  def sort_by
    @sort_by || ProjectSearch.sorts(@filter_search.coordinate||[])[(filter_search.sort || :date_posted).to_sym]
  end

  def filter_search_params(params)
    if params[:filter_search].present?
      params.require(:filter_search).permit(:zipcode,:country, :is_mine, :is_active, :is_closed, :contact_id, :distance_miles, :distance_zip, :sort, :query, :page_size, geohash: [], coordinate: [])
    end
  end
end
