require 'csv'
require 'zip'
require 'tempfile'

class ProjectExporter
  # These dont need to appear in the exported csv
  ProjectExclusions = %w(company_id contact_id updated_at id project_num)
  AddressExclusions = %w(id addressable_id addressable_type created_at updated_at lat lng)

  # Based on ProjectCsvImportsController#template
  AssetHeaderTemplate = [
    'Owned',
    'Rental',
    'For Sale',
    'For Rent',
    'Type of Asset',
    'Durability',
    '*Category',
    '*Title',
    'Description',
    'Make',
    'Model',
    'Year',
    'Hours',
    'Miles',
    'Condition',
    '*Market value',
    '*Price/Cost',
    'Quantity',
    '*Time/Units',
    'Asset number',
    '*In use until',
    'Notes',
    '*Zip code',
    'Current lat',
    'Current lng'
  ]

  def self.export(user,export_project_only = false,export_assets_only = false , filtered_assets_result = nil, browser_timezone = nil, filtered_assets = nil, exported_at = nil, is_filtered = false)
    self.new.export(user,export_project_only,export_assets_only, filtered_assets_result, browser_timezone, filtered_assets, exported_at, is_filtered)
  end

  def export(user,export_project_only = false,export_assets_only = false ,filtered_assets_result = nil, browser_timezone = nil, filtered_assets = nil, exported_at = nil, is_filtered = false)
    if !export_assets_only
      projects_csv = export_projects(user)
    end
    if !export_project_only
      assets_csv = export_assets(user , export_assets_only ,filtered_assets_result, browser_timezone, filtered_assets, exported_at, is_filtered)
    end
    zip_of(user, projects_csv, assets_csv,export_project_only, export_assets_only)
  end

  private

    def zip_of(user, projects_csv, assets_csv ,export_project_only = false,export_assets_only = false)
      datestamp = DateTime.now.strftime("%m-%d-%Y")
      assets_file_name = "Assets-list-#{datestamp}.csv"
      if export_assets_only
        assets_file_name = "Assets-filtered-list-#{datestamp}.csv"
      end
      path = zipfile_path(user, datestamp)

      # Zip::File.open will reopen the file if it exists, and add to it
      # Removing to ensure we always get a fresh file
      FileUtils.rm(path, force: true)

      Zip::File.open(path, Zip::File::CREATE) do |zipfile|
        if !export_assets_only
          zipfile.add "Projects #{datestamp}.csv", projects_csv.path
        end
        if !export_project_only
          zipfile.add assets_file_name, assets_csv.path
        end
      end

      path
    end

    def zipfile_path(user, datestamp)
      "/tmp/#{user.company.name}_projects-#{datestamp}.zip"
    end

    ## Projects #########################################################################

    def export_projects(user)
      csv_data = export_projects_data(user)
      temp_file = Tempfile.open(['projects', '.csv'])
      temp_file.write(csv_data)
      temp_file.close
      temp_file
    end

    def export_projects_data(user)
      CSV.generate do |csv|
        csv << projects_header + address_header
        user.company.projects.each do |project|
          unless project.deleted_at
            row = []
            row << project_values(project)
            row << address_values(project)
            csv << row.flatten
          end
        end
      end
    end

    def projects_header
      cols = Project.columns.map(&:name).reject do |col|
        col.in?(ProjectExclusions)
      end
      cols = full_capitalize(cols)
      # cols = replace_in cols, 'Id', 'Project ID' # Id => Project ID. Must happen after capitalize
      cols.unshift('ID/Number')
      cols
    end

    def address_header
      cols = Address.columns.map(&:name).reject do |col|
        col.in?(AddressExclusions)
      end
      cols = replace_in cols, 'line_1', 'Address-line_1' # line_1 => Address-line_1
      cols = replace_in cols, 'line_2', 'Address-line_2' # line_2 => Address-line_2
      cols = full_capitalize(cols)
      cols = replace_in cols, 'Zip', 'Zip code'
      cols
    end

    def project_values(project)
      values = project.attributes
        .reject{ |key, _|  key.in?(ProjectExclusions) }
        .values

      # replace project ID with project_num
      values.unshift(project.project_num)

      values
    end

    def address_values(project)
      project.address.attributes
        .reject{ |key, _|  key.in?(AddressExclusions) }
        .values
    end

    ################################################################################

    def export_assets(user,export_assets_only = false,filtered_assets_result = nil, browser_timezone = nil, filtered_assets = nil, exported_at = nil, is_filtered = false)
      csv_data = export_assets_data(user,export_assets_only , filtered_assets_result, browser_timezone, filtered_assets, exported_at, is_filtered)
      temp_file = Tempfile.open(['assets', '.csv'])
      temp_file.write(csv_data)
      temp_file.close
      temp_file
    end

    def export_assets_data(user,export_assets_only = false,filtered_assets_result = nil, browser_timezone = nil, filtered_assets = nil, exported_at = nil, is_filtered = false)
      i = 0
      CSV.generate do |csv|
        if i == 0
          is_filtered ? csv << ["Asset - Filters = #{filtered_assets}"] : csv << ["Report:   #{filtered_assets} Assets"]
          csv << [replace_date_time(exported_at)]
        end
        csv << assets_header
        assets = Array.new
        if export_assets_only
          assets = filtered_assets_result
        else
          assets = user.company.assets
        end

        assets.each do |asset|
          csv << asset_values(asset, browser_timezone) unless asset.deleted? || (asset.rental == true and asset.public == true)
        end
        i = i + 1
      end
    end

    def assets_header
      AssetHeaderTemplate + ['Last Scan Date','Last Scan Time','Project Name']
    end

    def asset_values(asset, browser_timezone = nil)
      # get attribute names from header, downcased, whitespace replaced and non alphabet removed
      # Similar to AssetCsvImportJob#perform:23

      attrs = AssetHeaderTemplate.map do |attr|
        attr.downcase.gsub(/\s+/, '_').gsub(/[^a-z_]/, '')
      end

      # These are aliases of the original attributes
      attrs = replace_in attrs, 'type_of_asset','type'
      attrs = replace_in attrs, 'asset_number','asset_num'
      attrs = replace_in attrs, 'notes','notes'
      attrs = replace_in attrs, 'for_sale','for_sale?'
      attrs = replace_in attrs, 'for_rent', 'for_rent?'
      attrs = replace_in attrs, 'pricecost', 'price'
      attrs = replace_in attrs, 'timeunits', 'units'
      h = {}
      attrs.each do |an|
        h[an] = asset.public_send(an)
      end

      # Category and zip_code appear as objects. Conver them to values
      h['category'] = asset.category.try(:name)
      h['zip_code'] = asset.zip_code.try(:zip_code)

      # get the last scan date from elasticsearch
      location_version = AssetsVersionSearch.asset_inventory(asset.id).to_a
      h['last_scan_date'] = (location_version.count > 0) ? DateTime.parse(location_version[0].created_at.to_s).in_time_zone(browser_timezone).strftime('%m/%d/%Y') : 'n/a'
      h['last_scan_time'] = (location_version.count > 0) ? DateTime.parse(location_version[0].created_at.to_s).in_time_zone(browser_timezone).strftime('%I:%M %p %Z') : 'n/a'

      # Add the project-name finally
      h['project_name'] = asset.project.try(:name)

      h.values
    end

    ################################################################################

    def replace_in(arr, old, neww)
      index = arr.index(old)
      return arr unless index

      arr1 = arr.dup
      arr1[index] = neww
      arr1
    end

    def full_capitalize(list)
      list.map do |str|
        str.split(' ').map(&:capitalize).join(' ')
      end
    end

    def replace_date_time(exported_at)
      exported_at.sub(" ", " at ") if exported_at
    end
end
