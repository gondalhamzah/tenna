class Category < ActiveRecord::Base
  CATEGORY_TYPES = %w(equipment material)

  has_many :assets, dependent: :restrict_with_error
  has_many :wanted_assets, dependent: :restrict_with_error

  validates :name, :category_group_id,presence: true
  validates :category_type, presence: true, inclusion: {in: CATEGORY_TYPES}

  belongs_to :category_group

  # Update Elasticsearch index on asset save
  update_index('assets#asset') { assets }

  scope :with_type, -> (type) { where(category_type: type).order(:name) }

  def self.by_type
    order(:name).group_by(&:category_type).transform_keys { |k| k.titleize }
  end

  def self.by_name
    order(:name).pluck(:name)
  end

end
