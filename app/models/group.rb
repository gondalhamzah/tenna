class Group < ActiveRecord::Base
  HISTORY_FIELDS = %w{ name notes group_num client description state project_id}.freeze
  HISTORY_STRING_NIL_CHECK_FIELDS = %w{notes group_num client description}.freeze

  belongs_to :project
  belongs_to :company
  validates :name, :company, :project, presence: true
  has_many :assets, dependent: :nullify
  has_many :csv_imports, dependent: :destroy

  # Update Elasticsearch index on group save
  update_index('groups#group') { self }

  # This callback is used to create version for assets
  after_save :create_version, :change_assets_project
  scope :available, ->{ where(deleted_at: nil).where.not(state: 'closed').order(:name) }

  def project_name
    if company.present? && project.present? && project.company == company
      project.name
    end
  end

  # instead of deleting, indicate the user requested a delete & timestamp it
  def soft_delete
    update_attribute(:deleted_at, Time.current)
  end

  def total_assets_count
    assets.not_sold_deleted.count
  end

  def total_assets_value
    assets.not_sold_deleted.sum(:price)
  end

  def change_assets_project
    # if update and project_id is changed
    if !self.id_changed? && self.changes.key?('project_id')
      Asset.transaction do
        self.assets.not_sold_deleted.each do |asset|
          asset.update(
            project_id: self.project_id,
            zip_code_id: ZipCode.find_or_create_by(zip_code: self.project.address.zip).id
          )
        end
      end
    end
  end

  def create_version
    # Find current logged in user id
    current_user_id = User.current.try(:id)
    action = self.id_changed? ? 'create' : 'update'

    GroupsVersion.transaction do
      if action == 'create'
        GroupsVersion.create!(group_id: self.id, field_name: 'newly_created', from: '', to: '',
            user_id: current_user_id, company_id: self.company_id, action: action)
      end
      self.changes.each do |field_name, field_value|
        if Group::HISTORY_FIELDS.include? field_name
          if not ((Group::HISTORY_STRING_NIL_CHECK_FIELDS.include? field_name) && field_value[0].blank? && field_value[1].blank?)
            GroupsVersion.create!(group_id: self.id, field_name: field_name, from: field_value[0], to: field_value[1],
              user_id: current_user_id, company_id: self.company_id, action: action)
          end
        end
      end
    end
  end
end
