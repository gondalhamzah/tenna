class ProjectAssignment < ActiveRecord::Base
  scope :by_project_name, -> {includes(:project).order("projects.name asc")}
  belongs_to :project
  belongs_to :user
  enum role: %w(project_admin project_user)
  PROJECT_ADMIN = 'project_admin'
  PROJECT_USER = 'project_user'

  # holds "display" based transformations for the following roles
  @@_role_lookup = {PROJECT_ADMIN => 'Leader', PROJECT_USER => 'Member'}

  validates :project, :user, :role, presence: true
  validates :user_id, uniqueness: { scope: :project, message: 'is already assigned to project' }
  validate :project_and_user_must_be_in_same_company
  validates :user, inclusion: { in: ->(record) { User.company_user }, message: "user must have a role of 'Company User'"}

  def is_project_contact?
    self.user == self.project.contact
  end

  def self.role_lookup
    @@_role_lookup
  end

  def project_and_user_must_be_in_same_company
    if project.present? && user.present? && project.company != user.company
      errors.add(:user_id, 'is not in same company as project')
    end
  end

  # Change the creator of an asset if that creator is removed from the project.
  before_destroy do |project_assignment|
    ## Assets
    project_assignment.user.assets.each do |asset|
      if asset.project == project_assignment.project
        if asset.project.contact != project_assignment.user
          asset.creator = asset.project.contact
        else
          asset.creator = asset.company.contact
        end
        asset.save
      end
    end
  end

  #Change project contact is the user is removed
  before_destroy do |project_assignment|
    if project_assignment.is_project_contact?
      project.update(contact: project.company.contact)
    end
  end
end
