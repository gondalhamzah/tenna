class Lead < ActiveRecord::Base
	after_save :customer_mail, :sales_rep_emal

	def customer_mail
		if (lead_type.downcase == 'learn more' or lead_type.downcase == 'request a demo' or lead_type.downcase == 'contact us')
			LeadsMailer.customer_confirmation(self).deliver_now
		end
	end

	def sales_rep_emal
		if (lead_type.downcase == 'learn more' or lead_type.downcase == 'request a demo' or lead_type.downcase == 'contact us')
			LeadsMailer.sales_rep_notification(self).deliver_now
		end
	end
end