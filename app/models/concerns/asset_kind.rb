module AssetKind
  extend ActiveSupport::Concern

  included do
    validate :asset_kind_is_valid
  end

  ASSET_KINDS = %w[owned rental for_sale for_rent]

  def asset_kind_is_valid
    errors.add(:asset_kind, 'must be one of (owned, rental, for_rent, for_sale)') unless %w{owned rental for_rent for_sale}.include?(self.asset_kind)
    if creator.present? && creator.public?
      errors.add(:asset_kind, 'must be one of (for_rent, for_sale)') unless %w{for_rent for_sale}.include?(self.asset_kind)
    end
  end

  def public_private
    if %w{for_rent for_sale}.include?(asset_kind)
      'public'
    else
      'private'
    end
  end

  def public_private=(val)
  end

  def owned_rental
    if asset_kind != 'rental'
      'owned'
    else
      'rental'
    end
  end

  def owned_rental=(val)
  end

  def for_sale_rental
    if asset_kind == 'for_sale'
      'for_sale'
    elsif asset_kind == 'for_rent'
      'for_rent'
    end
  end

  def for_sale_rental=(val)
  end

  def asset_kind=(val)
    if val == 'rental'
      self.owned = false
      self.public = false
      self.rental = true
    elsif val == 'for_rent'
      self.owned = true
      self.public = true
      self.rental = true
    elsif val == 'for_sale'
      self.owned = true
      self.public = true
      self.rental = false
    elsif val == 'owned'
      self.owned = true
      self.public = false
      self.rental = false
    end
  end

  def asset_kind
    if !owned && !public && rental
      'rental'
    elsif owned && public && rental
      'for_rent'
    elsif owned && public && !rental
      'for_sale'
    elsif owned && !public && !rental
      'owned'
    else
      'invalid'
    end
  end

  def is_rental?
    !self.owned && !self.public && self.rental
  end

  def for_rent?
    self.owned && self.public && self.rental
  end

  def for_sale?
    self.owned && self.public && !self.rental
  end

  def is_owned?
    self.owned && !self.public && !self.rental
  end

  def validate_public_rental_owned_values
    unless is_owned? || is_rental? || for_rent? || for_sale?
      self.errors.add(:combinations,'are invalid for these fields "owned, for_rent, for_sale, rental".')
    end
  end

end
