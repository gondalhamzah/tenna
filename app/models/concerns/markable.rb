module Markable
  extend ActiveSupport::Concern

  included do
    has_many :marks, as: :markable, dependent: :delete_all
  end

  class_methods do
    def marked_as(mark, marker)
      joins(:marks).where(marks: { mark: mark, marker: marker})
    end

    def favorites_for(marker)
      marked_as(:favorite, marker)
    end
  end

  def marked_as(mark, marker_id, marker_type)
    marks.detect do |m|
      m.mark == mark.to_s &&
        m.marker_id == marker_id &&
        m.marker_type == marker_type
    end
  end

  def favorite_for(marker)
    marked_as(:favorite, marker.id, marker.class.name)
  end

  def num_favorites
    marks.where(mark: 'favorite').size
  end
end
