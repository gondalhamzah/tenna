module Tokenable
  extend ActiveSupport::Concern

  included do
  end

  def has_tracking_code?
    self.has_qr? ||
    self.has_rfid? ||
    self.has_lora? ||
    self.has_sigfox? ||
    self.has_upc? ||
    self.has_bt? ||
    self.has_cellular? ||
    self.has_serialnumber?
  end

  def get_latest_created_at
    row = self.tracking_codes.order('created_at DESC').first
    row ? row.created_at : nil
  end

  def has_tags?
    has_qr? || has_rfid? || has_upc?
  end

  def has_trackers?
    has_cellular? || has_lora? || has_bt? || has_sigfox?
  end

  def has_qr?
    !self.qr.nil? && self.qr > 0
  end

  def has_rfid?
    !self.rfid.nil? && self.rfid > 0
  end

  def has_lora?
    !self.lora.nil? && self.lora > 0
  end

  def has_sigfox?
    !self.sigfox.nil? && self.sigfox > 0
  end

  def has_upc?
    !self.upc.nil? && self.upc > 0
  end

  def has_bt?
    !self.bt.nil? && self.bt > 0
  end

  def has_cellular?
    !self.cellular.nil? && self.cellular > 0
  end

  def tracking_code_name
    if has_tracking_code?
      if has_cellular?
        'Cellular'
      elsif has_lora?
        'Lora'
      elsif has_sigfox?
        'Sigfox'
      elsif has_bt?
        'Bluetooth'
      elsif has_rfid?
        'RFID'
      elsif has_qr?
        'QR'
      elsif has_upc?
        'UPC'
      end
    end
  end

  def has_serialnumber?
    !self.serialnumber.nil? && self.serialnumber > 0
  end

  def qr_count?
    self.tracking_codes.where(token_type: 0).count
  end

end
