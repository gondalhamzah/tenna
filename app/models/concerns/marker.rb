module Marker
  extend ActiveSupport::Concern

  included do
    has_many :marks, as: :marker, dependent: :delete_all
  end

  def marked_as(mark, type)
    marks.where(mark: mark, markable_type: type)
  end

  def favorite_asset_marks
    marked_as(:favorite, Asset)
  end
end
