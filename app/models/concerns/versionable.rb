module Versionable
  extend ActiveSupport::Concern

  included do
    HISTORY_FIELDS = %w{ category_id hours miles current_lat current_lng accuracy price notes project_id status asset_num group_id market_value maintenance quantity}.freeze
    HISTORY_STRING_NIL_CHECK_FIELDS = %w{notes asset_num}.freeze

    # This callback is used to create version for assets
    after_save :create_version
    before_save :check_last_location_changed
    # start_date, end_date and filter_name for analytics dashboard, we don't save these fields
    attr_accessor :start_date
    attr_accessor :end_date
    attr_accessor :filter_name
    
    after_initialize do |obj|
      obj.start_date = Time.zone.today - 3.months
      obj.end_date = Time.zone.today
    end

    def self.get_label_from_action_public_value(rental, owned, public)
      if !owned && !public && rental
        'rental'
      elsif owned && !public && rental
        'for_rent'
      elsif owned && public && !rental
        'for_sale'
      elsif owned && !public && !rental
        'owned'
      end
    end
  end

  def check_last_location_changed
    if self.current_lat_changed?
      self.last_location_changed = Time.now
    end
  end

  def create_version
    # Find current logged in user id
    current_user_id = User.current.try(:id)
    if current_user_id.nil? && self.company.nil?
      current_user_id = self.creator.try(:id)
    elsif current_user_id.nil? && !self.company.nil?
      current_user_id = self.company.contact.try(:id)
    end

    action = self.new_record? ? 'create' : 'update'

    if self.changes.keys.include? 'group_id'
      field_value = self.changes[:group_id]
      GroupsVersion.transaction do
        # group unlinked
        if field_value[0]
          GroupsVersion.create!(group_id: field_value[0], field_name: 'asset_id', from: self.id, to: nil,
            user_id: current_user_id, company_id: self.company_id, action: 'delete')
        end

        # group linked
        if field_value[1]
          GroupsVersion.create!(group_id: field_value[1], field_name: 'asset_id', from: nil, to: self.id,
            user_id: current_user_id, company_id: self.company_id, action: 'new')
        end
      end
    end

    AssetsVersion.transaction do
      if action == 'create'
        AssetsVersion.create!(asset_id: self.id, field_name: 'newly_created', from: '', to: '',
          whodunnit: current_user_id, project_id: self.try(:project).try(:id),
          group_id: self.try(:group).try(:id), company_id: self.company_id, action: action)
      end

      self.changes.each do |field_name, field_value|
        if Asset::HISTORY_FIELDS.include? field_name
          if not ((Asset::HISTORY_STRING_NIL_CHECK_FIELDS.include? field_name) && field_value[0].blank? && field_value[1].blank?)
            if field_name == 'maintenance'
              main_str = field_value[1] ? 'requested' : 'completed'
              AssetsVersion.create!(asset_id: self.id, field_name: "#{field_name} #{main_str}", from: '', to: '',
                whodunnit: current_user_id, project_id: self.try(:project).try(:id), group_id: self.try(:group).try(:id),
                company_id: self.company_id, action: action)
            else
              AssetsVersion.create!(asset_id: self.id, field_name: field_name, from: field_value[0], to: field_value[1],
                whodunnit: current_user_id, project_id: self.try(:project).try(:id), group_id: self.try(:group).try(:id),
                company_id: self.company_id, action: action)
            end
          end
        end
      end

      if self.rental_changed? || self.public_changed? || self.owned_changed?
        # get previous value
        from = Asset.get_label_from_action_public_value(self.rental_was, self.owned_was, self.public_was)
        to = Asset.get_label_from_action_public_value(self.rental, self.owned, self.public)
        AssetsVersion.create!(asset_id: self.id, field_name: 'action_public', from: from, to: to,
          whodunnit: current_user_id, project_id: self.try(:project).try(:id), group_id: self.try(:group).try(:id),
          company_id: self.company_id, action: action)
      end
    end
  end
end
