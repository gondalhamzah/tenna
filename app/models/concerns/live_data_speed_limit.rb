module LiveDataSpeedLimit extend ActiveSupport::Concern
  included do

    def check_speed_limit company_id, asset_id, location_payload, speed_payload, cond_ids
      avs = AssetsVersionsIndex::AssetsVersion
      avs = avs.filter({
        bool: {
          must: [
            { term: { asset_id: asset_id } },
            { range: { created_at: { gte: 'now-10m' } } },
            { nested: {
              path: 'tracking_info.array',
              query: {
                bool: {
                  must: [
                    { match: { 'tracking_info.array.type': 'Speed'} },
                    { range: { 'tracking_info.array.value_numeric': { gt: 0 } } },
                  ]
                }
              } }
            }
          ]
        }
      })
      avs = avs.limit(3)
      avs = avs.only(:id).load()

      points = ["#{location_payload['value_object']['latitude']},#{location_payload['value_object']['longitude']}"]
      avs.to_a.each do |av|
        location = av.tracking_info['array'].detect {|a| a['type'] == 'Location'}
        points << "#{location['value_object']['latitude']},#{location['value_object']['longitude']}"
      end

      if points.length > 2
        GoogleSpeedLimitJob.perform_now company_id, points.join('|'), asset_id, speed_payload, cond_ids
      end
    end
  end
end
