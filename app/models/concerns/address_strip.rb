module AddressStrip
  extend ActiveSupport::Concern

  included do
    before_validation do |obj|
      obj.line_1 = obj.line_1.strip unless obj.line_1.nil?
      obj.line_2 = obj.line_2.strip unless obj.line_2.nil?
      obj.city = obj.city.strip unless obj.city.nil?
      obj.state = obj.state.strip unless obj.state.nil?
      obj.zip = obj.zip.strip unless obj.zip.nil?
    end
  end
end
