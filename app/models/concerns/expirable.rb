module Expirable
  AVAILABLE_DURATION = 120.days
  EXPIRE_THRESHOLD = 120.days

  extend ActiveSupport::Concern

  included do
    attr_accessor :renew_expires_on
    scope :expired, -> { where(arel_table[:expires_on].lt(Time.zone.today)) }
    scope :not_expired, -> { where(arel_table[:expires_on].gteq(Time.zone.today)) }

    after_initialize do |obj|
      obj.renew_expires_on = false
      obj.expires_on ||= self.get_expire_duration
    end

    before_save do |obj|
      if obj.renew_expires_on == "1"
        obj.expires_on = self.class.total_expire_duration
      end
    end
  end

  class_methods do
    def expiring_in(days_from_today)
      if !days_from_today.is_a?(ActiveSupport::Duration)
        raise ArgumentError, 'Argument is not ActiveSupport::Duration'
      end
      where(expires_on: Time.zone.today + days_from_today)
    end

    def expiring_within(days_from_today)
      if !days_from_today.is_a?(ActiveSupport::Duration)
        raise ArgumentError, 'Argument is not ActiveSupport::Duration'
      end
      active.where(arel_table[:expires_on].lteq(Time.zone.today + days_from_today))
    end

    def available_duration
      AVAILABLE_DURATION
    end

    def expire_threshold
      EXPIRE_THRESHOLD
    end

    def total_expire_duration
      Time.zone.today + available_duration + expire_threshold
    end

  end

  def expired?
    expires_on.past?
  end

  def not_expired?
    !expired?
  end

  def get_expire_duration
    company.present? ? Date.new(2050, 1, 1) : self.class.total_expire_duration
  end

  def expiring_within?(days_from_today)
    if !days_from_today.is_a?(ActiveSupport::Duration)
      raise ArgumentError, 'Argument is not ActiveSupport::Duration'
    end
    expires_on <= Time.zone.today + days_from_today
  end

end
