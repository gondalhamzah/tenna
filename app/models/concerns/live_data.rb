module LiveData
  extend ActiveSupport::Concern
  included do
    after_save :check_tracking_info

    def get_dt_str(timestamp)
      Date.parse(timestamp).strftime('%Y-%m-%d %H:%m:%s')
    end

    def fire_alert(payload, cond, gauge)
      @message << cond.generate_message_and_log(payload)
    end

    def process_non_gauge_cond(payload)
      val = ''
      if payload.has_key?('value')
        val = payload['value']
      elsif payload.has_key?('value_boolean')
        val = payload['value_boolean'].to_s
      elsif payload.has_key?('value_object')
        payload['value_object'].map{|k,v| val << "#{k} = #{v}"} && true
      elsif payload.has_key?('value_string')
        val = payload['value_string']
      end

      @message << {
        message_uniq_id:  SecureRandom.hex(5),
        asset_id: self.asset_id,
        asset_name: self.asset.title,
        site: {name: self.asset.project_name},
        severity: '',
        type: payload['type'],
        value: val,
        msg: payload['description'],
        unit: payload['unit'],
        timestamp: get_dt_str(payload['timestamp'])
      }
    end

    def process_cond(payload, cond, gauge)
      if cond.values
        operator = cond.values['conditional']
        if cond.values['number']
          threshold = cond.values['number'].to_f
        else
          threshold = false
        end
        when_for = cond.values['when/for']
        unit = cond.values['unit']
      end
      real_value = nil
      prev_value = $redis.get "#{@asset_id}-#{gauge.id}-real-value"
      if payload.has_key?('value_boolean')
        real_value = payload['value_boolean']
        prev_value = prev_value == "true" unless prev_value.nil?
      elsif payload.has_key?('value_string')
        real_value = payload['value_string']
      else
        real_value = payload['value_numeric']
        prev_value = prev_value.to_f unless prev_value.nil?
      end

      return real_value if !cond.active

      if threshold
        case gauge.id
          when 1,2,9 #Engine Oil Temperature, Coolant Temperature, Engine Speed
            if real_value > threshold  and (prev_value.nil? ||  prev_value <= threshold)
              fire_alert(payload, cond, gauge)
            end
          when 3 #Battery Voltage
            last_timestamp = $redis.get("#{@asset_id}-battery-last-time")
            last_same_value_seconds = $redis.get("#{@asset_id}-battery-last-same-value-seconds").to_i
            seconds = cond.values['seconds'].to_i
            if (real_value == prev_value and !last_timestamp.nil? and
                !last_same_value_seconds.nil? and seconds and when_for)
              current_same_value_seconds = Time.now.getutc.to_i - last_timestamp.to_i
              if (real_value > threshold and operator == 'exceeds') || (real_value < threshold and operator == 'is below')
                if when_for == 'after' and current_same_value_seconds >= seconds and last_same_value_seconds < seconds
                  fire_alert(payload, cond, gauge)
                end
                if when_for == 'every' and (current_same_value_seconds / seconds).floor >= 1 and ((current_same_value_seconds / seconds).floor > (last_same_value_seconds / seconds).floor)
                  fire_alert(payload, cond, gauge)
                end
              end
            end
          when 7,8,11 #Fuel Level, Fuel Consumption, Idle Hours
            _r = real_value
            _p = prev_value
            if gauge.id == 11 # Idle Hours (payload unit: Hours, frontend unit: minutes)
              _r = _r * 60
              _p = _p * 60 unless _p.nil?
            end

            case operator
              when 'equals'
                if threshold == _r and (_p.nil? || _p != _r)
                  fire_alert(payload, cond, gauge)
                end
              when 'exceeds'
                if _r > threshold and (_p.nil? || _p <= threshold)
                  fire_alert(payload, cond, gauge)
                end
              when 'is below'
                if _r < threshold and (_p.nil? || _p >= threshold)
                  fire_alert(payload, cond, gauge)
                end
            end
          when 5 #Odometer
            if operator == 'every' && threshold != 0 && (real_value / threshold).floor >= 1 && (prev_value.nil? || (prev_value / threshold).floor < (real_value / threshold).floor)
              fire_alert(payload, cond, gauge)
            end

            if operator == 'exceeds' && real_value > threshold && (prev_value.nil? || prev_value <= threshold)
                fire_alert(payload, cond, gauge)
            end
          when 4 #Engine Run Time (payload unit: seconds)
            _r = real_value
            _p = prev_value
            if unit == 'hour(s)'
              _r = _r / 3600
              _p = _p / 3600 unless _p.nil?
            end
            if unit == 'minute(s)'
              _r = _r / 60
              _p = _p / 60 unless _p.nil?
            end

            if operator == 'every' && threshold != 0 && (_r / threshold).floor >= 1 && (_p.nil? || (_p / threshold).floor < (_r / threshold).floor)
              fire_alert(payload, cond, gauge)
            end
            if operator == 'exceeds' && _r > threshold && (_p.nil? || _p <= threshold)
                fire_alert(payload, cond, gauge)
            end
          when 6 #Speed

            if (operator == 'exceeds') && real_value > threshold && (prev_value.nil? || prev_value <= threshold)
              fire_alert(payload, cond, gauge)
            end
            if operator == "exceeds the posted limit by"
              @google_speed_check = true
              @google_speed_cond_ids << cond.id
            end
        end
      else
        case gauge.id
          when 65, 59 # Ignition, Power
            if operator == 'on' && real_value && (prev_value.nil? || prev_value != real_value)
              fire_alert(payload, cond, gauge)
            end
            if operator == 'off' and !real_value && (prev_value.nil? || prev_value != real_value)
              fire_alert(payload, cond, gauge)
            end
          when 10#Moving
            if operator == 'is' && real_value && (prev_value.nil? || prev_value != real_value)
              fire_alert(payload, cond, gauge)
            end
            if operator == 'is not' and !real_value && (prev_value.nil? || prev_value != real_value)
              fire_alert(payload, cond, gauge)
            end
          when 64, 70# Fatigue Driving, Seat Belt
            fire_alert(payload, cond, gauge) if real_value
          when 57,67 #Towering, MIL Alarm
            fire_alert(payload, cond, gauge) if real_value && (prev_value.nil? || real_value != prev_value)
          when 54, 55, 62, 63 #Hard Acceleration, Hard Braking, QuickLane Change, Sharp turn,
            if real_value > gauge.meta["threshold"]
              fire_alert(payload, cond, gauge)
            end
        end
      end
      return real_value
    end

    def fire_geofence_alert(msg, geofence, payload, dt)
      @message << {
        message_uniq_id:  SecureRandom.hex(5),
        asset_id: @asset_id,
        asset_name: @asset.title,
        site: {name: @asset.project_name},
        severity: 'high',
        type: 'Location',
        value: payload['value_object'],
        msg: msg,
        unit: '',
        timestamp: dt
      }
      SendLiveDataGeofenceJob.perform_now(@asset_id, geofence.id, msg, dt)
    end

    def process_geofence(payload, geofence, alert_enter_site, alert_exit_site)
      included = geofence.contains?(@asset.current_lat, @asset.current_lng)
      dt = get_dt_str(payload['timestamp'])

      if alert_enter_site and included
        msg = "#{@asset.title} entered #{geofence.name}"
        fire_geofence_alert msg, geofence, payload, dt
      end

      if alert_exit_site and !included
        msg = "#{@asset.title} exited #{geofence.name}"
        fire_geofence_alert msg, geofence, payload, dt
      end
    end

    def process_geofence_alert_cond(payload, cond, gauge)
      cond.values.each do |val|
        @checked_geofence_ids << val['geofence_id']
        next unless cond.active
        geofence = GeoFence.find_by(id: val['geofence_id'])
        next unless geofence
        process_geofence(payload, geofence, val['alert_enter_site'], val['alert_exit_site'])
      end
    end

    def check_tracking_info
      if self.tracking_info and self.tracking_info['array']
        @google_speed_check = false
        @location_payload = false
        @speed_payload = false

        payload_list = self.tracking_info['array']
        @asset_id = self.asset_id
        @asset = Asset.find(@asset_id)

        @company_id = self.asset.company_id

        group_ids = GaugeAlertGroup.where(asset_id: asset_id, company_id: @company_id).pluck(:id).compact
        if group_ids.blank?
          group_ids = GaugeAlertGroup.where(category: asset.category, company_id: @company_id).pluck(:id).compact
          if group_ids.blank?
            group_ids = GaugeAlertGroup.where(category_group: asset.category.category_group, company_id: @company_id).pluck(:id).compact
          end
        end
        @message = []
        @checked_geofence_ids = []
        @google_speed_cond_ids = []
        payload_list.each do |payload|
          return if !payload || !payload.has_key?('type')
          gauge = Gauge.find_by_name(payload['type'])
          if gauge
            if gauge.id == 12 and payload['type'] == 'Location' then # if location
              @origin_lat = @asset.current_lat
              @origin_lng = @asset.current_lng
              payload['value_object']['latitude'] = payload['value_object']['latitude'].to_f
              payload['value_object']['longitude'] = payload['value_object']['longitude'].to_f

              if @origin_lat != payload['value_object']['latitude'] || @origin_lng != payload['value_object']['longitude']
                # change asest location, but we don't need asset save callback to prevent calling "tracking_info_update"
                @asset.update_columns(
                  :current_lat =>payload['value_object']['latitude'],
                  :current_lng => payload['value_object']['longitude']
                )

                @asset.last_location_changed =  DateTime.parse(payload["timestamp"]).strftime(I18n.t('time.formats.elasticsearch'))
                @asset.save()

                @location_payload = payload
                # insert AV entry manually for location change since  asset save callback is not fired
                av_data = {
                  :asset_id => @asset.id,
                  :project_id => @asset.project_id,
                  :company_id => @asset.company_id,
                }
                av_data[:field_name] = 'cur_lat'
                av_data[:to] = payload['value_object']['latitude']
                av_data[:from] = @origin_lat
                AssetsVersion.create(av_data)

                av_data[:field_name] = 'cur_lng'
                av_data[:to] = payload['value_object']['longitude']
                av_data[:from] = @origin_lng
                AssetsVersion.create(av_data)

                # send mqtt message to frontend for asset location change on map
                @message << {
                  message_uniq_id:  SecureRandom.hex(5),
                  asset_id: @asset_id,
                  type: 'GeoLocation',
                  value: payload['value_object']
                }

                conditions = GaugeCondition.joins(:gauge_alert_group).where(gauge_alert_group_id: group_ids).where('gauge_alert_groups.gauge_id = ? AND cond_type = ?', gauge.id, 'geofence')
                conditions.each do |cond|
                  process_geofence_alert_cond payload, cond, gauge if cond.values
                end

                GeoFence.where(company_id: @company_id).where.not(id: @checked_geofence_ids).each do |g|
                  process_geofence payload, g, g.alert_enter_site, g.alert_exit_site if g.present?
                end
              end
            else
              conditions = GaugeCondition.joins(:gauge_alert_group).where(gauge_alert_group_id: group_ids).where('gauge_alert_groups.gauge_id = ? AND cond_type <> ?', gauge.id, 'geofence')
              real_value = nil
              conditions.each do |cond|
                real_value = process_cond payload, cond, gauge
              end

              if gauge.id == 3 # if battery, we need to set additional value(last timestamp, last same value seconds)
                prev_value = $redis.get "#{@asset_id}-#{gauge.id}-real-value"
                if prev_value.to_s != real_value.to_s or $redis.get("#{@asset_id}-battery-last-time").nil?
                  $redis.set "#{@asset_id}-battery-last-time", Time.now.getutc.to_i
                  $redis.expire "#{@asset_id}-battery-last-time", 60 * 15 # 15 minutes
                end
                last_same_value_seconds = Time.now.getutc.to_i  - $redis.get("#{@asset_id}-battery-last-time").to_i
                $redis.set "#{@asset_id}-battery-last-same-value-seconds", last_same_value_seconds
                $redis.expire "#{@asset_id}-battery-last-same-value-seconds", 60 * 15 # 15 minutes
              end

              $redis.set "#{@asset_id}-#{gauge.id}-real-value", real_value
              $redis.expire "#{@asset_id}-#{gauge.id}-real-value", 60 * 15 # 15 minutse
              if gauge.id == 6
                @speed_payload = payload
              end
            end
          else
            if payload.has_key?('unit') && Asset.units_valid_for_mqtt.include?(payload['unit'])
              process_non_gauge_cond(payload)
            end
          end
        end

        @message << {
          message_uniq_id:  SecureRandom.hex(5),
          asset_id: @asset_id,
          type: 'Payload',
          value: payload_list
        }

        LiveDataMqttJob.perform_now(@company_id, @message)

        # speed limit check using google speed limit api if there is speed, location changed payload and no gauge alert for speed
        # if speed is over 0
        if @speed_payload && @location_payload && @google_speed_check && @speed_payload['value_numeric'] > 0
          # live_data_speed_limit.rb
          check_speed_limit @company_id, @asset_id, @location_payload, @speed_payload, @google_speed_cond_ids
        end
      end
    end
  end

end
