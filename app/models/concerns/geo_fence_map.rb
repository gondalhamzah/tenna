module GeoFenceMap
  R = 6371
  def get_polylines_from_circle(lat, lng, radius, detail=8)

    _lat = lat * Math::PI / 180
    _lng = lng * Math::PI / 180
    d = radius / R

    points = []
    (0..360).step(detail) do |i|
      brng = i * Math::PI / 180
      pLat = Math.asin( Math.sin(_lat) * Math.cos(d) + Math.cos(_lat) * Math.sin(d) * Math.cos(brng))
      pLng = ((_lng + Math.atan2(Math.sin(brng) * Math.sin(d) * Math.cos(_lat), Math.cos(d) - Math.sin(_lat) * Math.sin(pLat))) * 180) / Math::PI
      pLat = (pLat * 180) / Math::PI
      points << [pLat, pLng]
    end

    points
  end

  def get_polylines(geo_fence)
    if geo_fence.shape == "circle"
      pos = geo_fence.center.split(",")
      get_polylines_from_circle(pos[0].to_f, pos[1].to_f, geo_fence.radius / 1000)
    else
      geo_fence.geo_points.map do |p|
        [p.lat, p.lng]
      end
    end
  end

  def static_map_for(geo_fence, options={})
    enc = Polylines::Encoder.encode_points(get_polylines(geo_fence))
    params = {
      :center => geo_fence.center,
      :size => "160x200",
      :maptype => "roadmap",
      :path => "fillcolor:0x#{geo_fence.color.gsub('#', '')}|weight:1|enc:#{enc}",
      :key => ENV['GOOGLE_MAP_API_KEY']
    }.merge(options)

    query_string =  params.map{|k,v| "#{k}=#{v}"}.join("&")
    "https://maps.googleapis.com/maps/api/staticmap?#{query_string}".html_safe
  end
end
