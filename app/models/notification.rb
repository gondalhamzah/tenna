class Notification < ActiveRecord::Base
  attr_accessor :subject
  LOCATIONS = ["Dashboard", "Assets Page", "Project Page", "Marketplace" ]
  NOTIFICATION_MEDIUM = ["Popup", "Email"]
  NOTIFICATION_TYPE = ["Release Notes", "Maintenance"]
  enum client_type: %w(admin company_admin company_user public_user field_member all_users active_company_users all_demo_users)
  enum user_type: %w(private_users)
  belongs_to :admin_user

  def self.dashboard_notifications(user)
    Notification.where("CAST((DATE(notification_datetime) - ?) as integer) <= days_before AND CAST ((DATE(notification_datetime) - ?) as integer) >= 0 and is_active = true and lower(notification_medium) = 'popup' and lower(location) = 'dashboard' and (client_type = ? or client_type = ?)", DateTime.now.utc,DateTime.now.utc, user[:role], 5)
  end

  def self.send_email?(params)
    send_email = false     
    if NOTIFICATION_TYPE.include? params[:notification][:notification_type] and params[:notification][:notification_medium].downcase == "email" and client_types.include? params[:notification][:client_type] and params[:notification][:message] != ""
      send_email = true
    end
    send_email
  end

  def self.get_email_ids(client_type, role)
    # {"admin"=>0, "company_admin"=>1, "company_user"=>2, "public_user"=>3, "field_member"=>4}
    emails = {}
    unless client_type == "public_user"
      companies = Company.all if (client_type == "all_users"  or User.roles.include? client_type  )
      companies = Company.where(company_type: 0) if ( client_type == "active_company_users")
      companies = Company.where(company_type: 1) if client_type == "all_demo_users"
      companies.each do |company|
        users = company.users.where(active:true).where(deleted_at: nil)
        users = users.where(role: User.roles[client_type]) if User.roles.include? client_type
        users_emails = users.pluck(:email)
        emails[company.name] = users_emails
      end
    else
      public_users = User.where(active:true).where(deleted_at: nil).where(role: User.roles[client_type])
      public_users_emails = public_users.pluck(:email)
      emails[""] = public_users_emails
    end
    emails
  end

  def self.get_subject_of_email(message)
    email_subject = "Technology Update for "
    begin
      tech_update_index = message.index('Technology Update')
      tech_update_version = message[tech_update_index..tech_update_index+30].split("</div>")[0].split()[2]
      email_subject+= tech_update_version   
    rescue Exception => e
    end
    email_subject
  end
end
