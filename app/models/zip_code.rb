class ZipCode < ActiveRecord::Base
  geocoder_init latitude: :lat, longitude: :lng

  # Assets must have a zip code. Since we display assets on a map, we need lat/lng coordinates for these zip codes.
  # Rather than store on the asset, we store here, in a separate zip code model. This way, we only need to geocode
  # a given zip code once.

  has_many :assets
  validates :zip_code, zip_code: true, presence: true

  validates :lat, :lng, presence: true

  # Perform geocoding
  before_validation :geocode, if: -> (obj) { obj.zip_code.present? && (obj.zip_code_changed? || obj.country_changed?)}

  # Update Elasticsearch index on zip code save
  update_index('assets#asset') { assets }

  def self.map_markers_for(search_scope)
    result_count_by_zip = AssetSearch.result_count_by_zip(search_scope)

    where(zip_code: result_count_by_zip.keys).pluck(:zip_code, :lat, :lng, :id).map do |zip_code|
      {
        id: zip_code[3],
        zip_code: zip_code[0],
        count: result_count_by_zip[zip_code[0]],
        lat: zip_code[1],
        lng: zip_code[2],
        icon: 'icn-buildsourced-public-location-pin.png'
      }
    end
  end

  def geocode
    if !country.nil?
      result = GenericGeocoder.search(zip_code,country).first
      if result
        if !result.latitude.nil? && !result.longitude.nil?
          self.lat = result.latitude
          self.lng = result.longitude
        end

        # Work around Google geocoder idiosyncrasy; some zip codes are for "neighborhoods", not "cities".
        city = result.city.nil? ? result.neighborhood : result.city

        if !city.nil? && !result.state.nil?
          self.city  = city
          self.state = result.state_code
        end
      end
    end
  end

  def location
    if city.present? && state.present?
      "#{city}, #{state} #{zip_code}"
    else
      zip_code
    end
  end

  def self.country_lists
    country = ISO3166::Country
    list = country.all
    arr = list.map{|a| [a.name]}
    temp = arr[0]
    arr[76] = "United Kingdom"
    arr[232] = "United States"
    arr[0] = arr[232]
    arr[232] = temp
    arr
  end
end
