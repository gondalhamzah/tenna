class Project < ActiveRecord::Base
  belongs_to :company
  belongs_to :contact, class_name: 'User'

  after_create :trigger_hooks_project_create

  has_many :csv_imports, dependent: :destroy
  has_many :groups, dependent: :nullify

  has_many :project_assignments, dependent: :destroy
  has_many :users, through: :project_assignments
  accepts_nested_attributes_for :project_assignments, allow_destroy: true

  has_many :assets, dependent: :nullify
  has_many :groups, dependent: :nullify
  has_one :address, as: :addressable, dependent: :destroy
  accepts_nested_attributes_for :address, allow_destroy: true

  validates :name, :company, :contact, :closeout_date, :address, presence: true
  validates :contact, inclusion: {in: ->(record) { record.company.users }, message: 'is not in the same company'}, unless: 'company.blank?'
  validates_each :contact_sms_number do |record, attr, value|
    record.errors.add(attr, 'must be of US. e.g +12123562424') if !value.blank? and !Phonelib.valid_for_country? value, 'US'
  end

  delegate :lat, :lng, to: :address, prefix: false, allow_nil: true
  delegate :zip, to: :address, prefix: 'location', allow_nil: true

  # Update Elasticsearch index on asset save
  update_index('projects#project') { self }

  scope :available, ->{ where(deleted_at: nil).where('closeout_date >= ?', Date.today).order(:name) }
  scope :not_deleted, ->{ where(deleted_at: nil) }

  def available_assets
    assets.where(state: 'available')
  end

  def self.by_distance(user, coordinate)
    FilterableProjects.new(
      user: user,
      search_options: {company: user.company},
      params:
      ActionController::Parameters.new( {
        filter_search: {
          coordinate: coordinate,
          sort: :distance
        }
      } ),
      zip_code: nil
    ).projects.per(200)
  end

  def self.map_markers_for(user)
    user
      .company
      .projects
      .includes(:address)
      .includes(:assets)
      .includes(:contact)
      .map do |project|
        {
          lat: project.address.lat,
          lng: project.address.lng,
          id: project.id,
          name: project.name,
          contact: project.contact.name,
          address: project.address.short_address,
          asset_count: project.assets.size,
          closeout_date: I18n.l(project.closeout_date, format: :month_day_year),
          icon: 'icn-buildsourced-private-location-pin.png'
        }
      end
  end

  after_initialize do |project|
    project.closeout_date ||= Time.zone.today + 7.days
  end

  def default_project?
    company.project_id == id
  end
  # instead of deleting, indicate the user requested a delete & timestamp it
  def soft_delete
    update_attribute(:deleted_at, Time.current)
  end

  def member_of_project?(user)
    self.project_assignments.where(user_id: user.id).present?
  end

  def project_available?
    self.deleted_at.nil? and self.closeout_date < Date.today
  end

  def has_assets?
    self.assets.not_sold_deleted.count > 0
  end

  def has_groups?
    self.groups.available.count > 0
  end

  def trigger_hooks_project_create
    Hook.trigger('Project_Created', self.to_json, self.company_id)
  end
end
