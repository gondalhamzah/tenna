class User < ActiveRecord::Base
  include Marker

  devise :invitable, :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :confirmable
  enum role: %w(admin company_admin company_user public_user field_member)

  attr_accessor :assign_to_project

  belongs_to :company

  has_many :contact_projects, class_name: 'Project', foreign_key: :contact_id
  has_many :assets, foreign_key: :creator_id
  has_many :wanted_assets, foreign_key: :creator_id
  has_many :csv_imports, dependent: :destroy
  has_many :project_exports, dependent: :destroy
  has_many :project_assignments, dependent: :destroy
  has_many :projects, through: :project_assignments
  has_many :hooks

  has_many :video_usages
  has_many :videos, through: :video_usages

  accepts_nested_attributes_for :project_assignments, reject_if: :do_not_assign_to_project?

  def do_not_assign_to_project?
    assign_to_project == '0'
  end

  validates :first_name, :last_name, :role, presence: true
  validates :company_id, absence: true, if: :public?
  validates :company_id, presence: true, if: :private?

  # admin and public_user cannot belong to company
  validates :company_name, presence: true, if: :public?

  # public_user must have a company name
  validates :company_name, absence: true, if: :private?

  # all private users and admin can't have a company name
  validates :password, format: {with: /\A(?=.{6})(?=.*[^a-zA-Z])/, message: 'you must include at least one special character'}, unless: 'password.nil?'

  # conditional because devise removes and encrypts password and we only want
  # to run validation when we're setting the password. regex ensures
  # at least 6 chars, one of which has to be non-alpha.
  validate :company_admin_validation
  validates :title, length: { maximum: 255}, format: { with: /\A[0-9a-zA-Z \.-]+\z/, message: "only allows letters, numbers, spaces, periods and hyphens" }, allow_blank: true

  validates :phone_number, phony_plausible: true
  phony_normalize :phone_number, default_country_code: 'US'
  validates :terms_of_service, acceptance: true
  validate :admin_validation

  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

  def is_company_contact?
    self.company.contact_id == id
  end

  def admin_validation
    if admin?
      errors.add(:company, 'must be Tenna') unless company.present? && company.admin_company?
    else
      errors.add(:role, 'must be admin') if company.present? && company.admin_company?
    end
  end

  # instead of deleting, indicate the user requested a delete & timestamp it
  def soft_delete
    update_attribute(:deleted_at, Time.current)
    if private?
      replace_creator_and_contact self
    end
  end

  # ensure user account is active
  def active_for_authentication?
    super && active? && !deleted_at
  end

  # provide a custom message for a deleted account
  def inactive_message
    if !active?
      contact = company.contact
      {
        message: :not_active,
        options: {name: contact.name, email: contact.email}
      }
    elsif deleted_at.present?
      :deleted_account
    else
      super
    end
  end

  def send_reset_password_instructions
    super if active_for_authentication?
  end

  def company_admin_validation
    if company.present?
      if company_admin? && role_changed? && company.admin_seats.present? && company.admin_seats_remaining < 1
        errors.add(:role, 'You have no remaining company admin seats. Please contact Tenna to add more.')
      elsif role_changed? && role_was == 'company_admin' && company.company_admins.count == 1
        errors.add(:role, 'You must have at least one company admin.')
      end
    end
  end

  ROLE_ADMIN = 'admin'
  ROLE_COMPANY_ADMIN = 'company_admin'
  ROLE_FIELD_MEMBER = 'field_member'
  ROLES = [['Public User', 'public_user'], ['Administrator', 'company_admin'], ['Admin', 'admin']]
  COMPANY_ROLES = [['Company User', 'company_user'], ['Administrator', 'company_admin'], ['Field', 'field_member']]
  COMPANY_ROLE_HASH = {'company_admin' => 'Administrator', 'company_user' => 'Company User', 'field_member' => 'Field'}

  # Company admins can now only invite company users.
  ASSIGNEABLE_ROLES_WHEN_COMPANY_ADMIN = [['Company User', 'company_user']]
  ASSIGNEABLE_ROLES_WHEN_NO_PROJECT_AND_COMPANY_ADMIN = [['Company User', 'company_user'], ['Field', 'field_member']]
  ROLE_COMPANY_USER = 'company_user'
  BUILDSOURCED_COMPANY_ROLES = [['Admin', 'admin']]

  default_scope { order(:last_name, :first_name) }
  scope :not_deleted, -> { where('deleted_at IS NULL') }
  scope :public_users, -> { where(self.public_user) }

  def full_details
    [name, email, phone_number].join(" - ")
  end

  def name
    "#{first_name.capitalize} #{last_name.capitalize}" unless first_name.nil? || last_name.nil?
  end

  def name_with_initial
    "#{[first_name, last_name[0]].join(' ')}."
  end

  def accepted_invitation?
    created_by_invite? && invitation_token.nil?
  end

  def public?
    public_user?
  end

  def private?
    !public?
  end

  def field_member?
    self.role == 'field_member'
  end

  def company_admin?
    self.role == 'company_admin'
  end

  def project_assigned?
    project_assignments.present?
  end

  def can_assign_projects?
    project_assignments.project_admin.any?
  end

  def can_flag?(asset)
    return (asset.public? and  asset.creator != self) if self.public?
    return (asset.public? and asset.company != self.company)
  end

  def role_gte_company_admin?
    admin? || company_admin? || field_member?
  end

  def self.by_name
    reorder(:first_name, :last_name).select(:id, :first_name, :last_name).map{|u| [u.name, u.id]}
  end

  def status
    if !confirmed?
      'Incomplete'
    elsif !active_for_authentication?
      'Inactive'
    else
      'Active'
    end
  end

  before_save do |user|
    if role_changed? && user.role.blank?
      user.role = nil
    end
  end

  after_save do |user|
    if role_changed? && user.company_admin?
      ProjectAssignment.destroy_all(user: user)
    end
  end

  before_destroy do |user|
    if user.private?
      replace_creator_and_contact user
    else
      user.assets.destroy
    end
  end

  private

  def replace_creator_and_contact(user)
    ## Assets
    # Change the creator of an asset if that creator is deleted
    user.assets.each do |asset|
      if asset.creator == user
        asset.creator = asset.company.contact
        asset.save
      end
    end

    ## Projects
    # Change the contact of a project if that contact is deleted
    user.contact_projects.each do |project|
      project.contact = project.company.contact
      project.save
    end
  end
end
