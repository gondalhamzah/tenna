class GeoFenceCompanyContact < ActiveRecord::Base
  belongs_to :geo_fence
  belongs_to :user

  def as_json(options=nil)
    ret = super()
    ret['name'] = self.user.name
    ret
  end
end
