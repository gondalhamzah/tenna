class RouteGroup < ActiveRecord::Base
  has_many :routes
  has_many :paths, through: :routes
end
