class GaugeAlertGroup < ActiveRecord::Base
  belongs_to :gauge
  belongs_to :category_group
  belongs_to :category
  belongs_to :asset
  belongs_to :company
  has_many :gauge_conditions, dependent: :delete_all

  def as_json(options=nil)
    ret = super(:only => [:id, :gauge_id, :company_id, :category_group_id, :category_id, :asset_id])
    ret[:gauge] = self.gauge.as_json
    ret
  end
end
