class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  enum role: %w(admin super_admin business_admin hr_admin)

  def name
    [first_name, last_name].join(' ')
  end
end
