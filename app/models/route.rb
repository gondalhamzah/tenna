class Route < ActiveRecord::Base
  extend FriendlyId

  friendly_id :uuid, use: [:slugged]
  friendly_id :slug_candidates, use: [:slugged, :finders]
  has_many :paths
  belongs_to :user, :foreign_key => 'user_id', :class_name => 'User'
  belongs_to :company
  belongs_to :route_group
  belongs_to :creator, :foreign_key => 'creator_id', :class_name => 'User'

  def valid_paths
    paths.where("deleted_at is null")
  end

  def valid_paths
    paths.where("deleted_at is null")
  end

  def should_generate_new_friendly_id?
    new_record?
  end

  def start_point
    paths.where(waypoint_type: 'start')
  end

  def end_point
    paths.where(waypoint_type: 'end')
  end

  private
  def slug_candidates
    [
      :name,
      [:id, :name]
    ]
  end
end
