class FilterSearch
  include ActiveModel::Model
  include ActiveModelAttributes

  # This model encapsulates all of the user-facing options that can be set to filter, search & sort assets.

  attr_accessor :favorites,
                :my_assets,
                :closed_assets,
                :active_assets,
                :tags,
                :trackers,
                :equipment,
                :material,
                :buy,
                :rental,
                :has_media,
                :category,
                :category_id,
                :category_group_id,
                :sold,
                :abandoned,
                :marketplace,
                :for_sale,
                :for_rent,
                :rental_mine,
                :buy_mine,
                :buy_others,
                :rental_others,
                :expiring_others,
                :expiring_mine,
                :in_use,
                :available,
                :req_maintenance,
                :has_qr_code,
                :has_trackers,
                :abandoned,
                :price_min,
                :price_max,
                :hours_min,
                :hours_max,
                :miles_min,
                :miles_max,
                :quantity,
                :quantity_min,
                :quantity_max,
                :year_min,
                :year_max,
                :condition,
                :available_on,
                :query,
                :sort,
                :page_size,
                :hide_filters,
                :assets_title,
                :distance_miles,
                :distance_zip,
                :is_mine,
                :contact_id,
                :certified,
                :expiring,
                :coordinate,
                :status,
                :geohash,
                :public,
                :is_others,
                :is_live_data,
                :expires_on,
                :my_wanted,
                :fulfilled,
                :project,
                :without_tracking_code,
                :sort_order,
                :date_from,
                :date_to,
                :export_type,
                :assets_report,
                :zipcode,
                :is_active,
                :is_closed,
                :default_project,
                :group,
                :active_tab,
                :country,
                :location_changed_in,
                :high,
                :medium,
                :low,
                :created_by_me,
                :current_driver


  validates :distance_zip, zip_code: true

  def distance_hash
    return @distance_hash if @distance_hash.present?
    return nil if distance_miles.blank? || distance_zip.blank? || (zip_code = ZipCode.find_or_create_by(zip_code: distance_zip)).nil?
    @distance_hash = {zip_code: zip_code.zip_code, lat: zip_code.lat, lng: zip_code.lng, miles: distance_miles}
  end

  def coordinate_hash
    return nil if coordinate.blank?
    a = coordinate.split(',')
    {
      lat: a[0],
      lon: a[-1]
    }
  end

  def is_mine
    bool_value @is_mine
  end

  def is_others
    bool_value @is_others
  end

  def is_any_filter_selected?
    self.favorites == "1" || self.equipment == "1" || self.material == "1" ||
    self.sold == "1" || self.buy == "1" || self.in_use == "1" || self.abandoned == "1" ||
    self.available == "1" || self.req_maintenance == "1" || self.has_qr_code == "1" ||self.has_trackers == "1" || self.rental == "1" ||
    self.expiring == "1" || (self.project != "" and self.project != nil) ||
    (self.category != "" and self.category != nil) ||
    (self.price_min != "" and self.price_min != nil) ||
    (self.price_max != "" and self.price_max != nil) ||
    (self.hours_min != "" and self.hours_min != nil) ||
    (self.hours_max != "" and self.hours_max != nil) ||
    (self.miles_min != "" and self.miles_min != nil) ||
    (self.miles_max != "" and self.miles_max != nil) ||
    (self.year_min != "" and self.year_min != nil) ||
    (self.year_max != "" and self.year_max != nil) ||
    (self.available_on != "" and self.available_on != nil) ||
    (self.query != "" and self.query != nil) || self.high == "1" ||
    self.medium == "1" || self.low == "1" ||
    (self.distance_miles != "" and self.distance_miles != nil)
  end

  private

  def bool_value value
    value.in?([true, 1, '1', 't', 'true'])
  end
end
