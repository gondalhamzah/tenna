class AssetVersion < PaperTrail::Version
  belongs_to :user, class_name: 'User', foreign_key: 'whodunnit'
  self.table_name = :asset_versions
  self.sequence_name = :asset_versions_id_seq
end
