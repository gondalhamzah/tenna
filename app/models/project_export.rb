class ProjectExport < ActiveRecord::Base
  mount_uploader :file, AttachmentUploader
  default_scope { order(created_at: :desc)}

  belongs_to :project
  belongs_to :user

  validates :user, presence: true
  enum status: %w(enqueued processing ready)
  enum export_type: %w(projects_and_assets projects assets)

  def enqueue(export_project_only = false , export_assets_only = false,filtered_assets_result = nil, browser_timezone = nil, filtered_assets = nil, exported_at = nil, is_filtered = false)
    ProjectExporterJob.perform_later(self,export_project_only,export_assets_only,filtered_assets_result, browser_timezone, filtered_assets, exported_at, is_filtered)
  end

end
