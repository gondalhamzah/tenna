
class WantedAssetSearch
  include ActiveModel::Model

  # favoriter...used on the Favorites page. user is also present, there.
  attr_accessor :filter_search, :creator, :project, :category, :user, :company

  FULL_TEXT_QUERY_FIELDS = [
    'wanted_asset.title',
    'wanted_asset.description',
    'wanted_asset.category'
  ]

  # defines the field types for asset searches we can do, and how they are ordered
  def self.sorts(filter_search = nil)
    {
      date_posted: {created_at: :desc},
      fulfilled_date: {fulfilled_date: :desc},
      price: {price: :asc},
      availability: {needed_on: :asc},
      distance: {}
    }.with_indifferent_access
  end

  def self.search(filter_search, user, creator: nil, project: nil, company: nil)
    new(
      filter_search: filter_search,
      user: user,
      creator: creator,
      project: project,
      company: company
    ).search()
  end

  def ability
    @ability ||= Ability.new(user)
  end

  def search
    apply_filters
      .filter(permissible)
      .filter(for_creator)
      .filter(in_company)
      .query(full_text_query)
      .order(sort_hash)
  end

  def apply_filters
    assets = WantedAssetsIndex::WantedAsset

    assets = my_wanted_assets_filter(assets) if bool_value(filter_search.my_wanted)

    if bool_value(filter_search.high) and bool_value(filter_search.medium) and bool_value(filter_search.low)
      assets = assets_with_all_priorities(assets)
    elsif bool_value(filter_search.high) and bool_value(filter_search.medium)
      assets = assets_with_high_med_priority(assets)
    elsif bool_value(filter_search.high) and bool_value(filter_search.low)
      assets = assets_with_high_low_priority(assets)
    elsif bool_value(filter_search.medium) and bool_value(filter_search.low)
      assets = assets_with_med_low_priority(assets)
    elsif bool_value(filter_search.high)
      assets = wanted_assets_high_priority(assets)
    elsif bool_value(filter_search.medium)
      assets = wanted_assets_medium_priority(assets)
    elsif bool_value(filter_search.low)
      assets = wanted_assets_low_priority(assets)
    end

    assets = category_filter(assets, filter_search.category) if filter_search.category.present?

    if bool_value(filter_search.fulfilled)
      assets = fulfilled_filter(assets, filter_search.status)
    else
      assets = wanted_filter(assets, filter_search.status)
    end

    assets = project_filter(assets, filter_search.project) if filter_search.project.present?

    assets
  end

  def nested_permission(permission)
    permission.flat_map do |k, v|
      if v.is_a?(Hash) && v.has_key?(:project_assignments)
        # use nested query
        # see https://www.elastic.co/guide/en/elasticsearch/reference/1.5/query-dsl-nested-filter.html
        {
          nested: {
            path: 'project_assignments',
            filter: {
              bool: {
                must: v[:project_assignments].map { |k, v| {term: {"project_assignments.#{k}" => v}} }
              }
            }
          }
        }
      else
        {term: {k => v}}
      end
    end
  end

  # ensure that the update permission (being able to edit/renew an asset) is
  # part of the query against this user to elasticsearch
  def edit_permissions
    ability.model_adapter(Asset, :update).instance_variable_get(:@rules).map(&:conditions).map do |permission|
      {
        bool: {
          must: nested_permission(permission)
        }
      }
    end
  end

  def my_wanted_assets_filter assets
    assets.filter(
      bool: {
        must: [],
        should: edit_permissions,
        must_not: []
      }
    )
  end

  def assets_with_all_priorities assets
    assets.filter(terms: { priority: ["High","Medium","Low"], "execution": "or" })
  end

  def assets_with_high_med_priority assets
    assets.filter(terms: { priority: ["High","Medium"], "execution": "or" })
  end

  def assets_with_high_low_priority assets
    assets.filter(terms: { priority: ["High","Low"], "execution": "or" })
  end

  def assets_with_med_low_priority assets
    assets.filter(terms: { priority: ["Medium","Low"], "execution": "or" })
  end

  def wanted_assets_high_priority assets
    assets.filter{priority == "High"}
  end

  def wanted_assets_medium_priority assets
    assets.filter{priority == "Medium"}
  end

  def wanted_assets_low_priority assets
    assets.filter{priority == "Low"}
  end

  def category_filter assets, value
    assets.filter { category_exact == value }
  end

  def fulfilled_filter assets, value
    assets.filter [{
      term: {
        status: 'fulfilled'
      }
    }]
  end

  def wanted_filter assets, value
    assets.filter [{
      term: {
        status: 'contributed'
      }
    }]
  end

  def project_filter assets, value
    assets.filter [{
      term: {
        project_id: value
      }
    }]
  end

  def full_text_query
    if filter_search.query.present?
      filter_search.query.strip!

      {
        simple_query_string: {
          query: filter_search.query + '*',
          fields: FULL_TEXT_QUERY_FIELDS,
          default_operator: 'and'
        }
      }
    else
      {
        match_all: {}
      }
    end
  end

  def permissions
    ability.attributes_for(:read, WantedAsset).reject { |k, v| v.nil? }.map do |k, v|
      {term: {k => v}}
    end
  end

  def permissible
    {
      bool: {
        must: [],
        should: permissions,
        must_not: []
      }
    }
  end

  def for_creator
    return {} if creator.blank?
    {
      term: {
        creator_id: creator.id
      }
    }
  end

  def in_deleted
    {
      not: {
        terms: { status: ['deleted'] }
      }
    }
  end

  def in_company
    return {} if company.blank?
    {
      term: {
        company_id: company.id
      }
    }
  end

  def sort_hash
    WantedAssetSearch.sorts(filter_search)[(filter_search.sort.presence || :date_posted).to_sym]
  end

  def bool_value value
    value.in?([true, 1, '1', 't', 'true'])
  end

end
