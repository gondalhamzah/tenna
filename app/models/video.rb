class Video < ActiveRecord::Base

  has_many :video_usages
  has_many :users, through: :video_usages
  enum emphasis_indicator: %w(HOT NEW POPULAR IMPORTANT)

  def self.get_videos
    where(is_visible: true).order(relative_order: :asc)
  end

end
