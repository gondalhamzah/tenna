class Event < ActiveRecord::Base
  belongs_to :route
  belongs_to :asset
  belongs_to :user
  has_many :route_recurrings, dependent: :nullify

  enum event_type: %w(maint routing other)

  def edited?
    created_at != updated_at
  end

  def verb
    (edited?) ? 'edited' : 'generated'
  end

  def names
    res = []
    user_ids.split(',').map(&:to_i).each do |user_id|
      user = User.find(user_id)
      res << user.name
    end
    res.join(', ')
  end

  def action_dt_str
    if edited?
      updated_at.in_time_zone(timezone).strftime('%l:%M%P %Z on %m/%d/%Y').strip
    else
      created_at.in_time_zone(timezone).strftime('%l:%M%P %Z on %m/%d/%Y').strip
    end
  end

  def is_repeatable?
    !([nil, '', 'null'].include? rec_type) || event_pid.present?
  end

  def repetition_text
    # "day_3___" - each three days
    # "month_2___" - each two months
    # "month_1_1_2_" - second Monday of each month
    # "week_2___1,5" - Monday and Friday of each second week

    # type - the type of repetition: 'day','week','month','year'.
    # count - the interval between events in the "type" units.
    # day and count2 - define a day of a month ( first Monday, third Friday, etc ).
    # days - a comma-separated list of affected week days.
    # extra - extra info that can be used to change the presentation of recurring details.

    event = nil
    if ([nil, '', 'null'].include? rec_type) && event_pid.present?
      event = Event.find(event_pid)
    else
      event = self
    end

    if event.present? && !([nil, '', 'null'].include? event.rec_type)
      rec_type1, extra = event.rec_type.split('#')
      type, count, day, count2, days = rec_type1.split('_')
      count = count.to_i

      start_dt = event.start_date.in_time_zone(event.timezone)
      end_dt = event.end_date.in_time_zone(event.timezone)

      count_str = lambda { |str|  "every #{count > 1 ? (count.to_s+' ') : ''}#{str.pluralize(count)}" }

      if type == 'day'
        res = "#{count_str.call('day')}"
      elsif type == 'week'
        if count == 1 and day == '1,2,3,4,5'
          res = 'every work day'
        else
          days_str = days.split('_').map(&:to_i).map{|w| Date::DAYNAMES[w]}.join(', ')
          res = "#{count_str.call('week')} on the following days #{days_str}"
        end
      elsif type == 'month'
        if day.blank? and count2.blank?
          res = "on day #{start_dt.strftime('%e')} "
        else
          res = "on the #{count2.to_i.ordinalize} #{Date::DAYNAMES[day.to_i]} #{count_str.call('month')}"
        end
      elsif type == 'year'
        if day.blank? and count2.blank?
          res = "on #{start_dt.strftime('%B %e')} #{count_str.call('year')}"
        else
          res = "on the #{count2.to_i.ordinalize} #{Date::DAYNAMES[day.to_i]} in #{start_dt.strftime('%B')} #{count_str.call('year')}"
        end
      end

      if extra != 'no'
        if extra.blank?
          res = "#{res} until #{end_dt.strftime('%m/%d/%Y')}"
        else
          res = "#{res} until it has occurred #{extra} #{'time'.pluralize(extra.to_i)}"
        end
      end

      res
    else
      ''
    end
  end
end
