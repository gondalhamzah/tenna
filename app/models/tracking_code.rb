class TrackingCode < ActiveRecord::Base
  belongs_to :resource, polymorphic: true, touch: true
  attr_accessor :no_of_urls
  belongs_to :company, dependent: :destroy

  validates :token, presence: true, uniqueness: true
  enum status: %w(unassigned assigned decommissioned)
  enum token_type: %w(QR RFID LoRa Sigfox UPC BT Cellular SerialNumber)

  MAX_UNUSED_CODES = 250

  before_save :on_assigment

  default_scope { where(deleted_at: nil) }
  def decommission
    self.update(resource: nil, deleted_at: Time.now, status: 'decommissioned')
  end

  def qr_code
    url = "http://#{ENV["QR_DOMAIN"]}/#{token}"
    RQRCode::QRCode.new(url, :size => 6, :level => :q).as_png({ xdim: 4 }).to_data_url
  end

  def unassign
    resource.versions.create({
       event: 'update',
       whodunnit: PaperTrail.whodunnit,
       object: resource.attributes.to_yaml,
       object_changes: {
         tracking_code: [self.token, nil]
       }.to_yaml
     })
    resource.update(current_lat: nil, current_lng: nil)
    self.decommission
    AssetsIndex::Asset.update_index(resource)
  end

  def self.token_parser(token)
    valid_uri = false
    uri = URI.parse(token)
    uri_host = uri.host
    if uri_host
      valid_uri = uri_host.include? 'bsqr1'
    end

    if ( ( uri.kind_of?(URI::HTTP) or uri.kind_of?(URI::HTTPS) ) and valid_uri )
      parsed_token = uri.path.split('/').last
    else
      parsed_token = token
    end
    parsed_token
  end

  def self.find_tokens_by_type(resource_id, token_type)
    where(resource_id: resource_id, token_type: token_type, deleted_at: nil, status: 1).select('token').order(:created_at)
  end

  def self.find_token(resource_id, token, token_type)
    where(resource_id: resource_id, token: token, token_type: token_type)
  end

  private

  def on_assigment
    if (self.resource_id_changed? or self.resource_type_changed?) and self.resource.present?
      self.status = 'assigned'
    end
  end
end
