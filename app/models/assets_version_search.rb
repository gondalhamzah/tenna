# usage
# Total history
# AssetsVersionSearch.new(company_id: 4, asset_id: 2237).search()

# Edits
# AssetsVersionSearch.new(company_id: 4, asset_id: 2298, actions: ['create', 'update'], fields: ['price', 'public', 'notes', 'photo']).search()

# Hours
# AssetsVersionSearch.new(company_id: 4, asset_id: 2298, fields: ['hours']).search()

# Miles
# AssetsVersionSearch.new(company_id: 4, asset_id: 2298, fields: ['miles']).search()

# location
# AssetsVersionSearch.new(company_id: 4, asset_id: 2298, fields: ['current_lat', 'current_lng']).search()

# Reallocation
# AssetsVersionSearch.new(company_id: 4, asset_id: 2298, fields: ['project_id']).search()

# User History
# AssetsVersionSearch.new(company_id: 4, asset_id: 2298, user_id: 192).search()

class AssetsVersionSearch
  include ActiveModel::Model
  attr_accessor :user_id, :fields, :field_name_not_analyzed, :company_id, :status, :project_id, :asset_id, :actions, :filter_search, :user

  def initialize(attributes={})
    super
    @company_id ||= User.current.company_id if User.current
  end

  def self.total_history(asset_id)
    new(asset_id: asset_id).search()
  end

  def self.edits(asset_id)
    new(asset_id: asset_id, fields: ['price', 'action_public', 'notes', 'photo', 'newly_created']).search()
  end

  def self.group_history(asset_id)
    new(asset_id: asset_id, fields: ['group_id']).search()
  end

  def self.location(asset_id)
    new(asset_id: asset_id, fields: ['current_lat', 'current_lng']).search()
  end

  def self.asset_location(asset_id)
    new(asset_id: asset_id, fields: ['current_lat', 'current_lng']).search().order(created_at: :desc).limit(1)
  end

  def self.asset_inventory(asset_id)
    new(asset_id: asset_id, fields: ['inventory']).search().order(created_at: :desc).limit(1)
  end

  def self.asset_location_multiple(asset_id)
    new(asset_id: asset_id, fields: ['current_lat', 'current_lng']).search().order(created_at: :desc).limit(2)
  end

  def self.inventory(asset_id)
    inventory_versions = new(asset_id: asset_id, fields: ['current_lat', 'current_lng']).search()
    inventory_versions.each do |version|
      if version.field_name == 'current_lat'
        version.from.replace("Lat: #{version.from}") unless version.from.nil?
        version.to.replace("Lat: #{version.to}") unless version.to.nil?
      elsif version.field_name == 'current_lng'
        version.from.replace("Lng: #{version.from}") unless version.from.nil?
        version.to.replace("Lng: #{version.to}") unless version.to.nil?
      end
      version.field_name.replace('Inventory')
    end
    inventory_versions
  end

  def self.reallocation(asset_id)
    new(asset_id: asset_id, fields: ['project_id']).search()
  end

  def self.user_history(asset_id)
    new(asset_id: asset_id, user_id: User.current.try(:id)).search()
  end

  def self.hours(asset_id)
    new(asset_id: asset_id, fields: ['hours']).search()
  end

  def self.miles(asset_id)
    new(asset_id: asset_id, fields: ['miles']).search()
  end

  def self.quantity(asset_id)
    new(asset_id: asset_id, fields: ['quantity']).search()
  end

  def self.maintenance(asset_id)
    new(asset_id: asset_id, field_name_not_analyzed: ['maintenance requested','maintenance completed','notes']).search()
  end

  def self.get_all_history(asset_id, tab_name)
    return new(asset_id: asset_id).search_all() if tab_name == 'total_history'
    return new(asset_id: asset_id, fields: ['price', 'action_public', 'notes', 'photo', 'newly_created']).search_all() if tab_name == 'edits'
    return new(asset_id: asset_id, fields: ['group_id']).search_all() if tab_name == 'groups'
    return new(asset_id: asset_id, fields: ['current_lat', 'current_lng']).search_all() if tab_name == 'location'
    return new(asset_id: asset_id, fields: ['current_lat', 'current_lng']).search_all() if tab_name == 'inventory'
    return new(asset_id: asset_id, fields: ['project_id']).search_all() if tab_name == 'reallocation'
    return new(asset_id: asset_id, user_id: User.current.try(:id)).search_all() if tab_name == 'user_history'
    return new(asset_id: asset_id, fields: ['hours']).search_all() if tab_name == 'hours'
    return new(asset_id: asset_id, fields: ['miles']).search_all() if tab_name == 'miles'
    return new(asset_id: asset_id, fields: ['quantity']).search_all() if tab_name == 'quantity'
    return new(asset_id: asset_id, fields: ['tracking_info', 'auto']).search_all() if tab_name == 'tracking_info'
    return new(asset_id: asset_id, field_name_not_analyzed: ['maintenance requested','maintenance completed','notes']).search_all() if tab_name == 'maintenance'
  end

  def asset_filter_versions asset_filters, company_id, asset_id
    asset_conditions = []
    term_conditions = []
    drivers = asset_filters[:drivers] || []
    users = asset_filters[:users] || []
    titles = asset_filters[:asset_title] || []
    tracking_codes =  asset_filters[:tracking_codes] || []
    asset_management =  asset_filters[:asset_management] || []

    drivers.each do |driver|
      term_conditions << { match: { "asset.current_driver_id": driver} }
    end

    users.each do |user|
      term_conditions << { match: { "asset.creator_id": user} }
    end

    titles.each do |title|
      term_conditions << { match: { "asset.title": title} }
    end

    versions = AssetsVersionsIndex::AssetsVersion
    if(asset_id)
      versions = versions.filter({
                                   term: {
                                     "asset.id"=> asset_id
                                   }
                                 })
    end
    versions = versions.filter({
                                 term: {
                                   company_id: company_id
                                 }
                               }).filter({
                                    range: {
                                      created_at: {
                                          gte: 'now-7d'
                                        }
                                    }}).order(created_at: :desc).limit(100000)

    term_conditions << { terms: {"asset.project_id"=> asset_filters[:sites]}} if asset_filters[:sites].present?
    term_conditions << { terms: { "asset.category_id": asset_filters[:categories] } } if asset_filters[:categories].present?
    term_conditions << { terms: { "asset.location": asset_filters[:locations] } } if asset_filters[:locations].present?
    # term_conditions << { terms: { "asset.category_id": asset_filters[:category_groups] } } if asset_filters[:category_groups].present?
    term_conditions << { terms: { "asset.group_id": asset_filters[:groups] } } if asset_filters[:groups].present?
    #term_conditions << { terms: { "asset.creator_id": asset_filters[:users] } } if asset_filters[:users].present?
    #term_conditions << { terms: { "asset.title": asset_filters[:asset_title] } } if asset_filters[:asset_title].present?
    #term_conditions << { terms: { "asset.current_driver_id": drivers } } if drivers.present?
    term_conditions << { range: { "asset.qr": { gt: 0 } } } if tracking_codes.include? "qr"
    term_conditions << { range: { "asset.rfid": { gt: 0 } } } if tracking_codes.include? "rfid"
    term_conditions << { range: { "asset.lora": { gt: 0 } } } if tracking_codes.include? "lora"
    term_conditions << { range: { "asset.sigfox": { gt: 0 } } } if tracking_codes.include? "sigfox"
    term_conditions << { range: { "asset.upc": { gt: 0 } } } if tracking_codes.include? "upc"
    term_conditions << { range: { "asset.bt": { gt: 0 } } } if tracking_codes.include? "bt"
    term_conditions << { range: { "asset.cellular": { gt: 0 } } } if tracking_codes.include? "cellular"

    asset_conditions << {:query=>{:bool=>{:must=> term_conditions } } } if term_conditions.present?
    versions = versions.filter(asset_conditions)
    # versions = versions.filter({term: {field_name: "tracking_info"}})
    versions = versions.filter({term: {"asset.asset_kind": 'owned'}}) if asset_management.include? "owned"
    versions = versions.filter({term: {"asset.asset_kind": 'rental'}}) if asset_management.include? "rental"
    versions = versions.filter({term: {"asset.asset_kind": 'for_sale'}}) if asset_management.include? "for_sale"
    versions = versions.filter({term: {"asset.asset_kind": 'for_rent'}}) if asset_management.include? "for_rent"
    versions = versions.filter({term: {"asset.status":  'sold' }}) if asset_management.include? "sold"
    versions = versions.filter({term: {"asset.status": 'available'}}) if asset_management.include? "available"
    versions = versions.filter("asset.in_use_until" >= I18n.l(Date.today, format: :elasticsearch)) if asset_management.include? "in_use"
    versions
  end

  def search_all
    if fields.include? "tracking_info"
      apply_filters
        .order(created_at: :desc)
        .limit(1000)
    else
      apply_filters
        .order(created_at: :desc)
        .limit(10000)
    end
  end

  def search
    apply_filters
      .order(created_at: :desc)
      .limit(250)
  end

  def apply_filters
    assets_versions = AssetsVersionsIndex::AssetsVersion

    assets_versions = assets_versions.filter({
        bool: {
          must: [
            term: { company_id: company_id }
          ]
        }
      }) if company_id

    assets_versions = assets_versions.filter({
      bool: {
        must: [
          term: { asset_id: asset_id }
        ]
      }
    }) if asset_id

    assets_versions = assets_versions.filter({
      bool: {
        must: [
          term: { whodunnit: user_id }
        ]
      }
    }) if user_id

    assets_versions = assets_versions.filter({
      bool: {
        must: [
          terms: { field_name: fields }
        ]
      }
    }) if (fields || []).length > 0

    assets_versions = assets_versions.filter({
       bool: {
         must: [
           terms: { field_name_not_analyzed: field_name_not_analyzed }
         ]
       }
     }) if (field_name_not_analyzed || []).length > 0

    assets_versions = assets_versions.filter({
      bool: {
        must: [
          terms: { action: actions }
        ]
      }
    }) if (actions || []).length > 0

    assets_versions
  end

  def self.last_scan_location_and_date(assets_ids)
    assets_versions = AssetsVersionsIndex::AssetsVersion
    assets_versions = assets_versions
                        .filter{(asset_id == assets_ids)}
                        .filter({
                          bool: {
                            must: [
                              terms: { field_name: ['current_lat', 'current_lng'] }
                            ]
                          }
                        }).limit(10000)

    assets_versions
  end

  def self.report_assets_reallocations_for_project(filter_search, user)
    new(
        filter_search: filter_search,
        user: user,
      ).get_assets_reallocations_for_project()
  end

  def get_assets_reallocations_for_project()
    assets_versions = AssetsVersionsIndex::AssetsVersion
    project_id = @filter_search.project.to_s
    date_from = @filter_search.date_from
    date_to = @filter_search.date_to.to_date + 1.day if @filter_search.date_to.present?
    assets_versions = assets_versions
                        .filter({ term: {field_name: 'project_id'}})
                        .filter{ (from == project_id) | (to == project_id) }.limit(10000)
    assets_versions = assets_versions.filter{(created_at >= date_from)} if date_from.present?
    assets_versions = assets_versions.filter{(created_at <= date_to)} if date_to.present?

    assets_versions = assets_versions.order(created_at: :asc)
    assets_versions_departure = assets_versions.filter{(from == project_id)}
    assets_versions_arrival = assets_versions.filter{(to == project_id)}

    assets_versions = set_arrival_and_departure_objects(assets_versions_arrival,assets_versions_departure,project_id)
    if assets_versions.present?
      assets_versions.delete_if{|k| k[:arrival_obj].asset_id.nil?}.count
      assets_versions = assets_versions.sort_by{|k| k[:arrival_obj].asset_id} if @filter_search.sort_order.downcase == 'asset id'
      assets_versions = assets_versions.sort_by{|k| k[:arrival_obj].asset_title} if @filter_search.sort_order.downcase == 'asset title'
    end
    assets_versions
  end

  def asset_daily_filters asset_history
    asset_event_count = asset_history.aggregations(group_by_date: {date_histogram: {field: "created_at", interval: "day"}, aggregations: {dd: {nested: { path: "tracking_info.array" }, aggregations: {aa: { value_count: { field: "tracking_info.array.type" }}, aggregations: {top_hits: { size: 10000}}}}}})
    asset_history = asset_history.aggregations(group_by_date: {date_histogram: {field: "created_at", interval: "day"}, aggregations: {by_top_hit: { top_hits: { size: 10000}}}})
    if(asset_history.aggregations.dig("group_by_date").dig("buckets").present?)
      asset_history = asset_daily_data(asset_history, asset_event_count)
    else
      asset_history = []
    end
    asset_history
  end

  def asset_hour_filters asset_history, date
    asset_history = asset_history.filter({query: { range: { "created_at": { gte: date, lt: date.to_date+1 } } } })
    # asset_history = asset_history.aggregations(group_by_date: {date_histogram: {field: "created_at", interval: "hour"}, aggregations: {by_top_hit: { top_hits: { size: 10000}}}})
    asset_event_count = asset_history.aggregations(group_by_date: {date_histogram:  {field: "created_at", interval: "hour"}, aggregations: {dd: {nested: { path: "tracking_info.array" },  aggregations: {aa: { value_count: { field: "tracking_info.array.type" }}, aggregations: {top_hits: { size: 10000}}}}}})
    asset_history = asset_history.aggregations(group_by_date: {date_histogram: {field: "created_at", interval: "hour"}, aggregations: {by_min: { date_histogram: {field: "created_at", interval: "minute"}, aggregations: {by_top_hit: { top_hits: { size: 10000}}}}}})
    if(asset_history.aggregations.dig("group_by_date").dig("buckets").present?)
      asset_history = asset_history.aggregations["group_by_date"]["buckets"].each_with_index.map do |buck, index|
         event_count = asset_event_count.aggregations["group_by_date"]["buckets"][index].dig("dd").dig("aggregations").dig("hits").dig("hits").each_with_object(Hash.new(0)) { |h1, h2| h2[h1["_source"]["type"]] += 1 }
         by_min = buck.dig("by_min").dig("buckets").map do |min|
          align_events = {"events" => "", "event_count" => "", "asset" => "", "user" => ""} 
          align_events["events"] = hourly_events(min)
          align_events["event_count"] = event_count
          align_events["asset"] = min.dig("by_top_hit").dig("hits").dig("hits").first.dig("_source").dig("asset")
          align_events["user"] = min.dig("by_top_hit").dig("hits").dig("hits").first.dig("_source").dig("whodunnit_name")
          { Time.at(min['key'].to_i/1000).strftime("%I:%M") => align_events} 
        end
        { Time.at(buck['key'].to_i/1000).strftime("%I:00") => by_min}
      end
    else
      asset_history = []
    end
    asset_history
  end

  def hourly_events min
      data = min.dig('by_top_hit').dig('hits').dig('hits').map{|dd| dd["_source"]["tracking_info"]["array"] }.flatten.sort_by {|obj| obj["timestamp"]}.reverse.delete_if {|x| x["unit"] == "Geo Coordinates"}
      if data.length > 0
        data = detect_location_object data
        data
      else
        data = min.dig('by_top_hit').dig('hits').dig('hits').map{|dd| dd["_source"]["tracking_info"]["array"] }.flatten.sort_by {|obj| obj["timestamp"]}.reverse
        data = detect_location_object data
        data
      end
  end

  def detect_location_object data_array
    has_lat = data_array.detect {  |h| h["unit"] == "Current Lat" }
    if has_lat 
      has_lng = data_array.detect {  |h| h["unit"] == "Current Lng" }
      has_acc = data_array.detect {  |h| h["unit"] == "Accuracy" }
      has_acc = {"value_numeric": 0} if has_acc.nil?
      index = data_array.index {  |h| h["unit"] == "Current Lat" }
      data_array = data_array.delete_if {  |h| h["unit"] == "Accuracy" || h["unit"] == "Geo Coordinates" || h["unit"] == "Current Lat" || h["unit"] == "Current Lng" }
      value =  "lat: " + has_lat["value_numeric"].to_s + " ,lng: " + has_lng["value_numeric"].to_s + ", acc: " + has_acc["value_numeric"].to_s
      data_array.insert(index, {'unit': "Location", 'description': "Asset Location change", 'value_string': value, 'type': "Location", 'timestamp': has_lat["timestamp"]})
      data_array.compact
    else
      data_array
    end
  end

  def set_arrival_and_departure_objects(assets_versions_arrival, assets_versions_departure, project_id)
    assets_versions_arr = []
    visited_versions =[]
    assets_versions_arrival.each do |asset_version_arrival|
      have_departure = false
      assets_versions_departure.each do |asset_version_departure|
        if visited_versions.include? asset_version_arrival.id
          break
        end
        if visited_versions.include? asset_version_departure.id
          next
        end

        if asset_version_departure.from == asset_version_arrival.to and asset_version_departure.asset_id == asset_version_arrival.asset_id and asset_version_departure.created_at > asset_version_arrival.created_at
          assets_versions_arr << {arrival_obj: asset_version_arrival ,departure_obj: asset_version_departure}
          visited_versions << asset_version_arrival.id << asset_version_departure.id
          have_departure = true
        end
      end

      if asset_version_arrival.to == project_id and !have_departure
        assets_versions_arr << {arrival_obj: asset_version_arrival ,departure_obj: nil}
      end
    end
    assets_versions_arr
  end
end


def asset_daily_data asset_history, asset_event_count 
  asset_daily_events = asset_history.aggregations["group_by_date"]["buckets"].each_with_index.map do |buck, index|
    event_count = asset_event_count.aggregations["group_by_date"]["buckets"][index].dig("dd").dig("aggregations").dig("hits").dig("hits").each_with_object(Hash.new(0)) do |h1, h2| 
      h2[h1["_source"]["type"]] += 1
    end
    uni_events = buck.dig('by_top_hit').dig('hits').dig('hits').map{|dd| dd["_source"]["tracking_info"]["array"] }.flatten.sort_by {|obj| obj["timestamp"]}.delete_if {|x| x["unit"] == "Geo Coordinates"}
    if uni_events.length <= 0
      uni_events = buck.dig('by_top_hit').dig('hits').dig('hits').map{|dd| dd["_source"]["tracking_info"]["array"] }.flatten.sort_by {|obj| obj["timestamp"]}
    end

    uni_events = detect_location_object uni_events
    first = uni_events.first
    last = uni_events.last
    location = buck.dig('by_top_hit').dig('hits').dig('hits').last.dig("_source").dig("asset").dig("location")
    user = buck.dig('by_top_hit').dig('hits').dig('hits').last.dig("_source").dig("whodunnit_name")
    first[:created_at] = buck.dig('by_top_hit').dig('hits').dig('hits').first.dig("_source").dig("created_at")
    last[:created_at] = buck.dig('by_top_hit').dig('hits').dig('hits').last.dig("_source").dig("created_at")
   {Time.at(buck['key'].to_i/1000).to_date.to_s => {first_event: first, last_event: last, user: user, location: location, event_count: event_count}}
  end
  asset_daily_events
end
