module PaperTrail
  class Version < ActiveRecord::Base
    include PaperTrail::VersionConcern
    self.abstract_class = true

    def show_changes(params = '')
      output = []
      self.changeset.keys.each do |key|
        # if self.event == 'Photo Changes'
        #   output << '<strong>' + key.underscore.humanize + '</strong> changed'
        # else
        #   output << changes_process_key(self.changeset, key)
        # end
        if !params.empty? && params[:controller] == 'assets'
          output << asset_changes_process_key(self.changeset, key)
        else
          output << changes_process_key(self.changeset, key)
        end
      end
      output.compact.join('<br>')
    end

    def changes_process_key(changeset, key)
      from = changeset.values_at(key)[0][0]
      if from.class == Time
        from = from.in_time_zone('Eastern Time (US & Canada)').strftime('%d/%m/%Y %I:%M:%S %p')
      else
        from = from.to_s
      end
      to = changeset.values_at(key)[0][1]
      if to.class == Time
        to = to.in_time_zone('Eastern Time (US & Canada)').strftime('%d/%m/%Y %I:%M:%S %p')
      else
        to = to.to_s
      end
      if from != '' and to != '' and from != to and key != 'updated_at'
        '<b>' + key.underscore.humanize + '</b> changed' + (from != '' ? ' from <em>"' + from + '"</em> ' : '') + ' to <em>"' + to + '"</em>'
      elsif from == '' and to != '' and from != to and key != 'updated_at'
        '<b>' + key.underscore.humanize + '</b> set to <em>"' + to + '"</em>'
      end
    end

    def asset_changes_process_key(changeset, key)
      from = changeset.values_at(key)[0][0]
      to = changeset.values_at(key)[0][1]

      if key == 'project_id' && from.nil?
        'New Asset Created for <b><em>"' + Project.find(to).try(:name) + '"</em></b> ' + key.underscore.humanize
      elsif key == 'public'
        if from
          'Public marketplace asset moved to <em>"<b>Private Company Database</b>"</em>'
        else
          'Company asset moved to <em>"<b>Public Marketplace</b>"</em>'
        end
      elsif key =='tracking_code' && to.nil?
        '<b>Tracking code </b><em>"was removed"</em>'
      elsif key == 'current_lat' && to.nil?
        '<b>Current lat </b><em>"was removed"</em>'
      elsif key == 'current_lng' && to.nil?
        '<b>Current lng </b><em>"was removed"</em>'
      else
        if from.class == Time
          from = from.in_time_zone('Eastern Time (US & Canada)').strftime('%d/%m/%Y %I:%M:%S %p')
        else
          from = (key == 'project_id') ? Project.find(from).try(:name) : from.to_s
        end

        if to.class == Time
          to = to.in_time_zone('Eastern Time (US & Canada)').strftime('%d/%m/%Y %I:%M:%S %p')
        else
          to = (key == 'project_id') ? Project.find(to).try(:name) : to.to_s
        end

        if from != '' and to != '' and from != to and key != 'updated_at'
          if key == 'hours' || key == 'miles' || key == 'quantity'
            '<b>' + key.sub('_','/').titleize + '</b> changed' + (from != '' ? ' from <em>"' + from + '"</em> ' : '') + ' to <em>"' + to + '"</em>'
          else
            '<b>' + key.underscore.humanize + '</b> changed' + (from != '' ? ' from <em>"' + from + '"</em> ' : '') + ' to <em>"' + to + '"</em>'
          end
        elsif from == '' and to != '' and from != to and key != 'updated_at'
          if key == 'hours' || key == 'miles' || key == 'quantity'
            '<b>' + key.sub('_','/').titleize + '</b> set to <em>"' + to + '"</em>'
          else
            '<b>' + key.underscore.humanize + '</b> set to <em>"' + to + '"</em>'
          end
        end
      end
    end

    def user_full_name
      if self.whodunnit.nil?
        'n/a'
      else
        self.user.last_name.capitalize + ', ' + self.user.first_name.capitalize
      end
    end
  end
end
