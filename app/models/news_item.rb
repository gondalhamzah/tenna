class NewsItem < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  def should_generate_new_friendly_id?
    new_record?
  end

  def article_date
    self.post_date || self.updated_at
  end

  scope :published, -> { where(active: true) }
end
