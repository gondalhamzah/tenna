class AnalyticFilterLabel < ActiveRecord::Base
  belongs_to :company
  has_many :analytic_filters

  validates :name, presence: true, uniqueness: { scope: :company, message: 'Filter Label name should be unique' }
end
