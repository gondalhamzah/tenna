class GroupFilterSearch
  include ActiveModel::Model
  include ActiveModelAttributes

  # This model encapsulates all of the user-facing options that can be set to filter, search & sort assets.

  attr_accessor :query,
                :sort,
                :page_size,
                :project,
                :sort_order,
                :is_active,
                :is_closed,
                :default_project,
                :group

  
  def is_any_filter_selected?
    self.is_active == "1" || self.is_closed == "1" || self.default_project == "1" ||
    (self.query != "" and self.query != nil)
  end

  def is_closed?
    self.is_closed == "1" ? true : false
  end

  private

  def bool_value value
    value.in?([true, 1, '1', 't', 'true'])
  end
end
