class ListItem < ActiveRecord::Base
  belongs_to :list
  belongs_to :Asset
  enum status: %w(found not_found not_in_list not_in_assets)
end
