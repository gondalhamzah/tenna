class GaugeCondition < ActiveRecord::Base
  belongs_to :gauge_alert_group

  def severe_str
    case severe_level
      when "H"
        "HIGH"
      when "M"
        "MEDIUM"
      when "L"
        "LOW"
    end
  end

  def config_field_name
    "#{severe_str.downcase}_points"
  end

  def get_dt_str(timestamp)
    DateTime.parse(timestamp).strftime(I18n.t('date.formats.month_day_year_and_time'))
  end

  def generate_message_and_log(payload)
    gauge = self.gauge_alert_group.gauge
    msg = gauge.template
    self.values.each do |k, v|
      msg = msg.gsub("[#{k}]", v)
    end

    dt_for_frontend = get_dt_str(payload['timestamp'])

    asset_id = self.gauge_alert_group.asset_id
    asset = self.gauge_alert_group.asset
    SendLiveDataAlertJob.perform_now(asset_id, self.id, dt_for_frontend, payload)
    messg = {
      message_uniq_id:  SecureRandom.hex(5),
      asset_id: asset_id,
      asset_name: asset.title,
      site: {name: asset.project.try(:name),id: asset.project.try(:id)},
      severity: self.severe_str.downcase,
      type: self.gauge_alert_group.gauge.name,
      value: payload['value_numeric'],
      msg: msg,
      unit: payload['unit'],
      timestamp: dt_for_frontend
    }

    current_driver_id = asset ? asset.current_driver_id : nil
    GaugeAlertLog.create(gauge_condition_id: self.id,
                         dismissed: false,
                         asset_id: asset_id,
                         values: messg,
                         operator_id: current_driver_id
    )

    messg
  end
end
