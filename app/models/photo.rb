class Photo < Attachment
  mount_uploader :file, PhotoUploader
  default_scope ->{order(position: :asc)}

  # Update Elasticsearch index on asset save
  update_index('assets#asset') { asset }

  after_create :create_photo_created_version
  after_destroy :create_photo_deleted_version

  def create_photo_created_version
  	AssetsVersion.create!(asset_id: self.asset_id, field_name: 'photo', from: nil, to: self.id,
            whodunnit: User.current.try(:id), project_id: self.asset.try(:project).try(:id), company_id: self.asset.company_id, action: 'create') if self.id
  end

  def create_photo_deleted_version
  	AssetsVersion.create!(asset_id: self.asset_id, field_name: 'photo', from: self.id, to: nil,
            whodunnit: User.current.try(:id), project_id: self.asset.try(:project).try(:id), company_id: self.asset.company_id, action: 'destroy') if self.id
  end

end
