class ReportAssetsSearch
  include ActiveModel::Model
  include ActiveModelAttributes


  attr_accessor :sort_order,
                :date_from,
                :date_to,
                :export_type,
                :assets_report,
                :project,
                :projects,
                :asset_categories,
                :group,
                :equipment,
                :material,
                :asset_category_filter,
                :project_filter,
                :internal_rental_rate,
                :calculate_cost
end
