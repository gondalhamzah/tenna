class Address < ActiveRecord::Base
  attr_accessor :current_coords,:change_addr
  # This class is used to store addresses for companies as well as projects.
  belongs_to :addressable, polymorphic: true, touch: true

  # validates :zip, :line_1, :city, :state, presence: true
  validates :zip, zip_code: true, allow_blank: true

  include AddressStrip
  # Geocode the address so we can show pin on map
  after_validation :geocode, if: -> (a) { a.formatted_address_changed? }

  def formatted_address_changed?
    (changed_attributes.keys & ['line_1', 'line_2', 'city', 'state', 'zip']).present?
  end

  def formatted_address
    # Dont change "\n" to single quotes and strip!
    line_3 = "#{city}, #{state} #{zip}"
    [line_1, line_2, line_3].compact.join("\n")
  end

  def geocode
    result = GenericGeocoder.search(formatted_address, country, country ).first
    if result.nil? and @current_coords.present?
      coords = JSON.parse(@current_coords)
      curr_lat = coords["lat"]
      curr_lng = coords["lng"]
      rev_result = Geocoder.search([curr_lat,curr_lng]).first
      # self.line_1 = rev_result.street_address if rev_result.street_address
      # self.city = rev_result.city if rev_result.city
      # self.state = rev_result.state if rev_result.state
      # self.zip = rev_result.postal_code if rev_result.postal_code
      # self.lat = rev_result.latitude
      # self.lng = rev_result.longitude
    end

    # if lat.nil? or lng.nil?
    #   self.lat = result.latitude
    #   self.lng = result.latitude
    # end

    # if result and !Rails.env.test?
    #   if result.state
    #     unless (state.downcase == result.state.downcase || state.downcase == result.state_code.downcase)
    #       self.errors.add(:state, 'is not a valid State')
    #     end
    #   end

    #   unless result.postal_code == zip
    #     self.errors.add(:line_1, 'is not a valid Address')
    #   end
    # end

    if result && !result.latitude.nil? && !result.longitude.nil? && @change_addr.present?
      self.lat = result.latitude
      self.lng = result.longitude
    end
  end

  def current_coords=(value)
    @current_coords = value
  end

  def current_coords
    @current_coords
  end

  def change_addr=(value)
    @change_addr = value
  end

  def change_addr
    @change_addr
  end

  def short_address
    city.present? && state.present? ? "#{city}, #{state}" : zip
  end

end
