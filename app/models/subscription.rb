class Subscription < ActiveRecord::Base
  belongs_to :company

  enum category: {
    routing: 'routing',
    rfid: 'rfid',
    lora: 'lora',
    sigfox: 'sigfox',
    'asset reports': 'asset reports',
    'company reports': 'company reports',
    wantit: 'wantit',
    findit: 'findit',
    scheduleit: 'scheduleit',
    maintenance: 'maintenance',
    'add tracking to asset': 'add tracking to asset',
    livedata: 'livedata',
    'safety reports': 'safety reports',
  }

  enum state: {
    active: 'active',
    suspended: 'suspended'
  }

  before_create :generate_api_key,:check_subscription_presence unless  Rails.env.test?
  before_update :check_changed_attributes,:same_subscription_presence?
  before_destroy :delete_vendor_subaccount

  def soft_delete
    update_attributes(deleted_at: Time.current, state: :suspended)
  end

  def check_subscription_presence
    if company
      has_Subscription = company.subscriptions.where(category: category)
    end
    if has_Subscription.present?
      self.errors.add(:category, 'This subscription has already been added for this company.')
      return false
    end
  end

  def same_subscription_presence?
    if self.changes.include? 'category' and self.changes[:category][0] != self.changes[:category][1]
      if company and company.subscriptions.where(category: self.changes[:category][1]).present?
        self.errors.add(:category, 'This subscription has already been added for this company.')
        return false
      end
    elsif company and company.subscriptions.where(category: category).present?
      prev_subs =  company.subscriptions.where(category: category).first
      if prev_subs.category != category
        self.errors.add(:category, 'This subscription has already been added for this company.')
        return false
      end
    end
  end

  def reactivate
    update_attributes(deleted_at: nil, state: :active)
  end

  def check_changed_attributes
    if self.number_of_units_changed?
      update_number_of_units
      return false if self.errors.present?
    end
  end

  def generate_api_key
    api_key, vendor_id = get_new_api_key
    if api_key.present?
      self.api_key = api_key
      self.api_key_created_at = Time.now if self.api_key_created_at.blank?
      self.api_key_updated_at = Time.now
      self.vendor_id = vendor_id
    elsif self.subscription_flag_for_some
      true
    else
      errors.add(:api_key, 'could not be generated')
      false
    end
  end

  def subscription_flag_for_some
    if self.category == 'asset reports' or self.category == 'findit' or self.category == 'scheduleit' or self.category == 'livedata' or self.category == 'company reports' or self.category == 'maintenance' or self.category == 'add tracking to asset' or self.category == 'wantit' or self.category == 'rfid' or self.category == 'lora' or self.category == 'sigfox' or self.category == 'safety reports'
      true
    end
  end

  def update_api_key!
    generate_api_key

    self.save if errors.blank?
  end

  private
  def get_new_api_key
    api_key, vendor_id = case self.category
                           when 'routing'
                             get_new_routing_api_key
                           else
                             [nil, nil]
                         end

    [api_key, vendor_id]
  end

  def update_number_of_units
    case self.category
      when 'routing'
        update_number_of_routing_vehicles
      else
        nil
    end
  end

  def delete_vendor_subaccount
    case self.category
      when 'routing'
        delete_routific_subaccount
      else
        nil
    end
  end

  # Routing - start
  def get_new_routing_api_key
    Routific.setToken(ENV['ROUTIFIC_API_KEY'])

    data = {
      email: self.company.contact.email,
      password: SecureRandom.hex,          # Random password
      numVehicles: self.number_of_units || 1,
      plan: 'per_vehicle'
    }

    api_key = nil
    vendor_id = nil
    begin
      logger.debug "-----#{ Time.now }-----"
      logger.debug "Generating new API Key for \"#{self.company.name}\""
      response = RestClient.post('https://api.routific.com/users/subaccounts',
                                 data.to_json,
                                 'Authorization' => "bearer #{Routific.token}",
                                 content_type: :json,
                                 accept: :json
      )

      subaccount = JSON.parse(response)

      api_key = subaccount['token']
      vendor_id = subaccount['id']
    rescue => e
      puts e
      errorResponse = JSON.parse e.response.body
      puts "Received HTTP #{e.message}: #{errorResponse["error"]}"
    end

    [api_key, vendor_id]
  end

  def update_number_of_routing_vehicles
    Routific.setToken(ENV['ROUTIFIC_API_KEY'])

    data = {
      numVehicles: self.number_of_units || 1
    }

    begin
      logger.debug "-----#{ Time.now }-----"
      logger.debug "Updating number of vehicles for \"#{self.company.name}\""
      response = RestClient.put("https://api.routific.com/users/subaccounts/#{self.vendor_id}",
                                 data.to_json,
                                 'Authorization' => "bearer #{Routific.token}",
                                 content_type: :json,
                                 accept: :json
      )

      subaccount = JSON.parse(response)

      errors.add(:number_of_units, 'could not be updated') if subaccount['id'].blank?
    rescue => e
      puts "Received HTTP error when updating number of vehicles #{e.message}"
      errors.add(:number_of_units, 'could not be updated')
    end
  end

  def delete_routific_subaccount
    Routific.setToken(ENV['ROUTIFIC_API_KEY'])

    begin
      logger.debug "-----#{ Time.now }-----"
      logger.debug "Deleting Routific Subaccount for \"#{self.company.name}\""
      response = RestClient.delete("https://api.routific.com/users/subaccounts/#{self.vendor_id}",
                                   'Authorization' => "bearer #{Routific.token}")

      errors.add(:id, 'could not be deleted') if response != 'OK'
    rescue => e
      logger.debug "Received HTTP error when deleting routific subaccount #{e.message}"
      errors.add(:id, 'could not be deleted')
    end
  end
  # Routing - end
end
