class VehicleScoreCard < SafetyScoreCard
  attr_accessor :asset

  def initialize(attributes={})
    super
  end

  def search_params
    {asset_id: asset.id, created_at: (self.start_date.to_datetime..self.end_date.to_datetime)}
  end
end
