class AdminAbility
  include CanCan::Ability

  # Encapsulates all logic for authorizations in app. Devise handles authentication; CanCan handles authorization.
  # Example: Once a user has logged in, what operations are they allowed to perform and on which models. Can the
  # current user delete this asset? etc.

  def initialize(admin)
    can :manage, :all
    cannot :destroy, Company
    cannot :invite, User
    if admin.super_admin?
      cannot :hard_destroy, Company do |company|
        not company.can_be_destroyed?
      end
    else
      cannot :hard_destroy, Company
    end
    if admin.admin?
      cannot :manage, TrackingCode
    end
  end
end
