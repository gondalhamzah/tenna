
class ProjectSearch
  include ActiveModel::Model
  attr_accessor :filter_search, :user, :company

  # This class is used to generate an Elasticsearch query that represents the filter, search & sort criteria input
  # from the user

  FULL_TEXT_QUERY_FIELDS = [
    'project.name',
    'project.project_num'
  ]

  API_FULL_TEXT_QUERY_FIELDS = [
    'project.name'
  ]

  def self.sorts(coordinate = [])
    {
      date_posted: {updated_at: :desc},
      name: {name_not_analyzed: :asc},
      closeout_date: {closeout_date: :asc},
      distance: {
        _geo_distance: {
          coordinates: {
            lat: coordinate[0],
            lon: coordinate[1]
          },
          order: "asc",
          unit: "miles",
          distance_type: "plane"
        }
      }
    }
  end

  def self.search(filter_search, user, company: nil)
    new(
      filter_search: filter_search,
      user: user,
      company: company
    ).search
  end

  def self.api_search(filter_search, user, company: nil)
    new(
      filter_search: filter_search,
      user: user,
      company: company
    ).api_search()
  end

  def search
    apply_filters
      .filter(in_company)
      .query(full_text_query)
  end

  def api_search
    apply_filters
      .filter(in_company)
      .query(api_full_text_query)
  end

  def api_full_text_query
    if filter_search.query.present?
      filter_search.query.strip!
      {
        simple_query_string: {
          query: '*' + filter_search.query + '*',
          analyze_wildcard: true,
          fields: API_FULL_TEXT_QUERY_FIELDS
        }
      }
    end
  end

  def apply_filters
    projects = ProjectsIndex::Project
    projects = zip_code_filter(projects, filter_search.zipcode) if filter_search.zipcode != ""
    projects = zip_code_with_country_name_filter(projects, filter_search.distance_zip, filter_search.country.downcase) if filter_search.zipcode != "" and !filter_search.country.nil?
    projects = projects.filter{ !deleted_at }

    projects = distance_filter(projects, filter_search.distance_hash) if filter_search.distance_hash.present?
    projects = contact_id_filter(projects, filter_search.contact_id) if filter_search.contact_id.present?
    projects = my_projects_filter(projects) if (bool_value(filter_search.is_mine) && user)
    projects = geohash_filter(projects, filter_search.geohash) if (not filter_search.geohash.nil? and not filter_search.geohash.empty?)
    # Run 'if block' only if both filters (active and closed) are not checked
    if not bool_value(filter_search.is_active) && bool_value(filter_search.is_closed)
      projects = closed_status_filter(projects) if (bool_value(filter_search.is_closed)) # if closed filter applied
      projects = active_status_filter(projects) if (bool_value(filter_search.is_active)) # if active filter applied
    end

    projects
  end

  def self.get_project(project_id)
    project = ProjectsIndex::Project
    project = project.filter{(id == project_id)}.first
    project
  end

  private

  def geohash_filter projects, geohashes
    projects.filter({
      or: {
        filters: geohashes.map do |geohash|
          coords = GeoHash.decode_bbox(geohash)
          {
            geo_bounding_box: {
              coordinates: {
                bottom_left: {
                  lat: coords.first.first,
                  lon: coords.first.last
                },
                top_right: {
                  lat: coords.last.first,
                  lon: coords.last.last
                }
              }
            }
          }
        end
      }
    })
  end

  def distance_filter projects, value
    if value[:miles] == 'Exact'
      projects.filter { location_zip == value[:zip_code] }
    else
      projects.filter(geo_distance: {
        distance: "#{value[:miles]}miles",
        coordinates: {
          lat: value[:lat],
          lon: value[:lng]
        }
      })
    end
  end

  def zip_code_filter projects, value
    projects.filter { location_zip == value }
  end

  def zip_code_with_country_name_filter projects, distance_zip , cou
    if distance_zip
      projects.filter {(location_zip == distance_zip)}.filter{(country == cou)}
    end
  end

  def contact_id_filter projects, value
    projects.filter { contact_id == value }
  end

  def my_projects_filter projects
    contact_id_filter projects, user.id
  end

  def active_status_filter projects
    projects.filter { closeout_date > Date.today }
  end

  def closed_status_filter projects
    projects.filter { closeout_date <= Date.today }
  end

  def in_company
    return {} if company.blank?
    {
      term: {
        company_id: company.id
      }
    }
  end

  def full_text_query
    if filter_search.query.present?
      {
        simple_query_string: {
          query: filter_search.query+"*",
          analyze_wildcard: true,
          fields: FULL_TEXT_QUERY_FIELDS
        }
      }
    else
      {
        match_all: {}
      }
    end
  end

  def bool_value value
    value.in?([true, 1, '1', 't', 'true'])
  end
end
