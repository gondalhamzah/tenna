class GroupsVersion < ActiveRecord::Base
  belongs_to :user
  belongs_to :company
  # Update Elasticsearch index on GroupssVersion save
  update_index('groups_versions#groups_version') { self }
end
