class Hook < ActiveRecord::Base

  validates_presence_of :event_name, :subscription_url, :target_url, :provider

  belongs_to :user

  def self.trigger(event, data, company)
    hooks = where(event_name: event, company_id: company)
    return if hooks.empty?
    sendHook(hooks,data)
  end

  def self.trigger_asset(event, data, previous)
    hooks = where(event_name: event, company_id: data.company.id)
    return if hooks.empty?
    data = [{id: data.id, title: data.title, Currenct_Project: data.project.name, date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Currenct_Project_id: data.project.id, Previous_Project: previous.name, Previous_Project_id: previous.id, Description: "This asset was moved from project:" + previous.name + "to project: "+ data.project.name, Asset_description: data.description}]
    sendHook(hooks,data.to_json)
  end

  def self.trigger_new_asset(event, data)
    return if !data.company
    hooks = where(event_name: event, company_id: data.company.id)
    return if hooks.empty?
    data = [{id: data.id, title: data.title, date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Asset_description: data.description, Project_id: data.project.id, Project_Name: data.project.name, Description: "This asset was just created"}]
    sendHook(hooks,data.to_json)
  end

  def self.trigger_marked_rental_or_public(event, data)
    return if !data.company
     hooks = where(event_name: event, company_id: data.company.id)
    return if hooks.empty?
    data = [{id: data.id, title: data.title, date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Asset_description: data.description, Project_id: data.project.id, Project_Name: data.project.name, Description: "This asset was marked as Public."}]
    sendHook(hooks,data.to_json)
  end

  def self.trigger_hooks_video_view(event, video, data)
    return if !data.company
     hooks = where(event_name: event, company_id: data.company.id)
    return if hooks.empty?
    data = [{Description: "The user " + data.name + " has viewed his/her first video", User_ID: data.id, User_Name: data.name, User_Role: data.role, Company_Name: data.company.name, Company_id: data.company.id, Video_Name: video.name, Video_id: video.id}]
    sendHook(hooks,data.to_json)
  end

  def self.trigger_asset_remove_mp(event, data)
    return if !data.company
     hooks = where(event_name: event, company_id: data.company.id)
    return if hooks.empty?
    data = [{id: data.id, title: data.title, date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Asset_description: data.description, Project_id: data.project.id, Project_Name: data.project.name, Description: "This asset was removed from the marketplace."}]
    sendHook(hooks,data.to_json)
  end

  def self.trigger_available_on_asset(event, data)
    return if !data.company
     hooks = where(event_name: event, company_id: data.company.id)
    return if hooks.empty?
    data = [{id: data.id, title: data.title, date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Asset_description: data.description, Project_id: data.project.id, Project_Name: data.project.name, Description: "The 'avaialble on' date for this asset has passed."}]
    sendHook(hooks,data.to_json)
  end

  def self.trigger_route_stop(event, route, path)
    hooks = where(event_name: event, company_id: route.company_id)
    return if hooks.empty?
    route_done = {asset_title: "N/A", date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), project_name: "N/A", asset_description: "N/A", driver_name: route.user.name, driver_id: route.user.id, route_name: route.slug, route_id: route.id}
    if path.waypoint_id != 0
      if path.waypoint_type == "asset"
        asset = Asset.find path.waypoint_id
        route_done = {asset_title: asset.title, date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), project_name: "N/A", asset_description: asset.description, driver_name: route.user.name, driver_id: route.user.id, route_name: route.slug, route_id: route.id}
      else
        project = Project.find path.waypoint_id
        route_done = {asset_title: "N/A", date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), project_name: project.name, asset_description: "N/A", driver_name: route.user.name, driver_id: route.user.id, route_name: route.slug, route_id: route.id}
      end
    end
    route_done = [route_done]
    sendHook(hooks,route_done.to_json)
  end

  def self.trigger_hooks_maintenance(event, asset, maintenance)
    hooks = where(event_name: event, company_id: asset.company.id)
    return if hooks.empty?
    if maintenance
      data = [{id: asset.id, title: asset.title, Notes: asset.notes, date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Description: "Maintenance has been completed for this asset.", Project_id: asset.project.id, Project_Name: asset.project.name, Asset_description: asset.description}]
    else
      data = [{id: asset.id, title: asset.title, Notes: asset.notes, date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Description: "Maintenance has been request for this asset", Project_id: asset.project.id, Project_Name: asset.project.name, Asset_description: asset.description}]
    end
    sendHook(hooks,data.to_json)
  end


  def self.sendHook(hooks, data)
    hook_ids = hooks.pluck(:id)
    TriggerJob.perform_later(hook_ids, data)
  end

  def self.perform(klass, id)
    puts "Performing REST hook Resque job: #{klass} #{id}"
    event = "new_#{klass.to_s.underscore}"
    record = klass.camelize.constantize.find(id)
    Hook.trigger(event, record)
  end
  @queue = :rest_hook
  @retry_limit = 3
  @retry_delay = 5

  def self.hooks(event, record)
    Hook.find(:all, :conditions => {
        :event_name => event,
      })
  end

  def self.hooks_exist?(event, record)
    self.hooks(event, record).size > 0
  end

end
