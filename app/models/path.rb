class Path < ActiveRecord::Base
  belongs_to :route

  enum priority: [ :low, :regular, :high ]

  scope :projects, -> { where(waypoint_type: 'project') }
  scope :assets, -> { where(waypoint_type: 'asset') }
end
