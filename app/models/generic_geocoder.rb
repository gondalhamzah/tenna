class GenericGeocoder
  def self.valid_zip_code?(zip_code, country_code = nil)
    valid_zip = false
    # Geocoder.search("#{zip_code},#{country_code}").each do |result|
    Geocoder.search(zip_code, params: { components: "country: #{country_code}" }).each do |result|
      # if result.data['types'].include? 'postal_code' or result.data['address_components'][2]['types'].include? 'postal_code'
      if result.postal_code == zip_code or result.data['types'].include? 'postal_code' or result.data['formatted_address'].include? zip_code
        valid_zip = true
      end
    end
    valid_zip
  end

  def self.valid_zip_with_country?(zip_with_country)
    valid_zip = false
    Geocoder.search(zip_with_country).each do |result|
      if result.postal_code.present? and zip_with_country.include? result.postal_code or result.data['types'].include? 'postal_code'
        valid_zip = true
      end
    end
    valid_zip
  end

  def self.search(address, country = nil, country_code = nil )
    # if no results are found, the result will look like
    # ```
    # {"address_components"=>[{"long_name"=>"United States", "short_name"=>"US", "types"=>["country", "political"]}],
    #  "formatted_address"=>"United States",
    #  "geometry"=>
    #   {"bounds"=>{"northeast"=>{"lat"=>71.3867745, "lng"=>-66.9502861}, "southwest"=>{"lat"=>18.9106768, "lng"=>172.4458955}},
    #    "location"=>{"lat"=>37.09024, "lng"=>-95.712891},
    #    "location_type"=>"APPROXIMATE",
    #    "viewport"=>{"northeast"=>{"lat"=>49.38, "lng"=>-66.94}, "southwest"=>{"lat"=>25.82, "lng"=>-124.39}}},
    #  "partial_match"=>true,
    #  "place_id"=>"ChIJCzYy5IS16lQRQrfeQ5K5Oxw",
    #  "types"=>["country", "political"]}
    #  ```
    #  and we should treat this as no results
    results = Geocoder.search(address, params: { components: "country: #{country}" })
    # results = Geocoder.search("#{address},#{country}")
    # return [] if results.first.try(:formatted_address).downcase == country.downcase
    results
  end
end
