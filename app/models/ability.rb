class Ability
  include CanCan::Ability

  # Encapsulates all logic for authorizations in app. Devise handles authentication; CanCan handles authorization.
  # Example: Once a user has logged in, what operations are they allowed to perform and on which models. Can the
  # current user delete this asset? etc.

  def initialize(user)
    user ||= User.new

    alias_action :create, :read, :update, :destroy, to: :crud

    ## START: Admin
    if user.admin?
      can :manage, :all
      cannot :destroy, Company
      can :destroy, Company do |company|
        company.can_be_destroyed?
      end
      cannot :invite, User
      return
    end
    ## END: Admin

    # Log in is used to trigger a login.
    can :log_in, Asset unless user.new_record?

    ## START: Companies
    if user.private?
      can :read, Company, id: user.company_id
      can :update, Company, id: user.company_id if user.company_admin?
      can :print_qr, Company, id: user.company_id if user.company_admin?
      can :generate_printed_qr_codes, Company, id: user.company_id if user.company_admin?
      can :qr_code_status, Company, id: user.company_id if user.company_admin?
      can :download_printed_qr_codes, Company, id: user.company_id if user.company_admin?
      can :email_printed_qr_codes, Company, id: user.company_id if user.company_admin?
    end
    ## END: Companies

    ## START: Users
    if user.private?
      can :crud, User, company_id: user.company_id if user.company_admin?
      can :show, User,  company_id: user.company_id
      can :resend_invitation, Company
      cannot :destroy, User, id: user.id
      cannot :destroy, User, id: user.company.contact_id unless user.company.nil?
    else
      can :destroy, User
    end

    if user.company_admin? || user.project_assignments.project_admin.any?
      can :invite, User, company_id: user.company_id
      can [:new, :create, :reinvite_user] ,User ,company_id: user.company_id
    end
    ## END: Users

    ## START: Projects
    if user.private?
      can :create, Project, company_id: user.company_id if user.company_admin? || user.field_member?
      can :upload_multiple, Project, company_id: user.company_id if user.company_admin? || user.field_member?
      can :upload_multiple, Project, project_assignments: { user_id: user.id, role: 'project_admin' }
      can :read, Project, company_id: user.company_id
      can :export, Project, company_id: user.company_id
      can :search_by_name, Project, company_id: user.company_id
      if user.company_admin?
        can [:update, :destroy,:reverse_geocode_map], Project, company_id: user.company_id
      else
        can [:update, :destroy], Project, project_assignments: { user_id: user.id, role: 'project_admin' }
        can :update, Project, company_id: user.company_id if user.field_member?
      end
    end
    ## END: Projects

    ## START: Groups
    # @TODO We need to redefine group roles if neccessary
    if user.private?
      can :create, Group, company_id: user.company_id if user.company_admin? or user.field_member? or user.can_assign_projects?
      can :upload_multiple, Group, company_id: user.company_id if user.company_admin? || user.field_member?
      can :read, Group, company_id: user.company_id
      can :export, Group, company_id: user.company_id
      can :mark_as_closed, Group, company_id: user.company_id
      if user.company_admin? or user.field_member?
        can [:update, :destroy, :update_multiple], Group, company_id: user.company_id
      elsif user.can_assign_projects?
        can [:update, :destroy], Group, company_id: user.company_id , project_id: user.projects.ids
      else
        # can [:update, :destroy], Group, project_assignments: { user_id: user.id, role: 'project_admin' }
        can :update, Group, company_id: user.company_id if user.field_member?
      end
    end
    ## END: Groups

    ## START: ProjectAssignments
    if user.company_admin?
      can :manage, ProjectAssignment, project: { company_id: user.company_id }
      can :manage, ProjectAssignment, user: { company_id: user.company_id }
    elsif user.field_member?
      can :manage, ProjectAssignment, project: { company_id: user.company_id }
    else
      can :manage, ProjectAssignment, project: { project_assignments: { user_id: user.id, role: 'project_admin' } }
    end
    ## END: ProjectAssignments

    ## START: CsvImport
    if user.company_admin? || user.field_member?
      can :create, CsvImport
    else
      can :create, CsvImport ,project: { project_assignments: { user_id: user.id, role: 'project_user' }}
      can :create, CsvImport ,project: { project_assignments: { user_id: user.id, role: 'project_admin' }}

    end
    can :read, CsvImport, user_id: user.id
    can :read, CsvImport, company_id: user.company_id
    can :template, CsvImport
    ## END: CsvImport

    ## START: Assets
    if user.private?
      can :create, Asset, company_id: user.company_id if user.company_admin? || user.project_assigned? || user.field_member?
    else
      can :create, Asset, company_id: nil
    end

    can :read, Asset, creator_id: user.id
    can :read, Asset, company_id: user.company_id
    can :asset, Asset , company_id: user.company_id
    can :findit, Asset, company_id: user.company_id
    can :checklist, Asset, company_id: user.company_id
    can :read, Asset, public: true, owned: true, rental: false # for sale
    can :read, Asset, public: true, owned: true, rental: true # for rent
    can :read, Asset, public: false, owned: true, rental: true # for rent

    can :manage_inventory, Asset
    can :move_inventory, Asset
    can :inventory_assets, Asset
    can :update_quantity, Asset

    if user.private?
      if user.company_admin?
        can [:update, :destroy], Asset, company_id: user.company_id
      elsif user.field_member?
        # Field member can update all assets in the company with limited access
        can :update, Asset, company_id: user.company_id
      else
        # user is project admin, or is in project plus created asset
        can [:update, :destroy], Asset, project: { project_assignments: { user_id: user.id, role: 'project_admin' }}
        can [:update, :destroy], Asset, project: { project_assignments: { user_id: user.id}}, creator_id: user.id
      end
    else
      can [:update, :destroy], Asset, creator_id: user.id
    end

    if user.private?
      if user.company_admin?
        can :toggle_marketplace, Asset, company_id: user.company_id
      else
        can :toggle_marketplace, Asset, company_id: user.company_id, project: { project_assignments: { user_id: user.id, role: 'project_admin' }}
      end
    else
      # allows things like "mark as sold"
      can :toggle_marketplace, Asset, creator_id: user.id
    end
    ## END: Assets

    ## START: Safety Reports
    if user.company_admin?
      can :safety_reports_subscription, :safety_report
    end
    ## END: Safety Reports

    ## START: Wanted Assets
    can :create, WantedAsset, company_id: user.company_id
    can :read, WantedAsset, creator_id: user.id
    can :read, WantedAsset, company_id: user.company_id

    if user.private?
      if user.company_admin?
        can [:fulfill, :update, :destroy], WantedAsset, company_id: user.company_id
      else
        # user is project admin, or is in project plus created asset
        can [:fulfill, :update, :destroy], WantedAsset, creator_id: user.id
        can [:fulfill, :update, :destroy], WantedAsset, contact_id: user.id
      end
    end
    ## END: Wanted Assets

    ## START: Marks
    can :manage, Mark, marker: user
    ## END: Marks

    ## START: GeoFences
    if user.private?
      if user.company_admin?
        can :manage, GeoFence, company_id: user.company_id
        can :manage, Gauge
      else
        can :read, GeoFence, company_id: user.company_id
        can :read, Gauge
      end
    end
    ## END: GeoFences

    can :flag, Asset
  end

end
