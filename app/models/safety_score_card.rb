class SafetyScoreCard
  attr_accessor :calculated_score, :total_violations, :start_date, :end_date, :generated_at


  def initialize(attributes={})
    ## Check For Configurations
    safety_configs = attributes.delete(:safety_configs)
    raise StandardError.new('Please set up configuration for your company') if safety_configs.blank?

    # Any Attributes Sent will need to be initialized
    attributes.each do |name, value|
      send("#{name}=", value)
    end

    # Will initialize only attributes that have been set up in company safety Config
    safety_configs.each do |saf|
      gauge_type = saf.gauge.safety_indexify_name
      self.class.send(:attr_accessor, gauge_type.intern)
      send("#{gauge_type}=", {'violations_count' => 0, 'score' => 0})
    end

    @start_date ||= CompanyScoreCard::DEFAULT_VALUES[:end_date]
    @end_date ||= CompanyScoreCard::DEFAULT_VALUES[:start_date]
    @calculated_score ||= 100

    @generated_at = Time.now
    @total_violations = 0
  end

  def execute(safety_configs)
    search_params = self.search_params.merge({gauge_id: safety_configs.collect(&:gauge_id)})
    alert_logs = GaugeAlertLogSearch.new(search_params).search()
    grouped_logs = alert_logs.group_by(&:gauge_id)
    grouped_configs = safety_configs.group_by(&:score_type)
    ## Make Deductions
    deductions = grouped_configs['deduction']
    if deductions
      deductions.each do |config|
        relevant_alerts = grouped_logs[config.gauge_id.to_s]
        max_score_for_gauge = config.score_weight
        gauge_name = config.gauge.safety_indexify_name
        self.send(gauge_name)['score'] = max_score_for_gauge
        next if relevant_alerts.blank?
        total_viol = relevant_alerts.size


        op = '-'
        max_score_for_gauge = config.score_weight

        ## Getters
        deductions_array = relevant_alerts.collect{|al| config.send(al.gauge_condition_severe_level) || 0 }
        total_deduction = deductions_array.inject(&:+)
        calculated_score = eval("#{max_score_for_gauge} #{op} #{total_deduction}")

        ## Reset to zero if negative
        calculated_score = 0 if calculated_score < 0
        ## Setters
        self.send(gauge_name)['score'] = calculated_score
        self.send(gauge_name)['violations_count'] = total_viol

        self.total_violations += total_viol

      end
    end

    ## Calc Credits

    credits = grouped_configs['credit']
    zero_violations = (self.total_violations == 0)

    if credits
      credits.each do |config|
        relevant_alerts = grouped_logs[config.gauge_id.to_s]
        max_credit_for_gauge = config.score_weight
        gauge_name = config.gauge.safety_indexify_name

        self.send(gauge_name)['score'] = 0
        next if relevant_alerts.blank?
        total_alerts = relevant_alerts.size
        max_credit_for_gauge = config.score_weight

        ## Getters
        credits_array = relevant_alerts.collect{|al| zero_violations ? config.send("#{al.gauge_condition_severe_level}_no_violations") : config.send(al.gauge_condition_severe_level) }
        total_credit = credits_array.inject(&:+)
        calculated_score = (total_credit > max_credit_for_gauge) ? max_credit_for_gauge : total_credit

        ## Setters
        self.send(gauge_name)['score'] = calculated_score
        self.send(gauge_name)['violations_count'] = total_alerts
      end
    end

    self.calculated_score = safety_configs.map{|r| self.send(r.gauge.safety_indexify_name)['score'] }.inject(&:+)
    return self
  end
end
