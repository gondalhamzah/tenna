class GroupSearch
  include ActiveModel::Model
  attr_accessor :group_filter_search, :user, :company, :project

  # This class is used to generate an Elasticsearch query that represents the filter, search & sort criteria input
  # from the user

  PAGE_SIZES = [25, 50, 100]

  FULL_TEXT_QUERY_FIELDS = [
    'group.name',
    'group.group_num',
    'group.client',
  ]

  def self.sorts()
    {
      date_posted: {created_at: :desc},
      name: {name_not_analyzed: :asc},
    }
  end

  def self.search(group_filter_search, user, company: nil, project: nil)
    new(
      group_filter_search: group_filter_search,
      user: user,
      company: company,
      project: project
    ).search
  end

  def search
    apply_filters
      .filter(in_company)
      .filter(in_project)
      .filter{ !deleted_at }
      .query(full_text_query)
  end

  def apply_filters
    groups = GroupsIndex::Group
    statues = []
    statues.push('active') if bool_value(group_filter_search.is_active)
    statues.push('closed') if bool_value(group_filter_search.is_closed)
    groups = status_filter(statues, groups) if statues.length > 0
    groups = default_project_filter(groups) if bool_value(group_filter_search.default_project)
    groups = groups.filter{state!='closed'} if !bool_value(group_filter_search.is_closed)
    groups
  end

  private

  def status_filter statues, groups
    groups.filter{ state == statues }
  end

  def default_project_filter groups
    return groups if company.project.blank?
    groups = groups.filter({
      term: {
        project_id: company.project.id
      }
    })
  end

  def in_project
    return {} if group_filter_search.project.blank? && project.blank?
    {
      term: {
        project_id: group_filter_search.project || project.id
      }
    }
  end

  def in_company
    return {} if company.blank?
    {
      term: {
        company_id: company.id
      }
    }
  end

  def full_text_query
    if group_filter_search.query.present?
      {
        simple_query_string: {
          query: group_filter_search.query+"*",
          analyze_wildcard: true,
          fields: FULL_TEXT_QUERY_FIELDS
        }
      }
    else
      {
        match_all: {}
      }
    end
  end

  def bool_value value
    value.in?([true, 1, '1', 't', 'true'])
  end
end
