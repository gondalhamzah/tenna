class Asset < ActiveRecord::Base
  extend FriendlyId
  has_paper_trail class_name: 'AssetVersion',
    only: [:price, :project_id, :status, :public, :hours, :miles, :quantity, :current_lat, :current_lng]

  after_create :trigger_hooks_asset_create unless Rails.env.test?
  after_save :tracking_info_update

  attr_accessor :increment_reallocation_count,:country_name

  acts_as_taggable
  enum condition: %w(excellent good fair poor)

  state_machine :state, :initial => :available do
    after_transition any => [:sold, :deleted] do |asset, transition|
      #asset.tracking_code.try(:unassign)
    end

    event :expire do
      transition available: :expired
    end

    event :sell do
      transition [:available, :expired] => :sold
    end

    event :soft_delete do
      transition all => :deleted
    end
  end

  scope :available, -> { where("state = 'available' AND ((expires_on > current_date AND public=TRUE) or public=FALSE)")}
  scope :not_sold_deleted, -> { where.not(state: ['sold', 'deleted'])}
  scope :is_inventory, -> { where(is_inventory: true) }

  include Expirable # so it's not overridden by enum status
  include Markable
  include AssetKind
  include Versionable
  include Tokenable

  TYPES = %w[Equipment Material]
  RENT_UNITS = %w[day week month]
  DURABILITY = %w[permanent temporary]

  belongs_to :company
  belongs_to :creator, class_name: 'User'
  belongs_to :project
  belongs_to :category
  belongs_to :zip_code
  belongs_to :group
  belongs_to :current_driver, class_name: 'User'

  has_many :tracking_codes, as: :resource
  has_many :assets_versions

  has_many :pdfs, dependent: :destroy
  has_many :photos, dependent: :destroy
  has_many :list_items
  has_many :gauge_alert_groups, dependent: :destroy

  accepts_nested_attributes_for :pdfs, allow_destroy: true,
                                reject_if: proc { |attributes| attributes['file'].blank? && attributes['file_cache'].blank? }
  accepts_nested_attributes_for :photos, allow_destroy: true,
                                reject_if: proc { |attributes| attributes['file'].blank? && attributes['file_cache'].blank? &&
                                                              attributes['position'].blank?}
  accepts_nested_attributes_for :tracking_codes

  default_scope { with_active_company }

  delegate :lat, :lng, to: :zip_code, prefix: false, allow_nil: true

  validates :pdfs, nested_association_file_size: {maximum: 25.megabytes}
  validates :pdfs, length: { maximum: 2, :message => "Must be 2 files per upload" }, if: :pdfs_limit?

  #validates :photos, nested_association_length: {maximum: 4}, nested_association_file_size: {maximum: 20.megabytes}

  validates :photos, nested_association_file_size: {maximum: 4.megabytes}

  validates :type, :title, :creator_id, :category_id, :zip_code, :location_zip, :expires_on, :quantity, presence: true
  validates :year, :hours, :miles, :condition, absence: true, if: :material?
  #validates :make, presence: true, if: :equipment?
  validates :units, inclusion: { in: RENT_UNITS }, allow_blank: true, if: Proc.new{|a| !a.owned && !a.public && a.rental}
  validates :certified, inclusion: {in: [false], message: 'materials can not be certified'}, if: :material?
  validates_length_of  :time_length, maximum: 10
  validates :time_length, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 10, only_integer: true, message: 'Max time length can be 10' },
            if: Proc.new{ |a| ['rental', 'for_rent', 'for_sale'].include?(a.asset_kind) and a.time_length.nil? }
  validates :company_id, presence: true, if: 'creator.present? && creator.private?'
  validates :project_id, presence: true, if: 'creator.present? && creator.private? && creator.contact_projects.available.present?'
  validates :project_id, absence: true, if: 'creator.present? && creator.public_user?'
  validates :zip_code, associated: true
  # validate :project_must_include_creator
  validate :project_must_be_in_same_company

  validates :price, numericality: { greater_than_or_equal_to: 1, less_than: 999999, only_integer: true, message: 'Max price can be 999999' },
    if: Proc.new{ |a| ['rental', 'for_rent', 'for_sale'].include?(a.asset_kind) and a.price.present? }
  validates :quantity, :market_value, numericality: { greater_than_or_equal_to: 0 }

  # validates :asset_num, numericality: { only_integer: true }, allow_blank: TRUE
  # We used to use asset_num as unique value on assets to enable users to update assets via CSV import,
  # but were asked to remove this feature. Leaving as comment in case of future requirement.
  # validates :asset_num, uniqueness: {scope: :company}, allow_blank: true, if: 'company.present?'
  # validates :asset_num, uniqueness: {scope: :creator}, allow_blank: true, if: 'company.blank?'

  validates :rental, inclusion: {in: [true, false]}

  # Gives assets "friendly" URLs, in addition to ID-based
  friendly_id :slug_candidates, use: [:slugged, :finders]

  # Update Elasticsearch index on asset save
  update_index('assets#asset') { self }

  before_validation :cleanup_on_asset_conversion
  before_update :update_reallocation_count
  before_save :update_maintenance_date

  def tracking_code= value
    self.versions.create({
      event: "update",
      whodunnit: PaperTrail.whodunnit,
      object: self.attributes.to_yaml
    })
    value.update(resource: self)
    AssetsIndex::Asset.update_index(self)
  end

  def self.active
    where(arel_table[:expires_on].gteq(Time.zone.today)).where(state: :available)
  end

  def calculate_daily_cost
    if self.price && self.units && ['for_rent', 'rental'].include?(self.asset_kind)
      case units
        when 'day'
          self.price
        when 'week'
          self.price/7.0
        when 'month'
          self.price/30.0
      end
    end
  end

  def total_rental_asset
    (self.price || 0) * (self.time_length || 0)
  end

  def thirty_day_rental_cost
    calculate_daily_cost && (calculate_daily_cost * 30)
  end

  def self.creators_with_expiring_assets
    Asset
        .includes(:creator)
        .expiring_in(Asset::EXPIRE_THRESHOLD)
        .find_each
        .group_by(&:creator)
  end

  def self.expiring_in(days_from_today)
    if !days_from_today.is_a?(ActiveSupport::Duration)
      raise ArgumentError, 'Argument is not ActiveSupport::Duration'
    end
    where(expires_on: Time.zone.today + days_from_today)
  end

  def self.public(state)
    where(public: state)
  end

  def self.company_relation(user, company_relation)
    case company_relation
    when :owner
      my_assets(user)
    when :other
      not_my_assets(user)
    end
  end

  def photo_url
    image_src = self.photos.any? ? self.photos[0].file.thumb.url : '/img-no-image-list-view.jpg'

    if Rails.env.development?
      begin
        File.open(Dir.pwd + '/public' + image_src)
      rescue
        image_src = '/uploads/tmp/128x128.jpg'
      end
    end

    image_src
  end

  def self.map_markers(public, user, company_relation)
    sql = select('COUNT(*), "zip_codes"."zip_code", "zip_codes"."lat", "zip_codes"."lng", "title", "assets"."id"')
              .public(public)
              .company_relation(user, company_relation)
              .active
              .joins(:zip_code)
              .group('"zip_codes"."zip_code", "zip_codes"."lat", "zip_codes"."lng", "title", "assets"."id"')
              .to_sql
    connection.execute(sql).map do |marker|
      marker[:icon] = 'icn-buildsourced-public-location-pin.png'
      ActiveSupport::HashWithIndifferentAccess.new(marker)
    end
  end

  def self.with_active_company
    company_join = 'LEFT OUTER JOIN "companies" ON "companies"."id" = "assets"."company_id"'
    active_company = %{"companies"."active" = 't' OR "companies"."id" IS NULL}
    joins(company_join).where(active_company)
  end

  def self.my_assets_sold(user)
    my_assets(user).where(state: :sold)
  end

  def self.my_asset_reallocation_count(user)
    my_assets(user).sum(:reallocation_count)
  end

  def self.my_asset_views_count(user)
    my_assets(user).sum(:view_count)
  end

  # user's asset in terms of "ownership"
  # such that we're private and have the same company
  # or public (not private) and we're the same as the creator
  def self.my_assets(user)
    if user.private?
      where(company: user.company)
    else
      where(creator: user)
    end
  end

  def self.not_my_assets(user)
    if user.private?
      where('"assets"."company_id" != ? OR "assets"."company_id" IS ?', user.company.id, nil)
    else
      where.not(creator: user)
    end
  end

  def project_assignments
    project.try(:project_assignments)
  end

  def group_id
    group.try(:id)
  end

  def active?
    expires_on >= Time.zone.today && state == 'available'
  end

  def available?
    active? && (in_use_until == nil || Time.zone.today > in_use_until)
  end

  def closing_out?
    available? && project_id.present? && project.closeout_date.past?
  end

  def expired?
    expires_on.past?
  end

  def expiring_within?(days_from_today)
    unless days_from_today.is_a?(ActiveSupport::Duration)
      raise ArgumentError, 'Argument is not ActiveSupport::Duration'
    end
    available? && expires_on <= Time.zone.today + days_from_today
  end

  def material?
    type == 'Material'
  end

  def equipment?
    type == 'Equipment'
  end

  def project_must_be_in_same_company
    if company.present? && project.present? && project.company != company
      errors.add(:project_id, 'is not in company')
    end
  end

  def project_name
    if company.present? && project.present? && project.company == company
      project.name
    end
  end

  def location
    zip_code.location
  end

  def location_zip
    zip_code.zip_code if zip_code_id.present?
  end

  def location_zip=(value)
    zip_code = ZipCode.find_by_zip_code(value)
    if zip_code
      self.zip_code = zip_code
    elsif !value.nil?
      new_zip_code = ZipCode.new(zip_code: value)
      new_zip_code.save(validate: false)
      self.zip_code = new_zip_code
    end
  end

  def country_name=(value)
    if self.zip_code_id
      zip_code_obj = ZipCode.where("zip_code = ? AND (country = ?  OR country is null)", self.zip_code.zip_code, value).first
      if zip_code_obj
        if zip_code.country.nil?
          zip_code_obj.update(country: value)
        end
        self.zip_code = zip_code_obj
      end
      if !zip_code_obj
        self.zip_code = ZipCode.create(zip_code: self.zip_code.zip_code, country: value)
      end
    end
  end

  def country_name
    zip_code.country if zip_code_id.present?
  end

  def has_current_coordinates?
    current_lng.present? and current_lng.present?
  end

  def coordinate_in_zip_code?
    Geocoder.search("#{current_lat},#{current_lng}")[0].try(:postal_code) == location_zip
  end

  def contact
    if project.present?
      project.contact
    elsif company.present?
      company.contact
    else
      creator
    end
  end

  # Absolute URL to asset, so we can share link to asset
  def url
    options = Rails.application.config.action_mailer.default_url_options
    "http://#{options[:host]}/assets/#{friendly_id}"
  end

  def slug_candidates
    [
      :title,
      [:id, :title]
    ]
  end

  def sold?
    self.state == 'sold'
  end

  def rental_check?
    !(self.owned && self.public) && self.rental ? true : false
  end

  def flag_as_inappropriate
    update(flagged_as_inappropriate: true)
    AdminNotifications.flagged_asset(self).deliver
  end

  after_initialize do |asset|
    asset.quantity ||= 1
    asset.condition ||= 'good'
  end

  def cleanup_on_asset_conversion
    return unless self.material?
    self.certified = false
    self.make, self.model, self.condition, self.year, self.hours, self.miles = nil
  end

  def update_reallocation_count
    return unless self.project_id_changed? && self.increment_reallocation_count == 'Yes'
    self.reallocation_count += 1

    # Always reassign the creator to the new project's contact.
    # This way, there is no chance the creator won't be part of the new project.
    self.creator = self.project.contact unless self.project.nil?
    self.location_zip = self.project.address.zip
  end

  def last_25_versions
    self.versions.last(25)
  end

  def has_media
    self.photos.count > 0
  end

  def get_address_by_lat_lng
    geo = Geocoder.search("#{self.current_lat},#{self.current_lng}")
    if geo && geo.first
      address = geo.first.address

      if self.accuracy.nil?
        "#{address} (Accuracy: N/A)"
      else
        accuracy = self.accuracy.to_f/1609.344
        "#{address} (Accuracy: #{'%.2f' % accuracy} miles)"
      end
    end
  end

  def project_qr_code_same_location?
    if has_tracking_code? && self.project.present?
      # if distance between project location and QR code location is less than 200 km, regard as same place
      Geocoder::Calculations.distance_between([self.project.lat, self.project.lng], [self.current_lat, self.current_lng]) < 8
    end
  end

  def has_top_left_content?
    self.asset_num.present? || self.type == 'Material' || self.description?
  end

  def token
  end

  def token_type
  end

  def trigger_hooks_asset_create
    Hook.trigger_new_asset('Asset_Create', self)
  end

  def simple_json
    {
      id: self.id,
      category_group_id: self.try(:category).try(:category_group_id),
      category_id: self.category_id,
      title: self.title
    }
  end

  def self.units_valid_for_mqtt
    ["Category Id", "Hours", "Miles", "Price", "Notes", "Project Id", "Status", "Asset Num", "Group Id", "Market Value", "Maintenance", "Quantity"]
  end

  def tracking_info_update
    fields = ['category_id', 'hours', 'miles', 'current_lat', 'current_lng', 'accuracy', 'price', 'notes', 'project_id', 'group_id', 'market_value', 'quantity']
    if self.changed? and !self.changes.include? 'view_count'
      av = AssetsVersion.new
      av.asset_id = self.id
      av.field_name = 'tracking_info'
      av.project_id = self.project_id
      av.company_id = self.company_id
      av.whodunnit = PaperTrail.whodunnit
      tracking_info = []
      timestamp = Time.now.utc.strftime(I18n.t('time.formats.elasticsearch'))
      self.changes.except(:updated_at).keys.each do |key|
        if fields.include? key
          title_key = key.gsub('_',' ').titleize
          if key == 'maintenance'
            if self.changes[key][1]
              tracking = { 'unit': title_key, 'value_boolean': true, 'description': 'Asset '+ title_key +' requested.', 'type': title_key + ' Update.', 'timestamp': timestamp }
            else
              tracking = { 'unit': title_key, 'value_boolean': false, 'description': 'Asset '+ title_key +' completed.', 'type': title_key + ' Update.', 'timestamp': timestamp }
            end
          elsif ["status", "notes", "asset_num"].include? key
            tracking = { 'unit': title_key, value_string: self.changes[key][1], 'description': 'Asset '+ title_key +' changed.', 'type': title_key + ' Update.', 'timestamp': timestamp } if self.changes[key][1].present?
          else
            tracking = { 'unit': title_key, 'value_numeric': self.changes[key][1], 'description': 'Asset '+ title_key +' changed.', 'type': title_key + ' Update.', 'timestamp': timestamp } if self.changes[key][1].present?
          end
          tracking_info << tracking
        end
      end
      current_lat = self.lat
      current_lng = self.lng

      tracking = { 'unit': 'Geo Coordinates', 'description': 'Precise latitude and longitude', 'value_object': { 'latitude': current_lat, 'longitude': current_lng }, 'type': 'Location', 'timestamp': timestamp }
      tracking_info << tracking
      data = {array: tracking_info}
      av.tracking_info = data
      av.save
    end
  end

  def all_included_geofences
    res = []
    company.geo_fences.each do |g|
      if g.contains? current_lat, current_lng
        res << {id: g.id, name: g.name}
      end
    end
    res
  end

  private

  def update_maintenance_date
    if self.maintenance_changed?
      self.maintenance_date = DateTime.now
    end
  end

  def pdfs_limit?
    (pdfs.size - self.pdfs.count) > 2
  end
end
