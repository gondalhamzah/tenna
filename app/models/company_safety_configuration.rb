class CompanySafetyConfiguration < ActiveRecord::Base
  belongs_to :gauge
  belongs_to :company

  enum score_type: [:deduction, :credit]

  validate :weight_score_to_hundred

  validates_uniqueness_of :gauge_id, conditions: -> { where( company_id: User.current.company_id ) } if User.current
  validates_presence_of :score_type

  def score_operator
    case score_type
      when 'deduction'
        return '-'
      when 'credit'
        return '+'
      else
        return ''
    end
  end


  def self.score_fields
    ['high_points', 'medium_points', 'low_points']
  end

  def self.deduction_sum_for_company(company_id)
    CompanySafetyConfiguration.where(company_id: company_id, score_type: 'deduction').sum(:score_weight).to_i
  end

  def self.credit_sum_for_company(company_id)
    CompanySafetyConfiguration.where(company_id: company_id, score_type: 'credit').sum(:score_weight).to_i
  end

  def weight_score_to_hundred
    ## Weight Already in the database
    weight_sum =  CompanySafetyConfiguration.deduction_sum_for_company(self.company_id)
    already_over_hundered = weight_sum >= 100
    # If New Record
    if new_record?
      weight_sum = weight_sum + self.score_weight
    else
      if score_weight_changed?
        if self.score_weight_was < self.score_weight
          # If New score weight is greater then add the difference
          weight_sum = weight_sum + (self.score_weight - self.score_weight_was )
        elsif self.score_weight_was > self.score_weight
          # If New SCore weight is lesser then subtract the difference
          weight_sum = weight_sum - (self.score_weight_was - self.score_weight)
        end
      end
    end

    if  !already_over_hundered && (score_operator == '-' && weight_sum > 100)
      self.errors.add(:score_weight, "Weight Scores Sum for company cannot exceed 100 : Adding up to #{weight_sum} " )
    elsif weight_sum < 0
      self.errors.add(:score_weight,'Weight Scores Sum for company cannot be less than 0')
    elsif already_over_hundered
      self.errors.add(:score_weight,'Weight Scores Sum already add up to 100')
    end
  end


end
