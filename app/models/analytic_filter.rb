class AnalyticFilter < ActiveRecord::Base
  belongs_to :company
  belongs_to :user
  belongs_to :analytic_filter_label

  validates :name, presence: {message: "Filter name shouldn't be empty"}, uniqueness: { scope: :company, message: 'Filter name should be unique' }
  validates :analytic_filter_label_id, presence: {message: "Filter label shouldn't be empty"}
end
