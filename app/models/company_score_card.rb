## EXAMPLES
# All of the data for a company
# CompanyScoreCard.new(company: Company.find({company_id})).execute_report
# Filter By Driver
# CompanyScoreCard.new(company: Company.find({company_id}), operator_id: [{operator_id}]).execute_report
# List by Vehicle
# CompanyScoreCard.new(company: Company.find({company_id}), asset: [{asset_id}]).execute_report
class CompanyScoreCard

  attr_accessor :company,
                :average_score_by_driver,
                :average_score_by_vehicle,
                :average_violations_by_driver,
                :average_violations_by_vehicle,
                :driver_scorecards,
                :vehicle_scorecards,
                :asset_id,
                :operator_id,
                :scorecard_type,
                :deduction_sum,
                :credit_sum,
                :start_date, :end_date, :generated_at

  DEFAULT_VALUES = {start_date: 100.days.ago.to_time, end_date: Time.now, generated_at: Time.now}
  DEFAULT_SEARCH_PARAMS = {created_at: (DEFAULT_VALUES[:start_date]..DEFAULT_VALUES[:end_date])}

  def initialize(attributes={})
    attributes = DEFAULT_VALUES.merge(attributes)
    attributes.each do |name, value|
      send("#{name}=", value)
    end

    @start_date ||= DEFAULT_VALUES[:start_date]
    @end_date ||= DEFAULT_VALUES[:end_date]

    if @company
      @safety_configs = CompanySafetyConfiguration.includes(:gauge).where(company_id: company.id)

      @safety_configs.each do |saf|
        gauge_type = saf.gauge.safety_indexify_name
        self.class.send(:attr_accessor, gauge_type.intern)
        send("#{gauge_type.intern}=", {'average_score_by_driver' => 0, 'average_score_by_vehicle' => 0, 'average_violations_by_driver' => 0, 'average_violations_by_vehicle' => 0})
      end

      @deduction_sum = CompanySafetyConfiguration.deduction_sum_for_company(self.company.id)
      @credit_sum = CompanySafetyConfiguration.credit_sum_for_company(self.company.id)
      @average_score_by_driver = 100
      @average_score_by_vehicle = 100
      @scorecard_type ||= 'Driver'
      @operator_id ||= []
      @asset_id ||= []
      if @scorecard_type == 'Driver' && @operator_id.blank?
        ## Get All users
        @operator_id = @company.users.not_deleted.pluck(:id)
      elsif @scorecard_type == 'Vehicle' && @asset_id.blank?
        ## Get All Assets
        @asset_id = @company.equipments.available.pluck(:id)
      end

      initialize_vehicle_scorecards
      initialize_driver_score_cards
    else
      raise ArgumentError.new('Need to provide a company to filter from')
    end
  end

  # def self.scorecard_types
  #   ['Driver', 'Vehicle']
  # end

  def assets
    if ( self.respond_to?(:asset_id) && !self.asset_id.blank?)
      return Asset.find(asset_id)
    else
      []
    end
  end


  def users
    if ( self.respond_to?(:operator_id) && !self.operator_id.blank? )
      return User.find(operator_id)
    else
      []
    end
  end

  def search_params
    {company_id: company.id, created_at: (self.start_date.to_datetime..self.end_date.to_datetime)}
  end

  def initialize_vehicle_scorecards
    @vehicle_scorecards = []
    if scorecard_type == 'Vehicle' && !assets.blank?
      assets.each do |asset|
        @vehicle_scorecards << VehicleScoreCard.new(asset: asset,
                                                    safety_configs: @safety_configs,
                                                    start_date: self.start_date,
                                                    end_date: self.end_date,
                                                    calculated_score: self.deduction_sum)
      end
    end
  end

  def initialize_driver_score_cards
    @driver_scorecards = []
    if scorecard_type == 'Driver' && !users.blank?
      users.each do |operator|
        @driver_scorecards << DriverScoreCard.new(driver: operator,
                                                  safety_configs: @safety_configs,
                                                  start_date: self.start_date,
                                                  end_date: self.end_date,
                                                  calculated_score: self.deduction_sum )
      end
    end
  end


  def execute_report(safety_configs=@safety_configs)
      driver_scorecards.each do |driver_scorecard|
        driver_scorecard.execute(@safety_configs)
      end

      vehicle_scorecards.each do |vehicle_scorecard|
        vehicle_scorecard.execute(@safety_configs)
      end

      ### SET AVERAGE SCORES
      drivers_scores = driver_scorecards.collect(&:calculated_score)
      self.average_score_by_driver = drivers_scores.inject{ |sum, el| sum + el }.to_f / drivers_scores.size

      vehicle_scores = vehicle_scorecards.collect(&:calculated_score)
      self.average_score_by_vehicle = vehicle_scores.inject{ |sum, el| sum + el }.to_f / vehicle_scores.size

      ### SET AVERAGE VIOLATIONS
      drivers_violations = driver_scorecards.collect(&:total_violations)
      self.average_violations_by_driver = drivers_violations.inject{ |sum, el| sum + el }.to_f / drivers_violations.size

      vehicle_violations = vehicle_scorecards.collect(&:total_violations)
      self.average_violations_by_vehicle = vehicle_violations.inject{ |sum, el| sum + el }.to_f / vehicle_violations.size

      ## Set Gauge averages
      safety_configs.each do |safety_config|
        gauge_key = safety_config.gauge.safety_indexify_name

        ### SCORES
        gauge_scores_for_vehicle = vehicle_scorecards.collect{|r| r.send(gauge_key)['score']}.compact
        gauge_scores_for_driver = driver_scorecards.collect{|r| r.send(gauge_key)['score']}.compact
        send(gauge_key)['average_score_by_driver'] = gauge_scores_for_driver.inject{ |sum, el| sum + el }.to_f / gauge_scores_for_driver.size
        send(gauge_key)['average_score_by_vehicle'] = gauge_scores_for_vehicle.inject{ |sum, el| sum + el }.to_f / gauge_scores_for_vehicle.size

        ### Violations
        gauge_violations_for_vehicle = vehicle_scorecards.collect{|r| r.send(gauge_key)['violations_count']}.compact
        gauge_violations_for_driver = driver_scorecards.collect{|r| r.send(gauge_key)['violations_count']}.compact
        send(gauge_key)['average_violations_by_driver'] = gauge_violations_for_driver.inject{ |sum, el| sum + el }.to_f / gauge_violations_for_driver.size
        send(gauge_key)['average_violations_by_vehicle'] = gauge_violations_for_vehicle.inject{ |sum, el| sum + el }.to_f / gauge_violations_for_vehicle.size
      end
      self
  end
end
