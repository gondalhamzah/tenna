require 'csv'
require 'zip'
require 'tempfile'

class AssetsVersion < ActiveRecord::Base
  include LiveData
  include LiveDataSpeedLimit
  belongs_to :asset,class_name: 'Asset', foreign_key: 'asset_id'
  belongs_to :wanted_asset,class_name: 'WantedAsset', foreign_key: 'wanted_id'
  # Update Elasticsearch index on AssetsVersion save
  update_index('assets_versions#assets_version') { self }

  AssetReportTemplate = [
      'Asset OR Group',
      'Arrival Date/time',
      'From Project',
      'User',
      'Depart Date/Time',
      'To Project',
      'User',
      'Days on project between the movement'
    ]

  AllAssetTemplate = [
    'Asset Title',
    'Id',
    'Days on project'
  ]

  AssetTemplateWithRentalRate = [
    'Asset Title',
    'Id',
    'Days on project',
    'Internal Rental Rate',
    'Charge to Job'
  ]

  AssetTemplateWithoutJobCost = [
    'Asset Title',
    'Id',
    'Days on project',
    'Internal Rental Rate'
  ]

  def self.asset_values(assets_version,hash=false)
    arrival_obj = assets_version[:arrival_obj]
    departure_obj = assets_version[:departure_obj]
    asset_id = arrival_obj.asset_id

    assets = AssetsIndex::Asset
    asset = assets.filter({ term: {id: asset_id}})
    asset_title = asset.first.title if asset.first
    asset_num = asset.first.asset_num if asset.first
    asset_status = asset.first.status if asset.first

    if asset_status == "available"
      from_project_name = nil
      if arrival_obj.to
        from_project = Project.where(id: arrival_obj.to.to_i)
        from_project_name = from_project.first.name if from_project.first
      end
      if departure_obj
        to_project = Project.where(id: departure_obj.to.to_i)
        to_project_name = to_project.first.name if to_project.first
      end
      from_user_name = arrival_obj.whodunnit_name
      arrival_date = Date.parse(arrival_obj.created_at).strftime("%m/%d/%Y").to_s
      departure_date = nil
      to_user_name = nil
      days_on_project = nil
      if departure_obj
        departure_date = Date.parse(departure_obj.created_at).strftime("%m/%d/%Y").to_s
        to_user_name = departure_obj.whodunnit_name
        days_on_project = (Date.parse(departure_obj.created_at) - Date.parse(arrival_obj.created_at)).to_i
      else
        days_on_project = (DateTime.now - Date.parse(arrival_obj.created_at)).to_i
      end
      if hash
        values = {asset_title: asset_title,
                  asset_id: asset_num,
                  from_project: from_project_name,
                  arrival_date: arrival_date,
                  from_user_name: from_user_name,
                  to_project_name: to_project_name,
                  departure_date: departure_date ,
                  to_user_name: to_user_name,
                  days_on_project: days_on_project
                  }
      else
        values = [asset_title,arrival_date, from_project_name, from_user_name, departure_date, to_project_name, to_user_name, days_on_project]
      end
    else
      if hash
        values = {}
      else
        values = []
      end
    end
    values
  end

  def tracking_token?
    if self.field_name.present?
      if TrackingCode.token_types.include? self.field_name.upcase.split(" ")[0]
        true
      else
        false
      end
    else
      false
    end
  end

  def self.generate_csv_for_project_assets_report(asset_versions_with_total_days_of_assets_on_project,assets_versions,user,project,internal_rental_rate, job_cost)
    asset_versions_csv= CSV.generate do |csv|
        csv << AssetReportTemplate
        assets_versions.each do |assets_version|
            row = []
            values = AssetsVersion.asset_values(assets_version)
            if values.present?
              row << values
              csv << row.flatten
            end
        end
    end

    all_asset_csv= CSV.generate do |csv|
      if internal_rental_rate
        csv << AssetTemplateWithRentalRate if internal_rental_rate && job_cost
        csv << AssetTemplateWithoutJobCost if internal_rental_rate && !job_cost
      else
        csv << AllAssetTemplate
      end
        asset_versions_with_total_days_of_assets_on_project.each do |assets_version|
            row = []
            values_hash = AssetsVersion.asset_versions_with_total_days_of_assets_on_project_values(assets_version,true)
            if values_hash.present? and internal_rental_rate and values_hash[:internal_rate].present?
              charge = values_hash[:days_on_project] * values_hash[:internal_rate]
            else
               charge = 0
            end
            if internal_rental_rate
              values = [values_hash[:asset_title], values_hash[:asset_id], values_hash[:days_on_project],values_hash[:internal_rate],charge] if values_hash.present? && internal_rental_rate && job_cost
              values = [values_hash[:asset_title], values_hash[:asset_id], values_hash[:days_on_project],values_hash[:internal_rate]] if values_hash.present? && internal_rental_rate && !job_cost
            else
              values = [values_hash[:asset_title], values_hash[:asset_id], values_hash[:days_on_project]] if values_hash.present?
            end
            if values.present?
              row << values
              csv << row.flatten
            end
        end
    end
    AssetsVersion.export_assets_report(all_asset_csv,asset_versions_csv,user,project)
  end

  def self.get_latest_reallocation_date(asset)

    assets_versions = AssetsVersionsIndex::AssetsVersion
    assets_versions = assets_versions
                        .filter({ term: {field_name: "project_id"}})
                        .filter{ (project_id == asset.project_id) }
                        .filter{ (asset_id == asset.id) }

    assets_versions = assets_versions.order(created_at: :desc)
    latest_reallocation_date = nil
    if assets_versions.first
      latest_reallocation_date = assets_versions.first.created_at
    else
      project = Project.find asset.project_id
      latest_reallocation_date = project.created_at.to_s if project
    end
    latest_reallocation_date
  end

  def self.export_assets_report(all_asset_csv, asset_versions_csv,user,project)
      temp_file_all_assets = Tempfile.open(['all_assets', '.csv'])
      temp_file_all_assets.write(all_asset_csv)
      temp_file_all_assets.close
      temp_file_filtered_assets = Tempfile.open(['filtered_assets', '.csv'])
      temp_file_filtered_assets.write(asset_versions_csv)
      temp_file_filtered_assets.close
      datestamp = DateTime.now.strftime("%m-%d-%Y")
      path = "/tmp/#{project}-#{user.name}-Assets-#{datestamp}.zip"
      FileUtils.rm(path, force: true)
      Zip::File.open(path, Zip::File::CREATE) do |zipfile|
          zipfile.add "project_reallocated_assets.csv", temp_file_filtered_assets.path
          zipfile.add "project_assets.csv", temp_file_all_assets.path
      end
      path
  end

  def self.asset_versions_with_total_days_of_assets_on_project(asset_versions)
    asset_versions_of_grouped_assets = asset_versions.group_by{|h| h[:arrival_obj].asset_id}.values
    asset_versions_with_total_days_of_assets_on_project_arr = []
    asset_versions_of_grouped_assets.each do |asset_versions_of_grouped_asset|
      no_of_days_on_project = []
      asset_versions_of_grouped_asset.each do |asset_v|
        if asset_v[:departure_obj].present?
          no_of_days_on_project << (Date.parse(asset_v[:departure_obj].created_at) - Date.parse(asset_v[:arrival_obj].created_at)).to_i
        else
          no_of_days_on_project << ((DateTime.now) - Date.parse(asset_v[:arrival_obj].created_at)).to_i
        end
      end
      days_on_project = no_of_days_on_project.compact.sum
      asset_id = asset_versions_of_grouped_asset.first[:arrival_obj].asset_id
      asset_versions_with_total_days_of_assets_on_project_arr << {asset_id:asset_id, no_of_days_on_project:days_on_project}
    end
    asset_versions_with_total_days_of_assets_on_project_arr
  end

  def self.asset_versions_with_total_days_of_assets_on_project_values(asset,hash=false)
    asset_id = asset[:asset_id]
    days_on_project = asset[:no_of_days_on_project]
    assets = AssetsIndex::Asset
    asset = assets.filter({ term: {id: asset_id}})
    asset_title = asset.first.title if asset.first
    asset_num = asset.first.asset_num if asset.first
    internal_rate =  asset.first.internal_daily_rental_rate if  asset.first
    asset_status = asset.first.status if asset.first
    if asset_status == "available"
      if hash
      values = {
                asset_title: asset_title,
                asset_id: asset_num,
                days_on_project: days_on_project,
                internal_rate: internal_rate
                }
      else
        values = [asset_title,asset_num,days_on_project]
      end
    else
      if hash
      values = {}
      else
        values = []
      end
    end
    values
  end
end
