class WantedAsset < ActiveRecord::Base
  include Expirable
  extend FriendlyId

  attr_accessor :country_name,:increment_reallocation_count
  HISTORY_FIELDS = %w{ state project_id contact_id title type }.freeze
  # Gives assets "friendly" URLs, in addition to ID-based
  friendly_id :slug_candidates, use: [:slugged, :finders]

  TYPES = %w[Equipment Material]
  PROIRITIES = %w[High Medium Low]
  belongs_to :company
  belongs_to :project
  belongs_to :creator, class_name: 'User'
  belongs_to :contact, class_name: 'User'
  belongs_to :category
  belongs_to :zip_code

  delegate :lat, :lng, to: :zip_code, prefix: false, allow_nil: true
  validates_presence_of :title, :message => 'This field is required.'
  validates_presence_of :project_id, :message => 'Please select a project.', if: '!User.current.company_user?'
  validates_presence_of :category_id, :message => 'Must select a category.'
  validates_presence_of :needed_on, :message => 'Cant be blank.'
  validates :creator_id, :zip_code, presence: true
  validates :company_id, presence: true, if: 'creator.present? && creator.private?'
  validates :zip_code, associated: true
  after_save :save_wanted_asset_history
  has_many :assets_versions

  # Update Elasticsearch index on asset save
  update_index('wanted_assets#wanted_asset') { self }

  state_machine :state, :initial => :contributed do
    event :fulfill do
      transition contributed: :fulfilled
    end

    event :contributed do
      transition fulfilled: :contributed
    end

    event :soft_delete do
      transition all => :deleted
    end
  end

  self.inheritance_column = nil

  scope :contributed, -> { where(state: :contributed) }

  after_initialize do |wanted_asset|
    wanted_asset.needed_on ||= Time.zone.today
  end

  def save_wanted_asset_history
    current_user_id = User.current.try(:id)
    self.changes.each do |field_name, field_value|
      if WantedAsset::HISTORY_FIELDS.include? field_name
        if field_name == "state"
          field_name = "wanted_#{field_name}_#{changes["state"][1]}"
        end
        AssetsVersion.create!(wanted_id: self.id, field_name: field_name, from: field_value[0], to: field_value[1],
          whodunnit: current_user_id, project_id: self.try(:project).try(:id), group_id: self.try(:group).try(:id),
          company_id: self.company_id, action: 'wanted')
      end
    end
  end

  def location
    zip_code.location
  end

  def location_zip
    zip_code.zip_code if zip_code_id.present?
  end

  def location_zip=(value)
    zip_code = ZipCode.find_by_zip_code(value)
    if zip_code
      self.zip_code = zip_code
    elsif !value.nil?
      new_zip_code = ZipCode.new(zip_code: value)
      new_zip_code.save(validate: false)
      self.zip_code = new_zip_code
    end
  end

  def country_name=(value)
    if self.zip_code_id
      zip_code_obj = ZipCode.where("zip_code = ? AND (country = ?  OR country is null) ",self.zip_code.zip_code , value).first
      if zip_code_obj
        if zip_code.country.nil?
          zip_code_obj.update(country: value)
        end
        self.zip_code = zip_code_obj
      end
      if !zip_code_obj
        self.zip_code = ZipCode.create(zip_code: self.zip_code.zip_code, country: value)
      end
    end
  end

  def country_name
    zip_code.country if zip_code_id.present?
  end

  # Absolute URL to asset, so we can share link to wanted asset
  def url
    options = Rails.application.config.action_mailer.default_url_options
    "http://#{options[:host]}/wanted/#{friendly_id}"
  end

  def slug_candidates
    [
      :title,
      [
        :id,
        :title
      ]
    ]
  end

  def project_assignments
    project.try(:project_assignments)
  end

  def self.my_wanted_assets(user)
    where(company: user.company, state: :contributed)
  end

  def self.creators_with_expiring_wanted_assets
    includes(:creator)
      .expiring_in(Asset::EXPIRE_THRESHOLD)
      .find_each
      .group_by(&:creator)
  end

  def self.sorts user
    {
      date_posted: { created_at: :desc },
      price: { price: :asc },
    }
  end
end
