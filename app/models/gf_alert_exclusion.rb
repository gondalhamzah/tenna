class GfAlertExclusion < ActiveRecord::Base
  belongs_to :geo_fence
  belongs_to :category_group
  belongs_to :category
  belongs_to :asset
end
