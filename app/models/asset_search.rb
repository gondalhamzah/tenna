class AssetSearch
  include ActiveModel::Model

  # favoriter...used on the Favorites page. user is also present, there.
  attr_accessor :filter_search, :user, :creator, :favoriter, :project, :group, :company

  PAGE_SIZES = [25, 50, 100]

  # This class is used to generate an Elasticsearch query that represents the filter, search & sort criteria input
  # from the user

  FULL_TEXT_QUERY_FIELDS = [
    'asset.title',
    'asset.description',
    'asset.asset_num',
    'asset.make',
    'asset.model',
    'asset.category',
    'asset.tag_list',
  ]

  API_FULL_TEXT_QUERY_FIELDS = [
    'asset.title'
  ]

  # defines the field types for asset searches we can do, and how they are ordered
  def self.sorts(filter_search = nil)
    {
      date_created: {created_at: :desc},
      maintenance: {maintenance_date: :desc},
      name: {title: :asc},
      price: {price: :asc},
      in_use_until: {in_use_until: :desc}
    }.with_indifferent_access
  end

  def self.with_50_miles_search(filter_search, user)
    new(
      filter_search: filter_search,
      user: user,
      creator: user
    ).with_50_miles_search()
  end

  def self.search(filter_search, user, creator: nil, favoriter: nil, project: nil, group: nil, company: nil)
    new(
      filter_search: filter_search,
      user: user,
      creator: creator,
      favoriter: favoriter,
      project: project,
      group: group,
      company: company
    ).search()
  end

  def self.routing_search(filter_search, user, creator: nil, favoriter: nil, project: nil, group: nil, company: nil)
    new(
      filter_search: filter_search,
      user: user,
      creator: creator,
      favoriter: favoriter,
      project: project,
      group: group,
      company: company
    ).routing_search()
  end

  def self.api_search(filter_search, user, creator: nil, favoriter: nil, project: nil, group: nil, company: nil)
    new(
      filter_search: filter_search,
      user: user,
      creator: creator,
      favoriter: favoriter,
      project: project,
      group: group,
      company: company
    ).api_search()
  end

  def self.report_all_project_assets(filter_search, user)
    new(
        filter_search: filter_search,
        user: user,
      ).get_all_project_assets()
  end

  def self.search_public_assets(filter_search, user, creator: nil, favoriter: nil, project: nil, company: nil)
    new(
      filter_search: filter_search,
      user: user,
      creator: creator,
      favoriter: favoriter,
      project: project,
      company: company
    ).search_public_assets()
  end

  def self.result_count_by_zip(filter_search)
    aggregations = filter_search.aggregations(
      group_by_zip: {terms: {field: 'location_zip'}},
    ).aggregations['group_by_zip']['buckets']

    result_count_by_zip = {}
    aggregations.each do |aggregation|
      zip = aggregation['key']
      count = aggregation['doc_count']
      result_count_by_zip[zip] = count
    end

    result_count_by_zip
  end

  def ability
    @ability ||= Ability.new(user)
  end

  def with_50_miles_search
    apply_filters
      .filter(permissible)
      .filter(for_creator)
      .filter(not_sold)
      .filter(public_assets)
      .query(full_text_query)
      .order(sort_hash)
  end

  def get_all_project_assets
    assets = AssetsIndex::Asset
    assets = assets.filter({ term: {project_id: @filter_search.project.to_s}}).filter(in_deleted_expired)
    assets
  end

  def search
    apply_filters
      .filter(permissible)
      .filter(for_creator)
      .filter(in_project)
      .filter(in_group)
      .filter(in_company)
      .filter(not_sold)
      .filter(in_deleted_expired)
      .query(full_text_query)
      .order(sort_hash)
  end

  def routing_search
    routing_assets = apply_filters
      .filter(permissible)
      .filter(in_company)
      .filter(not_sold)
      .filter(in_deleted_expired)
      .query(full_text_query)
      .order(sort_hash)
  end

  def api_search
    apply_filters
     .filter(permissible)
     .filter(in_company)
     .filter(not_sold)
     .filter(in_deleted_expired)
     .query(api_full_text_query)
     .limit(1000)
     .order(sort_hash)
  end

  def search_public_assets
    apply_filters
      .filter(permissible)
      .filter(for_creator)
      .filter(in_project)
      .filter(in_company)
      .filter(not_sold)
      .filter(in_deleted_expired)
      .query(full_text_query)
      .filter(public_assets)
      .order(sort_hash)
  end

  def rental_count
    assets = AssetsIndex::Asset
    assets = expires_on_filter(assets, nil)
    rental_filter(assets).filter(in_company).filter(for_creator).filter(in_deleted_expired).filter(not_sold_for_hash).total_count
  end

  def owned_count
    assets = AssetsIndex::Asset
    assets = expires_on_filter(assets, nil)
    buy_filter(assets).filter(in_company).filter(for_creator).filter(in_deleted_expired).filter(not_sold_for_hash).total_count
  end

  def abandoned_assets
    project_ids = closed_projects
    get_abandoned_assets(project_ids)
  end

  def abandoned_count
    project_ids = closed_projects
    get_abandoned_assets(project_ids).filter(in_deleted_expired).total_count
  end

  def performing_count
    assets = AssetsIndex::Asset
    assets = expires_on_filter(assets, nil)
    available_assets_filter(assets).filter(in_company).filter(for_creator).filter(in_deleted_expired).total_count
  end

  def sold_count
    assets = AssetsIndex::Asset
    assets = expires_on_filter(assets, nil)

    status_filter(assets, 'sold')
    .filter(permissible)
    .filter(for_creator)
    .filter(in_project)
    .filter(in_company)
    .filter(in_deleted_expired).total_count
  end

  def in_use_count
    assets = AssetsIndex::Asset
    assets = expires_on_filter(assets, nil)

    in_use_filter(assets)
    .filter(permissible)
    .filter(for_creator)
    .filter(in_project)
    .filter(in_company)
    .filter(not_sold)
    .filter(in_deleted_expired).total_count
  end

  def type_count(params={})
    assets = AssetsIndex::Asset
    assets = expires_on_filter(assets, nil)
    if params.has_key?(:material) && params[:material]
      assets = material_filter(assets, true)
    end
    if params.has_key?(:equipment) && params[:equipment]
      assets = equipment_filter(assets, true)
    end
    if params.has_key?(:owned) && params[:owned]
      assets = buy_filter(assets)
    end
    if params.has_key?(:rental) && params[:rental]
      assets = rental_filter(assets)
    end
    assets.filter(permissible).filter(in_company).filter(for_creator).filter(in_deleted_expired).filter(not_sold_for_hash).total_count
  end

  def apply_filters
    # setup the assets instance and create the types of filters we should apply to it,
    # based on the filter & asset search state.
    # E.G. "filter_search.price_max" would indicate whether or not the max price was given as input to the filter


    assets = AssetsIndex::Asset
    assets = has_lat_lng(assets) if filter_search.is_live_data
    assets = location_changed_within_days(assets, filter_search.location_changed_in) if filter_search.location_changed_in and filter_search.location_changed_in != "0"
    assets = zip_code_filter(assets, filter_search.zipcode) if filter_search.zipcode != ""
    assets = zip_code_with_country_name_filter(assets, filter_search.distance_hash, filter_search.country.downcase) if filter_search.zipcode != "" and !filter_search.country.nil?
    assets = abandoned_assets if bool_value(filter_search.abandoned)

    assets = price_range_filter(assets, {min: filter_search.price_min, max: filter_search.price_max}) if filter_search.price_min.present? || filter_search.price_max.present?

    # by user's favorites if we have favorites and a user or a favoriter (favoriter is for the Favorites page)
    # favoriter only seems to non-nil when we're logged in, so we will always have a user (and therefore an id)
    assets = favorites_filter(assets, user.id) if (bool_value(filter_search.favorites) && user) || favoriter

    # by my asset if we have assets (see my_assets doc string) or any expiring and we have a user
    # my_assets does not seem to be part of the filter types on the UI.

    # assets = my_assets_filter(assets) if (bool_value(filter_search.my_assets) || bool_value(filter_search.expiring)) && user
    assets = my_assets_filter(assets) if bool_value(filter_search.expiring) && user

    unless bool_value(filter_search.equipment) && bool_value(filter_search.material)
      assets = equipment_filter(assets, true) if bool_value(filter_search.equipment)
      assets = material_filter(assets, true) if bool_value(filter_search.material)
    end

    assets = category_filter(assets, filter_search.category) if filter_search.category.present?
    assets = category_id_filter(assets, filter_search.category_id) if filter_search.category_id.present?
    assets = category_group_id_filter(assets, filter_search.category_group_id) if filter_search.category_group_id.present?
    assets = current_driver_filter(assets, filter_search.current_driver) if filter_search.current_driver.present?
    assets = available_filter(assets, filter_search.available_on) if filter_search.available_on.present?

    if (bool_value(filter_search.rental) && !bool_value(filter_search.buy)) ||
       (!bool_value(filter_search.rental) && bool_value(filter_search.buy))
      assets = rental_filter(assets) if bool_value(filter_search.rental)
      assets = buy_filter(assets) if bool_value(filter_search.buy)
    end

    assets = has_media_filter(assets, 1) if bool_value(filter_search.has_media)
    assets = distance_filter(assets, filter_search.distance_hash) if filter_search.distance_hash.present?

    # assets = status_filter(assets, filter_search.status)
    if (bool_value(filter_search.for_rent) && !bool_value(filter_search.for_sale)) || (!bool_value(filter_search.for_rent) && bool_value(filter_search.for_sale))
      assets = for_rent_filter(assets) if bool_value(filter_search.for_rent)
      assets = for_sale_filter(assets) if bool_value(filter_search.for_sale)
    end

    assets = status_filter(assets, 'sold') if bool_value(filter_search.sold)
    assets = in_use_filter(assets) if bool_value(filter_search.in_use) && !bool_value(filter_search.available)
    assets = available_assets_filter(assets) if bool_value(filter_search.available) && !bool_value(filter_search.in_use)
    assets = req_maintenance_filter(assets) if bool_value(filter_search.req_maintenance)
    if bool_value(filter_search.has_qr_code) and bool_value(filter_search.has_trackers)
      assets = has_qr_and_tracking_code(assets)
    elsif bool_value(filter_search.has_qr_code)
      assets = has_qr_code_filter(assets)
    elsif bool_value(filter_search.has_trackers)
      assets = has_tracking_code_filter(assets)
    end
    assets = abandoned_filter(assets) if bool_value(filter_search.abandoned)
    assets = expires_on_filter(assets, filter_search.expires_on )

    unless bool_value(filter_search.material)
      assets = certified_filter(assets) if bool_value(filter_search.certified)
      assets = year_range_filter(assets, {min: filter_search.year_min, max: filter_search.year_max}) if filter_search.year_min.present? || filter_search.year_max.present?
      assets = hours_range_filter(assets, {min: filter_search.hours_min, max: filter_search.hours_max}) if filter_search.hours_min.present? || filter_search.hours_max.present?
      assets = miles_range_filter(assets, {min: filter_search.miles_min, max: filter_search.miles_max}) if filter_search.miles_min.present? || filter_search.miles_max.present?
      assets = condition_filter(assets, filter_search.condition) if filter_search.condition.present?
      assets = quantity_range_filter(assets, {min: filter_search.quantity_min, max: filter_search.quantity_max}) if filter_search.quantity_min.present? || filter_search.quantity_max.present?
    end

    assets = expiring_filter(assets) if bool_value(filter_search.expiring)
    assets = public_filter(assets) if bool_value(filter_search.public)

    # is_mine and is_others should be exclusive
    unless filter_search.is_mine && filter_search.is_others
      assets = is_mine_filter(assets, filter_search.is_mine) if filter_search.is_mine.present?
      assets = is_mine_filter(assets, false) if filter_search.is_others.present?
    end

    if filter_search.created_by_me.present?
      assets = created_by_me_filter(assets)
    end

    assets = geohash_filter(assets, filter_search.geohash) if (not filter_search.geohash.nil? and not filter_search.geohash.empty?)
    assets = without_tracking_code_filter(assets) if bool_value(filter_search.without_tracking_code)
    assets = project_filter(assets, filter_search.project) if filter_search.project.present?
    assets = group_filter(assets, filter_search.group) if filter_search.group.present?
    if bool_value(filter_search.rental_mine) and bool_value(filter_search.buy_mine)
      assets = is_mine_filter(assets, true)
      assets = for_rent_and_sale_filter(assets)
    elsif bool_value(filter_search.rental_mine)
      assets = is_mine_filter(assets, true)
      # assets = rental_filter(assets)
      assets = for_rent_filter(assets)
    elsif bool_value(filter_search.buy_mine)
      assets = is_mine_filter(assets, true)
      # assets = buy_filter(assets)
      assets = for_sale_filter(assets)
    end

    if bool_value(filter_search.buy_others) and bool_value(filter_search.rental_others)
      assets = is_mine_filter(assets, false)
      assets = for_rent_and_sale_filter(assets)
    elsif bool_value(filter_search.buy_others)
      assets = is_mine_filter(assets, false)
      # assets = buy_filter(assets)
      assets = for_sale_filter(assets)
    elsif bool_value(filter_search.rental_others)
      assets = is_mine_filter(assets, false)
      # assets = rental_filter(assets)
      assets = for_rent_filter(assets)
    end

    if bool_value(filter_search.expiring_mine)
      assets = is_mine_filter(assets, true)
      assets = expiring_filter(assets)
    end
    if bool_value(filter_search.expiring_others)
      assets = is_mine_filter(assets, false)
      assets = expiring_filter(assets)
    end

    assets
  end

  def project_filter assets, value
    assets.filter [{
      term: {
        project_id: value
      }
    }]
  end

  def group_filter assets, value
    assets.filter [{
      term: {
        group_id: value
      }
    }]
  end

  def geohash_filter assets, geohashes
    assets.filter({
      or: {
        filters: geohashes.map do |geohash|
          coords = GeoHash.decode_bbox(geohash)
          {
            geo_bounding_box: {
              coordinates: {
                bottom_left: {
                  lat: coords.first.first,
                  lon: coords.first.last
                },
                top_right: {
                  lat: coords.last.first,
                  lon: coords.last.last
                }
              }
            }
          }
        end
      }
    })
  end

  def year_range_filter assets, value
    range_filter(assets, :year, value[:min], value[:max])
  end

  def price_range_filter assets, value
    range_filter(assets, :price, value[:min], value[:max])
  end

  def hours_range_filter assets, value
    range_filter(assets, :hours, value[:min], value[:max])
  end

  def miles_range_filter assets, value
    range_filter(assets, :miles, value[:min], value[:max])
  end

  def quantity_range_filter assets, value
    range_filter(assets, :quantity, value[:min], value[:max])
  end

  def favorites_filter assets, marker_id
    assets.filter { (marks.mark == 'favorite') & (marks.marker_id == marker_id.to_i) & (marks.marker_type == 'User') }
  end

  def nested_permission(permission)
    permission.flat_map do |k, v|
      if v.is_a?(Hash) && v.has_key?(:project_assignments)
        # use nested query
        # see https://www.elastic.co/guide/en/elasticsearch/reference/1.5/query-dsl-nested-filter.html
        {
          nested: {
            path: 'project_assignments',
            filter: {
              bool: {
                must: v[:project_assignments].map { |k, v| {term: {"project_assignments.#{k}" => v}} }
              }
            }
          }
        }
      else
        {term: {k => v}}
      end
    end
  end

  # ensure that the update permission (being able to edit/renew an asset) is
  # part of the query against this user to elasticsearch
  def edit_permissions
    ability.model_adapter(Asset, :update).instance_variable_get(:@rules).map(&:conditions).map do |permission|
      {
        bool: {
          must: nested_permission(permission)
        }
      }
    end
  end

  # setup what a my assets filter requires for an effective run (what the user is allowed to see (an edit standpoint))
  def my_assets_filter assets
    assets.filter(
      bool: {
        must: [],
        should: edit_permissions,
        must_not: []
      }
    )
  end

  def equipment_filter assets, value
    assets.filter { equipment == value }
  end

  def material_filter assets, value
    assets.filter { material == value }
  end

  def created_by_me_filter assets
    user_id = @user.id
    assets.filter { creator_id == user_id }
  end

  def is_mine_filter assets, is_mine
    if user.public?
      if bool_value(is_mine)
        assets.filter({
          bool: {
            must: [
              term: { creator_id: @user.id}
            ]
          }
        })
      else
        assets.filter({
          bool: {
            must_not: [
              term: { creator_id: @user.id}
            ]
          }
        })
      end
    else
      if bool_value(is_mine)
        assets.filter({
          bool: {
            must: [
              term: { company_id: @user.company.id}
            ]
          }
        })
      else
        assets.filter({
          bool: {
            must_not: [
              term: { company_id: @user.company.id}
            ]
          }
        })
      end
    end
  end

  def rental_filter assets
    assets.filter { asset_kind == 'rental' }
  end

  def public_filter assets
    assets.filter({ bool: {must: {terms: { asset_kind: ['for_sale', 'for_rent']}}}})
  end

  def buy_filter assets
    assets.filter { asset_kind == 'owned' }
  end

  def for_rent_and_sale_filter assets
    assets.filter(terms: { asset_kind: ['for_sale','for_rent']})
  end

  def for_sale_filter assets
    assets.filter { asset_kind == 'for_sale' }
  end

  def for_rent_filter assets
    assets.filter { asset_kind == 'for_rent' }
  end

  def category_filter assets, value
    assets.filter { category_exact == value }
  end

  def category_id_filter assets, value
    assets.filter { category_id == value }
  end

  def category_group_id_filter assets, value
    assets.filter { category_group_id == value }
  end

  def has_media_filter assets, min
    assets.filter { photos_count >= min.to_i }
  end

  def in_use_filter assets
    assets.filter { in_use_until >= I18n.l(Date.today, format: :elasticsearch) }
  end

  def available_assets_filter assets
    assets.filter { in_use_until < I18n.l(Date.today, format: :elasticsearch) }
  end

  def available_filter assets, value
    assets.filter { available_on <= I18n.l(Date.parse(value), format: :elasticsearch) }
  end

  def req_maintenance_filter assets
    assets.filter { maintenance == true }
  end

  def has_qr_and_tracking_code(assets)
    assets.filter({
                    or: [
                      { range: { qr: { gt: 0 }}},
                      { range: { rfid: { gt: 0 }}},
                      { range: { upc: { gt: 0 }}},
                      { range: { cellular: { gt: 0 }}},
                      { range: { serialnumber: { gt: 0 }}},
                      { range: { lora: { gt: 0 }}},
                      { range: { sigfox: { gt: 0 }}},
                      { range: { bt: { gt: 0 }}}
                    ]
                  })
  end

  def has_qr_code_filter(assets)
    assets.filter({
                    or: [
                      { range: { qr: { gt: 0 }}},
                      { range: { rfid: { gt: 0 }}},
                      { range: { upc: { gt: 0 }}} ,
                      { range: { serialnumber: { gt: 0 }}}
                    ]
                  })
    # for and operator execution
    # assets.filter(terms: { asset_tags_list: ["QR","RFID","UPC"], "execution": "and" })
    # for or execution in an array
    # assets.filter(terms: { asset_tags_list: ["QR","RFID"] })
    # assets.filter { has_tracking_code == true }
  end

  def has_lat_lng assets
    assets.filter{ has_location == true }
  end

  def location_changed_within_days assets, daysVal
    val = case daysVal.to_i
      when 1 # 1 year
        'now-1y/d'
      when 2 # 6 month
        'now-6M/d'
      when 3 # 3 month
        'now-3M/d'
      when 4 # 1 month
        'now-1M/d'
      when 5 # 1 week
        'now-1w/d'
      when 6 # 1 day
        'now-1d'
      when 7 # 1 hr
        'now-1h'
    end

    assets.filter({range: {
      last_location_changed: {
          gte: val
        }
      }})
  end

  def has_tracking_code_filter(assets)
    assets.filter({
      or: [{
        range: {
          cellular: {
            gt: 0
          }
        }
      }, {
        range: {
          lora: {
            gt: 0
          }
        }
      }, {
        range: {
          sigfox: {
            gt: 0
          }
        }
      }, {
        range: {
          bt: {
            gt: 0
          }
        }
      }]
    })
    # assets.filter(terms: { asset_tags_list: ["BT","Cellular","LoRa","Sigfox"], "execution": "and" })
  end

  def assets_with_qr_code
    assets = AssetsIndex::Asset
               .filter(in_company)
    assets = status_filter(assets, 'available')
    assets = has_qr_code_filter assets
    assets.total_count
  end

  def condition_filter(assets, value)
    assets.filter { condition == value }
  end

  def zip_code_filter(assets, value)
    assets.filter { location_zip == value }
  end

  def zip_code_with_country_name_filter assets, distance_hash , cou
    if distance_hash
      zip_code = distance_hash[:zip_code]
      assets.filter {(location_zip == zip_code)}.filter{(country == cou)}
    else
      assets
    end
  end

  def distance_filter(assets, value)
    if value[:miles] == 'Exact'
      assets.filter { location_zip == value[:zip_code] }
    else
      assets.filter(geo_distance: {
        distance: "#{value[:miles]}miles",
        coordinates: {
          lat: value[:lat],
          lon: value[:lng]
        }
      })
    end
  end

  def without_tracking_code_filter(assets)
    assets.filter [{
      term: {
        has_tracking_code: false
      }
    }]
  end

  def status_filter assets, value
    assets.filter [{
      term: {
        status: (value || 'available')
      }
    }]
  end

  def expires_on_filter(assets, value)
    # if private user, skip this filter
    return assets if company or user.try(:company)

    assets.filter({
      or: [
        {
          and: [
            {
              range: {
                expires_on: {
                  gte: I18n.l(Date.parse(value), format: :elasticsearch)
                }
              }
            },
            {
              terms: {
                asset_kind: ['for_sale', 'for_rent']
              }
            }
          ]
        },
        {
          terms: {
            asset_kind: ['owned', 'rental']
          }
        }
      ]
    })
  rescue
    assets.filter({
      or: [
        {
          and: [
            {
              range: {
                expires_on: {
                  gte: 'now/d'
                }
              }
            },
            {
              terms: {
                asset_kind: ['for_sale', 'for_rent']
              }
            }
          ]
        },
        {
          terms: {
            asset_kind: ['owned', 'rental']
          }
        }
      ]
    })
  end

  def certified_filter(assets)
    assets.filter { certified == true }
  end

  def expiring_filter(assets)
    assets.filter { expires_on <=  Asset::EXPIRE_THRESHOLD.from_now }
  end

  def current_driver_filter(assets, values)
    # Use select tags
    assets.filter [{
                     terms: { current_driver_id: values }
                   }]
  end

  def cost_and_value(project)
    assets = AssetsIndex::Asset
    project_assets = assets
                       .filter{ (project_id == project) }
                       .filter( { not: { terms: { status: ['deleted', 'sold'] } } } )
    cost_agg = project_assets.aggs( {cost: {sum: {field: 'price'}}})
    value_agg = project_assets.aggs( {assets_value: {sum: {field: 'market_value'} } } )
    cost = cost_agg.aggs
    value = value_agg.aggs
    { cost: cost['cost']['value'], value: value['assets_value']['value'] }
  end

  def analytic_filters(asset_filters, company)
    term_conditions = []
    conditions = []
    drivers = asset_filters[:drivers]
    tracking_codes =  asset_filters[:tracking_codes] || []
    asset_management =  asset_filters[:asset_management] || []
    assets = AssetsIndex::Asset
    assets = assets.filter({
      term: {
        company_id: company
      }
    }).per(10000)
    term_conditions << { terms: { project_id: asset_filters[:sites] } } if asset_filters[:sites].present?
    term_conditions << { terms: { category_id: asset_filters[:categories] } } if asset_filters[:categories].present?
    term_conditions << { terms: { group_id: asset_filters[:groups] } } if asset_filters[:groups].present?
    term_conditions << { terms: { creator_id: asset_filters[:users] } } if asset_filters[:users].present?
    term_conditions << { terms: { title: asset_filters[:asset_title] } } if asset_filters[:asset_title].present?
    conditions << { bool: { must: term_conditions } } if term_conditions.present?
    assets = assets.filter(conditions)
    assets = current_driver_filter(assets, drivers) if asset_filters[:drivers].present?
    assets = tracking_code_filters(assets, tracking_codes)
    assets = assets_management(assets, asset_management)
    assets_ids = assets.map { |asset| asset.id }
    return assets_ids, assets
  end

  private

  def tracking_code_filters(assets, tracking_codes)
    return assets if tracking_codes.nil?
    conditions = []
    conditions << {query: { range: { qr: { gt: 0 } } } } if tracking_codes.include? "qr"
    conditions << {query: { range: { rfid: { gt: 0 } } } } if tracking_codes.include? "rfid"
    conditions << {query: { range: { lora: { gt: 0 } } } } if tracking_codes.include? "lora"
    conditions << {query: { range: { sigfox: { gt: 0 } } } } if tracking_codes.include? "sigfox"
    conditions << {query: { range: { upc: { gt: 0 } } } } if tracking_codes.include? "upc"
    conditions << {query: { range: { bt: { gt: 0 } } } } if tracking_codes.include? "bt"
    conditions << {query: { range: { cellular: { gt: 0 } } } } if tracking_codes.include? "cellular"
    assets = assets.filter(conditions)
    assets
  end

  def permissible
    {
      bool: {
        must: [],
        should: permissions,
        must_not: []
      }
    }
  end

  def for_creator
    return {} if creator.blank?
    {
      term: {
        creator_id: creator.id
      }
    }
  end

  def assets_management assets, asset_management
    assets = buy_filter(assets) if asset_management.include? "owned"
    assets = rental_filter(assets) if asset_management.include? "rental"
    assets = for_sale_filter(assets) if asset_management.include? "for_sale"
    assets = for_rent_filter(assets) if asset_management.include? "for_rent"
    assets = abandoned_filter(assets) if asset_management.include? "abandoned"
    assets = status_filter(assets, 'sold') if asset_management.include? "sold"
    assets = status_filter(assets) if asset_management.include? "available"
    assets = in_use_filter(assets) if asset_management.include? "in_use"
    assets
  end

  def in_project
    return {} if project.blank?
    {
      term: {
        project_id: project.id
      }
    }
  end

  def in_group
    return {} if group.blank?
    {
      term: {
        group_id: group.id
      }
    }
  end

  def in_company
    return {} if company.blank?
    {
      term: {
        company_id: company.id
      }
    }
  end

  def not_sold
    return {} if bool_value(filter_search.try(:sold))
    {
      not: {
        terms: { status: ['sold'] }
      }
    }
  end

  def not_sold_for_hash
    return {} if bool_value(filter_search[:sold])
    {
      not: {
        terms: { status: ['sold'] }
      }
    }
  end

  def in_deleted_expired
    {
      not: {
        or: [
          {
            range: {
              expires_on: {
                lt: I18n.l(Date.today, format: :elasticsearch)
              }
            }
          },
          terms: {
            status: ['deleted']
          }
        ]
      }
    }
  end

  def not_deleted
    {
      not: {
        terms: { status: ['deleted'] }
      }
    }
  end

  def public_assets
    {
      bool: {
        must: [
          terms: {
            asset_kind: ['for_sale', 'for_rent']
          }
        ],
        must_not: [{term: {status: 'deleted'}}, {term: {status: 'expired'}}]
      }
    }
  end

  def full_text_query
    if filter_search.query.present?
      filter_search.query.strip!
      {
        simple_query_string: {
          query: filter_search.query + '*',
          analyze_wildcard: true,
          fields: FULL_TEXT_QUERY_FIELDS,
          default_operator: 'and'
        }
      }
    else
      {
        match_all: {}
      }
    end
  end

  def api_full_text_query
    if filter_search.query.present?
      filter_search.query.strip!
      {
        simple_query_string: {
          query: '*' + filter_search.query + '*',
          analyze_wildcard: true,
          fields: API_FULL_TEXT_QUERY_FIELDS
        }
      }
    end
  end

  def permissions
    ability.attributes_for(:read, Asset).reject { |k, v| v.nil? }.map do |k, v|
      {term: {k => v}}
    end
  end

  def range_filter(assets, key, min, max)
    min_filter = {}
    max_filter = {}
    min_filter = {gte: min.to_i} if min.present?
    max_filter = {lt: max.to_i} if max.present?
    range = min_filter.merge(max_filter)

    if range.present?
      assets.filter(range: {key => range})
    else
      assets
    end
  end

  def bool_value(value)
    value.in?([true, 1, '1', 't', 'true'])
  end

  def sort_hash
    if filter_search.sort.try(:to_sym) == :distance && filter_search.coordinate_hash
      {
        _geo_distance: {
          coordinates: filter_search.coordinate_hash,
          order: 'asc'
        }
      }
    else
      #puts AssetSearch.sorts(filter_search)[(filter_search.sort.presence || :date_created).to_sym]
      #puts filter_search.sort.presence
      AssetSearch.sorts(filter_search)[(filter_search.sort.presence || :name).to_sym]
    end
  end

  def get_abandoned_assets(project_ids)
    assets = AssetsIndex::Asset
      .filter(in_company)
      .filter(not_deleted)
      .filter({
        not: {
          terms: { status: ['sold'] }
        }
      })

    assets.filter({
      bool: {
        must: [
          terms: {
            project_id: project_ids
          }
        ]
      }
    })
  end

  def abandoned_filter(assets)
    project_ids = closed_projects
    assets.filter(in_company)
        .filter(in_deleted_expired)
        .filter({
          bool: {
            must: [
              terms: {
                project_id: project_ids
              }
            ]
          }
        })
  end

  def closed_projects
    ProjectsIndex::Project
      .filter(in_company)
      .filter{ closeout_date < I18n.l( Date.today, format: :elasticsearch ) }
      .filter{ !deleted_at }
      .limit(1000)
      .map { |project| project.id }
  end
end
