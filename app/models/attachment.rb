class Attachment < ActiveRecord::Base
  belongs_to :asset

  # Base class for asset file attachments (photos, PDFs)

  validates :file, presence: true

end
