class GeoFence < ActiveRecord::Base
  include GeoFenceMap
  include Geometry

  belongs_to :company

  has_many :geo_fence_company_contacts, :dependent => :delete_all
  has_many :geo_fence_other_contacts, :dependent => :delete_all
  has_many :geo_points, :dependent => :delete_all
  has_many :gf_alert_exclusions, :dependent => :delete_all

  def as_json(options=nil)
    ret = super(:only => [:id, :center, :shape, :name, :color, :radius, :alert_enter_site, :alert_exit_site, :zoom_level])
    ret[:other_contacts] = self.geo_fence_other_contacts.map(&:as_json)
    ret[:company_contacts] = self.geo_fence_company_contacts.map(&:as_json)
    ret[:points] = self.geo_points.map(&:as_json)
    relations = self.gf_alert_exclusions.select do |ex|
      ex.asset.present?
    end
    ret[:map_img] = static_map_for(self)
    ret
  end

  def contains?(lat, lng)
    points = []
    if self.shape == "polygon" then
      geo_points.each do |pt|
        points << Point(pt.lat, pt.lng)
      end
    else
      get_polylines(self).each do |pts|
        points << Point(pts[0], pts[1])
      end
    end
    area = Polygon.new points

    pt = Point(lat, lng)
    area.contains?(pt)
  end
end
