class List < ActiveRecord::Base
  has_many :list_items
  enum list_type: %w(inventory checklist)
end
