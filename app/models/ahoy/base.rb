module Ahoy
  class Base < ActiveRecord::Base
    self.abstract_class = true

    establish_connection ENV['AHOY_DATABASE_URL']
  end
end
