class DriverScoreCard < SafetyScoreCard
  attr_accessor :driver

  def initialize(attributes={})
    super
  end

  def search_params
    {operator_id: self.driver.id, created_at: (self.start_date.to_datetime..self.end_date.to_datetime)}
  end

end
