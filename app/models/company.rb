class Company < ActiveRecord::Base
  mount_uploader :company_logo, CompanyLogoUploader

  after_create :allocate_tracking_codes,:create_by_default_on_subscriptions

  belongs_to :contact, class_name: 'User', dependent: :delete
  accepts_nested_attributes_for :contact

  has_many :users, -> { where(deleted_at: nil) }, dependent: :delete_all
  has_many :projects
  has_many :assets
  has_many :equipments
  has_many :wanted_assets
  has_many :groups
  has_one :address, as: :addressable, dependent: :destroy
  belongs_to :project, dependent: :destroy
  has_many :subscriptions
  has_many :routes
  has_many :tracking_codes
  has_many :geo_fences
  has_many :analytic_filter_labels

  accepts_nested_attributes_for :address, allow_destroy: true
  accepts_nested_attributes_for :project

  validates :name, :address, :contact_id, presence: true
  validates :admin_seats, numericality: {only_integer: true, greater_than: 0}, allow_blank: true
  validates :contact_id, inclusion: {in: ->(record) { record.users.ids }, message: 'is not in the same company'}, unless: 'contact.blank?'

  validates :website, url: {allow_blank: true}

  validates :name, uniqueness: {scope: :region, message: 'already exists'}

  enum company_type: %w(client demo)
  enum qr_status: %w(not_requested requested in_progress created downloaded)
  enum qr_print_type: ['laserPrint', 'thermalPrint']
  enum qr_format: ['4_x_1', '4_x_2', '4_x_2.5', '4_x_3', '4_x_4',
                 'avery_5160', 'avery_5163', 'avery_5963', 'avery_6572', 'avery_6871']

  def admin_seats_remaining
    if admin_seats.present?
      admin_seats - users.company_admin.count
    end
  end

  def create_by_default_on_subscriptions
    Subscription.create([{company_id: self.id, category: 'wantit',state: 'active',payment: true},
      {company_id: self.id, category: 'company reports',state: 'active',payment: true},
      {company_id: self.id, category: 'add tracking to asset',state: 'active',payment: true},
      {company_id: self.id, category: 'maintenance',state: 'active',payment: true}])
  end

  def qr_requested_or_generating?
    qr_status.in? ["requested", "in_progress"]
  end

  def total_codes_to_create
    TrackingCode::MAX_UNUSED_CODES - total_unused_codes
  end

  def allocate_tracking_codes
    tokens = []
    self.total_codes_to_create.times do
      uuid = SecureRandom.uuid
      uuid = uuid.gsub!('-', '')
      str = Base64.encode64([uuid].pack('H*'))[0..21]
      str = str.gsub('+', '-')
      str = str.gsub('/', '~')
      tokens << str
    end
    TrackingCode.transaction do
      tokens.each do |token|
        self.tracking_codes.create(token: token)
      end
    end
  end

  def total_unused_codes
    tracking_codes.unassigned.count
  end

  def total_used_codes
    tracking_codes.assigned.count
  end

  def company_admins
    users.where(role: User.roles[:company_admin])
  end

  def field_members
    users.where(role: User.roles[:field_member])
  end

  def can_create_exports?
    self.projects.any? or self.assets.any?
  end

  def can_be_destroyed?
    assets.empty? and wanted_assets.empty? and groups.empty? and ( projects.empty? ||  (projects.count == 1 and projects.first == self.project))
  end

  # Disables/enables all users of a company when their company gets changed from active/inactive
  def update_active_state!
    if previous_changes[:active].present?
      if active?
        users.update_all(active: true)
      else
        users.update_all(active: false)
      end
    end
  end
end

