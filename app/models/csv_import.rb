class CsvImport < ActiveRecord::Base
  belongs_to :user
  belongs_to :project
  belongs_to :group

  validates :user, :file, :state, :job_type, presence: true

  enum state: %w(working success failure)
  enum job_type: %w(AssetCsvImportJob)

  mount_uploader :file, AttachmentUploader

  # Creates the Sidekiq job to process the CSV after save
  after_commit :schedule_job

  def company_id
    project.try(:company_id) || user.try(:company_id)
  end

  private

  def schedule_job
    job_type.constantize.perform_later(id)
  end
end
