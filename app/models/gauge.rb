class Gauge < ActiveRecord::Base
    def as_json(options=nil)
        ret = super(:only => [:id, :name, :template, :meta, :can_request_service])
        ret['active'] = ActionController::Base.helpers.asset_path("live_data/gauge/#{self.icon}-Active.png")
        ret['inactive'] = ActionController::Base.helpers.asset_path("live_data/gauge/#{self.icon}-Inactive.png")
        ret
    end

    def safety_indexify_name
      name.parameterize.underscore
    end
end
