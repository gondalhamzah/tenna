class Mark < ActiveRecord::Base
  belongs_to :marker, polymorphic: true
  belongs_to :markable, polymorphic: true

  validates :marker, presence: true
  validates :markable, presence: true
  validates :mark, presence: true, inclusion: { in: ["favorite"] }
  validates :marker_type, inclusion: { in: ["User"] }
  validates :markable_type, inclusion: { in: ["Asset"] }

  # Update Elasticsearch index on asset save
  update_index('assets#asset') { markable }
end
