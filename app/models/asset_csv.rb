class AssetCsv
  include ActiveModel::Model
  include ActiveModelAttributes

  attr_accessor :rental,
                :public,
                :type,
                :category,
                :durability,
                :title,
                :description,
                :make,
                :model,
                :year,
                :hours,
                :miles,
                :condition,
                :market_value,
                :price,
                :quantity,
                :asset_num,
                :available_on,
                :in_use_until,
                :location_zip,
                :certified,
                :units,
                :tag_list,
                :notes,
                :creator,
                :project,
                :group,
                :company,
                :owned,
                :country_name

  validates :creator, :project, :available_on, :type, :market_value, presence: true
  validate :in_use_until_validity
  validate :asset_validity

  # Give aliases to CSV data values
  alias_attribute :in_use_until, :available_on
  alias_attribute :zip_code, :location_zip
  alias_attribute :time_units, :units
  alias_attribute :type_of_asset, :type
  alias_attribute :asset_number, :asset_num
  alias_attribute :tags, :tag_list

  def create!
    asset.save!
  end

  def asset(reinitialize = false)
    # We used to use asset_num as unique value on assets to enable users to update assets via CSV import,
    # but were asked to remove this feature. Leaving as comment in case of future requirement.
    # @asset = (!reinitialize && @asset) || creator.assets.where(asset_num: asset_num).first_or_initialize.tap{|a| a.attributes = attributes(false).merge(status: 'available')}
    @asset = Asset.new(attributes(false).merge(status: 'available'))
  end

  def attributes(strip_blank = true)
    # remove blank attributes
    strip_blank ? super().delete_if { |a, v| v.blank? } : super()
  end

  def self.accessible_attr_names
    attr_names - %i(creator company project) + %i(ownrentalfor_sale zip_code hours miles type_of_asset asset_number tags notes for_rent for_sale pricecost time_units country)
  end

  def certified= value
    @certified = value.present? ? value : false
  end

  def condition= value
    @condition = value.present? ? value.downcase : value
  end

  def pricecost= value
    @price = value.present? ? value.to_i : value
  end

  def time_units= value
    @units = value.present? ? value.downcase : value
  end

  def for_sale= value
    @for_sale = bool_value(value)
  end

  def for_rent= value
    @for_rent = bool_value(value)
  end

  def public= value
    @public = (@for_sale || @for_rent)
  end

  def owned= value
    @owned = bool_value(value)
  end

  def rental= value
    @rental = bool_value(value)
  end

  def country= value
    @country_name = value
  end

  private

  def available_on_validity
    self.available_on = parse_date(available_on) if available_on.is_a?(String)
    errors.add(:available_on, 'date needs to be after 2000.') if available_on.is_a?(Date) && available_on < Date.parse('2000-01-01')
  rescue ArgumentError
    errors.add(:available_on, 'date format needs to be in the correct format.')
  end

  def in_use_until_validity
    self.in_use_until = parse_date(in_use_until) if in_use_until.is_a?(String)
    errors.add(:in_use_until, 'date needs to be in the future.') unless in_use_until.is_a?(Date) && in_use_until > Time.zone.now.to_date
  rescue
    errors.add(:in_use_until, 'date format needs to be in the correct format.')
  end

  def associations_prep
    self.company = project.try(:company) || creator.try(:company)
    process_category
  end

  def parse_date date_str
    format = I18n.t(date_str =~ /\d{4}\z/ ? 'month_day_year' : 'month_day_year_short', scope: %i(date formats))
    Date.strptime(date_str, format)
  end

  # Match category to existing category in DB
  def process_category
    if category.is_a?(String)
      category_obj = Category.where("LOWER(NAME) = ?", category.downcase).first
      if category_obj
        self.category = category_obj
      else
        errors.add(:category, :not_available, availables: Category.pluck(:name).join(', '))
      end
    end
  end

  def asset_validity
    associations_prep
    errors.add(:type, :not_available, availables: Asset::TYPES.join(', ')) if type.present? && !type.in?(Asset::TYPES)
    if errors.empty?
      dummy = asset(true)
      dummy.errors.each { |a, e| errors.add(a, e) } if dummy.invalid?
      if dummy.valid?
        @rental = true if @for_rent
        dummy.validate_public_rental_owned_values
        dummy.errors.each { |a, e| errors.add(a, e) }
      end
    end
  end

  def bool_value value
    value.in?([true, 1, '1', 't', 'true', 'TRUE', 'T'])
  end
end
