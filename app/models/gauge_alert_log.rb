class GaugeAlertLog < ActiveRecord::Base
  belongs_to :gauge_condition
  belongs_to :asset
  belongs_to :operator, class_name: 'User'

  #Update Elasticsearch index on save
  update_index('gauge_alert_logs#gauge_alert_log') { self }

end
