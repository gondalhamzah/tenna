class Visit < Ahoy::Base
  has_many :ahoy_events, class_name: "Ahoy::Event"
  belongs_to :user


  def ahoy_events_count
    ahoy_events.count
  end
end
