class RouteRecurring < ActiveRecord::Base
  belongs_to :route
  belongs_to :event
  validates_uniqueness_of :fired_date, scope: :route_id
  has_many :driver_stops
end
