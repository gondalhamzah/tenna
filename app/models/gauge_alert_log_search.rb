### EXAMPLES
# SEARCH BY NESTED
# GaugeAlertLogSearch.new(values: {unit: 'MPH'}).search
# GaugeAlertLogSearch.new(values: {unit: 'MPH'}).search
# INDEX
#   {"id"=>1,
#   "asset_id"=>11905,
#  "asset_name"=>"car 4",
#  "gauge_name"=>"speed",
#  "gauge_condition_id"=>6,
#  "gauge_condition_severe_level"=>"high_points",
#  "created_at"=>"2018-01-13T17:33:20.907Z",
#  "updated_at"=>"2018-01-15T23:20:51.994Z",
#  "operator_id"=>462,
#  "operator_name"=>"Tim Cook",
#  "company_id"=>"50",
#  "values"=>
#  {"msg"=>"when Speed exceeds speed by 200 mph",
#   "type"=>"Speed",
#   "unit"=>"MPH",
#   "value"=>300,
#   "severity"=>"high",
#   "timestamp"=>"2018-01-13T12:33:20.891-05:00"}}

class GaugeAlertLogSearch
  include ActiveModel::Model
  include ActiveModelAttributes
  attr_accessor :id,
                :asset_id,
                :asset_name,
                :gauge_id,
                :gauge_name,
                :gauge_condition_id,
                :gauge_condition_severe_level,
                :company_id,
                :values,
                :created_at,
                :updated_at,
                :operator_id,
                :operator_name

  def initialize(attributes={})
    super
    if User.current
      @company_id ||= User.current.company_id
    end
  end


  def apply_filters
    gauge_alert_logs = GaugeAlertLogsIndex::GaugeAlertLog
    attributes.keys.each do |column_name|
      val = send(column_name.intern)
      case val
        when -> (n) { n.is_a?(Hash) }
          val.each do |k, v|
            gauge_alert_logs = gauge_alert_logs.filter({ bool: {must: [match: {"#{column_name}.#{k}" => v }] } })
          end
        when -> (n) { n.is_a?(Range) }
          first_val, last_val = val.first, val.last
          first_val = first_val.utc if first_val.is_a?(DateTime)
          last_val = last_val.utc  if last_val.is_a?(DateTime)
          gauge_alert_logs = gauge_alert_logs.filter({
                                                       range:
                                                         {
                                                           column_name.intern =>
                                                             {
                                                               "gte" => first_val,
                                                               "lte" => last_val
                                                             }
                                                         }
                                                     })
        when -> (n) {n.is_a?(Array) }
          gauge_alert_logs = gauge_alert_logs.filter({
                                                      bool: { must: [ terms: { column_name.intern => val } ] }
                                                     })
        when -> (n)  { !n.blank? }
          gauge_alert_logs = gauge_alert_logs.filter({ bool: { must: [term: {column_name.intern => val}]}}) if val
      end
    end
    gauge_alert_logs
  end

  def search()
    apply_filters
      .order(created_at: :desc)
      .limit(100000)
  end
end
