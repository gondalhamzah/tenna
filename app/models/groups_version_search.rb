
class GroupsVersionSearch
  include ActiveModel::Model
  attr_accessor :user_id, :fields, :company_id, :status, :group_id, :actions,:filter_search, :user

  def initialize(attributes={})
    super
    @company_id ||= User.current.company_id if User.current
  end

  def self.total_history(group_id)
    new(group_id: group_id).search()
  end

  def self.edits(group_id)
    new(
      group_id: group_id,
      fields: %w{name notes group_num client description state}).search()
  end

  def self.moves(group_id)
    new(
      group_id: group_id,
      fields: %w{project_id asset_id}).search()
  end

  def self.user_history(group_id)
    new(group_id: group_id, user_id: User.current.try(:id)).search()
  end

  def search
    apply_filters
      .order(created_at: :desc)
      .limit(250)
  end

  def apply_filters
    groups_versions = GroupsVersionsIndex::GroupsVersion

    groups_versions = groups_versions.filter({
        bool: {
          must: [
            term: { company_id: company_id }
          ]
        }
      }) if company_id

    groups_versions = groups_versions.filter({
      bool: {
        must: [
          term: { group_id: group_id }
        ]
      }
    }) if group_id

    groups_versions = groups_versions.filter({
      bool: {
        must: [
          term: { user_id: user_id }
        ]
      }
    }) if user_id

    groups_versions = groups_versions.filter({
      bool: {
        must: [
          terms: { field_name: fields }
        ]
      }
    }) if (fields || []).length > 0

    groups_versions = groups_versions.filter({
      bool: {
        must: [
          terms: { action: actions }
        ]
      }
    }) if (actions || []).length > 0

    groups_versions
  end

end
