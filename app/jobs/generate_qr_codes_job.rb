class GenerateQrCodesJob < ActiveJob::Base
  include TrackingCodesHelper
  queue_as :default

  def perform(company_id, qr_format)
    begin
      company = Company.find_by_id company_id
      file_locations = files_details(company.id, qr_format)
      tracking_codes = company.tracking_codes
      Prawn::Labels.types = prawn_label(qr_format)
      count = 1

      if Rails.env.development?
        Prawn::Labels.generate(file_locations[:pdf_file_path], tracking_codes, type: "#{qr_format}", document: { page_layout: :portrait }) do |pdf, tracking_code|
          puts "#{count}: Generating Codes ....... #{file_locations[:pdf_file_path]}"
          url = "http://#{ENV['QR_DOMAIN']}/#{tracking_code.token}"
          qr = RQRCode::QRCode.new(url, size: 6, level: :q).as_png
          qr.save(file_locations[:qr_image_path])
          printed_patches[qr_format][0].each do |key, value|
            if key.eql? :qr
              pdf.image file_locations[:qr_image_path], at: value[0], width: value[1]
            else
              pdf.image patch_file_path(qr_format, key), at: value[0], width: value[1]
            end
          end
          count += 1
        end
      else
        obj = S3_OBJECT.object(qr_codes_file_path(company.id, qr_format))
        labels = Prawn::Labels.render(tracking_codes, :type => "#{qr_format}", document: { page_layout: :portrait }) do |pdf, tracking_code|
          puts "#{count}: Generating Codes ....... #{file_locations[:pdf_file_path]}"
          url = "http://#{ENV['QR_DOMAIN']}/#{tracking_code.token}"
          qr = RQRCode::QRCode.new(url, :size => 6, :level => :q).as_png
          qr.save(file_locations[:qr_image_path])
          printed_patches[qr_format][0].each do |key, value|
            if key.eql? :qr
              pdf.image file_locations[:qr_image_path], :at => value[0], :width => value[1]
            else
              pdf.image patch_file_path(qr_format, key), :at => value[0], :width => value[1]
            end
          end
          count += 1
        end
        obj.put(body: labels)
      end
      company = Company.find_by_id company_id
      company.qr_status = 'created'
      company.save!
    rescue => e
      puts "In rescue, error: #{e.backtrace}, location: #{e.backtrace_locations}"
    end
  end
end
