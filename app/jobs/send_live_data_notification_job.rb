require "#{Rails.root}/app/helpers/sms_message_helper"
require "#{Rails.root}/lib/aws_sns"
include AwsSns
include SmsMessageHelper

class SendLiveDataNotificationJob < ActiveJob::Base
  include ActionView::Helpers
  # This is a CSV import job run by SideKiq. It parses the uploaded CSV file, creating assets for each row and saving
  queue_as :default

  def perform(notification)
    contact = notification[:to]
    data = notification[:data]
    notes = contact[:notes]
    asset = Asset.find_by_id(data[:asset_id])
    if contact[:user_id]
      user = User.find_by_id(contact[:user_id])
      LiveDataMailer.notification(user.email, data, asset, notes).deliver if user && user.email.present?
    end

    LiveDataMailer.notification(contact[:email], data, asset, notes).deliver if contact[:email]

    sms_live_data_notification(contact[:phone], data, asset, notes) if contact[:phone]
  end
end
