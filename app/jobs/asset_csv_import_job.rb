class AssetCsvImportJob < ActiveJob::Base
  include ActionView::Helpers

  # This is a CSV import job run by SideKiq. It parses the uploaded CSV file, creating assets for each row and saving
  queue_as :default

  def perform(csv_import_id)
    csv_import = CsvImport.where(id: csv_import_id).first
    return if csv_import.nil? || !csv_import.working?
    attribute_names = []
    errors = []
    assets_added = assets_updated = 0
    Chewy.strategy(:atomic) do
      Asset.transaction do
        CSV.parse(csv_import.file.read).each_with_index do |row, i|
          if i.zero?
            # get attribute names from header, downcased, whitespace replaced and non alphabet removed
            attribute_names = row.map { |an| an.downcase.gsub(/\s+/, '_').gsub(/[^a-z_]/, '')  if an}
            if (invalid_attributes = attribute_names - AssetCsv.accessible_attr_names.map(&:to_s)).any?
              errors << "CSV contains invalid attributes: #{invalid_attributes.join(', ')}"
              break
            end
          else
            if csv_import.project_id.nil?
              attributes = Hash[*attribute_names.zip(row).flatten(1)].merge(creator: csv_import.user, group: csv_import.group, project: csv_import.group.project)
            elsif csv_import.group_id.nil?
              attributes = Hash[*attribute_names.zip(row).flatten(1)].merge(creator: csv_import.user, project: csv_import.project)
            end
            attributes['public'] = false
            a_csv = AssetCsv.new(attributes)
            if a_csv.invalid?
              errors << "Line #{i+1} : #{a_csv.errors.full_messages.join('; ')}"
              next
            end
            a_csv.asset.new_record? ? assets_added += 1 : assets_updated += 1
            a_csv.create!
          end
        end
        raise ActiveRecord::Rollback if errors.present?
      end
    end

    if errors.empty?
      csv_import.update_attributes!(message: "#{assets_added} assets has been added.", state: 'success')
    else
      csv_import.update_attributes!(message: sanitize(errors.join('<br>') + '<br><br>*Please REFRESH your browser (F5) before reattempting your upload.*', tags: %w(br)), state: 'failure')
    end

  rescue StandardError => e
    csv_import.update_attributes!(message: 'An unexpected error has occurred'  + '<br><br>Please refresh your browser (F5) before reattempting your upload.', state: 'failure')

    # Any errors in the CSV import get logged in Airbrake. This may end up being overkill, as users who mangle CSV files
    # will generate exceptions in Airbrake that are caused by user error, not application issues
    Airbrake.notify e, cgi_data: ENV.to_hash if defined?(Airbrake)
    raise
  end
end
