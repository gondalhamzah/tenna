require "#{Rails.root}/app/helpers/sms_message_helper"
require "#{Rails.root}/lib/aws_sns"
include AwsSns
include SmsMessageHelper

class SendLiveDataAlertJob < ActiveJob::Base
  include ActionView::Helpers
  # This is a CSV import job run by SideKiq. It parses the uploaded CSV file, creating assets for each row and saving
  queue_as :default

  def perform(asset_id, cond_id, dt, payload)
    asset = Asset.find_by_id(asset_id)
    cond = GaugeCondition.find_by_id(cond_id)
    contacts = cond.contacts
    msg = cond.gauge_alert_group.gauge.template
    cond.values.each do |k, v|
      msg = msg.gsub("[#{k}]", v)
    end

    if contacts['company']
      contacts['company'].each do |c|
        u = User.find_by_id(c['id'])
        if u
          LiveDataMailer.alert(u.name, u.email, msg, asset, cond, dt) if c['email']
          sms_live_data_alert(u.name, u.phone_number, msg, asset, cond, dt) if c['mobile'] && u.phone_number
        end
      end
    end

    if contacts['other']
      contacts['other'].each do |c|
        LiveDataMailer.alert('there', c['email'], msg, asset, cond, dt) if c['email']
        sms_live_data_alert('there', c['mobile'], msg, asset, cond, dt) if c['mobile']
      end
    end
  end
end
