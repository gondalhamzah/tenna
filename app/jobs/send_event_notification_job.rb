require "#{Rails.root}/app/helpers/sms_message_helper"
require "#{Rails.root}/lib/aws_sns"
include AwsSns
include SmsMessageHelper

class SendEventNotificationJob < ActiveJob::Base
  include ActionView::Helpers
  # This is a CSV import job run by SideKiq. It parses the uploaded CSV file, creating assets for each row and saving
  queue_as :default

  def perform(event_id, maint_complete = false)
    event = Event.find(event_id)
    case event.event_type
      when "maint"
        if event.start_date < event.end_date and event.start_date > Time.now.utc and event.end_date > Time.now.utc
          EventMaintJob.set(wait_until: event.start_date).perform_later(event_id, true)
          EventMaintJob.set(wait_until: event.end_date).perform_later(event_id, false)
        end
        if !maint_complete
          event.user_ids.split(',').map(&:to_i).each do |user_id|
            user = User.find(user_id)
            EventMailer.maint_event_notification(event, event.asset, user).deliver if user.email.present?
            sms_maint_event_notification(user, event.asset, event) if user.phone_number.present?
          end
        else
            user = User.find(event.user_id)
            EventMailer.maint_complete_notification(event, event.asset, user).deliver if user.email.present?
            sms_maint_complete_event_notification(user, event.asset, event) if user.phone_number.present?
        end
      when "routing"
        recurring = RouteRecurring.where(fired_date: Time.now.strftime("%Y-%m-%d"), route_id: event.route.id).first
        if recurring.blank?
          recurring = RouteRecurring.create(
            route_id: event.route.id,
            fired_date: Time.now.strftime("%Y-%m-%d"),
            event_id: event.id
          )
        end
        driver_stop_count = DriverStop.where(route_recurring_id: recurring.id).count
        if driver_stop_count == 0
          event.route.valid_paths.each do |path|
            DriverStop.create(
              route_recurring_id: recurring.id,
              path_id: path.id
            )
          end
        end
        recurring.event_id = event.id
        recurring.save
        event.user_ids.split(',').map(&:to_i).each do |user_id|
          user = User.find(user_id)
          EventMailer.recurring_route_gen_notification(user, recurring, event).deliver if user.email.present?
          sms_recurring_route_gen_notification(user, recurring, event) if user.phone_number.present?
        end
      when "other"
        event.user_ids.split(',').map(&:to_i).each do |user_id|
          user = User.find(user_id)
          EventMailer.other_event_notification(user, event).deliver if user.email.present?
          sms_other_event_notification(user, event) if user.phone_number.present?
        end
    end
    if [nil, '', 'null'].include? event.rec_type and event.start_date < Time.now.utc + 30.minutes
      event.update(processed: true)
    end
  end
end
