class ProjectExporterJob < ActiveJob::Base
  queue_as :project_export

  def perform(project_export, export_project_only = false, export_assets_only = false, filtered_assets_result = nil, browser_timezone = nil, filtered_assets = nil, exported_at = nil, is_filtered = false)
    project_export.update(status: 'processing')
    csv = ProjectExporter.export(project_export.user, export_project_only, export_assets_only, filtered_assets_result, browser_timezone, filtered_assets, exported_at, is_filtered)
    project_export.update(status: 'ready', exported_at: Time.now, file: File.open(csv))
    File.unlink(csv)
  end
end
