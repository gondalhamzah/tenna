require 'sidekiq-scheduler'

class EventCheckJob
  include Sidekiq::Worker
  sidekiq_options queue: 'events'

  def perform
    # require "ice_cube"
    valid_events = Event.where("start_date BETWEEN ? AND ?", Time.now.utc, Time.now.utc + 45.minutes)
      .where(rec_type: [nil, '', 'null'], processed: false).pluck('id')

    events = Event.where.not(rec_type: [nil, '', 'null', 'none'])

    # events.each do |ev|
    #   next if (Event.where(event_pid: ev.id).where("date(start_date)=?", Time.now.utc.to_date.to_formatted_s(:db)).count() > 0)
    #   next if (ev.end_date.year != 9999 and ev.end_date < Time.now.utc)
    #
    #   # "day_3___" - each three days
    #   # "month_2___" - each two months
    #   # "month_1_1_2_" - second Monday of each month
    #   # "week_2___1,5" - Monday and Friday of each second week
    #
    #   # type - the type of repetition: 'day','week','month','year'.
    #   # count - the interval between events in the "type" units.
    #   # day and count2 - define a day of a month ( first Monday, third Friday, etc ).
    #   # days - a comma-separated list of affected week days.
    #   # extra - extra info that can be used to change the presentation of recurring details.
    #
    #   rec_type, extra = ev.rec_type.split('#')
    #   type, count, day, count2, days = rec_type.split('_')
    #   schedule = IceCube::Schedule.new(now = ev.start_date) do |s|
    #     if type == 'day'
    #       rule = IceCube::Rule.daily(count)
    #     elsif type == 'week'
    #       rule = IceCube::Rule.weekly(count)
    #     elsif type == 'month'
    #       rule = IceCube::Rule.monthly(count)
    #     elsif type == 'year'
    #       rule = IceCube::Rule.yearly(count)
    #     end
    #     rule = rule.day_of_week(day.to_i=>[count.to_i]) if !count.blank? and !day.blank?
    #     rule = rule.day(days.split(',').map(&:to_i)) if !days.blank?
    #     rule = rule.count(extra.to_i) if !extra.blank? and extra != 'no'
    #     s.add_recurrence_rule(rule)
    #     valid_events << ev.id if s.occurs_between?(Time.now.utc, Time.now.utc + 45.minutes)
    #   end
    # end

    Event.where("id IN (?)", valid_events).each do |event|
      # make sub event and mark it as processed
      _event = event
      if [nil, '', 'null'].include? event.rec_type
        event.processed = true
        event.save
      else
        now = Time.now.utc
        start_date = event.start_date
        end_date = event.end_date
        start_date = start_date.change({year: now.year, month: now.month, day: now.day})
        start_date
        _event = Event.create(
          start_date: start_date,
          end_date: start_date + event.event_length.seconds,
          event_length: start_date.to_time.to_i + Time.now.in_time_zone(event.timezone).utc_offset,
          event_pid: event.id,
          route_id: event.route_id,
          event_type: event.event_type,
          asset_id: event.asset_id,
          text: event.text,
          user_ids: event.user_ids,
          timezone: event.timezone,
          user_id: event.user_id,
          processed: true
        )
      end
      SendEventNotificationJob.perform_later(_event.id)
    end
  end
end
