require 'sidekiq-scheduler'
class AvailableOnJob
  include Sidekiq::Worker
  sidekiq_options queue: 'events'

  def perform
    Asset.all.each do |asset|
      if asset.available_on
        if Date.today() == asset.available_on
          Hook.trigger_available_on_asset('avaiable_on', asset)
        end
      end
    end
  end
end
