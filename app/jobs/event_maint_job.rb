class EventMaintJob < ActiveJob::Base
  queue_as :events

  def perform(event_id, maint)
    event = Event.find(event_id)
    asset = event.asset
    Chewy.strategy(:atomic) do
      asset.update_columns(maintenance: maint)
      maint_str = maint ? 'requested' : 'completed'
      AssetsVersion.transaction do
        AssetsVersion.create!(asset_id: asset.id, field_name: "maintenance #{maint_str}", from: '', to: '',
            whodunnit: event.user_id, project_id: asset.try(:project).try(:id), group_id: asset.try(:group).try(:id),
            company_id: asset.company_id, action: 'update')
      end
    end
  end
end
