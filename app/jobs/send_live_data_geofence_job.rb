require "#{Rails.root}/app/helpers/sms_message_helper"
require "#{Rails.root}/lib/aws_sns"
include AwsSns
include SmsMessageHelper

class SendLiveDataGeofenceJob < ActiveJob::Base
  include ActionView::Helpers
  queue_as :default

  def perform(asset_id, geo_fence_id, msg, dt)
    asset = Asset.find_by_id(asset_id)
    company_id = asset.company_id
    geo_fence = GeoFence.find(geo_fence_id)

    geo_fence.geo_fence_company_contacts.each do |c|
      u = User.find_by_id(c.user_id)
      if u
        LiveDataMailer.geofence_alert(u.name, u.email, msg, asset, dt) if c.email
        sms_live_data_geofence_alert(u.name, u.phone_number, msg, asset, dt) if c.text && u.phone_number
      end
    end

    geo_fence.geo_fence_other_contacts.each do |c|
      LiveDataMailer.geofence_alert("there", c.email, msg, asset, dt) if c.email && c.email_enabled
      sms_live_data_geofence_alert("there", c.phone, msg, asset, dt) if c.phone && c.phone_enabled
    end
  end
end
