class LiveDataMqttJob < ActiveJob::Base
  queue_as :default

  def perform(company_id, payload)
    token = MqttJwtGenerator.new(company_id).perform
    puts token
    puts ENV['MQTT_HOST']
    puts ENV['MQTT_PORT']
    puts SecureRandom.hex(4)
    MQTT::Client.connect(
      :host => ENV['MQTT_HOST'],
      :username => token,
      :version => '3.1',
      :client_id => SecureRandom.hex(4),
      :keep_alive => 999999,
      :ssl => true,
      :port => ENV['MQTT_PORT'],
      :password => 'jwt2'
    ) do |c|
      puts '*************************'
      puts 'Connected'
      puts '*************************'

      # message = {
      #     asset_id: asset_id,
      #     severity: cond.severe_str.downcase,
      #     type: cond.gauge_alert_group.gauge.name,
      #     # value: payload["value_numeric"],
      #     value: msg,
      #     unit: payload["unit"],
      #     # description: msg,
      #     timestamp: Time.now
      # }

      c.publish("/#{company_id}/notifications", payload.to_json)

      puts '*************************'
      puts 'Message Published'
      puts '*************************'
    end
  end
end
