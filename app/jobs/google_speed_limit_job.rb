class GoogleSpeedLimitJob < ActiveJob::Base
  queue_as :default

  def perform(company_id, points_str, asset_id, speed_payload, cond_ids)
    prev_speed = $redis.get "#{asset_id}-speed-limit"
    prev_speed = prev_speed.to_f
    return if !prev_speed.nil? && prev_speed == speed_payload["value_numeric"]

    url = "https://roads.googleapis.com/v1/speedLimits?path=#{points_str}&units=MPH&key=#{ENV['GOOGLE_SPEED_LIMIT_API_KEY']}"
    response = HTTParty.get(url, :headers => {:referer => "http://dl1.tenna.com/bot.html "})

    if response.code == 200
      res = JSON.parse response.body
      limits = res["speedLimits"]
      if limits and limits.length > 0
        cond_ids.each  do |cond_id|
          # puts "@" * 10
          # puts limits[0]
          # puts "@" * 10
          cond = GaugeCondition.find(cond_id)
          if limits[0]["speedLimit"] < speed_payload["value_numeric"] - cond.values['number'].to_f
            LiveDataMqttJob.perform_now(
              company_id,
              [cond.generate_message_and_log(speed_payload)]
            )
          end

          $redis.set "#{asset_id}-speed-limit", speed_payload["value_numeric"]
          $redis.expire "#{asset_id}-speed-limit", 60 * 5 # recheck after 5 minutse
        end
      end
    end
  end
end
