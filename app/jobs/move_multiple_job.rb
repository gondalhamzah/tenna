class MoveMultipleJob < ActiveJob::Base
  queue_as :default

  def perform(authorized_group, project)
    begin
      authorized_group.update(project_id: project.id)
    rescue => e
      puts "In rescue, error: #{e.backtrace}"
    end
  end
end