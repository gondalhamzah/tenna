class TriggerJob < ActiveJob::Base
  queue_as :default

  def perform(hook_ids, data)
  	hooks = Hook.where(id: hook_ids)
    hooks.each do |hook|
      RestClient.post(hook.target_url, data) do |response, request, result|
        if response.code.eql? 410
          hook.destroy
        end
      end
    end
  end
end
