class NestedAssociationFileSizeValidator < ActiveModel::EachValidator

  # Custom validator that works around Rails bug that checks associated file sizes, including those marked
  # for deletion.

  def initialize(options)
    unless options[:maximum].present?
      raise ArgumentError, ':maximum is a required option'
    end

    super
  end

  def validate_each(record, attribute, value)
    not_marked_for_destruction = value.reject(&:marked_for_destruction?)

    if not_marked_for_destruction.any? { |o| o.file.present? && o.file.size > options[:maximum] }
      record.errors[attribute] << (options[:message] || "The maximum file size of #{attribute} is #{ActiveSupport::NumberHelper.number_to_human_size(options[:maximum])}")
    end
  end

end
