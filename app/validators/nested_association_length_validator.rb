class NestedAssociationLengthValidator < ActiveModel::EachValidator

  # Custom validator that works around Rails bug that checks number of associated files before considering those marked
  # for deletion.

  def initialize(options)
    unless options[:maximum].present?
      raise ArgumentError, ':maximum is a required option'
    end

    super
  end

  def validate_each(record, attribute, value)
    not_marked_for_destruction = value.reject(&:marked_for_destruction?)

    if not_marked_for_destruction.length > options[:maximum]
      record.errors[attribute] << (options[:message] || "The maximum number of #{attribute} is #{options[:maximum]}")
    end
  end

end
