class ImagePreviewInput < SimpleForm::Inputs::FileInput

  # Custom input that renders asset photos, with delete option and cache field to allow file to be associated with
  # asset, even after validation errors.

  def input(wrapper_options = nil)
    # :preview_version is a custom attribute from :input_html hash, so you can pick custom sizes
    version = input_html_options.delete(:preview_version)
    out = ActiveSupport::SafeBuffer.new # the output buffer we're going to build
    # check if there's an uploaded file (eg: edit mode or form not saved)
    if object.send("#{attribute_name}?")
      # append preview image to output
      out << template.image_tag(object.send(attribute_name).tap { |o| break o.send(version) if version }.send('url'), class: 'responsive-img')
    else
      out << template.image_tag('/img-no-image-list-view.jpg', class: 'responsive-img')
    end

    out << template.link_to(template.image_tag('icn-upload-cancel.svg'), '#', :class => "destroy-#{attribute_name}")

    # allow multiple submissions without losing the tmp version
    out << @builder.hidden_field("#{attribute_name}_cache").html_safe

    out << @builder.hidden_field('position', class: "image_position").html_safe
    out << @builder.hidden_field('_destroy').html_safe
  end

end
