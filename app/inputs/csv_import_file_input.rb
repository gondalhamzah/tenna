class CsvImportFileInput < SimpleForm::Inputs::FileInput
  self.default_options = { label: false, required: false }

  def input(wrapper_options = nil)
    template.content_tag :div, class: 'file-field input-field' do
      safe_join([
        template.content_tag(:div, class: 'btn'){
          safe_join([template.content_tag(:span, label_text), super])
        },
        template.content_tag(:div, class: 'file-path-wrapper'){
          template.text_field_tag nil, nil, class: 'file-path validate'
        }
      ])
    end
  end
end