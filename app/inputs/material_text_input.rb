class MaterialTextInput < SimpleForm::Inputs::TextInput

  # Force textareas to have classes required for Materialize styling

  def input(wrapper_options = nil)
    input_html_options[:class].push(:'materialize-textarea')

    super
  end
end
