class MaterialDateInput < SimpleForm::Inputs::StringInput

  # Force date inputs to have classes required for Materialize styling
  def input(wrapper_options = nil)
    input_html_options[:class].push(:datepicker)
    input_html_options[:type] = :date

    super
  end
end
