class PdfPreviewInput < SimpleForm::Inputs::FileInput

  # Custom input that renders asset PDF links, with delete option and cache field to allow file to be associated with
  # asset, even after validation errors.

  def input(wrapper_options = nil)
    out = ActiveSupport::SafeBuffer.new # the output buffer we're going to build

    pdf = object.send(attribute_name)

    # append preview image to output
    out << template.link_to(pdf.file.filename, pdf.url, class: 'pdf-link', target: '_blank')
    out << " (#{template.number_to_human_size(pdf.file.size)})"

    out << template.link_to(template.image_tag('icn-upload-cancel.svg'), '#', :class => "destroy-#{attribute_name}")

    # allow multiple submissions without losing the tmp version
    out << @builder.hidden_field("#{attribute_name}_cache").html_safe

    out << @builder.hidden_field('_destroy').html_safe
  end

end
