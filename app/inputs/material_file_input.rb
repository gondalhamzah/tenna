class MaterialFileInput < SimpleForm::Inputs::FileInput

  # Force file inputs to have classes required for Materialize styling

  def input(wrapper_options = nil)
    icon = input_html_options.delete(:icon)
    icon ||= 'icon-attach-file'

    merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)

    markup = '<div class="file-field input-field">' +
        '<div class="waves-effect waves-light btn">' +
        "<i class='#{icon} left'></i>#{label_text}" +
        @builder.file_field(attribute_name, merged_input_options) +
        '</div>' +
        '<div class="file-path-wrapper">' +
        '<input class="file-path validate" type="text">' +
        '</div>' +
        '</div>'

    markup.html_safe
  end
end
