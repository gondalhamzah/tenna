class GroupedCollectionSelectInput < SimpleForm::Inputs::GroupedCollectionSelectInput

  # Force all select inputs to include the browser-default class, which disables Materialize stying
  def input_html_classes
    super.push('browser-default')
  end
end
