require "application_responder"
require 'csv'
class ApplicationController < ActionController::Base
  before_filter :set_current_user
  around_filter :set_time_zone
  before_action :track_action
  before_action :configure_permitted_parameters, if: :devise_controller?

  self.responder = ApplicationResponder
  respond_to :html

  # Track who is responsible for changes using paper_trail
  before_filter :set_paper_trail_whodunnit

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  #before_filter :configure_permitted_parameters, if: :devise_controller?

  # Allows us to cookie a user's location if they set lat, lng & addr query params,
  # which are geocoded client-side.
  before_filter :set_location

  def access_denied(exception)
    redirect_to root_path, alert: exception.message
  end

  def set_current_user
    User.current = current_user
    # Chewy.settings = {prefix: "shemp3_development_#{current_user.company_id}"}
  end

  # def after_sign_in_path_for(resource)
  #   marketplace_path
  # end

  rescue_from CanCan::AccessDenied do |exception|
    if user_signed_in?
      respond_to do |format|
        format.json { render json: {error: exception.message}, status: :forbidden }
        format.any { redirect_to root_url, alert: exception.message }
      end
    else
      store_location_for(Devise.default_scope, request.fullpath)
      redirect_to new_user_session_url, alert: t('devise.failure.unauthenticated')
    end
  end

  def title(text)
    content_for :title, text
  end


  protected
  def authenticate_superadmin!
    authenticate_user!
    raise ActionController::RoutingError.new('Not Found') unless current_user.admin?
  end

  def zapier_login!
    @user = User.find_by_email(params[:user][:email])
    if @user
      unless @user.valid_password?(params[:user][:password])
        render unauthenticated: 'unable to signin to tenna.', :status => 403
      end
      @user
    else
      render :json => 'unable to signin', :status => 403
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:first_name, :last_name, :email, :company_name, :phone_number, :password, :password_confirmation, :terms_of_service, :subscription_opt) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:title, :first_name, :last_name, :email, :company_name, :phone_number, :password, :password_confirmation, :current_password, :subscription_opt) }
    devise_parameter_sanitizer.for(:invite).concat([:first_name, :last_name, :role])
    devise_parameter_sanitizer.for(:accept_invitation).concat([:terms_of_service, :subscription_opt])
  end

  def config_projects
    return unless user_signed_in?
    @projects = []

    if current_user.role_gte_company_admin?
      @projects = current_user.company.projects
    else
      @projects = current_user.projects
    end
  end

  def config_groups
    return unless user_signed_in?
    @groups = []

    # @TODO: check only company admin can link to group?
    if current_user.private?
      @groups = current_user.company.groups.available
    end
  end

  def set_location
    if params[:lat].present? && params[:lng].present?
      results = Geocoder.search("#{params[:lat]},#{params[:lng]}")
    elsif params[:addr].present?
      results = GenericGeocoder.search(params[:addr])
    end

    if results.present?
      result = results[0]
      cookies[:coordinates] = { lat: result.latitude, lng: result.longitude, accuracy: params[:accuracy].present? ? params[:accuracy] : 0.0 }.to_json
      cookies[:zip_code] = result.postal_code
    end
  end
  private

  def set_time_zone
    old_time_zone = Time.zone
    Time.zone = browser_timezone if browser_timezone.present?
    yield
  ensure
    Time.zone = old_time_zone
  end

  def after_sign_in_path_for(resource)
    if resource == :user
      dashboard_path
    else
      super
    end
  end

  def after_sign_out_path_for(resource)
    if resource == :user
      if Rails.env.development? || Rails.env.test?
        root_path
      elsif Rails.env.staging?
        'http://tennamarketing.lumenoweb.com/'
      elsif Rails.env.production?
        'http://www.tenna.com'
      end
    else
      super
    end
  end

  def browser_timezone
    cookies['browser.timezone']
  end

  def track_action
    ahoy.track "Viewed #{controller_path}##{action_name}", params: request.path_parameters
  end
end
