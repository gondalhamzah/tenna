class VideoUsagesController < ApplicationController

  def add_video_usage
    video_usage_created = VideoUsage.create!(user_id: current_user.try(:id), video_id: params[:video_id], when_watched: Time.now())
    video = Video.find params[:video_id]
    video_views = VideoUsage.where video_id: video.id
    if video_views.count == 1
      Hook.trigger_hooks_video_view('Video', video, current_user) 
    end
    respond_to do |format|
      format.json { render :json => 'success' }
    end
    rescue_from ActiveRecord::Errors unless video_usage_created
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def video_usage_params
      params.require(:video_usage).permit(:user_id, :video_id, :when_watched)
    end
end
