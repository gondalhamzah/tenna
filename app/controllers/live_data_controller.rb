class LiveDataController < ApplicationController
  include BuildMapMarkers
  include AssetsHelper
  include CategoriesHelper

  before_action :authenticate_user!
  before_action :live_data_subscription, only: [:index]

  def index
    authorize! :read, Gauge.all.first

    if params[:user].blank?
      @tracking_code = params[:tracking_code]
      @assign_tracking_code = !@tracking_code.nil?

    if current_user.private?
      search_options = {company: current_user.company}
    else
      search_options = {creator: current_user}
    end
    if params[:filter_search]
      filters = true
    else
      filters = false
    end
    params[:filter_search] ||= {}
    params[:filter_search][:without_tracking_code] = @assign_tracking_code
    if params[:filter_search][:distance_zip]
      if params[:filter_search][:distance_zip].split(',').count == 2
        splitted_zip_code = params[:filter_search][:distance_zip].split(',')
        distance_zip = splitted_zip_code.first
        country_name = splitted_zip_code.second
        params[:filter_search][:distance_zip] = distance_zip
        params[:filter_search][:country] = country_name
      end
    end

      filterable_assets = FilterableAssets.new(
        user: current_user,
        search_options: search_options,
        params: params,
        zip_code: cookies[:zip_code],
        live_data: true
      )

      map_markers = ZipCode.map_markers_for(filterable_assets.filtered_assets_scope)
      @markers = build_map_markers(map_markers)

      params[:page] ||= 1
      @filter_search = filterable_assets.filter_search

      # @assets = filterable_assets.assets.page(params[:page]).per(@filter_search.page_size.to_i)
      @assets = filterable_assets.assets.page(params[:page]).per(100000)
      session[:filterable_assets_params] = params
    else
      render :nothing => true, :status => 200
    end

    if filters
      respond_to do |format|
        format.js { render json: {data: to_map_data(@assets) }}
      end
    end
  end

  def get_mqtt_token
    company_id = current_user.company.id
    token = MqttJwtGenerator.new(company_id, current_user.id).perform
    respond_to do |format|
      format.js { render json: { token: token }}
    end
  end

  def live_data_subscription
    if current_user.company
      company = current_user.company
      @company_livedata_subscription = company.subscriptions.where(category: 'livedata',state: 'active',payment: true).first
    end
  end

  def send_notification
    SendLiveDataNotificationJob.perform_now(params[:notification])
    respond_to do |format|
      format.js { render nothing: true, status: 200 }
    end
  end


end
