class MarksController < ApplicationController
  before_action :authenticate_user!
  before_action :load_markable, only: [:create, :destroy]

  # Currently, the only marks are favorites, where a user can favorite an asset.

  def create
    @mark = @markable.marks.new(mark_params)
    @mark.marker = current_user
    authorize! :create, @mark

    @mark.save

    if request.xhr?
      render json: {action: action_url(@mark), method: :delete}
    else
      respond_with @mark, location: -> { url_for(get_markable_url(@mark)) }
    end

  end

  def destroy
    @mark = Mark.find(params[:id])
    @mark.destroy

    if request.xhr?
      render json: {action: action_url(Mark.new), method: :post}
    else
      respond_with @mark, location: -> { url_for(get_markable_url(@mark)) }
    end
  end

  private

  def action_url(mark)
    polymorphic_path([@markable.becomes(Asset), mark])
  end

  def mark_params
    params.require(:mark).permit(:mark)
  end

  def load_markable
    klass = [Asset].detect { |c| params["#{c.name.underscore}_id"] }
    @markable = klass.find(params["#{klass.name.underscore}_id"])
  end

  def get_markable_url(mark)
    markable = ([Equipment, Material].include? mark.markable.class) ? mark.markable.becomes(Asset) : mark.markable
    url_for(markable)
  end

end
