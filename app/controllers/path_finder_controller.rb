class PathFinderController < ApplicationController
  respond_to :html, :js, only: :route_history
  include PathFinderHelper
  include AwsSns
  include SmsMessageHelper

  before_action :authenticate_user!, except: [:show, :show_multi]
  before_action :config_projects, only: [:index, :edit]

  def search_by_name
    @company_routes = current_user.company.routes

    if params[:name].present?
      @company_routes = @company_routes.where("lower(name) LIKE ?", "%#{params[:name].downcase}%")
    end

    render json: @company_routes.select(:id, :user_id, :name, :slug, :created_at)
  end

  def index
    @company = current_user.company
    @users = @company.users
    @company_routing_subscription = @company.subscriptions.where(category: 'routing').first
    @number_of_vehicles = @company_routing_subscription&.number_of_units || 0

    if @company_routing_subscription.present? && @company_routing_subscription.state != 'active'
      @subscription_not_active = true
      render 'not_subscribed', layout: 'application' and return
    end

    if @number_of_vehicles == 0
      render 'not_subscribed', layout: 'application' and return
    end

    if current_user.private?
      search_options = { company: current_user.company }
    else
      search_options = { creator: current_user }
    end

    params[:filter_search] ||= {}

    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: search_options,
      params: params,
      zip_code: cookies[:zip_code]
    )

    params[:page] ||= 1
    @filter_search = filterable_assets.filter_search
    routing_assets = filterable_assets.routing_assets.page(params[:page]).per(9999)
    @path_finder_assets = routing_assets.group_by(&:project_id)
  end

  def manage_routes
    @company = current_user.company
    @users = @company.users
    @company_routing_subscription = @company.subscriptions.where(category: 'routing').first
    @number_of_vehicles = @company_routing_subscription&.number_of_units || 0

    if @company_routing_subscription.present? && @company_routing_subscription.state != 'active'
      @subscription_not_active = true
      render 'not_subscribed', layout: 'application' and return
    end

    if @number_of_vehicles == 0
      render 'not_subscribed', layout: 'application' and return
    end

    if current_user.private?
      search_options = { company: current_user.company }
    else
      search_options = { creator: current_user }
    end

    params[:filter_search] ||= {}

    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: search_options,
      params: params,
      zip_code: cookies[:zip_code]
    )

    params[:page] ||= 1
    @filter_search = filterable_assets.filter_search
    routing_assets = filterable_assets.routing_assets.page(params[:page]).per(9999)
    @path_finder_assets = routing_assets.group_by(&:project_id)
  end

  def waypoint_options
    render partial: 'waypoint_options',
           layout: false,
           locals: {
             waypoint_id: params[:waypoint_id],
             waypoint_type: params[:waypoint_type],
             start_time: nil,
             end_time: nil,
             vehicle_load: 0,
             priority: 'Regular',
             service_notes: '',
             time_on_site: '',
             notify_customer: false
           }
  end

  def project_assets
    project_id = params[:id]

    if current_user.private?
      search_options = { company: current_user.company }
    else
      search_options = { creator: current_user }
    end

    params[:filter_search] = JSON.parse(params[:filter_options])
    params[:filter_search]['project'] = project_id

    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: search_options,
      params: params,
      zip_code: cookies[:zip_code]
    )

    params[:page] ||= 1
    @filter_search = filterable_assets.filter_search
    @assets = filterable_assets.routing_assets.page(params[:page]).per(9999)

    render partial: 'project_assets', layout: false, locals: { assets: @assets }
  end

  def create
    route_generation_errors = generate_routes
    if route_generation_errors.any?
      render json: { errors: route_generation_errors }, status: 422 and return
    end

    route_saving_errors = save_created_routes
    if route_saving_errors.any?
      render json: { errors: route_saving_errors }, status: 422 and return
    end

    ids = []

    # create event, recurring and driver stop
    @generated_routes.each do |route|
      start_time = "#{Time.now.strftime("%Y-%m-%d")} #{route.start_time.strftime("%H:%M:%S")}".in_time_zone(Time.zone.name).utc
      end_time = "#{Time.now.strftime("%Y-%m-%d")} #{route.end_time.strftime("%H:%M:%S")}".in_time_zone(Time.zone.name).utc
      event = Event.create(
        start_date: start_time,
        end_date: end_time,
        event_type: "routing", # for routing
        text: route.name,
        route_id: route.id,
        user_id: current_user.id,
        timezone: Time.zone.name,
        user_ids: route.user_id.to_s
      )

      recurring = RouteRecurring.create(
        route_id: route.id,
        fired_date: Time.now.strftime("%Y-%m-%d"),
        event_id: event.id
      )
      ids << "#{route.slug}:#{recurring.fired_date}"

      route.valid_paths.each do |path|
        DriverStop.create(
          route_recurring_id: recurring.id,
          path_id: path.id
        )
      end
      if routing_subscription_verified.present?
        SendEventNotificationJob.perform_later(event.id)
      end
    end

    render json: { ids: ids }
  end

  def edit
    @company = current_user.company

    @route = Route.find(params[:id])
    if @route.nil? or @route.company != @company
      render status: 404 and return
    end

    @users = @company.users
    @company_routing_subscription = @company.subscriptions.where(category: 'routing').first
    @number_of_vehicles = @company_routing_subscription&.number_of_units || 0

    if @company_routing_subscription.present? && @company_routing_subscription.state != 'active'
      @subscription_not_active = true
      render 'not_subscribed', layout: 'application' and return
    end

    if @number_of_vehicles == 0
      render 'not_subscribed', layout: 'application' and return
    end

    if current_user.private?
      search_options = { company: current_user.company }
    else
      search_options = { creator: current_user }
    end

    params[:filter_search] ||= {}

    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: search_options,
      params: params,
      zip_code: cookies[:zip_code]
    )

    params[:page] ||= 1
    @filter_search = filterable_assets.filter_search
    routing_assets = filterable_assets.routing_assets.page(params[:page]).per(9999)
    @path_finder_assets = routing_assets.group_by(&:project_id)

    # implement algorithm here
    @route_group = @route.route_group

    @edit_errors = {}
    @driver_errors = []
    @asset_errors = []
    @project_errors = []
    @asset_projects = []

    # get driver metadata and verify that all drivers exist in the system
    @driver_metadata = JSON.parse(@route_group.serialized_driver_metadata)['drivers']
    @driver_metadata.each_with_index do |driver, idx|
      user = User.where(id: driver['id']).not_deleted.first
      if user.blank?
        user_name = User.find(driver['id'])&.name
        @driver_errors << user_name if user_name
        @driver_metadata.delete_at idx
      end
    end
    @selected_driver_ids = @driver_metadata.collect {|d| d['id']}

    # verify projects exist
    @selected_projects = @route_group.paths.projects.to_a
    @selected_projects.each_with_index do |project_waypoint, idx|
      project = Project.where(id: project_waypoint.waypoint_id).not_deleted.first
      if project.blank?
        project_name = Project.find(project_waypoint.waypoint_id)&.name
        @project_errors << project_name if project_name
        @selected_projects.delete_at idx
      end
    end
    @selected_project_ids = @selected_projects.collect {|p| p['waypoint_id']}

    # verify assets exist
    @selected_assets = @route_group.paths.assets.to_a
    @selected_assets.each_with_index do |asset_waypoint, idx|
      asset = Asset.where(id: asset_waypoint.waypoint_id).not_sold_deleted.first
      if asset.blank?
        asset_title = Asset.find(asset_waypoint.waypoint_id)&.title
        @asset_errors << asset_title if asset_title
        @selected_assets.delete_at idx
      end
    end
    @selected_asset_ids = @selected_assets.collect {|p| p['waypoint_id']}

    # collect all assets' project ids
    @asset_project_ids = @selected_assets.collect {|a| Asset.find(a.waypoint_id)&.project_id}
  end

  def update
    @initial_route = Route.find(params[:id])

    @route_group = @initial_route.route_group

    route_generation_errors = generate_routes
    if route_generation_errors.any?
      render json: { errors: route_generation_errors }, status: 422 and return
    end

    route_saving_errors = save_updated_routes
    if route_saving_errors.any?
      render json: { errors: route_saving_errors }, status: 422 and return
    end

    @route_group.routes.each do |route|
      RouteRecurring.transaction do
        RouteRecurring.where(route_id: route.id).each do |rr|
          rr.driver_stops.update_all(route_recurring_id: nil)
        end
      end
    end

    @empty_routes = []
    ids = []

    @generated_routes.each do |route|
      route.touch
      @empty_routes << route if route.paths.count == 0

      event = Event.where(route_id: route.id).first
      recurring = RouteRecurring.where(route_id: route.id, event_id: event.try(:id)).first

      if event.blank?
        event = Event.create(
          start_date: "#{Time.now.strftime("%Y-%m-%d")} #{route.start_time}",
          end_date: "#{Time.now.strftime("%Y-%m-%d")} #{route.end_time}",
          event_type: "routing", # for routing
          text: route.name,
          timezone: Time.zone.name,
          user_id: current_user.id,
          route_id: route.id,
          user_ids: route.user_id.to_s
        )

        RouteRecurring.destroy_all(route_id: route.id)
        recurring = RouteRecurring.create(
          route_id: route.id,
          fired_date: Time.now.strftime("%Y-%m-%d"),
          event_id: event.id
        )
      end

      ids << "#{route.slug}:#{recurring.fired_date}"

      route.valid_paths.each do |path|
        DriverStop.create(
          route_recurring_id: recurring.id,
          path_id: path.id
        )
      end
    end

    render json: { ids: ids }
  end

  def show
    route = Route.find_by_slug!(params[:id])
    @route_recurring = RouteRecurring.includes(:route).where(
      route_id: route.id,
      fired_date: params[:fired_date]
    ).first
  end

  def show_multi
    ids_list = params[:ids].split(',')
    @route_recurrings = []
    ids_list.each do |ids|
      slugs = ids.split(':')
      route = Route.find_by_slug(slugs[0])
      @route_recurrings.push(
        RouteRecurring.includes(:route).where(
          route_id: route.id,
          fired_date: slugs[1]
        ).first
      )
    end
  end

  def mark_as_done
    begin
      @driver_stop = DriverStop.includes(:path).find(params[:id])
      @driver_stop.done_timestamp = Time.now
      @driver_stop.done_lat = params[:lat]
      @driver_stop.done_lng = params[:lng]
      @driver_stop.save
      @path = @driver_stop.path
      @route = @path.route
      if @path.notify_customer
        asset = false
        project = if @path.waypoint_type == 'project'
                    Project.find(@path.waypoint_id)
                  elsif @path.waypoint_type == 'asset'
                    asset = Asset.find(@path.waypoint_id)
                    asset.project
                  end
        if project.present? and routing_subscription_verified.present?
          EventMailer.path_visited_notification(@route, project, asset, @driver_stop, @path).deliver if project.contact_email.present?
          sms_path_visited_notification(@route, project, asset, @driver_stop, @path) if project.contact_sms_number.present?
        end
      end
    rescue => error
      p error.backtrace
      render json: { status: :error }, status: 422 and return
    end
    Hook.trigger_route_stop('Route', @route, @path)
    render json: { status: :success }
  end

  def mark_as_undone
    begin
      @driver_stop = DriverStop.find(params[:id])
      @driver_stop.done_timestamp = nil
      @driver_stop.done_lat = nil
      @driver_stop.done_lng = nil
      @driver_stop.save
    rescue
      render json: { status: :error }, status: 422 and return
    end

    render json: { status: :success }
  end

  def mark_all_as_undone
    begin
      @route_recurring = RouteRecurring.find_by_id(params[:id])

      @route_recurring.driver_stops.each do |driver_stop|
        driver_stop.done_timestamp = nil
        driver_stop.done_lat = nil
        driver_stop.done_lng = nil
        driver_stop.save
      end
    rescue
      render json: { status: :error }, status: 422 and return
    end

    render json: { status: :success }
  end

  def route_history_search
    @company = current_user.company
    @recurrings = RouteRecurring.joins(:route).includes(:route)
                    .where("routes.company_id = #{@company.id}")
                    .order(created_at: :desc)

    if params[:route_history][:driver].present?
      @recurrings = @recurrings.where("routes.user_id=#{params[:route_history][:driver]}")
    end

    from_date = params[:route_history][:from_date].present? ? Date.parse(params[:route_history][:from_date]) : Date.parse('1/1/1900')
    to_date = params[:route_history][:to_date].present? ? Date.parse(params[:route_history][:to_date]) : Date.parse('1/1/2100')

    @recurrings = @recurrings.where(created_at: from_date.beginning_of_day..to_date.end_of_day)
                             .page(params[:route_history][:page] || 1).per(25)

    if params[:route_history][:project].present?
      matched_recurring_ids = DriverStop.joins(:path)
                                .where("paths.waypoint_type='project'")
                                .where("paths.waypoint_id=#{params[:route_history][:project]}")
                                .collect(&:route_recurring_id).uniq
      @recurrings = @recurrings.where(id: matched_recurring_ids)
    end
  end

  def preview
    @route_recurring = RouteRecurring.includes(:route).joins(:route)
                         .where(routes: { slug: params[:route_slug] }, fired_date: params[:fired_date])
                         .first

    render layout: false
  end

  def send_route
    @route_recurring = RouteRecurring.find(params[:recurring_route_id])
    @route = @route_recurring.route
    @fired_date = @route_recurring.fired_date

    driver = @route.user
    if params[:driver_notification_check].present? and routing_subscription_verified.present?
      EventMailer.route_generated_notification(driver, driver.email, @route, @fired_date).deliver if driver.email.present? unless Rails.env.test?
      sms_route_generated_notification(driver, driver.phone_number, @route, @fired_date) if driver.phone_number.present? unless Rails.env.test?
    end

    if params[:other_notification_check].present? and routing_subscription_verified.present?
      EventMailer.route_generated_notification(driver, params[:notify_email], @route, @fired_date).deliver if params[:notify_email].present? unless Rails.env.test?
      sms_route_generated_notification(driver, params[:notify_phone], @route, @fired_date) if params[:notify_phone].present? unless Rails.env.test?
    end

    render json: { status: :success }
  end

  def recurring
    render layout: 'dhtmlx'
  end

  def routing_subscription_verified
    @company = current_user.company
    @company_routing_subscription = @company.subscriptions.where(category: 'routing').first
  end

  private

  def generate_routes
    errors = []

    if params['path_finder'] && params['path_finder']['user']
      selected_driver_ids = params['path_finder']['user'].reject {|e| e.to_s.blank?}
    end
    if selected_driver_ids.blank?
      errors << 'Please select a Driver'
      return errors
    end
    drivers = selected_driver_ids.collect {|uid| User.find(uid)}

    if params['path_finder']['asset'].blank? && params['path_finder']['project'].blank?
      errors << 'Please select some assets'
      return errors
    end
    assets = []
    if params['path_finder']['asset']
      assets = params['path_finder']['asset'].collect {|k, v| k if v == '1'}.compact.map {|a_id| Asset.find(a_id)}
    end
    projects = []
    if params['path_finder']['project']
      projects = params['path_finder']['project'].collect {|k, v| k if v == '1'}.compact.map {|p_id| Project.find(p_id)}
    end

    @company = drivers.first.company

    visits = {}
    @asset_locations = {}
    assets.each_with_index do |asset, idx|
      lat = asset.current_lat || asset.project.address.lat
      lng = asset.current_lng || asset.project.address.lng
      visits["asset_order_#{idx}"] = {
        location: { name: asset.id.to_s, lat: lat, lng: lng }
      }
      visits["asset_order_#{idx}"]['start'] = params['waypoint_options_asset'][asset.id.to_s]['start_time'] if params['waypoint_options_asset'][asset.id.to_s]['start_time'].present?
      visits["asset_order_#{idx}"]['end'] = params['waypoint_options_asset'][asset.id.to_s]['end_time'] if params['waypoint_options_asset'][asset.id.to_s]['end_time'].present?
      visits["asset_order_#{idx}"]['load'] = params['waypoint_options_asset'][asset.id.to_s]['load'] if params['waypoint_options_asset'][asset.id.to_s]['load'].present?
      visits["asset_order_#{idx}"]['duration'] = params['waypoint_options_asset'][asset.id.to_s]['duration'].to_i.abs if params['waypoint_options_asset'][asset.id.to_s]['duration'].present?
      visits["asset_order_#{idx}"]['priority'] = params['waypoint_options_asset'][asset.id.to_s]['priority'].downcase
      @asset_locations[asset.id.to_s] = [lat, lng]
    end

    @project_locations = {}
    projects.each_with_index do |project, idx|
      lat = project.address.lat
      lng = project.address.lng
      visits["project_order_#{idx}"] = {
        location: { name: project.id.to_s, lat: lat, lng: lng }
      }
      visits["project_order_#{idx}"]['start'] = params['waypoint_options_project'][project.id.to_s]['start_time'] if params['waypoint_options_project'][project.id.to_s]['start_time'].present?
      visits["project_order_#{idx}"]['end'] = params['waypoint_options_project'][project.id.to_s]['end_time'] if params['waypoint_options_project'][project.id.to_s]['end_time'].present?
      visits["project_order_#{idx}"]['load'] = params['waypoint_options_project'][project.id.to_s]['load'] if params['waypoint_options_project'][project.id.to_s]['load'].present?
      visits["project_order_#{idx}"]['duration'] = params['waypoint_options_project'][project.id.to_s]['duration'].to_i.abs if params['waypoint_options_project'][project.id.to_s]['duration'].present?
      visits["project_order_#{idx}"]['priority'] = params['waypoint_options_project'][project.id.to_s]['priority'].downcase
      @project_locations[project.id.to_s] = [lat, lng]
    end

    time_vertification = PathFinderHelper.verify_start_end_times(visits)
    if time_vertification.key?(:error)
      errors << time_vertification[:error]
      return errors
    end

    fleet = {}
    @driver_data = {}
    @drivers_metadata = []
    drivers.each do |driver|
      driver_metadata = {}
      _key = driver.id.to_s
      driver_options = params['user_options'][_key]
      @driver_data[_key] = {
        start_time: driver_options['start_time'],
        end_time: driver_options['end_time'],
        name: driver_options['name']
      }
      start_location_address = driver_options['start_location'].empty? ? @company.address.formatted_address : driver_options['start_location']
      start_location = GenericGeocoder.search(start_location_address).first

      if start_location.nil?
        errors << 'Please enter valid start address for each driver'
        return errors
      end

      fleet["vehicle_#{driver.id}"] = { start_location: { id: "start_#{driver.id}", name: start_location.address, lat: start_location.latitude, lng: start_location.longitude } }

      end_location_address = driver_options['end_location']
      if end_location_address.present?
        end_location = GenericGeocoder.search(end_location_address).first

        if end_location.nil?
          errors << 'Please enter valid end address for each driver'
          return errors
        end

        fleet["vehicle_#{driver.id}"]['end_location'] = { id: "end_#{driver.id}", name: end_location.address, lat: end_location.latitude, lng: end_location.longitude }
      end
      fleet["vehicle_#{driver.id}"]['capacity'] = driver_options['capacity'].to_i.abs if driver_options['capacity'].present?
      fleet["vehicle_#{driver.id}"]['min_visits'] = 1

      driver_metadata[:id] = driver.id
      driver_metadata[:start_location] = start_location_address
      driver_metadata[:end_location] = end_location_address
      driver_metadata[:start_time] = @driver_data[_key][:start_time]
      driver_metadata[:end_time] = @driver_data[_key][:end_time]
      driver_metadata[:capacity] = driver_options['capacity']
      driver_metadata[:name] = driver.name
      driver_metadata[:route_name] = @driver_data[_key][:name]

      @drivers_metadata << driver_metadata
    end

    options = { balance: true, min_visits_per_vehicle: 1, min_vehicles: false }
    plan = { visits: visits, fleet: fleet, options: options }

    routific_api_key = @company.subscriptions.where(category: 'routing').first&.api_key
    if routific_api_key.blank?
      errors << 'Problem with subscription, please contact support'
      return errors
    end

    Routific.setToken(routific_api_key)
    @routific_route = Routific.getRoute(plan)

    Routific.setToken(ENV['ROUTIFIC_API_KEY']) # Reset to global token for Admin purposes

    return errors
  end

  def save_created_routes
    errors = []

    if @routific_route.nil?
      errors << 'A route could not be generated with the given constraints'
      return errors
    end

    @generated_routes = []

    if @routific_route.class == RoutificApi::Route
      if @routific_route.unserved.present?
        errors << 'Some paths cannot be visited within the constraints'
        return errors
      end

      ActiveRecord::Base.transaction do
        # create route group
        route_group = RouteGroup.create(
          serialized_driver_metadata: { drivers: @drivers_metadata }.to_json,
          filters: request.env['HTTP_REFERER'].sub(request.original_url, '')
        )

        @routific_route.vehicleRoutes.each do |vr_id, vr|
          driver_id_str = vr_id.sub('vehicle_', '')
          route = Route.new
          route.user_id = driver_id_str.to_i
          route.creator_id =current_user.id
          route.timezone = Time.zone.name
          route.company_id = @company.id
          route.name = @driver_data[driver_id_str][:name]
          route.start_time = @driver_data[driver_id_str][:start_time]
          route.end_time = @driver_data[driver_id_str][:end_time]
          route.route_group_id = route_group.id
          route.save

          # success
          vr.each_with_index do |waypoint, idx|
            if waypoint.location_id.start_with? 'start'
              # start
              waypoint_type = 'start'
              waypoint_id = 0
              waypoint_address = waypoint.location_name.gsub(/(\\n)+/, ', ')
              waypoint_notify_customer = false
            elsif waypoint.location_id.start_with? 'end'
              # end
              waypoint_type = 'end'
              waypoint_id = 0
              waypoint_address = waypoint.location_name.gsub(/(\\n)+/, ', ')
              waypoint_notify_customer = false
            else
              # paths
              waypoint_type = waypoint.location_id.start_with?('asset') ? 'asset' : 'project'
              waypoint_id = waypoint.location_name.to_i
              waypoint_lat = eval('@' + waypoint_type + '_locations')[waypoint.location_name][0]
              waypoint_lng = eval('@' + waypoint_type + '_locations')[waypoint.location_name][1]
              waypoint_address = Geocoder.search(eval('@' + waypoint_type + '_locations')[waypoint.location_name]).first.address.gsub(/(\\n)+/, ', ')
              waypoint_service_notes = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['service_notes']

              waypoint_time_on_site = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['duration']
              waypoint_priority = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['priority'].downcase
              waypoint_load = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['load']
              waypoint_begin_window = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['start_time']
              waypoint_end_window = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['end_time']
              waypoint_notify_customer = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['notify_customer']
            end

            Path.create(
              waypoint_type: waypoint_type,
              waypoint_id: waypoint_id,
              address: waypoint_address,
              route_id: route.id,
              sequence: idx,
              service_notes: waypoint_service_notes,
              time_on_site: waypoint_time_on_site,
              priority: waypoint_priority,
              load: waypoint_load,
              begin_window: waypoint_begin_window,
              end_window: waypoint_end_window,
              notify_customer: waypoint_notify_customer,
              lat: waypoint_lat,
              lng: waypoint_lng
            )
          end

          @generated_routes << route
        end
      end

      if @generated_routes.nil?
        errors << 'A route could not be generated with the given constraints'
        return errors
      end
    else
      errors << 'An error occurred when trying to calculate the route. <br>Please try changing your waypoint parameters or try again in some time.'
      return errors
    end

    return errors
  end

  def save_updated_routes
    errors = []

    if @routific_route.nil?
      errors << 'A route could not be generated with the given constraints'
      return errors
    end

    @generated_routes = []

    if @routific_route.class == RoutificApi::Route
      if @routific_route.unserved.present?
        errors << 'Some paths cannot be visited within the constraints'
        return errors
      end

      ActiveRecord::Base.transaction do
        # update route group with new data
        @route_group.update_attributes(serialized_driver_metadata: { drivers: @drivers_metadata }.to_json, filters: request.env['HTTP_REFERER'].sub(request.original_url, ''))

        # disassociate current paths from all routes in the route group
        @route_group.paths.update_all(route_id: nil)

        @routific_route.vehicleRoutes.each do |vr_id, vr|
          driver_id_str = vr_id.sub('vehicle_', '')

          route = Route.where(user_id: driver_id_str, route_group_id: @route_group.id)&.first || Route.new

          route.user_id = driver_id_str.to_i
          route.company_id = @company.id
          route.creator_id = current_user.id
          route.name = @driver_data[driver_id_str][:name]
          route.start_time = @driver_data[driver_id_str][:start_time]
          route.end_time = @driver_data[driver_id_str][:end_time]
          route.route_group_id = @route_group.id
          route.save

          # success
          vr.each_with_index do |waypoint, idx|
            if waypoint.location_id.start_with? 'start'
              # start
              waypoint_type = 'start'
              waypoint_id = 0
              waypoint_address = waypoint.location_name.gsub(/(\\n)+/, ', ')
              waypoint_notify_customer = false
            elsif waypoint.location_id.start_with? 'end'
              # end
              waypoint_type = 'end'
              waypoint_id = 0
              waypoint_address = waypoint.location_name.gsub(/(\\n)+/, ', ')
              waypoint_notify_customer = false
            else
              # paths
              waypoint_type = waypoint.location_id.start_with?('asset') ? 'asset' : 'project'
              waypoint_id = waypoint.location_name.to_i
              waypoint_lat = eval('@' + waypoint_type + '_locations')[waypoint.location_name][0]
              waypoint_lng = eval('@' + waypoint_type + '_locations')[waypoint.location_name][1]
              waypoint_address = Geocoder.search(eval('@' + waypoint_type + '_locations')[waypoint.location_name]).first.address.gsub(/(\\n)+/, ', ')
              waypoint_service_notes = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['service_notes']

              waypoint_time_on_site = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['duration']
              waypoint_priority = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['priority'].downcase
              waypoint_load = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['load']
              waypoint_begin_window = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['start_time']
              waypoint_end_window = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['end_time']
              waypoint_notify_customer = params['waypoint_options_' + waypoint_type][waypoint_id.to_s]['notify_customer']
            end

            Path.create(
              waypoint_type: waypoint_type,
              waypoint_id: waypoint_id,
              address: waypoint_address,
              route_id: route.id,
              sequence: idx,
              service_notes: waypoint_service_notes,
              time_on_site: waypoint_time_on_site,
              priority: waypoint_priority,
              load: waypoint_load,
              begin_window: waypoint_begin_window,
              end_window: waypoint_end_window,
              notify_customer: waypoint_notify_customer,
              lat: waypoint_lat,
              lng: waypoint_lng
            )
          end

          @generated_routes << route
        end
      end

      if @generated_routes.nil?
        errors << 'A route could not be generated with the given constraints'
        return errors
      end
    else
      errors << 'An error occurred when trying to calculate the route. <br>Please try changing your waypoint parameters or try again in some time.'
      return errors
    end

    return errors
  end
end
