class ListsController < ApplicationController
  before_action :filters, only: [:save_inventory,:save_checklist]

  def index
    @browser_timezone = browser_timezone
    @lists = List.where(company_id: current_user.company_id).order('created_at DESC').first(5)
  end

  def filters
    @filters = params[:filters]
  end

  def save_inventory
    if params[:type] == 'inventory'
      asset_ids = params[:ids]
      if asset_ids
        list = List.new(list_type: params[:type])
        list.company_id = current_user.company.id if current_user.company
        list.filter = params[:filters] if params[:filters].present?
        if list.save!
          asset_ids.each do |asset_id|
            list_item = ListItem.new(list_id: list.id)
            asset_id.sub! 'asset_', ''
            list_item.asset_id = asset_id
            list_item.save!
          end
          render json: { status: 200, message: 'List saved successfully.' }
        else
          render json: { status: 500, message: 'Unexpected error occur while saving list.' }
        end
      else
        render json: { status: 500, message: 'Please scan at least one asset to save the list.' }
      end
    end
  end

  def save_checklist
    if params[:type] == 'checklist'
      if params[:ids]
        found_list = params[:ids][:found_list]
        not_found_list = params[:ids][:not_found_list]
        not_in_list_list = params[:ids][:not_in_list_list]
        not_in_assets_list = params[:ids][:not_in_assets_list]
      end
      asset_ids = {}
      asset_ids[:found] = found_list if found_list
      asset_ids[:not_found] = not_found_list if not_found_list
      asset_ids[:not_in_list] = not_in_list_list if not_in_list_list
      asset_ids[:not_in_assets] = not_in_assets_list if not_in_assets_list

      if asset_ids.present?
        list = List.new(list_type: params[:type])
        list.company_id = current_user.company.id if current_user.company
        list.filter = params[:filters] if params[:filters].present?
        if list.save!
          asset_ids.each do |status, ids|
            ids.each do |id|
              list_item = ListItem.new(list_id: list.id, status: status)
              if (!(status.to_s == 'not_in_assets'))
                id.sub! 'asset_', ''
                list_item.asset_id = id
              elsif (status.to_s == 'not_in_assets')
                id.sub! 'rfid_', ''
                list_item.token = id
              end
              list_item.save!
            end
          end
          render json: { status: 200, message: 'List saved successfully.' }
        else
          render json: { status: 500, message: 'Unexpected error occur while saving list.' }
        end
      else
        render json: { status: 500, message: 'Please scan at least one asset to save the list.' }
      end
    end
  end
end
