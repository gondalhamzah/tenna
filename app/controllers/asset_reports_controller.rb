class AssetReportsController < ApplicationController
  before_action :authenticate_user!
  def set_filters
    # dummy asset to get default start and end date, not used in logic
    @asset = Asset.new()
    if current_user.company
      company = current_user.company

      @filters_arr = AnalyticFilter.where(company: company, is_recommended: false).order(created_at: :desc)
      @filters = @filters_arr&.group_by(&:analytic_filter_label_id).as_json
      @filters_arr = @filters_arr.as_json
      filter_label_obj = {}
      @filter_labels = AnalyticFilterLabel.where(company: company).order(created_at: :desc)
      @filter_labels.map{|fl| filter_label_obj[fl.id]=fl.name }
      @filter_label_obj = filter_label_obj.as_json

      @recommended_filters_arr = AnalyticFilter.where(is_recommended: true).order(created_at: :desc)
      @recommended_filters = @recommended_filters_arr&.group_by(&:analytic_filter_label_id).as_json
      @recommended_filter_labels = @recommended_filters_arr.collect(&:analytic_filter_label).uniq
      @recommended_filters_arr = @recommended_filters_arr.as_json
      recommended_filter_label_obj = {}
      @recommended_filter_labels.map{|fl| recommended_filter_label_obj[fl.id]=fl.name }
      @recommended_filter_label_obj = recommended_filter_label_obj.as_json

      # @TODO confirm category
      @company_analytics_subscription = company.subscriptions.where(category: 'asset reports', state: 'active').first
    end
  end

  def index
    set_filters
  end

  def filter_post
    @filter_label = AnalyticFilterLabel.where(:name => params[:label_name], :company_id => current_user.company.id).first_or_create
    @filter = AnalyticFilter.where(name: params[:name], company_id: current_user.company.id).first_or_initialize.tap do |filter|
      filter.filter_json = params[:filter_json]
      filter.start_date = Date.strptime(params[:start_date], '%m-%d-%Y')
      filter.end_date = Date.strptime(params[:end_date], '%m-%d-%Y')
      filter.duration = params[:duration]
      filter.user_id = current_user.id
      filter.analytic_filter_label_id = @filter_label.id
      filter.save
    end
    set_filters
    respond_to :json
  end

  def filter_patch
    label_name = params[:label_name]
    filter_label = AnalyticFilterLabel.where(:name => label_name, :company_id => current_user.company.id).first_or_create
    @filter = AnalyticFilter.find(params[:id])
    @filter.update_attributes(
      name: params[:name],
      filter_json: params[:filter_json],
      start_date: params[:start_date],
      end_date: params[:end_date],
      analytic_filter_label_id: filter_label.id
    )
    set_filters

    respond_to do |format|
      format.json do
        render :filter_post
      end
    end
  end

  def filter_label_post
    name = params[:name]
    company_id = current_user.company.id
    AnalyticFilterLabel.create(name: name, company_id: company_id)
    set_filters
    render :json => {filters: @filters, filter_labels: @filter_labels, filter_label_obj: @filter_label_obj, filters_arr: @filters_arr}
  end

  def filter_label_patch
    name = params[:name]
    analytic_filter_label = AnalyticFilterLabel.find(params[:id].to_i)
    analytic_filter_label.name = name
    analytic_filter_label.save
    set_filters
    render :json => {filters: @filters, filter_labels: @filter_labels, filter_label_obj: @filter_label_obj, filters_arr: @filters_arr}
  end

  def filter_get
    @filter = AnalyticFilter.where(id: params[:id]).first
    if(@filter && @filter.analytic_filter_label_id)
      @filter_label = AnalyticFilterLabel.find(@filter.analytic_filter_label_id)
    end
    set_filters
    @asset.filter_name = (@filter && @filter['name']) ? @filter['name'] : nil
    @asset.start_date = (@filter && @filter['start_date']) ? @filter['start_date'] : nil
    @asset.end_date = (@filter && @filter['end_date']) ? @filter['end_date'] : nil
    render :index
  end

  def delete_myfilter
    filter = AnalyticFilter.find(params[:id].to_i)
    @filter = nil if(@filter && @filter.id == filter.id)
    filter.destroy
    filter.save
    company = current_user.company
    @filters_arr = AnalyticFilter.where(company: company).order(created_at: :desc)
    @filters = @filters_arr&.group_by(&:analytic_filter_label_id).as_json
    @filters_arr = @filters_arr.as_json
    render json: {message: 'successfully deleted'}
  end

  def pre_filter_get
    set_filters
    @filter = @predefined_filters[params[:key]]
    @asset.start_date = Time.zone.today - @filter['days'].days
    @asset.filter_name = @filter['name']
    render :index
  end
end
