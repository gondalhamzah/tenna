class GroupCsvImportsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :group, parent: true
  load_and_authorize_resource :csv_import, through: :group, parent: false

  def show
    render json: { csv_import: @csv_import }
  end

  def new
    @csv_import = CsvImport.new
  end

  def create
    @csv_import = current_user.csv_imports.new(csv_import_params.merge({group: @group}))
    if @csv_import.save
      flash[:notice] = "Group state is active now." if @group.update_attribute('state', "active")
      respond_to do |format|
        format.js
      end
    else
      render 'new'
    end
  end

  private

  def csv_import_params
    params[:csv_import] ||= {file: nil}
    params.require(:csv_import).permit(:file)
  end
end
