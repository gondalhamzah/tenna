class GeoPositionsController < ApplicationController
  skip_before_filter :set_location

  def index
    # TODO: put this into the service
    if params[:lat].present? && params[:lng].present?
      results = geocoder_coordinates(params[:lat], params[:lng]) || default_coordinates
    elsif params[:addr].present?
      results = geocoder_coordinates_addr(params[:addr])
    else
      results = default_coordinates
    end

    if results.present?
      result = results[0]
      cookies[:coordinates] = { lat: result.latitude, lng: result.longitude, accuracy: params[:accuracy].present? ? params[:accuracy] : 0.0 }.to_json
      cookies[:zip_code] = result.postal_code
    end

    respond_to do |format|
      format.json {
        render json: {
          success: result.present? ? true : false,
          status:  result.present? ? 200 : 404,
          message: result.present? ? 'Position was set successfully' : 'Wrong zip code!',
          coordinates: result.present? ? { lat: result.latitude, lng: result.longitude } : nil,
          zip_code:    result.present? ? result.postal_code : nil
        }
      }
    end
  end

  private

  def geocoder_coordinates(lat, lng)
    result = Geocoder.search("#{lat},#{lng}")
    return nil if result.empty?
    result
  end

  def geocoder_coordinates_addr(addr)
    result = GenericGeocoder.search(addr)
    return nil if result.empty?
    result
  end

  def default_coordinates
    geocoder_coordinates(CONFIGS['geo_data']['default']['lat'], CONFIGS['geo_data']['default']['lng'])
  end
end
