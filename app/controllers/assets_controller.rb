class AssetsController < ApplicationController
  include BuildMapMarkers
  include ChartHelper
  include VersionGraph
  include AssetsHelper

  before_action :authenticate_user!, except: [:show, :flag, :asset_create], :if =>lambda{ !params[:user].present?}
  before_action :config_projects, only: [:index, :findit, :checklist, :show, :new, :create, :edit, :update, :sold, :qr_scanning], :if =>lambda{ !params[:user].present?}
  before_action :find_it_subscription, only: [:index,:checklist,:findit]
  before_action :tracking_subscription, only: [:show]
  before_action :schedule_maintenance_subscription, only: [:edit,:update]
  before_action :config_groups, only: [:index, :findit, :checklist, :show, :new, :edit, :qr_scanning], :if =>lambda{ !params[:user].present?}

  load_and_authorize_resource if: lambda{ !params[:user].present? }
  # before_action :check_user_authorize, only: [:qr_scanning]
  skip_load_and_authorize_resource only: [:history_download, :index, :findit, :checklist, :update_multiple, :sold, :qr_scanning, :search_by_title]
  before_action :zapier_login!, :if => lambda {params[:user].present?}
  before_action :check_tracking, only: [:show], :if =>lambda{ !params[:user].present?}

  def index
    if params[:user].blank?
      @tracking_code = params[:tracking_code]
      @assign_tracking_code = !@tracking_code.nil?
      @new_wireless_tracker = (@tracking_code.split(/-|_/)[0] == 'tenna') unless @tracking_code.nil?

      if @tracking_code
        tracking_c = TrackingCode.find_by(token: params[:tracking_code])
        @tracking_type = nil
        if tracking_c
          @tracking_type = tracking_c.token_type
          @tracking_token_id = tracking_c.id
        end
        @tracking_asset = Asset.find(params[:tracking_asset_id]) if params[:tracking_asset_id]
      end

      if current_user.private?
        search_options = {company: current_user.company}
      else
        search_options = {creator: current_user}
      end
      params[:filter_search] ||= {}
      if params[:filter_search][:category].present? and params[:filter_search][:category].to_i != 0
        cat_id = params[:filter_search][:category]
        params[:filter_search][:category] = Category.find(cat_id).name
      end

      # comment it for not restricted to assigning the QR code to only assets without a QR code
      # params[:filter_search][:without_tracking_code] = @assign_tracking_code

      if params[:filter_search][:distance_zip]
        if params[:filter_search][:distance_zip].split(',').count == 2
          splitted_zip_code = params[:filter_search][:distance_zip].split(',')
          distance_zip = splitted_zip_code.first
          country_name = splitted_zip_code.second
          params[:filter_search][:distance_zip] = distance_zip
          params[:filter_search][:country] = country_name
        end
      end

      if params[:filter_search][:current_driver].present? && params[:filter_search][:current_driver] == [""]
        # MEANS NO NEED TO FILTER BY DRIVER
        params[:filter_search].delete(:current_driver)
      end

      filterable_assets = FilterableAssets.new(
        user: current_user,
        search_options: search_options,
        params: params,
        zip_code: cookies[:zip_code]
      )

      map_markers = ZipCode.map_markers_for(filterable_assets.filtered_assets_scope)
      @markers = build_map_markers(map_markers)

      params[:page] ||= 1
      @filter_search = filterable_assets.filter_search

      @assets = filterable_assets.assets.page(params[:page]).per(@filter_search.page_size.to_i)
      session[:filterable_assets_params] = params
    else
      render :nothing => true, :status => 200
    end
  end

  def search_by_title
    if current_user.private?
      search_options = { company: current_user.company }
    else
      search_options = { creator: current_user }
    end
    params[:filter_search] ||= {}
    params[:filter_search][:query] = params[:title]

    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: search_options,
      params: params,
      zip_code: cookies[:zip_code]
    )
    @api_assets = filterable_assets.api_assets

    render json: @api_assets.to_json(only: [:id, :title, :description, :project_id])
  end

  def findit
    @items_count = false
    if params[:list_id].present?
      list = List.find_by_id params[:list_id]
      if list.present?
        @active_tabs = 'tab-inventory'
        asset_ids = list.list_items.collect {|l| l.asset_id}
        @inventory_assets = Asset.where(id: asset_ids)
      end
    end

    @tracking_code = params[:tracking_code]
    @assign_tracking_code = !@tracking_code.nil?

    if current_user.private?
      search_options = { company: current_user.company }
    else
      search_options = { creator: current_user }
    end

    params[:filter_search] ||= {}
    params[:filter_search][:without_tracking_code] = @assign_tracking_code

    params[:list_type] = list.list_type if list

    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: search_options,
      params: params,
      zip_code: cookies[:zip_code]
    )

    map_markers = ZipCode.map_markers_for(filterable_assets.filtered_assets_scope)
    @markers = build_map_markers(map_markers)

    params[:page] ||= 1
    @filter_search = filterable_assets.filter_search

    @active_tab = params[:active_tab]
    @assets = filterable_assets.assets.page(params[:page]).per(@filter_search.page_size.to_i) if (@active_tab == 'tab-assets' || @active_tab.nil? || @active_tab == 'tab-inventory')
    @assets = filterable_assets.assets.page(params[:page]).per(10000) if @active_tab == 'tab-checklist'
    # @checklist_assets_not_found = @assets if @active_tab == 'tab-checklist'
    session[:filterable_assets_params] = params
    @filters = ""
    params[:filter_search].each do |k, v|
      if (v.to_s != "" && v.to_s != "0")
        @filters += k +":"+ v.to_s + ","
      end
    end
    render "index"
  end

  def checklist
    @items_count = false
    if params[:list_id].present?
      list = List.find_by_id params[:list_id]
      if list.present?
        @active_tabs = 'tab-checklist'
        asset_ids_found = list.list_items.where(status: 0).collect {|l| l.asset_id}
        @items_count = true if list.list_items.count > 0
        @checklist_assets_found = Asset.where(id: asset_ids_found)
        asset_ids_not_found = list.list_items.where(status: 1).collect {|l| l.asset_id}
        @checklist_assets_not_found = Asset.where(id: asset_ids_not_found)
        asset_ids_not_in_lists = list.list_items.where(status: 2).collect {|l| l.asset_id}
        @checklist_assets_not_in_lists = Asset.where(id: asset_ids_not_in_lists)
        @checklist_assets_not_in_assets = list.list_items.where(status: 3)
      end
    end

    @tracking_code = params[:tracking_code]
    @assign_tracking_code = !@tracking_code.nil?

    if current_user.private?
      search_options = { company: current_user.company }
    else
      search_options = { creator: current_user }
    end

    filters = false
    filters = true if params[:filter_search].present?
    params[:filter_search] ||= {} #if !params[:list].present?
    params[:filter_search][:without_tracking_code] = @assign_tracking_code #if !params[:list].present?

    params[:list_type] = list.list_type if list

    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: search_options,
      params: params,
      zip_code: cookies[:zip_code]
    )

    map_markers = ZipCode.map_markers_for(filterable_assets.filtered_assets_scope)
    @markers = build_map_markers(map_markers)

    params[:page] ||= 1
    @filter_search = filterable_assets.filter_search

    @active_tab = "tab-checklist"
    @assets = filterable_assets.assets.page(params[:page]).per(@filter_search.page_size.to_i) if (@active_tab == 'tab-assets' || @active_tab.nil? || @active_tab == 'tab-inventory')
    @assets = filterable_assets.assets.page(params[:page]).per(10000) if @active_tab == 'tab-checklist'
    @checklist_assets_not_found = @assets if filters
    session[:filterable_assets_params] = params
    @filters = ''
    params[:filter_search].each do |k, v|
      if (v.to_s != '' && v.to_s != '0')
        @filters += k +':'+ v.to_s + ','
      end
    end
    render 'index'
  end

  def asset
    token = params[:rfid].strip
    token = TrackingCode.token_parser(token)
    tracking_code = TrackingCode.where('token = ? and resource_type = ?', token, 'Asset').first
    asset = tracking_code.resource if tracking_code && tracking_code.resource.company_id == current_user.company_id
    if asset and asset.state != 'sold' and asset.state != 'deleted'
      unless cookies[:coordinates].nil?
        current_coordinates = JSON.parse(cookies[:coordinates])
        asset.update(current_lat: current_coordinates['lat'].next_float,
                     current_lng: current_coordinates['lng'].next_float,
                     accuracy: current_coordinates['accuracy'])
      end
      render json: { asset_tr: render_to_string('assets/_asset', :layout => false, :locals => { asset: asset, show_selection_cell: true }), status: 200, message: 'success' }
    elsif tracking_code
      render json: { status: 403, message: 'You do not have permissions to view this asset' }
    else
      render json: { status: 404, message: 'Asset not found' }
    end
  end

  def sold
    if current_user.private?
      search_options = { company: current_user.company }
    else
      search_options = { creator: current_user }
    end

    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: search_options,
      params: params.deep_merge("filter_search": { sold: true }),
      zip_code: cookies[:zip_code]
    )
    params[:page] ||= 1
    @filter_search = filterable_assets.filter_search
    @assets = filterable_assets.assets.page(params[:page]).per(@filter_search.page_size.to_i)
  end

  def history_download
    tab = params[:tab]
    asset_id = params[:asset_id]
    @asset = Asset.find(asset_id)
    @asset_num = Asset.find(asset_id).try(:asset_num)
    @versions = AssetsVersionSearch.get_all_history(asset_id, tab)
    @exported_at = format_time(Time.now)
    csv_name = "history-#{tab}-#{DateTime.now().strftime('%Y%m%d')}.csv"
    respond_to do |format|
      #format.html
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"#{csv_name}\""
        headers['Content-Type'] ||= 'text/csv; charset=iso-8859-1; header=present'
      end
    end
  end

  def show
    if user_signed_in?
      @favorite = @asset.favorite_for(current_user) || Mark.new
    end

    unless @asset.active? || @asset.sold?
      redirect_to dashboard_url, flash: { error: 'Asset is no longer available.' }
    end

    unless user_signed_in? && @asset.creator == current_user
      @asset.increment(:view_count, by = 1)
      @asset.save
    end

    @versions = get_base_versions(@asset.id, @asset.start_date.to_formatted_s(:db), @asset.end_date.to_formatted_s(:db))
    build_group_data()
  end

  def new
    @project_zips = @projects.map {|project| { :project_id => project.id, :zip => project.address.zip, :country => project.address.country }.compact}
    if params[:dup_slug]
      base = Asset.find_by_slug(params[:dup_slug])
      @asset = base.dup
    else
      config_asset
      @asset.build_zip_code
      @asset.type = Asset::TYPES[0]

      if current_user.private?
        @asset.asset_kind = 'owned'
      else
        @asset.asset_kind = 'for_sale'
      end

      if @projects.length == 1
        @asset.project = @projects[0]
      end
    end

    if params.dig(:id)
      @old_asset_category = Asset.find(params[:id]).category.id
      @subtracted_quantity = params[:value]
    end

    if params[:project_id]
      @asset.project = Project.find(params[:project_id])
    end

    if params[:group_id]
      group = Group.find(params[:group_id])
      @asset.group = group
      @asset.project = group.project
    end
  end

  def edit
    @project_zips = @projects.map {|project| { :project_id => project.id, :zip => project.address.zip, :country => project.address.country }.compact}
  end

  def create
    @asset = Asset.new(asset_params)
    config_asset
    create_pdfs
    create_photos
    @asset.asset_kind if params[:user].present?

    if @asset.save
      move_quantities(@asset, params[:asset_id]) if params[:asset_id].present?
    end
    assign_tracking_code

    respond_to do |format|
      format.html {respond_with @asset.becomes(Asset)}
      format.json {render nothing: true, status: 200}
    end
  end

  def update
    create_pdfs
    create_photos
    maintenance = @asset.maintenance
    if params[:asset] and params[:asset][:maintenance] == "0"
      event = Event.where(asset_id: @asset.id, processed: true, deleted: false).where("start_date < ?", Time.now.utc+30).first
      if event
        SendEventNotificationJob.perform_later(event.id, true)
        event.deleted = true
        event.save!
      end
    end
    destroy_photos
    if asset_params[:project_id].present? and asset_params[:group_id].present?
      params[:material].delete("group_id") if params[:material]
      params[:equipment].delete("group_id") if params[:equipment]
    end
    if asset_params[:group_id] && !asset_params[:project_id]
      group = Group.find(asset_params[:group_id])
      if group.project_id != @asset.project_id
        @asset.project_id = group.project_id
        @asset.save
        trigger_hooks_maintenance(@asset, maintenance)
      end
    end

    if asset_params[:event].nil?
      if asset_params[:move_group] == 'Yes'
        @asset.group.project_id = asset_params[:project_id]
        @asset.group.save
        project = Project.find(asset_params[:project_id])
        group_assets = Asset.not_sold_deleted.where(group_id: @asset.group.id)

        unless cookies[:coordinates].nil?
          current_coordinates = JSON.parse(cookies[:coordinates])
        end

        # this workaround has done just to capture the asset history on mass updates
        Asset.transaction do
          group_assets.each do |asset|
            if asset.has_tracking_code?
              asset.update(
                project_id: project.id,
                zip_code_id: ZipCode.find_or_create_by(zip_code: project.address.zip).id,
                current_lat: current_coordinates['lat'].next_float,
                current_lng: current_coordinates['lng'].next_float,
                accuracy: current_coordinates['accuracy']
              )
            else
              asset.update(
                project_id: project.id,
                zip_code_id: ZipCode.find_or_create_by(zip_code: project.address.zip).id
              )
            end
          end
        end
      elsif asset_params[:unlink_group] == 'Yes'
        @asset.group_id = nil
        @asset.save
        trigger_hooks_maintenance(@asset, maintenance)
      else
        @asset.update(asset_params)
        trigger_hooks_maintenance(@asset, maintenance)
      end
    else
      @asset.fire_state_event(asset_params[:event])
    end

    # if reallocate(project change), remove associated group
    if !asset_params[:increment_reallocation_count].blank? && @asset.try(:group).try(:project_id) != @asset.try(:project_id)
      @asset.group_id = nil
      @asset.save
      trigger_hooks_maintenance(@asset, maintenance)
    end

    if params[:qr_scan].present?
      respond_to do |format|
        format.js{
          render json: {data: @asset, status: 200}
        }
      end
    else
      if asset_params[:unlink_group_from_qr] == 'Yes'
        redirect_to qr_scanning_asset_url(@asset, with_tracking_code: true)
      else
        respond_with @asset.becomes(Asset)
      end
    end
  end

  def update_multiple
    error_messages = {}
    errors_flash_msg = ''
    asset_ids = params[:asset_ids].split(',')
    assets = Asset.accessible_by(current_ability, :update).where.not(state: ['deleted', 'sold'])

    if %w[public sold].include? params[:asset_action]
      assets = assets.accessible_by(current_ability, :toggle_marketplace)
    elsif params[:asset_action] == 'delete'
      assets = assets.accessible_by(current_ability, :destroy)
    end

    authorized_asset_ids = assets.where(id: asset_ids).pluck(:id)
    authorized_assets = Asset.where(id: authorized_asset_ids)
    actions = {
      public: -> {
        Asset.transaction do
          authorized_assets.each do |aa|
            if aa.asset_kind != 'rental'
              if aa.has_tracking_code?
                aa.tracking_codes.each_with_index do |trackingCode, index|
                  aa.tracking_codes[index].unassign
                end
              end
              if aa.is_inventory?
                authorized_asset_ids.delete(aa.id)
                next
              else
                aa.assign_attributes(public: true)
                if aa.validate
                  aa.save
                  Hook.trigger_marked_rental_or_public('rental_public', aa)
                else
                  error_messages[aa.title] = aa.errors.messages
                end
              end
            else
              authorized_asset_ids.delete(aa.id)
            end
          end
        end
      },
      sold: -> {
        Asset.transaction do
          authorized_assets.each do |aa|
            if aa.asset_kind != 'rental'
              aa.sell
            else
              authorized_asset_ids.delete(aa.id)
            end
          end
        end
      },
      reallocate: -> {
        project = Project.find(params[:project_id])
        # only operate on assets whose project_id is changing
        authorized_assets = authorized_assets.where("assets.project_id != #{project.id} OR assets.project_id IS NULL")
        authorized_asset_ids = authorized_assets.pluck(:id)

        # this workaround has done just to capture the asset history on mass actions
        Asset.transaction do
          authorized_assets.each do |aa|
            previous = aa.project
            aa.assign_attributes(reallocation_count: aa.reallocation_count + 1, project_id: project.id, group_id: nil, creator_id: project.contact_id, location_zip: project.address.zip)
            if aa.validate
              aa.save
              Hook.trigger_asset('Asset_Reallocation', aa, previous)
            else
              error_messages[aa.title] = aa.errors.messages
            end
          end
        end
      },
      link_to_group: -> {
        group = Group.find(params[:group_id])

        # only operate on assets whose group_id is changing
        authorized_assets = authorized_assets.where("group_id != #{group.id} OR group_id IS NULL")
        authorized_asset_ids = authorized_assets.pluck(:id)

        # this workaround done just to capture the asset history on mass actions
        Asset.transaction do
          authorized_assets.each do |aa|
            aa.assign_attributes(group_id: group.id)
            aa.validate ? aa.save : error_messages[aa.title] = aa.errors.messages
          end
        end
      },
      delete: -> {
        Asset.transaction do
          authorized_assets.each do |aa|
            if aa.validate and !aa.has_tracking_code?
              aa.soft_delete
            elsif aa.has_tracking_code?
              error_messages[aa.id] = { 'Please remove Tracking ID from asset': ["#{aa.title}"] }
            else
              error_messages[aa.title] = aa.errors.messages
            end
          end
        end
      }
    }

    actions[params[:asset_action].to_sym].call
    AssetsIndex::Asset.update_index(authorized_asset_ids)

    if error_messages.count > 0
      error_messages.each do |k, errors|
        errors_flash_msg += k.to_s + ' : '
        errors.each do |e_k, er|
          errors_flash_msg += e_k.to_s + ' : ' + er.first + '<br>'
        end
      end
    end

    if params[:asset_action] == 'sold'
      flash[:notice] = "Successfully sold #{ActionController::Base.helpers.pluralize(authorized_asset_ids.size - error_messages.count, 'asset')}.<br>#{errors_flash_msg}"
    else
      flash[:notice] = "Successfully updated #{ActionController::Base.helpers.pluralize(authorized_asset_ids.size - error_messages.count, 'asset')}.<br>#{errors_flash_msg}"
    end

    if params[:filter_search]
      if params[:filter_search][:hide_filters] == 'true'
        params_query = '?' + { filter_search: params[:filter_search] }.to_query
        redirect_to params[:redirect_to] + params_query and return
      end
    end

    if request.referrer.present? and (request.referrer.include?('findit') or request.referrer.include?('checklist'))
      redirect_to request.referrer and return
    else
      redirect_to params[:redirect_to] and return
    end
  end

  def flag
    if !user_signed_in? or current_user.can_flag?(@asset)
      @asset.flag_as_inappropriate
      flash[:notice] = 'Thank you for reporting this item.'
    end
    redirect_to asset_path(@asset)
  end

  def destroy
    if @asset.has_tracking_code?
      render json: { data: 'Remove tracking IDs.', status: 200 }
      # redirect_to asset_path(@asset), flash: {error: 'Remove tracking IDs.'}
    else
      @asset.soft_delete
      render json: { data: 'Asset has been deleted.', status: 200 }
      # respond_with(@asset.becomes(Asset), location: assets_url)
    end
  end

  def manage_inventory
    @selected_asset = Asset.find_by_id params[:selected_id] if params[:selected_id]
    @asset = Asset.find_by_id params[:id]
  end

  def update_quantity
    asset = Asset.find_by_id params[:id]
    quantity = params[:quantity].to_i
    if params[:selected_asset]
      selected_asset = Asset.find_by_id params[:selected_asset]
      eligible_to_transfer = false
      if (params[:operator] == "transferLower" && asset.quantity >= quantity && selected_asset.present?)
        this_quantity = asset.quantity - quantity
        that_quantity = selected_asset.quantity + quantity
        eligible_to_transfer = true
      elsif (params[:operator] == "transferUpper" && selected_asset.present? && selected_asset.quantity >= quantity)
        this_quantity = asset.quantity + quantity
        that_quantity = selected_asset.quantity - quantity
        eligible_to_transfer = true
      else
        eligible_to_transfer = false
      end

      if eligible_to_transfer
        asset.update(quantity: this_quantity)
        selected_asset.update(quantity: that_quantity)
        flash[:notice] = 'Quantity Successfully transfered'
        redirect_to asset_path(@asset.id)

      else
        flash[:error] = 'Unable to transfer Quantity, Selected less than the Limit'
        redirect_to manage_inventory_asset_path(@asset.id)
      end
    else
      asset.quantity += quantity if (params[:operator] == "transferUpper")
      asset.quantity -= quantity if (params[:operator] == "transferLower" && asset.quantity >= quantity)
      if asset.save
        flash[:notice] = 'Quantity Updated'
        redirect_to asset_path(@asset.id)
      else
        flash[:error] = 'Unable to transfer Quantity'
        redirect_to manage_inventory_asset_path(@asset.id)
      end
    end
  end

  def inventory_assets
    asset = Asset.find_by_id(params[:id])
    @assets = this_company_assets(asset)
  end

  def move_inventory
    flag = false
    asset1 = Asset.find_by_id params[:asset1_id].to_i
    asset2 = Asset.find_by_id params[:asset2_id].to_i
    if asset2.quantity > 0
      asset1.quantity += (asset2.quantity)
      asset1.is_inventory = true
      asset2.is_inventory = false
      asset2.quantity = 1
      (flag = true) if (asset1.save && asset2.save)
      response = {asset1: asset1.title, asset2: asset2.quantity, flag: flag}
    end
    response = {flag: flag} if (flag == false)
    return render json: response
  end

  def asset_create
  end

  def log_in
    redirect_to @asset.becomes(Asset)
  end

  def qr_scanning
    @asset = Asset.find_by_slug params[:id]
    if not params[:error].blank?
      error_code = params[:error].to_i
      if error_code == 0
        redirect_to asset_path(@asset), flash: { error: "We can't detect your location" }
      elsif error_code == 1
        redirect_to asset_path(@asset), flash: { error: 'Your location is not correct.' }
      end
    end

    if params[:with_tracking_code] and @asset.state != 'sold' and @asset.state != 'deleted'
      unless cookies[:coordinates].nil?
        # Find current logged in user id
        current_user_id = User.current.try(:id)
        if current_user_id.nil? && @asset.company.nil?
          current_user_id = @asset.creator.try(:id)
        elsif current_user_id.nil? && !@asset.company.nil?
          current_user_id = @asset.company.contact.try(:id)
        end
        current_coordinates = JSON.parse(cookies[:coordinates])
        current_lat = current_coordinates['lat'].next_float
        current_lng = current_coordinates['lng'].next_float
        accuracy = current_coordinates['accuracy'].to_f/1609.344
        a_accuracy = @asset.accuracy.to_f/1609.344
        ActiveRecord::Base.connection.execute "update assets set current_lat = #{current_lat}, current_lng = #{current_lng}, accuracy = #{accuracy} where id = #{@asset.id}"
        AssetsVersion.create!(asset_id: @asset.try(:id), field_name: 'inventory', from: "lat: #{'%.07f' % @asset.current_lat}, lng: #{'%.07f' % @asset.current_lng}, accu: #{'%.02f' % a_accuracy} miles",
                              to: "lat: #{'%.07f' % current_lat}, lng: #{'%.07f' % current_lng}, accu: #{'%.02f' % accuracy} miles", whodunnit: current_user_id, project_id: @asset.try(:project).try(:id),
                              group_id: @asset.try(:group).try(:id), company_id: @asset.company_id, action: 'inventoried asset')
      end
      flash[:notice] = 'Inventory updated successfully.'
      @projects_by_distance = true
    else
      @project_id, @query_string = request.referrer.match(/projects\/(\d+)(.*)/).try(:captures) if request.referrer
      redirect_to asset_path(@asset), flash: { error: 'you can not access sold and deleted assets.' }
    end
  end

  def find_it_subscription
    if current_user && current_user.company.present?
      company = current_user.company
      @company_findit_subscription = company.subscriptions.where(category: 'findit',state: 'active',payment: true).first
      # @company_findit_suspended_subs = company.subscriptions.where(category: 'findit', state: 'suspended').first
      # if @company_findit_suspended_subs
      #     flash.now[:notice] = "Your subscription has been suspended for this features."
      # end
    end
  end

  def tracking_subscription
    if current_user && current_user.company.present?
      company = current_user.company
      @company_tracking_subscription = company.subscriptions.where(category: 'add tracking to asset',state: 'active',payment: true).first
    end
  end

  def schedule_maintenance_subscription
    if current_user && current_user.company.present?
      company = current_user.company
      @company_asset_schedule_subscription = company.subscriptions.where(category: 'maintenance',state: 'active',payment: true).first
    end
  end

  private

  def config_asset
    @asset ||= Asset.new()
    if current_user
      @asset.company = current_user.company
      @asset.creator = current_user
      if current_user.private?
        @asset.expires_on = DateTime.new(2050, 1, 1)
      end
      if ['for_rent', 'rental'].include?(@asset.asset_kind)
        @asset.price = 1 if @asset.price.nil?
        @asset.time_length = 1 if @asset.time_length.nil?
      end
    else
      @asset.company = @user.company
      @asset.creator = @user
    end
  end

  def create_pdfs
    if params.has_key? :new_pdfs
      params[:new_pdfs][:file].each do |file|
        @asset.pdfs.build(file: file)
      end
    end
  end

  def create_photos
    if params.has_key? :new_photos
      params[:new_photos][:file].each do |file|
        unless params[:destroy_un_save_photos].split(",").include? file.original_filename
          @asset.photos.build(file: file)
        end
      end
    end
    if params.has_key? :upload_csv_photos
      images = params[:upload_csv_photos].split(",")
      images && images.each do |image|
        @asset.photos.build(remote_file_url: image)
      end
    end
  end

  def destroy_photos
    if params.has_key? :destroy_photos_ids
      params[:destroy_photos_ids].split(",").map(&:to_i).each do |photo|
        @asset.photos.where(id: photo).first.destroy if @asset.photos.where(id: photo).first
      end
    end
  end

=begin
  def check_user_authorize
    @asset = Asset.find_by_slug params[:id]
    if (!(@asset.public?) && !(current_user.company_id == @asset.company_id))
      redirect_to root_path, alert: "You are not authorized to access this page."
    end
  end
=end

  def assign_tracking_code
    unless params[:tracking_code].nil?
      tracking_code = TrackingCode.where(token: params[:tracking_code], resource: nil).first
      @asset.tracking_code = tracking_code unless tracking_code.nil?
      unless cookies[:coordinates].nil?
        current_coordinates = JSON.parse(cookies[:coordinates])
        @asset.update(current_lat: current_coordinates['lat'],
                      current_lng: current_coordinates['lng'],
                      accuracy: current_coordinates['accuracy'])
      else
        @asset.update(current_lat: nil, current_lng: nil)
      end
    end
  end

  def asset_params
    if !params[:user].blank?
      zapier_asset_params
    end
    asset_params = [ :current_driver_id, :type, :title, :asset_num, :description, :project_id, :group_id, :category_id, :location_zip,
                    :price, :available_on, :quantity, :quantity_unit, :units, :renew_expires_on,
                    :market_value, :in_use_until, :durability, :move_group, :unlink_group, :unlink_group_from_qr, :action, :notes,
                    :increment_reallocation_count, :expires_on, :event, :asset_kind, :maintenance,
                    :public_private, # 'public' or 'private'
                    :owned_rental, # 'owned' or 'rental'
                    :for_sale_rent, # 'for_sale' or 'for_rent'
                    :time_length,
                    :start_date,
                    :end_date,
                    :is_inventory,
                    :asset_id,
                    :country_name, :owned, :rental,:internal_daily_rental_rate,
                    pdfs_attributes: [:id, :file, :file_cache, :'_destroy'],
                    photos_attributes: [:id, :file, :file_cache, :position, :'_destroy'],
                    tracking_code_attributes: [:token, :token_type]]
    params_type, type = nil
    if (params.has_key? 'equipment')
      params_type = :equipment
      type = Asset::TYPES[0]
    elsif (params.has_key? 'material')
      params_type = :material
      type = Asset::TYPES[1]
    else
      params_type = :asset
      type = params[params_type][:type]
    end
    if type == Asset::TYPES[0]
      params[:asset][:condition] = params[:condition].downcase if params[:condition].present?
      asset_params.concat([:make, :model, :year, :hours, :miles, :quantity, :certified, :condition])
    end
    params.require(params_type).permit(asset_params)
  end

  def trigger_hooks_maintenance(asset, maintenance)
    if maintenance != asset.maintenance
      Hook.trigger_hooks_maintenance('Maintenance', asset, maintenance)
    end
  end

  def check_tracking
    if params[:asset_token]
      tracking = TrackingCode.find_by(token: params[:asset_token])
      tracking.status = 1
      tracking.resource_id = params[:id]
      tracking.resource_type = "Asset"
      tracking.save!
    end
  end

  def zapier_asset_params
    params[:asset][:project_id] = @user.company.project_id
    params[:asset][:location_zip] = params[:zip_code]
    ############# HARD CODED ###########################
    params[:asset][:country_name] = 'United State of America'
    ############################################
    params[:asset][:category_id] = Category.where(name: params[:category]).first.id
  end
end
