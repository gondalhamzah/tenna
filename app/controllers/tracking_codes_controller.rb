class TrackingCodesController < ApplicationController
  before_action :authenticate_user!, :get_current_user
  before_action :initialize_asset, only: [:assign, :add_token, :get_tokens, :delete_token, :get_current_user]

  def show
    @tracking_code = TrackingCode.find_by(token: params[:id])
    unless @tracking_code.nil?
      if @tracking_code.resource.nil?
        redirect_to assets_url(tracking_code: params[:id])
      else
        if current_user.private? && @tracking_code.resource.company_id != current_user.company.id
          current_coordinates = JSON.parse(cookies[:coordinates])
          UnauthorizedNotification.notification(current_user.first_name, current_user.last_name, current_user.email, current_user.company.name, current_user.role, asset_url(@tracking_code.resource), @tracking_code.token, current_coordinates['lat'], current_coordinates['lng']).deliver_now
          redirect_to unauthorized_tracking_code_path
        else
          redirect_to qr_scanning_asset_url(@tracking_code.resource, with_tracking_code: true)
        end
      end
    else
      tracking_code_split = params[:id].split(/-|_/)
      if tracking_code_split[0] == 'tenna'
        @tracking_type = tracking_code_split[1]
        @tracking_code = tracking_code_split[2]

        redirect_to assets_path tracking_code: params[:id]
      else
        redirect_to assets_url(tracking_code: params[:id])
        # render :not_found
      end
    end
  end

  def assign
    tracking_code_split = params[:id].split(/-|_/)
    @new_wireless_tracker = (tracking_code_split[0] == 'tenna')
    @tracking_code = nil
    asset_tracker = nil
    if @new_wireless_tracker
      tracking_type = tracking_code_split[1]
      tracking_code_token = tracking_code_split[2]

      tracking_type = case tracking_type
                        when 'rfid'
                          'RFID'
                        when 'lora'
                          'LoRa'
                        when 'cellular'
                          'Cellular'
                        when 'ble'
                          'BT'
                        when 'sigfox'
                          'Sigfox'
                        else
                          raise ActionController::RoutingError.new('Not Found')
                      end
      @qr_tracking_code = TrackingCode.create(token: params[:id], token_type: 'QR', status: 'unassigned')
      @wireless_tracking_code = TrackingCode.create(token: tracking_code_token, token_type: tracking_type, status: 'unassigned')
      total_tracking_codes = Hash.new(0)

      @qr_tracking_code.resource_id = @asset.id
      @qr_tracking_code.resource_type = "Asset"
      @qr_tracking_code.status = "assigned"
      @qr_tracking_code.save
      total_tracking_codes[@qr_tracking_code.token_type.downcase] += 1
      AssetsVersion.transaction do
      AssetsVersion.create!(asset_id: @asset.try(:id), field_name: "#{@qr_tracking_code.token_type.downcase} added", from: '', to: @qr_tracking_code.token,
                            whodunnit: @current_user_id, project_id: @asset.try(:project).try(:id), group_id: @asset.try(:group).try(:id),
                            company_id: @asset.company_id, action: 'token created')
      end
      @wireless_tracking_code.resource_id = @asset.id
      @wireless_tracking_code.resource_type = "Asset"
      @wireless_tracking_code.status = "assigned"
      @wireless_tracking_code.save
      AssetsVersion.transaction do
      AssetsVersion.create!(asset_id: @asset.try(:id), field_name: "#{@wireless_tracking_code.token_type.downcase} added", from: '', to: @wireless_tracking_code.token,
                            whodunnit: @current_user_id, project_id: @asset.try(:project).try(:id), group_id: @asset.try(:group).try(:id),
                            company_id: @asset.company_id, action: 'token created')
      end
      total_tracking_codes[@wireless_tracking_code.token_type.downcase] += 1
      # @asset.tracking_code = @qr_tracking_code
      # @asset.tracking_code = @wireless_tracking_code
      asset_tracker = @asset
      Asset.transaction do
        total_tracking_codes.each do |k, v|
          current_code_count = @asset["#{k}"]
          asset_code = v + ((current_code_count.nil? || current_code_count < 0) ? 0 : current_code_count.to_i)
          @asset.update("#{k}": asset_code)
        end
      end
    else
      @tracking_code = TrackingCode.find_by(token: params[:id])
      if @tracking_code.nil? or @asset.nil?
        add_token
      else
        @asset.tracking_code = @tracking_code
        @asset.qr = 1 + ((@asset.qr.nil? ||  @asset.qr < 0) ? 0 : @asset.qr)
      end
    end
    if !@tracking_code.nil? or !asset_tracker.nil?
      unless cookies[:coordinates].nil?
        current_coordinates = JSON.parse(cookies[:coordinates])
        @asset.update(current_lat: current_coordinates['lat'],
                      current_lng: current_coordinates['lng'],
                      accuracy: current_coordinates['accuracy'])
        flash[:notice] = 'Asset successfully linked.'
      else
        @asset.update(current_lat: nil, current_lng: nil)
      end
      AssetsIndex::Asset.update_index(@asset)
      redirect_to asset_url @asset
    end
  end

  def unassign
    @tracking_code = TrackingCode.find_by(token: params[:id])
    asset = @tracking_code.resource
    @tracking_code.unassign
    redirect_to asset_url(asset)
  end

  # assign function to assign tracking code to asset

  def add_token
    tracking_code_split = params[:id].split(/-|_/) if !params[:id].nil?
    @new_wireless_tracker = (tracking_code_split[0] == 'tenna') if !params[:id].nil?
    @tracking_code = nil
    asset_tracker = nil
    if !@new_wireless_tracker
      tracking_assigned = ''
      begin
        tracking_tokens = params['tracking_tokens']
        token_id = nil || params['token_id']
        total_tracking_codes = Hash.new(0)
        TrackingCode.transaction do
          tracking_tokens.each do |tracking_token|
            tracking_hash = tracking_token.gsub(/[{}]/, '').split(', ').map{ |h| h1, h2 = h.split(':'); {h1.strip => h2.strip}}.reduce(:merge)
            if tracking_hash["token"] != ""
              if token_id.nil?
                tracking_hash['token'].downcase!
                total_tracking_codes[tracking_hash['token_type'].downcase] += 1
                tracking_assigned = TrackingCode.new(tracking_hash).save!
                AssetsVersion.transaction do
                  AssetsVersion.create!(asset_id: @asset.try(:id), field_name: "#{tracking_hash['token_type'].downcase} added", from: '', to: tracking_hash['token'],
                                        whodunnit: @current_user_id, project_id: @asset.try(:project).try(:id), group_id: @asset.try(:group).try(:id),
                                        company_id: @asset.company_id, action: 'token created')
                end
              else
                tracking_assigned = TrackingCode.find(token_id)
                tracking_assigned.resource_id = tracking_hash["resource_id"]
                tracking_assigned.resource_type = tracking_hash["resource_type"]
                tracking_assigned.status = tracking_hash["status"]
                tracking_assigned.save
                total_tracking_codes[tracking_assigned.token_type.downcase] += 1
                AssetsVersion.transaction do
                  AssetsVersion.create!(asset_id: @asset.try(:id), field_name: "#{tracking_assigned.token_type.downcase} added", from: '', to: tracking_assigned.token,
                                        whodunnit: @current_user_id, project_id: @asset.try(:project).try(:id), group_id: @asset.try(:group).try(:id),
                                        company_id: @asset.company_id, action: 'token created')
                end
              end
            end
          end
        end

        Asset.transaction do
          total_tracking_codes.each do |k, v|
            current_code_count = @asset["#{k}"]
            asset_code = v + ((current_code_count.nil? || current_code_count < 0) ? 0 : current_code_count.to_i)
            @asset.update("#{k}": asset_code)
          end
        end

        unless cookies[:coordinates].nil?
          current_coordinates = JSON.parse(cookies[:coordinates])
          @asset.update(current_lat: current_coordinates['lat'].next_float,
                        current_lng: current_coordinates['lng'].next_float,
                        accuracy: current_coordinates['accuracy'])
        else
          @asset.update(current_lat: nil, current_lng: nil)
        end

        AssetsIndex::Asset.update_index(@asset)

        if params[:js_success]
          js_success = params[:js_success]
        else
          js_success = ""
        end
        if !defined? js_success or js_success == ""
          respond_to do |format|
            format.json { render json: {json: 'success' ,asset_name: @asset.title}}
          end
        else
          flash[:notice] = 'Asset successfully linked.'
          redirect_to asset_url(id: @asset.id)
        end
        rescue_from ActiveRecord::Errors unless tracking_assigned
      end
    else
      assign
    end
  end

  def unauthorized
  end

  def get_tokens
    token_type = params['token_type']
    tokens = TrackingCode.find_tokens_by_type @asset.try(:id), token_type
    respond_to do |format|
      format.html { render :layout => nil }
      format.json { render :json => tokens }
    end
  end

  def delete_token
    token = params['token'].downcase
    token_type = params['token_type']
    token_type_key = TrackingCode.token_types.key(token_type.to_i)
    tracking_code = TrackingCode.find_token  @asset.id, token, token_type
    tracking_code_deleted = tracking_code.delete(tracking_code.pluck(:id))
    json_response = tracking_code_deleted ? 'success' : 'error'

    if tracking_code_deleted
      AssetsVersion.create!(asset_id: @asset.try(:id), field_name: "#{token_type_key} deleted", from: token, to: '',
                            whodunnit: @current_user_id, project_id: @asset.try(:project).try(:id), group_id: @asset.try(:group).try(:id),
                            company_id: @asset.company_id, action: 'token deleted')

      asset_code_count =  @asset["#{token_type_key.downcase}"]
      current_codes = (asset_code_count.nil? || asset_code_count <= 0) ? 0 : asset_code_count - 1
      @asset.update("#{token_type_key.downcase}": current_codes)

      AssetsIndex::Asset.update_index(@asset)
    end

    respond_to do |format|
      format.html { render :layout => nil }
      format.json { render :json => json_response }
    end
    rescue_from ActiveRecord::Errors unless tracking_code_deleted
  end

private

  def initialize_asset
    @asset = Asset.find(params[:asset_id]) if params[:asset_id].present?
  end

  def get_current_user
    current_user_id = User.current.try(:id)
    if current_user_id.nil? && self.company.nil?
      @current_user_id = self.creator.try(:id)
    elsif current_user_id.nil? && !self.company.nil?
      @current_user_id = self.company.contact.try(:id)
    else
      @current_user_id = current_user_id
    end
  end

  def tracking_code_params
    params.require(:tracking_code).permit(:token, :resource_id, :resource_type, :status, :token_type)
  end

end

