class ProfileController < ApplicationController
  before_action :authenticate_user!

  def index
    if current_user.private?
      redirect_to root_path
    end
  end
end
