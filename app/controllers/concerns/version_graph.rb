module VersionGraph
  extend ActiveSupport::Concern

  private

  def get_chart_sery_data(name, data)
    res = {name: name, data: []}
    data.each do |d|
      res[:data] << [d.created_at, d.to.to_f]
    end
    res
  end

  def get_base_versions(asset_id, start_date, end_date, queries=false, company_id=nil)
    versions = AssetsVersionsIndex::AssetsVersion
    term = {asset_id: asset_id} if !asset_id.blank?
    term = {company_id: company_id} if !company_id.blank?

    versions = versions.filter({
      bool: {
        must: [
          term: term
        ]
      }
    })
    .filter({
      range: {
        created_at: {
          gte: start_date,
          lte: end_date,
          format: "yyyy-MM-dd",
          time_zone: "0:00"
        }
      }
    })
    if queries
      queries.each do|query|
        versions = versions.filter(query) if query.present?
      end    
    end
    versions = versions.order(created_at: :asc).limit(100000)
    versions
  end

  def build_group_data
    @price_versions = @versions.filter({
      bool: {
        must: [
          terms: { field_name: ['price'] }
        ]
      }
    })

    @value_version = @versions.filter({
      bool: {
        must: [
          terms: { field_name: ['market_value'] }
        ]
      }
    })

    @hm_version = @versions.filter({
      bool: {
        must: [
          terms: { field_name: ['hours', 'miles'] }
        ]
      }
    })

    geo_versions = @versions.filter({
      bool: {
        must: {
          terms: { field_name_not_analyzed: ['current_lat', 'current_lng'] }
        }
      }
    })

    @geo_data = []
    geo_versions.each_slice(2) do |versions|
      if versions[0].field_name_not_analyzed == 'current_lat'
        lat = versions[0].to unless versions[0].nil?
        lng = versions[1].to unless versions[1].nil?
      else
        lng = versions[0].to unless versions[0].nil?
        lat = versions[1].to unless versions[1].nil?
      end
      @geo_data << {geo: [lat, lng], dt: DateTime.parse(versions[0].created_at).in_time_zone.strftime('%m/%d/%Y %I:%M %p %Z')}
    end

    @chart = basic_chart(title: 'Line Chart', show_legend: false, colors: ["#007FBF", "#91E300"])
    @chart[:chart] = { type: 'spline' }
    @chart[:xAxis] =  {
      type: 'datetime',
      dateTimeLabelFormats: {
        month: '%e. %b',
        year: '%b'
      },
      title: {
        text: 'Date'
      }
    }
    @chart[:plotOptions] = {
      spline: {
        marker: {
          enabled: true
        }
      }
    }
    @series = []
    @series << get_chart_sery_data('Price', @price_versions) if @price_versions.count > 0
    @series << get_chart_sery_data('Market Value', @value_version) if @value_version.count > 0
    @series << get_chart_sery_data('Hours Miles', @hm_version) if @hm_version.count > 0
  end
end
