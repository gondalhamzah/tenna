module GaugeGraphData
  extend ActiveSupport::Concern

  private

  def get_dt_str(timestamp)
    DateTime.parse(timestamp).strftime(I18n.t('date.formats.month_day_year_and_time'))
  end

  def get_value_dt(av, gauge)
    item = av.tracking_info['array'].detect {|a| a['type'] == gauge.name}
    if [10, 59, 65, 64, 57,67, 70].include?(gauge.id) # if boolean data
      value = item['value_boolean']
    else
      value = item['value_numeric']
    end
    {
      "value": value,
      "datetime": get_dt_str(item['timestamp'])
    }
  end

  def get_stats_detail(gauge, order_type)
    query = {
        bool: {
            must: [
              { match: { 'tracking_info.array.type': gauge.name} },
            ]
        }
    }

    sort_hash = {
      "tracking_info.array.value_numeric": {
          order: order_type,
          mode:  order_type == 'desc' ? 'max' : 'min' ,
          nested_filter: {
              query: query
          }
      }
    }

    if order_type == 'latest'
      sort_hash = {
        "tracking_info.array.timestamp": {
            order: 'desc',
            nested_filter: {
                query: query
            }
        }
      }
    end

    avs = AssetsVersionsIndex::AssetsVersion
    avs = avs.filter({
        bool: {
            must: [
                { term: { asset_id: @asset_id } },
                { term: { field_name: 'auto'} },
                { range: { created_at: { gte: "now-#{ENV['LIVE_DATA_ASSET_TIME_SPAN'] or 15}d" } } },
                {
                    nested: {
                        path: 'tracking_info.array',
                        query: query
                    }
                }
            ]
        }
    })

    items = avs.order(sort_hash).limit(1).only(:id).load().to_a
    if items.length > 0
      get_value_dt(items[0], gauge)
    end
  end

  def asset_data
      res = {};
      asset = Asset.find_by_id(@asset_id)
      if asset.present?
          res[:data] = {
              geo: [asset.current_lat, asset.current_lng],
              title: asset.title,
              site: (asset.project.present? ? {
                id: asset.project_id,
                name: asset.project.name
              } : nil),
              location: asset.location,
              # img: photo_thumb(asset),
              img_url: asset.photo_url,
              asset_num: asset.asset_num,
              lastUpdate: asset.updated_at.strftime('%m/%d/%y %l:%M %p'),
              assetId: asset.id,
              hours: asset.hours || 0,
              miles: asset.miles || 0,
              tracking_code: asset.tracking_code_name,
              notes: asset.notes
          }
          category = asset.category;
          category_group = asset.category&.category_group;
          gauges = GaugeAlertGroup.where(
              company_id: @company_id,
              category_group_id: category_group&.id,
              category_id: category&.id,
              asset_id: @asset_id
          )
          .where.not(gauge_id: 12)
          .collect do |item|
              gauge = item.gauge
              nil unless gauge.graph_type
              stats = {
                  type: gauge.graph_type,
                  label: gauge.name,
                  value: nil,
                  unit: gauge.meta['stats_unit'],
                  max: nil,
                  min: nil,
                  last: nil,
              }

              latest = get_stats_detail(gauge, 'latest')

              if latest
                stats['value'] = latest[:value]
                stats['last'] = latest
              end

              case gauge.graph_type
                when 'halfSolid'
                  stats['max'] = get_stats_detail(gauge, 'desc')
                when 'fullSolid'
                  stats['max'] = get_stats_detail(gauge, 'desc')
                  stats['min'] = get_stats_detail(gauge, 'asc')
                when 'odometer'
                  stats['min'] = get_stats_detail(gauge, 'asc')
              end

              stats
          end
          res[:gauges] = gauges
      end
      res
  end
end
