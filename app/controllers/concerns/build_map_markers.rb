module BuildMapMarkers
  extend ActiveSupport::Concern

  private

  def build_map_markers(map_markers)
    map_markers = map_markers.reject { |map_marker| map_marker[:lat].blank? || map_marker[:lng].blank? }

    markers = []

    map_markers.each do |map_marker|

      marker_name = (map_marker[:name] || map_marker[:title] || map_marker[:zip_code]).gsub("'", "\'")

      markers << {
        id: map_marker[:id],
        name: marker_name,
        coordinates: {
          zip_code: map_marker[:zip_code],
          lat:      map_marker[:lat],
          lng:      map_marker[:lng]
        },
        icon: map_marker[:icon] || nil #for zip codes
      }
    end

    markers
  end
end
