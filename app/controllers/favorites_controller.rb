class FavoritesController < ApplicationController
  include BuildMapMarkers

  before_action :authenticate_user!

  def index
    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: {favoriter: current_user},
      params: params,
      zip_code: cookies[:zip_code]
    )

    if filterable_assets.filter_search.sort.nil?
      filterable_assets.sort_by = {'marks.created_at' => :desc}
    end

    map_markers = ZipCode.map_markers_for(filterable_assets.filtered_assets_scope)
    @markers = build_map_markers(map_markers)

    params[:page] ||= 1
    @assets = filterable_assets.assets.page(params[:page])
    @filter_search = filterable_assets.filter_search
  end
end
