class GeoFencesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def create
    @geo_fence = GeoFence.new(geo_fence_params)
    config_geo_fence
    @geo_fence.save
    config_related

    respond_to do |format|
      format.json { render :json => @geo_fence }
    end
  end

  def update
    @geo_fence.update(geo_fence_params)
    config_related

    respond_to do |format|
      format.json { render :json => @geo_fence }
    end
  end

  def destroy
    @geo_fence.destroy

    respond_to do |format|
      format.json { render :json => {status: 'OK'}}
    end
  end

  private

  def config_geo_fence
    @geo_fence ||= GeoFence.new()
    @geo_fence.company = current_user.company
  end

  def config_related
    @geo_fence.geo_fence_company_contacts.destroy_all
    @geo_fence.geo_fence_other_contacts.destroy_all
    @geo_fence.geo_points.destroy_all
    @geo_fence.gf_alert_exclusions.destroy_all

    if params[:company_contacts]
      params[:company_contacts].each do |contact|
        GeoFenceCompanyContact.create(
          geo_fence: @geo_fence,
          user_id: contact['user_id'],
          email: contact['email'],
          text: contact['text']
        )
      end
    end

    if params[:other_contacts]
      params[:other_contacts].each do |contact|
        GeoFenceOtherContact.create(
          geo_fence: @geo_fence,
          email: contact['email'],
          phone: contact['phone'],
          name: contact['name'],
          email_enabled: contact['email_enabled'],
          phone_enabled: contact['phone_enabled']
        )
      end
    end

    params[:points].each do |point|
      GeoPoint.create(
        geo_fence: @geo_fence,
        lat: point['lat'],
        lng: point['lng']
      )
    end

    @geo_fence.reload
  end

  def geo_fence_params
    params.require(:geo_fence).permit(:shape, :name, :color, :radius, :alert_enter_site, :alert_exit_site, :center, :zoom_level)
  end
end
