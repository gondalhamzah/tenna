class RobotsController < ApplicationController
  layout false

  # Ensures that only production gets crawled by Googlebot

  def index
    if canonical_host?
      render 'allow'
    else
      render 'disallow'
    end
  end

  private

  def canonical_host?
    request.host =~ /www\.tenna\.com/
  end

end
