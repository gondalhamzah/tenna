class EventsController < ApplicationController
  respond_to :html, :js, only: :db_action
  before_action :authenticate_user!, only: [:index]
  before_action :schedule_it_subscription, only: [:index]
  def index
    company = current_user.company
    @event = nil
    @asset = nil
    if params[:event_id]
      @event = Event.find_by_id(params[:event_id])
    end

    if params[:asset_id]
      @asset = Asset.find_by_id(params[:asset_id])
      @event = Event.where(asset_id: params[:asset_id]).where("event_pid is null or event_pid = 0").first
    end
    if company
      events = Event.joins("JOIN users ON users.id = events.user_id AND users.company_id=#{company.id}")
        .where.not(deleted: true).includes(:route).all

      @events_json = events.map{|event| {
          :id => event.id,
          :start_date => event.start_date.to_formatted_s(:db),
          :end_date => event.end_date.to_formatted_s(:db),
          :route_id => event.route_id,
          :text => event.text,
          :color => pick_color(event),
          :rec_type => event.rec_type,
          :event_length => event.event_length,
          :event_pid => event.event_pid,
          :event_type => event.event_type,
          :asset_id => event.asset_id,
          :user_ids => event.user_ids,
        }
      }.to_json
      @routes = company.routes.order(:name).all.map{|r| {:key=>r.id, :label=>r.name}}.to_json
      @assets = company.assets.available.order(:title).all.map{|a| {:key=>a.id, :label=>a.title}}.to_json
      @users = company.users.reorder(:first_name).all.map{|u| {:key=>u.id, :label=>u.name}}.to_json
    end
    render layout: 'dhtmlx'
  end

  def pick_color(event)
    if event.event_type == "maint"
      return "#fd4f00"
    elsif event.event_type == "routing"
      return "#f6b436"
    else
      return "#00b2d3"
    end
  end

  def db_action
    id = params['id']
    text = params['text']
    start_date = DateTime.strptime(params['start_date'], "%Y-%m-%d %H:%M")
    end_date = DateTime.strptime(params['end_date'], "%Y-%m-%d %H:%M")
    event_length = params['event_length']
    rec_type = ([nil, '', 'null'].include? params['rec_type']) ? nil : params['rec_type']
    event_pid = params['event_pid'].to_i > 0 ? params['event_pid'].to_i : nil

    event_type = params['event_type']
    route_id =  params['route_id'].to_i > 0 ? params['route_id'].to_i : nil
    asset_id = params['asset_id'].to_i > 0 ? params['asset_id'].to_i : nil
    user_ids = params['user_ids']
    mode = params['!nativeeditor_status']

    tid = id

    case mode
      when 'inserted'
        event = Event.create :start_date => start_date, :end_date => end_date,
          :text => text, :rec_type => rec_type, :event_length => event_length,
          :event_pid => event_pid, :event_type => event_type,
          :route_id => route_id, :asset_id => asset_id, :user_ids => user_ids,
          :timezone => Time.zone.name, :user => current_user
        tid = event.id
        if rec_type == 'none'
          mode = 'deleted'
        end
        if scheduled_event_notification.present?
          SendEventNotificationJob.perform_later(event.id)
        end
      when 'deleted'
        if !rec_type.blank?
          Event.where(event_pid: id).destroy_all
        end

        if !event_pid.blank?
          event = Event.find(id)
          event.rec_type = 'none'
          event.save
        else
          Event.find(id).destroy
        end

      when 'updated'
        if rec_type != ''
          Event.where(event_pid: id).destroy_all
        end
        event = Event.find(id)
        event.start_date = start_date
        event.end_date = end_date
        event.text = text
        event.rec_type = rec_type
        event.event_length = event_length
        event.event_pid = event_pid
        event.user = current_user
        event.event_type = event_type
        event.route_id = route_id
        event.asset_id = asset_id
        event.user_ids = user_ids

        event.save
        if scheduled_event_notification.present?
          SendEventNotificationJob.perform_later(event.id)
        end
      end

      render :json => {
            :type => mode,
            :sid => id,
            :tid => tid,
        }
  end

  def schedule_it_subscription
    if current_user.company
      company = current_user.company
      @company_schedule_subscription = company.subscriptions.where(category: 'scheduleit',state: 'active',payment: true).first
    end
  end

  def scheduled_event_notification
      company = current_user.company
      @scheduled_event_notification = company.subscriptions.where(category: 'scheduleit',state: 'active',payment: true).first
  end
end
