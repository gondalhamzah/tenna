class PagesController < ApplicationController
  before_action :check_if_signed_in, only: :index

  # Uses high voltage gem to serve static homepage

  def index
    render 'pages/index', layout: 'homepage'
  end

  def update_cookies
    respond_to do |format|
      format.json { head :ok }
    end
  end

  def check_if_signed_in
    redirect_to dashboard_path if signed_in?
  end

  def learn_more_email
    LearnMore.send_email(params).deliver
    flash[:success] = '<b>Thanks!</b><br />Your information has been submitted. A Tenna representative will contact you within 24 hours.'
    redirect_to :root
  end

  def leads_mailer_email
    LeadsMailer.send_email(params).deliver
    flash[:success] = '<b>Thanks!</b><br />Your download will begin shortly.'
    redirect_to :root
  end

  def market_place
    render 'pages/market_place', layout: 'homepage'
  end

  def value
    render 'pages/value', layout: 'homepage'
  end

  def industries_served
    render 'pages/industries_served', layout: 'homepage'
  end

  def support
    render 'pages/support', layout: 'homepage'
  end

  def about_us
    render 'pages/about_us', layout: 'homepage'
  end
end
