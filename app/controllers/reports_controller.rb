class ReportsController < ApplicationController
  before_action :company_reports_subscription
  def index
    @filter_search = ReportAssetsSearch.new

    filterable_projects = FilterableProjects.new(
      user: current_user,
      search_options: {company: current_user.company},
      params:{},
      zip_code: cookies[:zip_code]
    )
    projects = filterable_projects.projects.per(10000)
    @projects = projects.collect{|project| [project[:name],project[:id]] if project}.compact if projects.present?
  end

  def generate_project_assets_report
    unless params[:report_assets_search][:project].present?
      params[:report_assets_search][:project] =  params[:report_assets_search][:projects]
    end
    filterable_assets = FilterableReportAssets.new(
      user: current_user,
      params: params,
      zip_code: cookies[:zip_code]
    )

    @assets_versions = filterable_assets.report_project_assets_and_reallocations
    @asset_versions_with_total_days_of_assets_on_project = AssetsVersion.asset_versions_with_total_days_of_assets_on_project(@assets_versions)
    project_id = params[:report_assets_search][:project]
    @project = Project.find project_id
    @assets_count = @project.assets.where.not(state: ['deleted', 'sold']).count
    datestamp = DateTime.now.strftime('%m-%d-%Y')
    @internal_rental_rate =  params[:report_assets_search][:internal_rental_rate]
    @calculate_cost = params[:report_assets_search][:calculate_cost]  

    if @internal_rental_rate == "1"
      @internal_rental_rate = true
    else
      @internal_rental_rate = false
    end

    @calculate_cost = ( @calculate_cost == "1" ? true : false) 

    if params[:report_assets_search][:export_type] == 'csv'
      file_path = AssetsVersion.generate_csv_for_project_assets_report(@asset_versions_with_total_days_of_assets_on_project, @assets_versions, current_user, @project.name,@internal_rental_rate, @calculate_cost)
      send_file file_path, :type => 'application/zip', :x_sendfile => true, filename: "#{@project.name}-Assets-#{datestamp}.zip"
    elsif params[:report_assets_search][:export_type] == 'pdf'
      @asset = AssetsVersion.new
      @datetime = DateTime.now
      leaders_count = @project.project_assignments.where(role:0).count
      members_count = @project.project_assignments.where(role:1).count
      @team_members = { leaders:leaders_count, members:members_count }
      asset_search = AssetSearch.new
      @assets_value = asset_search.cost_and_value(project_id)

      pdf = render_to_string pdf: "#{@project.name}-Assets-#{@datetime.strftime('%m-%d-%Y')}.pdf",
            template: 'reports/generate_project_assets_report.html.erb',
            orientation: 'Landscape',
            page_size: 'A4',
            :margin => {:top => 20, :bottom =>37 },
            header: {html: {template: 'reports/header.pdf.erb',
                                     locals:{project_name: @project.name,date: @datetime.strftime("%m/%d/%Y")}},
                                     :spacing => 15
                                     },
            :footer => {
              :content => render_to_string('reports/footer.pdf.erb'),
              :spacing => 0
            }
      send_data(pdf,
         :filename    => "#{@project.name}-Assets-#{@datetime.strftime('%m-%d-%Y')}.pdf",
         :disposition => 'attachment')
    end
    CompaniesVersion.create!(field_name: 'site utilization', from: '', to: "#{params[:report_assets_search][:export_type]}",
                             whodunnit: current_user.try(:id), company_id: current_user.company.try(:id), action: 'download')
  end

  def generate_asset_location_audit_report
    params[:report_assets_search][:asset_categories] = params[:report_assets_search][:asset_categories].reject(&:empty?)
    params[:report_assets_search][:projects] =  params[:report_assets_search][:projects].reject(&:empty?)

    asset_location_audit_by_category = params[:report_assets_search][:asset_category_filter] == '1'
    asset_location_audit_by_project = params[:report_assets_search][:project_filter] == '1'

    filterable_assets = FilterableReportAssets.new(
      user: current_user,
      params: params,
      zip_code: cookies[:zip_code]
    )

    @datetime = DateTime.now
    audit_report = {project: {}, category:{}}
    both_reports_flag = (asset_location_audit_by_category and  asset_location_audit_by_project)
    if asset_location_audit_by_project || asset_location_audit_by_category
      if params[:report_assets_search][:projects].count > 0 || asset_location_audit_by_project || both_reports_flag
        @assets_grouped_by_proj, @location_match_mismatch, @location_match_total_by_proj, @location_mismatch_total_by_proj, @projects_names, @location_versions_by_proj = filterable_assets.asset_location_audit_by_project(current_user,cookies[:zip_code], both_reports_flag)
        audit_report[:project][:template] = 'reports/generate_asset_location_audit_report_by_project.html.erb'
        audit_report[:project][:filename] = "Asset-Location-Audit-By-Project-#{ @datetime.strftime('%m-%d-%Y') }"

      elsif params[:report_assets_search][:asset_categories].count > 0 || asset_location_audit_by_category
        @assets_grouped_by_category, @location_match_total_by_cat, @location_mismatch_total_by_cat, @location_versions_by_cat  = filterable_assets.asset_location_audit_by_category(current_user)
        audit_report[:category][:template] = 'reports/generate_asset_location_audit_report_by_category.html.erb'
        audit_report[:category][:filename] = "Asset-Location-Audit-By-Category-#{@datetime.strftime('%m-%d-%Y')}"
      end
    end

    @user = current_user
    @browser_timezone = browser_timezone

    final_files = {project: {}, category: {} }
    audit_report.each do |key,val|
      if val.present?
        final_files[key][:filename] = val[:filename]
        pdf = render_to_string pdf: "#{val[:filename]}.pdf",
              template: val[:template],
              orientation: 'Landscape',
              page_size: 'A4',
              show_as_html: true,
              :margin => {:top => 20, :bottom =>37 },
              header: {html: {template: 'reports/asset_location_audit_report_header.pdf.erb',
                                       locals:{date: @datetime.strftime('%m/%d/%Y'), asset_location_audit_by_project: asset_location_audit_by_project, asset_location_audit_by_category: asset_location_audit_by_category, both_reports_flag: both_reports_flag }},
                                       :spacing => 15
                                       },
              :footer => {
                :content => render_to_string('reports/footer.pdf.erb'),
                :spacing => 0
              }
        final_files[key][:pdf] = pdf
      end
    end

    if final_files[:project].present?
      pdf_file = final_files[:project][:pdf]
      file_name = final_files[:project][:filename]
    elsif final_files[:category]
      pdf_file = final_files[:category][:pdf]
      file_name = final_files[:category][:filename]
    end

  send_data(pdf_file,
            :filename    => "#{file_name}.pdf",
            :disposition => 'attachment')

  CompaniesVersion.create!(field_name: 'asset location audit', from: '', to: 'pdf',
                             whodunnit: current_user.try(:id), company_id: current_user.company.try(:id), action: 'download')
  end

  def project_assets_report
    @project = params[:project_id]
    @filter_search = ReportAssetsSearch.new
  end

  def company_reports_subscription
    if current_user.company
      company = current_user.company
      @company_reports_subscription = company.subscriptions.where(category: 'company reports',state: 'active',payment: true).first
    end
  end
end
