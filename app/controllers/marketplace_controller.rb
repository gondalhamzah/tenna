class MarketplaceController < ApplicationController
  include BuildMapMarkers

  def index
    filterable_assets = FilterableAssets.new(
      user: current_user,
      params: params,
      zip_code: cookies[:zip_code]
    )
    map_markers = ZipCode.map_markers_for(filterable_assets.filtered_assets_scope)
    @markers = build_map_markers(map_markers)

    if params[:filter_search]
      if params[:filter_search][:sold] == '1'
        if current_user.private?
          search_options = { company: current_user.company }
        else
          search_options = { creator: current_user }
        end
        filterable_assets.search_options.deep_merge!(search_options)
      end
    end

    params[:page] ||= 1
    @filter_search = filterable_assets.filter_search
    @assets = filterable_assets.public_assets.page(params[:page]).per(@filter_search.page_size.to_i)
  end
end
