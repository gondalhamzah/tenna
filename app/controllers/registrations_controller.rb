class RegistrationsController < Devise::RegistrationsController

  # We override the default user deletion to soft-delete instead. This impacts public users who delete their own
  # accounts, as well as company admins who delete users from their company. However, Tenna admins who delete
  # users via /admin will hard delete users, as they don't hit this controller action.

  # DELETE /resource
  def destroy
    authorize! :destroy, resource

    resource.soft_delete
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    set_flash_message :notice, :destroyed if is_flashing_format?
    yield resource if block_given?
    respond_with_navigational(resource) { redirect_to after_sign_out_path_for(resource_name) }
  end

  protected

  # Public users are the only users who can sign up; must assign public_user role.
  def sign_up_params
    super.merge({role: 'public_user'})
  end

  def after_update_path_for(resource)
    if resource.public?
      user_profile_path
    else
      company_user_path(resource.company, resource)
    end
  end

end
