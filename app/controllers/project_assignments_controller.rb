class ProjectAssignmentsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :project, parent: true
  load_and_authorize_resource :project_assignment, through: :project, parent: false

  before_action :config_users, only: [:new, :create]

  # Makes your create and update action redirect to the collection on success.
  responders :collection

  def index
  end

  def new
  end

  def create
    @project_assignment = ProjectAssignment.new(project_assignment_params)
    config_project_assignment
    if @project_assignment.save
      respond_with @project_assignment
    else
      redirect_to project_assignments_url(@project)
    end
  end

  def update
    if @project_assignment.update(project_assignment_params)
      respond_with @project_assignment
    else
      redirect_to project_assignments_url(@project)
    end
  end

  def destroy
    @project_assignment.destroy
    respond_with(@project_assignment, location: params[:back] || project_assignments_url(@project))
  end

  private

  def config_project_assignment
    @project_assignment.project = @project
  end

  def config_users
    if @project.company.admin_company?
      @users = []
    else
      @users = @project.company.users.company_user - @project.users
    end
  end

  def project_assignment_params
    project_assignment_params = [:role]

    if params[:action] == 'create'
      project_assignment_params.concat([:user_id])
    end

    params.require(:project_assignment).permit(project_assignment_params)
  end

end
