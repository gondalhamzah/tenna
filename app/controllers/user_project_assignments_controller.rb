class UserProjectAssignmentsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :company, parent: true
  load_and_authorize_resource :user, through: :company, parent: true
  load_and_authorize_resource :project_assignment, through: :user, parent: false

  before_action :config_users, only: [:new, :create]


  def new
  end

  def create
    @project_assignment = ProjectAssignment.new(project_assignment_params)
    config_project_assignment
    if @project_assignment.save
      redirect_to company_user_path(@company, @user)
    else
      redirect_to company_user_path(@company, @user)
    end
  end

  private

  def config_project_assignment
    @project_assignment.user = @user
  end

  def config_users
    if @company.admin_company?
      @projects = []
    else
      @projects = @company.projects.available - @user.projects.available
    end
  end

  def project_assignment_params
    project_assignment_params = [:role]

    if params[:action] == 'create'
      project_assignment_params.concat([:project_id])
    end

    params.require(:project_assignment).permit(project_assignment_params)
  end

end
