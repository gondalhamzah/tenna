class ProjectsController < ApplicationController
  include BuildMapMarkers

  before_action :authenticate_user!, except: [:sample_project], :if =>lambda{ !params[:user].present?}

  before_action :config_projects, only: [:show]
  before_action :config_groups, only: [:show]

  load_and_authorize_resource if: lambda{ !params[:user].present?} and :action != :sample_project

  before_action :zapier_login!, :if =>lambda{ params[:user].present?}

  before_action :config_project, only: :new

  def index
    if params[:user].blank?
      map_markers = Project.map_markers_for(current_user)
      @markers = build_map_markers(map_markers)

      params[:page] ||= 1
      params[:filter_search] ||= {}
      if params[:filter_search][:distance_zip]
          if params[:filter_search][:distance_zip].split(',').count == 2
            splitted_zip_code = params[:filter_search][:distance_zip].split(',')
            distance_zip = splitted_zip_code.first
            country_name = splitted_zip_code.second
            params[:filter_search][:distance_zip] = distance_zip
            params[:filter_search][:country] = country_name
          end
      end
      filterable_projects = FilterableProjects.new(
        user: current_user,
        search_options: {company: current_user.company},
        params: params,
        zip_code: cookies[:zip_code]
      )

      @contacts = current_user.company.users
      @filter_search = filterable_projects.filter_search
      @projects = filterable_projects.projects.page(params[:page]).per(@filter_search.page_size.to_i)
    else
      render :nothing => true, :status => 200
    end
  end

  def search_by_name
    if current_user.private?
      search_options = { company: current_user.company }
    else
      search_options = { creator: current_user }
    end
    params[:filter_search] ||= {}
    params[:filter_search][:query] = params[:q]

    # TODO (AssetReports): Search in ES when no query string is sent, using AR now
    if params[:q].present?
      filterable_projects = FilterableProjects.new(
        user: current_user,
        search_options: search_options,
        params: params,
        zip_code: cookies[:zip_code]
      )
      @api_projects = filterable_projects.api_projects
    else
      @api_projects = current_user.company.projects
    end

    render json: @api_projects.to_json(only: [:id, :name])
  end

  def reverse_geocode_map
    result = Geocoder.search([params[:lat],params[:lng]]).first
    # long_name = result.address_components[1]["long_name"] if result.address_components[1]["long_name"].present?
    street = result.street_address if result.street_address
    zip = result.postal_code if result.postal_code
    data = {street: street,city: result.city, state: result.state, country: result.country, zip: zip}
    render json: {data: data}
  end

  def show
    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: { project: @project },
      params: params,
      zip_code: cookies[:zip_code]
    )

    map_markers = ZipCode.map_markers_for(filterable_assets.filtered_assets_scope)
    @markers = build_map_markers(map_markers)

    params[:page] ||= 1
    @filter_search = filterable_assets.filter_search
    @assets = filterable_assets.assets.page(params[:page]).per(@filter_search.page_size.to_i)
    @show_import_link = @project.member_of_project?(current_user) || current_user.company_admin? || current_user.field_member?
    @groups = @groups.where(:project_id=>params[:id])

    filterable_groups = FilterableGroups.new(
      user: current_user,
      search_options: {company: current_user.company, project: @project},
      params: params
    )

    @group_filter_search = filterable_groups.group_filter_search
    params[:g_page] ||= 1
    @project_groups = filterable_groups.groups.page(params[:g_page]).per(@group_filter_search.page_size.to_i)
  end

  def new
    @project.contact = current_user
  end

  def edit
  end

  def create
    @project = Project.new(project_params)
    config_project
    add_contact_as_admin

    @project.save
    respond_to do |format|
      format.html { respond_with @project }
      format.json { render nothing: true, status: 200 }
    end
    flash[:notice] = 'Site was successfully created.'
  end

  def upload_multiple
    success = true, count = 0
    file = params[:file]
    if file and File.extname(file.original_filename) == ".csv"
      Project.transaction do
        CSV.foreach(file.path, headers: true, header_converters: lambda { |header| header.downcase.gsub(/\s+/, '').gsub(/[^0-9a-z ]/i, '') }) do |row|
          row_hash = row.to_hash
          closeoutdate = Date.strptime(row_hash['closeoutdate'], '%m/%d/%Y') if row_hash['closeoutdate']
          attributes_hash = {
            project_num: row_hash['idnumber'],
            name: row_hash['name'],
            description: row_hash['description'],
            duration: row_hash['duration'],
            closeout_date: closeoutdate,
            contact_id: current_user.company.contact.id,
            address_attributes:
            {
              line_1: row_hash['addressline1'],
              line_2: row_hash['addressline2'],
              city:   row_hash['city'],
              state:  row_hash['state'],
              zip:    row_hash['zipcode'],
              country:row_hash['country']
            }
          }
          @project = Project.new attributes_hash
          config_project
          unless @project.save
            success = false
            raise ActiveRecord::Rollback
          end
          count += 1
        end
      end
    else
      flash[:error] = 'Error: Invalid file.'
    end

    if success
      flash[:notice] = "#{count} Projects imported successfully."
    else
      flash[:error] = 'Error: Invalid data.'
    end

    redirect_to projects_path
  end

  def update
    @project.attributes = project_params
    add_contact_as_admin

    @project.save
    respond_with @project
    flash[:notice] = 'Site was successfully updated.'
  end

  def destroy
    if @project.users.any?
      flash[:alert] = 'You may want to reassign the users associated with this project before deleting.'
      redirect_to project_url(@project)
    elsif @project.groups.available.any?
      flash[:alert] = 'In order to delete this project you must first reallocate all groups currently assigned to this project.'
      redirect_to project_url(@project)
    elsif @project.assets.available.any?
      flash[:alert] = 'In order to delete this project you must first reallocate all assets currently assigned to this project.'
      redirect_to project_url(@project)
    else
      @project.soft_delete
      respond_with(@project, location: projects_url)
      flash[:notice] = 'Site was successfully deleted.'
    end
  end

  def sample_project
    project = Project.first
    render json: { status: 201, project: project }
  end

  private

  def config_project
    @project ||= Project.new()
    @project.address ||= Address.new()
    if params[:user].blank?
      @project.company = current_user.company
    else
      @project.contact = @user
      @project.company = @user.company
    end
  end

  def project_params
    if params[:user].present?
      zapier_params
    end
      params.require(:project).permit(:name, :project_num, :description, :contact_id, :duration, :closeout_date, :contact_sms_number, :contact_email, address_attributes: [:id, :line_1, :line_2, :city, :state, :zip, :country,:lat,:lng,:current_coords,:change_addr])
  end

  # This ensures that, upon project creation/update, the project contact gets added to the project as a project admin.
  # We do allow for them to be removed from the project, but remain the contact, but adding them initially is a
  # sensible default.
  def add_contact_as_admin
    unless @project.contact.role_gte_company_admin? || (@project.contact.projects.include? @project)
      @project.project_assignments.build({project: @project, user: @project.contact, role: ProjectAssignment.roles[:project_admin]})
    end
  end

  def zapier_params
    if @user
      params[:project][:address_attributes] = Hash.new
      params[:project][:address_attributes][:line_1] = params[:line_1]
      params[:project][:address_attributes][:line_2] = params[:line_2]
      params[:project][:address_attributes][:city] = params[:city]
      params[:project][:address_attributes][:state] = params[:state]
      params[:project][:address_attributes][:zip] = params[:zip]
      params[:project][:address_attributes][:country] = params[:country]
    end
  end
end
