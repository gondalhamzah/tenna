class CompanySafetyConfigurationsController < ApplicationController
  before_action :set_company_safety_configuration, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @company_safety_configurations = CompanySafetyConfiguration.where(company_id: current_user.company_id)
    respond_with(@company_safety_configurations)
  end

  def show
    respond_with(@company_safety_configuration)
  end

  def new
    @company_safety_configuration = CompanySafetyConfiguration.new(company_id: current_user.company_id)
    respond_with(@company_safety_configuration)
  end

  def edit
  end

  def create
    @company_safety_configuration = CompanySafetyConfiguration.new(company_safety_configuration_params)
    @company_safety_configuration.save
    respond_with(@company_safety_configuration, location: company_safety_configurations_path)
  end

  def update
    @company_safety_configuration.update(company_safety_configuration_params)
    respond_with(@company_safety_configuration, location: company_safety_configurations_path)
  end

  def destroy
    @company_safety_configuration.destroy
    respond_with(@company_safety_configuration, location: company_safety_configurations_path)
  end

  private
    def set_company_safety_configuration
      @company_safety_configuration = CompanySafetyConfiguration.find(params[:id])
    end

    def company_safety_configuration_params
      params.require(:company_safety_configuration).permit(:gauge_id, :company_id, :score_type, :score_weight, :high_points, :medium_points, :low_points)
    end
end
