class ZapierController < ApplicationController

before_filter :authenticate_username_and_pwd!, only: [:create, :destroy, :update_lat_lng]
protect_from_forgery with: :null_session

################## SAMPLE FUNCITON
  def sample_project
    project = [{id: "12123", name: "Sample Project Title", description: "Sample Description" , company: "Sample COmpany", contact_id: "12341"}]
    render json: {data: project }
  end

  def sample_asset
    asset = [{id: "12123", title: "Sample Asset Title", date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Asset_description: "Sample Description of asset", Project_id: "12341", Project_Name: "sample_project", Description: "This asset was just created"}]
    render json: {data: asset }
  end

  def sample_marked_rental_or_public
    asset = [{id: "12123", title: "Sample Asset Title", date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Asset_description: "Sample Description of asset", Project_id: "12341", Project_Name: "sample_project", Description: "This asset was marked as Public"}]
    render json: {data: asset }
  end

  def sample_asset_remove_mp
    asset = [{id: "12123", title: "Sample Asset Title", date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Asset_description: "Sample Description of asset", Project_id: "12341", Project_Name: "sample_project", Description: "This asset was removed from the marketplace"}]
    render json: {data: asset }
  end

  def sample_avaiable_on
    asset = [{id: "12123", title: "Sample Asset Title", date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Asset_description: "Sample Description of asset", Project_id: "12341", Project_Name: "sample_project", Description: "The 'avaialble on' date for this asset has passed."}]
    render json: {data: asset }
  end

  def sample_video_view
    asset = [{Description: "The user Test User has viewed his/her first video", User_ID: "Sample id", User_Name: "Sample Name", User_Role: "Sample Role", Company_Name: "Sample Company", Company_id: "Sample ID", Video_Name: "Sample Name", Video_id: "Sample id"}]
    render json: {data: asset }
  end

  def sample_asset_reallocation
    data = Asset.last
    asset = [{id: "21212", title: "Sample Asset Name", Currenct_Project: data.project.name, date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Currenct_Project_id: data.project.id, Previous_Project: "Sample", Previous_Project_id: "12345", Description: "This asset was moved from project: Sample to project: "+ data.project.name, Asset_description: data.description}]
    render json: {data: asset}
  end

  def sample_route_stop
    route = [{asset_title: "Sample Asset Title", date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), project_name: "Sample Project Name", asset_description: "Sample Description", driver_name: "Driver A", driver_id: "1231", route_name: "Route Abc", route_id: "1432"}]
    render json: {data: route}
  end

  def sample_maintenance_data
    data = [{id: "2", title: "Sample Asset Title", Notes: "Sample notes", date: Time.now.strftime('%m/%d/%Y %I:%M %p %Z'), Description: "Maintenance has been request for this asset", Project_id: "5", Project_Name: "Sample Project Name", Asset_description: "Asset description"}]
    render json: {data: data}
  end

  def update_lat_lng
    if !params[:zapier].blank?
      asset = Asset.find params[:zapier][:asset_id]
        asset.update(current_lat: params[:zapier][:lat], current_lng: params[:zapier][:lng])
        render :nothing => true, :status => 200
    else
      render :nothing => true, :status => 200
    end
  end

  def create
    sub_url = params[:subscription_url]
    target_url = params[:target_url]
    event = params[:event]
    provider = "zapier"
    hook = Hook.create(subscription_url: sub_url, target_url: target_url,
              event_name: event,company_id: @user.company_id, provider: provider)
    render :nothing => true, :status => 500 and return unless hook.save
    render :json => hook.to_json(:only => :id), :status => 201
  end

  def destroy
    hook = Hook.find(params[:id]) if params[:id]
    if hook.nil? && params[:subscription_url]
      hook = Hook.find_by_subscription_url(params[:subscription_url]).destroy
    end
    hook.destroy
    render :nothing => true, :status => 200
  end

  def create_user
    if !params[:zapier].blank?
      zapier_login!
      @zapier_user = @user
      company_assignment = CompanyAssignmentsController.new
      company_assignment.request = request
      company_assignment.response = response
      company_assignment.create
    end
    render :nothing => true, :status => 200
  end

  def update_hour_millage_asset
    if !params[:zapier].blank?
      asset = Asset.find params[:zapier][:asset_id]
      asset.hours = params[:zapier][:hour_millage]
      asset.miles = params[:zapier][:hour_millage]
      if asset.save!
        render :nothing => true, :status => 200
      else
        render :error => 'Cannot Update asset', :status => 500
      end
    else
      render :nothing => true, :status => 200
    end
  end

  def authenticate_username_and_pwd!
    @user = User.find_by_email(params[:user][:email])
    unless @user.valid_password?(params[:user][:password])
      render unauthenticated: 'Unable to signin to tenna.', :status => 403
    end
  end
end
