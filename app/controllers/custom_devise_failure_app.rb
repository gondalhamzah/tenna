class CustomDeviseFailureApp < Devise::FailureApp

  protected

  # warden_options[:message] is passed by devise using the inactive_message
  # method on the user model.
  # it should have a `message` and `options` key that will be passed to I18n.
  def custom_message?
    warden_options[:message].present? && warden_options[:message].is_a?(Hash)
  end

  def i18n_options(options)
    custom_message? ? options.merge(warden_options[:message][:options]) : super
  end

  def i18n_message(default = nil)
    @message = warden_options[:message][:message] if custom_message?
    super
  end
end
