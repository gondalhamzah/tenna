class WantedAssetsController < ApplicationController
  include BuildMapMarkers

  before_action :authenticate_user!
  before_action :config_projects,:want_it_subscription, only: [:index, :new, :create, :edit, :update]

  load_and_authorize_resource
  skip_load_and_authorize_resource only: [:update_multiple]

  def index
    params[:filter_search] ||= {}
    if params[:filter_search][:category].present? and params[:filter_search][:category].to_i != 0
      cat_id = params[:filter_search][:category]
      params[:filter_search][:category] = Category.find(cat_id).name
    end

    filterable_assets = FilterableWantedAssets.new(
      user: current_user,
      search_options: { company: current_user.company },
      params: params,
      zip_code: cookies[:zip_code]
    )
    map_markers = ZipCode.map_markers_for(filterable_assets.filtered_assets_scope)
    @markers = build_map_markers(map_markers)

    params[:page] ||= 1
    @wanted_assets = filterable_assets.assets.page(params[:page])
    @filter_search = filterable_assets.filter_search
  end

  def edit
    @wanted_asset = @wanted_asset.becomes(WantedAsset)
    @project_zips = @projects.map{ |project| {:project_id => project.id, :zip =>project.address.zip, :country => project.address.country }.compact }
    @users = current_user.company.users
  end

  def update
    if wanted_asset_params[:event].nil?
      @wanted_asset.update(wanted_asset_params)
    else
      @wanted_asset.fire_state_event(wanted_asset_params[:event])
    end
    respond_with(@wanted_asset, location: wanted_assets_url)
  end

  def update_multiple
    asset_ids = params[:asset_ids].split(',')
    assets = WantedAsset.accessible_by(current_ability, :update)
    if params[:asset_action] == 'fulfill'
      assets = assets.accessible_by(current_ability, :fulfill)
    else params[:asset_action] == 'delete'
      assets = assets.accessible_by(current_ability, :destroy)
    end
    authorized_asset_ids = assets.where(id: asset_ids).pluck(:id)
    authorized_assets = WantedAsset.where(id: authorized_asset_ids)

    actions = {
      fulfill: -> {
        WantedAsset.transaction do
          authorized_assets.each do |a|
            a.fulfill
            a.fulfilled_date = Time.now()
            a.save
          end
        end
      },
      delete: -> {
        WantedAsset.transaction do
          authorized_assets.each do |a|
            a.soft_delete
          end
        end
      }
    }

    actions[params[:asset_action].to_sym].call
    
    WantedAssetsIndex::WantedAsset.update_index(authorized_asset_ids)

    if authorized_asset_ids.size != asset_ids.size
      not_all_updated = " #{ActionController::Base.helpers.pluralize(asset_ids.size - authorized_asset_ids.size, 'asset')} not updated because of missing authorization."
    end

    flash[:notice] = "Successfully updated #{ActionController::Base.helpers.pluralize(authorized_asset_ids.size, 'asset')}.#{not_all_updated}"
    redirect_to params[:redirect_to]
  end

  def create
    @wanted_asset = WantedAsset.new(wanted_asset_params)
    config_wanted_asset
    @wanted_asset.save
    respond_with(@wanted_asset, location: wanted_assets_url)
  end

  def new
    config_wanted_asset
    @project_zips = @projects.map{ |project| {:project_id => project.id, :zip =>project.address.zip, :country => project.address.country }.compact }
    @users = current_user.company.users
    @wanted_asset.build_zip_code
    @wanted_asset.type = WantedAsset::TYPES[0]
  end

  def destroy
    @wanted_asset.soft_delete
    respond_with(@wanted_asset, location: wanted_assets_url)
  end

  # Since wanted assets are, comparatively, much simpler than regular assets, and don't extend the Asset base class,
  # filtering (my wanted) is done here instead of via asset_search instance.

  def self.filter(filter)
    if filter
      where(creator: filter)
    end
  end

  def want_it_subscription
    if current_user.company
      company = current_user.company
      @company_wantit_subscription = company.subscriptions.where(category: 'wantit',state: 'active',payment: true).first
    end
  end


  private

  def filter_search_params
    params.require(:filter_search).permit(:sort, :my_wanted, :query, :category, :fulfilled, :project)
  end

  def config_wanted_asset
    @wanted_asset ||= WantedAsset.new()
    @wanted_asset.creator = current_user
    @wanted_asset.company = current_user.company
  end

  def wanted_asset_params
    wanted_asset_params = [:title, :description, :category_id, :contact_id, :location_zip, :project_id, :needed_on, :type, :event,:country_name,:priority]

    # if (params.has_key? 'equipment')
    #   params_type = :equipment
    # elsif (params.has_key? 'material')
    #   params_type = :material
    # else
    #   params_type = :wanted_asset
    # end

    params.require(:wanted_asset).permit(wanted_asset_params)
  end

  def wanted_assets_order sort
    WantedAsset.sorts(current_user)[sort.to_sym]
  end
end
