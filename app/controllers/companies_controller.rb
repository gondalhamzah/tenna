class CompaniesController < ApplicationController
  include TrackingCodesHelper
  before_action :authenticate_user!
  load_and_authorize_resource

  def show
    @company_users = @company.users.where('role in  (?)', [0,1, 4]).page(params[:c_page])
    @project_users = @company.users.where.not('role in (?)', [0, 1, 4]).page(params[:p_page])
  end

  def edit
  end

  def print_qr
  end

  def download_printed_qr_codes
    if params.has_key? 'label_file_name'
      if Rails.env.development?
        if File.exists? qr_codes_file_path(@company.id, params['label_file_name'])
          send_file(qr_codes_file_path(@company.id, params['label_file_name']),
            filename: qr_codes_file_name(@company.id, params['label_file_name']),
            :type => 'application/pdf',
            :disposition => 'attachment' )
        else
          flash[:error] = 'QR codes you are trying to access does not exist.'
          redirect_to print_company_qr_path(@company)
        end
      else
        obj = S3_OBJECT.object(qr_codes_file_path(@company.id, params[:label_file_name]))
        if obj.exists?
          data = open(obj.presigned_url(:get, expires_in: 60 * 60))
          send_data data.read, filename: "#{qr_codes_file_name(@company.id, params[:label_file_name])}", type: 'application/pdf', :x_sendfile => true
        else
          flash[:error] = 'QR codes you are trying to access does not exist.'
          redirect_to print_company_qr_path(@company)
        end
      end
    else
      flash[:error] = 'File label is not specified.'
      redirect_to print_company_qr_path(@company)
    end
  end

  def email_printed_qr_codes
    response = Hash.new

    file_obj = Rails.env.development? ? (File.exists? qr_codes_file_path(@company.id, params['label_format'])) : S3_OBJECT.object(qr_codes_file_path(@company.id, params['label_format'])).exists?
    company = current_user.company
    @company_tracking_subscription = company.subscriptions.where(category: 'add tracking to asset',state: 'active',payment: true).first
    if file_obj and @company_tracking_subscription.present?
      if TrackingCodeMailer.send_generated_qr_codes(current_user.email, qr_codes_file_name(@company.id, params['label_format']), @company.id, params['label_format']).deliver
        response = get_response(params[:action], 'success')
      else
        response = get_response(params[:action], 'failed')
      end
    else
      response = get_response(params[:action], 'not_generated')
    end
    respond_to do |format|
      format.json { render json: response }
    end
  end

  def generate_printed_qr_codes
    @company.allocate_tracking_codes
    response = Hash.new
    if (valid_label_and_print_type)
      if GenerateQrCodesJob.perform_later(@company.id, params[:label_format])
        @company.update(qr_status: 'requested', qr_print_type: params['printedType'], qr_format: params[:label_format])
        response = get_response(params[:action], 'generating')
      else
        response = get_response(params[:action], 'failed')
      end
    else
      response = get_response(params[:action], 'invalid_print_or_label')
    end
    respond_to do |format|
      format.json { render json: response }
    end
  end

  def qr_code_status
    response = Hash.new
    if @company.qr_status == 'created'
      response[:status] = true
      response[:used_codes_count] = @company.total_used_codes
      response[:unused_codes_count] = @company.total_unused_codes
    else
      response[:status] = false
    end
    return render json: response
  end

  def update
    @company.update(company_params)
    respond_with @company
  end

  def resend_invitation
    user = User.find(params[:format])
    user.invite!(current_user)
    redirect_to company_path(current_user.company.id), notice: 'Re-invitation email sent successfully.'
  end

  def valid_label_and_print_type
    if ((params.has_key? 'printedType') && (params.has_key? 'label_format'))
      if is_valid_print_type_and_label params['printedType'], params['label_format']
        true
      end
    end
  end

  private

  def company_params
    params.require(:company).permit(:name, :company_logo, :remove_company_logo, :region, :website, :contact_id, address_attributes: [:id, :line_1, :line_2, :city, :state, :zip])
  end
end
