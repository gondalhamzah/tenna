class SafetyReportsController < ApplicationController
  before_action :authenticate_user!
  before_action :safety_reports_subscription

  def landing_page
    @company_safety_configurations = CompanySafetyConfiguration.includes(:gauge).where(company_id: current_user.company_id, score_type: 'deduction')

    if @company_safety_configurations.blank?
      render :have_safety_config
      return false
    end

    @company_scorecard = CompanyScoreCard.new(company: User.current.company,
                                              start_date: params[:start_date_submit],
                                              end_date: params[:end_date_submit],
                                              scorecard_type: params[:scorecard_type],
                                              operator_id: params[:operator_id],
                                              asset_id: params[:vehicle_id]).execute_report
    if request.xhr?
      resp_partial = render_to_string(partial: 'safety_reports/safety_reports_card', locals: {company_safety_configurations: @company_safety_configurations, company_scorecard: @company_scorecard})
      response_obj = {}
      response_obj['partial'] = resp_partial
      respond_to do |format|
        format.json { render json: response_obj.to_json }
      end
    else

    end
  end

  def safety_reports_subscription
    authorize! :safety_reports_subscription, :safety_report
    if current_user.company
      company = current_user.company
      @safety_reports_subscription = company.subscriptions.where(category: 'safety reports',state: 'active',payment: true).first
      render :not_subscribed if @safety_reports_subscription.blank?
      return false
    else
      redirect_to root_path
    end
  end

  def not_subscribed
    respond_to do |format|
      format.html
    end
  end

def have_safety_config
end

#   def get_scorecard
#     @company_safety_configurations = CompanySafetyConfiguration.includes(:gauge).where(company_id: current_user.company_id)
#     @company_scorecard = CompanyScoreCard.new(company: User.current.company).execute_report
#     @scorecard_type = params[:score_cat]
#     resp_partial = render_to_string(partial: 'safety_reports/safety_reports_card')
#     response_obj = {}
#     response_obj['partial'] = resp_partial
#     respond_to do |format|
#       format.json { render json: response_obj.to_json }
#     end
#   end
end
