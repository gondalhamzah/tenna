class CompanyAssignmentsController < ApplicationController
  before_action :authenticate_user!, :if =>lambda{ !params[:zapier].present? }

  load_and_authorize_resource :company, parent: true, if: lambda{ !params[:zapier].present?}
  load_and_authorize_resource :user, through: :company, parent: false, id_param: :id, if: lambda{ !params[:zapier].present?}

  before_action :zapier_login!, :if =>lambda{ params[:zapier].present? }
  # Makes your create and update action redirect to the collection on success.
  responders :collection

  def index
    @users = @users.not_deleted
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    config_user

    # User will have a no password validation error, despite being invited via Devise Invitable.
    # Seems to be a bug w/ Devise Invitable. For now, we're expecting this validation error,
    # so we ignore it explicitly.
    @user.validate
    @user.errors.delete(:'password')
    if @user.errors.count == 0
      if @zapier_user
        @user.invite!(@zapier_user)
      else
        @user.invite!(current_user)
      end
    respond_to do |format|
      format.html { redirect_to company_url(@company) }
      format.json { render nothing: true, status: 200 }
    end

    else
      render :new
    end
  end

  def reinvite_user
    @user = User.find_by_id(params[:user_id])
    if @user.invite!(current_user)
      flash[:notice] = "Invitation sent successfully."
    else
      flash[:notice] = "Invitation could not sent successfully."
    end
    respond_to do |format|
      format.js
    end
  end

  def update
    if @user.update_without_password(user_params)
      redirect_to company_user_url(@company, @user)
    else
      render :edit
    end
  end

  def destroy
    unless (current_user.company_admin? and @user.company_admin?)
      @user.soft_delete
    else
      flash[:alert] = "You do not have permissions to remove #{@user.name}"
    end
    redirect_to company_url(@company)
  end

  private

  def config_user
    @user.company = @company
    if params[:zapier].present?
      @user.role = "company_user"
      new_user = @user
      @zapier_user = zapier_login!
      new_user.company = @zapier_user.company
      @user = new_user
    end
  end

  def user_params
    user_params = [:first_name, :last_name, :email, :phone_number, :title, :subscription_opt]

    if ['create', 'update'].include?(params[:action])
      user_params.concat([:role])
    end
    if !params[:zapier].present?
      params.require(:user).permit(user_params)
    else
      params.require(:zapier).permit(user_params)
    end
  end

end
