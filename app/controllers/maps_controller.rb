class MapsController < ApplicationController
  # RESTful endpoint for returning asset(s) in a given ZIP code. On maps, we cluster assets by zip code.
  # If > 1, we say "X assets in zip code". Otherwise, we hit this endpoint to retrieve asset details dynamically.

  def zip_validator
    zip_code = params[:zip_code]
    country_code = params[:country_code]
    render json: {
      zip_code: zip_code,
      country_code: country_code,
      valid: (GenericGeocoder.valid_zip_code?(zip_code,country_code))
    }
  end

  def zip_with_country_validator
    zip_wth_country = params[:zip_country]
    render json: {
      zip_code: zip_wth_country,
      valid: (GenericGeocoder.valid_zip_with_country?(zip_wth_country))
    }
  end

  def index
    filterable_assets = FilterableAssets.new(
      user: current_user,
      params: params,
      zip_code: cookies[:zip_code]
    )

    params[:page] ||= 1
    @assets = filterable_assets.assets.page(params[:page])
    @filter_search = filterable_assets.filter_search

    # Mitigates performance risk. Prevents users from serializing all viewable assets.
    unless @filter_search.distance_miles == 'Exact' && @filter_search.distance_zip.present?
      head :bad_request
    end
  end

end
