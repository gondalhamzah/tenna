class PasswordsController < Devise::PasswordsController
  # POST /resource/password
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    yield resource if block_given?
    if successfully_sent?(resource)
      respond_with({}, location: after_sending_reset_password_instructions_path_for(resource_name))
    else
      respond_with(resource, location: new_password_path(resource_name))
      flash.discard('notice')
    end
  end

  protected

  def successfully_sent?(resource)
    notice = if Devise.paranoid
      resource.errors.clear
      :send_paranoid_instructions
    elsif resource.errors.empty? && resource.active_for_authentication?
      :send_instructions
    end

    if notice
      set_flash_message :notice, notice if is_flashing_format?
      true
    elsif resource.persisted? && !resource.active_for_authentication?
      set_flash_message :alert, resource.inactive_message if is_flashing_format?
      false
    end
  end
end
