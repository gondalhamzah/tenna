class MapController < ApplicationController
  def index
    if request.xhr?
      # respond to Ajax request
      @render_element_id = params[:render_element_id]
      @total = params[:total].to_i

      elements_list = params[:elements_list].first(2)
      entity        = params[:entity]
      @group_type   = params[:group_type]
      buckets = params[:buckets]
      @more_path = case @group_type
                   when 'owned_assets'         then assets_path({filter_search: { geohash: buckets, buy: true, public: current_user.public?}})
                   when 'rental_assets'        then assets_path({filter_search: { geohash: buckets, rental: true, public: current_user.public?}})
                   when 'public_assets'        then marketplace_path({filter_search: { geohash: buckets, is_mine: true}})
                  #  when 'private_assets'       then assets_path({filter_search: { geohash: buckets, is_mine: false}})
                   when 'public_public_assets' then marketplace_path({filter_search: { geohash: buckets, is_others: true}})
                   when 'projects'             then projects_path({filter_search: { geohash: buckets, is_mine: false}})
                   end

      @partial = (entity == 'Asset') ? 'assets_sidebar' : 'projects_sidebar';

      @entities = entity.constantize.where(id: elements_list)
    end

    respond_to do |format|
      format.js
    end
  end
end
