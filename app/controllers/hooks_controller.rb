class HooksController < ApplicationController
  def create
    hook = Hook.new params
    render :nothing => true, :status => 500 and return unless hook.save
    Rails.logger.info "Created REST hook: #{hook.inspect}"
    # The Zapier documentation says to return 201 - Created.
    render :json => hook.to_json(:only => :id), :status => 201
  end

  def destroy
    hook = Hook.find(params[:id]) if params[:id]
    if hook.nil? && params[:subscription_url]
      hook = Hook.find_by_subscription_url(params[:subscription_url]).destroy
    end
    Rails.logger.info "Destroying REST hook: #{hook.inspect}"
    hook.destroy
    render :nothing => true, :status => 200
  end
end