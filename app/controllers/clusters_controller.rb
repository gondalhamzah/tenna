class ClustersController < ApplicationController
  skip_before_filter :set_location
  respond_to :json

  def index
    @response = {}

    %w(assets projects).each do |type|
      @response[type] = clusters_aggregator_for(type).new(**clusters_params.symbolize_keys).results
    end

    respond_to do |format|
      format.json do
        render json: @response.to_json
      end
    end
  end

  private

  def clusters_params
    params.permit(:company_id, :creator_id, :zoom, bounds: [top_left: [:lat, :lng], bottom_right: [:lat, :lng]])
  end

  def clusters_aggregator_for(type)
    #For some reasons, .const_get returns Asset model
    "ClustersAggregator::#{type.classify}".constantize
  end
end
