class ExportsController < ApplicationController
  before_action :authenticate_user!
  before_action :filtered_assets_result, only: [:create]

  include AssetsHelper

  def index
    @project_exports = current_user.project_exports.where(export_type: 0).limit(5)
  end

  def export_project
    @project_exports = current_user.project_exports.where(export_type: 1).limit(5)
  end

  def export_assets
    @project_exports = current_user.project_exports.where(export_type: 2).limit(5)
  end

  def show
    @export = current_user.project_exports.find(params[:id])
  end

  def create
    filtered_assets = @filtered_assets.to_ary if @filtered_assets
    exported_at = format_time(Time.now)
    assets_filter = filter_description(params[:filtered_assets], true)
    (params[:filtered_assets].present? && params[:filtered_assets][:hide_filters].present?) ? is_filtered = false : is_filtered = true
    export_project_only = params['export_project_only'] == 'true'
    export_assets_only = params['export_assets_only'] == 'true'
    export_type = 0
    if export_project_only
      export_type = 1
    elsif export_assets_only
      export_type = 2
    end
    @project_export = current_user.project_exports.new(export_params.merge({enqueued_at: Time.now,export_type: export_type}))
    if @project_export.save
      @project_export.update(export_filtered_assets: assets_filter, is_filtered_asset: is_filtered)
      @project_export.enqueue(export_project_only,export_assets_only,filtered_assets,browser_timezone,assets_filter,exported_at,is_filtered)
      if export_project_only
        redirect_to export_project_exports_path
      elsif export_assets_only
        redirect_to export_assets_exports_path(export_assets_only: true, filtered_assets: params[:filtered_assets])
      else
        redirect_to exports_path
      end
    else
      if export_assets_only
        redirect_to assets_path
      else
        redirect_to projects_path
      end
    end
  end

  private
  def export_params
    params.permit(:project_id)
  end

  def filtered_assets_result
    if session[:filterable_assets_params]
      params = ActionController::Parameters.new(session[:filterable_assets_params])
      tracking_code = params[:tracking_code]
      assign_tracking_code = !tracking_code.nil?

      if current_user.private?
        search_options = {company: current_user.company}
      else
        search_options = {creator: current_user}
      end

      params[:filter_search] ||= {}
      params[:filter_search][:without_tracking_code] = assign_tracking_code

      filterable_assets = FilterableAssets.new(
        user: current_user,
        search_options: search_options,
        params: params,
        zip_code: cookies[:zip_code]
      )

      total_assets_count = filterable_assets.assets.total
      @filtered_assets = filterable_assets.assets.limit(total_assets_count)
    end
  end
end
