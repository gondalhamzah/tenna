class GroupsController < ApplicationController
  include BuildMapMarkers
  before_action :authenticate_user!
  before_action :config_projects, only: [:index, :new, :edit, :create, :show]
  before_action :config_group, only: :new
  before_action :config_groups, only: [:show]
  load_and_authorize_resource

  def index
    params[:page] ||= 1
    filterable_groups = FilterableGroups.new(
      user: current_user,
      search_options: {company: current_user.company},
      params: params
    )

    @group_filter_search = filterable_groups.group_filter_search
    @groups = filterable_groups.groups.page(params[:page]).per(@group_filter_search.page_size.to_i)
  end

  def create
    @group = Group.new(group_params)
    config_group

    @group.save
    respond_with @group
  end

  def upload_multiple
    success = true, count = 0
    file = params[:file]
    if file and File.extname(file.original_filename) == ".csv"
      Group.transaction do
        CSV.foreach(file.path, headers: true, header_converters: lambda { |header| header.downcase.gsub(/\s+/, '').gsub(/[^0-9a-z ]/i, '') }) do |row|
          row_hash = row.to_hash
          attributes_hash = {
            project_id: current_user.company.project && current_user.company.project.id,
            group_num: row_hash["id"],
            name: row_hash["name"],
            description: row_hash["description"],
            notes: row_hash["notes"],
            client: row_hash["client"],
            company_id: current_user.company.id
          }
          @group = Group.new attributes_hash
          unless @group.save
            success = false
            raise ActiveRecord::Rollback
          end
          if row_hash.include?("zipcode")
            success = false
          end
          count += 1
        end
      end
    else
      flash[:error] = "Error: Invalid file."
    end

    if success
      flash[:notice] = "#{count} Groups imported successfully."
    else
      flash[:error] = "Error: Invalid data."
    end

    redirect_to groups_path
  end

  def update_multiple
    groups_ids = params[:group_ids].split(',')

    groups = Group.accessible_by(current_ability, :update)
    if params[:group_action] == 'delete'
      groups = groups.accessible_by(current_ability, :destroy)
    end
    authorized_group_ids = groups.where(id: groups_ids).pluck(:id)

    authorized_groups = Group.where(id: authorized_group_ids)

    actions = {
      mark_as_closed: -> {
        Group.transaction do
          authorized_groups.each do |ag|
            ag.update(state: 'closed')
          end
        end
      },
      delete: -> {
        Group.transaction do
          authorized_groups.each do |ag|
            ag.soft_delete
          end
        end
      },
      move: -> {
        project = Project.find(params[:project_id])

        # only operate on groups whose project_id is changing
        authorized_groups = authorized_groups.where("project_id != #{project.id} OR project_id IS NULL")
        authorized_group_ids = authorized_groups.pluck(:id)

        # this workaround done just to capture the asset history on mass actions
        Group.transaction do
          authorized_groups.each do |ag|
            if ag.assets.count > 500
              MoveMultipleJob.perform_now(ag, project)
            else
              ag.update(project_id: project.id)
            end
          end
        end
      }
    }

    actions[params[:group_action].to_sym].call

    GroupsIndex::Group.update_index(authorized_groups)

    if authorized_groups.size != groups_ids.size
      not_all_updated = " #{ActionController::Base.helpers.pluralize(groups_ids.size - authorized_group_ids .size, 'group')} not updated because of missing authorization."
    end

    flash[:notice] = "Successfully updated #{ActionController::Base.helpers.pluralize(authorized_group_ids.size, 'group')}.#{not_all_updated}"
    redirect_to params[:redirect_to]
  end

  def show
    filterable_assets = FilterableAssets.new(
      user: current_user,
      search_options: { group: @group },
      params: params,
      zip_code: cookies[:zip_code]
    )

    map_markers = ZipCode.map_markers_for(filterable_assets.filtered_assets_scope)
    @markers = build_map_markers(map_markers)

    params[:page] ||= 1
    @show_import_link = current_user.company_admin?
    @filter_search = filterable_assets.filter_search
    @assets = filterable_assets.assets.page(params[:page]).per(@filter_search.page_size.to_i)
  end

  def new
  end

  def edit
  end

  def update
    @group.attributes = group_params
    @group.save
    respond_with @group
  end

  def destroy
    if @group.assets.available.any?
      flash[:alert] = "In order to delete this group you must first reallocate all assets currently assigned to this group"
      redirect_to group_url(@group)
    else
      @group.soft_delete
      redirect_to groups_url
    end
  end

  def mark_as_closed
    if @group.assets.available.any?
      flash[:alert] = "In order to close this group you must first reallocate all assets currently assigned to this group"
      redirect_to group_url(@group)
    else
      @group.update(state: 'closed')
      flash[:notice] = "Group closed successfully."
      redirect_to groups_url
    end
  end

  private

  def config_group
    @group ||= Group.new()
    @group.project = @projects.available[0] if @projects.available.length > 0 and !@group.project
    @group.company = current_user.company
  end

  def group_params
    params.require(:group).permit(:name, :group_num, :client, :project_id, :description, :notes)
  end
end
