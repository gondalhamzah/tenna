class LeadsController < ApplicationController
  def create
    params[:lead] = params.merge(params).except(:controller, :action)
    @lead = Lead.new(lead_params) if params[:lead].present?
    if params[:lead].present? and @lead.save
      render json: {data: @lead ,:status => 200 }
    else
      render json: { status: 500, message: 'Something went wrong.' }
    end
  end

  private
  def lead_params
    params.require(:lead).permit(:lead_type, :first_name, :last_name, :email, :phone_num, :industry, :reason_contact, :title, :company_name, :comment)
  end
end