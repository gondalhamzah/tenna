class InvitationsController < Devise::InvitationsController
  before_action :authenticate_user!

  protected

  # We override invite_params to set the role of the invited user. Allowed values are based on inviter's role.
  # This made more sense before removing company admin's ability to invite other company admins.
  def invite_params
    ip = devise_parameter_sanitizer.sanitize(:invite)
    ip[:company] = current_inviter.company

    if current_inviter.company_user?
      ip[:role] = User.roles[:company_user]
    elsif current_inviter.company_admin?
      if ip[:role] != 'company_admin' && ip[:role] != 'company_user'
        ip[:role] = User.roles[:company_user]
      end
    end

    ip
  end

  def after_invite_path_for(resource)
    params['back'] || super  # grab the back link from the URI param
  end

end
