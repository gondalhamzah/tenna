class AnalyticsController < ApplicationController
  include ChartHelper
  include VersionGraph
  include AssetsVersionsHelper

  respond_to :json

  def create
    start_date = params[:start_date]
    end_date = params[:end_date]

    queries = false
    if !params['groups'].blank?
      queries = get_groups_filter(params['groups'])
    end
    @versions = get_base_versions(params[:asset_id], start_date, end_date, queries, params[:company_id])
    @company_wide = !params[:company_id].blank?
    build_group_data()

    @csvData = [['Category', 'Sub Category', 'Asset Name', 'Date', 'Action', 'New', 'Old', 'User']]
    @csvData.shift if !@company_wide
    @versions.each do |v|
      row = []
      row << v.category_group
      row << v.category_name
      row << v.asset_title if @company_wide
      row << DateTime.parse(v.created_at).in_time_zone.strftime('%m/%d/%Y %I:%M %p %Z')
      row << version_action_label(v)
      row << version_value(v, 'to')
      row << version_value(v, 'from')
      row << v.whodunnit_name
      @csvData << row
    end
  end

  def get_token_types
    @token_types = TrackingCode.token_types.map do |tc_key, tc_val|
      {value: tc_key.downcase, label: tc_key}
    end

    render json: @token_types.to_json
  end


  def asset_daily_filter
    asset_id = params[:asset_id]
    search = AssetsVersionSearch.new
    asset_history = AssetsVersionSearch.get_all_history(asset_id, "tracking_info")
    asset_history = search.asset_daily_filters(asset_history)
    render json: { asset_id => asset_history}
    # render json: {asset_daily: asset_history.to_json}
  end

# pass date as params to find minit
  def asset_hour_filter
    asset_id = params[:asset_id]
    date = params[:date]
    search = AssetsVersionSearch.new
    asset_history = AssetsVersionSearch.get_all_history(asset_id, "tracking_info")
    asset_history = search.asset_hour_filters(asset_history, date)
    render json: { asset_id => asset_history}
    # render json: {asset_daily: asset_history.to_json}
  end

  def asset_most_recent_filter
    asset_id = params[:asset_id]
    search = AssetsVersionSearch.new
    asset_history = AssetsVersionSearch.get_all_history(asset_id, "tracking_info")
    asset_history = payload_tracking_info(asset_history)
    render json: asset_history.to_json
  end

  def payload_tracking_info asset_history
    history = []
    if asset_history.to_a.count > 0
      asset_history.to_a.each do |hist|
        history << hist.tracking_info["array"] if hist.tracking_info
      end
    end
    history
  end

  def search_groups_by_name
    @groups = current_user.company.groups

    # TODO (AssetReports): Search in ES, using AR now
    render json: @groups.to_json(only: [:id, :name])
  end

  def get_analytic_filter_labels
    @analytic_filter_labels = current_user.company.analytic_filter_labels
    if @analytic_filter_labels.present?
      render json: @analytic_filter_labels.to_json(only: [:id, :name])
    else
      render json: [].to_json
    end
  end

  def search_users_by_name
    users = current_user.company.users

    @users_id_name = users.map do |u|
      { id: u.id, name: "#{u.first_name} #{u.last_name}" }
    end

    render json: @users_id_name.to_json
  end

  def get_categories
    @category_groups = CategoryGroup.all

    render json: @category_groups.to_json(only: [:id, :name])
  end

  def get_subcategories_by_category_id
    category_group = CategoryGroup.find(params[:category_id])
    @categories = category_group.categories

    render json: @categories.to_json(only: [:id, :name])
  end

  def assets_filters
    page = params[:offset] ? params[:offset] : 1
    per_page = params[:limit] ? params[:limit] : 10
    asset_id = params[:asset_id]
    asset_filters = params[:filters][:asset_filters] ? params[:filters][:asset_filters] : {}
    company_id = params[:company_id] || current_user.company_id
    serach = AssetsVersionSearch.new
    versions = serach.asset_filter_versions(asset_filters, company_id, asset_id)
    assets = analytic_event_filters(params[:filters], versions)
    if(assets.present? and assets.aggregations.dig('group_by_asset').dig('buckets').present?)
      total_assets = assets.aggregations['group_by_asset']['buckets'].map do |buck|
        data = buck.dig('by_top_hit').dig('hits').dig('hits').map{|dd|
          begin
            dd['_source']['tracking_info']['array'] unless dd['_source']['tracking_info']['array'].nil?
          rescue
            []
          end
        }.flatten
        if data.present?
          data = data.compact.sort_by{|obj| obj["timestamp"]}.reverse.uniq{|x| x["unit"]}
          data  = detect_location_object data
        else 
          data = []
        end
        next if data.nil? or data == []
        {buck['key'] => {asset: buck.dig('by_top_hit').dig('hits').dig('hits').first.dig('_source').dig('asset'), events: data}}
      end
      total_assets = total_assets.compact
      paginated_assets = Kaminari.paginate_array(total_assets).page(page).per(per_page)
      render json: {assets: paginated_assets, count: total_assets.count}
    else
      render json: {assets: [], count: 0}
    end
  end


  def more_most_recent
    page = params[:offset] ? params[:offset] : 0
    per_page = params[:limit] ? params[:limit] : 10
    event_name = params[:event_name] ? params[:event_name].titleize : ""
    asset_id = params[:asset_id] ? params[:asset_id] : ""
    if !event_name.nil? and !asset_id.nil?
      company_id = params[:company_id] || current_user.company_id
      serach = AssetsVersionSearch.new
      versions = AssetsVersionSearch.get_all_history(asset_id, "tracking_info")
      assets = versions.aggregations(group_by_asset: {terms: {field: 'asset_id', size: 10000}, aggregations: {by_top_hit: { top_hits: { size: 10000}}}})
      if(assets.aggregations.dig("group_by_asset").dig("buckets").present?)
        total_assets = assets.aggregations["group_by_asset"]["buckets"].map{|buck| {buck['key'] => {asset: buck.dig('by_top_hit').dig('hits').dig('hits').first.dig("_source").dig("asset"), events: buck.dig('by_top_hit').dig('hits').dig('hits').map{|dd|
          begin
            dd["_source"]["tracking_info"]["array"]
          rescue
            []
          end
        }.flatten.sort_by {|obj| obj["timestamp"]}}}}
        event_hash = total_assets[0][asset_id][:"events"].select {|aa| aa["type"] == event_name}
        event_hash = event_hash.drop(page).first(per_page)
        render json: {assets_history: event_hash}
      else
        render json: {assets: [], count: 0}
      end
    else
        render json: {assets: [], count: 0}
    end
  end

  def analytic_event_filters(filters,versions)
    event_filters = filters["event_filters"]
    conditions = []

    if(event_filters.present?)
      event_filters.each do |key,value|
        if(key == "alarms")
          value.each do |v|
            conditions << build_query(v, nil, nil)
          end
        elsif(key == "notes_filter")
          case value
            when "All"
              conditions << build_query("notes", "All", nil)
            when "Text Contains"
              conditions << build_query("notes", "Text Contains", event_filters["notes_filter_by"].to_s)
          end
        elsif(key == "reallocations_filter")
          case value
            when "All"
              conditions << build_query("reallocations", "All", nil)
            when "Text Contains"
              conditions << build_query("reallocations", "Text Contains", event_filters["reallocations_filter_by"].to_s)
          end
        elsif(key == "gate_scans_filter")
          case value
            when "All"
              conditions << build_query("rfid gate", "All", nil)
            when "Text Contains"
              conditions << build_query("rfid gate", "Text Contains", event_filters["gate_scans_filter_by"].to_s)
          end
        elsif(key == "maintenance")
          case value
            when "All"
              conditions << build_query(key, "All", nil)
            when "Requested"
              conditions << build_query(key, value, nil)
            when "Completed"
              conditions << build_query(key, value, nil)
          end
        elsif(key == "readings" || key =="value")
          value.each do|i, j|
            case i
              when 'coolant_temperature', 'speed', 'fuel_level', 'fuel_consumption', 'engine_speed', 'odometer', 'odometer_total_miles', 'idle_hours', 'course', 'current_street_address', 'moving', 'seat_belt', 'market_value', 'price'
                j.values.each do|k|
                  filterValue = k.dig("filterValue").to_s
                  if(k.dig("filterOption"))
                    case k.dig("filterOption").dig("label")
                      when 'All', 'Degrees?'
                        conditions << build_query(i, "All", filterValue)
                      when 'Greater Than'
                        conditions << build_query(i, "gt", filterValue)
                      when 'Less Than'
                        conditions << build_query(i, "lt", filterValue)
                      when 'Equal To', 'Exactly Matches'
                        conditions << build_query(i, "eq", filterValue)
                      when 'Text Contains'
                        conditions << build_query(i, "Text Contains", filterValue)
                      when 'Moving'
                        conditions << build_query(i, "Moving", true)
                      when 'Not Moving'
                        conditions << build_query(i, "Not Moving", false)
                      when 'On'
                        conditions << build_query(i, "On", false)
                      when 'Off'
                        conditions << build_query(i, "Off", true)
                    end
                  end
                end
            end
          end
        end
      end
    end
    versions = versions.filter(conditions).limit(100000).aggregations(group_by_asset: {terms: {field: 'asset_id', size: 10000}, aggregations: {by_top_hit: { top_hits: { size: 10000}}}})
    versions
  end

  private

  def build_query(key, operator, value)
    return {} if key.blank? and operator.blank?
    case key
      when 'seat_belt'
        case operator
          when 'On'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Seat Belt Alarm"}}, {term: {value_boolean: value}}]}}}}
          when 'Off'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Seat Belt Alarm"}}, {term: {value_boolean: value}}]}}}}
        end
      when 'moving'
        case operator
          when 'Moving'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Motion"}},{term: {"tracking_info.array.value_boolean": value}}]}}}}
          when 'Not Moving'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Motion"}},{term: {"tracking_info.array.value_boolean": value}}]}}}}
        end
      when 'maintenance'
        case operator
          when 'Requested'
            return {bool: {must: [{term: {field_name_not_analyzed: 'maintenance requested'}}]}}
          when 'Completed'
            return {bool: {must: [{term: {field_name_not_analyzed: 'maintenance completed'}}]}}
        end
      when 'low_voltage', 'hard_acceleration', 'hard_braking', 'quick_lane_change', 'sharp_turn'
        return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: key.titleize}}], must_not: [{term: {"tracking_info.array.value_numeric"=> nil}}]}}}}
      when 'towing', 'fatigued_driving', 'ignition_on', 'ignition_off'
        return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: key.titleize}},{term: {"tracking_info.array.value_boolean": true}}]}}}}
      when 'power_on', 'power_off'
        return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Device #{key.titleize}"}},{term: {"tracking_info.array.value_boolean": true}}]}}}}
      when 'high_rpm'
        return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: 'High RPM'}}], must_not: [{term: {"tracking_info.array.value_numeric"=> nil}}]}}}}
      when 'idle_engine'
        return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: 'Idle Engine During Trip'}}], must_not: [{term: {"tracking_info.array.value_numeric"=> nil}}]}}}}
    end
    case operator
      when 'lt'
        case key
          when 'coolant_temperature'
            value = value.to_f/33.8
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Coolant Temperature"}},{range: {"tracking_info.array.value_numeric": {lt: value}}}]}}}}
          when 'odometer'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Trip Distance"}},{range: {"tracking_info.array.value_numeric": {lt: value.to_f}}}]}}}}
          when 'odometer_total'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Odometer"}},{range: {"tracking_info.array.value_numeric": {lt: value.to_f}}}]}}}}
          when 'speed','fuel_level', 'fuel_consumption', 'engine_speed', 'course'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: key.titleize}},{range: {"tracking_info.array.value_numeric": {lt: value.to_f}}}]}}}}
          when 'idle_hours'
            value = value.to_f * 60
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Idle Engine"}},{range: {"tracking_info.array.value_numeric": {lt: value.to_f}}}]}}}}
          when 'price', 'market_value'
            return {bool: {must: [{term: {field_name_not_analyzed: key}},{range: {to_i: {lt: value.to_i}}}]}}
        end
      when 'gt'
        case key
          when 'coolant_temperature'
            value = value.to_f/33.8
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Coolant Temperature"}},{range: {"tracking_info.array.value_numeric": {gt: value}}}]}}}}
          when 'odometer'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Trip Distance"}},{range: {"tracking_info.array.value_numeric": {gt: value.to_f}}}]}}}}
          when 'odometer_total'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Odometer"}},{range: {"tracking_info.array.value_numeric": {gt: value.to_f}}}]}}}}
          when 'speed','fuel_level', 'fuel_consumption', 'engine_speed', 'course'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: key.titleize}},{range: {"tracking_info.array.value_numeric": {gt: value.to_f}}}]}}}}
          when 'idle_hours'
            value = value.to_f * 60
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Idle Engine"}},{range: {"tracking_info.array.value_numeric": {gt: value.to_f}}}]}}}}
          when 'price', 'market_value'
            return {bool: {must: [{term: {field_name_not_analyzed: key}},{range: {to_i: {gt: value.to_i}}}]}}
        end
      when 'eq'
        case key
          when 'coolant_temperature'
            value = value.to_f/33.8
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Coolant Temperature"}},{term: {"tracking_info.array.value_numeric": value}}]}}}}
          when 'odometer'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Trip Distance"}},{term: {"tracking_info.array.value_numeric": value.to_f}}]}}}}
          when 'odometer_total_miles'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Odometer"}},{term: {"tracking_info.array.value_numeric": value.to_f}}]}}}}
          when 'speed', 'fuel_level', 'fuel_consumption', 'engine_speed'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: key.titleize}},{term: {"tracking_info.array.value_numeric": value.to_f}}]}}}}
          when 'idle_hours'
            value = value.to_f * 60
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Idle Engine"}},{term: {"tracking_info.array.value_numeric": value}}]}}}}
          when 'current_street_address'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Address"}},{term: {"tracking_info.array.value_string": value}}]}}}}
          when 'price', 'market_value'
            return {bool: {must: [{term: {field_name_not_analyzed: key}},{term: {to_i: value.to_i}}]}}
        end
      when 'All'
        case key
          when 'coolant_temperature', 'speed', 'fuel_level', 'fuel_consumption', 'engine_speed'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: key.titleize}}], must_not: [{term: {"tracking_info.array.value_numeric"=> nil}}]}}}}
          when 'odometer'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Trip Distance"}}], must_not: [{term: {"tracking_info.array.value_numeric"=> nil}}]}}}}
          when 'course'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Course"}}], must_not: [{term: {"tracking_info.array.value_numeric"=> nil}}]}}}}
          when 'odometer_total'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Odometer"}}], must_not: [{term: {"tracking_info.array.value_numeric"=> nil}}]}}}}
          when 'idle_hours'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Idle Engine"}}], must_not: [{term: {"tracking_info.array.value_numeric"=> nil}}]}}}}
          when 'current_street_address'
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Address"}}], must_not: [{term: {"tracking_info.array.value_string"=> nil}}]}}}}
          when 'price', 'market_value'
            return {bool: {must: [{term: {field_name_not_analyzed: key}},{exists: {field: "to_i"}}]}}
          when 'reallocations'
            return {bool: {must: [{term: {action: "reallocation"}},{exists: {field: "project_name"}}]}}
          when 'notes'
            return {bool: {must: [{term: {field_name_not_analyzed: key}},{exists: {field: "to_not_analyzed"}}]}}
          when 'rfid gate'
            return {bool: {must: [{term: {field_name_not_analyzed: 'RFID Gate'}},{exists: {field: "to_not_analyzed"}}]}}
          when 'maintenance'
            return {bool: {must: [{terms: {field_name_not_analyzed: ['maintenance requested', 'maintenance completed']}}]}}
        end
      when 'Text Contains'
        str_val = Regexp.quote(value.downcase)
        case key
          when 'current_street_address'
            str_val = Regexp.quote(value)
            return {nested: {path: "tracking_info.array", query: {bool: {must: [{term: {type: "Address"}},{regexp: {"tracking_info.array.value_string" => ".*#{str_val}.*"}}]}}}}
          when 'reallocations'
            return {bool: {must: [{term: {action: "reallocation"}}, {regexp: {"project_name" => ".*#{str_val}.*"}}]}}
          when 'notes'
            return {bool: {must: [{term: {field_name_not_analyzed: key}}, {regexp: {"to_not_analyzed" => ".*#{str_val}.*"}}]}}
          when 'rfid gate'
            return {bool: {must: [{term: {field_name_not_analyzed: 'RFID Gate'}}, {regexp: {"to_not_analyzed" => ".*#{str_val}.*"}}]}}
        end
    end
  end

  def get_group_filter(group)
    conditions = []
    group['rules'].each do |rule|
      condition = get_rule_filter(rule['key'], rule['oper'], rule['value'], rule['catType'])
      conditions << condition if !condition.blank?
    end
    {
      bool: {must: conditions.flatten}

    } if !conditions.blank?
  end

  def set_condition(condition)
    {
      bool: {should: condition}
    }
  end

  def detect_location_object data_array
    has_lat = data_array.detect {  |h| h["unit"] == "Current Lat" }
    if has_lat
      has_lng = data_array.detect {  |h| h["unit"] == "Current Lng" }
      has_acc = data_array.detect {  |h| h["unit"] == "Accuracy" }
      index = data_array.index {  |h| h["unit"] == "Current Lat" }
      has_acc = {"value_numeric": 0} if has_acc.nil?
      data_array = data_array.delete_if {  |h| h["unit"] == "Accuracy" || h["unit"] == "Geo Coordinates" || h["unit"] == "Current Lat" || h["unit"] == "Current Lng" }
      value =  "lat: " + has_lat["value_numeric"].to_s + " ,lng: " + has_lng["value_numeric"].to_s + ", acc: " + has_acc["value_numeric"].to_s
      data_array.insert(index, {'unit': "Location", 'description': "Asset Location change", 'value_string': value, 'type': "Lat lng change", 'timestamp': has_lat["timestamp"]})
      data_array.compact
    else
      data_array
    end
  end

  def get_groups_filter(groups)
    conditions = []
    cat_conditions = []
    tag_conditions = []
    live_conditions = []
    attr_conditions = []

    groups.each do |group|
      condition = get_group_filter(group)
      if !condition.blank?
        if group['groupType'] == "categorybtn"
          cat_conditions << condition
        elsif group['groupType'] == "tagbtn"
          tag_conditions << condition
        elsif group['groupType'] == "attrbtn"
          attr_conditions << condition
        elsif group['groupType'] == "livebtn"
          live_conditions << condition
        end
        conditions << condition
      end
    end
    cat_conditions = set_condition(cat_conditions)
    tag_conditions = set_condition(tag_conditions)
    attr_conditions = set_condition(attr_conditions)
    live_conditions = set_condition(live_conditions)

    return cat_conditions, tag_conditions, live_conditions, attr_conditions
  end
end
