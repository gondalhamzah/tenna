class NewsItemsController < ApplicationController
  layout 'homepage'

  def index
    @news_items = NewsItem.published.order(priority: :desc).order(post_date: :desc).page(params[:page]).per(10)
  end

  def show
    @news_item = NewsItem.find(params[:id])

    if params[:flash_message].present?
      flash[:alert] = params[:flash_message] if params[:flash_message].present?
      redirect_to @news_item
    end
  end
end
