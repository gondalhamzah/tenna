require 'json'
class GaugeAlertsController < ApplicationController
    before_filter :set_filter_params, :only => [:assets_by_category]
    before_filter :check_conflict_status, :only => [:create, :reset]
    include GaugeGraphData

    # returns assets filtered by category_id and query
    # sample request url
    # /gauge_alerts/search_assets_by_category?category_id=<int>&category_group_id=<int>&query=<string>
    def search_assets_by_category
        if current_user.private?
            search_options = {company: current_user.company}
        else
            search_options = {creator: current_user}
        end
        dup_params = params.dup
        dup_params[:filter_search] = dup_params
        filterable_assets = FilterableAssets.new(
            user: current_user,
            search_options: search_options,
            params: dup_params,
            zip_code: '',
            live_data: true
        )

        assets = filterable_assets.api_search_assets_by_category

        respond_to do |format|
            format.json { render :json => assets.map{|asset| asset.simple_json} }
        end
    end

    # returns assets which have gauge data by category_id
    # sample request url
    # /gauge_alerts/category_assets?category_id=<int>
    def category_assets
        category_id = params[:category_id]
        company_id = current_user.company.id
        asset_ids = GaugeAlertGroup.where(company_id: company_id, category_id: category_id).pluck('asset_id').compact

        respond_to do |format|
            format.json { render :json => Asset.where(id: asset_ids).map{|asset| asset.simple_json} }
        end
    end

=begin
    batch create or update gauge conditions
    URL: POST /gauge_alerts
    Query Params: category_group_id, category_id, asset_id
    POST Body:
    {
        "gauges": [<gauge ids>],
        "data": {
            <gague_id>: {
                "alerts": [
                    {id: <gauge_condition_id: optional>, values: json, severe_level: string, notes: string, contacts: json, active: true/false}
                    ...
                ],
                "services": [
                    {id: <gauge_condition_id: optional>, values: json, severe_level: string, notes: string, contacts: json, active: true/false}
                    ...
                ],
                "geofences": { # this will valid when gauge_id is 12 (GeoFence)
                    active: true/false,
                    values: [
                        {geofence_id: <int>, alert_enter_site: true/false, alert_exit_site: true/false},
                        ...
                    ]
                }
            },
            ....
        },
        "deleted_conditions": [<gauge condition ids>],
        "deleted_gauges":[<gauge alert ids],
        "deleted_assets": [<asset_ids],
        "version": [version_number]
    }
=end
    def create
        gauges = params[:gauges] || []
        data = params[:data] || {}
        deleted_conditions = params[:deleted_conditions] || []
        deleted_gauges = params[:deleted_gauges] || []
        deleted_assets = params[:deleted_assets] || []

        # get or create gauge group
        gauges.each do |gauge_id|
            gauge_group = config_gauge_group(gauge_id)
            gauge_params = data[gauge_id.to_s] || {}
            alerts_data = gauge_params["alerts"] || []
            services_data = gauge_params["services"] || []
            set_conditions(gauge_group, alerts_data, "alert")
            set_conditions(gauge_group, services_data, "service")
            if gauge_id == 12 && !gauge_params["geofences"].blank?
                set_conditions(gauge_group, gauge_params["geofences"], "geofence")
            end
        end

        # delete gauge conditions
        if !deleted_conditions.blank?
            cond_ids = GaugeCondition.where(id: deleted_conditions).pluck(:id).compact
            GaugeAlertLog.where(gauge_condition_id: cond_ids).delete_all
            GaugeCondition.where(id: cond_ids).delete_all
            GaugeAlertGroup.where(company_id: @company_id).where('id NOT IN (SELECT DISTINCT(gauge_alert_group_id) FROM gauge_conditions)').delete_all
        end

        # delete gauges
        if !deleted_gauges.blank?
            group_ids = GaugeAlertGroup.where(company_id: @company_id, category_group_id: @category_group_id, category_id: @category_id, asset_id: @asset_id, gauge_id: deleted_gauges).pluck(:id).compact
            cond_ids = GaugeCondition.where(gauge_alert_group_id: group_ids).pluck(:id).compact
            GaugeAlertLog.where(gauge_condition_id: cond_ids).delete_all
            GaugeCondition.where(id: cond_ids).delete_all
            GaugeAlertGroup.where(id: group_ids).delete_all
        end

        # deleted assets
        if !deleted_assets.blank?
            GaugeAlertGroup.where(asset_id: deleted_assets).delete_all
        end

        $redis.set(redis_key, $redis.get(redis_key).to_i + 1)

        respond_to do |format|
            format.json { render :json => res_data }
        end
    end

=begin
    URL: POST /gauge_alerts/reset
    Query Params: category_group_id, category_id, asset_id
    POST Body:
    {
      'level': 'sub_category/category'
      'version': number
    }
=end
    def reset
      group_ids = GaugeAlertGroup.where(company_id: @company_id, category_group_id: @category_group_id, category_id: @category_id, asset_id: @asset_id).pluck(:id).compact
      cond_ids = GaugeCondition.where(gauge_alert_group_id: group_ids).pluck(:id).compact
      GaugeAlertLog.where(gauge_condition_id: cond_ids).delete_all
      GaugeCondition.where(id: cond_ids).delete_all
      GaugeAlertGroup.where(id: group_ids).delete_all

      category_id = @category_id
      if params[:level] == 'category'
        category_id = nil
      else
        # if sub category doesn't have any alert configs, we need to reset to category alerts.
        if GaugeAlertGroup.where(company_id: @company_id, category_group_id: @category_group_id, category_id: category_id).count() == 0
          category_id = nil
        end
      end

      GaugeAlertGroup.where(company_id: @company_id, category_group_id: @category_group_id, category_id: category_id).each do |g|
        new_group = config_gauge_group(g.gauge_id)
        g.gauge_conditions.each do |cond|
          new_cond = cond.dup
          new_cond.gauge_alert_group_id = new_group.id
          new_cond.save
        end
      end

      res = res_data

      $redis.set(redis_key, $redis.get(redis_key).to_i + 1)

      respond_to do |format|
        format.json { render :json => res }
      end
    end

=begin
    URL: GET /gauge_alerts
    Query Params: category_group_id, category_id, asset_id
    Response Body:
     {
        "gauges": [<gauge ids>],
        "data": {
            <gague_id>: {
                "alerts": [
                    {id: <gauge_condition_id: optional>, values: json, severe_level: string, notes: string, contacts: json, active: true/false}
                    ...
                ],
                "services" [
                    {id: <gauge_condition_id: optional>, values: json, severe_level: string, notes: string, contacts: json, active: true/false}
                    ...
                ],
                "geofences": { # this will valid when gauge_id is 12 (GeoFence)
                    active: true/false,
                    values: [
                        {geofence_id: <int>, alert_enter_site: true/false, alert_exit_site: true/false},
                        ...
                    ]
                }
            },
            ....
        },
        "initials": {
            // if there are no gauges, it'll have parent data
        },
        "version": [version_number]
    }
=end

    def index
        @category_group_id = params[:category_group_id]
        @category_id = params[:category_id]
        @asset_id = params[:asset_id]
        @company_id = current_user.company.id

        res = res_data
        initials = {}
        if res[:gauges].blank?
            if @asset_id
                @asset_id = nil
                initials = res_data
                if initials[:gauges].blank?
                    @category_id = nil
                    initials = res_data
                end
            else
                @category_id = nil
                initials = res_data
            end
        end
        res[:initials] = initials

        respond_to do |format|
            format.json { render :json => res }
        end
    end

=begin
    URL: GET /gauge_alerts/asset_detail
    Query Params: asset_id
    Response Body:
     {
        "gauges": [<gauge>],
        "data": <asset>
    }
=end

    def asset_detail
        @asset_id = params[:asset_id]
        @company_id = current_user.company.id
        res = asset_data

        respond_to do |format|
            format.json { render :json => res }
        end
    end

    private
    def check_conflict_status
      @category_group_id = params[:category_group_id]
      @category_id = params[:category_id]
      @asset_id = params[:asset_id]
      @company_id = current_user.company.id

      if (params[:version] || '0').to_i != $redis.get(redis_key).to_i
        respond_to do |format|
          format.json { render :json => res_data, :status => 409 }
        end
      end
    end

    def set_filter_params
        params[:filter_search] ||= {}
        params[:filter_search][:category_id] = (params[:category_id] || '')
        params[:filter_search][:category_group_id] = (params[:category_group_id] || '')
        params[:filter_search][:query] = (params[:query] || '')
    end

    def config_gauge_group(gauge_id)
        GaugeAlertGroup.find_or_create_by(gauge_id: gauge_id, company_id: @company_id, category_group_id: @category_group_id, category_id: @category_id, asset_id: @asset_id)
    end

    def set_conditions(gauge_group, conditions, cond_type)
        if cond_type != "geofence"
            conditions.each do |cond|
                if cond["id"]
                    gauge_condition = GaugeCondition.find(cond["id"])
                else
                    gauge_condition = GaugeCondition.new()
                end

                gauge_condition.gauge_alert_group_id = gauge_group.id
                gauge_condition.cond_type = cond_type
                gauge_condition.active = cond["active"]
                gauge_condition.notes = cond["notes"]
                gauge_condition.values = cond["values"] || {}
                gauge_condition.severe_level = cond["severe_level"] || "L"
                gauge_condition.contacts = cond["contacts"] || {}
                gauge_condition.save
            end
        else
            GaugeCondition.where(cond_type: "geofence", gauge_alert_group: gauge_group).delete_all
            gauge_condition = GaugeCondition.create(gauge_alert_group: gauge_group, cond_type: "geofence", values: conditions["values"], active: conditions["active"])
        end
    end

    def redis_key
      _key = "gauge_alert_#{@company_id}-#{@category_group_id}-#{@category_id}-#{@asset_id}"
      $redis.set(_key, 0) if $redis.get(_key).nil?
      _key
    end

    def res_data
        res = {}

        groups = GaugeAlertGroup.where(company_id: @company_id, category_group_id: @category_group_id, category_id: @category_id, asset_id: @asset_id)
        gauge_ids = []
        data = {}
        groups.each do |group|
            gauge_ids << group.gauge_id
            group_data = {alerts: [], services: []}
            if group.gauge_id != 12
                GaugeCondition.where(gauge_alert_group: group).order('id asc').each do |cond|
                    if cond.cond_type == "alert"
                        group_data[:alerts] << cond
                    elsif cond.cond_type == "service"
                        group_data[:services] << cond
                    end
                end
            else
                cond = GaugeCondition.where(gauge_alert_group: group, cond_type: "geofence").first
                group_data[:geofences] = cond
            end
            data[group.gauge_id] = group_data
        end

        res[:gauges] = gauge_ids
        res[:data] = data
        res[:version] = $redis.get(redis_key).to_i

        res
    end
end
