class UsersController < ApplicationController
  before_action :authenticate_user!

  def search_by_name
    @company_users = current_user.company.users.not_deleted

    if params[:name].present?
      @company_users = @company_users.where("lower(first_name) LIKE ? or lower(last_name) LIKE ?", "%#{params[:name].downcase}%", "%#{params[:name].downcase}%")
    end

    render json: @company_users.select(:id, :first_name, :last_name, :email)
  end
end
