class DashboardController < ApplicationController
  include BuildMapMarkers
  include ChartHelper

  before_action :authenticate_user!

  def index
    search = AssetSearch.new(filter_search: {}, user: current_user, creator: nil, favoriter: nil, project: nil, company: current_user.company)

    if current_user.private?
      @my_assets_expiring_count = search.abandoned_count
    else
      @my_assets_expiring_count = Asset.accessible_by(current_ability, :update).expiring_within(Asset::EXPIRE_THRESHOLD).count
    end

    @my_assets_sold_count = Asset.my_assets_sold(current_user).count
    my_assets = Asset.my_assets(current_user).where.not(state: ['deleted', 'expired', 'sold' ])
    @my_assets_count = my_assets.count()

    if current_user.private?
      qr_search = AssetSearch.new(filter_search: nil, user: current_user, creator: nil, favoriter: nil, project: nil, company: current_user.company)
      @public_assets_count = Asset.not_my_assets(current_user).where(:public => true).count
      @public_assets_value = Asset.not_my_assets(current_user).where(:public => true).sum(:price)
      @maintenance_required_count = my_assets.where(maintenance: true).count
      @my_assets_value = my_assets.sum(:market_value)
      @my_asset_reallocation_count = Asset.my_asset_reallocation_count(current_user)
      @assets_with_qr_code = qr_search.assets_with_qr_code
      @my_wanted_count = WantedAsset.my_wanted_assets(current_user).count
      @project_updates = current_user.company.projects.not_deleted.order(updated_at: :desc).limit(10)
      @recently_added_resources = Asset.accessible_by(current_ability)
                                    .active
                                    .includes(:category, :company, :marks, :photos, :zip_code)
                                    .order(updated_at: :desc)
                                    .where(company: current_user.company)
                                    .limit(10)
      public_value = my_assets.where(:public => true).sum(:market_value)
      private_owned_value = my_assets.where(:public => false, :owned => true, :rental => false).sum(:market_value)
      private_owned_value_count = my_assets.where(:public => false, :owned => true, :rental => false).count
      private_rental_value = my_assets.where(:public => false, :owned => false, :rental => true).sum(:market_value)
      private_rental_value_count = my_assets.where(:public => false, :owned => false, :rental => true).count
      private_rental_value_daily = my_assets.where(:public => false, :owned => false, :rental => true, :units => 'day').sum(:market_value)
      private_rental_value_weekly = my_assets.where(:public => false, :owned => false, :rental => true, :units => 'week').sum(:market_value)
      private_rental_value_monthly = my_assets.where(:public => false, :owned => false, :rental => true, :units => 'month').sum(:market_value)
      private_for_rent_value = my_assets.where(:public => true, :owned => true, :rental => true).sum(:market_value)
      private_for_rent_value_count = my_assets.where(:public => true, :owned => true, :rental => true).count
      private_for_sale_value = my_assets.where(:public => true, :owned => true, :rental => false).sum(:market_value)
      private_for_sale_value_count = my_assets.where(:public => true, :owned => true, :rental => false).count
      @chart1 = pie_chart cats: ['Rentals', 'Owned'],
                          name: 'Asset',
                          values: [search.rental_count, search.owned_count],
                          show_legend: true

      @chart2 = column_chart cats: ['Equipment', 'Materials'],
                             names: ['Owned', 'Rentals'],
                             values: [ [
                               search.type_count(:owned=>true, :equipment=>true),
                               search.type_count(:owned=>true, :material=>true)
                             ], [
                               search.type_count(:rental=>true, :equipment=>true),
                               search.type_count(:rental=>true, :material=>true)
                             ] ],
                             colors: ["#91E300", "#007FBF"],
                             show_legend: true

      @chart3 = pie_chart cats: ['Sold', 'In Use', 'Abandoned', 'Available'],
                          name: 'Asset',
                          values: [search.sold_count, search.in_use_count, search.abandoned_count, search.performing_count ],
                          colors: ["#DCD7CB", "#717167", "#000000", "#02BAD8"]

      @chart4 = column_chart cats: ['Owned', 'Rentals','Marketplace' ],
                             series: [{
                                 name: 'Owned',
                                 data: [private_owned_value, nil,nil],
                                 tooltip: {
                                  useHTML: true,
                                  headerFormat: "<div><span><b>{point.x}</b></span><br/>",
                                  pointFormat:   "<span>Total Assets: #{private_owned_value_count}</span>",
                                  footerFormat: "</div>"
                                  },
                               }, {
                                 name: 'Rentals',
                                 data: [nil, private_rental_value,nil],
                                 tooltip: {
                                  useHTML: true,
                                  headerFormat: "<div><span><b>Rental Costs</b></span><br/>",
                                  pointFormat:   "<span>Daily:  $#{private_rental_value_daily.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse}</span><br/>
                                                  <span>Weekly: $#{private_rental_value_weekly.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse}</span><br/>
                                                  <span>Monthly: $#{private_rental_value_monthly.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse}</span><br/>
                                                  <span>Total Assets: #{private_rental_value_count}</span>",
                                  footerFormat: "</div>"
                                  },
                              },{
                                 name: 'For Rent',
                                 data: [nil,nil,private_for_rent_value],
                                 tooltip: {
                                  useHTML: true,
                                  headerFormat: "<div><span><b>For Rent</b></span><br/>",
                                  pointFormat:   "<span>Total: ${point.y}</span><br/><span>Total Assets: #{private_for_rent_value_count}</span>",
                                  footerFormat: "</div>"
                                 },
                              },{
                                 name: 'For Sale',
                                 data: [nil,nil,private_for_sale_value],
                                 tooltip: {
                                  useHTML: true,
                                  headerFormat: "<div><span><b>For Sale</b></span><br/>",
                                  pointFormat:   "<span>Total: ${point.y}</span><br/><span>Total Assets: #{private_for_sale_value_count}</span>",
                                  footerFormat: "</div>"
                                 },
                              }],
                             colors: ['#fd5a00', '#f7bd3d', '#3aa33a','#b1d200'],
                             show_legend: true,
                             dollar: true,
                             stacked: true,
                             show_data_labels: false
    else
      @my_asset_views_count = Asset.my_asset_views_count(current_user)
      @my_assets_value = my_assets.sum(:price)
      if cookies[:zip_code].present?
        filterSearch = FilterSearch.new
        filterSearch.distance_miles = 50
        filterSearch.distance_zip = cookies[:zip_code]
        @asset_nearby_count = AssetSearch.with_50_miles_search(filterSearch, current_user).count
      else
        @asset_nearby_count = '-'
      end

      @recently_added_materials = Material.accessible_by(current_ability)
                                    .active
                                    .includes(:category, :company, :marks, :photos, :zip_code)
                                    .order(updated_at: :desc)
                                    .limit(10)

      @recently_added_equipment = Equipment.accessible_by(current_ability)
                                    .active
                                    .includes(:category, :company, :marks, :photos, :zip_code)
                                    .order(updated_at: :desc)
                                    .limit(10)


    end
    @notifications = Notification.dashboard_notifications(current_user)
  end
end
