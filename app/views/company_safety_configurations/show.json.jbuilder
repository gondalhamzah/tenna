json.extract! @company_safety_configuration, :id, :gauge_id, :company_id, :score_type, :score_weight, :high_points, :medium_points, :low_points, :created_at, :updated_at
