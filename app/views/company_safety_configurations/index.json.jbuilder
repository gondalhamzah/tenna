json.array!(@company_safety_configurations) do |company_safety_configuration|
  json.extract! company_safety_configuration, :id, :gauge_id, :company_id, :score_type, :score_weight, :high_points, :medium_points, :low_points
  json.url company_safety_configuration_url(company_safety_configuration, format: :json)
end
