json.ignore_nil!

json.array!(@assets) do |asset|
  json.partial! 'maps/asset', asset: asset
end
