json.extract! asset, :id, :title, :price

json.image asset.photos.any? ? asset.photos[0].file.thumb.url : ActionController::Base.helpers.asset_path('/img-no-image-list-view.jpg')

