class PdfUploader < AttachmentUploader

  def extension_white_list
    %w(jpg jpeg png pdf)
  end

end
