class PhotoUploader < AttachmentUploader
  include CarrierWave::MiniMagick

  process :efficient_conversion => [1024, 1024]

  version :thumb do
    process resize_to_fill: [128, 128]
  end

  def extension_white_list
    %w(jpg jpeg png)
  end

  def filename
    super.chomp(File.extname(super)) + '.jpg' if super
  end

  private

  def efficient_conversion(width, height)
    manipulate! do |img|
      # Fixes issue where iOS photos can appear sideways, as they specify rotation information in EXIF data
      img.auto_orient
      img.format('jpg') do |c|
        c.fuzz '3%'
        c.trim
        c.resize "#{width}x#{height}>"
        c.resize "#{width}x#{height}<"
      end
      img
    end
  end

end
