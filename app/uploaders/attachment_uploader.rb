class AttachmentUploader < CarrierWave::Uploader::Base

  def store_dir
    "uploads/#{model.class.to_s.pluralize.underscore}/#{model.id}"
  end

end
