class CompanyLogoUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  version :thumb do
    process resize_to_fit: [128, 26]
  end

  version :medium do
    process resize_to_fit: [200, 200]
  end

  def extension_white_list
    %w(jpg jpeg png)
  end

end
