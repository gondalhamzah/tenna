class GroupsVersionsIndex < Chewy::Index

  define_type GroupsVersion do
    field :id, type: :integer
    field :field_name
    field :field_name_not_analyzed, index: 'not_analyzed', value: -> {field_name.downcase}
    field :from
    field :to
    field :user_id, type: :integer
    field :whodunnit_name, index: 'not_analyzed', value: -> {User.find_by_id(user_id).try(:name)}
    field :group_id
    field :company_id, type: :integer
    field :action
    field :created_at, type: :date
    field :updated_at, type: :date
  end

end
