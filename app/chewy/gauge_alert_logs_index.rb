class GaugeAlertLogsIndex < Chewy::Index
  define_type GaugeAlertLog do
    field :id, type: :integer

    field :asset_id, type: :integer
    field :asset_name, value: -> (gal) {"#{gal.try(:asset).try(:title)}"}

    field :gauge_id, value: -> (gal) {"#{gal.try(:gauge_condition).try(:gauge_alert_group).try(:gauge).try(:id)}"}
    field :gauge_name, value: -> (gal) {"#{gal.try(:gauge_condition).try(:gauge_alert_group).try(:gauge).try(:name).try(:parameterize).try(:underscore)}"}

    field :gauge_condition_id, type: :integer
    field :gauge_condition_severe_level, value: -> (gal) {"#{gal.try(:gauge_condition).try(:config_field_name)}"}

    field :created_at, type: :date
    field :updated_at, type: :date

    field :operator_id, type: :integer
    field :operator_name, value: -> (gal) {"#{gal.try(:operator).try(:name) }"}

    field :company_id, value: -> (gal) {"#{gal.try(:operator).try(:company_id)}"}
    field :values, type: :object
  end
end
