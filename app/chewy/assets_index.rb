class AssetsIndex < Chewy::Index

  # not_analyzed
  # Index this field, so it is searchable, but index the value exactly as specified. Do not analyze it.
  define_type Asset do
    field :id, type: :integer
    field :equipment, value: -> {type == 'Equipment'}, type: :boolean
    field :material, value: -> {type == 'Material'}, type: :boolean
    field :expires_on, type: :date
    field :title
    field :title_not_analyzed, index: 'not_analyzed', value: -> { title }
    field :description
    field :asset_num
    field :make
    field :model
    field :rental
    field :category, value: -> {category.try(:name)} # for full-text search
    field :category_exact, value: -> {category.try(:name)}, index: 'not_analyzed' # for filtered search
    field :category_id, value: -> {category.try(:id)}
    field :category_group_id, value: -> { category.try(:category_group).try(:id) }
    field :has_tracking_code, value: -> {has_tracking_code?}, type: :boolean
    field :maintenance, type: :boolean
    field :year, type: :integer
    field :price, type: :integer, value: -> {price.to_i} # force nil to 0
    field :internal_daily_rental_rate, type: :integer, value: -> {internal_daily_rental_rate.to_i}
    field :market_value, type: :integer, value: -> {market_value.to_i} # force nil to 0
    field :public, type: :boolean
    field :owned, type: :boolean
    field :hours, type: :integer
    field :hours_since, type: :integer
    field :use_j1939_engine_hours, type: :boolean
    field :miles, type: :integer
    field :miles_since, type: :integer
    field :use_j1939_miles, type: :boolean
    field :maintenance_date, type: :date
    field :company_id, type: :integer, properties: {null_value: 'NULL'}
    field :creator_id, type: :integer
    field :project_id, type: :integer
    field :group_id, type: :integer
    # use nested objects
    # see https://www.elastic.co/guide/en/elasticsearch/guide/current/nested-objects.html
    field :project_assignments, type: 'nested' do
      field :user_id, type: :integer
      field :role, index: 'not_analyzed'
    end
    # note: might want to switch this to use nested object like above
    # this way works fine for now, but once we add another
    # mark or marker_type, the results will be mixed
    field :marks do
      field :marker_id, type: :integer
      field :marker_type, index: 'not_analyzed'
      field :mark, index: 'not_analyzed'
      field :created_at, type: :date
    end
    field :status, index: 'not_analyzed', value: -> {state}
    field :condition, index: 'not_analyzed'
    field :created_at, type: :date
    field :updated_at, type: :date
    field :in_use_until, type: :date, value: -> { in_use_until.nil? ? '2016-01-01' : in_use_until }
    field :photos_count, value: -> {photos.size}, type: :integer
    field :available_on, type: :date
    field :coordinates, type: 'geo_point', value: ->{ lat.present? && lng.present? ? {lat: lat, lon: lng} : nil }
    field :has_location, type: :boolean, value: -> { current_lat.present? && current_lng.present? }
    field :location_zip
    field :country, index: 'not_analyzed', value: -> {zip_code.country.downcase if zip_code and zip_code.country}
    field :has_trackers, value: -> {has_trackers?}, type: :boolean
    field :has_tags, value: -> {has_tags?}, type: :boolean
    field :qr, type: :integer
    field :rfid, type: :integer
    field :lora, type: :integer
    field :sigfox, type: :integer
    field :upc, type: :integer
    field :current_driver_id, type: :integer
    field :serialnumber, type: :integer
    field :bt, type: :integer
    field :cellular, type: :integer
    field :certified, type: :boolean
    # these fields (tag_list, asset_tag_list) no longer used so dont uncomment them
    # field :tag_list
    # field :asset_tags_list, index: 'not_analyzed', value: -> {tracking_codes.collect{|c| c.token_type}}
    field :same_location, value: -> {project_qr_code_same_location?} # possible values true, false and nil
    field :asset_kind, index: 'not_analyzed', value: -> {asset_kind}
    field :last_location_changed, type: :date
  end
end
