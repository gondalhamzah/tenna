class WantedAssetsIndex < Chewy::Index
  define_type WantedAsset do
    field :id, type: :integer
    field :title
    field :description
    field :make
    field :model
    field :category, value: -> { category.try(:name) } # for full-text search
    field :category_exact, value: -> { category.try(:name) }, index: 'not_analyzed' # for filtered search
    field :hours_miles, type: :integer
    field :project_id, type: :integer
    field :project_assignments, type: 'nested' do
      field :user_id, type: :integer
      field :role, index: 'not_analyzed'
    end
    field :company_id, type: :integer
    field :creator_id, type: :integer
    field :quantity, type: :integer
    field :needed_on, type: :date
    field :price, type: :integer
    field :status, index: 'not_analyzed', value: -> {state}
    field :priority, index: 'not_analyzed', value: -> {priority}
    field :created_at, type: :date
    field :updated_at, type: :date
    field :fulfilled_date, type: :date
  end
end
