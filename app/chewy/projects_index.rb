class ProjectsIndex < Chewy::Index

  # not_analyzed
  # Index this field, so it is searchable, but index the value exactly as specified. Do not analyze it.

  define_type Project do
    field :id
    field :name
    field :name_not_analyzed, index: 'not_analyzed', value: -> {name.downcase}
    field :created_at, type: :date
    field :closeout_date, type: :date
    field :company_id, type: :integer
    field :contact_id, type: :integer
    field :created_at, type: :date
    field :updated_at, type: :date
    field :closeout_date, type: :date
    field :coordinates, type: 'geo_point', value: ->{ lat.present? && lng.present? ? {lat: lat, lon: lng} : nil }
    field :location_zip
    field :deleted_at, type: :date
    field :project_num
    field :country , index: 'not_analyzed', value: -> {address.country.downcase if address and address.country}, type: :string
  end
end
