class GroupsIndex < Chewy::Index

  # not_analyzed
  # Index this field, so it is searchable, but index the value exactly as specified. Do not analyze it.

  define_type Group do
    field :name
    field :name_not_analyzed, index: 'not_analyzed', value: -> {name.downcase}
    field :project_id, type: :integer
    field :created_at, type: :date
    field :company_id, type: :integer
    field :updated_at, type: :date
    field :deleted_at, type: :date
    field :group_num
    field :client
    field :state
  end
end
