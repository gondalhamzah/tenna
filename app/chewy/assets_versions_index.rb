class AssetsVersionsIndex < Chewy::Index
  define_type AssetsVersion do
    field :id, type: :integer
    field :field_name
    field :field_name_not_analyzed, index: 'not_analyzed', value: -> {field_name.downcase}
    field :from
    field :to
    field :to_not_analyzed, index: 'not_analyzed', value: -> {to.try(:downcase)}
    field :to_i, type: :integer, value: -> {to.to_i if not(field_name.downcase == 'notes' or ['qr', 'rfid', 'lora', 'sigfox', 'upc', 'bt', 'cellular', 'serialnumber'].include?(field_name.downcase))}
    field :whodunnit, type: :integer
    field :whodunnit_name, index: 'not_analyzed', value: -> {User.find_by_id(whodunnit.to_i).try(:name)}
    field :user_name, index: 'not_analyzed', value: -> {User.find_by_id(whodunnit.to_i).try(:name).try(:downcase)}
    field :asset_id
    field :wanted_id
    field :project_id, type: :integer
    field :project_name, index: 'not_analyzed', value: -> {Project.find_by_id(project_id).try(:name).try(:downcase)}
    field :group_id, type: :integer
    field :group_name, index: 'not_analyzed', value: -> {Group.find_by_id(group_id).try(:name).try(:downcase)}
    field :company_id, type: :integer
    field :category_name, index: 'not_analyzed', value: -> {Asset.find_by_id(asset_id).try(:category).try(:name).try(:parameterize)}
    field :wanted_category_name, index: 'not_analyzed', value: -> {WantedAsset.find_by_id(wanted_id).try(:category).try(:name).try(:parameterize) if wanted_id.present?}
    field :category_group, index: 'not_analyzed', value: -> {Asset.find_by_id(asset_id).try(:category).try(:category_group).try(:name).try(:parameterize)}
    field :wanted_category_group, index: 'not_analyzed', value: -> {WantedAsset.find_by_id(wanted_id).try(:category).try(:category_group).try(:name).try(:parameterize) if wanted_id.present?}
    field :action
    field :created_at, type: :date
    field :updated_at, type: :date
    field :asset_title, value: -> {Asset.find_by_id(asset_id).try(:title).try(:downcase)}
    field :wanted_asset_title, value: -> {WantedAsset.find_by_id(wanted_id).try(:title).try(:downcase) if wanted_id.present?}
    field :asset, type: :object do
      field :id
      field :title
      field :creator_id
      field :project_id
      field :category_id
      field :project_name, value: -> {Project.find_by_id(project_id).try(:name).try(:downcase)}
      field :group_id
      field :group_name, value: -> {Group.find_by_id(group_id).try(:name).try(:downcase)}
      field :current_driver_id
      field :description
      field :price
      field :status, index: 'not_analyzed', value: -> {Asset.find(id).state}
      field :market_value, type: :integer, value: -> {Asset.find(id).market_value.to_i} # force nil to 0
      field :qr, type: :integer
      field :rfid, type: :integer
      field :lora, type: :integer
      field :sigfox, type: :integer
      field :upc, type: :integer
      field :current_driver_id, type: :integer
      field :bt, type: :integer
      field :cellular, type: :integer
      field :asset_kind, value: -> {Asset.find(id).asset_kind}
      field :image_url, value: -> {Asset.find(id).photo_url}
      field :in_use_until, type: :date, value: -> { Asset.find(id).in_use_until.nil? ? '2016-01-01' : in_use_until }
      field :location, value: -> {Asset.find(id).location}
    end
    # field :tracking_info, value: -> {tracking_info.to_json if tracking_info}

    # https://www.elastic.co/guide/en/elasticsearch/reference/1.7/query-dsl-nested-query.html
    field :tracking_info, type: :object do
      field :array, type: :nested
    end
    # field :tracking_info, value: -> {tracking_info.to_json if tracking_info}
  end
end
