class MaintMailer < ApplicationMailer
  def maint_event_notification(event, asset, user)
    @event = event
    @asset = asset
    @user = user
    mail to: user.email, subject: 'Tenna Asset Maintenance Notification'
  end
end
