class UnauthorizedNotification < ApplicationMailer

  def notification(firstname, lastname, user_email, user_company, role, asset_url, url_of_scan, lat, lng)
    @first_name = firstname
    @last_name = lastname
    @user_email = user_email
    @user_company = user_company
    @role = role
    @asset_url = asset_url
    @url_of_scan = url_of_scan
    @lat = lat
    @lng = lng
    @date = Time.now.strftime('%d/%m/%Y')
    @time = Time.now.strftime('%I:%M:%S %p')

    mail to: 'support@tenna.com', subject: 'ALERT: Unauthorized Scan'
  end
end
