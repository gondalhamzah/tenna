class Notifications < ApplicationMailer
  include Roadie::Rails::Mailer

  def expiring_assets(user, assets)
    @user = user
    @assets = assets

    roadie_mail to: user.email, subject: "Expiring assets"
  end
  def expiring_wanted_assets(user, wanted_assets)
    @user = user
    @wanted_assets = wanted_assets

    roadie_mail to: user.email, subject: "Expiring wanted assets"
  end
end
