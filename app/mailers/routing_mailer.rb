class RoutingMailer < ApplicationMailer
  def path_visited_notification(route, project, asset, driver_stop, path)
    @path = path
    @route = route
    @asset = asset
    @project = project
    @driver_stop = driver_stop
    mail to: project.contact_email, subject: 'Tenna Route Notification'
  end

  def route_generated_notification(driver, email, route, fired_date)
    @route = route
    @fired_date = fired_date
    @driver = driver
    @creator_name = @route.creator.name
    mail to: email, subject: 'Tenna New Route Generated'
  end
end
