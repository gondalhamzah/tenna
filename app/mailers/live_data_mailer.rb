class LiveDataMailer < ApplicationMailer
  def notification(email, notification, asset, notes, subject="Live Data Notification")
    @asset = asset
    @notification = notification
    @notes = notes
    mail to: email, subject: subject
  end

  def alert(name, email, message, asset, cond, date_time)
    @name = name
    @asset = asset
    @message = message
    @cond = cond
    @gauge = cond.gauge_alert_group.gauge
    @date_time = date_time
    subject = "#{cond.severe_str} Alert. #{asset.title}"
    mail to: email, subject: subject
  end

  def geofence_alert(name, email, message, asset, date_time)
    @name = name
    @asset = asset
    @message = message
    @date_time = date_time
    subject = "High Alert. #{message}"

    mail to: email, subject: subject
  end
end
