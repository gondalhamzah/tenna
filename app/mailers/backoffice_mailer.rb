class BackofficeMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  # en.backoffice_mailer.bsqr_urls.subject

  def bsqr_urls(urls)
    @urls = urls
    mail to: 'kolson@tenna.com, mtahir@tenna.com', from: 'mtahir@tenna.com', subject: "URLs for QR codes, created on #{Time.now.strftime('%d/%m/%Y %I:%M:%S %p')}"
  end
end
