class LearnMore < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  # en.backoffice_mailer.bsqr_urls.subject

  def send_email(params)
    @email_params = params
    mail to: 'learnmore@tenna.com, mtahir@tenna.com', from: 'admin@tenna.com', subject: "Learn More #{Time.now.strftime('%d/%m/%Y %I:%M:%S %p')}"
  end
end
