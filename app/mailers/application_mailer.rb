class ApplicationMailer < ActionMailer::Base
  default from: 'Tenna <no-reply@tenna.com>'
  layout 'mailer'

  helper MailerHelper

end
