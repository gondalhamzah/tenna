class EventMailer < ApplicationMailer
  def path_visited_notification(route, project, asset, driver_stop, path)
    @path = path
    @route = route
    @asset = asset
    @project = project
    @driver_stop = driver_stop
    mail to: project.contact_email, subject: 'Tenna Route Notification'
  end

  def route_generated_notification(driver, email, route, fired_date)
    @route = route
    @fired_date = fired_date
    @driver = driver
    mail to: email, subject: 'Tenna New Route Generated'
  end

  def recurring_route_gen_notification(user, recurring_route, event)
    @recurring_route = recurring_route
    @event = event
    @user = user
    mail to: user.email, subject: 'Tenna New Recurring Route Generated'
  end

  def maint_event_notification(event, asset, user)
    @event = event
    @asset = asset
    @user = user
    mail to: user.email, subject: 'Tenna Asset Maintenance Notification'
  end

  def maint_complete_notification(event, asset, user)
    @event = event
    @asset = asset
    @user = user
    mail to: user.email, subject: 'Tenna Asset Maintenance Notification'
  end

  def other_event_notification(user, event)
    @event = event
    @user = user
    mail to: user.email, subject: 'Tenna Event Notification'
  end
end
