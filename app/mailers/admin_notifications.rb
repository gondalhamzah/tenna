class AdminNotifications < ApplicationMailer
  include Roadie::Rails::Mailer

  def flagged_asset(asset)
    @asset = asset
    roadie_mail to: 'systems@tenna.com', subject: 'An asset was flagged as inappropriate.'
  end

  def send_email(to, params,email_subject)
    @email_template = params[:notification][:message]
    roadie_mail to: to, from: 'admin@tenna.com', subject: email_subject
  end

  def resend_api_key(company)
	@compay = company
	to = company.contact.email
	roadie_mail to: to, from: 'admin@tenna.com', subject: 'API key notification'
  end
end

