class TrackingCodeMailer < ApplicationMailer
  include TrackingCodesHelper

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  # en.backoffice_mailer.bsqr_urls.subject

  def send_generated_qr_codes(to, file_name, company_id, qr_format)
    if Rails.env.development?
      attachments[file_name] = File.read("#{Rails.root}/tmp/#{file_name}")

      @to = to
      @file_name = file_name
      mail to: @to, subject: 'QR Tags'
    else
      obj = S3_OBJECT.object(qr_codes_file_path(company_id, qr_format))

      if obj.exists?
        data = open(obj.presigned_url(:get, expires_in: 60 * 60))
        attachments[file_name] = data.read
        @to = to
        @file_name = file_name
        mail to: @to, subject: 'QR Tags'
      end
    end
  end
end
