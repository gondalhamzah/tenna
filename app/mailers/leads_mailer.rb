class LeadsMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  # en.leads_mailer.send_email.subject

  def send_email(params)
    @lead_email_params = params
    mail to: 'support@tenna.com, mtahir@tenna.com', from: 'admin@tenna.com', subject: "Learn More #{Time.now.strftime('%d/%m/%Y %I:%M:%S %p')}"
  end

  def customer_confirmation(customer)
    @customer = customer
    email = customer.email
    mail to: email, subject: 'Customer Confiramation Email.'
  end

  def sales_rep_notification(customer)
	@customer = customer
	mail to: ['jcueva@tenna.com', 'mtahir@tenna.com'], from: 'admin@tenna.com', subject: "#{customer.lead_type.humanize}"
  end
end
