class DeviseMailer < Devise::Mailer
  include Roadie::Rails::Automatic
  layout 'mailer'

  def confirmation_instructions(record, token, opts={})
    opts[:from] = 'support@tenna.com'
    super
  end

  def invitation_instructions(record, token, opts={})
    if record.class == User
      if record.invited_by_id.nil?
        opts[:subject] = 'You have been invited to Tenna'
      else
        opts[:subject] = "#{User.find(record.invited_by_id).name} at #{record.company.name} invited you to Tenna"
      end
    else
      opts[:template_path] = 'admin_user/mailer'
      opts[:template_name] = 'invitation_instructions'
    end
    opts[:from] = 'support@tenna.com'
    super
  end
end
