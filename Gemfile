source 'https://rubygems.org'
ruby '2.3.1'

gem 'rails', '4.2.6'
gem 'pg'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'remotipart', '~> 1.2'
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary', '~> 0.12.3.1'
gem 'pdfkit'
gem 'ckeditor'
gem 'intercom-rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
gem 'devise', '~> 3.5.4'
gem 'devise_invitable', '~> 1.5.2'
gem 'admin_invitable'
gem 'country_select'
gem 'phonelib'
gem 'zapier_rest_hooks'
gem 'ahoy_matey'

# gem 'seed-fu', '~> 2.3'
gem 'airbrake'
gem 'newrelic_rpm'
gem 'carrierwave', '~> 0.11.2'
gem 'mini_magick'
gem 'fog-aws'
gem 'aws-sdk'
gem 'paper_trail', '~> 5.1.1'
gem 'kaminari'
gem 'rqrcode'
gem 'rqrcode_png'
gem 'prawn-labels'
gem 'redis'
gem 'redis-namespace'
gem 'sidekiq', '~> 4.0.1'
gem 'sidekiq-scheduler'
gem 'sinatra', require: nil
gem 'high_voltage', '~> 2.2.1'
gem 'rubyzip'
gem 'browser-timezone-rails'
gem 'turbolinks'

# admin
gem 'activeadmin', github: 'activeadmin'
gem 'active_skin'
gem 'chartkick'
gem 'groupdate'
gem 'active_admin_editor', github: 'ejholmes/active_admin_editor'
gem 'loofah'

gem 'acts-as-taggable-on', '~> 3.4'
gem 'cancancan', '1.13.1'
gem 'foundation-rails'
gem 'simple_form'
gem 'responders'
gem 'materialize-sass', '~> 0.100'
gem 'phony_rails'
gem 'actionview-encoded_mail_to'
gem 'chewy', '~> 0.8.4'
gem 'elasticsearch-extensions', require: false
gem 'geocoder'
gem 'gmaps4rails'
gem 'autoprefixer-rails'
gem 'roadie-rails'
gem 'validate_url'
gem 'ffaker'
gem 'friendly_id', '~> 5.1.0'
gem 'dalli'
gem 'modernizr-rails'
# gem 'byebug'
gem 'faraday_middleware-aws-signers-v4'
gem 'bundler', '>= 1.8.4'
gem 'geohash', git: 'https://github.com/davetroy/geohash'
gem 'font-awesome-rails'
gem 'select2-rails'
gem 'state_machines-activerecord'
gem 'routific'
gem 'react-rails'
gem 'ice_cube'
gem "browser", require: "browser/browser"
gem 'browserify-rails'
gem 'jwt'
gem 'polylines'
gem 'mqtt'
gem 'ruby-geometry', require: 'geometry'
gem 'httparty'

source 'https://rails-assets.org' do
  gem 'rails-assets-enquire'
  gem 'rails-assets-jquery-alphanum'
  gem 'rails-assets-js-cookie'
  gem 'rails-assets-lodash'
  gem 'rails-assets-markerclustererplus'
  gem 'rails-assets-swiper'
  gem 'rails-assets-Sortable'
  gem 'rails-assets-leaflet'
  gem 'rails-assets-leaflet.markercluster', '0.4.0.hotfix.1'
  gem 'rails-assets-Leaflet.awesome-markers'
  gem 'rails-assets-classnames'
  gem 'rails-assets-sweetalert2'
  gem 'rails-assets-requirejs'
  gem 'rails-assets-tomchentw--react-google-maps'
end

group :development do
  gem 'annotate', require: false
  gem 'bullet' # detects N+1 queries
  gem 'ctags_bundler_rails', require: false
  gem 'quiet_assets'
  gem 'rails-erd'
  gem 'bundler-audit'
  gem 'web-console', '~> 2.0'
  gem 'guard-livereload', '~> 2.5', require: false
  gem 'faker'
  gem 'awesome_print'
end

group :test do
  gem 'webmock'
  gem 'ansi' # for colored reporter
  gem 'simplecov', :require => false
  gem 'minitest-rails'
  gem 'mocha'
  gem 'shoulda'
  gem 'timecop'
  gem 'email_spec'
end

group :staging do
  gem 'rest-client'
  gem 'json'
end


group :development, :test do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'rails_dt'
  gem 'spring'
  gem 'dotenv-rails'
  gem 'pry-rails'
  gem 'cucumber-rails', :require => false
  gem 'selenium-webdriver'
  gem 'chromedriver-helper'
  gem 'letter_opener'
  # Pry navigation commands via byebug
  gem 'pry-byebug'
  gem 'factory_girl_rails', '~> 4.0' # easy fixtures
  gem 'rspec-rails', '~> 3.0' # better unit testing
  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'rspec-activejob'
  gem 'poltergeist'
  gem 'valid_attribute' # concise validation testing
  gem 'database_cleaner'
  gem 'whiny_validation'
end

group :staging, :production do
  gem 'puma'
  gem 'rails_12factor' # for heroku
  gem 'foreman'
end
