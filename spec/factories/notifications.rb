FactoryGirl.define do
  factory :notification do
    user nil
    message "MyString"
    notification_datetime "2016-10-30 14:57:45"
    days_before 1
    duration 1
    is_active false
    notification_type "MyString"
    notification_medium "MyString"
    location "MyString"
    metadata "MyString"
    ip ""
    client_type 1
  end
end
