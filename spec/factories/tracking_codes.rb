FactoryGirl.define do
  factory :tracking_code do
    token SecureRandom.hex
    status 0
    resource nil
  end
end
