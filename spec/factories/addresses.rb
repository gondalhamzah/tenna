FactoryGirl.define do
  factory :address do
    line_1 "745 Atlantic Ave"
    line_2 ""
    city "Boston"
    state "MA"
    zip "02111"
    country "USA"
    addressable_type Company
    association :addressable, factory: :company
    # will want to randomize addressable when project factory is made

    trait :unassigned_address do
      addressable_type nil
      addressable nil
    end

    trait :company_address do
      addressable_type Company
      association :addressable, factory: :company
    end

    trait :project_address do
      addressable_type Project
      association :addressable, factory: :project
    end

    factory :company_address, traits: [:company_address]
    factory :project_address, traits: [:project_address]
    factory :unassigned_address, traits: [:unassigned_address]
  end
end