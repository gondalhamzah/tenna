FactoryGirl.define do
  factory :news_item do
    title "MyString"
    description "MyString"
    post_date "2016-11-18"
    slug "mystring"
    created_by 2
    last_updated_by 2
    priority 1
    featured false
  end
end
