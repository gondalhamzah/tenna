FactoryGirl.define do
  factory :category do
    sequence(:name) { |n| "Test Category #{n}" }
    category_type { Category::CATEGORY_TYPES.sample }
    category_group
  end
end
