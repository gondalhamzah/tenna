FactoryGirl.define do
  factory :gauge_condition do
    gauge_alert_group nil
    gauge_alert_type nil
    values ""
    active ""
    notes "MyText"
    severe_level "MyString"
  end
end
