FactoryGirl.define do
  factory :geo_fence do
    company nil
    type ""
    name "MyString"
    color "MyString"
    radius 1.5
    alert_enter_site false
    alert_exit_site false
    notes "MyText"
  end
end
