FactoryGirl.define do
  factory :email_template do
    name "MyString"
    subject "MyString"
    body "{{#each assets}}{{/each}}"
  end

end
