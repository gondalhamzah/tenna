FactoryGirl.define do
  factory :companies_version do
    field_name "MyString"
    from "MyString"
    to "MyString"
    whodunnit 1
    company_id 1
    action "MyString"
  end
end
