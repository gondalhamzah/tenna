FactoryGirl.define do
  factory :gf_alert_exclusion do
    geo_fence nil
    category_group nil
    category nil
    asset nil
    exclude false
    alert_enter_site nil
    alert_exit_site nil
  end
end
