FactoryGirl.define do
	factory :subscription do
   	company
   	api_key "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTFlZmYwZmE1NTEzZWE0MGFjZDcyN2IiLCJpYXQiOjE0OTUyMDM1OTl9.7rmGymdZylGi1HGDVonReQ0-j-MuhI8RASmykrgebrY"
   	category "routing"
   	number_of_units 10
   	state "active"
   	api_key_created_at "Fri, 19 May 2017 14:19:59 UTC +00:00"
   	note "Sultan Routing Note here."
   	vendor_id "591eff0fa5513ea40acd727b"
   	api_key_updated_at "Fri, 19 May 2017 14:19:59 UTC +00:00"
	 	deleted_at nil
	 	created_at "Fri, 19 May 2017 14:19:57 UTC +00:00"
	 	updated_at "Fri, 19 May 2017 14:19:57 UTC +00:00"
  end
end