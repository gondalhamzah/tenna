FactoryGirl.define do
  factory :event do
    start_date "2017-04-17 17:39:34"
    end_date "2017-04-17 17:39:34"
    text "MyString"
    rec_type "MyString"
    event_length 1
    event_pid 1
    route nil
  end
end
