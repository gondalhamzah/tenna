FactoryGirl.define do
  factory :company do
    to_create { |instance| instance.save(validate: false) }
    # have to bypass to avoid contact circularity
    sequence(:name) { |n| "Test Company#{n}" }
    region 'East'
    website  'http://www.testco.com'
    active true
    association :contact, factory: :user

    trait :admin_company do
      name 'Tenna'
      region nil
      website 'http://www.tenna.com'
      created_at Time.now
      updated_at Time.now
      active true
      admin_company true
    end

    after(:create) do |company, evaluator|
      contact = company.contact
      contact.company = company
      contact.company_name = nil
      contact.role = 'company_admin'
      contact.save
      FactoryGirl.create(:unassigned_address, addressable_type: Company, addressable: company)
    end

    factory :admin_company, traits: [:admin_company]
  end
end
