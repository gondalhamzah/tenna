FactoryGirl.define do
  factory :admin_user do
	 first_name 'Austin'
	 last_name 'Conti'
	 email 'aconti@tenna.com'
	 password 'password1'
	 current_sign_in_at Time.zone.now
	 last_sign_in_at Time.zone.now
	 created_at Time.zone.now
	 updated_at Time.zone.now
	 role 'admin'
  end
end
