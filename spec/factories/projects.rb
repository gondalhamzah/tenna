FactoryGirl.define do
  factory :project do
    to_create { |instance| instance.save(validate:false) }
    sequence(:name) { |n| "Test Project #{n}" }
    project_num FFaker::Guid.guid
    description FFaker::Lorem.paragraph
    duration "6 months"
    closeout_date Time.zone.now + 5.month
    company
    association :contact, factory: :user
    after(:create) do |project, evaluator|
      contact = project.contact
      contact.company = project.company
      contact.company_name = nil
      contact.save
      FactoryGirl.create(:unassigned_address, addressable_type: Project, addressable: project)
    end
  end
end
