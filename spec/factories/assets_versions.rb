FactoryGirl.define do
  factory :assets_version do
    asset_id 1
    field_name "MyString"
    from "MyString"
    to "MyString"
    whodunnit 1
    project_id 1
    company_id 1
  end
end
