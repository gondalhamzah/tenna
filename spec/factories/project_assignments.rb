FactoryGirl.define do
  factory :project_assignment do
    project
    role "project_user"
    association :user, factory: :company_user

    after(:build) do |project_assignment, evaluator|
      project_assignment.user.company = project_assignment.project.company
      project_assignment.user.company_name = nil
    end

    trait :project_assignment_admin do
      role "project_admin"
    end
    
    factory :project_assignment_admin, traits: [:project_assignment_admin]
  end
end
