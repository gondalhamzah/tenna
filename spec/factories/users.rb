FactoryGirl.define do
  factory :user do
    first_name "Joe"
    sequence(:last_name) { |n| "Schmoe#{n}" }
    sequence(:email) { |n| "joe#{n}@test.com" }
    sequence(:company_name) { |n| "Joe Co#{n}" }
    role "public_user"
    confirmed_at Date.today
    password "password1"
    phone_number Phony.normalize("+1 #{FFaker::PhoneNumber.short_phone_number}")
    active true

    trait :admin do
      role "admin"
      company_name nil
    end

    trait :company_admin do
      to_create { |instance| instance.save(validate: false) }
      role "company_admin"
      company_name nil
      company
      # circumvent company contact circularity
      after(:create) do |user, evaluator|
        company = user.company
        user_contact = user.company.contact
        company.contact = user
        company.save
        user_contact.destroy
      end
    end

    trait :company_user do
      role "company_user"
      company_name nil
      company
    end

    trait :deleted_user do
      deleted_at Time.now
    end

    trait :field_member do
      role "field_member"
      sequence(:email) { |n| "field_member#{n}@test.com" }
      company_name nil
      company
    end

    factory :admin, traits: [:admin]
    factory :company_admin, traits: [:company_admin]
    factory :company_user, traits: [:company_user]
    factory :deleted_user, traits: [:company_user, :deleted_user]
    factory :field_member, traits: [:field_member]
  end
end
