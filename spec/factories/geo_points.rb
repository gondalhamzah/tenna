FactoryGirl.define do
  factory :geo_point do
    geo_fence nil
    lat 1.5
    lng 1.5
  end
end
