FactoryGirl.define do
  factory :groups_version do
    group_id 1
    field_name "MyString"
    from "MyString"
    to "MyString"
    action "MyString"
    user nil
    company nil
  end
end
