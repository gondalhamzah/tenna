FactoryGirl.define do
  factory :asset do
    type "Equipment"
    sequence(:title) { |n| "Test Asset#{n}" }
    description { FFaker::Lorem.paragraph }
    association :creator, factory: :user
    category
    zip_code
    location_zip "44672"
    make { FFaker::Vehicle.make }
    model { FFaker::Vehicle.model }
    year { FFaker::Vehicle.year }
    hours 106
    miles 106
    price 971
    market_value 1000
    available_on { Time.zone.now + 5.weeks }
    expires_on { Time.zone.now + 4.days }
    quantity 1
    certified true
    condition { Asset.conditions[:good] }
    country_name "US"
    sequence(:asset_num) { |n| n }
    public { asset_kind == 'for_sale' || asset_kind == 'for_rent' ? true : false }
    rental { %w{rental}.include?(asset_kind) ? true : false }
    owned { asset_kind != 'rental' ? true : false }
    transient do
      asset_kind 'for_sale'
    end
  end
end
