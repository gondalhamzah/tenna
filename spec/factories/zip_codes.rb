FactoryGirl.define do
  factory :zip_code do
    zip_code '44672'
    lat 37.3676715
    lng -93.7029002
    city 'Everton'
    state 'MO'
  end
end
