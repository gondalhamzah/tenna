FactoryGirl.define do
  factory :driver_stop do
    route_recurring nil
    path nil
    done_at "2017-04-26 17:21:48"
    done_lat 1.5
    done_lng 1.5
  end
end
