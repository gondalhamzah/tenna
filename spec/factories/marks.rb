FactoryGirl.define do
  factory :mark do
    association :marker, factory: :user
    association :markable, factory: :asset
    mark "favorite"
  end
end
