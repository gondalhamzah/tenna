FactoryGirl.define do
  factory :company_safety_configuration do
    gauge nil
    company nil
    score_type 1
    score_weight "9.99"
    high_points "9.99"
    medium_points "9.99"
    low_points "9.99"
  end
end
