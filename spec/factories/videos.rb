FactoryGirl.define do
  factory :video do
    url "MyString"
    description "MyString"
    page_name "MyString"
    relative_order 1
    emphasis_indicator 1
    is_visible false
  end
end
