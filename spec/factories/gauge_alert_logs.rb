FactoryGirl.define do
  factory :gauge_alert_log do
    gauge_condition nil
    dismissed false
    asset nil
    values ""
  end
end
