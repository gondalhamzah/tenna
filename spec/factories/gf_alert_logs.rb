FactoryGirl.define do
  factory :gf_alert_log do
    geo_fence nil
    asset nil
    enter false
    dismissed false
  end
end
