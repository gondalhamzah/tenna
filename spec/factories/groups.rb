FactoryGirl.define do
  factory :group do
    project nil
    name "MyString"
    company nil
    notes "MyString"
  end
end
