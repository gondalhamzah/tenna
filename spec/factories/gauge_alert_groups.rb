FactoryGirl.define do
  factory :gauge_alert_group do
    gauge nil
    category_group nil
    category nil
    asset nil
  end
end
