Geocoder.configure(lookup: :test)

Geocoder::Lookup::Test.add_stub(
  '44672', [
    {
      'latitude'   => 37.3676715,
      'longitude'  => -93.7029002,
      'city'       => 'Everton',
      'state_code' => 'MO',
      'types'      => ['postal_code']
    }
  ]
)

Geocoder::Lookup::Test.set_default_stub(
  [
    {
      'latitude'     => 40.7143528,
      'longitude'    => -74.0059731,
      'address'      => 'New York, NY, USA',
      'state'        => 'New York',
      'state_code'   => 'NY',
      'country'      => 'United States',
      'country_code' => 'US',
      'types'        => ['postal_code']
    }
  ]
)
