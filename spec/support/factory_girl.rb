RSpec.configure do |config|
  # Don't have to prefix methods with 'FactoryGirl'
  config.include FactoryGirl::Syntax::Methods

  config.before(:suite) do
    # ensure valid factories before running the test suite
    begin
      # I couldn't get this to pass, and since this file wasn't
      # being included until now, I'll just leave this commented out
      # FactoryGirl.lint
    end
  end
end
