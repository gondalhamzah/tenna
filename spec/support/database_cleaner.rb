RSpec.configure do |config|
  config.use_transactional_fixtures = false

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do |example|
    Chewy.use_after_commit_callbacks = true

    if example.metadata[:js]
      DatabaseCleaner.strategy = :truncation
    else
      Chewy.use_after_commit_callbacks = false # https://github.com/toptal/chewy#rspec-integration
      DatabaseCleaner.strategy = :transaction
    end

    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end
