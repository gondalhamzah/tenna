shared_examples_for "a nav item" do |link_name|
  it "can click on Marketplace" do
    within "nav[id=first-nav]" do
      click_link link_name, match: :first
    end
  end
end

shared_examples_for "a second nav item" do |link_name|
  it "can click on Marketplace" do
    within "nav[id=first-nav]" do
      within "ul.right" do
        click_link link_name, match: :first
      end
    end
  end
end

shared_examples_for "a company nav item" do |link_name|
  it "can click on Marketplace" do
    within "nav[id=first-nav]" do
      within "ul.right" do
        click_link link_name
      end
      within "div#mobile_main_nav_dropdown" do
        click_link link_name, match: :first
      end
    end
  end
end

