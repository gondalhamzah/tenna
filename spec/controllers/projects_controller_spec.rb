
require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do
  let(:company) { create(:company) }
  let(:creator) { create(:company_admin, company_id: company.id) }
  let(:project) { create(:project, contact: creator) }
  let(:project2) { create(:project, contact: creator) }
  let(:projecttest) { create(:project, contact: creator) }
  let(:group) { create(:group, company: creator.company, project: project) }
  let(:asset) { create(:asset, project: project) }

  describe "Get #show" do
    before(:each) do
      sign_in creator
    end
    it "successfully redirect to show page of group" do
      redirect_to(project_path id: project.id)
      expect(response).to have_http_status(:success)
    end
    it "should validate project not be empty" do
      get :show, id: project.id
      expect(assigns(:project)).not_to be_nil
    end
    it "should validate project has groups" do
      get :show, id: project.id
      expect(assigns(:project_groups)).not_to be_nil
    end
    it "should invalidate if project has no group" do
      get :show, id: project2.id
      expect(assigns(:project_groups)).not_to be_nil
    end
    it "should validate project has assets" do
      get :show, id: project.id
      expect(assigns(:assets)).not_to be_nil
    end
  end
  
  describe "Get #index" do
    before(:each) do
      sign_in creator
    end
    it "should validate project" do
      get :index
      expect(assigns(:projects)).not_to be_nil
    end
    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end

  describe 'GET #new' do
    before(:each) do
      sign_in creator
    end    
    it "renders new page" do
      get :new
      expect(response).to render_template("new")
    end
    it "should validate project" do
      get :new
      expect(assigns[:project]).not_to be_nil
    end
    it "should validate user is login" do
      get :new
      expect(assigns[:current_user]).not_to be_nil
    end
  end
  
  
  describe 'GET edit' do
    before(:each) do
      sign_in creator
    end    
    it "renders edit page" do
      get :edit, id: project.id
      expect(response).to render_template("edit")
    end
  end
  
  describe "Post #create" do
    before(:each) do
      sign_in creator
    end    
    it "successfully create a group" do
      project3 = FactoryGirl.attributes_for(:project)
      project3[:contact_id] = creator.id
      expect{
        post :create, project: project3 
      }.to change(Project,:count).by(1)
    end
    it "renders new page" do
      project3 = FactoryGirl.attributes_for(:project)
      project3[:contact_id] = creator.id
      post :create, project: project3 
      # expect(response).to render_template("new")
    end
  end

  describe "Delete #destroy" do
    before(:each) do
      sign_in creator
      company.contact = creator
      company.save
      company.project = project
      company.save
    end    
    it "successfully destroy a group" do
      expect{
        delete :destroy, id: project2.id
      }.to change(Project,:count).by(1)
    end
    it "destroy a group if it has assets" do
    delete :destroy, id: project.id
    # expect(flash[:notice]).to eq "Congratulations on buying our stuff!"
    end
  end

end
