require 'rails_helper'

describe AssetsController do
  let(:company) { create(:company) }
  let(:creator) { create(:company_user, company_id: company.id) }
  let(:project_to) { create(:project, company_id: company.id, contact_id: creator.id) }
  let(:project_from) { create(:project, company_id: company.id, contact_id: creator.id) }
  let(:assets) { create_list(:asset, 4, company_id: company.id, project_id: (project_from.id%2 == 0) ? nil : project_from.id, creator_id: creator.id) }

  describe "PUT #update_multipe" do
    before(:each) do
      sign_in creator
    end
    # company user not have ability to reallocate
    # it "reallocates selected assets" do
    #   create(:project_assignment, project_id: project_from.id, user_id: creator.id)
    #   create(:project_assignment, project_id: project_to.id, user_id: creator.id)
    #   update_params = { asset_ids: assets.map(&:id).join(','), project_id: project_to.id, redirect_to: projects_path, asset_action: 'reallocate'}
    #   put :update_multiple, update_params
    #   assets.each { |asset| expect(asset.reload.project_id).to eq(project_to.id) }
    #   expect(response).to redirect_to(projects_path)
    # end

    it "updates index after asset reallocation" do
      create(:project_assignment, project_id: project_from.id, user_id: creator.id)
      create(:project_assignment, project_id: project_to.id, user_id: creator.id)
      update_params = { asset_ids: assets.map(&:id).join(','), project_id: project_to, redirect_to: projects_path, asset_action: 'reallocate' }
      expect {put :update_multiple, update_params}.to update_index(AssetsIndex::Asset).and_reindex(assets.map(&:id))
    end
  end
end
