require 'rails_helper'

RSpec.describe ClustersController, type: :controller do
  let(:assets_aggregator)   { double }
  let(:projects_aggregator) { double }
  let(:params) { {company_id: 4, creator_id: 3, bounds: {'top_left' => {'lat' => 2, 'lng' => 1}, 'bottom_right' => {'lat' => 2, 'lng' => 1}}, zoom: 4 } }
  before { allow(ClustersAggregator::Asset).to receive(:new).with(params).and_return(assets_aggregator) }
  before { allow(ClustersAggregator::Project).to receive(:new).with(params).and_return(projects_aggregator) }
  before { allow(assets_aggregator).to receive(:results).and_return({aggregation: 'assets'}) }
  before { allow(projects_aggregator).to receive(:results).and_return({aggregation: 'projects'}) }

  before { get :index, params.merge(format: :json) }

  it 'returns clusters data' do
    expect(response).to be_ok
    expect(assigns[:response]).to eq({
                                       'assets'   => {aggregation: 'assets'},
                                       'projects' => {aggregation: 'projects'}
                                     })
  end
end
