require 'rails_helper'

RSpec.describe GeoPositionsController, type: :controller do
  describe 'show' do
    let(:default_params) { { format: :json } }
    let(:custom_params)  { {} }
    let(:params) { default_params.merge(custom_params) }

    let(:default_lat) { CONFIGS['geo_data']['default']['lat'] }
    let(:default_lng) { CONFIGS['geo_data']['default']['lng'] }

    let(:default_geo_object) { Geocoder.search("#{default_lat},#{default_lng}")[0] }

    subject { get :index, params }

    context 'when position wasn`t set' do
      it 'set`s coordinates cookie to default value' do
        subject
        expect(cookies[:coordinates]).to eq(
          {lat: default_geo_object.latitude, lng: default_geo_object.longitude, accuracy: 0.0}.to_json
        )
      end

      it 'set`s zip_code cookie to default value' do
        subject
        expect(cookies[:zip_code]).to eq default_geo_object.postal_code
      end

      it 'response with appropriate json' do
        subject

        expect(json).to eq({
          success:     true,
          status:      200,
          message:     'Position was set successfully',
          coordinates: { lat: default_geo_object.latitude, lng: default_geo_object.longitude },
          zip_code:    default_geo_object.postal_code
        })
      end
    end

    context 'when position was set' do
      context 'by lat and lng attributes' do
        let(:custom_params)  { { lat: lat, lng: lng } }

        let(:geo_object) do
          Geocoder.search("#{lat},#{lng}")[0]
        end

        context 'and geocoder didn`t find any matches' do
          let(:lat) { 42.366423 }
          let(:lng) { 44.008777 }

          it 'set`s coordinates cookie to default value' do
            subject
            expect(cookies[:coordinates]).to eq(
              {lat: default_geo_object.latitude, lng: default_geo_object.longitude, accuracy: 0.0}.to_json
            )
          end

          it 'set`s zip_code cookie to default value' do
            subject
            expect(cookies[:zip_code]).to eq default_geo_object.postal_code
          end

          it 'response with appropriate json' do
            subject

            expect(json).to eq({
              success:     true,
              status:      200,
              message:     'Position was set successfully',
              coordinates: { lat: default_geo_object.latitude, lng: default_geo_object.longitude },
              zip_code:    default_geo_object.postal_code
            })
          end
        end

        context 'and geocoder find match' do
          let(:lat) { 37.944759 }
          let(:lng) { -91.751303 }

          it 'set`s coordinates cookie to specific value' do
            subject
            expect(cookies[:coordinates]).to eq(
              {lat: geo_object.latitude, lng: geo_object.longitude, accuracy: 0.0}.to_json
            )
          end

          it 'set`s zip_code cookie to specific value' do
            subject
            expect(cookies[:zip_code]).to eq geo_object.postal_code
          end

          it 'response with appropriate json' do
            subject

            expect(json).to eq({
              success:     true,
              status:      200,
              message:     'Position was set successfully',
              coordinates: { lat: geo_object.latitude, lng: geo_object.longitude },
              zip_code:    geo_object.postal_code
            })
          end
        end
      end

      context 'by addr attribute' do
        let(:custom_params)  { { addr: addr } }

        let(:geo_object) do
          GenericGeocoder.search(addr)[0]
        end

        context 'and GenericGeocoder didn`t find any matches' do
          let(:addr) { 111111111111111111 }

          # UsaGeocoder in test env always return NY if didn't find any matches
          before { allow(GenericGeocoder).to receive(:search).with(addr).and_return([]) }

          it 'didn`t set coordinates cookie' do
            subject
            expect(cookies[:coordinates]).to eq nil
          end

          it 'didn`t set zip_code cookie' do
            subject
            expect(cookies[:zip_code]).to eq nil
          end

          it 'response with appropriate json' do
            subject

            expect(json).to eq({
              success:     false,
              status:      404,
              message:     'Wrong zip code!',
              coordinates: nil,
              zip_code:    nil
            })
          end
        end

        context 'and GenericGeocoder find match' do
          let(:addr) { 11003 }

          it 'set`s coordinates cookie to specific value' do
            subject
            expect(cookies[:coordinates]).to eq(
              { lat: geo_object.latitude, lng: geo_object.longitude, accuracy: 0.0 }.to_json
            )
          end

          it 'set`s zip_code cookie to specific value' do
            subject
            expect(cookies[:zip_code]).to eq geo_object.postal_code
          end

          it 'response with appropriate json' do
            subject

            expect(json).to eq({
              success:     true,
              status:      200,
              message:     'Position was set successfully',
              coordinates: { lat: geo_object.latitude, lng: geo_object.longitude },
              zip_code:    geo_object.postal_code
            })
          end
        end
      end
    end
  end
end
