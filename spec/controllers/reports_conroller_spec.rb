
require 'rails_helper'

RSpec.describe ReportsController, type: :controller do
  let(:company) { create(:company) }
  let(:creator) { create(:company_admin, company_id: company.id) }
  let(:project1) { create(:project, contact: creator, company: company) }
  let(:project2) { create(:project, contact: creator, company: company) }
  let(:project3) { create(:project, contact: creator, company: company) }
  let(:asset1) { create(:asset, company_id: company.id, project_id: project1.id, creator_id: creator.id) }
  let(:asset2) { create(:asset, company_id: company.id, project_id: project2.id, creator_id: creator.id) }
  let(:asset3) { create(:asset, company_id: company.id, project_id: project3.id, creator_id: creator.id) }


  describe "Get #index" do
    before(:each) do
      sign_in creator
    end
    it "successfully create report for asset project sorted by asset id" do
        asset1.created_at = Time.zone.now - 4.day
        asset1.save
        asset2.created_at = Time.zone.now - 10.day
        asset2.save
        asset3.created_at = Time.zone.now - 15.day
        asset3.save
        asset2.project = project1
        asset2.save
        asset_history = AssetsVersion.last
        asset_history.created_at =  Time.zone.now - 6.day
        asset_history.save
        asset1.project = project2
        asset1.save
        asset_history = AssetsVersion.last
        asset_history.created_at =  Time.zone.now - 2.day
        asset_history.save
        asset3.project = project1
        asset3.save
        asset_history = AssetsVersion.last
        asset_history.created_at =  Time.zone.now - 2.day
        asset_history.save
        asset3.project = project3
        asset3.save
        asset_history = AssetsVersion.last
        asset_history.created_at =  Time.zone.now - 1.day
        asset_history.save
        AssetsIndex.import(asset: asset1)
        AssetsIndex.import(asset: asset2)
        AssetsIndex.import(asset: asset3)
        AssetsVersionsIndex.import(asset: asset1)
        AssetsVersionsIndex.import(asset: asset2)
        AssetsVersionsIndex.import(asset: asset3)
        ProjectsIndex.import(asset: project1)
        projects = project1.id
        asset_days = [{:asset_id=> asset1.id, :no_of_days_on_project=>0}, {:asset_id=>asset2.id, :no_of_days_on_project=>6}, {:asset_id=>asset3.id, :no_of_days_on_project=>1}]
        members = {:leaders=>0, :members=>0}
        post :generate_project_assets_report, :report_assets_search => { "projects" => projects , "sort_order"=>"Asset Id", "date_from"=>"", "date_to"=>"", "export_type"=>"pdf", "project"=>""}
        expect(assigns(:asset_versions_with_total_days_of_assets_on_project)).to eq(asset_days)
        expect(assigns(:project)).to eq(project1)
        expect(assigns(:assets_count)).to eq(1)
        expect(assigns(:team_members)).to eq(members)
    end

    it "successfully create report for asset project with sorting order of asset title" do
      asset1.title = "XYZ"
      asset1.created_at = Time.zone.now - 4.day
      asset1.save
      asset2.title = "UVW"
      asset2.created_at = Time.zone.now - 10.day
      asset2.save
      asset3.title = "ABC"
      asset3.created_at = Time.zone.now - 15.day
      asset3.save
      asset2.project = project1
      asset2.save
      asset_history = AssetsVersion.last
      asset_history.created_at =  Time.zone.now - 6.day
      asset_history.save
      asset1.project = project2
      asset1.save
      asset_history = AssetsVersion.last
      asset_history.created_at =  Time.zone.now - 2.day
      asset_history.save
      asset3.project = project1
      asset3.save
      asset_history = AssetsVersion.last
      asset_history.created_at =  Time.zone.now - 2.day
      asset_history.save
      asset3.project = project3
      asset3.save
      asset_history = AssetsVersion.last
      asset_history.created_at =  Time.zone.now - 1.day
      asset_history.save
      AssetsIndex.import(asset: asset1)
      AssetsIndex.import(asset: asset2)
      AssetsIndex.import(asset: asset3)
      AssetsVersionsIndex.import(asset: asset1)
      AssetsVersionsIndex.import(asset: asset2)
      AssetsVersionsIndex.import(asset: asset3)
      ProjectsIndex.import(asset: project1)
      projects = project1.id
      asset_days = [{:asset_id=>asset3.id, :no_of_days_on_project=>1}, {:asset_id=>asset2.id, :no_of_days_on_project=>6}, {:asset_id=>asset1.id, :no_of_days_on_project=>0}]
      members = {:leaders=>0, :members=>0}
      post :generate_project_assets_report, :report_assets_search => { "projects" => projects , "sort_order"=>"Asset Title", "date_from"=>"", "date_to"=>"", "export_type"=>"pdf", "project"=>""}
      expect(assigns(:asset_versions_with_total_days_of_assets_on_project)).to eq(asset_days)
      expect(assigns(:project)).to eq(project1)
      expect(assigns(:assets_count)).to eq(1)
      expect(assigns(:team_members)).to eq(members)
    end
  end
end
