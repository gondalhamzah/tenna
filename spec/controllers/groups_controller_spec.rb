
require 'rails_helper'

RSpec.describe GroupsController, type: :controller do
  let(:company) { create(:company) }
  let(:creator) { create(:company_admin, company_id: company.id) }
  let(:project) { create(:project, contact: creator) }
  let(:group) { create(:group, company: creator.company, project: project) }

  describe "Get #show" do
    before(:each) do
      sign_in creator
    end
    it "successfully redirect to show page of group" do
      redirect_to(group_path id: group.id)
      expect(response).to have_http_status(:success)
    end
    it "should validate group import link" do
      get :show, id: group.id
      expect(assigns(:groups)).not_to be_nil
    end
  end
  
  describe "Get #index" do
    before(:each) do
      sign_in creator
    end
    it "successfully redirect to index page of groups" do
      redirect_to(groups_path)
      expect(response).to have_http_status(:success)
    end
    it "should validate group filer search" do
      get :index
      expect(assigns(:groups)).not_to be_nil
    end
    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end

  describe 'GET #new' do
    before(:each) do
      sign_in creator
    end    
    it "renders new page" do
      get :new
      expect(response).to render_template("new")
      get :edit, :id => group.id
      expect(response).to render_template("edit")
    end
  end
  
  describe 'GET edit' do
    before(:each) do
      sign_in creator
    end    
    it "renders edit page" do
    end
  end
  
  describe "Post #create" do
    before(:each) do
      sign_in creator
    end    
    it "successfully create a group" do
      group_b = FactoryGirl.attributes_for(:group)
      group_b[:project] = project
      expect{
        post :create, group: group_b 
      }.to change(Group,:count).by(1)
    end
    it "should not successfully create a group if project is empty" do
      expect{
        group_b = FactoryGirl.attributes_for(:group)
        post :create, group: group_b 
      }.to change(Group,:count).by(0)
    end
    it "should not successfully create a group if company is empty" do
      expect{
        group_b = FactoryGirl.attributes_for(:group)
        post :create, group: group_b 
      }.to change(Group,:count).by(0)
    end
    it "validate company of group" do
      group_b = FactoryGirl.attributes_for(:group)
      post :create, group: group_b
      expect(assigns[:group]).not_to eq nil 
      expect(response).to render_template("new")
    end
  end
end
