require 'rails_helper'

RSpec.describe "company_safety_configurations/index", type: :view do
  before(:each) do
    assign(:company_safety_configurations, [
      CompanySafetyConfiguration.create!(
        :gauge => nil,
        :company => nil,
        :score_type => 1,
        :score_weight => "9.99",
        :high_points => "9.99",
        :medium_points => "9.99",
        :low_points => "9.99"
      ),
      CompanySafetyConfiguration.create!(
        :gauge => nil,
        :company => nil,
        :score_type => 1,
        :score_weight => "9.99",
        :high_points => "9.99",
        :medium_points => "9.99",
        :low_points => "9.99"
      )
    ])
  end

  it "renders a list of company_safety_configurations" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
