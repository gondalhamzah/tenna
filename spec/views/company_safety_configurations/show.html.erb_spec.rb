require 'rails_helper'

RSpec.describe "company_safety_configurations/show", type: :view do
  before(:each) do
    @company_safety_configuration = assign(:company_safety_configuration, CompanySafetyConfiguration.create!(
      :gauge => nil,
      :company => nil,
      :score_type => 1,
      :score_weight => "9.99",
      :high_points => "9.99",
      :medium_points => "9.99",
      :low_points => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
  end
end
