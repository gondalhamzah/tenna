require 'rails_helper'

RSpec.describe "company_safety_configurations/edit", type: :view do
  before(:each) do
    @company_safety_configuration = assign(:company_safety_configuration, CompanySafetyConfiguration.create!(
      :gauge => nil,
      :company => nil,
      :score_type => 1,
      :score_weight => "9.99",
      :high_points => "9.99",
      :medium_points => "9.99",
      :low_points => "9.99"
    ))
  end

  it "renders the edit company_safety_configuration form" do
    render

    assert_select "form[action=?][method=?]", company_safety_configuration_path(@company_safety_configuration), "post" do

      assert_select "input#company_safety_configuration_gauge_id[name=?]", "company_safety_configuration[gauge_id]"

      assert_select "input#company_safety_configuration_company_id[name=?]", "company_safety_configuration[company_id]"

      assert_select "input#company_safety_configuration_score_type[name=?]", "company_safety_configuration[score_type]"

      assert_select "input#company_safety_configuration_score_weight[name=?]", "company_safety_configuration[score_weight]"

      assert_select "input#company_safety_configuration_high_points[name=?]", "company_safety_configuration[high_points]"

      assert_select "input#company_safety_configuration_medium_points[name=?]", "company_safety_configuration[medium_points]"

      assert_select "input#company_safety_configuration_low_points[name=?]", "company_safety_configuration[low_points]"
    end
  end
end
