require 'rails_helper'
require 'elasticsearch/extensions/test/cluster'

describe 'Nav Items' do
  before do
    login_as(user, scope: :user)
  end

  context 'As a private user' do
    let(:user)  { create :company_user, role: :company_admin }
    before :each do
      visit dashboard_path
    end

    it_behaves_like 'a nav item', 'Marketplace'
    it_behaves_like 'a company nav item', 'Company'
    it_behaves_like 'a nav item', 'Sites'
    it_behaves_like 'a nav item', 'Assets'

    it 'can click on My Name' do
      within 'nav[id=first-nav]' do
        within 'ul.right' do
          click_link user.name_with_initial
        end
      end
    end
  end
end
