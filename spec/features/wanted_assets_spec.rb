require 'rails_helper'

feature 'wanted_assets' do
  let(:company_a)  {create(:company)}

  let(:company_admin) { create(:company_user, role: :company_admin, company: company_a) }
  let(:company_user) { create(:company_user, role: :company_user, company: company_a) }

  let(:wanted_asset) { create(:wanted_asset, creator: company_admin.id, company: company_admin.company, title: 'Shovel') }
  let(:wanted_asset) { create(:wanted_asset, creator: company_admin.id, company: company_admin.company, title: 'Sheet Pile') }

  let(:wanted_asset) { create(:wanted_asset, creator: company_user.id, company: company_user.company, title: 'Sheet for Temporary SOE') }
  let(:wanted_asset) { create(:wanted_asset, creator: company_user.id, company: company_user.company, title: 'Grease Trailer') }

  context 'A company admin user' do
    before :each do
      login_as(company_admin, scope: :company_admin)
    end

    it 'can access his company assets' do
      visit(assets_path(company_a))
      expect(current_path).to eq(assets_path(company_a))
    end

    it 'can access all wanted assets' do
      visit(wanted_assets_path(company_a))
      expect(current_path).to eq(wanted_assets_path(company_a))
    end
  end


  context 'A company user' do
    before :each do
      login_as(company_user, scope: :company_user)
    end

    it 'can access his company assets' do
      visit(assets_path(company_a))
      expect(current_path).to eq(assets_path(company_a))
    end

    it 'can access his wanted assets' do
      visit(wanted_assets_path(company_a))
      expect(current_path).to eq(wanted_assets_path(company_a))
    end
  end
end
