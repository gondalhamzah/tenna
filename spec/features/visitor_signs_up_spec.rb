require 'rails_helper'

feature 'Visitor signs up' do
  def sign_up_with(email, company_name, first_name, last_name, password)
    visit new_user_registration_path
    fill_in 'Email', with: email
    fill_in 'Company Name', with: company_name
    fill_in 'First Name', with: first_name
    fill_in 'Last Name', with: last_name
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    check 'user_terms_of_service'
    click_button 'Join'
  end

  def no_terms_sign_up_with(email, company_name, first_name, last_name, password)
    visit new_user_registration_path
    fill_in 'Email', with: email
    fill_in 'Company Name', with: company_name
    fill_in 'First Name', with: first_name
    fill_in 'Last Name', with: last_name
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    uncheck 'user_terms_of_service'
    click_button 'Join'
  end

  scenario 'with valid email and password' do
    sign_up_with 'valid@example.com', 'company', 'John', 'Doe', 'password@'

    expect(page).to have_content(I18n.t('devise.registrations.signed_up_but_unconfirmed'))
  end

  scenario 'with invalid email' do
    sign_up_with 'invalid', 'company', 'John', 'Doe', 'password@'

    # expect(page).to have_content('Sign In')
  end

  scenario 'with blank password' do
    sign_up_with 'valid@example.com', 'company', 'John', 'Doe', ''

    # expect(page).to have_content('Sign In')
  end

  scenario 'with unchecked terms of service' do
    no_terms_sign_up_with 'valid@example.com', 'company', 'John', 'Doe', 'password@'
    # expect(page).to have_content('Sign In')
  end
end
