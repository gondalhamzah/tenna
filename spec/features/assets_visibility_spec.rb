require "rails_helper"

feature "Asset Visibility" do
  let(:private_user) { create(:company_user, role: :company_admin) }
  let(:private_project) { create(:project, company: private_user.company) }
  let(:public_user) { create(:user) }

  let(:public_asset) { create(:asset, company: create(:company), asset_kind: 'for_sale') }

  context "Guest user" do
    before do
      3.times{
        AssetsIndex.import! asset: create(:asset, creator: public_user, type: "Equipment", asset_kind: 'for_sale')
      }
      2.times{
        AssetsIndex.import! asset: create(:asset, creator: private_user, company: private_user.company, project: private_project, type: "Equipment", asset_kind: 'for_rent')
      }
      AssetsIndex.import! asset: create(:asset, creator: private_user, company: private_user.company, project: private_project, type: "Equipment", asset_kind: 'rental')
      AssetsIndex.import! asset: public_asset
    end

    # it "can see only public assets" do
    #   visit marketplace_url
    #   expect(page).to have_css("#table-assets tr", count: 6)
    #   Asset.where(owned: true, rental: true, public: true).each do |a|
    #     expect(page).to have_css("#asset_#{a.id}", count: 1)
    #   end
    # end
  end

  context "Public user" do
    before do
      login_as(public_user, scope: :user)
      3.times{
        AssetsIndex.import! asset: create(:asset, creator: public_user, type: 'Equipment', asset_kind: 'for_sale')
      }
      2.times{
        AssetsIndex.import! asset: create(:asset, creator: private_user, company: private_user.company, project: private_project, type: 'Equipment', asset_kind: 'for_sale')
      }
      AssetsIndex.import! asset: create(:asset, creator: private_user, company: private_user.company, project: private_project, type: 'Equipment', asset_kind: 'rental')

      AssetsIndex.import! asset: public_asset

      expect(private_user.assets.count).to eq(3)
      expect( FilterableAssets.new(user: nil, params: {}, zip_code: nil).public_assets.count).to eq(6)
    end

    # scenario "In marketplace can view only his own and public assets" do
    #   visit(marketplace_path)
    #   expect(page).to have_selector('table#table-assets tr', count: 6)
    #   Asset.where(owned: true, public: true).each do |a|
    #     expect(page).to have_css("#asset_#{a.id}", count: 1)
    #   end
    # end

    # scenario "In dashboard can view only his own and public assets" do
    #   visit(dashboard_path)
    #   # public for_sale asset's check
    #   expect(page).to have_content(public_user.assets.where(owned: true, public: true, rental: false).first.title)
    #   expect(page).to have_content(private_user.assets.where(owned: true, public: true, rental: false).first.title)
    #   expect(page).to_not have_content(private_user.assets.where(rental: true, owned: false, public: false).first.title)
    #   expect(page).to have_content(public_asset.title)
    #   expect(page).to have_selector('table#table-assets tr', count: 6)
    # end
  end

  context "Private user" do
    before do
      login_as(private_user, scope: :user)
      3.times {
        AssetsIndex.import! asset: create(:asset, creator: public_user, type: 'Equipment', asset_kind: 'for_sale')
      }
      2.times {
        AssetsIndex.import! asset: create(:asset, creator: private_user, company: private_user.company, project: private_project, type: 'Equipment', asset_kind: 'for_rent')
      }
      AssetsIndex.import! asset: create(:asset, creator: private_user, company: private_user.company, project: private_project, type: 'Equipment', asset_kind: 'rental')
      AssetsIndex.import! asset: public_asset
    end

    scenario "In marketplace can view only his own and public assets" do
      visit(marketplace_path)
      expect(page).to have_selector('table#table-assets tr', count: 6)
    end

    scenario "In dashboard can view only his own and company assets" do
      visit(dashboard_path)
      # public asset's check
      expect(page).to_not have_content(public_user.assets.where(public: true).first.title)
      # rental asset check
      expect(page).to have_content(private_user.assets.where(rental: true, owned: false, public: false).first.title)
      # TODO for rent check
      # expect(page).to have_content(private_user.assets.where(rental: true, owned: true, public: true).first.title)
      # for sale check
      expect(page).to have_content(private_user.assets.where(rental: false, owned: true, public: true).first.title)
      expect(page).to_not have_content(public_asset.title)
      expect(page).to have_selector('table#table-assets tr', count: 3)
    end
  end
end
