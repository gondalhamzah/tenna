require "rails_helper"

feature 'User forgot password' do
  let(:soft_deleted_user) { create(:deleted_user) }
  let(:new_user) { build(:company_user) }
  let(:exist_user) { create(:company_user) }

  context "when user is" do
    before do
      visit new_user_password_path
    end

    scenario 'soft deleted' do
      send_to(soft_deleted_user)
      message = "Your account is deleted."
      expect(page).to have_content(message)
    end

    scenario 'not found' do
      send_to(new_user)
      message = "not found"
      expect(page).to have_content(message)
    end

    scenario 'exist end not soft deleted' do
      send_to(exist_user)
      expect(page.current_path).to eq('/users/sign_in')
      # message = "You will receive an email with instructions on how to reset your password in a few minutes."
      # expect(page).to have_content(message)
    end

    def send_to(user)
      # fill_in 'Email', with: user.email
      find(:xpath, "//*[@id='user_email']").set(user.email)
      click_button 'Send Reset Instructions'
    end

  end
end
