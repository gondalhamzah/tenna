require "rails_helper"

feature "User Profile" do
  let(:company_a)  {create(:company)}
  let(:company_b)  {create(:company)}
  let(:company_admin) { create(:company_user, role: :company_admin, company: company_a) }
  let(:company_user_a) { create(:company_user, role: :company_user, company: company_a) }
  let(:company_user_b) { create(:company_user, role: :company_user, company: company_b) }

  context "A company admin" do
    before :each do
      login_as(company_admin, scope: :user)
    end

    it "can access his own profile" do
      visit company_user_path(company_a, company_admin)
      expect(current_path).to eq(company_user_path(company_a, company_admin))
    end
    it "cannot remove his own account" do
      visit company_user_path(company_a, company_admin)
      expect(page).to_not have_link("Remove")
    end

    it "can access profiles of users in his company" do
      visit company_user_path(company_a, company_user_a)
      expect(current_path).to eq(company_user_path(company_a, company_user_a))
    end

    it "can assign projects to users of his company" do
      visit company_user_path(company_a, company_user_a)
      click_link "Add site assignment"
    end

    it "cannot access profiles of users in other companies" do
      visit company_user_path(company_b, company_user_b)
      expect(current_path).to eq(dashboard_path)
    end
  end

  context "A company user" do
    before :each do
      login_as(company_user_a, scope: :user)
    end

    it "can access his own profile" do
      visit company_user_path(company_a, company_user_a)
      expect(current_path).to eq(company_user_path(company_a, company_user_a))
    end

    it "can edit his own profile" do
      visit company_user_path(company_a, company_user_a)
      within("#user-info .header-link") do
        click_link "Edit"
      end
    end

    it "cannot remove his own account" do
      visit company_user_path(company_a, company_user_a)
      expect(page).to_not have_link("Remove")
    end

    it "can assign projects to users of his company" do
      visit company_user_path(company_a, company_user_a)
      expect(page).to_not have_link("Add site assignment")
    end

    it "can access profiles of other users" do
      visit company_user_url(company_a, company_admin)
      expect(current_path).to eq(company_user_path(company_a, company_admin))
    end

    it "can access profiles of users on other companies" do
      visit company_user_url(company_b, company_user_b)
      expect(current_path).to eq(dashboard_path)
    end
  end
end
