require "rails_helper"

feature "Deleted User Visibility" do
  let!(:private_user) { create(:company_admin) } # create 2 users

  let!(:company_user1) { create(:company_user, company: private_user.company) }
  let!(:company_user2) { create(:company_user, company: private_user.company) }
  let!(:private_project) { create(:project, company: private_user.company) }

  context "Delete user from company" do
    before do
      allow_any_instance_of(ClustersAggregator::Base).to receive(:results).and_return({})

      login_as(private_user, scope: :user)
      visit(company_users_path(private_user.company))
      # click_link 'Remove', match: :first
      click_link 'Delete', match: :first
    end

    scenario "In marketplace can view only his own and public assets when create new project" do
      visit(new_project_path)
      expect(page).to have_selector('.project_contact option', count: 3)
    end

    scenario "In marketplace can view only his own and public assets when create new project assignment" do
      visit(new_project_assignment_path(private_project))
      expect(page).to have_selector('.project_assignment_user option', count: 1)
    end
  end
end
