require 'rails_helper'

describe 'Asset Form' do
  let!(:categories)  { create_list :category, 15 }

  describe 'update' do
    before do
      login_as(user, scope: :user)
      visit(edit_asset_path(asset))
    end

    context 'as private user' do
      let(:user)  { create :company_user, role: :company_admin }
      let(:project) { create(:project, company: user.company, contact: user.company.contact) }
      let(:category) { Category.find_by_category_type('material')}
      let(:asset) { create(:asset, creator: user, company: user.company, project_id: project.id, public: true,
                           type: 'Material', category_id: category.id) }

      # it 'allows to change asset title' do
      #   fill_in 'Title', with: 'New Random String1'
      #   click_button 'Update Asset'

      #   expect(asset.reload.title).to eq 'New Random String1'
      # end

      # it 'allows to change asset type' do
      #   choose 'Equipment'
      #   categories.map(&:name).map{ |category| expect(page).to have_text(category) }
      #   find('#equipment-select').find(:xpath, 'option[1]').select_option

      #   click_button 'Update Asset'
      #   expect(asset.reload.type).to eq 'Equipment'
      # end
    end

    context 'as public user' do
      let(:user)  { create :user }
      let(:category) { Category.find_by_category_type('material')}
      let(:asset) { create(:asset, creator: user, public: true, type: 'Material', category_id: category.id) }

      # it 'allows to change asset title' do
      #   fill_in 'asset_title', with: 'New Random String2'
      #   click_button 'Update Asset'

      #   expect(asset.reload.title).to eq 'New Random String2'
      # end

      # it 'allows to change asset type' do
      #   choose 'Equipment'
      #   categories.map(&:name).map{ |category| expect(page).to have_text(category) }
      #   find('#equipment-select').find(:xpath, 'option[1]').select_option
      #   click_button 'Update Asset'
      #   expect(asset.reload.type).to eq 'Equipment'
      # end
    end
  end
end
