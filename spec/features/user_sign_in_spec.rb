require 'rails_helper'

feature 'User signs in' do
  def sign_in(user = create(:company_user))
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Sign In'
  end

  scenario 'with valid email and password' do
    sign_in

    expect(page).to have_content('Log Out')
  end

  scenario 'with valid email and password for inactive user' do
    user = create(:company_user, active: false)

    sign_in(user)

    contact = user.company.contact
    message = "The account you are attempting to access is not currently active, please contact #{contact.name} at #{contact.email}. Thank you, Tenna."
    expect(page).to have_content(message)
  end
end
