require "rails_helper"

feature "QR Code user flow" do
  # context "Guest user" do
  #   it "requires authentication" do
  #     visit("http://test.bsqr1.com/123123")
  #     expect(page.current_path).to eq(new_user_session_path)
  #   end
  # end
  context "Private User" do
    let(:private_user) { create(:company_admin, role: :company_admin) }
    before :each do
      login_as(private_user, scope: :user)
    end
    # context "Invalid tracking code" do
    #   it "returns an error when the tracking code doesn't exist" do
    #     visit("http://test.bsqr1.com/123123")
    #     # fill_in "Email", with: private_user.email
    #     # fill_in "Password", with: "password1"
    #     # click_button "Sign In"
    #     expect(page).to have_content("is not a valid tracking code")
    #   end
    # end
    context "Existing tracking code" do
      let!(:tracking_code) {create(:tracking_code)}
      # context "unassigned" do
      #   it "allows the user to select the asset to associate" do
      #     # visit("http://test.bsqr1.com/123123")
      #     url = "http://test.bsqr1.com/#{tracking_code.token}"
      #     visit(url)
      #     # fill_in "Email", with: private_user.email
      #     # fill_in "Password", with: "password1"
      #     # click_button "Sign In"
      #     # expect(page.current_path).to eq(assets_path)
      #     expect(page.current_path).to eq("/tracking_codes/assets")


      #   end
      # end
    end
  end
end
