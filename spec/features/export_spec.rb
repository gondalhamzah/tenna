require "rails_helper"

feature "Export" do
  let(:private_user) { create(:company_user, role: :company_user) }
  context "Export All Project and Data" do
    before :each do
      login_as(private_user, scope: :user)
    end
    context "With projects" do
      before :each do
        create(:project_assignment_admin, user: private_user)
        create(:asset, creator: private_user, company: private_user.company, project: private_user.projects.first)
        # create(:group, company: private_user.company, project: private_user.projects.first)
        # create(:groups_version, user: private_user, company: private_user.company)
        # byebug
        ProjectsIndex.import! project: private_user.projects.first
        expect(private_user.projects).not_to be_empty
        # sleep 2
        visit projects_path
        # sleep 2
      end

      it "has a link to view the export page" do
        expect(page).to have_link("Sites and Assets Data Export")
        click_link "Sites and Assets Data Export"
        expect(current_path).to eq(exports_path)
      end
    end
  end
end


