require "rails_helper"

feature "User Profile" do
  let(:company_a)  {create(:company)}
  let(:company_b)  {create(:company)}
  let(:company_admin) { create(:company_user, role: :company_admin, company: company_a) }
  let(:company_user_a) { create(:company_user, role: :company_user, company: company_a) }
  let(:company_user_b) { create(:company_user, role: :company_user, company: company_b) }

  context "A company admin" do
    before :each do
      login_as(company_admin, scope: :user)
    end

    it "can access his company profile" do
      visit(company_path(company_a))
      expect(current_path).to eq(company_path(company_a))
    end
    
    it "can invite new users" do
      visit(company_path(company_a))
      expect(page).to have_link("Invite New User")
    end
    
    it "can edit company details" do
      visit(company_path(company_a))
      expect(page).to have_link("Edit")
    end
  end

  context "A company user" do
    before :each do
      login_as(company_user_a, scope: :user)
    end
    it "can access his company profile" do
      visit(company_path(company_a))
      expect(current_path).to eq(company_path(company_a))
    end
    
    it "cannot invite new users" do
      visit(company_path(company_a))
      expect(page).to_not have_link("Invite New User")
    end
    
    it "cannot edit company details" do
      visit(company_path(company_a))
      expect(page).to_not have_link("Edit")
    end
  end
end
