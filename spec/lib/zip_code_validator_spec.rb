require 'rails_helper'

class Validatable
  include ActiveModel::Model
  include ActiveModel::Validations

  attr_accessor :zip,:country

  validates :zip, zip_code: true
end

describe ZipCodeValidator do
  subject { Validatable.new(zip: zip, country: country) }

  context 'when zip is valid' do
    let(:zip) { '02575' }
    let(:country) { 'US' }

    it { is_expected.to be_valid }
  end

  context 'when zip+4 is valid' do
    let(:zip) { '02575-0605' }
    let(:country) { 'US' }

    it { is_expected.to be_valid }
  end

  context 'when post code is invalid' do
    let(:zip) { '12345' }
    let(:country) { 'US' }

    it { is_expected.to be_valid }
  end

end