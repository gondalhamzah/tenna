require 'rails_helper'

RSpec.describe ClustersAggregator::Project, type: :service do
  let(:instance) { described_class.new(company_id: 1, creator_id: 3, bounds: 2, zoom: 3) }

  context '#index_name' do
    subject { instance.send(:index_name) }
    it { is_expected.to eq ProjectsIndex::Project.index_name }
  end

  context '#document_type' do
    subject { instance.send(:document_type) }
    it { is_expected.to eq ProjectsIndex::Project.type_name }
  end
end
