require 'rails_helper'

describe ProjectExporter do

  let!(:company) { FactoryGirl.create(:company) }
  let!(:user) { FactoryGirl.create(:company_user, company: company) }

  describe ".export" do

    def files_in(zip)
      Zip::File.open(zip).entries.map(&:name)
    end

    def column_data(zip_file, entry_file, attr = nil)
      data = CSV.parse(
        Zip::File.open(zip_file)
        .entries
        .detect{ |e| e.name =~ /#{entry_file}/ }
        .get_input_stream
        .read,
        headers: true,
        skip_lines: /^Report:/,
        skip_blanks: true
      )
      attr ? data.map{ |r| r[attr.to_s] } : data.to_a
    end

    let(:mmddyy) { DateTime.now.strftime("%m-%d-%Y") }

    before do
      projects = 2.times.map do
        FactoryGirl.create(:project, company: company, contact: company.contact)
      end

      2.times.map do
        FactoryGirl.create(
          :asset,
          company: company,
          project: projects.sample,
          creator: company.contact
        )
      end

      # `rm /tmp/*projects*.zip` # Delete any pre-existing zip files
    end

    it "returns path to a zip file" do
      zip_path = ProjectExporter.export(user)
      expect(zip_path.split(".").last).to eq('zip')
    end

    it "contains csv files for projects and assets" do
      zip = ProjectExporter.export(user)
      expect(files_in(zip)).to match_array(["Assets-list-#{mmddyy}.csv", "Projects #{mmddyy}.csv"])
    end

    it "projects-csv contains projects from user's company" do
      zip = ProjectExporter.export(user)
      project_nums = column_data(zip, 'Projects', 'ID/Number')
      expect(project_nums).to match_array(company.projects.map(&:project_num))
    end

    it "assets-csv contains assets from user's company" do
      zip = ProjectExporter.export(user)
      asset_titles = column_data(zip, 'Assets', '*Title')
      expect(asset_titles).to match_array(company.assets.map(&:title))
    end
=begin
    it "assets-csv also contains the project names" do
      # Fails intermittently
      zip = ProjectExporter.export(user)
      actual = column_data(zip, 'assets', 'Project Name')
      expected = company.assets.map(&:project).map(&:name).uniq
      expect(actual).to match_array(expected)
    end
=end
  end
end
