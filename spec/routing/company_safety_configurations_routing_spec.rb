require "rails_helper"

RSpec.describe CompanySafetyConfigurationsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/company_safety_configurations").to route_to("company_safety_configurations#index")
    end

    it "routes to #new" do
      expect(:get => "/company_safety_configurations/new").to route_to("company_safety_configurations#new")
    end

    it "routes to #show" do
      expect(:get => "/company_safety_configurations/1").to route_to("company_safety_configurations#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/company_safety_configurations/1/edit").to route_to("company_safety_configurations#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/company_safety_configurations").to route_to("company_safety_configurations#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/company_safety_configurations/1").to route_to("company_safety_configurations#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/company_safety_configurations/1").to route_to("company_safety_configurations#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/company_safety_configurations/1").to route_to("company_safety_configurations#destroy", :id => "1")
    end

  end
end
