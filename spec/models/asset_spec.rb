require 'rails_helper'

describe Asset do
  it { should belong_to(:company) }
  it { should belong_to(:creator).class_name 'User' }
  it { should belong_to(:project) }
  it { should belong_to(:category) }
  it { should have_many :pdfs}
  it { should have_many :photos}
  it { should accept_nested_attributes_for :pdfs}
  it { should accept_nested_attributes_for :photos }
  it { should_not validate_inclusion_of(:units).in_array(Asset::RENT_UNITS) }
  it { should validate_presence_of :type }
  it { should validate_presence_of :title }
  it { should validate_presence_of :creator_id }
  it { should validate_presence_of :category_id }
  it { should validate_presence_of :location_zip }
  it { should validate_presence_of :zip_code }
  #it { should validate_presence_of :available_on }
  it { should validate_presence_of :expires_on }
  it { should validate_presence_of :quantity }
  # it { should validate_presence_of :action }


  context 'price for public assets' do
    before do
      subject.asset_kind = 'for_rent'
      subject.price = 0.001
    end
    it { should validate_numericality_of(:price).is_greater_than_or_equal_to(1).is_less_than(999999).only_integer.with_message('Max price can be 999999')}
  end

  context 'units for rentals' do
    before do
      subject.rental = true
      subject.owned = false
      subject.public = false
    end
    it { should validate_inclusion_of(:units).in_array(Asset::RENT_UNITS).allow_blank}
  end

  it { should validate_numericality_of(:quantity).is_greater_than_or_equal_to(0)}

  it 'price is optional for private assets' do
    company = create(:company, active: true)
    project = create(:project, company: company, contact: company.contact)
    asset = build(:asset, public: false, company: company, price: nil, project: project, creator: company.contact)
    expect(asset).to be_valid
    asset = build(:asset, public: false, company: company, price: 10, project: project, creator: company.contact)
    expect(asset).to be_valid
    asset = build(:asset, public: false, company: company, price: 'something', project: project, creator: company.contact)
    expect(asset).to be_valid
  end

  it 'filters out assets for inactive companies' do
    company = create(:company, active: true)
    project = create(:project, company: company, contact: company.contact)
    company_asset = create(:asset, company: company, project: project, creator: company.contact, public: true)
    asset = create(:asset)

    expect(Asset.count).to eq 2
    company.update(active: false)
    expect(Asset.count).to eq 1
  end

  context 'correct active status' do
    before do
      @assets = create_list(:asset, 2, expires_on: Time.zone.today + 1.day)
      @expired_asset = create(:asset, expires_on: Time.zone.today - 1.day)
    end

    it 'returns .active assets' do
      expect(Asset.count).to eq 3
      expect(Asset.active.count).to eq 2
    end

    it 'sold assets are not active' do
      @assets[0].sell
      expect(Asset.active.count).to eq 1
    end
  end

  it 'returns .expiring assets' do
    create_list(:asset, 2, expires_on: Time.zone.today + 1.day)
    create(:asset, expires_on: Time.zone.today - 1.day)

    expect(Asset.count).to eq 3
    expect(Asset.expiring_in(1.day).count).to eq 2
  end

  it 'maintains active state' do
    asset = build(:asset, expires_on: Time.zone.today + 1.day)
    expect(asset.active?).to eq true

    asset.expires_on = Time.zone.today - 1.day
    expect(asset.active?).to eq false
  end

  it 'maintains active state for sold asset' do
    asset = build(:asset, expires_on: Time.zone.today + 1.day)
    expect(asset.active?).to eq true

    asset.sell
    expect(asset.active?).to eq false
  end

  it 'initializes renew_expires_on to fail' do
    expect(Asset.new.renew_expires_on).to be_falsey
  end

  it 'allows to renew expiration date' do
    Timecop.freeze do
      asset = build(:asset)
      expect(asset.expires_on).to eq((Time.zone.now + 4.days).to_date)
      asset.update(renew_expires_on: '0')
      expect(asset.expires_on).to eq((Time.zone.now + 4.days).to_date)
      asset.update(renew_expires_on: '1')
      expect(asset.expires_on).to eq((Time.zone.now + Asset.available_duration + Asset.expire_threshold).to_date)
    end
  end
end
