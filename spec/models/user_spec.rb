require 'rails_helper'

describe User do
  it { is_expected.to have_many :projects }
  it { is_expected.to have_many(:assets).with_foreign_key :creator_id }
  it { is_expected.to have_many(:contact_projects).class_name('Project').with_foreign_key :contact_id }
  it { is_expected.to belong_to :company }
  it { is_expected.to have_many(:project_assignments).dependent :destroy }
  it { is_expected.to have_many(:projects).through :project_assignments }
  it { is_expected.to have_many :project_exports }

  it { is_expected.to validate_presence_of :first_name }
  it { is_expected.to validate_presence_of :last_name }
  it { is_expected.to validate_presence_of :role }
  it { is_expected.to_not have_valid(:password).when('short', 'nospecialchar') }
  it { is_expected.to have_valid(:password).when('goodpass2', 'passr!') }

  context 'should invalidate admin with a non-admin company' do
    let(:admin) { build :company_admin, role: 'admin' }

    before { admin.valid? }

    it { expect(admin.errors[:company]).to include 'must be Tenna' }
  end

  context 'should invalidate public_user with a company_id' do
    let(:public_user) { build :company_user, role: 'public_user' }

    before { public_user.valid? }

    it { expect(public_user.errors[:company_id]).to include 'must be blank' }
  end

  context 'should invalidate admin with a company_name' do
    let(:admin) { build :admin, company_name: 'test' }

    before { admin.valid? }

    it { expect(admin.errors[:company_name]).to include 'must be blank' }
  end

  context 'should invalidate company_admin with a company_name' do
    let(:company_admin) { build :company_admin, company_name: 'test' }

    before { company_admin.valid? }

    it { expect(company_admin.errors[:company_name]).to include 'must be blank' }
  end

  context 'should invalidate company_user with a company_name' do
    let(:company_user) { build :company_user, company_name: 'test' }

    before { company_user.valid? }

    it { expect(company_user.errors[:company_name]).to include 'must be blank' }
  end

  context 'should invalidate company_admins that don\'t have a company_id' do
    let(:company_admin) { build :company_admin, company_id: nil }

    before { company_admin.valid? }

    it { expect(company_admin.errors[:company_id]).to include 'can\'t be blank' }
  end

  context 'should invalidate company_users that don\'t have a company_id' do
    let(:company_user) { build :company_user, company_id: nil }

    before { company_user.valid? }

    it { expect(company_user.errors[:company_id]).to include 'can\'t be blank' }
  end

  context 'should invalidate public_users with a company_id' do
    let(:public_user) { build :company_user, role: 'public_user' }

    before { public_user.valid? }

    it { expect(public_user.errors[:company_id]).to include 'must be blank' }
  end

  describe '#can_assign_projects?' do
    context 'should return false when user is not a project admin for any projects' do
      let(:user)  { create(:project_assignment).user }

      it { expect(user.can_assign_projects?).to eq false }
    end

    context 'should return true when user is a project admin for at least one project' do
      let(:user) { create(:project_assignment_admin).user }

      it { expect(user.can_assign_projects?).to eq true }
    end
  end

  describe '#name_with_initial' do
    context 'return name with initial' do
      let(:user) { create :user, first_name: 'Robert', last_name: 'Trujillo' }

      it { expect(user.name_with_initial).to eq 'Robert T.' }
    end
  end
end
