require 'rails_helper'

RSpec.describe GroupsVersion, type: :model do
  it { should belong_to(:user) }
  it { should belong_to(:company) }
end
