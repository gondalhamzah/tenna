require "rails_helper"

describe Project do

  it { should accept_nested_attributes_for(:address).allow_destroy true }
  it { should accept_nested_attributes_for(:project_assignments).allow_destroy true }
  it { should have_many(:project_assignments).dependent :destroy }
  it { should have_many(:csv_imports).dependent :destroy }
  it { should have_many(:assets).dependent :nullify }
  it { should have_many(:users).through(:project_assignments) }
  it { should have_one(:address).dependent :destroy }
  it { should belong_to :company }
  it { should belong_to(:contact).class_name "User" }

  it { should validate_presence_of :name }
  it { should validate_presence_of :company }
  it { should validate_presence_of :contact }
  it { should validate_presence_of :closeout_date }
  it { should validate_presence_of :address }

  it "should invalidate projects when contact doesn't belong to the same company the project belongs to" do
    project = FactoryGirl.build(:project, contact: FactoryGirl.build(:user))
    project.valid?
    expect(project.errors[:contact]).to include "is not in the same company"
  end
end
