require "rails_helper"

describe ProjectAssignment do
  it { should belong_to :project }
  it { should belong_to :user }

  it { should validate_presence_of :project }
  it { should validate_presence_of :user }
  it { should validate_presence_of :role }

  it "should invalidate project assignments with users with a role of public_user" do
    project_assignment = FactoryGirl.build(:project_assignment, user: FactoryGirl.build(:user))
    project_assignment.valid?
    expect(project_assignment.errors[:user]).to include "user must have a role of 'Company User'"
  end
  it "should invalidate project assignments with users with a role of company_admin" do
    project_assignment = FactoryGirl.build(:project_assignment, user: FactoryGirl.build(:company_admin))
    project_assignment.valid?
    expect(project_assignment.errors[:user]).to include "user must have a role of 'Company User'"
  end
  it "should invalidate project assignments with users with a role of admin" do
    project_assignment = FactoryGirl.build(:project_assignment, user: FactoryGirl.build(:admin))
    project_assignment.valid?
    expect(project_assignment.errors[:user]).to include "user must have a role of 'Company User'"
  end
  it "should invalidate project_assignments that have the same user_id and project_id combination as an existing project_assignment" do
    project_assignment = FactoryGirl.create(:project_assignment)
    invalid_project_assignment = FactoryGirl.build(:project_assignment,
      user_id: project_assignment.user_id, project_id: project_assignment.project_id)
    invalid_project_assignment.valid?

    expect(invalid_project_assignment.errors[:user_id]).to include "is already assigned to project"
  end
end
