require 'rails_helper'
require 'elasticsearch/extensions/test/cluster'
describe AssetSearch do
  before(:all) do
    if !Elasticsearch::Extensions::Test::Cluster.running?
      @test_cluster_should_stop = true
      Elasticsearch::Extensions::Test::Cluster.start
    end
  end

  after(:all) do
    if @test_cluster_should_stop
      Elasticsearch::Extensions::Test::Cluster.stop
    end
  end

  before { AssetsIndex.purge! }

  describe 'assets filtered by active and permissions' do
    let(:company) { create(:company) }
    let(:user) { create(:company_user, company: company) }
    let(:company2) { create(:company) }
    let(:project) { create(:project, company: company, contact: company.contact) }
    let(:project2) { create(:project, company: company2, contact: company2.contact) }
    let(:company_asset) do
      create(
        :asset,
        title: 'robot',
        type: 'Equipment',
        company: company,
        project: project,
        creator: company.contact,
        asset_kind: 'rental'
      )
    end
    let(:company_asset2) do
      create(
        :asset,
        title: 'jarjar',
        type: 'Equipment',
        company: company,
        project: project,
        creator: company.contact,
        asset_kind: 'owned'
      )
    end
    let(:company2_asset) do
      create(
        :asset,
        title: 'vacuum',
        type: 'Equipment',
        company: company2,
        project: project2,
        creator: company2.contact,
        asset_kind: 'rental'
      )
    end
    let(:public_asset) { create(:asset, title: 'kangaroo', type: 'Equipment') }

    before do
      # Use `import!` here to be sure all the objects are imported
      # correctly before examples run.
      AssetsIndex.import!(asset: company_asset)
      AssetsIndex.import!(asset: company_asset2)
      AssetsIndex.import!(asset: company2_asset)
      AssetsIndex.import!(asset: public_asset)
    end

    it 'returns private assets for private users' do
      expect(AssetSearch.search(FilterSearch.new({query: 'robot'}), company.contact).load).to eq [company_asset.becomes(Equipment)]
    end

    it 'returns private assets for private users in same company' do
      expect(AssetSearch.search(FilterSearch.new({query: 'jarjar'}), user).load).to eq [company_asset2.becomes(Equipment)]
    end

    it 'returns public assets for private users' do
      expect(AssetSearch.search(FilterSearch.new({query: 'kangaroo'}), company.contact).load).to eq [public_asset.becomes(Equipment)]
    end

    it 'returns public assets for public users' do
      expect(AssetSearch.search(FilterSearch.new({query: 'kangaroo'}), create(:user)).load).to eq [public_asset.becomes(Equipment)]
    end

    it 'does not return private assets for public users' do
      expect(AssetSearch.search(FilterSearch.new({query: 'robot'}), create(:user)).load).to eq []
    end

    it 'does not return private assets for other companies' do
      expect(AssetSearch.search(FilterSearch.new({query: 'robot'}), company2.contact).load).to eq []
    end

    it 'does not return private assets in same company' do
      expect(AssetSearch.search(FilterSearch.new({query: 'vacuum'}), company2.contact).load).to eq [company2_asset.becomes(Equipment)]
    end

    it 'does not return expired public assets for public users' do
      expect(AssetSearch.search(FilterSearch.new({query: 'kangaroo'}), company.contact).load).to eq [public_asset.becomes(Equipment)]

      public_asset.update(expires_on: Time.zone.today - 1.day)
      AssetsIndex.import!(asset: public_asset)

      expect(AssetSearch.search(FilterSearch.new({query: 'kangaroo'}), create(:user)).load).to eq []
    end
    it 'returns public assets for public users with query not whole query string' do
      expect(AssetSearch.search(FilterSearch.new({query: 'kan'}), create(:user)).load).to eq [public_asset.becomes(Equipment)]
    end
  end
end
