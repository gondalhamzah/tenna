require 'rails_helper'

RSpec.describe Markable do
  context ".class_methods" do
    it "returns all markables for a marker" do
      user = create(:user)
      marks = create_list(:mark, 2, marker: user)
      markables = Asset.where(id: marks.collect(&:markable_id))
      expect(Asset.marked_as(marks[0].mark, user)).to match_array markables
    end

    it "returns all favorited assets for a user" do
      user = create(:user)
      favorites = create_list(:mark, 2, marker: user, mark: 'favorite')
      assets = Asset.where(id: favorites.collect(&:markable_id))
      expect(Asset.favorites_for(user)).to match_array assets
    end
  end

  context "#instance_methods" do
    it "returns a mark for a marker" do
      mark = create(:mark)
      expect(mark.markable.marked_as(mark.mark, mark.marker_id, mark.marker_type)).to eq mark
    end

    it "returns a favorite for a user" do
      user = create(:user)
      asset = create(:asset)
      favorite = create(:mark, marker: user, markable: asset, mark: 'favorite')
      expect(asset.favorite_for(user)).to eq favorite
    end
  end
end
