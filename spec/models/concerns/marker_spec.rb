require 'rails_helper'

RSpec.describe Marker do
  it "returns all marks for a markable" do
    user = create(:user)
    marks = create_list(:mark, 2, marker: user)
    expect(user.marked_as(marks[0].mark, Asset)).to match_array marks
  end

  it "returns all favorites for a user" do
    user = create(:user)
    favorites = create_list(:mark, 2, marker: user, mark: 'favorite')
    expect(user.favorite_asset_marks).to match_array favorites
  end
end
