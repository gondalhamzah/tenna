require 'rails_helper'

RSpec.describe TrackingCode, type: :model do
  it {should validate_presence_of(:token)}
  it {should validate_uniqueness_of(:token)}

  it "sets status and dateled_at on decommission" do
    trk = create(:tracking_code)
    expect(trk.deleted_at).to be_nil
    trk.decommission
    expect(trk.status).to eq("decommissioned")
    expect(trk.deleted_at).to_not be_nil
  end

  context "on tracking code change" do
    it "create an history item when a qr code is assigned" do
      asset = create(:asset)
      asset.tracking_code = create(:tracking_code)
      asset.reload
      expect(TrackingCode.first.resource.id).to eq(asset.id)
      #expect(asset.tracking_code.status).to eq("assigned")
    end
  end
end
