require "rails_helper"

describe Address do
  it { should belong_to :addressable }
  it "should invalidate addresses without a zip" do
    address = FactoryGirl.build(:address, zip: nil)
    address.valid?
    # expect(address.errors[:zip]).to include "can't be blank"
  end

  it 'tracks dirty state of formatted_address' do
    address = FactoryGirl.create(:address, city: 'sad city', lat: 10)
    address.lat = 12
    expect(address.formatted_address_changed?).to eq false
    address.city = 'fun city'
    expect(address.formatted_address_changed?).to eq true
  end

  context "when address belongs to a company" do
    before(:each) do
      @address = FactoryGirl.build(:company_address, line_1: nil, city: nil, state: nil)
      @address.valid?
    end
    it "should invalidate addresses without a line_1" do
      # expect(@address.errors[:line_1]).to include "can't be blank"
    end
    it "should invalidate addresses without a city" do
      # expect(@address.errors[:city]).to include "can't be blank"
    end
    it "should invalidate addresses without a state" do
      # expect(@address.errors[:state]).to include "can't be blank"
    end
  end

  context "when address belongs to a project" do
      before(:each) do
      @address = FactoryGirl.build(:project_address, line_1: nil, city: nil, state: nil)
      @address.valid?
    end
    it "should invalidate addresses without a line_1" do
      # expect(@address.errors[:line_1]).to include "can't be blank"
    end
    it "should invalidate addresses without a city" do
      # expect(@address.errors[:city]).to include "can't be blank"
    end
    it "should invalidate addresses without a state" do
      # expect(@address.errors[:state]).to include "can't be blank"
    end
  end
end
