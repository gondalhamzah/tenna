require 'rails_helper'

RSpec.describe Group, type: :model do

	it { should belong_to(:project) }
	it { should belong_to(:company) }
	it { should have_many(:assets).dependent :nullify }
	it { should have_many(:csv_imports).dependent :destroy }
  it { should validate_presence_of :name }
  it { should validate_presence_of :company }
  it { should validate_presence_of :project }

  

end
