require "rails_helper"

describe Company do
  it { should accept_nested_attributes_for :contact }
  it { should accept_nested_attributes_for :project }
  it { should accept_nested_attributes_for(:address).allow_destroy true }
  it { should have_many :users }
  it { should have_many :projects }
  it { should have_many :assets }
  it { should have_many :groups }
  it { should have_many :wanted_assets }
  it { should have_one(:address).dependent :destroy }
  it { should belong_to(:contact).class_name "User" }
  it { should belong_to(:project).dependent :destroy }

  it { should validate_presence_of :name }
  it { should validate_presence_of :address }
  it { should validate_presence_of :contact_id }
  # it { should validate_presence_of :project }
  it { should_not have_valid(:admin_seats).when("three", "!", 0, -2) }
  it { should have_valid(:admin_seats).when(3) }

  it "should invalidate company when contact belongs to a differenct company" do
    contact = FactoryGirl.create(:company_admin)
    company = FactoryGirl.build(:company, contact: contact)
    company.valid?
    expect(company.errors[:contact_id]).to include "is not in the same company"
  end

  it "should change active state for all users when active changes" do
    company = create(:company)
    users = FactoryGirl.create_list(:company_user, 3, company: company)

    expect(company.active?).to eq true
    expect(company.users.pluck(:active).all?).to be true

    company.update(active: false)
    company.update_active_state!

    company.update(active: true)
    company.update_active_state!

    expect(company.active?).to eq true
    expect(company.users.pluck(:active).all?).to be true
  end

  it "should invalidate if no default project exits" do
    company = create(:company)
    company.project = nil 
    # expect(company.save).to eq false
  end

  it "should invalidate if no address for project exits" do
    company = create(:company)
    company.address = nil 
    expect(company.save).to eq false
  end

  it "should invalidate if name project uniqueness fail" do
    company1 = create(:company)
    company2 = create(:company)
    company2.name = company1.name
    expect(company2.save).to eq false
  end
end
