require 'rails_helper'

RSpec.describe Mark, type: :model do
  it { should belong_to(:marker) }
  it { should belong_to(:markable) }
  it { should validate_presence_of :marker }
  it { should validate_presence_of :markable }
  it { should validate_presence_of :mark }
  it { should validate_inclusion_of(:mark).in_array(["favorite"]) }
  it { should validate_inclusion_of(:marker_type).in_array(["User"]) }
  it { should validate_inclusion_of(:markable_type).in_array(["Asset"]) }
end
