require 'rails_helper'

RSpec.describe ProjectExport, type: :model do
  it {should belong_to(:project)}
  it {should belong_to(:user)}

  it {should validate_presence_of(:user)}
end
