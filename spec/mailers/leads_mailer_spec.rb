require 'rails_helper'

RSpec.describe LeadsMailer, type: :mailer do
  describe 'send_email' do
    let(:mail) { LeadsMailer.send_email({:subject => "Learn More #{Time.now.strftime('%d/%m/%Y %I:%M:%S %p')}", :to => 'support@tenna.com', :from => 'admin@tenna.com'}) }

    it 'renders the headers' do
      Timecop.freeze do
        expect(mail.subject).to eq("Learn More #{Time.now.strftime('%d/%m/%Y %I:%M:%S %p')}")
      end
      expect(mail.to).to eq(['support@tenna.com', 'mtahir@tenna.com'])
      expect(mail.from).to eq(['admin@tenna.com'])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('Hello')
    end
  end

end
