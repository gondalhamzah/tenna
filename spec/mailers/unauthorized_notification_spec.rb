require "rails_helper"

RSpec.describe UnauthorizedNotification, type: :mailer do
  describe "notification" do
    let(:mail) { UnauthorizedNotification.notification("firstname", "lastname", "user_email", "user_company", "role", "asset_url", "url_of_scan", "lat", "lng") }

    it "renders the headers" do
      expect(mail.subject).to eq("ALERT: Unauthorized Scan")
    end

  end

end
