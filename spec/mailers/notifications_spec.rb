require 'rails_helper'

RSpec.describe Notifications, type: :mailer do
  describe 'expiring_assets' do
    let(:asset1) { create(:asset) }
    let(:creator) { asset1.creator }
    let(:asset2) { create(:asset, creator: creator) }
    let(:mail) { Notifications.expiring_assets(creator, [asset1, asset2]) }
    it 'renders the headers' do
      expect(mail.subject).to eq('Expiring assets')
      expect(mail.to).to eq([creator.email])
      expect(mail.from).to eq(['no-reply@tenna.com'])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('Assets is expiring in two days. Please renew them if you would like them to stay current on Tenna.')
    end
  end

end
