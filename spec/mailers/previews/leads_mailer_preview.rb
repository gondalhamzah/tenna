# Preview all emails at http://localhost:3000/rails/mailers/leads_mailer
class LeadsMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/leads_mailer/send_email
  def send_email
    LeadsMailer.send_email
  end

end
