# Preview all emails at http://localhost:3000/rails/mailers/backoffice_mailer
class BackofficeMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/backoffice_mailer/bsqr_urls
  def bsqr_urls
    BackofficeMailer.bsqr_urls
  end

end
