# Preview all emails at http://localhost:3000/rails/mailers/notifications
class NotificationsPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/notifications/expiring_assets
  def expiring_assets
    Notifications.expiring_assets
  end

end
