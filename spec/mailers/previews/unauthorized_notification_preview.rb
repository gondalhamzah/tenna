# Preview all emails at http://localhost:3000/rails/mailers/unsubscribed_notification
class UnauthorizedNotificationPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/unsubscribed_notification/notification
  def notification
    UnauthorizedNotification.notification
  end

end
