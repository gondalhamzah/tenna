require 'rails_helper'

RSpec.describe BackofficeMailer, type: :mailer do
  describe 'bsqr_urls' do
    let(:mail) { BackofficeMailer.bsqr_urls(['http://example.com/kjhskjdhkhdf']) }

    it 'renders the headers' do
      Timecop.freeze do
        expect(mail.subject).to eq("URLs for QR codes, created on #{Time.now.strftime('%d/%m/%Y %I:%M:%S %p')}")
      end
      expect(mail.to).to eq(['kolson@tenna.com', 'mtahir@tenna.com'])
      expect(mail.from).to eq(['mtahir@tenna.com'])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('Hi')
    end
  end
end
