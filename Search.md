# BuildSourced Search Documentation

## Maintaining the AssetsIndex

### What should change if Asset model undergoes structure changes (database migration)

All the Elasticsearch indexes are maintained via a Gem called Chewy, which is a high-level framework based on the official elasticsearch-ruby client. For example, in `asset.rb`...

```
update_index('assets#asset') { self }
```

... ensures that the index is updated when a CRUD operation occurs on an asset model. So as long as changes are made in Rails (ActiveRecord), the index is automatically maintained.

However, in the event the model undergoes structural changes, you will need to update `asset_index.rb` accordingly. For example, if you add a new field to the asset model, you'll likely want to index it by adding a new field in `asset_index.rb`.

### Should index be re-build in the event of such a change. And how?

Yes. Whenever making a structural change to the index, the easiest way to see that change reflected is to rebuild the index. This is done via Chewy.

```
rake chewy:reset
```

## Index querying

### What is the responsibility of the FilterableAssets class?

FilterableAssets is a service to retrieve filtered assets. It is initialized with the current user and various options, including a `FilterSearch` instance, containing a search string, filters and sort order specified by the user, and exposes 2 helpful methods.

`filtered_assets_scope`: Returns an Elasticsearch search scope, which can be used as a basis for further querying.

`assets`: Queries Elasticsearch using `filtered_assets_scope` and returns fully-populated asset instances for matches, ready to be rendered to the page in a list view.

### Where are the filters for index querying setup?

Reference `filter_search.rb` for a list of user-facing asset filters. Additionally, in `AssetSearch#search`, a number of additional filters are applied.

`active_and_permissible`: Ensures only active assets are returned. Additionally, ensures user can only see the assets they should be allowed to see based on their ability.

`for_creator`: If creator is set, returns only his assets.

`in_project`: If set, returns only assets for the given project. Used to show only projects assets on project details page.

`in_company`: If set, returns only assets for your company.


### What are the differences between AssetSearch#apply_filters and FilterSearch.

`filter_search.rb` is bound to the `_filters.html.erb`, which is rendered whenever asset filters are shown. It is responsible for holding the current search/filter state, so the filters can be re-rendered with their state.

`asset_search.rb` is the model responsible for actually building the Elasticsearch query. `apply_filters` is a method that adds filters (price range, favorites, etc.) to the Elasticsearch query being constructed.

### What parts to look for while debugging for missing results?

Chewy's hooks into the appropriate ActiveRecord callbacks to ensure that any model update is propagated to the search index. If you are seeing missing results, this means the database has been updated directly (not via ActiveRecord). This should be avoided. However, the easy fix is to simply rebuild the index.

```
rake chewy:reset
``` 

### In what places is full-text happening?

See `asset_search.rb`.

```
  FULL_TEXT_QUERY_FIELDS = [
    'asset.title',
    'asset.make',
    'asset.model',
    'asset.category',
    'asset.tag_list'
  ]
```

## Testing

### What are the helpers/libraries/settings to use while testing code involving Elasticsearch?

There is a spec for asset search (`spec/models/asset_search_spec.rb`). You must have Elasticsearch installed locally to run the test.

To run all tests, run...


```
rake test
rspec

```

[CircleCI](https://circleci.com/gh/srcbld/rz-buildsourced-web) is currently configured to run tests whenever commits are pushed to GitHub. In `circle.yml`, you can see we download Elasticsearch in the container in order to run these tests during CI.

### What parts of the search bits are covered well, and what parts need work?

Asset search basics have test coverage (i.e. returning only active assets that you have the ability to see, etc.), but there is certainly opportunity to add additional test coverage for the multitude of filters (price range, model year range, etc.).


## Deployment
### Are there any instructions specific to Elasticsearch deployment?

When deploying changes that impact the index, it needs to be rebuilt.

Example:

```
heroku run rake chewy:reset -a buildsourced-staging
```

This uses the [Heroku Toolbelt](https://toolbelt.herokuapp.com) to run the `rake chewy:reset` task against the `buildsourced-staging` application.

### Should the Elasticsearch be restarted with each deployment. If not, when should it be restarted

It never needs to be restarted. We're using AWS Elasticsearch, which is a managed solution.

### What should be done for monitoring it?

You can go into AWS and setup alerting on the Elasticsearch instance.

## Search

### How does search work / Describe the implementation

Assets and Projects are indexed as part of the model definition. Chewy integrates with ActiveRecord and makes this automatic when models are created/updated/deleted.

When using the marketplace, queries are dynamically built based on your query string and selected filters. Specifics about this is documented in code and other sections of this document. 

### Why elasticsearch was the implementation

From a technical perspective the use of a search engine is driven by the requirements of full text search, geospacial search, filtering, and sorting. These features, while supported by MySQL, would have severely limited the functionality and/or performance of the marketplace.

We compared ElasticSearch and Solr and went with ElasticSearch for a few reasons. The many ways in which they are comparable (features, scalability, performance, community, and cost) do not cause any significant difference at such a small scale. We went with ElasticSearch due to the better integration with Rails (as previously mentioned, using the Chewy gem), and prior experience with ElasticSearch on the team. 

### Explain the configuration and parameters for setup

Locally, Elasticsearch requires very little setup. Simply install it and start the service. By default, the application will connect on the default address and port.

For server environments (DEV, STG & PROD), there are dedicated AWS Elasticsearch instances. Within Heroku, there are config/environment variables that configure the appropriate AWS access keys, region, URL, etc.

### How elasticsearch is used, and by which models views and controllers

Models: Any model that has `update_index`. Currently: Asset, ZipCode, Photo, Project, Mark & Category

Controllers: Any controller that uses `FilterableAssets`. Currently: FavoritesController, ProjectsController, MarketplaceController, MapsController & AssetsController

Views: See Controllers


### Which files are touched by search

Not sure what this means. No files are modified as a result of searching.

### Which files and items are dependent upon search. Be specific with filenames and functions/actions.

With the exception of the dashboard, all pages that show lists of assets query Elasticsearch for their results. When viewing a single asset, we know its ID and lookup in the DB directly. For the dashboard, we used model-scoped queries (DB).

### Why is empty search used for marketplace

Not sure what this means. The initial state of the marketplace includes no user-specified query string or filters, just like all the views with that allow users to specify filters.

### What is the relationship between filters and elasticsearch

Any search string, filters and sort preferences set by the user are encapsulated in a FilterSearch instance. In the controller, the FilterAssets service builds an AssetSearch instance. This builds the actual Elasticsearch query string, with the relevant query & filters options.

This [webinar](https://www.elastic.co/webinars/get-started-with-elasticsearch) is a good reference for understanding what an actual Elasticsearch query looks like.

### Why is elasticsearch used for filters

See: "Why elasticsearch was the implementation"

### Describe the search query string used for each page that generates a search call

The model `filter_search.rb` is bound to the `_filters.html.erb`, which is rendered whenever asset filters are shown. Reference `filter_search.rb` to see all possible query strings.

### List all search query strings which are used

See: "Describe the search query string used for each page that generates a search call"